import request from "../utils/request";
import { API_HOST } from "./serviceUri";
import { stringify } from "qs";

export async function listcenter(payload) {
  
  return request(`${API_HOST}/api/Payment/ListCenter`, {
    method: "GET"
  });
}
export async function search(payload) {
  
  return request(`${API_HOST}/api/Payment/Search?${stringify(payload)}`, {
    method: "GET"
  });
}
export async function list(payload) {
  
  return request(`${API_HOST}/api/Payment/List?${stringify(payload)}`, {
    method: "GET"
  });
}
export async function view(payload) {
  
  return request(`${API_HOST}/api/Payment/View/${payload.id}`, {
    method: "GET"
  });
}
export async function insert(payload) {
  
  return request(`${API_HOST}/api/Payment/Insert`, {
    method: "POST",
    headers: {
      "Content-type": "application/json",
      
    },
    body: JSON.stringify(payload)
  });
}
export async function update(payload) {
  
  return request(`${API_HOST}/api/Payment/Update/${payload.id}`, {
    method: "POST",
    headers: {
      "Content-type": "application/json",
      
    },
    body: JSON.stringify(payload)
  });
}
export async function delete_payment(payload) {
  
  return request(`${API_HOST}/api/Payment/Delete/${payload.id}`, {
    method: "POST",
    headers: {
      "Content-type": "application/json",
      
    },
    body: {
      ...payload
    }
  });
}
