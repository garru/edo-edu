import request from "../utils/request";
import { API_HOST } from "./serviceUri";
import { stringify } from "qs";

export async function search(payload) {
  return request(`${API_HOST}/api/Student/Search?${stringify(payload)}`, {
    method: "GET"
  });
}

export async function view({ id, classId }) {
  return request(`${API_HOST}/api/Student/View/${id}/${classId}`, {
    method: "GET"
  });
}
export async function listcenter(payload) {
  return request(`${API_HOST}/api/Student/ListCenter`, {
    method: "GET"
  });
}
export async function listprogram(payload) {
  return request(`${API_HOST}/api/Student/ListProgram`, {
    method: "GET"
  });
}
export async function listclass({ centerId, programId }) {
  return request(`${API_HOST}/api/Student/ListClass/${centerId}/${programId}`, {
    method: "GET"
  });
}
export async function changeclass(payload) {
  const { id, classId } = payload;
  delete payload.id
  return request(`${API_HOST}/api/Student/ChangeClass/${id}/${classId}`, {
    method: "POST",
    headers: {
      "Content-type": "application/json"
    },
    body: JSON.stringify(payload)
  });
}
