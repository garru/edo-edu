import request from "../utils/request";
import { API_HOST } from "./serviceUri";
import { stringify } from "qs";

export async function departement(payload) {
  
  return request(`${API_HOST}/api/DepartementRole/Departement`, {
    method: "GET"
  });
}
export async function listbydepartmentid(payload) {
  
  return request(
    `${API_HOST}/api/DepartementRole/List/${payload.departmentId}`,
    {
      method: "GET"
    }
  );
}
export async function list(payload) {
  
  return request(`${API_HOST}/api/DepartementRole/List`, {
    method: "GET"
  });
}
export async function view(payload) {
  
  return request(`${API_HOST}/api/DepartementRole/View/${payload.id}`, {
    method: "GET"
  });
}
export async function insert(payload) {
  
  return request(`${API_HOST}/api/DepartementRole/Insert`, {
    method: "POST",
    headers: {
      "Content-type": "application/json"
    },
    body: JSON.stringify(payload)
  });
}
export async function update(payload) {
  
  return request(`${API_HOST}/api/DepartementRole/Update/${payload.id}`, {
    method: "POST",
    headers: {
      "Content-type": "application/json"
    },
    body: JSON.stringify(payload)
  });
}
export async function delete_departementrole(payload) {
  
  return request(`${API_HOST}/api/DepartementRole/Delete/${payload.id}`, {
    method: "POST",
    headers: {
      "Content-type": "application/json"
    },
    body: JSON.stringify(payload)
  });
}
