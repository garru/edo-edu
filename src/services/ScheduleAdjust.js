import request from '../utils/request';
import { API_HOST } from "./serviceUri";
import { stringify } from 'qs';

export async function view(payload) {
  return request(`${ API_HOST }/api/ScheduleAdjust/View/${payload.id}` , {
    method: 'GET'
  });
}
export async function lesson(payload) {
  return request(`${ API_HOST }/api/ScheduleAdjust/Lesson/${payload.id}` , {
    method: 'GET'
  });
}
export async function evaluation(payload) {
  return request(`${ API_HOST }/api/ScheduleAdjust/Evaluation/${payload.id}/{studentId}?${stringify(payload)}` , {
    method: 'GET'
  });
}
export async function listcenter(payload) {
  return request(`${ API_HOST }/api/ScheduleAdjust/ListCenter` , {
    method: 'GET'
  });
}
export async function listtime(payload) {
  return request(`${ API_HOST }/api/ScheduleAdjust/ListTime/${payload.centerId}` , {
    method: 'GET'
  });
}
export async function listroom(payload) {
  return request(`${ API_HOST }/api/ScheduleAdjust/ListRoom/${payload.centerId}` , {
    method: 'GET'
  });
}
export async function listdomestic(payload) {
  return request(`${ API_HOST }/api/ScheduleAdjust/ListDomestic/${payload.centerId}` , {
    method: 'GET'
  });
}
export async function listforeign(payload) {
  return request(`${ API_HOST }/api/ScheduleAdjust/ListForeign/${payload.centerId}` , {
    method: 'GET'
  });
}
export async function updatelessonbyid(payload) {
  return request(`${ API_HOST }/api/ScheduleAdjust/UpdateLesson/${payload.id}` , {
    method: 'GET'
  });
}
export async function updatelessonbyid(payload) {
  return request(`${ API_HOST }/api/ScheduleAdjust/UpdateLesson/${payload.id}` , {
    method: 'POST',
    body: {
      ...payload,
    },
  });
}
