import request from "../utils/request";
import { API_HOST } from "./serviceUri";
import { stringify } from "qs";

export async function listcenter(payload) {
  
  return request(`${API_HOST}/api/Account/ListCenter`, {
    method: "GET"
  });
}
export async function gender(payload) {
  
  return request(`${API_HOST}/api/Account/Gender`, {
    method: "GET"
  });
}
export async function maritalstatus(payload) {
  
  return request(`${API_HOST}/api/Account/MaritalStatus`, {
    method: "GET"
  });
}
export async function contracttype(payload) {
  
  return request(`${API_HOST}/api/Account/ContractType`, {
    method: "GET"
  });
}
export async function listnationality(payload) {
  
  return request(`${API_HOST}/api/Account/ListNationality`, {
    method: "GET"
  });
}
export async function listprovince(payload) {
  
  return request(`${API_HOST}/api/Account/ListProvince`, {
    method: "GET"
  });
}
export async function listgroup(payload) {
  
  return request(`${API_HOST}/api/Account/ListGroup`, {
    method: "GET"
  });
}
export async function search(payload) {
  
  return request(`${API_HOST}/api/Account/Search?${stringify(payload)}`, {
    method: "GET"
  });
}
export async function view(payload) {
  
  return request(`${API_HOST}/api/Account/View/${payload.id}`, {
    method: "GET"
  });
}
export async function insert(payload) {
  
  return request(`${API_HOST}/api/Account/Insert`, {
    method: "POST",
    headers: {
      "Content-type": "application/json",
      
    },
    body: JSON.stringify(payload)
  });
}
export async function update(payload) {
  
  return request(`${API_HOST}/api/Account/Update/${payload.id}`, {
    method: "POST",
    headers: {
      "Content-type": "application/json",
      
    },
    body: JSON.stringify(payload)
  });
}
export async function delete_account(payload) {
  
  return request(`${API_HOST}/api/Account/Delete/${payload.id}`, {
    method: "POST",
    headers: {
      "Content-type": "application/json",
      
    },
    body: {
      ...payload
    }
  });
}
export async function lock(payload) {
  
  return request(`${API_HOST}/api/Account/Lock/${payload.id}`, {
    method: "POST",
    headers: {
      "Content-type": "application/json",
      
    },
    body: {
      ...payload
    }
  });
}
export async function unlock(payload) {
  
  return request(`${API_HOST}/api/Account/UnLock/${payload.id}`, {
    method: "POST",
    headers: {
      "Content-type": "application/json",
      
    },
    body: {
      ...payload
    }
  });
}

export async function getprovince() {
  
  return request(`${API_HOST}/api/Account/ListProvince`, {
    method: "GET"
  });
}
export async function getnationality() {
  
  return request(`${API_HOST}/api/Account/ListNationality`, {
    method: "GET"
  });
}
