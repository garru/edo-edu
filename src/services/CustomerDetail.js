import request from "../utils/request";
import { API_HOST } from "./serviceUri";
import { stringify } from "qs";

export async function view(payload) {
  return request(`${API_HOST}/api/CustomerDetail/View/${payload.id}`, {
    method: "GET"
  });
}
export async function listdiscussion(payload) {
  return request(
    `${API_HOST}/api/CustomerDetail/ListDiscussion/${payload.id}`,
    {
      method: "GET"
    }
  );
}
export async function insertdiscussion(payload) {
  return request(`${API_HOST}/api/CustomerDetail/InsertDiscussion`, {
    method: "POST",
    headers: {
      "Content-type": "application/json"
    },
    body: JSON.stringify(payload)
  });
}
export async function listappointment(payload) {
  return request(
    `${API_HOST}/api/CustomerDetail/ListAppointment/${payload.id}`,
    {
      method: "GET"
    }
  );
}
export async function listinvoice(payload) {
  return request(`${API_HOST}/api/CustomerDetail/ListInvoice/${payload.id}`, {
    method: "GET"
  });
}
export async function printinvoice(payload) {
  return request(
    `${API_HOST}/api/CustomerDetail/PrintInvoice/${payload.invoiceId}`,
    {
      method: "GET"
    }
  );
}
export async function liststudentbyid(payload) {
  return request(`${API_HOST}/api/CustomerDetail/ListStudent/${payload.id}`, {
    method: "GET"
  });
}
export async function liststudentbystudentid(payload) {
  return request(`${API_HOST}/api/CustomerDetail/ListStudent/${payload.id}`, {
    method: "GET"
  });
}
export async function listlesson(payload) {
  return request(`${API_HOST}/api/CustomerDetail/ListLesson/${payload.id}`, {
    method: "GET"
  });
}
export async function viewlesson(payload) {
  return request(
    `${API_HOST}/api/CustomerDetail/ViewLesson/${payload.studentId}/${payload.lessonId}`,
    {
      method: "GET"
    }
  );
}
export async function listcourse(payload) {
  return request(`${API_HOST}/api/CustomerDetail/ListCourse/${payload.id}`, {
    method: "GET"
  });
}
