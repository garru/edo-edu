export function getErrorMessage(errors, field) {
  let type = errors[field]?.type;
  if (!type) return "";
  if (type === "required") return "Trường bắt buộc";
  switch (field) {
    case "email":
      if ((type = "pattern")) return "Email không đúng định dạng";
      break;
    default:
        break;
  }
}
