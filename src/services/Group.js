import request from "../utils/request";
import { API_HOST } from "./serviceUri";
import { stringify } from "qs";

export async function list(payload) {
  return request(`${API_HOST}/api/Group/List`, {
    method: "GET"
  });
}
export async function permission(payload) {
  return request(`${API_HOST}/api/Group/Permission`, {
    method: "GET"
  });
}
export async function view(payload) {
  return request(`${API_HOST}/api/Group/View/${payload.id}`, {
    method: "GET"
  });
}
export async function insert(payload) {
  return request(`${API_HOST}/api/Group/Insert`, {
    method: "POST",
    headers: {
      "Content-type": "application/json"
    },
    body: JSON.stringify(payload)
  });
}
export async function update(payload) {
  const { id } = payload;
  payload.id = undefined;
  return request(`${API_HOST}/api/Group/Update/${id}`, {
    method: "POST",
    headers: {
      "Content-type": "application/json"
    },
    body: JSON.stringify(payload)
  });
}
export async function delete_group(payload) {
  return request(`${API_HOST}/api/Group/Delete/${payload.id}`, {
    method: "POST",
    headers: {
      "Content-type": "application/json"
    },
    body: JSON.stringify(payload)
  });
}
