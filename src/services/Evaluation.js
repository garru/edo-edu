import request from "../utils/request";
import { API_HOST } from "./serviceUri";
import { stringify } from "qs";

export async function evaluationobject(payload) {
  
  return request(`${API_HOST}/api/Evaluation/EvaluationObject`, {
    method: "GET"
  });
}
export async function list(payload) {
  
  return request(`${API_HOST}/api/Evaluation/List`, {
    method: "GET"
  });
}
export async function view(payload) {
  
  return request(`${API_HOST}/api/Evaluation/View/${payload.id}`, {
    method: "GET"
  });
}
export async function insert(payload) {
  
  return request(`${API_HOST}/api/Evaluation/Insert`, {
    method: "POST",
    headers: {
      "Content-type": "application/json",
      
    },
    body: JSON.stringify(payload)
  });
}
export async function update(payload) {
  
  return request(`${API_HOST}/api/Evaluation/Update/${payload.id}`, {
    method: "POST",
    headers: {
      "Content-type": "application/json",
      
    },
    body: {
      ...payload
    }
  });
}
export async function delete_evaluation(payload) {
  
  return request(`${API_HOST}/api/Evaluation/Delete/${payload.id}`, {
    method: "POST",
    headers: {
      "Content-type": "application/json",
      
    },
    body: {
      ...payload
    }
  });
}
