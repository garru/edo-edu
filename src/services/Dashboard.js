import request from "../utils/request";
import { API_HOST } from "./serviceUri";
import { stringify } from "qs";

export async function schedulenext(payload) {
  return request(`${ API_HOST }/api/Dashboard/ScheduleNext` , {
    method: 'GET'
  });
}
export async function schedulecourseware(payload) {
  return request(`${ API_HOST }/api/Dashboard/ScheduleCourseware?${stringify(payload)}` , {
    method: 'GET'
  });
}
export async function coursewarereceive(payload) {
  return request(`${ API_HOST }/api/Dashboard/CoursewareReceive/${payload.scheduleId}?${stringify(payload)}` , {
    method: 'POST',
    body: {
      ...payload,
    },
  });
}
export async function coursewarereturn(payload) {
  return request(`${ API_HOST }/api/Dashboard/CoursewareReturn/${payload.scheduleId}?${stringify(payload)}` , {
    method: 'POST',
    body: {
      ...payload,
    },
  });
}
export async function todolist(payload) {
  return request(`${API_HOST}/api/Dashboard/TodoList`, {
    method: "GET"
  });
}
export async function workdaystatistic(payload) {
  return request(`${API_HOST}/api/Dashboard/WorkdayStatistic`, {
    method: "GET"
  });
}
