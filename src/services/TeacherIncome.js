import request from '../utils/request';
import { API_HOST } from "./serviceUri";
import { stringify } from 'qs';

export async function search(payload) {
  return request(`${ API_HOST }/api/TeacherIncome/Search?${stringify(payload)}` , {
    method: 'GET'
  });
}
