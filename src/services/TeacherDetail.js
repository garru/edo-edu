import request from '../utils/request';
import { API_HOST } from "./serviceUri";
import { stringify } from 'qs';

export async function view(payload) {
  return request(`${ API_HOST }/api/TeacherDetail/View/${payload.id}` , {
    method: 'GET'
  });
}
export async function evaluation(payload) {
  return request(`${ API_HOST }/api/TeacherDetail/Evaluation/${payload.id}` , {
    method: 'GET'
  });
}
export async function observationhistory(payload) {
  return request(`${ API_HOST }/api/TeacherDetail/ObservationHistory/${payload.id}?${stringify(payload)}` , {
    method: 'GET'
  });
}
export async function observationview(payload) {
  return request(`${ API_HOST }/api/TeacherDetail/ObservationView/${payload.id}/{observationId}?${stringify(payload)}` , {
    method: 'GET'
  });
}
export async function parentcommentlist(payload) {
  return request(`${ API_HOST }/api/TeacherDetail/ParentCommentList/${payload.id}` , {
    method: 'GET'
  });
}
export async function parentcommentview(payload) {
  return request(`${ API_HOST }/api/TeacherDetail/ParentCommentView/${payload.id}/{parentCommentId}?${stringify(payload)}` , {
    method: 'GET'
  });
}
