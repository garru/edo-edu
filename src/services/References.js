import request from '../utils/request';
import { API_HOST } from "./serviceUri";

export async function getGioiTinh() {
  return request(`${ API_HOST }/api/Account/Gender` , {
    method: 'GET'
  });
}
export async function getTinhTrangHonNhan() {
  return request(`${ API_HOST }/api/Account/MaritalStatus` , {
    method: 'GET'
  });
}
export async function getDanhMucHopDong() {
  return request(`${ API_HOST }/api/Account/ContractType` , {
    method: 'GET'
  });
}
export async function getDanhSachThuongHieu() {
  return request(`${ API_HOST }/api/Brand/List` , {
    method: 'GET'
  });
}
export async function getDanhSachTrungTam() {
  return request(`${ API_HOST }/api/Center/List` , {
    method: 'GET'
  });
}
export async function getDanhSachPhongBan() {
  return request(`${ API_HOST }/api/DepartementRole/Departement` , {
    method: 'GET'
  });
}
export async function getDanhSachQuyen() {
  return request(`${ API_HOST }/api//Group/Permission` , {
    method: 'GET'
  });
}
export async function getDanhSachDoiTuongDanhGia() {
  return request(`${ API_HOST }/api/Evaluation/EvaluationObject` , {
    method: 'GET'
  });
}
export async function getTieuChiDanhGia() {
  return request(`${ API_HOST }/api/ProgramLesson/ListEvaluation` , {
    method: 'GET'
  });
}
export async function getDanhMucApDungCho() {
  return request(`${ API_HOST }/api/StudentStatus/EvaluationObject` , {
    method: 'GET'
  });
}
