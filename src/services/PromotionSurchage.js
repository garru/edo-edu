import request from "../utils/request";
import { API_HOST } from "./serviceUri";
import { stringify } from "qs";

export async function listcenter(payload) {
  
  return request(`${API_HOST}/api/PromotionSurchage/ListCenter`, {
    method: "GET"
  });
}
export async function search(payload) {
  
  return request(
    `${API_HOST}/api/PromotionSurchage/Search?${stringify(payload)}`,
    {
      method: "GET"
    }
  );
}
export async function list(payload) {
  
  return request(
    `${API_HOST}/api/PromotionSurchage/List?${stringify(payload)}`,
    {
      method: "GET"
    }
  );
}
export async function codes(payload) {
  
  return request(
    `${API_HOST}/api/PromotionSurchage/Codes?${stringify(payload)}`,
    {
      method: "GET"
    }
  );
}
export async function view(payload) {
  
  return request(`${API_HOST}/api/PromotionSurchage/View/${payload.id}`, {
    method: "GET"
  });
}
export async function insert(payload) {
  
  return request(`${API_HOST}/api/PromotionSurchage/Insert`, {
    method: "POST",
    headers: {
      "Content-type": "application/json",
      
    },
    body: {
      ...payload
    }
  });
}
export async function update(payload) {
  
  return request(`${API_HOST}/api/PromotionSurchage/Update/${payload.id}`, {
    method: "POST",
    headers: {
      "Content-type": "application/json",
      
    },
    body: {
      ...payload
    }
  });
}
export async function delete_promotionsurchage(payload) {
  
  return request(`${API_HOST}/api/PromotionSurchage/Delete/${payload.id}`, {
    method: "POST",
    headers: {
      "Content-type": "application/json",
      
    },
    body: {
      ...payload
    }
  });
}
