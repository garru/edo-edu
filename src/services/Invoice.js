import request from "../utils/request";
import { API_HOST } from "./serviceUri";
import { stringify } from "qs";

export async function listcenter(payload) {
  return request(`${API_HOST}/api/Invoice/ListCenter`, {
    method: "GET"
  });
}
export async function listtype(payload) {
  return request(`${API_HOST}/api/Invoice/ListType`, {
    method: "GET"
  });
}
export async function listclass(payload) {
  return request(`${API_HOST}//api/Invoice/ListClass/${payload.centerId}`, {
    method: "GET"
  });
}
export async function listschedule(payload) {
  console.log(payload);
  return request(`${API_HOST}/api/Invoice/ListSchedule/${payload.centerId}`, {
    method: "GET"
  });
}
export async function listpayment(payload) {
  return request(`${API_HOST}/api/Invoice/ListPayment/${payload.centerId}`, {
    method: "GET"
  });
}
export async function listprice(payload) {
  return request(`${API_HOST}/api/Invoice/ListPrice/${payload.centerId}`, {
    method: "GET"
  });
}
export async function listissuer(payload) {
  return request(`${API_HOST}/api/Invoice/ListIssuer/${payload.centerId}`, {
    method: "GET"
  });
}
export async function getpromotion(payload) {
  return request(`${API_HOST}/api/Invoice/GetPromotion/${payload.code}`, {
    method: "GET"
  });
}
export async function listpromotion(payload) {
  return request(`${API_HOST}/api/Invoice/ListPromotion`, {
    method: "GET"
  });
}

export async function listsurchage(payload) {
  return request(`${API_HOST}/api/Invoice/ListSurchage`, {
    method: "GET"
  });
}

export async function search(payload) {
  return request(`${API_HOST}/api/Invoice/Search?${stringify(payload)}`, {
    method: "GET"
  });
}

export async function view(payload) {
  return request(`${API_HOST}/api/Invoice/View/${payload.id}`, {
    method: "GET"
  });
}

export async function insert(payload) {
  return request(`${API_HOST}/api/Invoice/Insert`, {
    method: "POST",
    headers: {
      "Content-type": "application/json"
    },
    body: JSON.stringify(payload)
  });
}
export async function update(payload) {
  return request(`${API_HOST}/api/Invoice/Update/${payload.id}`, {
    method: "POST",
    headers: {
      "Content-type": "application/json"
    },
    body: JSON.stringify(payload)
  });
}

export async function paid(payload) {
  return request(`${API_HOST}/api/Invoice/Paid/${payload.id}`, {
    method: "POST",
    headers: {
      "Content-type": "application/json"
    },
    body: JSON.stringify(payload)
  });
}

export async function _delete(payload) {
  return request(`${API_HOST}/api/Invoice/Delete/${payload.id}`, {
    method: "POST",
    headers: {
      "Content-type": "application/json"
    },
    body: JSON.stringify(payload)
  });
}
