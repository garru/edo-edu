import request from "../utils/request";
import { API_HOST } from "./serviceUri";
import { stringify } from "qs";

export async function listcenter(payload) {
  
  return request(`${API_HOST}/api/TeachingTime/ListCenter`, {
    method: "GET"
  });
}
export async function list(payload) {
  
  return request(`${API_HOST}/api/TeachingTime/List`, {
    method: "GET"
  });
}
export async function view(payload) {
  
  return request(`${API_HOST}/api/TeachingTime/View/${payload.id}`, {
    method: "GET"
  });
}
export async function insert(payload) {
  
  return request(`${API_HOST}/api/TeachingTime/Insert`, {
    method: "POST",
    headers: {
      "Content-type": "application/json",
      
    },
    body: JSON.stringify(payload)
  });
}
export async function update(payload) {
  
  return request(`${API_HOST}/api/TeachingTime/Update/${payload.id}`, {
    method: "POST",
    headers: {
      "Content-type": "application/json",
      
    },
    body: JSON.stringify(payload)
  });
}
export async function delete_teachingtime(payload) {
  
  return request(`${API_HOST}/api/TeachingTime/Delete/${payload.id}`, {
    method: "POST",
    headers: {
      "Content-type": "application/json",
      
    },
    body: {
      ...payload
    }
  });
}
