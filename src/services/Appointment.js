import request from "../utils/request";
import { API_HOST } from "./serviceUri";
import { stringify } from "qs";

export async function listperform(payload) {
  
  return request(
    `${API_HOST}/api/Appointment/ListPerform?${stringify(payload)}`,
    {
      method: "GET"
    }
  );
}
export async function insert(payload) {
  
  return request(`${API_HOST}/api/Appointment/Insert`, {
    method: "POST",
    headers: {
      "Content-type": "application/json",
      
    },
    body: JSON.stringify(payload)
  });
}
export async function view(payload) {
  
  return request(`${API_HOST}/api/Appointment/View/${payload.id}`, {
    method: "GET"
  });
}
export async function update(payload) {
  const id = payload.id;
  delete payload.id;
  return request(`${API_HOST}/api/Appointment/Update/${id}`, {
    method: "POST",
    headers: {
      "Content-type": "application/json",
      
    },
    body: JSON.stringify(payload)
  });
}
