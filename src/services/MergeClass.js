import request from "../utils/request";
import { API_HOST } from "./serviceUri";

export async function listcenter(payload) {
  return request(`${API_HOST}/api/ClassMerge/ListCenter`, {
    method: "GET"
  });
}
export async function listclass(payload) {
  return request(`${API_HOST}/api/ClassMerge/ListClass/${payload.centerId}`, {
    method: "GET"
  });
}
export async function merge(payload) {
  return request(`${API_HOST}/api/ClassMerge/Merge`, {
    method: "POST",
    headers: {
      "Content-type": "application/json"
    },
    body: JSON.stringify(payload)
  });
}
