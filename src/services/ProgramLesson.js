import request from "../utils/request";
import { API_HOST } from "./serviceUri";
import { stringify } from "qs";

export async function listevaluation(payload) {
  
  return request(`${API_HOST}/api/ProgramLesson/ListEvaluation`, {
    method: "GET"
  });
}
export async function list(payload) {
  
  return request(`${API_HOST}/api/ProgramLesson/List/${payload.programId}`, {
    method: "GET"
  });
}
export async function view(payload) {
  return request(`${ API_HOST }/api/ProgramLesson/View/${payload.id}` , {
    method: 'GET'
  });
}
export async function upload(payload) {
  return request(`${ API_HOST }/api/ProgramLesson/Upload` , {
    method: 'POST',
    body: {
      ...payload,
    },
  });
}
export async function download(payload) {
  return request(`${ API_HOST }/api/ProgramLesson/Download/${payload.code}` , {
    method: 'GET'
  });
}
export async function insert(payload) {
  
  return request(`${API_HOST}/api/ProgramLesson/Insert`, {
    method: "POST",
    headers: {
      "Content-type": "application/json",
      
    },
    body: JSON.stringify(payload)
  });
}
export async function update(payload) {
  
  return request(`${API_HOST}/api/ProgramLesson/Update/${payload.id}`, {
    method: "POST",
    headers: {
      "Content-type": "application/json",
      
    },
    body: {
      ...payload
    }
  });
}
export async function delete_programlesson(payload) {
  
  return request(`${API_HOST}/api/ProgramLesson/Delete/${payload.id}`, {
    method: "POST",
    headers: {
      "Content-type": "application/json",
      
    },
    body: {
      ...payload
    }
  });
}
