import request from "../utils/request";
import { API_HOST } from "./serviceUri";
import { stringify } from "qs";

export async function currentstudent({ classId, id }) {
  return request(`${API_HOST}/api/StudentDetail/View/${id}/${classId}`, {
    method: "GET"
  });
}

export async function currentschedule({ classId, id }) {
  return request(`${API_HOST}/api/StudentDetail/Schedule/${id}/${classId}`, {
    method: "GET"
  });
}

export async function currentreserve({ classId, id }) {
  return request(`${API_HOST}/api/StudentDetail/Reserve/${id}/${classId}`, {
    method: "GET"
  });
}

export async function currentcredit({ classId, id }) {
  return request(`${API_HOST}/api/StudentDetail/Credit/${id}/${classId}`, {
    method: "GET"
  });
}
