import request from "../utils/request";
import { API_HOST } from "./serviceUri";
import { stringify } from "qs";

export async function listcenter(payload) {
  
  return request(`${API_HOST}/api/Room/ListCenter`, {
    method: "GET"
  });
}
export async function listtype(payload) {
  
  return request(`${API_HOST}/api/Room/ListType`, {
    method: "GET"
  });
}
export async function liststatus(payload) {
  
  return request(`${API_HOST}/api/Room/ListStatus`, {
    method: "GET"
  });
}
export async function search(payload) {
  
  return request(`${API_HOST}/api/Room/Search?${stringify(payload)}`, {
    method: "GET"
  });
}
export async function view(payload) {
  
  return request(`${API_HOST}/api/Room/View/${payload.id}`, {
    method: "GET"
  });
}
export async function insert(payload) {
  
  return request(`${API_HOST}/api/Room/Insert`, {
    method: "POST",
    headers: {
      "Content-type": "application/json",
      
    },
    body: JSON.stringify(payload)
  });
}
export async function update(payload) {
  
  return request(`${API_HOST}/api/Room/Update/${payload.id}`, {
    method: "POST",
    headers: {
      "Content-type": "application/json",
      
    },
    body: JSON.stringify(payload)
  });
}
export async function delete_room(payload) {
  
  return request(`${API_HOST}/api/Room/Delete/${payload.id}`, {
    method: "POST",
    headers: {
      "Content-type": "application/json",
      
    }
  });
}
export async function checklesson(payload) {
  
  return request(`${API_HOST}/api/Room/CheckLesson`, {
    method: "POST",
    headers: {
      "Content-type": "application/json",
      
    },
    body: {
      ...payload
    }
  });
}
export async function listroom(payload) {
  
  return request(`${API_HOST}/api/Room/ListRoom/${payload.centerId}`, {
    method: "GET"
  });
}
export async function teacherdomestic(payload) {
  
  return request(`${API_HOST}/api/Room/TeacherDomestic/${payload.centerId}`, {
    method: "GET"
  });
}
export async function teacherforeign(payload) {
  
  return request(`${API_HOST}/api/Room/TeacherForeign/${payload.centerId}`, {
    method: "GET"
  });
}
