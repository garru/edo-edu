import request from "../utils/request";
import { API_HOST } from "./serviceUri";
import { stringify } from "qs";

export async function search(payload) {
  
  return request(`${API_HOST}/api/WorkCalendar/Search?${stringify(payload)}`, {
    method: "GET"
  });
}
export async function view(payload) {
  
  return request(`${API_HOST}/api/WorkCalendar/View/${payload.id}`, {
    method: "GET"
  });
}
export async function upload(payload) {
  
  return request(`${API_HOST}/api/WorkCalendar/Upload`, {
    method: "POST",
    headers: {
      "Content-type": "application/json",
      
    },
    body: {
      ...payload
    }
  });
}
export async function template(payload) {
  
  return request(`${API_HOST}/api/WorkCalendar/Template`, {
    method: "GET"
  });
}
export async function import_workcalendar(payload) {
  
  return request(`${API_HOST}/api/WorkCalendar/Import`, {
    method: "POST",
    headers: {
      "Content-type": "application/json",
      
    },
    body: {
      ...payload
    }
  });
}
export async function insert(payload) {
  
  return request(`${API_HOST}/api/WorkCalendar/Insert`, {
    method: "POST",
    headers: {
      "Content-type": "application/json",
      
    },
    body: {
      ...payload
    }
  });
}
export async function update(payload) {
  
  return request(`${API_HOST}/api/WorkCalendar/Update/${payload.id}`, {
    method: "POST",
    headers: {
      "Content-type": "application/json",
      
    },
    body: {
      ...payload
    }
  });
}
export async function delete_workcalendar(payload) {
  
  return request(`${API_HOST}/api/WorkCalendar/Delete/${payload.id}`, {
    method: "POST",
    headers: {
      "Content-type": "application/json",
      
    },
    body: {
      ...payload
    }
  });
}
