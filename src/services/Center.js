import request from "../utils/request";
import { API_HOST } from "./serviceUri";
import { stringify } from "qs";

export async function listbrand(payload) {
  return request(`${API_HOST}/api/Center/ListBrand`, {
    method: "GET"
  });
}
export async function listdirector(payload) {
  return request(`${API_HOST}/api/Center/ListDirector?${stringify(payload)}`, {
    method: "GET"
  });
}
export async function list(payload) {
  return request(`${API_HOST}/api/Center/List`, {
    method: "GET"
  });
}
export async function view(payload) {
  return request(`${API_HOST}/api/Center/View/${payload.id}`, {
    method: "GET"
  });
}
export async function insert(payload) {
  return request(`${API_HOST}/api/Center/Insert`, {
    method: "POST",
    headers: {
      "Content-type": "application/json"
    },
    body: JSON.stringify(payload)
  });
}
export async function update(payload) {
  const { id } = payload;
  payload.id = undefined;
  return request(`${API_HOST}/api/Center/Update/${id}`, {
    method: "POST",
    headers: {
      "Content-type": "application/json"
    },
    body: JSON.stringify(payload)
  });
}
export async function delete_center(payload) {
  return request(`${API_HOST}/api/Center/Delete/${payload.id}`, {
    method: "POST",
    headers: {
      "Content-type": "application/json"
    },
    body: JSON.stringify(payload)
  });
}
