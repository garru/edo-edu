import request from "../utils/request";
import { API_HOST } from "./serviceUri";
import { stringify } from "qs";

export async function viewreserve({ classId, id }) {
  return request(
    `${API_HOST}/api/StudentReserve/ViewReserve/${id}/${classId}`,
    {
      method: "GET"
    }
  );
}

export async function listmonth({ classId, id }) {
  return request(`${API_HOST}/api/StudentReserve/ListMonth`, {
    method: "GET"
  });
}

export async function viewback({ classId, id }) {
  return request(`${API_HOST}/api/StudentReserve/ViewBack/${id}/${classId}`, {
    method: "GET"
  });
}

export async function listcenter({ classId, id }) {
  return request(`${API_HOST}/api/StudentReserve/ListCenter`, {
    method: "GET"
  });
}

export async function listprogram({ classId, id }) {
  return request(`${API_HOST}/api/StudentReserve/ListProgram`, {
    method: "GET"
  });
}
export async function listclass({ centerId, programId }) {
  return request(
    `${API_HOST}/api/StudentReserve/ListClass/${centerId}/${programId}`,
    {
      method: "GET"
    }
  );
}
export async function listschedule({ classId, id }) {
  return request(`${API_HOST}/api/StudentReserve/ListSchedule/${classId}`, {
    method: "GET"
  });
}

export async function insertreserve(payload) {
  const { id, classId } = payload;
  delete payload.id;
  return request(
    `${API_HOST}/api/StudentReserve/InsertReserve/${id}/${classId}`,
    {
      method: "POST",
      headers: {
        "Content-type": "application/json"
      },
      body: JSON.stringify(payload)
    }
  );
}
export async function insertback(payload) {
  const { id, classId } = payload;
  delete payload.id;
  return request(`${API_HOST}/api/StudentReserve/InsertBack/${id}/${classId}`, {
    method: "POST",
    headers: {
      "Content-type": "application/json"
    },
    body: JSON.stringify(payload)
  });
}
