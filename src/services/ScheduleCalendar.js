import request from "../utils/request";
import { API_HOST } from "./serviceUri";
import { stringify } from "qs";

export async function listcenter(payload) {
  return request(`${API_HOST}/api/ScheduleCalendar/ListCenter`, {
    method: "GET"
  });
}
export async function listteacher(payload) {
  return request(`${API_HOST}/api/ScheduleCalendar/ListTeacher?${stringify(payload)}`, {
    method: "GET"
  });
}
export async function listclass(payload) {
  return request(`${API_HOST}/api/ScheduleCalendar/ListClass?${stringify(payload)}`, {
    method: "GET"
  });
}
export async function list(payload) {
  console.log(payload);
  return request(`${API_HOST}/api/ScheduleCalendar/List?${stringify(payload)}`, {
    method: "GET"
  });
}
export async function view(payload) {
  return request(`${API_HOST}/api/ScheduleCalendar/View/${payload.id}`, {
    method: "GET"
  });
}
