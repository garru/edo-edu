import request from "../utils/request";
import { API_HOST } from "./serviceUri";

export async function view(payload) {
  return request(`${API_HOST}/api/ScheduleAttendance/View/${payload.id}`, {
    method: "GET"
  });
}
export async function liststatus(payload) {
  return request(`${API_HOST}/api/ScheduleAttendance/ListStatus`, {
    method: "GET"
  });
}
export async function commentview(payload) {
  return request(
    `${API_HOST}/api/ScheduleAttendance/CommentView/${payload.id}/${payload.studentId}`,
    {
      method: "GET"
    }
  );
}

export async function commentdictionary(payload) {
  return request(
    `${API_HOST}/api/ScheduleAttendance/CommentDictionary/${payload.id}`,
    {
      method: "GET"
    }
  );
}

export async function updatestatus(payload) {
  return request(
    `${API_HOST}/api/ScheduleAttendance/UpdateStatus/${payload.id}`,
    {
      method: "POST",
      headers: {
        "Content-type": "application/json"
      },
      body: JSON.stringify(payload)
    }
  );
}

export async function commentupdate(payload) {
  return request(
    `${API_HOST}/api/ScheduleAttendance/CommentUpdate/${payload.id}`,
    {
      method: "POST",
      headers: {
        "Content-type": "application/json"
      },
      body: JSON.stringify(payload)
    }
  );
}

export async function finish(payload) {
  return request(`${API_HOST}/api/ScheduleAttendance/Finish/${payload.id}`, {
    method: "POST",
    headers: {
      "Content-type": "application/json"
    },
    body: JSON.stringify(payload)
  });
}
