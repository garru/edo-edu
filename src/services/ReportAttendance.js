import request from "../utils/request";
import { API_HOST } from "./serviceUri";
import { stringify } from "qs";
import moment from "moment";

export async function listcenter(payload) {
  return request(`${API_HOST}/api/ReportAttendance/ListCenter`, {
    method: "GET"
  });
}
export async function listclass(payload) {
  return request(
    `${API_HOST}/api/ReportAttendance/ListClass/${payload.centerId}`,
    {
      method: "GET"
    }
  );
}
export async function search(payload) {
  return request(
    `${API_HOST}/api/ReportAttendance/Search?${stringify(payload)}`,
    {
      method: "GET"
    }
  );
}
export async function download(payload) {
  return request(
    `${API_HOST}/api/ReportAttendance/Download?${stringify(payload)}`,
    {
      method: "GET",
      download: true,
      fileName: `tong_hop_diem_danh_${moment().format("DD_MM_YYY_hh_mm_ss")}`
    }
  );
}
