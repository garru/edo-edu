import request from '../utils/request';
import { API_HOST } from "./serviceUri";
import { stringify } from 'qs';

export async function search(payload) {
  return request(`${ API_HOST }/api/Teacher/Search?${stringify(payload)}` , {
    method: 'GET'
  });
}
export async function view(payload) {
  return request(`${ API_HOST }/api/Teacher/View/${payload.id}` , {
    method: 'GET'
  });
}

export async function viewprice(payload) {
  return request(`${ API_HOST }/api/Teacher/ViewPrice/${payload.id}` , {
    method: 'GET'
  });
}
export async function insertprice(payload) {
  return request(`${ API_HOST }/api/Teacher/InsertPrice/${payload.teacherId}` , {
    method: 'POST',
    body: {
      ...payload,
    },
  });
}
export async function updateprice(payload) {
  return request(`${ API_HOST }/api/Teacher/UpdatePrice/${payload.teacherId}/{id}?${stringify(payload)}` , {
    method: 'POST',
    body: {
      ...payload,
    },
  });
}
export async function deleteprice(payload) {
  return request(`${ API_HOST }/api/Teacher/DeletePrice/${payload.teacherId}/{id}?${stringify(payload)}` , {
    method: 'POST',
    body: {
      ...payload,
    },
  });
}
