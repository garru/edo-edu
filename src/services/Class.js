import request from "../utils/request";
import { API_HOST } from "./serviceUri";
import { stringify } from "qs";

export async function listcenter(payload) {
  return request(`${API_HOST}/api/Class/ListCenter`, {
    method: "GET"
  });
}
export async function listprogram(payload) {
  return request(`${API_HOST}/api/Class/ListProgram`, {
    method: "GET"
  });
}
export async function listmanage(payload) {
  return request(`${API_HOST}/api/Class/ListManage/${payload.centerId}`, {
    method: "GET"
  });
}
export async function listsurchage(payload) {
  console.log(payload);
  return request(`${API_HOST}/api/Class/ListSurchage/${payload.centerId}`, {
    method: "GET"
  });
}
export async function listroom(payload) {
  return request(`${API_HOST}/api/Class/ListRoom/${payload.centerId}`, {
    method: "GET"
  });
}
export async function listprice(payload) {
  return request(`${API_HOST}/api/Class/ListPrice/${payload.centerId}`, {
    method: "GET"
  });
}
export async function liststatus(payload) {
  return request(`${API_HOST}/api/Class/ListStatus`, {
    method: "GET"
  });
}
export async function search(payload) {
  return request(`${API_HOST}/api/Class/Search?${stringify(payload)}`, {
    method: "GET"
  });
}
export async function view(payload) {
  return request(`${API_HOST}/api/Class/View/${payload.id}`, {
    method: "GET"
  });
}
export async function insert(payload) {
  return request(`${API_HOST}/api/Class/Insert`, {
    method: "POST",
    headers: {
      "Content-type": "application/json"
    },
    body: JSON.stringify(payload)
  });
}
export async function update(payload) {
  return request(`${API_HOST}/api/Class/Update/${payload.id}`, {
    method: "POST",
    headers: {
      "Content-type": "application/json"
    },
    body: JSON.stringify(payload)
  });
}
export async function delete_class(payload) {
  return request(`${API_HOST}/api/Class/Delete/${payload.id}`, {
    method: "POST",
    headers: {
      "Content-type": "application/json"
    },
    body: JSON.stringify(payload)
  });
}
