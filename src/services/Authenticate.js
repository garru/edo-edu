import request from "../utils/request";
import { API_HOST } from "./serviceUri";
import { stringify } from "qs";

export async function login(payload) {
  return request(`${API_HOST}/api/Authenticate/Login`, {
    method: "POST",
    body: payload,
    headers: {
      "Content-type": "application/json"
    },
    noToken: true
  });
}
export async function refresh_token(payload) {
  const token = localStorage.getItem("token");
  return request(`${API_HOST}/api/Authenticate/Refresh-Token`, {
    method: "POST",
    headers: {
      "Content-type": "application/json"
    },
    body: {
      ...payload
    }
  });
}
