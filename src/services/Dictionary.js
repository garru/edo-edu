import request from "../utils/request";
import { API_HOST } from "./serviceUri";
import { stringify } from "qs";

export async function listdic(payload) {
  
  return request(`${API_HOST}/api/Dictionary/ListDic`, {
    method: "GET"
  });
}
export async function getdic(payload) {
  
  return request(`${API_HOST}/api/Dictionary/GetDic?${stringify(payload)}`, {
    method: "GET"
  });
}
