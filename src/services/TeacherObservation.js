import request from '../utils/request';
import { API_HOST } from "./serviceUri";
import { stringify } from 'qs';

export async function list(payload) {
  return request(`${ API_HOST }/api/TeacherObservation/List/${payload.id}` , {
    method: 'GET'
  });
}
export async function listcenter(payload) {
  return request(`${ API_HOST }/api/TeacherObservation/ListCenter?${stringify(payload)}` , {
    method: 'GET'
  });
}
export async function listclass(payload) {
  return request(`${ API_HOST }/api/TeacherObservation/ListClass/${payload.centerId}` , {
    method: 'GET'
  });
}
export async function listtype(payload) {
  return request(`${ API_HOST }/api/TeacherObservation/ListType` , {
    method: 'GET'
  });
}
export async function listcriteria(payload) {
  return request(`${ API_HOST }/api/TeacherObservation/ListCriteria/${payload.type}` , {
    method: 'GET'
  });
}
export async function viewedit(payload) {
  return request(`${ API_HOST }/api/TeacherObservation/ViewEdit/${payload.id}/{observationId}?${stringify(payload)}` , {
    method: 'GET'
  });
}
export async function insert(payload) {
  return request(`${ API_HOST }/api/TeacherObservation/Insert/${payload.id}` , {
    method: 'POST',
    body: {
      ...payload,
    },
  });
}
export async function update(payload) {
  return request(`${ API_HOST }/api/TeacherObservation/Update/${payload.id}/{observationId}?${stringify(payload)}` , {
    method: 'POST',
    body: {
      ...payload,
    },
  });
}
export async function delete_teacherobservation(payload) {
  return request(`${ API_HOST }/api/TeacherObservation/Delete/${payload.id}/{observationId}?${stringify(payload)}` , {
    method: 'POST',
    body: {
      ...payload,
    },
  });
}
export async function processview(payload) {
  return request(`${ API_HOST }/api/TeacherObservation/ProcessView/${payload.id}/{observationId}?${stringify(payload)}` , {
    method: 'GET'
  });
}
export async function processsummary(payload) {
  return request(`${ API_HOST }/api/TeacherObservation/ProcessSummary` , {
    method: 'POST',
    body: {
      ...payload,
    },
  });
}
export async function processupdate(payload) {
  return request(`${ API_HOST }/api/TeacherObservation/ProcessUpdate/${payload.id}/{observationId}?${stringify(payload)}` , {
    method: 'POST',
    body: {
      ...payload,
    },
  });
}
