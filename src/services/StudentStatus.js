import request from "../utils/request";
import { API_HOST } from "./serviceUri";
import { stringify } from "qs";

export async function applyfor(payload) {
  
  return request(`${API_HOST}/api/StudentStatus/ApplyFor`, {
    method: "GET"
  });
}
export async function list(payload) {
  
  return request(`${API_HOST}/api/StudentStatus/List`, {
    method: "GET"
  });
}
export async function view(payload) {
  
  return request(`${API_HOST}/api/StudentStatus/View/${payload.id}`, {
    method: "GET"
  });
}
export async function insert(payload) {
  
  return request(`${API_HOST}/api/StudentStatus/Insert`, {
    method: "POST",
    headers: {
      "Content-type": "application/json",
      
    },
    body: JSON.stringify(payload)
  });
}
export async function update(payload) {
  
  return request(`${API_HOST}/api/StudentStatus/Update/${payload.id}`, {
    method: "POST",
    headers: {
      "Content-type": "application/json",
      
    },
    body: JSON.stringify(payload)
  });
}
export async function delete_studentstatus(payload) {
  
  return request(`${API_HOST}/api/StudentStatus/Delete/${payload.id}`, {
    method: "POST",
    headers: {
      "Content-type": "application/json",
      
    },
    body: {
      ...payload
    }
  });
}
