import request from "../utils/request";
import { API_HOST } from "./serviceUri";
import { stringify } from "qs";

export async function listcenter(payload) {
  
  return request(`${API_HOST}/api/CoursePrice/ListCenter`, {
    method: "GET"
  });
}
export async function listprogram(payload) {
  
  return request(`${API_HOST}/api/CoursePrice/ListProgram`, {
    method: "GET"
  });
}
export async function listtype(payload) {
  
  return request(`${API_HOST}/api/CoursePrice/ListType`, {
    method: "GET"
  });
}
export async function search(payload) {
  
  return request(`${API_HOST}/api/CoursePrice/Search?${stringify(payload)}`, {
    method: "GET"
  });
}
export async function list(payload) {
  
  return request(`${API_HOST}/api/CoursePrice/List`, {
    method: "GET"
  });
}
export async function view(payload) {
  
  return request(`${API_HOST}/api/CoursePrice/View/${payload.id}`, {
    method: "GET"
  });
}
export async function insert(payload) {
  
  return request(`${API_HOST}/api/CoursePrice/Insert`, {
    method: "POST",
    headers: {
      "Content-type": "application/json",
      
    },
    body: JSON.stringify(payload)
  });
}
export async function update(payload) {
  
  return request(`${API_HOST}/api/CoursePrice/Update/${payload.id}`, {
    method: "POST",
    headers: {
      "Content-type": "application/json",
      
    },
    body: JSON.stringify(payload)
  });
}
export async function delete_courseprice(payload) {
  
  return request(`${API_HOST}/api/CoursePrice/Delete/${payload.id}`, {
    method: "POST",
    headers: {
      "Content-type": "application/json",
      
    },
  });
}
