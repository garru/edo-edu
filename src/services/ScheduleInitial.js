import request from '../utils/request';
import { API_HOST } from "./serviceUri";
import { stringify } from 'qs';

export async function listprogram(payload) {
  return request(`${ API_HOST }/api/ScheduleInitial/ListProgram` , {
    method: 'GET'
  });
}
export async function listroom(payload) {
  return request(`${ API_HOST }/api/ScheduleInitial/ListRoom/${payload.centerId}` , {
    method: 'GET'
  });
}
export async function listfinish(payload) {
  return request(`${ API_HOST }/api/ScheduleInitial/ListFinish` , {
    method: 'GET'
  });
}
export async function listrepeat(payload) {
  return request(`${ API_HOST }/api/ScheduleInitial/ListRepeat` , {
    method: 'GET'
  });
}
export async function listtime(payload) {
  return request(`${ API_HOST }/api/ScheduleInitial/ListTime/${payload.centerId}` , {
    method: 'GET'
  });
}
export async function listdomestic(payload) {
  return request(`${ API_HOST }/api/ScheduleInitial/ListDomestic/${payload.centerId}` , {
    method: 'GET'
  });
}
export async function listforeign(payload) {
  return request(`${ API_HOST }/api/ScheduleInitial/ListForeign/${payload.centerId}` , {
    method: 'GET'
  });
}
export async function view(payload) {
  return request(`${ API_HOST }/api/ScheduleInitial/View/${payload.id}` , {
    method: 'GET'
  });
}
export async function arrange(payload) {
  return request(`${ API_HOST }/api/ScheduleInitial/Arrange/${payload.id}` , {
    method: 'POST',
    body: {
      ...payload,
    },
  });
}
export async function lessoninsert(payload) {
  return request(`${ API_HOST }/api/ScheduleInitial/LessonInsert/${payload.id}` , {
    method: 'POST',
    body: {
      ...payload,
    },
  });
}
export async function lessonupdate(payload) {
  return request(`${ API_HOST }/api/ScheduleInitial/LessonUpdate/${payload.id}` , {
    method: 'POST',
    body: {
      ...payload,
    },
  });
}
export async function initial(payload) {
  return request(`${ API_HOST }/api/ScheduleInitial/Initial/${payload.id}` , {
    method: 'POST',
    body: {
      ...payload,
    },
  });
}
