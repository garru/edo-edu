import request from "../utils/request";
import { API_HOST } from "./serviceUri";
import { stringify } from "qs";

export async function search(payload) {
  return request(`${API_HOST}/api/Customer/Search?${stringify(payload)}`, {
    method: "GET"
  });
}
export async function listcenter(payload) {
  return request(`${API_HOST}/api/Customer/ListCenter`, {
    method: "GET"
  });
}
export async function listpresenter(payload) {
  return request(
    `${API_HOST}/api/Customer/ListPresenter?${stringify(payload)}`,
    {
      method: "GET"
    }
  );
}
export async function listevent(payload) {
  return request(`${API_HOST}/api/Customer/ListEvent`, {
    method: "GET"
  });
}
export async function listgender(payload) {
  return request(`${API_HOST}/api/Customer/ListGender`, {
    method: "GET"
  });
}
export async function listsource(payload) {
  return request(`${API_HOST}/api/Customer/ListSource`, {
    method: "GET"
  });
}
export async function listprovince(payload) {
  return request(`${API_HOST}/api/Customer/ListProvince`, {
    method: "GET"
  });
}
export async function listdistrict(payload) {
  return request(
    `${API_HOST}/api/Customer/ListDistrict?${stringify(payload)}`,
    {
      method: "GET"
    }
  );
}
export async function listquanhe(payload) {
  return request(
    `${API_HOST}/api/Customer/ListClassify?${stringify(payload)}`,
    {
      method: "GET"
    }
  );
}
export async function listtrangthai(payload) {
  return request(`${API_HOST}/api/Customer/ListStatus?${stringify(payload)}`, {
    method: "GET"
  });
}
export async function insertcustomer(payload) {
  return request(`${API_HOST}/api/Customer/InsertCustomer`, {
    method: "POST",
    headers: {
      "Content-type": "application/json"
    },
    body: JSON.stringify(payload)
  });
}
export async function insertparent(payload) {
  return request(`${API_HOST}/api/Customer/InsertParent`, {
    method: "POST",
    headers: {
      "Content-type": "application/json"
    },
    body: JSON.stringify(payload)
  });
}
export async function insertstudent(payload) {
  return request(`${API_HOST}/api/Customer/InsertStudent`, {
    method: "POST",
    headers: {
      "Content-type": "application/json"
    },
    body: JSON.stringify(payload)
  });
}
export async function updateclassify(payload) {
  return request(
    `${API_HOST}/api/Customer/UpdateClassify/${payload.id}/${payload.classify}`,
    {
      method: "POST",
      headers: {
        "Content-type": "application/json"
      },
      body: {
        ...payload
      }
    }
  );
}
export async function updatestatus(payload) {
  return request(
    `${API_HOST}/api/Customer/UpdateStatus/${payload.id}/${payload.status}`,
    {
      method: "POST",
      headers: {
        "Content-type": "application/json"
      },
      body: {
        ...payload
      }
    }
  );
}

export async function searchparent(payload) {
  return request(`${API_HOST}/api/Customer/GetParent?${stringify(payload)}`, {
    method: "GET"
  });
}
