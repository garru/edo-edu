import request from '../utils/request';
import { API_HOST } from "./serviceUri";
import { stringify } from 'qs';

export async function view(payload) {
  return request(`${ API_HOST }/api/StudentAbsence/View/${payload.id}/{scheduleId}?${stringify(payload)}` , {
    method: 'GET'
  });
}
export async function listprogram(payload) {
  return request(`${ API_HOST }/api/StudentAbsence/ListProgram?${stringify(payload)}` , {
    method: 'GET'
  });
}
export async function listclass(payload) {
  return request(`${ API_HOST }/api/StudentAbsence/ListClass/${payload.centerId}/{programId}?${stringify(payload)}` , {
    method: 'GET'
  });
}
export async function listschedule(payload) {
  return request(`${ API_HOST }/api/StudentAbsence/ListSchedule/${payload.classId}` , {
    method: 'GET'
  });
}
export async function insert(payload) {
  return request(`${ API_HOST }/api/StudentAbsence/Insert/${payload.id}/{scheduleId}?${stringify(payload)}` , {
    method: 'POST',
    body: {
      ...payload,
    },
  });
}
