import request from "../utils/request";
import { API_HOST } from "./serviceUri";
import { stringify } from "qs";

export async function search(payload) {
  
  return request(`${API_HOST}/api/Program/Search?${stringify(payload)}`, {
    method: "GET"
  });
}
export async function upload(payload) {
  
  return request(`${API_HOST}/api/Program/Upload`, {
    method: "POST",
    headers: {
      "Content-type": "application/json",
    },
    body: {
      ...payload
    }
  });
}
export async function template(payload) {
  
  return request(`${API_HOST}/api/Program/Template`, {
    method: "GET"
  });
}
export async function view(payload) {
  
  return request(`${API_HOST}/api/Program/View/${payload.id}`, {
    method: "GET"
  });
}
export async function insert(payload) {
  
  return request(`${API_HOST}/api/Program/Insert`, {
    method: "POST",
    headers: {
      "Content-type": "application/json",
    },
    body: JSON.stringify(payload)
  });
}
export async function update(payload) {
  
  return request(`${API_HOST}/api/Program/Update/${payload.id}`, {
    method: "POST",
    headers: {
      "Content-type": "application/json",
      
    },
    body: {
      ...payload
    }
  });
}
export async function delete_program(payload) {
  
  return request(`${API_HOST}/api/Program/Delete/${payload.id}`, {
    method: "POST",
    headers: {
      "Content-type": "application/json",
      
    },
    body: {
      ...payload
    }
  });
}
