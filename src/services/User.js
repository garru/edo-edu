import request from "../utils/request";
import { API_HOST } from "./serviceUri";
import { stringify } from "qs";

export async function view(payload) {
  
  return request(`${API_HOST}/api/User/View`, {
    method: "GET"
  });
}
export async function updateinfo(payload) {
  
  return request(`${API_HOST}/api/User/UpdateInfo`, {
    method: "POST",
    body: payload,
    headers: {
      "Content-type": "application/json",
      
    }
  });
}
export async function updateavatar(payload) {
  
  return request(`${API_HOST}/api/User/UpdateAvatar`, {
    method: "POST",
    body: payload,
    headers: {
      "Content-type": "application/json",
      
    }
  });
}
