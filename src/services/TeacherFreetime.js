import request from "../utils/request";
import { API_HOST } from "./serviceUri";
import { stringify } from "qs";

export async function search(payload) {
  
  return request(
    `${API_HOST}/api/TeacherFreetime/Search?${stringify(payload)}`,
    {
      method: "GET"
    }
  );
}
export async function listitem(payload) {
  
  return request(
    `${API_HOST}/api/TeacherFreetime/ListItem/${payload.freeMonth}`,
    {
      method: "GET"
    }
  );
}
export async function view(payload) {
  
  return request(`${API_HOST}/api/TeacherFreetime/View/${payload.id}`, {
    method: "GET"
  });
}
export async function insert(payload) {
  
  return request(`${API_HOST}/api/TeacherFreetime/Insert`, {
    method: "POST",
    headers: {
      "Content-type": "application/json",
      
    },
    body: {
      ...payload
    }
  });
}
export async function update(payload) {
  
  return request(`${API_HOST}/api/TeacherFreetime/Update/${payload.id}`, {
    method: "POST",
    headers: {
      "Content-type": "application/json",
      
    },
    body: {
      ...payload
    }
  });
}
