import request from "../utils/request";
import { API_HOST } from "./serviceUri";

export async function view(payload) {
  return request(`${API_HOST}/api/ClassOffset/View/${payload.id}`, {
    method: "GET"
  });
}

export async function listprogram(payload) {
  return request(`${API_HOST}/api/ClassOffset/ListProgram`, {
    method: "GET"
  });
}
export async function listclass(payload) {
  return request(
    `${API_HOST}/api/ClassOffset/ListClass/${payload.centerId}/${payload.programId}`,
    {
      method: "GET"
    }
  );
}
export async function offset(payload) {
  return request(`${API_HOST}/api/ClassOffset/Offset`, {
    method: "POST",
    headers: {
      "Content-type": "application/json"
    },
    body: JSON.stringify(payload)
  });
}
