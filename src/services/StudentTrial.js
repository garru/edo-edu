import request from "../utils/request";
import { API_HOST } from "./serviceUri";
import { stringify } from "qs";

export async function listcenter(payload) {
  return request(`${API_HOST}/api/StudentTrial/ListCenter`, {
    method: "GET"
  });
}
export async function listprogram(payload) {
  return request(`${API_HOST}/api/StudentTrial/ListProgram`, {
    method: "GET"
  });
}
export async function listclass(payload) {
  return request(
    `${API_HOST}/api/StudentTrial/ListClass?${stringify(payload)}`,
    {
      method: "GET"
    }
  );
}
export async function listschedule(payload) {
  return request(
    `${API_HOST}/api/StudentTrial/ListSchedule?${stringify(payload)}`,
    {
      method: "GET"
    }
  );
}
export async function view(payload) {
  return request(`${API_HOST}/api/StudentTrial/View?${stringify(payload)}`, {
    method: "GET"
  });
}
export async function insert(payload) {
  return request(`${API_HOST}/api/StudentTrial/Insert`, {
    method: "POST",
    headers: {
      "Content-type": "application/json"
    },
    body: JSON.stringify(payload)
  });
}
export async function append(payload) {
  return request(`${ API_HOST }/api/StudentTrial/Append/${payload.id}` , {
    method: 'POST',
    body: {
      ...payload,
    },
  });
}
