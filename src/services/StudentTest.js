import request from "../utils/request";
import { API_HOST } from "./serviceUri";
import { stringify } from "qs";

export async function listcenter(payload) {
  return request(`${API_HOST}/api/StudentTest/ListCenter`, {
    method: "GET"
  });
}
export async function listteacher(payload) {
  return request(`${API_HOST}/api/StudentTest/ListTeacher/${payload.id}`, {
    method: "GET"
  });
}
export async function view(payload) {
  return request(`${API_HOST}/api/StudentTest/View?${stringify(payload)}`, {
    method: "GET"
  });
}
export async function insert(payload) {
  return request(`${API_HOST}/api/StudentTest/Insert`, {
    method: "POST",
    headers: {
      "Content-type": "application/json"
    },
    body: JSON.stringify(payload)
  });
}

export async function append(payload) {
  return request(`${ API_HOST }/api/StudentTest/Append/${payload.id}` , {
    method: 'POST',
    body: {
      ...payload,
    },
  });
}