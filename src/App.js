import React from "react";
import "./App.css";
import { withRouter } from "react-router-dom";
import Home from "./pages/home";
import Login from "./pages/auth/login";
import "react-big-calendar/lib/css/react-big-calendar.css";
import "react-toastify/dist/ReactToastify.css";
import Loading from "./components/Loading";
import Error from "./components/Error";
import Success from "./components/Success";
import { connect } from "dva";

class App extends React.Component {
  render() {
    const token = window.localStorage.getItem("token");
    return (
      <div>
        {token && token !== "undefined" ? <Home /> : <Login />}
        {this.props.Global.loading && <Loading></Loading>}
        {this.props.Global.error && <Error></Error>}
        {this.props.Global.success && <Success></Success>}
      </div>
    );
  }
}

export default withRouter(
  connect(({ Global }) => ({
    Global
  }))(App)
);
