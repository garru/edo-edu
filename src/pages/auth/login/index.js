import React, { useState } from "react";
import { useForm } from "react-hook-form";
import "./index.css";
import {
  GoogleReCaptchaProvider,
  GoogleReCaptcha
} from "react-google-recaptcha-v3";
import { GoogleLogin } from "react-google-login";
import { connect } from "dva";
class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  responseSuccessGoogle = data => {
    const { dispatch } = this.props;
    console.log(data);
    dispatch({
      type: "Global/showLoading"
    });
    dispatch({
      type: "Authenticate/login",
      payload: {
        idToken: data.getAuthResponse().id_token
      }
    }).then(data => {
      console.log(data);
      if (data?.accessToken) {
        localStorage.setItem("token", data.accessToken);
        localStorage.setItem("user_infor", JSON.stringify(data));
        dispatch({
          type: "Global/hideLoading"
        });
        setTimeout(() => {
          window.location.reload();
        }, 200);
      } else {
        dispatch({
          type: "Global/hideLoading"
        });
        dispatch({
          type: "Global/showError"
        });
      }
    });
  };

  responseFailedGoogle = data => {
    const { dispatch } = this.props;
    dispatch({
      type: "Global/showError"
    });
  };

  render() {
    return (
      <section className="login">
        {/* <GoogleReCaptchaProvider reCaptchaKey="6LdE2H0aAAAAAOyoy4Fn2pXVMH6DZO9yus-XcIlt">
        <GoogleReCaptcha onVerify={console.log("verifying")} />
      </GoogleReCaptchaProvider> */}
        <h1>Đăng nhập</h1>
        <div style={{ textAlign: "center" }}>
          <div className="btn-google">
            <i className="fab fa-google"></i>
            <span>Sign in with Google</span>
            <GoogleLogin
              clientId="639972451931-9h89l81onk5vqravd456p0a4p9n6sso1.apps.googleusercontent.com"
              buttonText="Sign in with Google"
              onSuccess={this.responseSuccessGoogle}
              onFailure={this.responseFailedGoogle}
              cookiePolicy={"single_host_origin"}
              style={{ background: "#4688f1", color: "white" }}
              className="btn-google--overlay"
            />
          </div>
        </div>

        {/* <form onSubmit={handleSubmit(onSubmit)}>
        <div className="row">
          <div className="col-lg-3">
            <label class="" htmlFor="username">
              Tên đăng nhập
            </label>
          </div>
          <div className="col-lg-9">
            <input
              type="text"
              name="username"
              className="form-control"
              id="username"
              ref={register({ required: true })}
            ></input>
            {errors.exampleRequired && (
              <span className="error-message">This field is required</span>
            )}
          </div>
        </div>
        <div className="row">
          <div className="col-lg-3">
            <label class="" htmlFor="password">
              Mật khẩu
            </label>
          </div>
          <div className="col-lg-9">
            <input
              type="password"
              name="password"
              className="form-control"
              id="password"
              ref={register({ required: true })}
            ></input>
            {errors.exampleRequired && (
              <span className="error-message">This field is required</span>
            )}
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6">
            <div className="form-group">
              <input type="checkbox" name="yeu_cau_mk" id="yeu_cau_mk" />
              <label htmlFor="yeu_cau_mk">&nbsp;&nbsp; Ghi nhớ mật khẩu</label>
            </div>
          </div>
          <div className="col-lg-6" style={{ textAlign: "right" }}>
            <div className="form-group">
              <a href="#" onClick={() => setShow(true)}>
                Quên mật khẩu?
              </a>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-12" style={{ textAlign: "center" }}>
            <div className="form-group">
              <button type="submit" class="btn btn-primary mb-3">
                Đăng nhập
              </button>
            </div>
          </div>
        </div>
      </form>
      {show && <ForgotPassword onClose={() => setShow(false)}></ForgotPassword>} */}
      </section>
    );
  }
}

// Login.propTypes = {
//   setToken: PropTypes.func.isRequired
// };

export default connect(({ Authenticate, Global }) => ({
  Authenticate,
  Global
}))(Login);
