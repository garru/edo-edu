import React, { Component, useState, useRef, useEffect } from "react";
import { useForm } from "react-hook-form";
import { getErrorMessage } from "../../../services/FormValidation";
import { Modal, Button } from "react-bootstrap";

export default function ForgotPassword(props) {
  const [show, setShow] = useState(true);
  const handleClose = () => {
    // setShow(false);
    props.onClose();
  };
  const { register, handleSubmit, errors } = useForm();
  const [showModal, setShowModal] = useState(true);
  const [count, setCount] = useState(60);
  const emailReg = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  const [step, setStep] = useState("email");
  const timer = () => setCount(count - 1);
  useEffect(() => {
    if (count <= 0) {
      return;
    }
    const id = step === "otp" ? setInterval(timer, 1000) : null;
    return () => clearInterval(id);
  }, [count, step]);

  const onSubmitEmail = data => {
    console.log("email: ", data);
    // setCount(60);
    setStep("otp");
  };

  const onSubmitOtp = data => {
    console.log("otp: ", data);
    setStep("pw");
  };

  const onSubmitPw = data => {
    console.log("pw: ", data);
    handleClose();
  };

  function getTitle(step) {
    switch (step) {
      case "otp":
        return "Xác thực";
      case "pw":
        return "Thay đổi mật khẩu";
      default:
        return "Lấy lại mật khẩu";
    }
  }

  return (
    <Modal
      size={"lg"}
      show={show}
      onHide={handleClose}
      backdrop="static"
      keyboard={false}
      id="ForgotPassword"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title>{getTitle(step)}</Modal.Title>
      </Modal.Header>
      <Modal.Body style={{ padding: "40px 50px 10px" }}>
        {step === "email" && (
          <form onSubmit={handleSubmit(onSubmitEmail)}>
            <section>
              <div className="row">
                <div className="col-lg-12" style={{ marginBottom: 20 }}>
                  <p>Vui lòng cung cấp email đăng ký để lấy lại mật khẩu</p>
                </div>
                <div className="col-lg-2">Email</div>
                <div className="col-lg-10">
                  <input
                    type="text"
                    name="email"
                    className="form-control form-control-lg"
                    id="email"
                    placeholder="Nhập email"
                    ref={register({ required: true, pattern: emailReg })}
                  ></input>
                  <span className="error-message">
                    {getErrorMessage(errors, "email")}
                  </span>
                </div>
              </div>
            </section>
          </form>
        )}
        {step === "otp" && (
          <form onSubmit={handleSubmit(onSubmitOtp)}>
            <section>
              <div className="row">
                <div className="col-lg-12" style={{ marginBottom: 20 }}>
                  <p>
                    Mã OTP đã được gửi về email của bạn vui lòng nhập OTP để xác
                    thực tài khoản
                  </p>
                </div>
                <div className="col-lg-2">Mã OTP</div>
                <div className="col-lg-10">
                  <input
                    type="text"
                    name="otp"
                    className="form-control form-control-lg"
                    id="otp"
                    placeholder="Nhập mã OTP"
                    ref={register({ required: true, valueAsNumber: true })}
                  ></input>
                  <span className="error-message">
                    {getErrorMessage(errors, "otp")}
                  </span>
                </div>
                <div className="col-lg-12">
                  <p>
                    Bạn chưa nhận được mã? Gởi lại sau{" "}
                    <span style={{ color: "#007bff" }}>{count}s</span>
                  </p>
                </div>
              </div>
            </section>
          </form>
        )}
        {step === "pw" && (
          <form onSubmit={handleSubmit(onSubmitPw)}>
            <section>
              <div className="row">
                <div className="col-lg-4">Mật khẩu mới</div>
                <div className="col-lg-8">
                  <input
                    type="password"
                    name="password"
                    className="form-control form-control-lg"
                    id="password"
                    placeholder="Nhập mật khẩu mới"
                    ref={register({ required: true })}
                  ></input>
                  <span className="error-message">
                    {getErrorMessage(errors, "password")}
                  </span>
                </div>
              </div>
              <div className="row">
                <div className="col-lg-4">Nhập lại mật khẩu mới</div>
                <div className="col-lg-8">
                  <input
                    type="password"
                    name="confirmPassword"
                    className="form-control form-control-lg"
                    id="confirmPassword"
                    placeholder="Nhập lại mật khẩu mới"
                    ref={register({ required: true })}
                  ></input>
                  <span className="error-message">
                    {getErrorMessage(errors, "confirmPassword")}
                  </span>
                </div>
              </div>
            </section>
          </form>
        )}
      </Modal.Body>
      <Modal.Footer>
        {step === "email" && (
          <React.Fragment>
            <Button variant="primary" onClick={handleSubmit(onSubmitEmail)} style={{marginRight: 15}}>
              Xác nhận
            </Button>
            <Button variant="secondary" onClick={handleClose}>
              Đóng
            </Button>
          </React.Fragment>
        )}
        {step === "otp" && (
          <React.Fragment>
            <Button variant="primary" onClick={handleSubmit(onSubmitOtp)}>
              Xác nhận
            </Button>
            <Button
              variant={count ? "secondary" : "primary"}
              onClick={() => setCount(60)}
              disabled={!!count}
            >
              Gởi lại
            </Button>
          </React.Fragment>
        )}
        {step === "pw" && (
          <Button variant="primary" onClick={handleSubmit(onSubmitPw)}>
            Xác nhận
          </Button>
        )}
      </Modal.Footer>
    </Modal>
  );
}
