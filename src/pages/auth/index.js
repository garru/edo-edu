import React from 'react';
import './index.css';
import dva, {connect} from 'dva';
import {
    BrowserRouter,
    Router,
    Switch,
    HashRouter,
    Route,
    Link, withRouter, Redirect
} from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import Login from './login'
import BasicLayout from "../../layouts/BasicLayout";


function Page() {
    return (
        <BasicLayout>
            <Switch>
                <Redirect from="/*" to="/" />
                <Redirect push to="/login" component={Login}/>
                <Route path="/login" component={Login}/>
            </Switch>
        </BasicLayout>
    );
}

export default Page
