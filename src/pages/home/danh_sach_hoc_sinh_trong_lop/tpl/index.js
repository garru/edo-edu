import React from 'react';
import './index.css';
import './ax.css';

class Page extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
		<div class="" id="base" style={{"margin-left":"-150px"}}>
		   <div class="ax_default shape" id="u10290">
		      <div class="" id="u10290_div">
		      </div>
		      <div class="text" id="u10290_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default shape" id="u10291">
		      <div class="" id="u10291_div">
		      </div>
		      <div class="text" id="u10291_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default" data-height="40" data-label="account" data-left="13" data-top="895" data-width="192" id="u10293">
		      <div class="ax_default shape" data-label="accountLable" id="u10294">
		         <img class="img" id="u10294_img" src="images/account_information/accountlable_u55.svg"/>
		         <div class="text" id="u10294_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Nguyen Van A
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u10295">
		         <img class="img" id="u10295_img" src="images/login/u4.svg"/>
		         <div class="text" id="u10295_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="settingMenu" data-left="11" data-top="826" data-width="125" id="u10296">
		      <div class="ax_default shape" data-label="gearIconLable" id="u10297">
		         <img class="img" id="u10297_img" src="images/account_information/geariconlable_u58.svg"/>
		         <div class="text" id="u10297_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Cài đặt
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="44" data-left="11" data-top="826" data-width="44" id="u10298">
		         <div class="ax_default shape" data-label="gearIconBG" id="u10299">
		            <div class="" id="u10299_div">
		            </div>
		            <div class="text" id="u10299_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default shape" data-label="gearIconBG2" id="u10300">
		            <div class="" id="u10300_div">
		            </div>
		            <div class="text" id="u10300_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" data-label="gearIcon" id="u10301">
		            <img class="img" id="u10301_img" src="images/account_information/gearicon_u62.svg"/>
		            <div class="text" id="u10301_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="customerMenu" data-left="11" data-top="145" data-width="143" id="u10302">
		      <div class="ax_default shape" data-label="customerIconLable" id="u10303">
		         <img class="img" id="u10303_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u10303_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Khách hàng
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="customerIconBG" id="u10304">
		         <div class="" id="u10304_div">
		         </div>
		         <div class="text" id="u10304_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="customerIconBG2" id="u10305">
		         <div class="" id="u10305_div">
		         </div>
		         <div class="text" id="u10305_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" data-label="customer" id="u10306">
		         <img class="img" id="u10306_img" src="images/account_information/customer_u67.svg"/>
		         <div class="text" id="u10306_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="classMenu" data-left="11" data-top="198" data-width="143" id="u10307">
		      <div class="ax_default shape" data-label="classIconLable" id="u10308">
		         <img class="img" id="u10308_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u10308_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Lớp học
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG" id="u10309">
		         <div class="" id="u10309_div">
		         </div>
		         <div class="text" id="u10309_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG2" id="u10310">
		         <div class="" id="u10310_div">
		         </div>
		         <div class="text" id="u10310_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" data-label="classicon" id="u10311">
		         <img class="img" id="u10311_img" src="images/account_information/classicon_u72.svg"/>
		         <div class="text" id="u10311_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="dashboardMenu" data-left="11" data-top="91" data-width="143" id="u10312">
		      <div class="ax_default shape" data-label="dashboardIconLable" id="u10313">
		         <img class="img" id="u10313_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u10313_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Dashnoard
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="dashboardIconBG" id="u10314">
		         <div class="" id="u10314_div">
		         </div>
		         <div class="text" id="u10314_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="dashboardIconBG2" id="u10315">
		         <div class="" id="u10315_div">
		         </div>
		         <div class="text" id="u10315_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" data-label="dashboard" id="u10316">
		         <img class="img" id="u10316_img" src="images/account_information/dashboard_u77.svg"/>
		         <div class="text" id="u10316_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="129" data-label="AccountMenu" data-left="58" data-top="806" data-width="246" id="u10317" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default shape" id="u10318">
		         <div class="" id="u10318_div">
		         </div>
		         <div class="text" id="u10318_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u10319">
		         <div class="" id="u10319_div">
		         </div>
		         <div class="text" id="u10319_text">
		            <p>
		               <span style={{"font-family":"ArialMT, Arial","font-weight":"400","text-decoration":"none"}}>
		                  Xin chào:
		               </span>
		               <span style={{"font-family":"Arial Bold, Arial","font-weight":"700","text-decoration":"none"}}>
		                  Nguyen Van A
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u10320">
		         <div class="ax_default image" id="u10321">
		            <img class="img" id="u10321_img" src="images/account_information/u82.png"/>
		            <div class="text" id="u10321_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u10322">
		            <div class="" id="u10322_div">
		            </div>
		            <div class="text" id="u10322_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Đăng xuất
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default line" id="u10323">
		         <img class="img" id="u10323_img" src="images/account_information/u84.svg"/>
		         <div class="text" id="u10323_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u10324">
		         <div class="ax_default paragraph" id="u10325">
		            <div class="" id="u10325_div">
		            </div>
		            <div class="text" id="u10325_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tài khoản
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default image" id="u10326">
		            <img class="img" id="u10326_img" src="images/account_information/u87.png"/>
		            <div class="text" id="u10326_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="339" data-label="SettingPannel" data-left="58" data-top="525" data-width="339" id="u10327" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default shape" id="u10328">
		         <div class="" id="u10328_div">
		         </div>
		         <div class="text" id="u10328_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u10329">
		         <div class="" id="u10329_div">
		         </div>
		         <div class="text" id="u10329_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Hệ thống
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u10330">
		         <div class="ax_default icon" id="u10331">
		            <img class="img" id="u10331_img" src="images/account_information/u92.svg"/>
		            <div class="text" id="u10331_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u10332">
		            <div class="" id="u10332_div">
		            </div>
		            <div class="text" id="u10332_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tài khoản
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u10333">
		         <div class="ax_default paragraph" id="u10334">
		            <div class="" id="u10334_div">
		            </div>
		            <div class="text" id="u10334_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phân quyền
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u10335">
		            <img class="img" id="u10335_img" src="images/account_information/u96.svg"/>
		            <div class="text" id="u10335_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u10336">
		         <div class="" id="u10336_div">
		         </div>
		         <div class="text" id="u10336_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Đào tạo
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u10337">
		         <div class="ax_default paragraph" id="u10338">
		            <div class="" id="u10338_div">
		            </div>
		            <div class="text" id="u10338_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Thương hiệu
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u10339">
		            <img class="img" id="u10339_img" src="images/account_information/u100.svg"/>
		            <div class="text" id="u10339_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u10340">
		         <div class="ax_default paragraph" id="u10341">
		            <div class="" id="u10341_div">
		            </div>
		            <div class="text" id="u10341_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trung tâm
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u10342">
		            <img class="img" id="u10342_img" src="images/account_information/u103.svg"/>
		            <div class="text" id="u10342_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u10343">
		         <div class="ax_default icon" id="u10344">
		            <img class="img" id="u10344_img" src="images/account_information/u105.svg"/>
		            <div class="text" id="u10344_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u10345">
		            <div class="" id="u10345_div">
		            </div>
		            <div class="text" id="u10345_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Ngày nghỉ
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u10346">
		         <div class="ax_default icon" id="u10347">
		            <img class="img" id="u10347_img" src="images/account_information/u108.svg"/>
		            <div class="text" id="u10347_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u10348">
		            <div class="" id="u10348_div">
		            </div>
		            <div class="text" id="u10348_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phòng ban - Vai trò
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u10349">
		         <div class="ax_default paragraph" id="u10350">
		            <div class="" id="u10350_div">
		            </div>
		            <div class="text" id="u10350_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trạng thái học viên
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u10351">
		            <img class="img" id="u10351_img" src="images/account_information/u112.svg"/>
		            <div class="text" id="u10351_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u10352">
		         <div class="ax_default paragraph" id="u10353">
		            <div class="" id="u10353_div">
		            </div>
		            <div class="text" id="u10353_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tiêu chí đánh giá
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u10354">
		            <img class="img" id="u10354_img" src="images/account_information/u115.svg"/>
		            <div class="text" id="u10354_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u10355">
		         <div class="ax_default paragraph" id="u10356">
		            <div class="" id="u10356_div">
		            </div>
		            <div class="text" id="u10356_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Chương trình học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u10357">
		            <div class="ax_default icon" id="u10358">
		               <img class="img" id="u10358_img" src="images/account_information/u119.svg"/>
		               <div class="text" id="u10358_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u10359">
		         <div class="ax_default icon" id="u10360">
		            <img class="img" id="u10360_img" src="images/account_information/u121.svg"/>
		            <div class="text" id="u10360_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u10361">
		            <div class="" id="u10361_div">
		            </div>
		            <div class="text" id="u10361_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Bài học
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u10362">
		         <div class="ax_default paragraph" id="u10363">
		            <div class="" id="u10363_div">
		            </div>
		            <div class="text" id="u10363_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phòng học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u10364">
		            <img class="img" id="u10364_img" src="images/account_information/u125.svg"/>
		            <div class="text" id="u10364_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u10365">
		         <div class="ax_default paragraph" id="u10366">
		            <div class="" id="u10366_div">
		            </div>
		            <div class="text" id="u10366_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trường học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u10367">
		            <img class="img" id="u10367_img" src="images/account_information/u128.svg"/>
		            <div class="text" id="u10367_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default line" id="u10368">
		         <img class="img" id="u10368_img" src="images/account_information/u129.svg"/>
		         <div class="text" id="u10368_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u10369">
		         <div class="" id="u10369_div">
		         </div>
		         <div class="text" id="u10369_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Tài chính
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u10370">
		         <div class="ax_default paragraph" id="u10371">
		            <div class="" id="u10371_div">
		            </div>
		            <div class="text" id="u10371_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Học phí
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u10372">
		            <img class="img" id="u10372_img" src="images/account_information/u133.svg"/>
		            <div class="text" id="u10372_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u10373">
		         <div class="ax_default paragraph" id="u10374">
		            <div class="" id="u10374_div">
		            </div>
		            <div class="text" id="u10374_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Khuyến mại
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u10375">
		            <img class="img" id="u10375_img" src="images/account_information/u136.svg"/>
		            <div class="text" id="u10375_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="classMenu" data-left="11" data-top="252" data-width="143" id="u10376">
		      <div class="ax_default shape" data-label="classIconLable" id="u10377">
		         <img class="img" id="u10377_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u10377_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Giáo viên
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG" id="u10378">
		         <div class="" id="u10378_div">
		         </div>
		         <div class="text" id="u10378_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG2" id="u10379">
		         <div class="" id="u10379_div">
		         </div>
		         <div class="text" id="u10379_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u10380">
		         <img class="img" id="u10380_img" src="images/account_information/u141.svg"/>
		         <div class="text" id="u10380_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u10381">
		      <img class="img" id="u10381_img" src="images/account_information/u142.svg"/>
		      <div class="text" id="u10381_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u10382">
		      <img class="img" id="u10382_img" src="images/account_information/u143.svg"/>
		      <div class="text" id="u10382_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div id="u10289" style={{"display":"none","visibility":"hidden"}}>
		   </div>
		   <div class="ax_default" data-height="32" data-label="Header" data-left="133" data-top="17" data-width="170" id="u10383">
		      <div class="ax_default paragraph" id="u10384">
		         <div class="" id="u10384_div">
		         </div>
		         <div class="text" id="u10384_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Tạo hóa đơn
		               </span>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default shape" id="u10385">
		      <div class="" id="u10385_div">
		      </div>
		      <div class="text" id="u10385_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default label" id="u10386">
		      <div class="" id="u10386_div">
		      </div>
		      <div class="text" id="u10386_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Trung tâm
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default droplist" id="u10387">
		      <div class="" id="u10387_div">
		      </div>
		      <select class="u10387_input" id="u10387_input">
		         <option class="u10387_input_option" selected="" value="Hoàng Ngân">
		            Hoàng Ngân
		         </option>
		         <option class="u10387_input_option" value="Mai Hắc Đế">
		            Mai Hắc Đế
		         </option>
		      </select>
		   </div>
		   <div class="ax_default label" id="u10388">
		      <div class="" id="u10388_div">
		      </div>
		      <div class="text" id="u10388_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Loại hóa đơn
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default droplist" id="u10389">
		      <div class="" id="u10389_div">
		      </div>
		      <select class="u10389_input" id="u10389_input">
		         <option class="u10389_input_option" selected="" value="Phiếu thu">
		            Phiếu thu
		         </option>
		         <option class="u10389_input_option" value="Phiếu chi">
		            Phiếu chi
		         </option>
		      </select>
		   </div>
		   <div class="ax_default label" id="u10390">
		      <div class="" id="u10390_div">
		      </div>
		      <div class="text" id="u10390_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Mục đích
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default droplist" id="u10391">
		      <div class="" id="u10391_div">
		      </div>
		      <select class="u10391_input" id="u10391_input">
		         <option class="u10391_input_option" value="Đặt cọc">
		            Đặt cọc
		         </option>
		         <option class="u10391_input_option" selected="" value="Học phí">
		            Học phí
		         </option>
		         <option class="u10391_input_option" value="Phụ thu">
		            Phụ thu
		         </option>
		      </select>
		   </div>
		   <div class="ax_default label" id="u10392">
		      <div class="" id="u10392_div">
		      </div>
		      <div class="text" id="u10392_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Mã hóa đơn liên quan
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default text_field" id="u10393">
		      <div class="" id="u10393_div">
		      </div>
		      <input class="u10393_input" id="u10393_input" type="text" value=""/>
		   </div>
		   <div class="ax_default label" id="u10394">
		      <div class="" id="u10394_div">
		      </div>
		      <div class="text" id="u10394_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Sử dụng khi có đặt cọc
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default label" id="u10395">
		      <div class="" id="u10395_div">
		      </div>
		      <div class="text" id="u10395_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Người nộp
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default text_field" id="u10396">
		      <div class="" id="u10396_div">
		      </div>
		      <input class="u10396_input" id="u10396_input" type="text" value=""/>
		   </div>
		   <div class="ax_default label" id="u10397">
		      <div class="" id="u10397_div">
		      </div>
		      <div class="text" id="u10397_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Tài khoản thu
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default droplist" id="u10398">
		      <div class="" id="u10398_div">
		      </div>
		      <select class="u10398_input" id="u10398_input">
		         <option class="u10398_input_option" selected="" value="Tiền mặt">
		            Tiền mặt
		         </option>
		         <option class="u10398_input_option" value="Vietcombank - Chuyển Khoản">
		            Vietcombank - Chuyển Khoản
		         </option>
		         <option class="u10398_input_option" value="Techcombank - Chuyển Khoản">
		            Techcombank - Chuyển Khoản
		         </option>
		         <option class="u10398_input_option" value="Vietcombank - Trả góp">
		            Vietcombank - Trả góp
		         </option>
		         <option class="u10398_input_option" value="Techcombank - Trả góp">
		            Techcombank - Trả góp
		         </option>
		      </select>
		   </div>
		   <div class="ax_default label" id="u10399">
		      <div class="" id="u10399_div">
		      </div>
		      <div class="text" id="u10399_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Nội dung
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default label" id="u10400">
		      <div class="" id="u10400_div">
		      </div>
		      <div class="text" id="u10400_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Gói học phí
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default droplist" id="u10401">
		      <div class="" id="u10401_div">
		      </div>
		      <select class="u10401_input" id="u10401_input">
		         <option class="u10401_input_option" value="BIBOP 1">
		            BIBOP 1
		         </option>
		         <option class="u10401_input_option" value="BIBOP 2">
		            BIBOP 2
		         </option>
		      </select>
		   </div>
		   <div class="ax_default text_area" id="u10402">
		      <div class="" id="u10402_div">
		      </div>
		      <textarea class="u10402_input" id="u10402_input"></textarea>
		   </div>
		   <div class="ax_default label" id="u10403">
		      <div class="" id="u10403_div">
		      </div>
		      <div class="text" id="u10403_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Người thu
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default label" id="u10404">
		      <div class="" id="u10404_div">
		      </div>
		      <div class="text" id="u10404_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Lớp học
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default droplist" id="u10405">
		      <div class="" id="u10405_div">
		      </div>
		      <select class="u10405_input" id="u10405_input">
		         <option class="u10405_input_option" value="Lớp BIBO 1">
		            Lớp BIBO 1
		         </option>
		         <option class="u10405_input_option" value="Lớp BIBO 2">
		            Lớp BIBO 2
		         </option>
		      </select>
		   </div>
		   <div class="ax_default label" id="u10406">
		      <div class="" id="u10406_div">
		      </div>
		      <div class="text" id="u10406_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Ngày tạo
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default droplist" id="u10407">
		      <div class="" id="u10407_div">
		      </div>
		      <select class="u10407_input" id="u10407_input">
		         <option class="u10407_input_option" value="Current user">
		            Current user
		         </option>
		      </select>
		   </div>
		   <div class="ax_default droplist" id="u10408">
		      <div class="" id="u10408_div">
		      </div>
		      <select class="u10408_input" id="u10408_input">
		         <option class="u10408_input_option" value="Để trống nếu là ngày hôm nay">
		            Để trống nếu là ngày hôm nay
		         </option>
		      </select>
		   </div>
		   <div class="ax_default shape" id="u10409">
		      <div class="" id="u10409_div">
		      </div>
		      <div class="text" id="u10409_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default label" id="u10410">
		      <div class="" id="u10410_div">
		      </div>
		      <div class="text" id="u10410_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Trung tâm
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default label" id="u10411">
		      <div class="" id="u10411_div">
		      </div>
		      <div class="text" id="u10411_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Loại hóa đơn
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default label" id="u10412">
		      <div class="" id="u10412_div">
		      </div>
		      <div class="text" id="u10412_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Số tiền
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default text_field" id="u10413">
		      <div class="" id="u10413_div">
		      </div>
		      <input class="u10413_input" id="u10413_input" type="text" value="10.000.000"/>
		   </div>
		   <div class="ax_default label" id="u10414">
		      <div class="" id="u10414_div">
		      </div>
		      <div class="text" id="u10414_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Giảm trừ
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default label" id="u10415">
		      <div class="" id="u10415_div">
		      </div>
		      <div class="text" id="u10415_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Buồi học
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default droplist" id="u10416">
		      <div class="" id="u10416_div">
		      </div>
		      <select class="u10416_input" id="u10416_input">
		         <option class="u10416_input_option" selected="" value="Buổi thứ 6">
		            Buổi thứ 6
		         </option>
		         <option class="u10416_input_option" value="Buổi thứ 7">
		            Buổi thứ 7
		         </option>
		         <option class="u10416_input_option" value="Buổi thứ 8">
		            Buổi thứ 8
		         </option>
		      </select>
		   </div>
		   <div class="ax_default label" id="u10417">
		      <div class="" id="u10417_div">
		      </div>
		      <div class="text" id="u10417_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Số buổi: 5
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default label" id="u10418">
		      <div class="" id="u10418_div">
		      </div>
		      <div class="text" id="u10418_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               200.000
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default label" id="u10419">
		      <div class="" id="u10419_div">
		      </div>
		      <div class="text" id="u10419_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               1.000.000
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default label" id="u10420">
		      <div class="" id="u10420_div">
		      </div>
		      <div class="text" id="u10420_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Phụ phí
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default" data-height="25" data-left="1608" data-top="189" data-width="222" id="u10421">
		      <div class="ax_default droplist" id="u10422">
		         <div class="" id="u10422_div">
		         </div>
		         <select class="u10422_input" id="u10422_input">
		            <option class="u10422_input_option" selected="" value="VND">
		               VND
		            </option>
		            <option class="u10422_input_option" value="%">
		               %
		            </option>
		         </select>
		      </div>
		      <div class="ax_default icon" id="u10423">
		         <img class="img" id="u10423_img" src="images/xuất_hóa_đơn/u10423.svg"/>
		         <div class="text" id="u10423_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default text_field" id="u10424">
		         <div class="" id="u10424_div">
		         </div>
		         <input class="u10424_input" id="u10424_input" type="text" value=""/>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="950" data-label="Chọn giảm trừ" data-left="0" data-top="0" data-width="1920" id="u10425" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default shape" id="u10426">
		         <div class="" id="u10426_div">
		         </div>
		         <div class="text" id="u10426_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u10427">
		         <div class="ax_default shape" id="u10428">
		            <div class="" id="u10428_div">
		            </div>
		            <div class="text" id="u10428_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default" data-height="0" data-label="Header" data-left="0" data-top="0" data-width="0" id="u10429">
		            <div class="ax_default paragraph" id="u10430">
		               <div class="" id="u10430_div">
		               </div>
		               <div class="text" id="u10430_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Giảm trừ
		                     </span>
		                  </p>
		               </div>
		            </div>
		         </div>
		         <div class="ax_default droplist" id="u10431">
		            <div class="" id="u10431_div">
		            </div>
		            <select class="u10431_input" id="u10431_input">
		               <option class="u10431_input_option" value="Chọn 1 loại giảm trừ">
		                  Chọn 1 loại giảm trừ
		               </option>
		            </select>
		         </div>
		         <div class="ax_default primary_button" id="u10432">
		            <div class="" id="u10432_div">
		            </div>
		            <div class="text" id="u10432_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Thêm
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u10433">
		            <img class="img" id="u10433_img" src="images/login/u29.svg"/>
		            <div class="text" id="u10433_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="25" data-left="1613" data-top="234" data-width="217" id="u10434">
		      <div class="ax_default droplist" id="u10435">
		         <div class="" id="u10435_div">
		         </div>
		         <select class="u10435_input" id="u10435_input">
		            <option class="u10435_input_option" selected="" value="VND">
		               VND
		            </option>
		            <option class="u10435_input_option" value="%">
		               %
		            </option>
		         </select>
		      </div>
		      <div class="ax_default text_field" id="u10436">
		         <div class="" id="u10436_div">
		         </div>
		         <input class="u10436_input" id="u10436_input" type="text" value=""/>
		      </div>
		      <div class="ax_default icon" id="u10437">
		         <img class="img" id="u10437_img" src="images/xuất_hóa_đơn/u10437.svg"/>
		         <div class="text" id="u10437_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="950" data-label="Chọn phụ phí" data-left="10" data-top="10" data-width="1920" id="u10438" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default shape" id="u10439">
		         <div class="" id="u10439_div">
		         </div>
		         <div class="text" id="u10439_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u10440">
		         <div class="ax_default shape" id="u10441">
		            <div class="" id="u10441_div">
		            </div>
		            <div class="text" id="u10441_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default" data-height="0" data-label="Header" data-left="0" data-top="0" data-width="0" id="u10442">
		            <div class="ax_default paragraph" id="u10443">
		               <div class="" id="u10443_div">
		               </div>
		               <div class="text" id="u10443_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Phụ phí
		                     </span>
		                  </p>
		               </div>
		            </div>
		         </div>
		         <div class="ax_default droplist" id="u10444">
		            <div class="" id="u10444_div">
		            </div>
		            <select class="u10444_input" id="u10444_input">
		               <option class="u10444_input_option" value="Chọn 1 loại phụ phí">
		                  Chọn 1 loại phụ phí
		               </option>
		            </select>
		         </div>
		         <div class="ax_default primary_button" id="u10445">
		            <div class="" id="u10445_div">
		            </div>
		            <div class="text" id="u10445_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Thêm
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u10446">
		            <img class="img" id="u10446_img" src="images/login/u29.svg"/>
		            <div class="text" id="u10446_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default label" id="u10447">
		      <div class="" id="u10447_div">
		      </div>
		      <div class="text" id="u10447_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Số tiền cần thu
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default label" id="u10448">
		      <div class="" id="u10448_div">
		      </div>
		      <div class="text" id="u10448_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Số tiền đã thu
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default label" id="u10449">
		      <div class="" id="u10449_div">
		      </div>
		      <div class="text" id="u10449_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Số tiền thực thu
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default text_field" id="u10450">
		      <div class="" id="u10450_div">
		      </div>
		      <input class="u10450_input" id="u10450_input" type="text" value="10.000.000"/>
		   </div>
		   <div class="ax_default text_field" id="u10451">
		      <div class="" id="u10451_div">
		      </div>
		      <input class="u10451_input" id="u10451_input" type="text" value="10.000.000"/>
		   </div>
		   <div class="ax_default text_field" id="u10452">
		      <div class="" id="u10452_div">
		      </div>
		      <input class="u10452_input" id="u10452_input" type="text" value="10.000.000"/>
		   </div>
		   <div class="ax_default primary_button" id="u10453">
		      <div class="" id="u10453_div">
		      </div>
		      <div class="text" id="u10453_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Tạo hóa đơn
		            </span>
		         </p>
		      </div>
		   </div>
		</div>
		
      </div>
    );
  }
}

export default Page;
