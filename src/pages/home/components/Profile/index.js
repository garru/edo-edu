import React, { Component } from "react";
import "./index.css";
import { BrowserRouter, Route, withRouter } from "react-router-dom";
import { connect } from 'dva';

const data = {
  ngay_sinh: "17/02/2011",
  ma_bn: "090123801923",
  ma_hs: "20093091093",
  hinh_thuc_dt: "Nội trú",
  tinh_trang: "Đang điều trị",
  khoa: "Khoa Nội Tiêu Hóa",
  bs_dieu_tri: "BS. Nguyễn Trường Sơn",
  dd_cham_soc: "ĐD Trần Y Lý",
  ghi_chu: "",
  thoi_gian_cap_nhat: "12:00 01/01/2000",
  cap_nhat_boi: "BS. Tuấn",
  ht_dieu_tri: 'Nội trú',
};

class DropDown extends Component {
  onChange = e => {
    this.props.history.push(`${e.target.value}`);
  };

  render() {
    return (
      <select className="form-control form-control-lg" id="exampleFormControlSelect1" onChange={this.onChange}>
        <option disabled="" selected="">
          Lựa chọn
        </option>
        <option value="/cap_nhat_thong_tin_nguoi_benh">Điều chỉnh thông tin
          người bệnh
        </option>
        <option value="/edit_encounter_inpatient_infor">Điều chỉnh thông tin đợt
          khám, chữa bệnh
        </option>
        <option value="/to_tham_kham">Thêm thông tin tờ thăm, khám</option>
        <option value="/them_thong_tin_to_dieu_tri">Thêm thông tin tờ điều trị
        </option>
        <option value="/thong_tin_lam_sang">Thêm thông tin tờ chăm sóc</option>
      </select>
    );
  }
}

const Menu = withRouter(DropDown);

class Profile extends Component {
  constructor(props) {
    super(props);
    var user_selected = this.props?.Mock?.user_selected;
    var index = parseInt(user_selected);
    console.log("index>>>>", index, user_selected);
    this.state = {
      lbl_profile_title: this.props?.Mock?.user[index]?.ho_va_ten,
      lbl_di_ung: this.props?.Mock?.user[index]?.di_ung,
      lbl_ngay_sinh: this.props?.Mock?.user[index]?.dob,
      lbl_ma_bn: this.props?.Mock?.user[index]?.ma_bhyt,
      img_profile: this.props?.Mock?.user[index]?.anh,
      lbl_gender: this.props?.Mock?.user[index]?.gioi_tinh,
      lbl_idc_chinh: this.props?.Mock?.tien_su_ban_than[index]?.ten_benh,
      label_tinh_trang: this.props?.Mock?.tien_su_ban_than[index]?.tinh_trang
    };
  }

  render() {
    return (
      <div class="iq-card no-border no-shadow ">
        <div class="iq-card-body split">
          <div className="profile">
            <div className="profile-image" style={{backgroundImage:`url(${this.state.img_profile})`}}>
            </div>
            <div className="profile-content">
              <div class="iq-card-header d-flex justify-content-between black p-0">
                <div class="iq-header-title">
                  <h5 class="card-title font-bold ">
                    {this.state.lbl_profile_title ??
                    '--'}, {this.state.lbl_gender ?? '--'}, 8 tuổi </h5>
                </div>
                <div class="iq-header-title">
                  <span>Ngày vào viện: </span>
                  <span className="font-bold">20/03/2020</span>
                </div>
                <div class="iq-header-title">
                  <span>Giường: </span> <span className="font-bold">
                      005
                    </span>
                </div>
                <div class="iq-header-title">
                  <span>Phòng: </span> <span className="font-bold">
                      003
                    </span>
                </div>
                <div class="iq-header-title">
                  <span>Dị ứng: </span> <span className="font-bold">
                      {this.state.lbl_di_ung ?? '--'}
                    </span>
                </div>
              </div>

              <div className="row p-t-10">
                <div className="col-lg-7">
                  <div className="row">
                    <div className="col-lg-6">
                      <div class="about-info m-0 p-0">
                        <div class="row">
                          <div class="col-6">Ngày sinh</div>
                          <div class="col-6 p-0 black">{this.state.lbl_ngay_sinh}
                          </div>
                          <div class="col-6">Mã BN</div>
                          <div class="col-6 p-0 black">{data.ma_bn}</div>
                          <div class="col-6">Mã HS</div>
                          <div class="col-6 p-0 black">{data.ma_hs}</div>
                          <div class="col-6">Hình thức Đ/trị</div>
                          <div class="col-6 p-0 black">{data.ht_dieu_tri}</div>
                          <div class="col-6">Tình trạng</div>
                          <div class="col-6 p-0 black">{this.state.label_tinh_trang ??
                          '--'}</div>
                        </div>
                      </div>
                    </div>

                    <div className="col-lg-6">
                      <div class="about-info m-0 p-0">
                        <div class="row">
                          <div class="col-5">Khoa</div>
                          <div class="col-7 black p-0">{data.khoa}</div>
                          <div class="col-5">BS điều trị</div>
                          <div class="col-7 black p-0">
                            {data.bs_dieu_tri}
                          </div>
                          <div class="col-5">ĐD chăm sóc</div>
                          <div class="col-7 black p-0">
                            {data.dd_cham_soc}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-lg-5">
                  <div class="about-info m-0 p-0">
                    <div class="row">
                      <div class="col-7">
                        Ghi chú (cập nhật cuối
                        lúc {data.thoi_gian_cap_nhat}{" "} bởi {data.cap_nhat_boi})
                      </div>
                      <div class="col-5">
                        <div class="form-group">
                          <Menu></Menu>
                        </div>
                      </div>
                      <div class="col-7">
                        <div className="form-group m-0">
                          <textarea className="form-control form-control-lg" id="exampleFormControlTextarea1" rows={1} defaultValue={""}/>
                        </div>
                      </div>
                      <div class="col-5">
                        <button className="btn btn-primary" type="submit" style={{ "margin-top": "23px" }}>
                          Kết nối HIE
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="profile-footer bg-white justify-content-between  align-center border-radius">
          <div className="profile-footer-item">
            <img className="m-r-5" src="images/small/u647.png" alt="avatar"></img>
            <span className="black">
                Chính: {this.state.lbl_idc_chinh ?? '--'} - <strong>I10</strong>
              </span>
          </div>
          <div className="profile-footer-item">
            <img className="m-r-5" src="images/small/u647.png" alt="avatar"></img>
            <span className="black">
                Phụ: Viêm phổi nhẹ - <strong>I10</strong>
              </span>
          </div>
          <div className="profile-footer-item">
            <img className="m-r-5" src="images/small/u651.png" alt="avatar"></img>
            <span className="black">
                <strong>25</strong> lần/phút
              </span>
          </div>
          <div className="profile-footer-item">
            <img className="m-r-5" src="images/small/u652.png" alt="avatar"></img>
            <span className="black">
                <strong>37</strong> oC
              </span>
          </div>
          <div className="profile-footer-item">
            <img className="m-r-5" src="images/small/u655.png" alt="avatar"></img>
            <span className="black">
                <strong>110/80</strong> mmHg
              </span>
          </div>
          <div className="profile-footer-item">
            <img className="m-r-5" src="images/small/u654.png" alt="avatar"></img>
            <span className="black">
                <strong>25</strong> lần/phút
              </span>
          </div>
          <div className="profile-footer-item">
            <img className="m-r-5" src="images/small/u653.png" alt="avatar"></img>
            <span className="black">
                <strong>98 </strong>(%)
              </span>
          </div>

          <div className="profile-footer-item ">
            <div className="d-flex align-center">
              <img className="size-20 m-r-5" src="images/small/u667.svg" alt="avatar"></img>
              <div>
                <span>Lần cuối lúc</span> <span> 12:00 </span>
                <span>20/04/2020</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

// export default Profile;
export default connect(({ Mock }) => ({
  Mock,
}))(Profile);
