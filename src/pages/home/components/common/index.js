import React, { Component, useState} from "react";
import moment from "moment";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { Modal, Button } from "react-bootstrap";

class Common extends Component {
  constructor(...args) {
    super(...args);
    this.state = {
        selectedDate: moment('01-02-2020', 'MM-DD-YYYY'),
        startDate: moment().toDate(),
        show: false
    };
}

  render() {

    return (
      <div>
        <table class="table table-bordered table-responsive-md table-striped">
          <thead>
            <tr>
              <th>Ví dụ</th>
              <th>Dùng</th>
              <th>Thẻ</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>
                <h5 className="font-bold">II. Lý do vào viện</h5>
              </td>
              <td>Title bắt đầu bằng la mã</td>
              <td>h5.font-bold</td>
            </tr>
            <tr>
              <td>
                <h6 className="font-bold">3. Tiêm chủng</h6>
              </td>
              <td>Title của bảng</td>
              <td>h6.font-bold</td>
            </tr>

            <tr>
              <td>
                <div className="update-text">
                  <p>
                    <span>Lần cuối cập nhật: </span>
                    <span>12:00 01/02/2010 </span>
                    <span class="m-r-20">bởi Bác sĩ A</span>
                    <span
                      class="fas font-20 mr-2 inline fa-plus black"
                      data-toggle="modal"
                      data-target="#ThemMoiTienSuThuocLa"
                    ></span>
                    <span class="fas font-20 mr-2 inline fa-edit black"></span>
                    <span
                      class="fas font-20 mr-2 inline fa-trash black"
                      data-toggle="modal"
                      data-target="#XoaThongTin"
                    ></span>
                  </p>
                </div>
              </td>
              <td>Thời gian cập nhật ở bảng</td>
              <td></td>
            </tr>
            <tr>
              <td>
                <ul>
                  <li>
                    <span className="blue">Quá trình khởi phát: </span>
                    <span>Vào viện do bị bỏng nước sôi</span>
                  </li>
                  <li>
                    <span className="blue">Quá trình khởi phát: </span>
                    <span>Vào viện do bị bỏng nước sôi</span>
                  </li>
                </ul>
              </td>
              <td>List thông tin</td>
              <td></td>
            </tr>

            <tr>
              <td>
                <div class="btn-group" role="group" aria-label="Basic example">
                  <button type="button" class="btn btn-outline-primary mb-3">
                    24h
                  </button>
                  <button type="button" class="btn btn-outline-primary mb-3">
                    48h
                  </button>
                  <button type="button" class="btn btn-outline-primary mb-3">
                    7d
                  </button>
                </div>
              </td>
              <td>Button group</td>
              <td></td>
            </tr>

            <tr>
              <td>
                <button
                  class="btn btn-primary btn-sm mr-2"
                  onClick={() => this.setState({show: true})}

                >
                  <span class="pl-1 font-bold">View Modal</span>
                </button>
              </td>
              <td>Class modal</td>
              <td>
                .modal-dialog.modal-xl.modal-dialog-centered.modal-dialog-scrollable
              </td>
            </tr>

            <tr>
              <td>
                <div className="input-inline">
                  <span>Vào ngày thứ </span>
                  <input
                    type="number"
                    onKeyDown={ (evt) => (evt.key==='e' ||evt.key==='E' ||evt.key==='-') && evt.preventDefault() }
                    class="form-control form-control-sm text-field"
                  ></input>
                  <span>của bệnh</span>
                </div>
              </td>
              <td>Input inline</td>
              <td></td>
            </tr>

            <tr>
              <td>
                <span class="table-add float-right mb-3 mr-2">
                  <button class="btn btn-primary btn-sm mr-2">
                    <span class="pl-1 font-bold">Thêm mới bệnh án</span>
                  </button>
                </span>
              </td>
              <td>Button thêm mới ở bảng</td>
              <td></td>
            </tr>

            <tr>
              <td>
                <ul>
                  <li>
                    <span className="yellow">.yellow</span>
                  </li>
                  <li>
                    <span className="blue">.blue</span>
                  </li>
                  <li>
                    <span className="gray">.gray</span>
                  </li>
                  <li>
                    <span className="blue-1">.blue-1</span>
                  </li>
                  <li>
                    <span className="red">.red</span>
                  </li>
                  <li>
                    <span className="green">.green</span>
                  </li>
                  <li>
                    <span className="black">.black</span>
                  </li>
                </ul>
              </td>
              <td>Class màu</td>
              <td></td>
            </tr>
            <tr>
              <td>
                <ul>
                  <li>
                    <span className="font-bold">.font-bold</span>
                  </li>
                  <li>
                    <span className="upper-case">.upper-case</span>
                  </li>
                </ul>
              </td>
              <td>Class font</td>
              <td></td>
            </tr>
            <tr>
              <td>
                <div className="form-group">
                  <label class="" for="ten_benh">
                    Tên bệnh:
                  </label>
                  <select class="form-control form-control-sm" id="ten_benh">
                    <option value="1">Chửa ngoài dạ con - 021</option>
                    <option value="2">Viêm họng mãn - J21</option>
                    <option value="3">Trĩ - K34</option>
                  </select>
                </div>
              </td>
              <td>Select options</td>
              <td></td>
            </tr>
            <tr>
              <td>
                <div className="form-group">
                  <label class="" for="ten_benh">
                    Tên bệnh:
                  </label>
                  <input type="email" className="form-control form-control-lg" id="email1"></input>
                </div>
              </td>
              <td>Input</td>
              <td></td>
            </tr>
            <tr>
              <td>
                <div className="form-group">
                  <label htmlFor="dateInput">Date Input</label>
                  <div className="custom-datepicker">
                  <DatePicker
                  id="dateInput"
                    className="form-control form-control-sm"
                    selected={new Date('08/06/2020')}
                    maxDate={new Date()}
                    showMonthDropdown
                    showYearDropdown
                    />
                    <label htmlFor="dateInput"><i className="fa fa-calendar"></i></label>
                </div>
                </div>
              </td>
              <td>Date input picker</td>
              <td></td>
            </tr>
            <tr>
              <td>
                <div className="form-group">
                  <span className="blue">Quá trình khởi phát:</span>
                  <textarea className="textarea form-control" rows="2"></textarea>
                </div>
              </td>
              <td>Text Area</td>
              <td></td>
            </tr>

            <tr>
              <td>
                <div className="form-group">
                  <input
                    type="checkbox"
                    className="checkbox-input inline m-r-5 m-t-5"
                    id="ts1"
                  ></input>
                  <label className="inline " htmlFor="ts1">
                    Tăng huyết áp
                  </label>
                </div>
              </td>
              <td>Checkbox</td>
              <td></td>
            </tr>

            <tr>
              <td>
                <div className="custom-control custom-radio custom-control-inline">
                  <input
                    type="radio"
                    id="tinh_trang_benh_co"
                    name="tinh_trang_benh_"
                    className="custom-control-input"
                  />
                  <label
                    className="custom-control-label"
                    htmlFor="tinh_trang_benh_co"
                  >
                    Có
                  </label>
                </div>
                <div className="custom-control custom-radio custom-control-inline">
                  <input
                    type="radio"
                    id="tinh_trang_benh_khong"
                    name="tinh_trang_benh_"
                    className="custom-control-input"
                  />
                  <label
                    className="custom-control-label"
                    htmlFor="tinh_trang_benh_khong"
                  >
                    Không
                  </label>
                </div>
              </td>
              <td>Radio Button</td>
              <td></td>
            </tr>
          </tbody>
        </table>
        <div className="row">
          <div className="col-2">pagination</div>
          <div className="col-10">
            <nav>
              <ul className="pagination justify-content-end algin-center m-0">
                <li>
                  <div className="input-inline m-r-20">
                    <span>Số bản ghi của trang </span>
                    <select className="form-control form-control-sm width-70">
                      <option value="10">10</option>
                      <option value="20">20</option>
                      <option value="30">30</option>
                    </select>
                  </div>
                </li>
                <li>
                  <div className="input-inline m-r-20">
                    <span>Trang </span>
                    <select className="form-control form-control-sm">
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                    </select>
                    <span>của 2</span>
                  </div>
                </li>
                <li className="page-item">
                  <a className="page-link first-child" href="#">
                    <span className="fa fa-step-backward m-r-5"></span>
                    Đầu tiên
                  </a>
                </li>
                <li className="page-item">
                  <a className="page-link" href="#">
                    <span className="fa fa-chevron-left m-r-5"></span>
                    Trang trước
                  </a>
                </li>
                <li className="page-item">
                  <a className="page-link" href="#">
                    Tiếp <span className="fa fa-chevron-right"></span>
                  </a>
                </li>
                <li className="page-item">
                  <a className="page-link" href="#">
                    Cuối cùng <span className="fa fa-step-forward"></span>
                  </a>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        
        <div className="row">
          <div className="col-2">pagination</div>
          <div className="col-10">
            <form>
              
            </form>
          
          </div>
          </div>
          <Modal
      size={"lg"}
      show={this.state.show}
      // onHide={handleClose}
      backdrop="static"
      keyboard={false}
      id="ForgotPassword"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title>Modal title</Modal.Title>
      </Modal.Header>
      <Modal.Body style={{ padding: "40px 50px 10px" }}>
      Modal body
      </Modal.Body>
      <Modal.Footer>

        Modal footer
      </Modal.Footer>
      </Modal>
      </div>
    );
  }
}

export default Common;
