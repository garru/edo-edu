export const HEADER = [
  {
    title: "Tên kỳ nghỉ",
    key: "ten_ky_nghi",
    class: "tb-width-400"
  },
  {
    title: "Năm",
    key: "nam",
    class: "tb-width-100"
  },
  {
    title: "Lặp",
    key: "lap",
    class: "tb-width-100"
  },
  {
    title: "Ngày",
    key: "ngay",
    class: "tb-width-100"
  },
  {
    title: "Độ dài",
    key: "do_dai",
    class: "tb-width-100"
  }
];


export const DATA = [
  {
    id: "1",
    ten_ky_nghi: "Nghỉ tết dương lich",
    lap: false,
    nam: "2021",
    ngay: "1 tháng 1",
    do_dai: "1 ngày"
  },
  {
    id: "1",
    ten_ky_nghi: "Nghỉ tết dương lich",
    lap: false,
    nam: "2021",
    ngay: "1 tháng 1",
    do_dai: "1 ngày"
  },
  {
    id: "1",
    ten_ky_nghi: "Nghỉ tết dương lich",
    lap: false,
    nam: "2021",
    ngay: "1 tháng 1",
    do_dai: "1 ngày"
  },
  {
    id: "1",
    ten_ky_nghi: "Nghỉ tết dương lich",
    lap: false,
    nam: "2021",
    ngay: "1 tháng 1",
    do_dai: "1 ngày"
  }
];
