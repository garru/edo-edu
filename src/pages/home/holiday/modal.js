import React from "react";
import { Modal, Button } from "react-bootstrap";
import { connect } from "dva";
import { Formik, ErrorMessage } from "formik";
import moment from "moment";

class CustomModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      show: true,
      count: 0,
      centers: []
    };
  }

  handleClose = () => {
    this.props.onClose();
  };

  render() {
    const { isCreateNew, Holiday, id } = this.props;
    const { name, isRepeat, fromDate, toDate } = isCreateNew
      ? {
          name: "",
          isRepeat: false,
          fromDate: new Date().toISOString(),
          toDate: new Date().toISOString()
        }
      : Holiday?.current_holiday;
    return (
      <Modal
        size={"lg"}
        show={this.state.show}
        onHide={this.handleClose}
        backdrop="static"
        keyboard={false}
        id="AddNew"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Formik
          enableReinitialize={true}
          initialValues={{
            name: name,
            fromDate: moment(fromDate).format("YYYY-MM-DD"),
            toDate: moment(toDate).format("YYYY-MM-DD"),
            isRepeat: isRepeat
          }}
          validate={values => {
            const errors = {};
            !values.name && (errors.name = "Trường bắt buộc");
            moment(toDate).valueOf() < moment(fromDate).valueOf() &&
              (errors.name =
                "Ngày bắt đầu phải bé hơn hoặc bằng ngày kết thúc");
            return errors;
          }}
          onSubmit={(values, { setSubmitting }) => {
            const { dispatch } = this.props;
            dispatch({
              type: "Global/showLoading"
            });
            dispatch({
              type: isCreateNew ? "Holiday/insert" : "Holiday/update",
              payload: {
                name: values.name,
                fromDate: new Date(values.fromDate).toISOString(),
                toDate: new Date(values.toDate).toISOString(),
                isRepeat: values.isRepeat,
                id: id || undefined
              }
            })
              .then(res => {
                dispatch({
                  type: "Global/hideLoading"
                });
                !res.code
                  ? dispatch({
                      type: "Global/showSuccess"
                    })
                  : dispatch({
                      type: "Global/showError"
                    });
                !res.code && this.props.onClose(true);
              })
              .catch(() => {
                dispatch({
                  type: "Global/showError"
                });
                dispatch({
                  type: "Global/hideLoading"
                });
              });
          }}
        >
          {({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
            setFieldValue
            /* and other goodies */
          }) => (
            <form style={{ width: "100%" }} onSubmit={handleSubmit}>
              <Modal.Header closeButton>
                <Modal.Title>
                  {isCreateNew ? "Tạo kỳ nghỉ mới" : "Chỉnh sửa kỳ nghỉ"}
                </Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <div className="row">
                  <div className="col-12">
                    <div className="form-group">
                      <label htmlFor="name">Tên kỳ nghỉ:</label>
                      <input
                        type="text"
                        name="name"
                        className="form-control form-control-lg"
                        id="name"
                        value={values.name}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        placeholder={"Nhập Tên kỳ nghỉ"}
                      ></input>
                      <ErrorMessage
                        component="p"
                        className="error-message"
                        name="name"
                      ></ErrorMessage>
                    </div>
                  </div>
                  <div className="col-12">
                    <div className="row">
                      <div className="col-6">
                        <div className="form-group">
                          <label htmlFor="fromDate">Từ ngày:</label>
                          <input
                            type="date"
                            className="form-control form-control-lg"
                            id="fromDate"
                            value={values.fromDate}
                            onBlur={handleBlur}
                            defaultValue={isCreateNew ?  moment().format("YYYY-MM-DD") : moment(fromDate).format("YYYY-MM-DD")}
                            onChange={handleChange}
                          />
                        </div>
                      </div>

                      <div className="col-6">
                        <div className="form-group">
                          <label htmlFor="toDate">Đến ngày:</label>
                          <input
                            type="date"
                            className="form-control form-control-lg"
                            id="toDate"
                            value={values.toDate}
                            onBlur={handleBlur}
                            defaultValue={isCreateNew ?  moment().format("YYYY-MM-DD") : moment(toDate).format("YYYY-MM-DD")}
                            onChange={handleChange}
                          />
                        </div>
                      </div>
                      <div className="col-12">
                        <div className="form-group m-0">
                          <input
                            type="checkbox"
                            name="isRepeat"
                            id="isRepeat"
                            value={values.name}
                            onChange={handleChange}
                            onBlur={handleBlur}
                          />
                          <label htmlFor="isRepeat">
                            &nbsp;&nbsp; Lập hằng năm
                          </label>
                          <p>Thời gian: 0 ngày</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </Modal.Body>
              <Modal.Footer>
                <Button
                  variant="primary"
                  style={{ marginRight: 15 }}
                  type="submit"
                >
                  {isCreateNew ? "Tạo mới" : "Cập nhật"}
                </Button>
                <Button variant={"secondary"} onClick={this.handleClose}>
                  Hủy
                </Button>
              </Modal.Footer>
            </form>
          )}
        </Formik>
      </Modal>
    );
  }
}
export default connect(({ Holiday, Global }) => ({
  Holiday,
  Global
}))(CustomModal);
