import React from "react";
import { connect } from "dva";
import { Button } from "react-bootstrap";
import CustomModal from "./modal";
import * as _ from "lodash";
import PageHeader from "../../../components/PageHeader";
import CustomTooltip from "../../../components/CustomTooltip";
import CustomTable from "../../../components/Table";

class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id: null,
      activeIndex: null,
      showModal: false,
      isCreateNew: false,
      target: null,
      show: false,
      tootlTipindex: null
    };
  }

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: "Global/showLoading"
    });
    dispatch({
      type: "Holiday/search"
    }).then(
      () => {
        dispatch({
          type: "Global/hideLoading"
        });
      },
      error => {
        dispatch({
          type: "Global/hideLoading"
        });
        dispatch({
          type: "Global/showError"
        });
      }
    );
  }

  updateOptions(data) {
    const { dispatch } = this.props;
    dispatch({
      type: "Global/showLoading"
    });
    dispatch({
      type: "Holiday/updateoptions",
      payload: data
    }).then(res => {
      dispatch({
        type: "Global/hideLoading"
      });
      !res &&
        dispatch({
          type: "Global/showError"
        });
    });
  }

  renderTooltip = () => {
    const { dispatch } = this.props;
    const { tootlTipindex } = this.state;
    const dataSource = [
      {
        label: "Chỉnh sửa",
        className: "fa fa-edit",
        onClick: e => {
          dispatch({
            type: "Global/showLoading"
          });
          dispatch({
            type: "Holiday/view",
            payload: { id: this.state.id }
          }).then(() => {
            dispatch({
              type: "Global/hideLoading"
            });
            this.setState({ isCreateNew: false, showModal: true });
          });
          this.handleClick(e, tootlTipindex, true);
        }
      },
      {
        label: "Xoá",
        className: "fa fa-trash-alt",
        onClick: e => {
          this.handleClick(e, tootlTipindex, true);
          dispatch({
            type: "Global/showLoading"
          });
          dispatch({
            type: "Holiday/delete_holiday",
            payload: { id: this.state.id }
          }).then(res => {
            if (res.code) {
              dispatch({
                type: "Global/hideLoading"
              });
              dispatch({
                type: "Global/showError"
              });
            } else {
              dispatch({
                type: "Holiday/updateoptions",
                payload: {}
              }).then(() => {
                dispatch({
                  type: "Global/hideLoading"
                });
                dispatch({
                  type: "Global/showSuccess"
                });
              });
            }
          });
        }
      }
    ];
    return (
      <CustomTooltip
        show={this.state.show}
        onHide={e => {
          this.handleClick(e, tootlTipindex);
        }}
        dataSource={dataSource}
        target={this.state.target}
      ></CustomTooltip>
    );
  };

  handleClick(e, index, isHide) {
    this.setState({
      show: !(index === this.state.activeIndex),
      target: isHide ? null : e.target,
      activeIndex: index === this.state.activeIndex ? null : index
    });
  }

  onClose = isUpdate => {
    this.setState({ isCreateNew: false, showModal: false });
    if (isUpdate) {
      const { dispatch } = this.props;
      dispatch({
        type: "Global/showLoading"
      });
      dispatch({
        type: "Holiday/updateoptions",
        payload: {}
      }).then(() => {
        dispatch({
          type: "Global/hideLoading"
        });
        dispatch({
          type: "Global/showSuccess"
        });
      });
    }
  };

  render() {
    const bre = {
      title: "Kỳ nghỉ trong năm",
      subTitle:
        "Danh sách các kỳ nghỉ trong năm, các buổi học rơi vào các ngày nghỉ này sẽ được dồn vào các ngày bình thường khác",
      breadcrums: [
        {
          title: "Home",
          path: "/"
        },
        {
          title: "Kỳ nghỉ trong năm",
          path: ""
        }
      ]
    };

    const columns = [
      {
        title: "Tên kỳ nghỉ",
        key: "name",
        class: "tb-width-400"
      },
      {
        title: "Năm",
        key: "year",
        class: "tb-width-100"
      },
      {
        title: "Lặp",
        key: "isRepeat",
        class: "tb-width-100",
        render: (col, index) => (
          <input
            type="checkbox"
            name={`laisRepeatp${col.id}`}
            checked={col.isRepeat || false}
          />
        )
      },
      {
        title: "Ngày",
        key: "range",
        class: "tb-width-100"
      },
      {
        title: "Độ dài",
        key: "duration",
        class: "tb-width-100"
      },
      {
        title: "",
        key: "action",
        class: "tb_width_50",
        render: (col, index) => (
          <>
            <i
              className="fa fa-ellipsis-h"
              style={{
                marginRight: 0,
                marginLeft: "auto"
              }}
              onClick={e => {
                e.preventDefault();
                this.handleClick(e, index);
                this.setState({ id: col.id, tootlTipindex: index });
              }}
            />
          </>
        )
      }
    ];

    return (
      <React.Fragment>
        <PageHeader {...bre}></PageHeader>
        <div className="iq-card">
          <div className="iq-card-body">
            <div className="row">
              <div className="col-12 text-right">
                <div className="form-group">
                  <Button
                    variant="primary"
                    type="submit"
                    onClick={() =>
                      this.setState({ isCreateNew: true, showModal: true })
                    }
                  >
                    Tạo mới
                  </Button>
                </div>
              </div>
              <div className="col-12">
                <CustomTable
                  dataSource={this.props.Holiday.list_holiday}
                  total={this.props.Holiday.total_record}
                  columns={columns}
                  onChange={data => this.updateOptions(data)}
                  noPaging
                ></CustomTable>
              </div>
            </div>
          </div>
          {this.state.showModal && (
            <CustomModal
              isCreateNew={this.state.isCreateNew}
              onClose={this.onClose}
              selectedItem={this.state.selectedItem}
              id={this.state.id}
            ></CustomModal>
          )}
        </div>
        {this.renderTooltip()}
      </React.Fragment>
    );
  }
}

export default connect(({ Holiday, Global }) => ({
  Holiday,
  Global
}))(Page);
