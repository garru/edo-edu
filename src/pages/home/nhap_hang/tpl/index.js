import React from 'react';
import './index.css';
import './ax.css';

class Page extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
		<div class="" id="base" style={{"margin-left":"-150px"}}>
		   <div class="ax_default shape" id="u4969">
		      <div class="" id="u4969_div">
		      </div>
		      <div class="text" id="u4969_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default shape" id="u4970">
		      <div class="" id="u4970_div">
		      </div>
		      <div class="text" id="u4970_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default" data-height="40" data-label="account" data-left="10" data-top="895" data-width="192" id="u4972">
		      <div class="ax_default shape" data-label="accountLable" id="u4973">
		         <img class="img" id="u4973_img" src="images/account_information/accountlable_u55.svg"/>
		         <div class="text" id="u4973_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Nguyen Van A
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u4974">
		         <img class="img" id="u4974_img" src="images/login/u4.svg"/>
		         <div class="text" id="u4974_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="settingMenu" data-left="8" data-top="826" data-width="125" id="u4975">
		      <div class="ax_default shape" data-label="gearIconLable" id="u4976">
		         <img class="img" id="u4976_img" src="images/account_information/geariconlable_u58.svg"/>
		         <div class="text" id="u4976_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Cài đặt
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="44" data-left="8" data-top="826" data-width="44" id="u4977">
		         <div class="ax_default shape" data-label="gearIconBG" id="u4978">
		            <div class="" id="u4978_div">
		            </div>
		            <div class="text" id="u4978_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default shape" data-label="gearIconBG2" id="u4979">
		            <div class="" id="u4979_div">
		            </div>
		            <div class="text" id="u4979_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" data-label="gearIcon" id="u4980">
		            <img class="img" id="u4980_img" src="images/account_information/gearicon_u62.svg"/>
		            <div class="text" id="u4980_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="customerMenu" data-left="8" data-top="145" data-width="143" id="u4981">
		      <div class="ax_default shape" data-label="customerIconLable" id="u4982">
		         <img class="img" id="u4982_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u4982_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Khách hàng
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="customerIconBG" id="u4983">
		         <div class="" id="u4983_div">
		         </div>
		         <div class="text" id="u4983_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="customerIconBG2" id="u4984">
		         <div class="" id="u4984_div">
		         </div>
		         <div class="text" id="u4984_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" data-label="customer" id="u4985">
		         <img class="img" id="u4985_img" src="images/account_information/customer_u67.svg"/>
		         <div class="text" id="u4985_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="classMenu" data-left="8" data-top="198" data-width="143" id="u4986">
		      <div class="ax_default shape" data-label="classIconLable" id="u4987">
		         <img class="img" id="u4987_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u4987_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Lớp học
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG" id="u4988">
		         <div class="" id="u4988_div">
		         </div>
		         <div class="text" id="u4988_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG2" id="u4989">
		         <div class="" id="u4989_div">
		         </div>
		         <div class="text" id="u4989_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" data-label="classicon" id="u4990">
		         <img class="img" id="u4990_img" src="images/account_information/classicon_u72.svg"/>
		         <div class="text" id="u4990_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="dashboardMenu" data-left="8" data-top="91" data-width="143" id="u4991">
		      <div class="ax_default shape" data-label="dashboardIconLable" id="u4992">
		         <img class="img" id="u4992_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u4992_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Dashnoard
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="dashboardIconBG" id="u4993">
		         <div class="" id="u4993_div">
		         </div>
		         <div class="text" id="u4993_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="dashboardIconBG2" id="u4994">
		         <div class="" id="u4994_div">
		         </div>
		         <div class="text" id="u4994_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" data-label="dashboard" id="u4995">
		         <img class="img" id="u4995_img" src="images/account_information/dashboard_u77.svg"/>
		         <div class="text" id="u4995_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="129" data-label="AccountMenu" data-left="55" data-top="806" data-width="246" id="u4996" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default shape" id="u4997">
		         <div class="" id="u4997_div">
		         </div>
		         <div class="text" id="u4997_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u4998">
		         <div class="" id="u4998_div">
		         </div>
		         <div class="text" id="u4998_text">
		            <p>
		               <span style={{"font-family":"ArialMT, Arial","font-weight":"400","text-decoration":"none"}}>
		                  Xin chào:
		               </span>
		               <span style={{"font-family":"Arial Bold, Arial","font-weight":"700","text-decoration":"none"}}>
		                  Nguyen Van A
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="-3" data-top="0" data-width="0" id="u4999">
		         <div class="ax_default image" id="u5000">
		            <img class="img" id="u5000_img" src="images/account_information/u82.png"/>
		            <div class="text" id="u5000_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u5001">
		            <div class="" id="u5001_div">
		            </div>
		            <div class="text" id="u5001_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Đăng xuất
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default line" id="u5002">
		         <img class="img" id="u5002_img" src="images/account_information/u84.svg"/>
		         <div class="text" id="u5002_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="-3" data-top="0" data-width="0" id="u5003">
		         <div class="ax_default paragraph" id="u5004">
		            <div class="" id="u5004_div">
		            </div>
		            <div class="text" id="u5004_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tài khoản
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default image" id="u5005">
		            <img class="img" id="u5005_img" src="images/account_information/u87.png"/>
		            <div class="text" id="u5005_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="339" data-label="SettingPannel" data-left="55" data-top="525" data-width="339" id="u5006" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default shape" id="u5007">
		         <div class="" id="u5007_div">
		         </div>
		         <div class="text" id="u5007_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u5008">
		         <div class="" id="u5008_div">
		         </div>
		         <div class="text" id="u5008_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Hệ thống
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="-3" data-top="0" data-width="0" id="u5009">
		         <div class="ax_default icon" id="u5010">
		            <img class="img" id="u5010_img" src="images/account_information/u92.svg"/>
		            <div class="text" id="u5010_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u5011">
		            <div class="" id="u5011_div">
		            </div>
		            <div class="text" id="u5011_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tài khoản
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="-3" data-top="0" data-width="0" id="u5012">
		         <div class="ax_default paragraph" id="u5013">
		            <div class="" id="u5013_div">
		            </div>
		            <div class="text" id="u5013_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phân quyền
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u5014">
		            <img class="img" id="u5014_img" src="images/account_information/u96.svg"/>
		            <div class="text" id="u5014_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u5015">
		         <div class="" id="u5015_div">
		         </div>
		         <div class="text" id="u5015_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Đào tạo
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="-3" data-top="0" data-width="0" id="u5016">
		         <div class="ax_default paragraph" id="u5017">
		            <div class="" id="u5017_div">
		            </div>
		            <div class="text" id="u5017_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Thương hiệu
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u5018">
		            <img class="img" id="u5018_img" src="images/account_information/u100.svg"/>
		            <div class="text" id="u5018_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="-3" data-top="0" data-width="0" id="u5019">
		         <div class="ax_default paragraph" id="u5020">
		            <div class="" id="u5020_div">
		            </div>
		            <div class="text" id="u5020_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trung tâm
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u5021">
		            <img class="img" id="u5021_img" src="images/account_information/u103.svg"/>
		            <div class="text" id="u5021_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="-3" data-top="0" data-width="0" id="u5022">
		         <div class="ax_default icon" id="u5023">
		            <img class="img" id="u5023_img" src="images/account_information/u105.svg"/>
		            <div class="text" id="u5023_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u5024">
		            <div class="" id="u5024_div">
		            </div>
		            <div class="text" id="u5024_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Ngày nghỉ
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="-3" data-top="0" data-width="0" id="u5025">
		         <div class="ax_default icon" id="u5026">
		            <img class="img" id="u5026_img" src="images/account_information/u108.svg"/>
		            <div class="text" id="u5026_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u5027">
		            <div class="" id="u5027_div">
		            </div>
		            <div class="text" id="u5027_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phòng ban - Vai trò
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="-3" data-top="0" data-width="0" id="u5028">
		         <div class="ax_default paragraph" id="u5029">
		            <div class="" id="u5029_div">
		            </div>
		            <div class="text" id="u5029_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trạng thái học viên
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u5030">
		            <img class="img" id="u5030_img" src="images/account_information/u112.svg"/>
		            <div class="text" id="u5030_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="-3" data-top="0" data-width="0" id="u5031">
		         <div class="ax_default paragraph" id="u5032">
		            <div class="" id="u5032_div">
		            </div>
		            <div class="text" id="u5032_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tiêu chí đánh giá
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u5033">
		            <img class="img" id="u5033_img" src="images/account_information/u115.svg"/>
		            <div class="text" id="u5033_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="-3" data-top="0" data-width="0" id="u5034">
		         <div class="ax_default paragraph" id="u5035">
		            <div class="" id="u5035_div">
		            </div>
		            <div class="text" id="u5035_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Chương trình học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default" data-height="0" data-left="-3" data-top="0" data-width="0" id="u5036">
		            <div class="ax_default icon" id="u5037">
		               <img class="img" id="u5037_img" src="images/account_information/u119.svg"/>
		               <div class="text" id="u5037_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="-3" data-top="0" data-width="0" id="u5038">
		         <div class="ax_default icon" id="u5039">
		            <img class="img" id="u5039_img" src="images/account_information/u121.svg"/>
		            <div class="text" id="u5039_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u5040">
		            <div class="" id="u5040_div">
		            </div>
		            <div class="text" id="u5040_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Bài học
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="-3" data-top="0" data-width="0" id="u5041">
		         <div class="ax_default paragraph" id="u5042">
		            <div class="" id="u5042_div">
		            </div>
		            <div class="text" id="u5042_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phòng học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u5043">
		            <img class="img" id="u5043_img" src="images/account_information/u125.svg"/>
		            <div class="text" id="u5043_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="-3" data-top="0" data-width="0" id="u5044">
		         <div class="ax_default paragraph" id="u5045">
		            <div class="" id="u5045_div">
		            </div>
		            <div class="text" id="u5045_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trường học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u5046">
		            <img class="img" id="u5046_img" src="images/account_information/u128.svg"/>
		            <div class="text" id="u5046_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default line" id="u5047">
		         <img class="img" id="u5047_img" src="images/account_information/u129.svg"/>
		         <div class="text" id="u5047_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u5048">
		         <div class="" id="u5048_div">
		         </div>
		         <div class="text" id="u5048_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Tài chính
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="-3" data-top="0" data-width="0" id="u5049">
		         <div class="ax_default paragraph" id="u5050">
		            <div class="" id="u5050_div">
		            </div>
		            <div class="text" id="u5050_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Học phí
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u5051">
		            <img class="img" id="u5051_img" src="images/account_information/u133.svg"/>
		            <div class="text" id="u5051_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="-3" data-top="0" data-width="0" id="u5052">
		         <div class="ax_default paragraph" id="u5053">
		            <div class="" id="u5053_div">
		            </div>
		            <div class="text" id="u5053_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Khuyến mại
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u5054">
		            <img class="img" id="u5054_img" src="images/account_information/u136.svg"/>
		            <div class="text" id="u5054_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="classMenu" data-left="8" data-top="252" data-width="143" id="u5055">
		      <div class="ax_default shape" data-label="classIconLable" id="u5056">
		         <img class="img" id="u5056_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u5056_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Giáo viên
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG" id="u5057">
		         <div class="" id="u5057_div">
		         </div>
		         <div class="text" id="u5057_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG2" id="u5058">
		         <div class="" id="u5058_div">
		         </div>
		         <div class="text" id="u5058_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u5059">
		         <img class="img" id="u5059_img" src="images/account_information/u141.svg"/>
		         <div class="text" id="u5059_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u5060">
		      <img class="img" id="u5060_img" src="images/account_information/u142.svg"/>
		      <div class="text" id="u5060_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u5061">
		      <img class="img" id="u5061_img" src="images/account_information/u143.svg"/>
		      <div class="text" id="u5061_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div id="u4968" style={{"display":"none","visibility":"hidden"}}>
		   </div>
		   <div class="ax_default" data-height="55" data-label="Header" data-left="133" data-top="17" data-width="528" id="u5062">
		      <div class="ax_default paragraph" id="u5063">
		         <div class="" id="u5063_div">
		         </div>
		         <div class="text" id="u5063_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Nhập hàng
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u5064">
		         <div class="" id="u5064_div">
		         </div>
		         <div class="text" id="u5064_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Cho phép thêm sản phẩm với số lượng, giá thành khác nhau trong một lần nhập kho
		               </span>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="788" data-left="182" data-top="203" data-width="1186" id="u5065">
		      <div class="ax_default" data-height="260" data-left="182" data-top="203" data-width="1185" id="u5066">
		         <div class="ax_default flow_shape" id="u5067">
		            <div class="" id="u5067_div">
		            </div>
		            <div class="text" id="u5067_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default heading_3" id="u5068">
		            <div class="" id="u5068_div">
		            </div>
		            <div class="text" id="u5068_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Diễn giải
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default text_field" id="u5069">
		            <div class="" id="u5069_div">
		            </div>
		            <input class="u5069_input" id="u5069_input" type="text" value=""/>
		         </div>
		      </div>
		      <div class="ax_default" data-height="190" data-left="182" data-top="480" data-width="1185" id="u5070">
		         <div class="ax_default flow_shape" id="u5071">
		            <div class="" id="u5071_div">
		            </div>
		            <div class="text" id="u5071_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default heading_3" id="u5072">
		            <div class="" id="u5072_div">
		            </div>
		            <div class="text" id="u5072_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Mã sản phẩm
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default heading_3" id="u5073">
		            <div class="" id="u5073_div">
		            </div>
		            <div class="text" id="u5073_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Số lượng
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default heading_3" id="u5074">
		            <div class="" id="u5074_div">
		            </div>
		            <div class="text" id="u5074_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Giá nhập
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default" data-height="25" data-left="629" data-top="513" data-width="290" id="u5075">
		            <div class="ax_default heading_3" id="u5076">
		               <div class="" id="u5076_div">
		               </div>
		               <div class="text" id="u5076_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Tên sản phẩm
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default text_field" id="u5077">
		               <div class="" id="u5077_div">
		               </div>
		               <input class="u5077_input" id="u5077_input" type="text" value=""/>
		            </div>
		         </div>
		         <div class="ax_default text_field" id="u5078">
		            <div class="" id="u5078_div">
		            </div>
		            <input class="u5078_input" id="u5078_input" type="text" value=""/>
		         </div>
		         <div class="ax_default text_field" id="u5079">
		            <div class="" id="u5079_div">
		            </div>
		            <input class="u5079_input" id="u5079_input" type="text" value=""/>
		         </div>
		         <div class="ax_default" data-height="25" data-left="629" data-top="612" data-width="290" id="u5080">
		            <div class="ax_default heading_3" id="u5081">
		               <div class="" id="u5081_div">
		               </div>
		               <div class="text" id="u5081_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Giá xuất dự kiến
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default text_field" id="u5082">
		               <div class="" id="u5082_div">
		               </div>
		               <input class="u5082_input" id="u5082_input" type="text" value=""/>
		            </div>
		         </div>
		         <div class="ax_default text_field" id="u5083">
		            <div class="" id="u5083_div">
		            </div>
		            <input class="u5083_input" id="u5083_input" type="text" value=""/>
		         </div>
		         <div class="ax_default" data-height="25" data-left="629" data-top="559" data-width="290" id="u5084">
		            <div class="ax_default heading_3" id="u5085">
		               <div class="" id="u5085_div">
		               </div>
		               <div class="text" id="u5085_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Đơn vị
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default droplist" id="u5086">
		               <div class="" id="u5086_div">
		               </div>
		               <select class="u5086_input" id="u5086_input">
		                  <option class="u5086_input_option" selected="" value="Chọn 1 giá trị">
		                     Chọn 1 giá trị
		                  </option>
		                  <option class="u5086_input_option" value="Chiếc">
		                     Chiếc
		                  </option>
		                  <option class="u5086_input_option" value="Quyển">
		                     Quyển
		                  </option>
		                  <option class="u5086_input_option" value="Thùng">
		                     Thùng
		                  </option>
		                  <option class="u5086_input_option" value="Hộp">
		                     Hộp
		                  </option>
		                  <option class="u5086_input_option" value="Phần">
		                     Phần
		                  </option>
		               </select>
		            </div>
		         </div>
		         <div class="ax_default" data-height="25" data-left="1009" data-top="612" data-width="292" id="u5087">
		            <div class="ax_default heading_3" id="u5088">
		               <div class="" id="u5088_div">
		               </div>
		               <div class="text" id="u5088_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Màu sắc
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default droplist" id="u5089">
		               <div class="" id="u5089_div">
		               </div>
		               <select class="u5089_input" id="u5089_input">
		                  <option class="u5089_input_option" selected="" value="Chọn 1 giá trị">
		                     Chọn 1 giá trị
		                  </option>
		                  <option class="u5089_input_option" value="Xanh">
		                     Xanh
		                  </option>
		                  <option class="u5089_input_option" value="Đen">
		                     Đen
		                  </option>
		                  <option class="u5089_input_option" value="Đỏ">
		                     Đỏ
		                  </option>
		                  <option class="u5089_input_option" value="Bỏ trống">
		                     Bỏ trống
		                  </option>
		               </select>
		            </div>
		         </div>
		         <div class="ax_default" data-height="25" data-left="1009" data-top="559" data-width="292" id="u5090">
		            <div class="ax_default heading_3" id="u5091">
		               <div class="" id="u5091_div">
		               </div>
		               <div class="text" id="u5091_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Kích cỡ
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default droplist" id="u5092">
		               <div class="" id="u5092_div">
		               </div>
		               <select class="u5092_input" id="u5092_input">
		                  <option class="u5092_input_option" selected="" value="Chọn 1 giá trị">
		                     Chọn 1 giá trị
		                  </option>
		                  <option class="u5092_input_option" value="M">
		                     M
		                  </option>
		                  <option class="u5092_input_option" value="L">
		                     L
		                  </option>
		                  <option class="u5092_input_option" value="XL">
		                     XL
		                  </option>
		                  <option class="u5092_input_option" value="S">
		                     S
		                  </option>
		                  <option class="u5092_input_option" value="Bỏ trống">
		                     Bỏ trống
		                  </option>
		               </select>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default primary_button" id="u5093">
		         <div class="" id="u5093_div">
		         </div>
		         <div class="text" id="u5093_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Thêm sản phẩm
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default line" id="u5094">
		         <img class="img" id="u5094_img" src="images/nh_p_h_ng/u5094.svg"/>
		         <div class="text" id="u5094_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" id="u5095">
		         <div class="ax_default table_cell" id="u5096">
		            <img class="img" id="u5096_img" src="images/nh_p_h_ng/u5096.png"/>
		            <div class="text" id="u5096_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Mã sản phẩm
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u5097">
		            <img class="img" id="u5097_img" src="images/nh_p_h_ng/u5097.png"/>
		            <div class="text" id="u5097_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tên sản phẩm
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u5098">
		            <img class="img" id="u5098_img" src="images/nh_p_h_ng/u5098.png"/>
		            <div class="text" id="u5098_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Số lượng
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u5099">
		            <img class="img" id="u5099_img" src="images/nh_p_h_ng/u5099.png"/>
		            <div class="text" id="u5099_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Giá nhập
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u5100">
		            <img class="img" id="u5100_img" src="images/nh_p_h_ng/u5100.png"/>
		            <div class="text" id="u5100_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tổng cộng
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u5101">
		            <img class="img" id="u5101_img" src="images/nh_p_h_ng/u5101.png"/>
		            <div class="text" id="u5101_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Giá xuất
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u5102">
		            <img class="img" id="u5102_img" src="images/account/u307.png"/>
		            <div class="text" id="u5102_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Danh mục
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u5103">
		            <img class="img" id="u5103_img" src="images/nh_p_h_ng/u5103.png"/>
		            <div class="text" id="u5103_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Đơn vị
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u5104">
		            <img class="img" id="u5104_img" src="images/nh_p_h_ng/u5104.png"/>
		            <div class="text" id="u5104_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Kích cỡ
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u5105">
		            <img class="img" id="u5105_img" src="images/nh_p_h_ng/u5105.png"/>
		            <div class="text" id="u5105_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Màu sắc
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u5106">
		            <img class="img" id="u5106_img" src="images/nh_p_h_ng/u5106.png"/>
		            <div class="text" id="u5106_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u5107">
		            <img class="img" id="u5107_img" src="images/nh_p_h_ng/u5107.png"/>
		            <div class="text" id="u5107_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     123
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u5108">
		            <img class="img" id="u5108_img" src="images/nh_p_h_ng/u5108.png"/>
		            <div class="text" id="u5108_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Sách Tiếng Anh 1
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u5109">
		            <img class="img" id="u5109_img" src="images/nh_p_h_ng/u5109.png"/>
		            <div class="text" id="u5109_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     200
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u5110">
		            <img class="img" id="u5110_img" src="images/nh_p_h_ng/u5110.png"/>
		            <div class="text" id="u5110_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     30.000 VNĐ
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u5111">
		            <img class="img" id="u5111_img" src="images/nh_p_h_ng/u5111.png"/>
		            <div class="text" id="u5111_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     6.000.000 VNĐ
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u5112">
		            <img class="img" id="u5112_img" src="images/nh_p_h_ng/u5112.png"/>
		            <div class="text" id="u5112_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     50.000 VNĐ
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u5113">
		            <img class="img" id="u5113_img" src="images/nh_p_h_ng/u5113.png"/>
		            <div class="text" id="u5113_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Dụng cụ học tập
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u5114">
		            <img class="img" id="u5114_img" src="images/nh_p_h_ng/u5114.png"/>
		            <div class="text" id="u5114_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Quyển
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u5115">
		            <img class="img" id="u5115_img" src="images/nh_p_h_ng/u5115.png"/>
		            <div class="text" id="u5115_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u5116">
		            <img class="img" id="u5116_img" src="images/nh_p_h_ng/u5116.png"/>
		            <div class="text" id="u5116_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u5117">
		            <img class="img" id="u5117_img" src="images/nh_p_h_ng/u5117.png"/>
		            <div class="text" id="u5117_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default text_field" id="u5118">
		         <div class="" id="u5118_div">
		         </div>
		         <input class="u5118_input" id="u5118_input" type="text" value="                                                                                                                             6.000.000 VNĐ"/>
		      </div>
		      <div class="ax_default primary_button" id="u5119">
		         <div class="" id="u5119_div">
		         </div>
		         <div class="text" id="u5119_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Tạo phiếu nhập kho
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="22" data-label="Paging" data-left="182" data-top="969" data-width="1184" id="u5120">
		         <div class="ax_default paragraph" id="u5121">
		            <div class="" id="u5121_div">
		            </div>
		            <div class="text" id="u5121_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Hiển thị 1 đến 1 trong tổng số 1 kết quả
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u5122">
		            <div class="" id="u5122_div">
		            </div>
		            <div class="text" id="u5122_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Đi tới trang
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default droplist" id="u5123">
		            <div class="" id="u5123_div">
		            </div>
		            <select class="u5123_input" id="u5123_input">
		               <option class="u5123_input_option" selected="" value="1">
		                  1
		               </option>
		               <option class="u5123_input_option" value="2">
		                  2
		               </option>
		               <option class="u5123_input_option" value="3">
		                  3
		               </option>
		               <option class="u5123_input_option" value="4">
		                  4
		               </option>
		               <option class="u5123_input_option" value="5">
		                  5
		               </option>
		               <option class="u5123_input_option" value="6">
		                  6
		               </option>
		               <option class="u5123_input_option" value="7">
		                  7
		               </option>
		               <option class="u5123_input_option" value="8">
		                  8
		               </option>
		               <option class="u5123_input_option" value="9">
		                  9
		               </option>
		               <option class="u5123_input_option" value="10">
		                  10
		               </option>
		               <option class="u5123_input_option" value="11">
		                  11
		               </option>
		               <option class="u5123_input_option" value="12">
		                  12
		               </option>
		               <option class="u5123_input_option" value="13">
		                  13
		               </option>
		               <option class="u5123_input_option" value="14">
		                  14
		               </option>
		            </select>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u5124">
		      <div class="" id="u5124_div">
		      </div>
		      <div class="text" id="u5124_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Nhập kho tự động
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u5125">
		      <img class="img" id="u5125_img" src="images/nh_p_h_ng/u5125.svg"/>
		      <div class="text" id="u5125_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default flow_shape" id="u5126">
		      <div class="" id="u5126_div">
		      </div>
		      <div class="text" id="u5126_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default" data-height="22" data-left="251" data-top="133" data-width="288" id="u5127">
		      <div class="ax_default heading_3" id="u5128">
		         <div class="" id="u5128_div">
		         </div>
		         <div class="text" id="u5128_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Cơ sở
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default droplist" id="u5129">
		         <div class="" id="u5129_div">
		         </div>
		         <select class="u5129_input" id="u5129_input">
		            <option class="u5129_input_option" selected="" value="Chọn 1 giá trị">
		               Chọn 1 giá trị
		            </option>
		            <option class="u5129_input_option" value="Hoàng Ngân">
		               Hoàng Ngân
		            </option>
		            <option class="u5129_input_option" value="Hội sở">
		               Hội sở
		            </option>
		            <option class="u5129_input_option" value="Văn Điển">
		               Văn Điển
		            </option>
		         </select>
		      </div>
		   </div>
		   <div class="ax_default" data-height="35" data-left="1011" data-top="513" data-width="290" id="u5130">
		      <div class="ax_default heading_3" id="u5131">
		         <div class="" id="u5131_div">
		         </div>
		         <div class="text" id="u5131_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Nhà cung cấp
		               </span>
		            </p>
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  <br/>
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default droplist" id="u5132">
		         <div class="" id="u5132_div">
		         </div>
		         <select class="u5132_input" id="u5132_input">
		            <option class="u5132_input_option" selected="" value="Chọn 1 giá trị">
		               Chọn 1 giá trị
		            </option>
		            <option class="u5132_input_option" value="Nhà cung cấp 1">
		               Nhà cung cấp 1
		            </option>
		            <option class="u5132_input_option" value="Nhà cung cấp 2">
		               Nhà cung cấp 2
		            </option>
		         </select>
		      </div>
		   </div>
		   <div class="ax_default" data-height="22" data-left="1011" data-top="131" data-width="290" id="u5133">
		      <div class="ax_default heading_3" id="u5134">
		         <div class="" id="u5134_div">
		         </div>
		         <div class="text" id="u5134_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Kho
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default droplist" id="u5135">
		         <div class="" id="u5135_div">
		         </div>
		         <select class="u5135_input" id="u5135_input">
		            <option class="u5135_input_option" selected="" value="Chọn 1 giá trị">
		               Chọn 1 giá trị
		            </option>
		            <option class="u5135_input_option" value="Kho 1">
		               Kho 1
		            </option>
		            <option class="u5135_input_option" value="Kho 2">
		               Kho 2
		            </option>
		            <option class="u5135_input_option" value="Kho 3">
		               Kho 3
		            </option>
		         </select>
		      </div>
		   </div>
		   <div class="ax_default" data-height="25" data-left="628" data-top="130" data-width="292" id="u5136">
		      <div class="ax_default heading_3" id="u5137">
		         <div class="" id="u5137_div">
		         </div>
		         <div class="text" id="u5137_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Danh mục
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default droplist" id="u5138">
		         <div class="" id="u5138_div">
		         </div>
		         <select class="u5138_input" id="u5138_input">
		            <option class="u5138_input_option" selected="" value="Chọn 1 giá trị">
		               Chọn 1 giá trị
		            </option>
		            <option class="u5138_input_option" value="Dụng cụ học tập">
		               Dụng cụ học tập
		            </option>
		            <option class="u5138_input_option" value="Cơ sở vật chất">
		               Cơ sở vật chất
		            </option>
		         </select>
		      </div>
		   </div>
		</div>
		
      </div>
    );
  }
}

export default Page;
