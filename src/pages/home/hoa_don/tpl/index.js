import React from 'react';
import './index.css';
import './ax.css';

class Page extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
		<div class="" id="base" style={{"margin-left":"-150px"}}>
		   <div class="ax_default shape" id="u6472">
		      <div class="" id="u6472_div">
		      </div>
		      <div class="text" id="u6472_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default shape" id="u6473">
		      <div class="" id="u6473_div">
		      </div>
		      <div class="text" id="u6473_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default" data-height="40" data-label="account" data-left="13" data-top="895" data-width="192" id="u6475">
		      <div class="ax_default shape" data-label="accountLable" id="u6476">
		         <img class="img" id="u6476_img" src="images/account_information/accountlable_u55.svg"/>
		         <div class="text" id="u6476_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Nguyen Van A
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u6477">
		         <img class="img" id="u6477_img" src="images/login/u4.svg"/>
		         <div class="text" id="u6477_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="settingMenu" data-left="11" data-top="826" data-width="125" id="u6478">
		      <div class="ax_default shape" data-label="gearIconLable" id="u6479">
		         <img class="img" id="u6479_img" src="images/account_information/geariconlable_u58.svg"/>
		         <div class="text" id="u6479_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Cài đặt
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="44" data-left="11" data-top="826" data-width="44" id="u6480">
		         <div class="ax_default shape" data-label="gearIconBG" id="u6481">
		            <div class="" id="u6481_div">
		            </div>
		            <div class="text" id="u6481_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default shape" data-label="gearIconBG2" id="u6482">
		            <div class="" id="u6482_div">
		            </div>
		            <div class="text" id="u6482_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" data-label="gearIcon" id="u6483">
		            <img class="img" id="u6483_img" src="images/account_information/gearicon_u62.svg"/>
		            <div class="text" id="u6483_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="customerMenu" data-left="11" data-top="145" data-width="143" id="u6484">
		      <div class="ax_default shape" data-label="customerIconLable" id="u6485">
		         <img class="img" id="u6485_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u6485_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Khách hàng
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="customerIconBG" id="u6486">
		         <div class="" id="u6486_div">
		         </div>
		         <div class="text" id="u6486_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="customerIconBG2" id="u6487">
		         <div class="" id="u6487_div">
		         </div>
		         <div class="text" id="u6487_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" data-label="customer" id="u6488">
		         <img class="img" id="u6488_img" src="images/account_information/customer_u67.svg"/>
		         <div class="text" id="u6488_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="classMenu" data-left="11" data-top="198" data-width="143" id="u6489">
		      <div class="ax_default shape" data-label="classIconLable" id="u6490">
		         <img class="img" id="u6490_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u6490_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Lớp học
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG" id="u6491">
		         <div class="" id="u6491_div">
		         </div>
		         <div class="text" id="u6491_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG2" id="u6492">
		         <div class="" id="u6492_div">
		         </div>
		         <div class="text" id="u6492_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" data-label="classicon" id="u6493">
		         <img class="img" id="u6493_img" src="images/account_information/classicon_u72.svg"/>
		         <div class="text" id="u6493_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="dashboardMenu" data-left="11" data-top="91" data-width="143" id="u6494">
		      <div class="ax_default shape" data-label="dashboardIconLable" id="u6495">
		         <img class="img" id="u6495_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u6495_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Dashnoard
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="dashboardIconBG" id="u6496">
		         <div class="" id="u6496_div">
		         </div>
		         <div class="text" id="u6496_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="dashboardIconBG2" id="u6497">
		         <div class="" id="u6497_div">
		         </div>
		         <div class="text" id="u6497_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" data-label="dashboard" id="u6498">
		         <img class="img" id="u6498_img" src="images/account_information/dashboard_u77.svg"/>
		         <div class="text" id="u6498_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="129" data-label="AccountMenu" data-left="58" data-top="806" data-width="246" id="u6499" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default shape" id="u6500">
		         <div class="" id="u6500_div">
		         </div>
		         <div class="text" id="u6500_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u6501">
		         <div class="" id="u6501_div">
		         </div>
		         <div class="text" id="u6501_text">
		            <p>
		               <span style={{"font-family":"ArialMT, Arial","font-weight":"400","text-decoration":"none"}}>
		                  Xin chào:
		               </span>
		               <span style={{"font-family":"Arial Bold, Arial","font-weight":"700","text-decoration":"none"}}>
		                  Nguyen Van A
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u6502">
		         <div class="ax_default image" id="u6503">
		            <img class="img" id="u6503_img" src="images/account_information/u82.png"/>
		            <div class="text" id="u6503_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u6504">
		            <div class="" id="u6504_div">
		            </div>
		            <div class="text" id="u6504_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Đăng xuất
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default line" id="u6505">
		         <img class="img" id="u6505_img" src="images/account_information/u84.svg"/>
		         <div class="text" id="u6505_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u6506">
		         <div class="ax_default paragraph" id="u6507">
		            <div class="" id="u6507_div">
		            </div>
		            <div class="text" id="u6507_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tài khoản
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default image" id="u6508">
		            <img class="img" id="u6508_img" src="images/account_information/u87.png"/>
		            <div class="text" id="u6508_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="339" data-label="SettingPannel" data-left="58" data-top="525" data-width="339" id="u6509" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default shape" id="u6510">
		         <div class="" id="u6510_div">
		         </div>
		         <div class="text" id="u6510_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u6511">
		         <div class="" id="u6511_div">
		         </div>
		         <div class="text" id="u6511_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Hệ thống
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u6512">
		         <div class="ax_default icon" id="u6513">
		            <img class="img" id="u6513_img" src="images/account_information/u92.svg"/>
		            <div class="text" id="u6513_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u6514">
		            <div class="" id="u6514_div">
		            </div>
		            <div class="text" id="u6514_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tài khoản
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u6515">
		         <div class="ax_default paragraph" id="u6516">
		            <div class="" id="u6516_div">
		            </div>
		            <div class="text" id="u6516_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phân quyền
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u6517">
		            <img class="img" id="u6517_img" src="images/account_information/u96.svg"/>
		            <div class="text" id="u6517_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u6518">
		         <div class="" id="u6518_div">
		         </div>
		         <div class="text" id="u6518_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Đào tạo
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u6519">
		         <div class="ax_default paragraph" id="u6520">
		            <div class="" id="u6520_div">
		            </div>
		            <div class="text" id="u6520_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Thương hiệu
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u6521">
		            <img class="img" id="u6521_img" src="images/account_information/u100.svg"/>
		            <div class="text" id="u6521_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u6522">
		         <div class="ax_default paragraph" id="u6523">
		            <div class="" id="u6523_div">
		            </div>
		            <div class="text" id="u6523_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trung tâm
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u6524">
		            <img class="img" id="u6524_img" src="images/account_information/u103.svg"/>
		            <div class="text" id="u6524_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u6525">
		         <div class="ax_default icon" id="u6526">
		            <img class="img" id="u6526_img" src="images/account_information/u105.svg"/>
		            <div class="text" id="u6526_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u6527">
		            <div class="" id="u6527_div">
		            </div>
		            <div class="text" id="u6527_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Ngày nghỉ
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u6528">
		         <div class="ax_default icon" id="u6529">
		            <img class="img" id="u6529_img" src="images/account_information/u108.svg"/>
		            <div class="text" id="u6529_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u6530">
		            <div class="" id="u6530_div">
		            </div>
		            <div class="text" id="u6530_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phòng ban - Vai trò
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u6531">
		         <div class="ax_default paragraph" id="u6532">
		            <div class="" id="u6532_div">
		            </div>
		            <div class="text" id="u6532_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trạng thái học viên
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u6533">
		            <img class="img" id="u6533_img" src="images/account_information/u112.svg"/>
		            <div class="text" id="u6533_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u6534">
		         <div class="ax_default paragraph" id="u6535">
		            <div class="" id="u6535_div">
		            </div>
		            <div class="text" id="u6535_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tiêu chí đánh giá
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u6536">
		            <img class="img" id="u6536_img" src="images/account_information/u115.svg"/>
		            <div class="text" id="u6536_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u6537">
		         <div class="ax_default paragraph" id="u6538">
		            <div class="" id="u6538_div">
		            </div>
		            <div class="text" id="u6538_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Chương trình học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u6539">
		            <div class="ax_default icon" id="u6540">
		               <img class="img" id="u6540_img" src="images/account_information/u119.svg"/>
		               <div class="text" id="u6540_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u6541">
		         <div class="ax_default icon" id="u6542">
		            <img class="img" id="u6542_img" src="images/account_information/u121.svg"/>
		            <div class="text" id="u6542_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u6543">
		            <div class="" id="u6543_div">
		            </div>
		            <div class="text" id="u6543_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Bài học
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u6544">
		         <div class="ax_default paragraph" id="u6545">
		            <div class="" id="u6545_div">
		            </div>
		            <div class="text" id="u6545_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phòng học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u6546">
		            <img class="img" id="u6546_img" src="images/account_information/u125.svg"/>
		            <div class="text" id="u6546_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u6547">
		         <div class="ax_default paragraph" id="u6548">
		            <div class="" id="u6548_div">
		            </div>
		            <div class="text" id="u6548_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trường học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u6549">
		            <img class="img" id="u6549_img" src="images/account_information/u128.svg"/>
		            <div class="text" id="u6549_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default line" id="u6550">
		         <img class="img" id="u6550_img" src="images/account_information/u129.svg"/>
		         <div class="text" id="u6550_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u6551">
		         <div class="" id="u6551_div">
		         </div>
		         <div class="text" id="u6551_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Tài chính
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u6552">
		         <div class="ax_default paragraph" id="u6553">
		            <div class="" id="u6553_div">
		            </div>
		            <div class="text" id="u6553_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Học phí
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u6554">
		            <img class="img" id="u6554_img" src="images/account_information/u133.svg"/>
		            <div class="text" id="u6554_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u6555">
		         <div class="ax_default paragraph" id="u6556">
		            <div class="" id="u6556_div">
		            </div>
		            <div class="text" id="u6556_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Khuyến mại
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u6557">
		            <img class="img" id="u6557_img" src="images/account_information/u136.svg"/>
		            <div class="text" id="u6557_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="classMenu" data-left="11" data-top="252" data-width="143" id="u6558">
		      <div class="ax_default shape" data-label="classIconLable" id="u6559">
		         <img class="img" id="u6559_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u6559_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Giáo viên
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG" id="u6560">
		         <div class="" id="u6560_div">
		         </div>
		         <div class="text" id="u6560_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG2" id="u6561">
		         <div class="" id="u6561_div">
		         </div>
		         <div class="text" id="u6561_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u6562">
		         <img class="img" id="u6562_img" src="images/account_information/u141.svg"/>
		         <div class="text" id="u6562_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u6563">
		      <img class="img" id="u6563_img" src="images/account_information/u142.svg"/>
		      <div class="text" id="u6563_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u6564">
		      <img class="img" id="u6564_img" src="images/account_information/u143.svg"/>
		      <div class="text" id="u6564_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div id="u6471" style={{"display":"none","visibility":"hidden"}}>
		   </div>
		   <div class="ax_default" data-height="32" data-label="Header" data-left="133" data-top="17" data-width="301" id="u6565">
		      <div class="ax_default paragraph" id="u6566">
		         <div class="" id="u6566_div">
		         </div>
		         <div class="text" id="u6566_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Danh sách hóa đơn
		               </span>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default label" id="u6568">
		      <div class="" id="u6568_div">
		      </div>
		      <div class="text" id="u6568_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Bộ lọc
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default label" id="u6569">
		      <div class="" id="u6569_div">
		      </div>
		      <div class="text" id="u6569_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Mã/họ tên học sinh
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default text_field" id="u6570">
		      <div class="" id="u6570_div">
		      </div>
		      <input class="u6570_input" id="u6570_input" type="text" value=""/>
		   </div>
		   <div class="ax_default label" id="u6571">
		      <div class="" id="u6571_div">
		      </div>
		      <div class="text" id="u6571_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Mã hóa đơn
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default text_field" id="u6572">
		      <div class="" id="u6572_div">
		      </div>
		      <input class="u6572_input" id="u6572_input" type="text" value=""/>
		   </div>
		   <div class="ax_default primary_button" id="u6573">
		      <div class="" id="u6573_div">
		      </div>
		      <div class="text" id="u6573_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Tìm kiếm
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default" id="u6574">
		      <div class="ax_default table_cell" id="u6575">
		         <img class="img" id="u6575_img" src="images/hóa_đơn/u6575.png"/>
		         <div class="text" id="u6575_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Mã hóa đơn
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6576">
		         <img class="img" id="u6576_img" src="images/hóa_đơn/u6576.png"/>
		         <div class="text" id="u6576_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Người nộp
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6577">
		         <img class="img" id="u6577_img" src="images/hóa_đơn/u6577.png"/>
		         <div class="text" id="u6577_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Người thụ hưởng
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6578">
		         <img class="img" id="u6578_img" src="images/hóa_đơn/u6578.png"/>
		         <div class="text" id="u6578_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Nội dung nộp
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6579">
		         <img class="img" id="u6579_img" src="images/hóa_đơn/u6579.png"/>
		         <div class="text" id="u6579_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Số tiền
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6580">
		         <img class="img" id="u6580_img" src="images/hóa_đơn/u6580.png"/>
		         <div class="text" id="u6580_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Ngày tới hạn
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6581">
		         <img class="img" id="u6581_img" src="images/hóa_đơn/u6579.png"/>
		         <div class="text" id="u6581_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Trạng thái
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6582">
		         <img class="img" id="u6582_img" src="images/hóa_đơn/u6582.png"/>
		         <div class="text" id="u6582_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6583">
		         <img class="img" id="u6583_img" src="images/hóa_đơn/u6575.png"/>
		         <div class="text" id="u6583_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  HD00001
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6584">
		         <img class="img" id="u6584_img" src="images/hóa_đơn/u6576.png"/>
		         <div class="text" id="u6584_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Nguyễn Văn A
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6585">
		         <img class="img" id="u6585_img" src="images/hóa_đơn/u6577.png"/>
		         <div class="text" id="u6585_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Nguyễn Văn B
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6586">
		         <img class="img" id="u6586_img" src="images/hóa_đơn/u6578.png"/>
		         <div class="text" id="u6586_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Đặt cọc Học phí cho lớp Bibop A
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6587">
		         <img class="img" id="u6587_img" src="images/hóa_đơn/u6579.png"/>
		         <div class="text" id="u6587_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  5.000.000
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6588">
		         <img class="img" id="u6588_img" src="images/hóa_đơn/u6580.png"/>
		         <div class="text" id="u6588_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  12/03/2021
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6589">
		         <img class="img" id="u6589_img" src="images/hóa_đơn/u6579.png"/>
		         <div class="text" id="u6589_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6590">
		         <img class="img" id="u6590_img" src="images/hóa_đơn/u6582.png"/>
		         <div class="text" id="u6590_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6591">
		         <img class="img" id="u6591_img" src="images/hóa_đơn/u6591.png"/>
		         <div class="text" id="u6591_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  HD00002
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6592">
		         <img class="img" id="u6592_img" src="images/hóa_đơn/u6592.png"/>
		         <div class="text" id="u6592_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Nguyễn Văn A
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6593">
		         <img class="img" id="u6593_img" src="images/hóa_đơn/u6593.png"/>
		         <div class="text" id="u6593_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Nguyễn Văn B
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6594">
		         <img class="img" id="u6594_img" src="images/hóa_đơn/u6594.png"/>
		         <div class="text" id="u6594_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Học phí cho lớp Bibop A
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6595">
		         <img class="img" id="u6595_img" src="images/hóa_đơn/u6595.png"/>
		         <div class="text" id="u6595_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  95.000.000
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6596">
		         <img class="img" id="u6596_img" src="images/hóa_đơn/u6596.png"/>
		         <div class="text" id="u6596_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  12/03/2021
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6597">
		         <img class="img" id="u6597_img" src="images/hóa_đơn/u6595.png"/>
		         <div class="text" id="u6597_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6598">
		         <img class="img" id="u6598_img" src="images/hóa_đơn/u6598.png"/>
		         <div class="text" id="u6598_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u6599">
		      <div class="" id="u6599_div">
		      </div>
		      <div class="text" id="u6599_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Đã thanh toán
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u6600">
		      <img class="img" id="u6600_img" src="images/brand_management/u764.svg"/>
		      <div class="text" id="u6600_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u6601">
		      <img class="img" id="u6601_img" src="images/brand_management/u764.svg"/>
		      <div class="text" id="u6601_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u6602">
		      <div class="" id="u6602_div">
		      </div>
		      <div class="text" id="u6602_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Chưa thanh toán
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="88" data-label="context menu" data-left="1746" data-top="303" data-width="120" id="u6603" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default shape" id="u6604">
		         <div class="" id="u6604_div">
		         </div>
		         <div class="text" id="u6604_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default label" id="u6605">
		         <div class="" id="u6605_div">
		         </div>
		         <div class="text" id="u6605_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Chi tiết
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default label" id="u6606">
		         <div class="" id="u6606_div">
		         </div>
		         <div class="text" id="u6606_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Thanh toán
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default label" id="u6607">
		         <div class="" id="u6607_div">
		         </div>
		         <div class="text" id="u6607_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Xóa hóa đơn
		               </span>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u6608">
		      <div class="" id="u6608_div">
		      </div>
		      <div class="text" id="u6608_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Đã thanh toán
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u6609">
		      <div class="" id="u6609_div">
		      </div>
		      <div class="text" id="u6609_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Chưa thanh toán
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u6610">
		      <div class="" id="u6610_div">
		      </div>
		      <div class="text" id="u6610_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Tất cả
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u6611">
		      <div class="" id="u6611_div">
		      </div>
		      <div class="text" id="u6611_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Tạo hóa đơn
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="950" data-label="Bill info" data-left="0" data-top="0" data-width="1920" id="u6612" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default shape" id="u6613">
		         <div class="" id="u6613_div">
		         </div>
		         <div class="text" id="u6613_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" id="u6614">
		         <div class="" id="u6614_div">
		         </div>
		         <div class="text" id="u6614_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u6615">
		         <div class="" id="u6615_div">
		         </div>
		         <div class="text" id="u6615_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Thông tin hóa đơn
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default image" id="u6616">
		         <img class="img" id="u6616_img" src="images/hóa_đơn/u6616.png"/>
		         <div class="text" id="u6616_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u6617">
		         <img class="img" id="u6617_img" src="images/evaluation_category/u2145.svg"/>
		         <div class="text" id="u6617_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default button" id="u6618">
		         <div class="" id="u6618_div">
		         </div>
		         <div class="text" id="u6618_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  In lại
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default button" id="u6619">
		         <div class="" id="u6619_div">
		         </div>
		         <div class="text" id="u6619_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Đóng
		               </span>
		            </p>
		         </div>
		      </div>
		   </div>
		</div>
		
      </div>
    );
  }
}

export default Page;
