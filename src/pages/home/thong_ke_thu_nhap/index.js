import React from "react";
import {connect} from "dva";
import ThongKeThuNhapHeader from "./thong_ke_thu_nhap_header";
import ThongKeThuNhapBody from "./thong_ke_thu_nhap_body";
import PageHeader from "../../../components/PageHeader";
import useTeacherIncome from "../../../hooks/useTeacherIncome";

class Page extends React.Component {
  hooks = useTeacherIncome({dispatch: this.props.dispatch})

  componentWillMount() {
    this.hooks.search();
  }

  render() {
    return (
      <>
        <PageHeader
          title="Thống kê thu nhập"
          breadcrums={[{
            title: "Home",
            path: "/"
          }, {
            title: "Thống kê thu nhập",
            path: ""
          }
          ]}
        />
        <div className="iq-card">
          <div className="iq-card-body">
            <ThongKeThuNhapHeader/>
            <ThongKeThuNhapBody/>
          </div>
        </div>
      </>
    )
  }
}

export default connect(({TeacherIncome}) => ({
  TeacherIncome
}))(Page);
