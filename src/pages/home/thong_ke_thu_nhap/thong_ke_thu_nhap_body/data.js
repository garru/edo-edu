export const HEADER = [
  {
    title: "Ngày",
    key: "ngay",
    class: "tb-width-200"
  },
  {
    title: "Mục",
    key: "muc",
    class: "tb-width-200"
  },
  {
    title: "Lớp",
    key: "lop",
    class: "tb-width-200"
  },
  {
    title: "Trung tâm",
    key: "trung_tam",
    class: "tb-width-200"
  },
  {
    title: "Phòng",
    key: "phong",
    class: "tb-width-200"
  },
  {
    title: "Thành tiền",
    key: "thanh_tien",
    class: "tb-width-200"
  }
];

export const DATA = [
  {
    id: "1",
    ngay: "2021-03-04",
    muc: "ABC",
    lop: "Anh Văn",
    trung_tam: "Anh Mỹ",
    phong: "123 ABC",
    thanh_tien: "200.000"
  },
  {
    id: "1",
    ngay: "2021-03-04",
    muc: "ABC",
    lop: "Anh Văn",
    trung_tam: "Anh Mỹ",
    phong: "123 ABC",
    thanh_tien: "200.000"
  },
  {
    id: "1",
    ngay: "2021-03-04",
    muc: "ABC",
    lop: "Anh Văn",
    trung_tam: "Anh Mỹ",
    phong: "123 ABC",
    thanh_tien: "200.000"
  },
  {
    id: "1",
    ngay: "2021-03-04",
    muc: "ABC",
    lop: "Anh Văn",
    trung_tam: "Anh Mỹ",
    phong: "123 ABC",
    thanh_tien: "200.000"
  },
  {
    id: "1",
    ngay: "2021-03-04",
    muc: "ABC",
    lop: "Anh Văn",
    trung_tam: "Anh Mỹ",
    phong: "123 ABC",
    thanh_tien: "200.000"
  },
  {
    id: "1",
    ngay: "2021-03-04",
    muc: "ABC",
    lop: "Anh Văn",
    trung_tam: "Anh Mỹ",
    phong: "123 ABC",
    thanh_tien: "200.000"
  },
  {
    id: "1",
    ngay: "2021-03-04",
    muc: "ABC",
    lop: "Anh Văn",
    trung_tam: "Anh Mỹ",
    phong: "123 ABC",
    thanh_tien: "200.000"
  },
];
