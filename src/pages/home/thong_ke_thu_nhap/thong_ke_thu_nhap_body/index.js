import React from "react";
import {connect} from "dva";
import {DATA} from "./data";
import CustomTable from "../../../../components/Table";

class Page extends React.Component {
  constructor(props) {
    super(props);
    const initCheckedItem = DATA.map(item => false);
    this.state = {
      show: initCheckedItem,
      target: false,
      listItem: DATA,
      activeIndex: null
    };
  }

  columns = [{
      title: "Ngày",
      key: "incomeDate",
      class: "tb-width-200"
    },
    {
      title: "Mục",
      key: "incomeType",
      class: "tb-width-200"
    },
    {
      title: "Lớp",
      key: "className",
      class: "tb-width-200"
    },
    {
      title: "Trung tâm",
      key: "centerName",
      class: "tb-width-200"
    },
    {
      title: "Phòng",
      key: "phong",
      class: "tb-width-200"
    },
    {
      title: "Thành tiền",
      key: "incomeAmount",
      class: "tb-width-200"
    }]

  render() {
    return (
      <div className="row">
        <div className="col-12 m-b-2">
          <h5>
            <b>Tổng thu trong kỳ: {this.props.TeacherIncome.summary}</b>
          </h5>
        </div>
        <div className="col-12">
          <CustomTable
            dataSource={this.props.TeacherIncome.list_teacher_income}
            total={this.props.TeacherIncome.total_record}
            columns={this.columns}
            onChange={data => this.updateOptions(data)}
            checkbox
          />
        </div>
      </div>
    );
  }
}

export default connect(({TeacherIncome}) => ({
  TeacherIncome
}))(Page);
