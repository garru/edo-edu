import React from "react";
import {connect} from "dva";
import {Formik} from "formik";
import useTeacherIncome from "../../../../hooks/useTeacherIncome";

const TeacherIncomeHeader = (props) => {
  const {search, validationSchema, initialValuesSearch} = useTeacherIncome({dispatch: props.dispatch})
  return (
    <div>
      <Formik
        initialValues={initialValuesSearch}
        validationSchema={validationSchema}
        onSubmit={search}
      >
        {({
            values, handleChange, handleBlur, handleSubmit, setFieldValue, setFieldTouched
          }) => (
          <form onSubmit={handleSubmit}>
            <div className="row">
              <div className="col-md-3 col-sm-6 col-xs-12">
                <div className="form-group">
                  <label htmlFor="bat_dau">Bắt đầu:</label>
                  <input
                    type="date"
                    className="form-control form-control-lg"
                    id="from_Date"
                    name="from_Date"
                    value={values.from_Date}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                </div>
              </div>
              <div className="col-md-3 col-sm-6 col-xs-12">
                <div className="form-group">
                  <label htmlFor="ket_thuc">Kết thúc:</label>
                  <input
                    type="date"
                    className="form-control form-control-lg"
                    id="to_Date"
                    name="to_Date"
                    value={values.to_Date}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                </div>
              </div>
            </div>
          </form>
        )}
      </Formik>
    </div>
  );
}

export default connect(({TeacherIncome}) => ({
  TeacherIncome
}))(TeacherIncomeHeader);
