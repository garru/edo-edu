import React from "react";
import { connect } from "dva";
import { Button } from "react-bootstrap";
import MultiSelect from "react-multi-select-component";
import {
  TRUNG_TAM_MULTIPLE,
  DANH_MUC_HOC_TAP,
  KHO
} from "../../../../mock/dropdown";

class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      trung_tam: [],
      kho: []
    };
  }

  componentWillMount() {}

  render() {
    return (
      <fieldset className="m-b-2">
        <legend>Tìm kiếm</legend>
        <form style={{ width: "100%" }}>
          <div className="row align-end">
            <div className="col-4">
              <div className="form-group">
                <label htmlFor="firstName"></label>
                <input
                  type="text"
                  name="firstName"
                  className="form-control form-control-lg"
                  id="firstName"
                  placeholder="Nhập từ khoá để tìm kiếm"
                  // ref={register({ required: true, minLength: 8 })}
                />
              </div>
            </div>
            <div className="col-4">
              <div className="form-group">
                <Button
                  variant="primary"
                  type="submit"
                  style={{ marginRight: 30 }}
                >
                  Tìm kiếm
                </Button>
              </div>
            </div>
          </div>
        </form>
      </fieldset>
    );
  }
}

export default connect(({ HealthcareService }) => ({
  HealthcareService
}))(Page);
