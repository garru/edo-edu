import React, { Component, useState, useRef, useEffect } from "react";
import { useForm } from "react-hook-form";

import { Modal, Button } from "react-bootstrap";
import moment from "moment";
import {
  NOI_SINH,
  NGUYEN_QUAN,
  QUOC_TICH,
  HON_NHAN,
  TRUNG_TAM,
  PHONG_BAN,
  VI_TRI,
  HOP_DONG,
  DANH_MUC_HOC_TAP
} from "../../../../mock/dropdown";

export default function CustomModal(props) {
  const [show, setShow] = useState(true);
  const handleClose = () => {
    // setShow(false);
    props.onClose();
  };
  const { register, handleSubmit, errors } = useForm();
  const emailReg = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  const { isCreateNew, selectedItem } = props;
  return (
    <Modal
      size={"lg"}
      show={show}
      onHide={handleClose}
      backdrop="static"
      keyboard={false}
      id="AddNew"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title>
          {isCreateNew ? "Thêm mới sản phẩm" : "Chỉnh sửa sản phẩm"}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div>
          <form>
            <div className="iq-card ">
              <div className="iq-card-body">
                <div className="row m-b-2">
                  <div className="col-12">
                    <div className="row font-size-14">
                      <div className="col-md-6 ">
                        <div className="form-group">
                          <label htmlFor="ma_san_pham">Mã sản phẩm:</label>
                          <input
                            type="text"
                            name="ma_san_pham"
                            className="form-control form-control-lg"
                            id="ma_san_pham"
                            defaultValue={
                              !isCreateNew ? selectedItem.ma_san_pham : ""
                            }
                            // ref={register({ required: true, minLength: 8 })}
                          ></input>
                        </div>
                        <div className="form-group">
                          <label htmlFor="danh_muc">Danh mục:</label>
                          <select
                            className="form-control form-control-lg"
                            id="danh_muc"
                            name="danh_muc"
                            defaultValue={
                              !isCreateNew ? selectedItem.danh_muc : ""
                            }
                          >
                            {DANH_MUC_HOC_TAP.map(item => (
                              <option
                    value={item.code}
                    disabled={item.code !== "" ? false : true}
                    selected={item.code !== "" ? false : true}
                    hidden={item.code !== "" ? false : true}
                  >
                    {item.text}
                  </option>
                            ))}
                          </select>
                        </div>
                        <div className="form-group">
                          <label htmlFor="kich_co">Kích cỡ:</label>
                          <input
                            type="text"
                            name="kich_co"
                            className="form-control form-control-lg"
                            id="kich_co"
                            defaultValue={
                              !isCreateNew ? selectedItem.kich_co : ""
                            }
                            // ref={register({ required: true, minLength: 8 })}
                          ></input>
                        </div>
                      </div>

                      <div className="col-md-6 ">
                        <div className="form-group">
                          <label htmlFor="ten_san_pham">Tên:</label>
                          <input
                            type="text"
                            name="ten_san_pham"
                            className="form-control form-control-lg"
                            id="ten_san_pham"
                            defaultValue={!isCreateNew ? selectedItem.ten_san_pham : ""}
                            // ref={register({ required: true, minLength: 8 })}
                          ></input>
                        </div>
                        <div className="form-group">
                          <label htmlFor="don_vi">Đơn vị:</label>
                          <input
                            type="text"
                            name="don_vi"
                            className="form-control form-control-lg"
                            id="don_vi"
                            defaultValue={
                              !isCreateNew ? selectedItem.don_vi : ""
                            }
                            // ref={register({ required: true, minLength: 8 })}
                          ></input>
                        </div>
                        <div className="form-group">
                          <label htmlFor="mau_sac">Màu sắc:</label>
                          <input
                            type="text"
                            name="mau_sac"
                            className="form-control form-control-lg"
                            id="mau_sac"
                            defaultValue={
                              !isCreateNew ? selectedItem.mau_sac : ""
                            }
                            // ref={register({ required: true, minLength: 8 })}
                          ></input>
                        </div>
                      </div>
                      <div className="col-12">
                        <div className="form-group">
                          <label htmlFor="mo_ta">Mô tả:</label>
                          <textarea
                            className="textarea form-control"
                            rows="4"
                            name="mo_ta"
                            defaultValue={
                              !isCreateNew ? selectedItem.mo_ta : ""
                            }
                          ></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="primary" style={{marginRight: 15}}>
          {isCreateNew ? "Tạo mới" : "Cập nhật"}
        </Button>
        <Button variant={"secondary"} onClick={handleClose}>
          Hủy
        </Button>
      </Modal.Footer>
    </Modal>
  );
}
