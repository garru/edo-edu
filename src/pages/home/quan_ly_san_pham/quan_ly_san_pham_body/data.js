export const HEADER = [
  {
    title: "STT",
    key: "id",
    class: "tb-width-50"
  },
  {
    title: "Mã sản phẩm",
    key: "ma_san_pham",
    class: "tb-width-150"
  },
  {
    title: "Tên sản phẩm",
    key: "ten_san_pham",
    class: "tb-width-300"
  },
  {
    title: "Danh mục",
    key: "danh_muc",
    class: "tb-width-200"
  },
  {
    title: "Đơn vị",
    key: "don_vi",
    class: "tb-width-200"
  },
  {
    title: "Kích cỡ",
    key: "kich_co",
    class: "tb-width-200"
  },
  {
    title: "Màu sắc",
    key: "mau_sac",
    class: "tb-width-200"
  }
];

export const DATA = [
  {
    id: "2",
    ma_san_pham: "345",
    ten_san_pham: "Ghế học sinh",
    danh_muc: "Cơ sở vật chất",
    don_vi: "Chiếc",
    kich_co: "Vừa",
    mau_sac: "Xanh"
  },
  {
    id: "2",
    ma_san_pham: "345",
    ten_san_pham: "Ghế học sinh",
    danh_muc: "Cơ sở vật chất",
    don_vi: "Chiếc",
    kich_co: "Vừa",
    mau_sac: "Xanh"
  },
  {
    id: "2",
    ma_san_pham: "345",
    ten_san_pham: "Ghế học sinh",
    danh_muc: "Cơ sở vật chất",
    don_vi: "Chiếc",
    kich_co: "Vừa",
    mau_sac: "Xanh"
  },
  {
    id: "2",
    ma_san_pham: "345",
    ten_san_pham: "Ghế học sinh",
    danh_muc: "Cơ sở vật chất",
    don_vi: "Chiếc",
    kich_co: "Vừa",
    mau_sac: "Xanh"
  },
];
