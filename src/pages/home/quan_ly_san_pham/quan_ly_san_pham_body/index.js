import React from "react";
import { connect } from "dva";
import { Button, Table, Overlay, Popover } from "react-bootstrap";
import { DATA, HEADER } from "./data";
import CustomModal from "./modal";
import * as _ from "lodash";

class Page extends React.Component {
  constructor(props) {
    super(props);
    const initCheckedItem = DATA.map(item => false);
    this.state = {
      show: initCheckedItem,
      target: false,
      listItem: DATA,
      activeIndex: null
    };
  }

  renderTooltip(index) {
    return (
      <Overlay
        show={this.state.show[index]}
        target={this.state.target}
        placement="bottom"
        containerPadding={20}
        rootClose={true}
        rootCloseEvent={"click"}
onHide={(e) => this.handleClick(e, index)}
      >
        <Popover id="popover-contained">
          <Popover.Content>
            <ul>
              <li
                onClick={e => {
                  this.handleClick(e, index, true);
                  this.setState({ isCreateNew: false, showModal: true });
                }}
              >
                <i
                  className="fa fa-edit"
                  style={{
                    marginRight: 0,
                    marginLeft: "auto"
                  }}
                />
                &nbsp; Chỉnh sửa
              </li>

              <li
                onClick={e => {
                  this.handleClick(e, index, true);
                }}
              >
                <i
                  className="fa fa-trash-alt"
                  style={{
                    marginRight: 0,
                    marginLeft: "auto"
                  }}
                />
                &nbsp; Xoá
              </li>
            </ul>
          </Popover.Content>
        </Popover>
      </Overlay>
    );
  }

  handleClick(e, index, isHide) {
    let { show, activeIndex, listItem } = this.state;
    show[index] = !show[index];
    index !== activeIndex && (show[activeIndex] = false);
    this.setState({
      show: show,
      target: isHide ? null : e.target,
      activeIndex: index,
      selectedItem: listItem[index]
    });
  }
  onClose = () => {
    this.setState({ isCreateNew: false, showModal: false });
  };
  render() {
    return (
      <div className="row">
        <div className="col-12 d-flex justify-content-end">
          <div className="form-group">
            <Button
              variant="primary"
              type="submit"
              onClick={() =>
                this.setState({ isCreateNew: true, showModal: true })
              }
            >
              Tạo mới
            </Button>
          </div>
        </div>
        <div className="col-12">
          <div style={{ width: "100%" }}>
            <div style={{ overflowX: "auto", width: "100%" }}>
              <Table
                bordered
                hover
                // style={{ minWidth: 1500 }}
                className="table"
              >
                <thead>
                  <tr>
                    {HEADER.map(item => (
                      <th className={`${item.class} ${item.hasSort ? "sort" : ""} ${this.state.sortField === item.key ? "active" : ""}`} key={item.key}>
                      <span>{item.title}</span>
                      </th>
                    ))}
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.listItem.map((item, index) => (
                    <React.Fragment>
                      <tr key={`account_${item.id}`}>
                        <td>{item.id}</td>
                        <td> {item.ma_san_pham}</td>
                        <td> {item.ten_san_pham}</td>
                        <td> {item.danh_muc}</td>
                        <td> {item.don_vi}</td>
                        <td> {item.kich_co}</td>
                        <td> {item.mau_sac}</td>
                        <td>
                          <i
                            className="fa fa-ellipsis-h"
                            style={{
                              marginRight: 0,
                              marginLeft: "auto"
                            }}
                            onClick={e => this.handleClick(e, index)}
                          />
                          {this.renderTooltip(index)}
                        </td>
                      </tr>
                    </React.Fragment>
                  ))}
                </tbody>
              </Table>
              {this.state.listItem && this.state.listItem.length ? (
                ""
              ) : (
                <div className="not-found">
                  <i class="fas fa-inbox"></i>
                  <span>Không tìm thấy kết quả</span>
                </div>
              )}
              {this.state.showModal && (
                <CustomModal
                  isCreateNew={this.state.isCreateNew}
                  onClose={this.onClose}
                  selectedItem={this.state.selectedItem}
                />
              )}
              <p>
                <b>
                  *, Số liệu được cập nhật liên tục mỗi khi nhập hàng và xuất
                  hàng
                </b>
              </p>
              <p>Hiển thị 1 đến 1 trong tổng số 1 kết quả</p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(({ HealthcareService }) => ({
  HealthcareService
}))(Page);
