import React from "react";
import {connect} from "dva";
import QuanLySanPhamHeader from "./quan_ly_san_pham_header";
import QuanLySanPhamBody from "./quan_ly_san_pham_body";
import PageHeader from "../../../components/PageHeader";

class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillMount() {
  }

  render() {
    return (
      <div>
        <PageHeader
          title="Quản lý sản phẩm"
          breadcrums={[{
            title: "Home",
            path: "/"
          }, {
            title: "Quản lý sản phẩm",
            path: "/quan_ly_san_pham"
          }]}
          subTitle="Có thể thêm trước danh sách hàng hóa của doanh nghiệp tại trang này. Phần mềm sẽ tự động lưu sản phẩm mới khi tạo mặt hàng mới mỗi khi nhập kho."
        />
        <div className="iq-card  pb-0">
          <div className="iq-card-body">
            <div className="">
              <QuanLySanPhamHeader/>
              <QuanLySanPhamBody/>
            </div>
          </div>
        </div>
      </div>

    );
  }
}

export default connect(({HealthcareService}) => ({
  HealthcareService
}))(Page);
