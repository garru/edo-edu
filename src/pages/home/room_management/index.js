import React from "react";
import { connect } from "dva";
import RoomHeader from "./room_header";
import RoomBody from "./room_body";
import PageHeader from "../../../components/PageHeader";

class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: "Room/search"
    });
    dispatch({
      type: "Room/listcenter"
    });
    dispatch({
      type: "Room/liststatus"
    });
    dispatch({
      type: "Room/listtype"
    });
  }

  render() {
    return (
      <>
        <PageHeader
          title="Quản lý phòng học"
          subTitle="Danh sách các phòng học được sử dụng trong trung tâm"
          breadcrums={[
            {
              title: "Home",
              path: "/"
            },
            {
              title: "Thêm mới lớp học",
              path: "/room_management"
            }
          ]}
        />
        <div className="iq-card">
          <div className="iq-card-body">
            <RoomHeader />
            <RoomBody />
          </div>
        </div>
      </>
    );
  }
}

export default connect(({ Center }) => ({
  Center
}))(Page);
