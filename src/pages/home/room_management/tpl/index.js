import React from 'react';
import './index.css';
import './ax.css';

class Page extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
		<div class="" id="base" style={{"margin-left":"-150px"}}>
		   <div class="ax_default shape" id="u2147">
		      <div class="" id="u2147_div">
		      </div>
		      <div class="text" id="u2147_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default shape" id="u2148">
		      <div class="" id="u2148_div">
		      </div>
		      <div class="text" id="u2148_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default" data-height="40" data-label="account" data-left="10" data-top="895" data-width="192" id="u2150">
		      <div class="ax_default shape" data-label="accountLable" id="u2151">
		         <img class="img" id="u2151_img" src="images/account_information/accountlable_u55.svg"/>
		         <div class="text" id="u2151_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Nguyen Van A
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u2152">
		         <img class="img" id="u2152_img" src="images/login/u4.svg"/>
		         <div class="text" id="u2152_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="settingMenu" data-left="8" data-top="826" data-width="125" id="u2153">
		      <div class="ax_default shape" data-label="gearIconLable" id="u2154">
		         <img class="img" id="u2154_img" src="images/account_information/geariconlable_u58.svg"/>
		         <div class="text" id="u2154_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Cài đặt
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="44" data-left="8" data-top="826" data-width="44" id="u2155">
		         <div class="ax_default shape" data-label="gearIconBG" id="u2156">
		            <div class="" id="u2156_div">
		            </div>
		            <div class="text" id="u2156_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default shape" data-label="gearIconBG2" id="u2157">
		            <div class="" id="u2157_div">
		            </div>
		            <div class="text" id="u2157_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" data-label="gearIcon" id="u2158">
		            <img class="img" id="u2158_img" src="images/account_information/gearicon_u62.svg"/>
		            <div class="text" id="u2158_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="customerMenu" data-left="8" data-top="145" data-width="143" id="u2159">
		      <div class="ax_default shape" data-label="customerIconLable" id="u2160">
		         <img class="img" id="u2160_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u2160_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Khách hàng
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="customerIconBG" id="u2161">
		         <div class="" id="u2161_div">
		         </div>
		         <div class="text" id="u2161_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="customerIconBG2" id="u2162">
		         <div class="" id="u2162_div">
		         </div>
		         <div class="text" id="u2162_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" data-label="customer" id="u2163">
		         <img class="img" id="u2163_img" src="images/account_information/customer_u67.svg"/>
		         <div class="text" id="u2163_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="classMenu" data-left="8" data-top="198" data-width="143" id="u2164">
		      <div class="ax_default shape" data-label="classIconLable" id="u2165">
		         <img class="img" id="u2165_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u2165_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Lớp học
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG" id="u2166">
		         <div class="" id="u2166_div">
		         </div>
		         <div class="text" id="u2166_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG2" id="u2167">
		         <div class="" id="u2167_div">
		         </div>
		         <div class="text" id="u2167_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" data-label="classicon" id="u2168">
		         <img class="img" id="u2168_img" src="images/account_information/classicon_u72.svg"/>
		         <div class="text" id="u2168_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="dashboardMenu" data-left="8" data-top="91" data-width="143" id="u2169">
		      <div class="ax_default shape" data-label="dashboardIconLable" id="u2170">
		         <img class="img" id="u2170_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u2170_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Dashnoard
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="dashboardIconBG" id="u2171">
		         <div class="" id="u2171_div">
		         </div>
		         <div class="text" id="u2171_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="dashboardIconBG2" id="u2172">
		         <div class="" id="u2172_div">
		         </div>
		         <div class="text" id="u2172_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" data-label="dashboard" id="u2173">
		         <img class="img" id="u2173_img" src="images/account_information/dashboard_u77.svg"/>
		         <div class="text" id="u2173_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="129" data-label="AccountMenu" data-left="55" data-top="806" data-width="246" id="u2174" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default shape" id="u2175">
		         <div class="" id="u2175_div">
		         </div>
		         <div class="text" id="u2175_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u2176">
		         <div class="" id="u2176_div">
		         </div>
		         <div class="text" id="u2176_text">
		            <p>
		               <span style={{"font-family":"ArialMT, Arial","font-weight":"400","text-decoration":"none"}}>
		                  Xin chào:
		               </span>
		               <span style={{"font-family":"Arial Bold, Arial","font-weight":"700","text-decoration":"none"}}>
		                  Nguyen Van A
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="-3" data-top="0" data-width="0" id="u2177">
		         <div class="ax_default image" id="u2178">
		            <img class="img" id="u2178_img" src="images/account_information/u82.png"/>
		            <div class="text" id="u2178_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u2179">
		            <div class="" id="u2179_div">
		            </div>
		            <div class="text" id="u2179_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Đăng xuất
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default line" id="u2180">
		         <img class="img" id="u2180_img" src="images/account_information/u84.svg"/>
		         <div class="text" id="u2180_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="-3" data-top="0" data-width="0" id="u2181">
		         <div class="ax_default paragraph" id="u2182">
		            <div class="" id="u2182_div">
		            </div>
		            <div class="text" id="u2182_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tài khoản
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default image" id="u2183">
		            <img class="img" id="u2183_img" src="images/account_information/u87.png"/>
		            <div class="text" id="u2183_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="339" data-label="SettingPannel" data-left="55" data-top="525" data-width="339" id="u2184" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default shape" id="u2185">
		         <div class="" id="u2185_div">
		         </div>
		         <div class="text" id="u2185_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u2186">
		         <div class="" id="u2186_div">
		         </div>
		         <div class="text" id="u2186_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Hệ thống
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="-3" data-top="0" data-width="0" id="u2187">
		         <div class="ax_default icon" id="u2188">
		            <img class="img" id="u2188_img" src="images/account_information/u92.svg"/>
		            <div class="text" id="u2188_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u2189">
		            <div class="" id="u2189_div">
		            </div>
		            <div class="text" id="u2189_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tài khoản
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="-3" data-top="0" data-width="0" id="u2190">
		         <div class="ax_default paragraph" id="u2191">
		            <div class="" id="u2191_div">
		            </div>
		            <div class="text" id="u2191_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phân quyền
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u2192">
		            <img class="img" id="u2192_img" src="images/account_information/u96.svg"/>
		            <div class="text" id="u2192_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u2193">
		         <div class="" id="u2193_div">
		         </div>
		         <div class="text" id="u2193_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Đào tạo
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="-3" data-top="0" data-width="0" id="u2194">
		         <div class="ax_default paragraph" id="u2195">
		            <div class="" id="u2195_div">
		            </div>
		            <div class="text" id="u2195_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Thương hiệu
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u2196">
		            <img class="img" id="u2196_img" src="images/account_information/u100.svg"/>
		            <div class="text" id="u2196_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="-3" data-top="0" data-width="0" id="u2197">
		         <div class="ax_default paragraph" id="u2198">
		            <div class="" id="u2198_div">
		            </div>
		            <div class="text" id="u2198_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trung tâm
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u2199">
		            <img class="img" id="u2199_img" src="images/account_information/u103.svg"/>
		            <div class="text" id="u2199_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="-3" data-top="0" data-width="0" id="u2200">
		         <div class="ax_default icon" id="u2201">
		            <img class="img" id="u2201_img" src="images/account_information/u105.svg"/>
		            <div class="text" id="u2201_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u2202">
		            <div class="" id="u2202_div">
		            </div>
		            <div class="text" id="u2202_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Ngày nghỉ
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="-3" data-top="0" data-width="0" id="u2203">
		         <div class="ax_default icon" id="u2204">
		            <img class="img" id="u2204_img" src="images/account_information/u108.svg"/>
		            <div class="text" id="u2204_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u2205">
		            <div class="" id="u2205_div">
		            </div>
		            <div class="text" id="u2205_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phòng ban - Vai trò
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="-3" data-top="0" data-width="0" id="u2206">
		         <div class="ax_default paragraph" id="u2207">
		            <div class="" id="u2207_div">
		            </div>
		            <div class="text" id="u2207_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trạng thái học viên
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u2208">
		            <img class="img" id="u2208_img" src="images/account_information/u112.svg"/>
		            <div class="text" id="u2208_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="-3" data-top="0" data-width="0" id="u2209">
		         <div class="ax_default paragraph" id="u2210">
		            <div class="" id="u2210_div">
		            </div>
		            <div class="text" id="u2210_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tiêu chí đánh giá
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u2211">
		            <img class="img" id="u2211_img" src="images/account_information/u115.svg"/>
		            <div class="text" id="u2211_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="-3" data-top="0" data-width="0" id="u2212">
		         <div class="ax_default paragraph" id="u2213">
		            <div class="" id="u2213_div">
		            </div>
		            <div class="text" id="u2213_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Chương trình học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default" data-height="0" data-left="-3" data-top="0" data-width="0" id="u2214">
		            <div class="ax_default icon" id="u2215">
		               <img class="img" id="u2215_img" src="images/account_information/u119.svg"/>
		               <div class="text" id="u2215_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="-3" data-top="0" data-width="0" id="u2216">
		         <div class="ax_default icon" id="u2217">
		            <img class="img" id="u2217_img" src="images/account_information/u121.svg"/>
		            <div class="text" id="u2217_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u2218">
		            <div class="" id="u2218_div">
		            </div>
		            <div class="text" id="u2218_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Bài học
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="-3" data-top="0" data-width="0" id="u2219">
		         <div class="ax_default paragraph" id="u2220">
		            <div class="" id="u2220_div">
		            </div>
		            <div class="text" id="u2220_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phòng học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u2221">
		            <img class="img" id="u2221_img" src="images/account_information/u125.svg"/>
		            <div class="text" id="u2221_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="-3" data-top="0" data-width="0" id="u2222">
		         <div class="ax_default paragraph" id="u2223">
		            <div class="" id="u2223_div">
		            </div>
		            <div class="text" id="u2223_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trường học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u2224">
		            <img class="img" id="u2224_img" src="images/account_information/u128.svg"/>
		            <div class="text" id="u2224_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default line" id="u2225">
		         <img class="img" id="u2225_img" src="images/account_information/u129.svg"/>
		         <div class="text" id="u2225_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u2226">
		         <div class="" id="u2226_div">
		         </div>
		         <div class="text" id="u2226_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Tài chính
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="-3" data-top="0" data-width="0" id="u2227">
		         <div class="ax_default paragraph" id="u2228">
		            <div class="" id="u2228_div">
		            </div>
		            <div class="text" id="u2228_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Học phí
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u2229">
		            <img class="img" id="u2229_img" src="images/account_information/u133.svg"/>
		            <div class="text" id="u2229_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="-3" data-top="0" data-width="0" id="u2230">
		         <div class="ax_default paragraph" id="u2231">
		            <div class="" id="u2231_div">
		            </div>
		            <div class="text" id="u2231_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Khuyến mại
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u2232">
		            <img class="img" id="u2232_img" src="images/account_information/u136.svg"/>
		            <div class="text" id="u2232_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="classMenu" data-left="8" data-top="252" data-width="143" id="u2233">
		      <div class="ax_default shape" data-label="classIconLable" id="u2234">
		         <img class="img" id="u2234_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u2234_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Giáo viên
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG" id="u2235">
		         <div class="" id="u2235_div">
		         </div>
		         <div class="text" id="u2235_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG2" id="u2236">
		         <div class="" id="u2236_div">
		         </div>
		         <div class="text" id="u2236_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u2237">
		         <img class="img" id="u2237_img" src="images/account_information/u141.svg"/>
		         <div class="text" id="u2237_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u2238">
		      <img class="img" id="u2238_img" src="images/account_information/u142.svg"/>
		      <div class="text" id="u2238_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u2239">
		      <img class="img" id="u2239_img" src="images/account_information/u143.svg"/>
		      <div class="text" id="u2239_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div id="u2146" style={{"display":"none","visibility":"hidden"}}>
		   </div>
		   <div class="ax_default" data-height="629" data-label="Room List" data-left="182" data-top="278" data-width="1631" id="u2240">
		      <div class="ax_default" id="u2241">
		         <div class="ax_default table_cell" id="u2242">
		            <img class="img" id="u2242_img" src="images/room_management/u2242.png"/>
		            <div class="text" id="u2242_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2243">
		            <img class="img" id="u2243_img" src="images/room_management/u2243.png"/>
		            <div class="text" id="u2243_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tên phòng
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2244">
		            <img class="img" id="u2244_img" src="images/room_management/u2244.png"/>
		            <div class="text" id="u2244_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Loại
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2245">
		            <img class="img" id="u2245_img" src="images/room_management/u2245.png"/>
		            <div class="text" id="u2245_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trung tâm
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2246">
		            <img class="img" id="u2246_img" src="images/room_management/u2246.png"/>
		            <div class="text" id="u2246_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Số lượng tối thiểu
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2247">
		            <img class="img" id="u2247_img" src="images/room_management/u2247.png"/>
		            <div class="text" id="u2247_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Số lượng tối đa
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2248">
		            <img class="img" id="u2248_img" src="images/room_management/u2248.png"/>
		            <div class="text" id="u2248_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2249">
		            <img class="img" id="u2249_img" src="images/room_management/u2249.png"/>
		            <div class="text" id="u2249_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2250">
		            <img class="img" id="u2250_img" src="images/room_management/u2250.png"/>
		            <div class="text" id="u2250_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Mickey
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2251">
		            <img class="img" id="u2251_img" src="images/room_management/u2251.png"/>
		            <div class="text" id="u2251_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phòng học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2252">
		            <img class="img" id="u2252_img" src="images/room_management/u2252.png"/>
		            <div class="text" id="u2252_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Hoàng Ngân
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2253">
		            <img class="img" id="u2253_img" src="images/room_management/u2253.png"/>
		            <div class="text" id="u2253_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     7
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2254">
		            <img class="img" id="u2254_img" src="images/room_management/u2254.png"/>
		            <div class="text" id="u2254_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     14
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2255">
		            <img class="img" id="u2255_img" src="images/room_management/u2255.png"/>
		            <div class="text" id="u2255_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2256">
		            <img class="img" id="u2256_img" src="images/room_management/u2256.png"/>
		            <div class="text" id="u2256_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2257">
		            <img class="img" id="u2257_img" src="images/room_management/u2257.png"/>
		            <div class="text" id="u2257_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Donal
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2258">
		            <img class="img" id="u2258_img" src="images/room_management/u2258.png"/>
		            <div class="text" id="u2258_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phòng học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2259">
		            <img class="img" id="u2259_img" src="images/room_management/u2259.png"/>
		            <div class="text" id="u2259_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Hoàng Ngân
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2260">
		            <img class="img" id="u2260_img" src="images/room_management/u2260.png"/>
		            <div class="text" id="u2260_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     7
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2261">
		            <img class="img" id="u2261_img" src="images/room_management/u2261.png"/>
		            <div class="text" id="u2261_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     15
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2262">
		            <img class="img" id="u2262_img" src="images/room_management/u2262.png"/>
		            <div class="text" id="u2262_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2263">
		            <img class="img" id="u2263_img" src="images/room_management/u2249.png"/>
		            <div class="text" id="u2263_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2264">
		            <img class="img" id="u2264_img" src="images/room_management/u2250.png"/>
		            <div class="text" id="u2264_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Minie
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2265">
		            <img class="img" id="u2265_img" src="images/room_management/u2251.png"/>
		            <div class="text" id="u2265_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phòng học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2266">
		            <img class="img" id="u2266_img" src="images/room_management/u2252.png"/>
		            <div class="text" id="u2266_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Hoàng Ngân
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2267">
		            <img class="img" id="u2267_img" src="images/room_management/u2253.png"/>
		            <div class="text" id="u2267_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     10
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2268">
		            <img class="img" id="u2268_img" src="images/room_management/u2254.png"/>
		            <div class="text" id="u2268_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     16
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2269">
		            <img class="img" id="u2269_img" src="images/room_management/u2255.png"/>
		            <div class="text" id="u2269_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2270">
		            <img class="img" id="u2270_img" src="images/room_management/u2256.png"/>
		            <div class="text" id="u2270_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2271">
		            <img class="img" id="u2271_img" src="images/room_management/u2257.png"/>
		            <div class="text" id="u2271_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Doraemon
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2272">
		            <img class="img" id="u2272_img" src="images/room_management/u2258.png"/>
		            <div class="text" id="u2272_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phòng học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2273">
		            <img class="img" id="u2273_img" src="images/room_management/u2259.png"/>
		            <div class="text" id="u2273_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Hoàng Ngân
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2274">
		            <img class="img" id="u2274_img" src="images/room_management/u2260.png"/>
		            <div class="text" id="u2274_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     7
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2275">
		            <img class="img" id="u2275_img" src="images/room_management/u2261.png"/>
		            <div class="text" id="u2275_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     14
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2276">
		            <img class="img" id="u2276_img" src="images/room_management/u2262.png"/>
		            <div class="text" id="u2276_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2277">
		            <img class="img" id="u2277_img" src="images/room_management/u2277.png"/>
		            <div class="text" id="u2277_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2278">
		            <img class="img" id="u2278_img" src="images/room_management/u2250.png"/>
		            <div class="text" id="u2278_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Nobita
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2279">
		            <img class="img" id="u2279_img" src="images/room_management/u2251.png"/>
		            <div class="text" id="u2279_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phòng học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2280">
		            <img class="img" id="u2280_img" src="images/room_management/u2252.png"/>
		            <div class="text" id="u2280_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Hoàng Ngân
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2281">
		            <img class="img" id="u2281_img" src="images/room_management/u2253.png"/>
		            <div class="text" id="u2281_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     7
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2282">
		            <img class="img" id="u2282_img" src="images/room_management/u2282.png"/>
		            <div class="text" id="u2282_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     14
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2283">
		            <img class="img" id="u2283_img" src="images/room_management/u2283.png"/>
		            <div class="text" id="u2283_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2284">
		            <img class="img" id="u2284_img" src="images/room_management/u2256.png"/>
		            <div class="text" id="u2284_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2285">
		            <img class="img" id="u2285_img" src="images/room_management/u2257.png"/>
		            <div class="text" id="u2285_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Xuka
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2286">
		            <img class="img" id="u2286_img" src="images/room_management/u2258.png"/>
		            <div class="text" id="u2286_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phòng học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2287">
		            <img class="img" id="u2287_img" src="images/room_management/u2259.png"/>
		            <div class="text" id="u2287_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Hoàng Ngân
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2288">
		            <img class="img" id="u2288_img" src="images/room_management/u2260.png"/>
		            <div class="text" id="u2288_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     12
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2289">
		            <img class="img" id="u2289_img" src="images/room_management/u2261.png"/>
		            <div class="text" id="u2289_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     20
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2290">
		            <img class="img" id="u2290_img" src="images/room_management/u2262.png"/>
		            <div class="text" id="u2290_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2291">
		            <img class="img" id="u2291_img" src="images/room_management/u2277.png"/>
		            <div class="text" id="u2291_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2292">
		            <img class="img" id="u2292_img" src="images/room_management/u2292.png"/>
		            <div class="text" id="u2292_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Supper man
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2293">
		            <img class="img" id="u2293_img" src="images/room_management/u2251.png"/>
		            <div class="text" id="u2293_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phòng học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2294">
		            <img class="img" id="u2294_img" src="images/room_management/u2252.png"/>
		            <div class="text" id="u2294_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Mai Hắc Đế
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2295">
		            <img class="img" id="u2295_img" src="images/room_management/u2253.png"/>
		            <div class="text" id="u2295_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     10
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2296">
		            <img class="img" id="u2296_img" src="images/room_management/u2282.png"/>
		            <div class="text" id="u2296_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     20
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2297">
		            <img class="img" id="u2297_img" src="images/room_management/u2283.png"/>
		            <div class="text" id="u2297_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2298">
		            <img class="img" id="u2298_img" src="images/room_management/u2256.png"/>
		            <div class="text" id="u2298_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2299">
		            <img class="img" id="u2299_img" src="images/room_management/u2257.png"/>
		            <div class="text" id="u2299_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Hulk
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2300">
		            <img class="img" id="u2300_img" src="images/room_management/u2258.png"/>
		            <div class="text" id="u2300_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phòng học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2301">
		            <img class="img" id="u2301_img" src="images/room_management/u2259.png"/>
		            <div class="text" id="u2301_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Mai Hắc Đế
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2302">
		            <img class="img" id="u2302_img" src="images/room_management/u2260.png"/>
		            <div class="text" id="u2302_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     8
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2303">
		            <img class="img" id="u2303_img" src="images/room_management/u2261.png"/>
		            <div class="text" id="u2303_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     16
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2304">
		            <img class="img" id="u2304_img" src="images/room_management/u2262.png"/>
		            <div class="text" id="u2304_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2305">
		            <img class="img" id="u2305_img" src="images/room_management/u2249.png"/>
		            <div class="text" id="u2305_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2306">
		            <img class="img" id="u2306_img" src="images/room_management/u2250.png"/>
		            <div class="text" id="u2306_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Aquaman
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2307">
		            <img class="img" id="u2307_img" src="images/room_management/u2251.png"/>
		            <div class="text" id="u2307_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phòng học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2308">
		            <img class="img" id="u2308_img" src="images/room_management/u2252.png"/>
		            <div class="text" id="u2308_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Mai Hắc Đế
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2309">
		            <img class="img" id="u2309_img" src="images/room_management/u2253.png"/>
		            <div class="text" id="u2309_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     7
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2310">
		            <img class="img" id="u2310_img" src="images/room_management/u2254.png"/>
		            <div class="text" id="u2310_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     17
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2311">
		            <img class="img" id="u2311_img" src="images/room_management/u2255.png"/>
		            <div class="text" id="u2311_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2312">
		            <img class="img" id="u2312_img" src="images/room_management/u2312.png"/>
		            <div class="text" id="u2312_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2313">
		            <img class="img" id="u2313_img" src="images/room_management/u2313.png"/>
		            <div class="text" id="u2313_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Yingyang
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2314">
		            <img class="img" id="u2314_img" src="images/room_management/u2314.png"/>
		            <div class="text" id="u2314_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phòng học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2315">
		            <img class="img" id="u2315_img" src="images/room_management/u2315.png"/>
		            <div class="text" id="u2315_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Mai Hắc Đế
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2316">
		            <img class="img" id="u2316_img" src="images/room_management/u2316.png"/>
		            <div class="text" id="u2316_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     7
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2317">
		            <img class="img" id="u2317_img" src="images/room_management/u2317.png"/>
		            <div class="text" id="u2317_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     17
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u2318">
		            <img class="img" id="u2318_img" src="images/room_management/u2318.png"/>
		            <div class="text" id="u2318_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default checkbox" id="u2319">
		         <label for="u2319_input" id="u2319_input_label" style={{"position":" absolute","left":" 0px"}}>
		            <img class="img" id="u2319_img" src="images/room_management/u2319.svg"/>
		            <div class="text" id="u2319_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </label>
		         <input id="u2319_input" type="checkbox" value="checkbox"/>
		      </div>
		      <div class="ax_default checkbox" id="u2320">
		         <label for="u2320_input" id="u2320_input_label" style={{"position":" absolute","left":" 0px"}}>
		            <img class="img" id="u2320_img" src="images/room_management/u2320.svg"/>
		            <div class="text" id="u2320_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </label>
		         <input id="u2320_input" type="checkbox" value="checkbox"/>
		      </div>
		      <div class="ax_default checkbox" id="u2321">
		         <label for="u2321_input" id="u2321_input_label" style={{"position":" absolute","left":" 0px"}}>
		            <img class="img" id="u2321_img" src="images/room_management/u2321.svg"/>
		            <div class="text" id="u2321_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </label>
		         <input id="u2321_input" type="checkbox" value="checkbox"/>
		      </div>
		      <div class="ax_default checkbox" id="u2322">
		         <label for="u2322_input" id="u2322_input_label" style={{"position":" absolute","left":" 0px"}}>
		            <img class="img" id="u2322_img" src="images/room_management/u2322.svg"/>
		            <div class="text" id="u2322_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </label>
		         <input id="u2322_input" type="checkbox" value="checkbox"/>
		      </div>
		      <div class="ax_default checkbox" id="u2323">
		         <label for="u2323_input" id="u2323_input_label" style={{"position":" absolute","left":" 0px"}}>
		            <img class="img" id="u2323_img" src="images/room_management/u2323.svg"/>
		            <div class="text" id="u2323_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </label>
		         <input id="u2323_input" type="checkbox" value="checkbox"/>
		      </div>
		      <div class="ax_default checkbox" id="u2324">
		         <label for="u2324_input" id="u2324_input_label" style={{"position":" absolute","left":" 0px"}}>
		            <img class="img" id="u2324_img" src="images/room_management/u2324.svg"/>
		            <div class="text" id="u2324_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </label>
		         <input id="u2324_input" type="checkbox" value="checkbox"/>
		      </div>
		      <div class="ax_default checkbox" id="u2325">
		         <label for="u2325_input" id="u2325_input_label" style={{"position":" absolute","left":" 0px"}}>
		            <img class="img" id="u2325_img" src="images/room_management/u2325.svg"/>
		            <div class="text" id="u2325_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </label>
		         <input id="u2325_input" type="checkbox" value="checkbox"/>
		      </div>
		      <div class="ax_default checkbox" id="u2326">
		         <label for="u2326_input" id="u2326_input_label" style={{"position":" absolute","left":" 0px"}}>
		            <img class="img" id="u2326_img" src="images/room_management/u2326.svg"/>
		            <div class="text" id="u2326_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </label>
		         <input id="u2326_input" type="checkbox" value="checkbox"/>
		      </div>
		      <div class="ax_default icon" id="u2327">
		         <img class="img" id="u2327_img" src="images/student_status/u1813.svg"/>
		         <div class="text" id="u2327_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u2328">
		         <img class="img" id="u2328_img" src="images/student_status/u1813.svg"/>
		         <div class="text" id="u2328_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u2329">
		         <img class="img" id="u2329_img" src="images/student_status/u1813.svg"/>
		         <div class="text" id="u2329_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u2330">
		         <img class="img" id="u2330_img" src="images/student_status/u1813.svg"/>
		         <div class="text" id="u2330_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u2331">
		         <img class="img" id="u2331_img" src="images/student_status/u1813.svg"/>
		         <div class="text" id="u2331_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u2332">
		         <img class="img" id="u2332_img" src="images/student_status/u1813.svg"/>
		         <div class="text" id="u2332_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u2333">
		         <img class="img" id="u2333_img" src="images/student_status/u1813.svg"/>
		         <div class="text" id="u2333_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default checkbox" id="u2334">
		         <label for="u2334_input" id="u2334_input_label" style={{"position":" absolute","left":" 0px"}}>
		            <img class="img" id="u2334_img" src="images/room_management/u2334.svg"/>
		            <div class="text" id="u2334_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </label>
		         <input id="u2334_input" type="checkbox" value="checkbox"/>
		      </div>
		      <div class="ax_default icon" id="u2335">
		         <img class="img" id="u2335_img" src="images/student_status/u1813.svg"/>
		         <div class="text" id="u2335_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default checkbox" id="u2336">
		         <label for="u2336_input" id="u2336_input_label" style={{"position":" absolute","left":" 0px"}}>
		            <img class="img" id="u2336_img" src="images/room_management/u2336.svg"/>
		            <div class="text" id="u2336_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </label>
		         <input id="u2336_input" type="checkbox" value="checkbox"/>
		      </div>
		      <div class="ax_default icon" id="u2337">
		         <img class="img" id="u2337_img" src="images/student_status/u1813.svg"/>
		         <div class="text" id="u2337_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default checkbox" id="u2338">
		         <label for="u2338_input" id="u2338_input_label" style={{"position":" absolute","left":" 0px"}}>
		            <img class="img" id="u2338_img" src="images/room_management/u2338.svg"/>
		            <div class="text" id="u2338_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </label>
		         <input id="u2338_input" type="checkbox" value="checkbox"/>
		      </div>
		      <div class="ax_default icon" id="u2339">
		         <img class="img" id="u2339_img" src="images/student_status/u1813.svg"/>
		         <div class="text" id="u2339_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="80" data-label="RoomManagementContextMenu" data-left="1772" data-top="368" data-width="150" id="u2340" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default shape" id="u2341">
		         <div class="" id="u2341_div">
		         </div>
		         <div class="text" id="u2341_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u2342">
		         <div class="ax_default paragraph" id="u2343">
		            <div class="" id="u2343_div">
		            </div>
		            <div class="text" id="u2343_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Xóa
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u2344">
		            <img class="img" id="u2344_img" src="images/account/u640.svg"/>
		            <div class="text" id="u2344_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u2345">
		         <div class="ax_default icon" id="u2346">
		            <img class="img" id="u2346_img" src="images/account/u635.svg"/>
		            <div class="text" id="u2346_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u2347">
		            <div class="" id="u2347_div">
		            </div>
		            <div class="text" id="u2347_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Chỉnh sửa
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="55" data-label="Header" data-left="133" data-top="17" data-width="516" id="u2348">
		      <div class="ax_default paragraph" id="u2349">
		         <div class="" id="u2349_div">
		         </div>
		         <div class="text" id="u2349_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Quản lý phòng học
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u2350">
		         <div class="" id="u2350_div">
		         </div>
		         <div class="text" id="u2350_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Danh sách các phòng học được sử dụng trong trung tâm
		               </span>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="22" data-label="Paging" data-left="182" data-top="918" data-width="1623" id="u2351">
		      <div class="ax_default paragraph" id="u2352">
		         <div class="" id="u2352_div">
		         </div>
		         <div class="text" id="u2352_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Hiển thị 1 đến 10 trong tổng số 120 kết quả
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u2353">
		         <div class="" id="u2353_div">
		         </div>
		         <div class="text" id="u2353_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Đi tới trang
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default droplist" id="u2354">
		         <div class="" id="u2354_div">
		         </div>
		         <select class="u2354_input" id="u2354_input">
		            <option class="u2354_input_option" selected="" value="1">
		               1
		            </option>
		            <option class="u2354_input_option" value="2">
		               2
		            </option>
		            <option class="u2354_input_option" value="3">
		               3
		            </option>
		            <option class="u2354_input_option" value="4">
		               4
		            </option>
		            <option class="u2354_input_option" value="5">
		               5
		            </option>
		            <option class="u2354_input_option" value="6">
		               6
		            </option>
		            <option class="u2354_input_option" value="7">
		               7
		            </option>
		            <option class="u2354_input_option" value="8">
		               8
		            </option>
		            <option class="u2354_input_option" value="9">
		               9
		            </option>
		            <option class="u2354_input_option" value="10">
		               10
		            </option>
		            <option class="u2354_input_option" value="11">
		               11
		            </option>
		            <option class="u2354_input_option" value="12">
		               12
		            </option>
		            <option class="u2354_input_option" value="13">
		               13
		            </option>
		            <option class="u2354_input_option" value="14">
		               14
		            </option>
		         </select>
		      </div>
		   </div>
		   <div class="ax_default" data-height="87" data-label="Search" data-left="182" data-top="88" data-width="1623" id="u2355">
		      <div class="ax_default box_1" id="u2356">
		         <div class="" id="u2356_div">
		         </div>
		         <div class="text" id="u2356_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u2357">
		         <div class="" id="u2357_div">
		         </div>
		         <div class="text" id="u2357_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Tìm kiếm
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="40" data-left="250" data-top="115" data-width="1487" id="u2358">
		         <div class="ax_default" data-height="25" data-left="250" data-top="122" data-width="534" id="u2359">
		            <div class="ax_default paragraph" id="u2360">
		               <div class="" id="u2360_div">
		               </div>
		               <div class="text" id="u2360_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Tên phòng
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default text_field" id="u2361">
		               <div class="" id="u2361_div">
		               </div>
		               <input class="u2361_input" id="u2361_input" type="text" value=""/>
		            </div>
		         </div>
		         <div class="ax_default primary_button" id="u2362">
		            <div class="" id="u2362_div">
		            </div>
		            <div class="text" id="u2362_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tìm kiếm
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="40" data-label="ButtonFilter" data-left="182" data-top="211" data-width="1623" id="u2363">
		      <div class="ax_default primary_button" id="u2364">
		         <div class="" id="u2364_div">
		         </div>
		         <div class="text" id="u2364_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Tạo mới
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default button" id="u2365">
		         <div class="" id="u2365_div">
		         </div>
		         <div class="text" id="u2365_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Chỉnh sửa
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u2366">
		         <img class="img" id="u2366_img" src="images/account/u302.svg"/>
		         <div class="text" id="u2366_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default droplist" id="u2367">
		         <div class="" id="u2367_div">
		         </div>
		         <select class="u2367_input" id="u2367_input">
		            <option class="u2367_input_option" selected="" value="Trung tâm hoạt động">
		               Trung tâm hoạt động
		            </option>
		            <option class="u2367_input_option" value="Hội sở">
		               Hội sở
		            </option>
		            <option class="u2367_input_option" value="Hoàng Ngân">
		               Hoàng Ngân
		            </option>
		            <option class="u2367_input_option" value="Văn Điển">
		               Văn Điển
		            </option>
		         </select>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="950" data-label="Edit room" data-left="-3" data-top="0" data-width="1920" id="u2368" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default shape" id="u2369">
		         <div class="" id="u2369_div">
		         </div>
		         <div class="text" id="u2369_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" id="u2370">
		         <div class="" id="u2370_div">
		         </div>
		         <div class="text" id="u2370_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-label="Header" data-left="0" data-top="0" data-width="0" id="u2371">
		         <div class="ax_default paragraph" id="u2372">
		            <div class="" id="u2372_div">
		            </div>
		            <div class="text" id="u2372_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Chỉnh sửa phòng học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u2373">
		            <div class="ax_default shape" id="u2374">
		               <div class="" id="u2374_div">
		               </div>
		               <div class="text" id="u2374_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u2375">
		               <div class="" id="u2375_div">
		               </div>
		               <div class="text" id="u2375_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Phòng học: Mickey
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u2376">
		               <div class="" id="u2376_div">
		               </div>
		               <div class="text" id="u2376_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Trung tâm: Hoàng Ngân
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u2377">
		               <div class="" id="u2377_div">
		               </div>
		               <div class="text" id="u2377_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Trung tâm: Hoàng Ngân
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u2378">
		               <div class="" id="u2378_div">
		               </div>
		               <div class="text" id="u2378_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Vị trí: Tầng 2
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default paragraph" id="u2379">
		               <div class="" id="u2379_div">
		               </div>
		               <div class="text" id="u2379_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Thông tin phòng học
		                     </span>
		                  </p>
		               </div>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-label="Tiêt học bị ảnh hưởng" data-left="0" data-top="0" data-width="0" id="u2380">
		         <div class="ax_default shape" id="u2381">
		            <div class="" id="u2381_div">
		            </div>
		            <div class="text" id="u2381_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default label" id="u2382">
		            <img class="img" id="u2382_img" src="images/room_management/u2382.svg"/>
		            <div class="text" id="u2382_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Các lớp đang sử dụng phòng
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u2383">
		            <div class="ax_default" id="u2384">
		               <div class="ax_default table_cell" id="u2385">
		                  <img class="img" id="u2385_img" src="images/room_management/u2385.png"/>
		                  <div class="text" id="u2385_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Mã lớp
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u2386">
		                  <img class="img" id="u2386_img" src="images/room_management/u2386.png"/>
		                  <div class="text" id="u2386_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Ngày học cũ
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u2387">
		                  <img class="img" id="u2387_img" src="images/room_management/u2387.png"/>
		                  <div class="text" id="u2387_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Giờ học cũ
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u2388">
		                  <img class="img" id="u2388_img" src="images/room_management/u2388.png"/>
		                  <div class="text" id="u2388_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Phòng mới
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u2389">
		                  <img class="img" id="u2389_img" src="images/room_management/u2389.png"/>
		                  <div class="text" id="u2389_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Ngày học mới
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u2390">
		                  <img class="img" id="u2390_img" src="images/room_management/u2389.png"/>
		                  <div class="text" id="u2390_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Giờ học mới
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u2391">
		                  <img class="img" id="u2391_img" src="images/room_management/u2391.png"/>
		                  <div class="text" id="u2391_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Giáo viên
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u2392">
		                  <img class="img" id="u2392_img" src="images/room_management/u2392.png"/>
		                  <div class="text" id="u2392_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Trạng thái
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u2393">
		                  <img class="img" id="u2393_img" src="images/room_management/u2393.png"/>
		                  <div class="text" id="u2393_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Thao tác
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u2394">
		                  <img class="img" id="u2394_img" src="images/room_management/u2394.png"/>
		                  <div class="text" id="u2394_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           BIBOP1
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u2395">
		                  <img class="img" id="u2395_img" src="images/room_management/u2395.png"/>
		                  <div class="text" id="u2395_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           11/12/2021
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u2396">
		                  <img class="img" id="u2396_img" src="images/room_management/u2396.png"/>
		                  <div class="text" id="u2396_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           19h30-21h00
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u2397">
		                  <img class="img" id="u2397_img" src="images/room_management/u2397.png"/>
		                  <div class="text" id="u2397_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Donal
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u2398">
		                  <img class="img" id="u2398_img" src="images/room_management/u2398.png"/>
		                  <div class="text" id="u2398_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           11/13/2021
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u2399">
		                  <img class="img" id="u2399_img" src="images/room_management/u2398.png"/>
		                  <div class="text" id="u2399_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           19h30-21h00
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u2400">
		                  <img class="img" id="u2400_img" src="images/room_management/u2400.png"/>
		                  <div class="text" id="u2400_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Nguyễn Văn A
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u2401">
		                  <img class="img" id="u2401_img" src="images/room_management/u2401.png"/>
		                  <div class="text" id="u2401_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u2402">
		                  <img class="img" id="u2402_img" src="images/room_management/u2402.png"/>
		                  <div class="text" id="u2402_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u2403">
		                  <img class="img" id="u2403_img" src="images/room_management/u2394.png"/>
		                  <div class="text" id="u2403_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           BIBOP1
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u2404">
		                  <img class="img" id="u2404_img" src="images/room_management/u2395.png"/>
		                  <div class="text" id="u2404_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           13/12/2021
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u2405">
		                  <img class="img" id="u2405_img" src="images/room_management/u2396.png"/>
		                  <div class="text" id="u2405_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           19h30-21h00
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u2406">
		                  <img class="img" id="u2406_img" src="images/room_management/u2397.png"/>
		                  <div class="text" id="u2406_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u2407">
		                  <img class="img" id="u2407_img" src="images/room_management/u2398.png"/>
		                  <div class="text" id="u2407_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u2408">
		                  <img class="img" id="u2408_img" src="images/room_management/u2398.png"/>
		                  <div class="text" id="u2408_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u2409">
		                  <img class="img" id="u2409_img" src="images/room_management/u2400.png"/>
		                  <div class="text" id="u2409_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Nguyễn Văn A
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u2410">
		                  <img class="img" id="u2410_img" src="images/room_management/u2401.png"/>
		                  <div class="text" id="u2410_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u2411">
		                  <img class="img" id="u2411_img" src="images/room_management/u2402.png"/>
		                  <div class="text" id="u2411_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u2412">
		                  <img class="img" id="u2412_img" src="images/room_management/u2394.png"/>
		                  <div class="text" id="u2412_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           BIBOP4
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u2413">
		                  <img class="img" id="u2413_img" src="images/room_management/u2395.png"/>
		                  <div class="text" id="u2413_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           18/12/2021
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u2414">
		                  <img class="img" id="u2414_img" src="images/room_management/u2396.png"/>
		                  <div class="text" id="u2414_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           19h30-21h00
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u2415">
		                  <img class="img" id="u2415_img" src="images/room_management/u2397.png"/>
		                  <div class="text" id="u2415_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u2416">
		                  <img class="img" id="u2416_img" src="images/room_management/u2398.png"/>
		                  <div class="text" id="u2416_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u2417">
		                  <img class="img" id="u2417_img" src="images/room_management/u2398.png"/>
		                  <div class="text" id="u2417_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u2418">
		                  <img class="img" id="u2418_img" src="images/room_management/u2400.png"/>
		                  <div class="text" id="u2418_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Nguyễn Văn A
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u2419">
		                  <img class="img" id="u2419_img" src="images/room_management/u2401.png"/>
		                  <div class="text" id="u2419_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u2420">
		                  <img class="img" id="u2420_img" src="images/room_management/u2402.png"/>
		                  <div class="text" id="u2420_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u2421">
		                  <img class="img" id="u2421_img" src="images/room_management/u2394.png"/>
		                  <div class="text" id="u2421_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           BIBOP4
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u2422">
		                  <img class="img" id="u2422_img" src="images/room_management/u2395.png"/>
		                  <div class="text" id="u2422_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           18/12/2021
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u2423">
		                  <img class="img" id="u2423_img" src="images/room_management/u2396.png"/>
		                  <div class="text" id="u2423_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           19h30-21h00
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u2424">
		                  <img class="img" id="u2424_img" src="images/room_management/u2397.png"/>
		                  <div class="text" id="u2424_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u2425">
		                  <img class="img" id="u2425_img" src="images/room_management/u2398.png"/>
		                  <div class="text" id="u2425_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u2426">
		                  <img class="img" id="u2426_img" src="images/room_management/u2398.png"/>
		                  <div class="text" id="u2426_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u2427">
		                  <img class="img" id="u2427_img" src="images/room_management/u2400.png"/>
		                  <div class="text" id="u2427_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Nguyễn Văn A
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u2428">
		                  <img class="img" id="u2428_img" src="images/room_management/u2401.png"/>
		                  <div class="text" id="u2428_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u2429">
		                  <img class="img" id="u2429_img" src="images/room_management/u2402.png"/>
		                  <div class="text" id="u2429_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u2430">
		                  <img class="img" id="u2430_img" src="images/room_management/u2430.png"/>
		                  <div class="text" id="u2430_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           BIBOP4
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u2431">
		                  <img class="img" id="u2431_img" src="images/room_management/u2431.png"/>
		                  <div class="text" id="u2431_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           18/12/2021
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u2432">
		                  <img class="img" id="u2432_img" src="images/room_management/u2432.png"/>
		                  <div class="text" id="u2432_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           19h30-21h00
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u2433">
		                  <img class="img" id="u2433_img" src="images/room_management/u2433.png"/>
		                  <div class="text" id="u2433_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u2434">
		                  <img class="img" id="u2434_img" src="images/room_management/u2434.png"/>
		                  <div class="text" id="u2434_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u2435">
		                  <img class="img" id="u2435_img" src="images/room_management/u2434.png"/>
		                  <div class="text" id="u2435_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u2436">
		                  <img class="img" id="u2436_img" src="images/room_management/u2436.png"/>
		                  <div class="text" id="u2436_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Nguyễn Văn A
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u2437">
		                  <img class="img" id="u2437_img" src="images/room_management/u2437.png"/>
		                  <div class="text" id="u2437_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u2438">
		                  <img class="img" id="u2438_img" src="images/room_management/u2438.png"/>
		                  <div class="text" id="u2438_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		            </div>
		            <div class="ax_default icon" id="u2439">
		               <img class="img" id="u2439_img" src="images/room_management/u2439.svg"/>
		               <div class="text" id="u2439_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default icon" id="u2440">
		               <img class="img" id="u2440_img" src="images/room_management/u2440.svg"/>
		               <div class="text" id="u2440_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default icon" id="u2441">
		               <img class="img" id="u2441_img" src="images/room_management/u2440.svg"/>
		               <div class="text" id="u2441_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default icon" id="u2442">
		               <img class="img" id="u2442_img" src="images/room_management/u2440.svg"/>
		               <div class="text" id="u2442_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default icon" id="u2443">
		               <img class="img" id="u2443_img" src="images/room_management/u2440.svg"/>
		               <div class="text" id="u2443_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default icon" id="u2444">
		               <img class="img" id="u2444_img" src="images/evaluation_category/u2084.svg"/>
		               <div class="text" id="u2444_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default icon" id="u2445">
		               <img class="img" id="u2445_img" src="images/evaluation_category/u2084.svg"/>
		               <div class="text" id="u2445_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default icon" id="u2446">
		               <img class="img" id="u2446_img" src="images/evaluation_category/u2084.svg"/>
		               <div class="text" id="u2446_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default icon" id="u2447">
		               <img class="img" id="u2447_img" src="images/evaluation_category/u2084.svg"/>
		               <div class="text" id="u2447_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default icon" id="u2448">
		               <img class="img" id="u2448_img" src="images/evaluation_category/u2084.svg"/>
		               <div class="text" id="u2448_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		         </div>
		         <div class="ax_default label" id="u2449">
		            <div class="" id="u2449_div">
		            </div>
		            <div class="text" id="u2449_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Có 15 tiết học bị ảnh hưởng, vui lòng sửa lại lịch học
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-label="Chỉnh sửa thông tin" data-left="0" data-top="0" data-width="0" id="u2450">
		         <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u2451">
		            <div class="ax_default shape" id="u2452">
		               <div class="" id="u2452_div">
		               </div>
		               <div class="text" id="u2452_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default paragraph" id="u2453">
		               <img class="img" id="u2453_img" src="images/room_management/u2453.svg"/>
		               <div class="text" id="u2453_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Thông tin chỉnh sửa
		                     </span>
		                  </p>
		               </div>
		            </div>
		         </div>
		         <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u2454">
		            <div class="ax_default label" id="u2455">
		               <div class="" id="u2455_div">
		               </div>
		               <div class="text" id="u2455_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Tên phòng
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default text_field" id="u2456">
		               <div class="" id="u2456_div">
		               </div>
		               <input class="u2456_input" id="u2456_input" type="text" value="Mickey"/>
		            </div>
		         </div>
		         <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u2457">
		            <div class="ax_default checkbox" id="u2458">
		               <label for="u2458_input" id="u2458_input_label" style={{"position":" absolute","left":" 0px"}}>
		                  <img class="img" id="u2458_img" src="images/room_management/u2458.svg"/>
		                  <div class="text" id="u2458_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Phòng họp
		                        </span>
		                     </p>
		                  </div>
		               </label>
		               <input id="u2458_input" type="checkbox" value="checkbox"/>
		            </div>
		            <div class="ax_default checkbox selected" id="u2459">
		               <label for="u2459_input" id="u2459_input_label" style={{"position":" absolute","left":" 0px"}}>
		                  <img class="img" id="u2459_img" src="images/room_management/u2459_selected.svg"/>
		                  <div class="text" id="u2459_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Phòng học
		                        </span>
		                     </p>
		                  </div>
		               </label>
		               <input checked="" id="u2459_input" type="checkbox" value="checkbox"/>
		            </div>
		            <div class="ax_default label" id="u2460">
		               <div class="" id="u2460_div">
		               </div>
		               <div class="text" id="u2460_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Loại phòng
		                     </span>
		                  </p>
		               </div>
		            </div>
		         </div>
		         <div class="ax_default label" id="u2461">
		            <div class="" id="u2461_div">
		            </div>
		            <div class="text" id="u2461_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trạng thái
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default droplist" id="u2462">
		            <div class="" id="u2462_div">
		            </div>
		            <select class="u2462_input" id="u2462_input">
		               <option class="u2462_input_option" selected="" value="Hoạt động">
		                  Hoạt động
		               </option>
		               <option class="u2462_input_option" value="Tạm đóng">
		                  Tạm đóng
		               </option>
		               <option class="u2462_input_option" value="Ngưng sử dụng">
		                  Ngưng sử dụng
		               </option>
		            </select>
		         </div>
		         <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u2463">
		            <div class="ax_default droplist" id="u2464">
		               <div class="" id="u2464_div">
		               </div>
		               <select class="u2464_input" id="u2464_input">
		                  <option class="u2464_input_option" value="Vui lòng chọn 1 giá trị">
		                     Vui lòng chọn 1 giá trị
		                  </option>
		                  <option class="u2464_input_option" selected="" value="Hoàng Ngân">
		                     Hoàng Ngân
		                  </option>
		                  <option class="u2464_input_option" value="Mai Hắc Đế">
		                     Mai Hắc Đế
		                  </option>
		               </select>
		            </div>
		            <div class="ax_default label" id="u2465">
		               <div class="" id="u2465_div">
		               </div>
		               <div class="text" id="u2465_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Trung tâm
		                     </span>
		                  </p>
		               </div>
		            </div>
		         </div>
		         <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u2466">
		            <div class="ax_default text_field" id="u2467">
		               <div class="" id="u2467_div">
		               </div>
		               <input class="u2467_input" id="u2467_input" type="text" value="Tầng 1"/>
		            </div>
		            <div class="ax_default label" id="u2468">
		               <div class="" id="u2468_div">
		               </div>
		               <div class="text" id="u2468_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Vị trí
		                     </span>
		                  </p>
		               </div>
		            </div>
		         </div>
		         <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u2469">
		            <div class="ax_default text_field" id="u2470">
		               <div class="" id="u2470_div">
		               </div>
		               <input class="u2470_input" id="u2470_input" type="text" value="14"/>
		            </div>
		            <div class="ax_default text_field" id="u2471">
		               <div class="" id="u2471_div">
		               </div>
		               <input class="u2471_input" id="u2471_input" type="text" value="7"/>
		            </div>
		            <div class="ax_default label" id="u2472">
		               <div class="" id="u2472_div">
		               </div>
		               <div class="text" id="u2472_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Dung lượng
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u2473">
		               <div class="" id="u2473_div">
		               </div>
		               <div class="text" id="u2473_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        -
		                     </span>
		                  </p>
		               </div>
		            </div>
		         </div>
		         <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u2474">
		            <div class="ax_default droplist" id="u2475">
		               <div class="" id="u2475_div">
		               </div>
		               <select class="u2475_input" id="u2475_input">
		                  <option class="u2475_input_option" selected="" value="20/12/2021">
		                     20/12/2021
		                  </option>
		               </select>
		            </div>
		            <div class="ax_default label" id="u2476">
		               <div class="" id="u2476_div">
		               </div>
		               <div class="text" id="u2476_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Thời gian
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default droplist" id="u2477">
		               <div class="" id="u2477_div">
		               </div>
		               <select class="u2477_input" id="u2477_input">
		                  <option class="u2477_input_option" selected="" value="20/12/2021">
		                     20/12/2021
		                  </option>
		               </select>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default primary_button" id="u2478">
		         <img class="img" id="u2478_img" src="images/room_management/u2478.svg"/>
		         <div class="text" id="u2478_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Lưu
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default button" id="u2479">
		         <div class="" id="u2479_div">
		         </div>
		         <div class="text" id="u2479_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Hủy
		               </span>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="950" data-label="Sửa tiết hocj" data-left="-3" data-top="0" data-width="1920" id="u2480" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default shape" id="u2481">
		         <div class="" id="u2481_div">
		         </div>
		         <div class="text" id="u2481_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" id="u2482">
		         <div class="" id="u2482_div">
		         </div>
		         <div class="text" id="u2482_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u2483">
		         <div class="" id="u2483_div">
		         </div>
		         <div class="text" id="u2483_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Chỉnh sửa tiết học
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u2484">
		         <div class="ax_default shape" id="u2485">
		            <div class="" id="u2485_div">
		            </div>
		            <div class="text" id="u2485_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default label" id="u2486">
		            <div class="" id="u2486_div">
		            </div>
		            <div class="text" id="u2486_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phòng học: Mickey
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default label" id="u2487">
		            <div class="" id="u2487_div">
		            </div>
		            <div class="text" id="u2487_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trung tâm: Hoàng Ngân
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default label" id="u2488">
		            <div class="" id="u2488_div">
		            </div>
		            <div class="text" id="u2488_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trung tâm: Hoàng Ngân
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default label" id="u2489">
		            <div class="" id="u2489_div">
		            </div>
		            <div class="text" id="u2489_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Vị trí: Tầng 2
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u2490">
		            <img class="img" id="u2490_img" src="images/room_management/u2490.svg"/>
		            <div class="text" id="u2490_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Thông tin tiết học hiện tại
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default label" id="u2491">
		            <div class="" id="u2491_div">
		            </div>
		            <div class="text" id="u2491_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Mã lớp: BIBOP1
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default label" id="u2492">
		            <div class="" id="u2492_div">
		            </div>
		            <div class="text" id="u2492_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Ngày học: 13/12/2021
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default label" id="u2493">
		            <div class="" id="u2493_div">
		            </div>
		            <div class="text" id="u2493_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Giờ học: 19h30-21h00
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default label" id="u2494">
		            <div class="" id="u2494_div">
		            </div>
		            <div class="text" id="u2494_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Giáo viên: Nguyễn Văn A
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u2495">
		         <div class="ax_default shape" id="u2496">
		            <div class="" id="u2496_div">
		            </div>
		            <div class="text" id="u2496_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u2497">
		            <img class="img" id="u2497_img" src="images/room_management/u2497.svg"/>
		            <div class="text" id="u2497_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Thông tin tiết học thay đổi
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default label" id="u2498">
		            <div class="" id="u2498_div">
		            </div>
		            <div class="text" id="u2498_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trung tâm
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default droplist" id="u2499">
		            <div class="" id="u2499_div">
		            </div>
		            <select class="u2499_input" id="u2499_input">
		               <option class="u2499_input_option" selected="" value="Hoàng Ngân">
		                  Hoàng Ngân
		               </option>
		               <option class="u2499_input_option" value="Phố Huế">
		                  Phố Huế
		               </option>
		            </select>
		         </div>
		         <div class="ax_default label" id="u2500">
		            <div class="" id="u2500_div">
		            </div>
		            <div class="text" id="u2500_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Giờ học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default text_field" id="u2501">
		            <div class="" id="u2501_div">
		            </div>
		            <input class="u2501_input" id="u2501_input" type="text" value=""/>
		         </div>
		         <div class="ax_default text_field" id="u2502">
		            <div class="" id="u2502_div">
		            </div>
		            <input class="u2502_input" id="u2502_input" type="text" value=""/>
		         </div>
		         <div class="ax_default label" id="u2503">
		            <div class="" id="u2503_div">
		            </div>
		            <div class="text" id="u2503_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     -
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default droplist" id="u2504">
		            <div class="" id="u2504_div">
		            </div>
		            <select class="u2504_input" id="u2504_input">
		               <option class="u2504_input_option" value="DD/MM/YYYY">
		                  DD/MM/YYYY
		               </option>
		            </select>
		         </div>
		         <div class="ax_default label" id="u2505">
		            <div class="" id="u2505_div">
		            </div>
		            <div class="text" id="u2505_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Ngày học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default label" id="u2506">
		            <div class="" id="u2506_div">
		            </div>
		            <div class="text" id="u2506_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phòng học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default droplist" id="u2507">
		            <div class="" id="u2507_div">
		            </div>
		            <select class="u2507_input" id="u2507_input">
		               <option class="u2507_input_option" value="Donal">
		                  Donal
		               </option>
		               <option class="u2507_input_option" value="Doraemon">
		                  Doraemon
		               </option>
		            </select>
		         </div>
		         <div class="ax_default label" id="u2508">
		            <div class="" id="u2508_div">
		            </div>
		            <div class="text" id="u2508_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Giáo viên
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default droplist" id="u2509">
		            <div class="" id="u2509_div">
		            </div>
		            <select class="u2509_input" id="u2509_input">
		               <option class="u2509_input_option" value="Nguyễn Hồng Hà">
		                  Nguyễn Hồng Hà
		               </option>
		               <option class="u2509_input_option" value="Vũ Thị Trang">
		                  Vũ Thị Trang
		               </option>
		            </select>
		         </div>
		      </div>
		      <div class="ax_default primary_button" id="u2510">
		         <div class="" id="u2510_div">
		         </div>
		         <div class="text" id="u2510_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Lưu
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default button" id="u2511">
		         <div class="" id="u2511_div">
		         </div>
		         <div class="text" id="u2511_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Hủy
		               </span>
		            </p>
		         </div>
		      </div>
		   </div>
		</div>
		
      </div>
    );
  }
}

export default Page;
