export const HEADER = [
  {
    title: "Tên phòng",
    key: "ten_phong",
    class: "tb-width-150"
  },
  {
    title: "Loại",
    key: "loai",
    class: "tb-width-300"
  },
  {
    title: "Trung tâm",
    key: "trung_tam",
    class: "tb-width-150"
  },
  {
    title: "Số lượng tối thiểu",
    key: "so_luong_toi_thieu",
    class: "tb-width-200"
  },
  {
    title: "Số lượng tối đa",
    key: "so_luong_toi_da",
    class: "tb-width-200"
  }
];

export const LOAI_TRUNG_TAM = [
  {
    code: "1",
    text: "Trung tâm hoạt động"
  },
  {
    code: "2",
    text: "Hội sở"
  },
  {
    code: "3",
    text: "Hoàng Ngân"
  },
  {
    code: "4",
    text: "Văn Điển"
  }
];

export const DATA = [
  {
    id: "1",
    ten_phong: "Mickey",
    loai: "Phòng học",
    trung_tam: "Hoàng Ngân",
    so_luong_toi_thieu: "7",
    so_luong_toi_da: "14"
  },
  {
    id: "1",
    ten_phong: "Mickey",
    loai: "Phòng học",
    trung_tam: "Hoàng Ngân",
    so_luong_toi_thieu: "7",
    so_luong_toi_da: "14"
  },
  {
    id: "1",
    ten_phong: "Mickey",
    loai: "Phòng học",
    trung_tam: "Hoàng Ngân",
    so_luong_toi_thieu: "7",
    so_luong_toi_da: "14"
  },
  {
    id: "1",
    ten_phong: "Mickey",
    loai: "Phòng học",
    trung_tam: "Hoàng Ngân",
    so_luong_toi_thieu: "7",
    so_luong_toi_da: "14"
  },
  {
    id: "1",
    ten_phong: "Mickey",
    loai: "Phòng học",
    trung_tam: "Hoàng Ngân",
    so_luong_toi_thieu: "7",
    so_luong_toi_da: "14"
  },
  {
    id: "1",
    ten_phong: "Mickey",
    loai: "Phòng học",
    trung_tam: "Hoàng Ngân",
    so_luong_toi_thieu: "7",
    so_luong_toi_da: "14"
  },
  {
    id: "1",
    ten_phong: "Mickey",
    loai: "Phòng học",
    trung_tam: "Hoàng Ngân",
    so_luong_toi_thieu: "7",
    so_luong_toi_da: "14"
  },
  {
    id: "1",
    ten_phong: "Mickey",
    loai: "Phòng học",
    trung_tam: "Hoàng Ngân",
    so_luong_toi_thieu: "7",
    so_luong_toi_da: "14"
  },
  {
    id: "1",
    ten_phong: "Mickey",
    loai: "Phòng học",
    trung_tam: "Hoàng Ngân",
    so_luong_toi_thieu: "7",
    so_luong_toi_da: "14"
  }
];
