import React from "react";
import { Button, Modal } from "react-bootstrap";
import moment from "moment";
import { Formik } from "formik";
import { connect } from "dva";
import Input from "../../../../components/Form/Input";
import useRoom from "../../../../hooks/useRoom";
import Select from "../../../../components/Form/Select";
import DateInput from "../../../../components/Form/Date";
import Radio from "../../../../components/Form/Radio";

function CustomModal(props) {
  const handleClose = () => {
    props.onClose();
  };
  const { dispatch, onClose, isCreateNew, Room } = props;
  const { initialValues, validationSchema, submit } = useRoom({
    dispatch,
    onClose,
    isCreateNew
  });
  return (
    <Modal
      size={"lg"}
      show
      onHide={handleClose}
      backdrop="static"
      keyboard={false}
      id="AddNew"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Formik
        initialValues={!isCreateNew ? Room?.current_room : initialValues}
        validationSchema={validationSchema}
        onSubmit={submit}
      >
        {({ values, handleChange, handleBlur, handleSubmit }) => (
          <form onSubmit={handleSubmit}>
            <Modal.Header closeButton>
              <Modal.Title>
                {isCreateNew ? "Thêm mới phòng học" : "Chỉnh sửa phòng học"}
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <div className="row">
                <div className="col-12">
                  <div className="row">
                    <div className="col-md-6 ">
                      <Input
                        id="name"
                        name="name"
                        label="Tên phòng:"
                        value={values.name}
                        onChange={handleChange}
                        onBlur={handleBlur}
                      />
                      <div className="form-group">
                        <label htmlFor="loai_phong">Loại phòng:</label>
                        <div className="row">
                          {props.Room.list_loai_phong.map(t => (
                            <div className="col-6">
                              <Radio
                                id="roomType"
                                name="roomType"
                                label={t.name}
                                value={t.id}
                                onChange={handleChange}
                                onBlur={handleBlur}
                              />
                            </div>
                          ))}
                        </div>
                      </div>
                      <Select
                        id="centerId"
                        name="centerId"
                        label="Trung tâm"
                        options={props.Room.list_center}
                        value={values.centerId}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        empty
                      />
                      <Input
                        id="location"
                        name="location"
                        label="Vị trí:"
                        value={values.location}
                        onChange={handleChange}
                        onBlur={handleBlur}
                      />
                    </div>
                    <div className="col-md-6 ">
                      <div className="form-group">
                        <label>Dung lượng:</label>
                        <div className="row">
                          <div className="col-6">
                            <Input
                              id="minSize"
                              name="minSize"
                              value={values.minSize}
                              onChange={handleChange}
                              onBlur={handleBlur}
                            />
                          </div>
                          <div className="col-6">
                            <Input
                              id="maxSize"
                              name="maxSize"
                              value={values.maxSize}
                              onChange={handleChange}
                              onBlur={handleBlur}
                            />
                          </div>
                        </div>
                      </div>
                      <Select
                        id="status"
                        name="status"
                        label="Trạng thái"
                        options={props.Room.list_trang_thai}
                        value={values.status}
                        onChange={handleChange}
                        onBlur={handleBlur}
                      />
                      <div className="form-group">
                        <label>Thời gian:</label>
                        <div className="row">
                          <div className="col-6">
                            <DateInput
                              name="effectFrom"
                              value={values.effectFrom}
                              onChange={handleChange}
                              onBlur={handleBlur}
                            />
                          </div>
                          <div className="col-6">
                            <DateInput
                              name="effectTo"
                              value={values.effectTo}
                              onChange={handleChange}
                              onBlur={handleBlur}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </Modal.Body>
            <Modal.Footer>
              <Button
                type="submit"
                variant="primary"
                style={{ marginRight: 15 }}
              >
                {isCreateNew ? "Tạo mới" : "Cập nhật"}
              </Button>
              <Button type="button" variant={"secondary"} onClick={handleClose}>
                Hủy
              </Button>
            </Modal.Footer>
          </form>
        )}
      </Formik>
    </Modal>
  );
}

export default connect(({ Room }) => ({ Room }))(CustomModal);
