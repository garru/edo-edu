import React from "react";
import {connect} from "dva";
import {Button} from "react-bootstrap";
import CustomModal from "./modal";
import CustomTable from "../../../../components/Table";
import CustomTooltip from "../../../../components/CustomTooltip";
import useRoom from "../../../../hooks/useRoom";

class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id: null,
      tooltipIndex: null,
      show: false,
      target: null,
      activeIndex: null
    };
  }

  _delete = useRoom({ dispatch: this.props.dispatch })._delete

  renderTooltip(index) {
    const { tooltipIndex } = this.state;
    const { dispatch } = this.props;
    const dataSource = [{
      label: "Chỉnh sửa",
      className: "fa fa-edit",
      onClick: e => {
        dispatch({
          type: "Room/view",
          payload: { id: this.state.id }
        }).then(() => {
          this.setState({ isCreatNew: false, showModal: true});
        });
        this.handleClick(e, tooltipIndex, true);
      }
    }, {
      label: "Xoá",
      className: "fa fa-trash-alt",
      onClick: e => {
        this.handleClick(e, tooltipIndex, true);
        this._delete({id: this.state.id});
      }
    }]
    return (
      <CustomTooltip
        show={this.state.show}
        onHide={e => this.handleClick(e, tooltipIndex)}
        dataSource={dataSource}
        target={this.state.target}
      />
    );
  }

  handleClick(e, index, isHide) {
    this.setState({
      show: !(index === this.state.activeIndex),
      target: isHide ? null : e.target,
      activeIndex: index === this.state.activeIndex ? null : index
    });
  }

  onClose = () => {
    this.setState({isCreateNew: false, showModal: false});
  };

  columns = [{
      title: "Tên phòng",
      key: "name",
      class: "tb-width-150"
    },
    {
      title: "Loại",
      key: "typeName",
      class: "tb-width-300"
    },
    {
      title: "Trung tâm",
      key: "centerName",
      class: "tb-width-150"
    },
    {
      title: "Số lượng tối thiểu",
      key: "minSize",
      class: "tb-width-200"
    },
    {
      title: "Số lượng tối đa",
      key: "maxSize",
      class: "tb-width-200"
    },
    {
      title: "",
      key: "action",
      class: "tb-width-50",
      render: (col, index) => (
        <i className="fa fa-ellipsis-h" onClick={e => {
          e.preventDefault();
          this.handleClick(e, index);
          this.setState({ id: col.id, tooltipIndex: index})
        }}/>
      )
    }];

  render() {
    return (
      <div className="row">
        <div className="col-12 d-flex justify-content-end m-t-2">
          <div className="form-group">
            <Button
              variant="primary"
              type="submit"
              onClick={() =>
                this.setState({isCreateNew: true, showModal: true})
              }
            >
              Tạo mới
            </Button>
          </div>
        </div>
        <div className="col-12">
          <CustomTable
            columns={this.columns}
            dataSource={this.props.Room.list_room}
            total={this.props.Room.total_record}
            onChange={data => this.updateOptions(data)}
            checkbox
          />
        </div>
        {this.state.showModal && (
          <CustomModal
            isCreateNew={this.state.isCreateNew}
            onClose={this.onClose}
            selectedItem={this.state.selectedItem}
            Center={this.props.Center}
          />
        )}
        {this.renderTooltip()}
      </div>
    );
  }
}

export default connect(({Room, Center}) => ({
  Room,
  Center
}))(Page);
