import React from "react";
import {connect} from "dva";
import {Button} from "react-bootstrap";
import { Formik } from "formik";
import useRoom from "../../../../hooks/useRoom";
import Input from "../../../../components/Form/Input";
import Select from "../../../../components/Form/Select";

const RoomHeader = (props) => {
  const { search } = useRoom({ dispatch: props.dispatch });
  return (
    <fieldset>
      <legend>Tìm kiếm</legend>
      <Formik
        initialValues={{}}
        onSubmit={(values) => {
          search(values);
        }}>
        {
          ({
             values, handleSubmit, handleChange, handleBlur
           }) => (
            <form onSubmit={handleSubmit}>
              <div className="row align-end">
                <div className="col-md-3 col-sm-6 col-xs-12">
                  <Input
                    id="name"
                    name="name"
                    label="Tên phòng:"
                    value={values.keyword}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                </div>
                <div className="col-md-3 col-sm-6 col-xs-12">
                  <Select
                    id="center_Id"
                    name="center_Id"
                    label="Trung tâm"
                    options={props.Center.list_center}
                    value={values.center_Id}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    empty
                  />
                </div>
                <div className="col-md-3 col-sm-12 col-xs-12 text-right">
                  <div className="form-group m-0">
                    <Button type="submit" variant="primary">
                      Tìm kiếm
                    </Button>
                  </div>
                </div>
              </div>
            </form>
          )
        }
      </Formik>
    </fieldset>
  );
}

export default connect(({Center}) => ({
  Center
}))(RoomHeader);
