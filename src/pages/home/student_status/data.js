export const TITLE = "Trạng thái học viên";
export const DESCRIPTION = "Các trạng thái của học viên theo buổi học hoặc khóa học";

export const HEADER = [
  {
    title: "Trạng thái điểm danh",
    key: "trang_thai_diem_danh",
    class: "tb-width-300"
  },
  {
    title: "Tất toán",
    key: "tat_toan",
    class: "tb-width-100"
  },
  {
    title: "Áp dụng",
    key: "ap_dung",
    class: "tb-width-150"
  }
];


export const DATA = [
  {
    id: "1",
    trang_thai_diem_danh: "Chưa điểm danh",
    tat_toan: true,
    ap_dung: "Buổi học"
  },
  {
    id: "1",
    trang_thai_diem_danh: "Chưa điểm danh",
    tat_toan: true,
    ap_dung: "Buổi học"
  },
  {
    id: "1",
    trang_thai_diem_danh: "Chưa điểm danh",
    tat_toan: true,
    ap_dung: "Buổi học"
  },
  {
    id: "1",
    trang_thai_diem_danh: "Chưa điểm danh",
    tat_toan: true,
    ap_dung: "Buổi học"
  },
  {
    id: "1",
    trang_thai_diem_danh: "Chưa điểm danh",
    tat_toan: true,
    ap_dung: "Buổi học"
  },
];
