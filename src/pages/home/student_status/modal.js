import React from "react";
import {Button, Modal} from "react-bootstrap";
import { Formik } from "formik";
import Input from "../../../components/Form/Input";
import useStudentStatus from "../../../hooks/useStudentStatus";
import {connect} from "dva";
import Radio from "../../../components/Form/Radio";
import Checkbox from "../../../components/Form/Checkbox";

function CustomModal(props) {
  const handleClose = () => {
    // setShow(false);
    props.onClose();
  };
  const { isCreateNew, onClose, StudentStatus } = props;
  const { initialValues, validationSchema, submit } = useStudentStatus({ dispatch: props.dispatch, isCreateNew, onClose })
  return (
    <Modal
      size={"md"}
      show
      onHide={handleClose}
      backdrop="static"
      keyboard={false}
      id="AddNew"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Formik
        initialValues={!isCreateNew ? StudentStatus?.current_student_status : initialValues}
        validationSchema={validationSchema}
        onSubmit={submit}
      >
        {({
            values, handleChange, handleBlur, handleSubmit
          }) => (
          <form onSubmit={handleSubmit}>
            <Modal.Header closeButton>
              <Modal.Title>
                {isCreateNew
                  ? "Thêm mới trạng thái học viên"
                  : "Chỉnh sửa trạng thái học viên"}
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <div>
                <form>
                  <div className="row">
                    <div className="col-12">
                      <Input
                        id="name"
                        name="name"
                        label="Tên trạng thái:"
                        value={values.name}
                        onChange={handleChange}
                        onBlur={handleBlur}
                      />
                      <div className="row">
                        <div className="col-4">
                          <label>Áp dụng cho:</label>
                        </div>
                        <div className="col-8">
                          <div className="row">
                            {props.StudentStatus.list_danh_muc_ap_dung_cho.map(t => (
                              <div className="col-6">
                                <Radio
                                  id={`applyFor_${t.id}`}
                                  name="applyFor"
                                  label={t.name}
                                  value={t.id}
                                  onChange={handleChange}
                                  onBlur={handleBlur}
                                />
                              </div>
                            ))}
                          </div>
                        </div>
                      </div>
                      <Checkbox
                        id="isFinal"
                        name="isFinal"
                        label="Tất toán khóa học/ buổi học"
                        value={values.isFinal}
                        onChange={handleChange}
                        onBlur={handleBlur}
                      />
                    </div>
                  </div>
                </form>
              </div>
            </Modal.Body>
            <Modal.Footer>
              <Button type="submit" variant="primary" style={{ marginRight: 15 }}>
                {isCreateNew ? "Tạo mới" : "Cập nhật"}
              </Button>
              <Button type="button" variant={"secondary"} onClick={handleClose}>
                Hủy
              </Button>
            </Modal.Footer>
          </form>
        )
        }
      </Formik>
    </Modal>
  );
}
export default connect(
  ({ StudentStatus }) => ({ StudentStatus })
)(CustomModal);