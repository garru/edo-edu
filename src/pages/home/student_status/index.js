import React from "react";
import {connect} from "dva";
import {Button} from "react-bootstrap";
import CustomModal from "./modal";
import PageHeader from "../../../components/PageHeader";
import CustomTable from "../../../components/Table";
import CustomTooltip from "../../../components/CustomTooltip";
import useStudentStatus from "../../../hooks/useStudentStatus";

class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id: null,
      tooltipIndex: null,
      show: false,
      target: null,
      activeIndex: null
    };
  }

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: "StudentStatus/list"
    });
    dispatch({
      type: "StudentStatus/applyfor"
    })
  }

  _delete = useStudentStatus({ dispatch: this.props.dispatch })._delete

  renderTooltip(index) {
    const { tooltipIndex } = this.state;
    const { dispatch } = this.props;
    const dataSource = [{
      label: "Chỉnh sửa",
      className: "fa fa-edit",
      onClick: e => {
        dispatch({
          type: "StudentStatus/view",
          payload: { id: this.state.id }
        }).then(() => {
          this.setState({ isCreatNew: false, showModal: true});
        });
        this.handleClick(e, tooltipIndex, true);
      }
    }, {
      label: "Xoá",
      className: "fa fa-trash-alt",
      onClick: e => {
        this.handleClick(e, tooltipIndex, true);
        this._delete({id: this.state.id});
      }
    }]
    return (
      <CustomTooltip
        show={this.state.show}
        onHide={e => this.handleClick(e, tooltipIndex)}
        dataSource={dataSource}
        target={this.state.target}
      />
    );
  }

  handleClick(e, index, isHide) {
    this.setState({
      show: !(index === this.state.activeIndex),
      target: isHide ? null : e.target,
      activeIndex: index === this.state.activeIndex ? null : index
    });
  }

  onClose = () => {
    this.setState({ isCreateNew: false, showModal: false });
  };

  columns = [
    {
      title: "Trạng thái điểm danh",
      key: "name",
      class: "tb-width-300"
    },
    {
      title: "Tất toán",
      key: "isFinal",
      class: "tb-width-100"
    },
    {
      title: "Áp dụng",
      key: "applyFor",
      class: "tb-width-150"
    },
    {
      title: "",
      key: "action",
      class: "tb-width-50",
      render: (col, index) => (
        <i className="fa fa-ellipsis-h" onClick={e => {
          e.preventDefault();
          this.handleClick(e, index);
          this.setState({ id: col.id, tooltipIndex: index})
        }}/>
      )
    }
  ]

  render() {
    return (
      <>
        <PageHeader
          title="Trạng thái học viên"
          subTitle=""
          breadcrums={[{
            title: "Home",
            path: "/"
          }, {
            title: "Trạng thái học viên",
            path: "/student_status"
          }]}
        />
        <div className="iq-card">
          <div className="iq-card-body">
            <div className="row">
              <div className="col-12 text-right">
                <div className="form-group">
                  <Button
                    variant="primary"
                    type="submit"
                    onClick={() =>
                      this.setState({ isCreateNew: true, showModal: true })
                    }
                  >
                    Tạo mới
                  </Button>
                </div>
              </div>
              <div className="col-12">
                <CustomTable
                  columns={this.columns}
                  dataSource={this.props.StudentStatus.list_student_status}
                  total={this.props.StudentStatus.total_record}
                  onChange={data => this.updateOptions(data)}
                  checkbox
                />
              </div>
            </div>
          </div>
          {this.state.showModal && (
            <CustomModal
              isCreateNew={this.state.isCreateNew}
              onClose={this.onClose}
              selectedItem={this.state.selectedItem}
            />
          )}
          {this.renderTooltip()}
        </div>
      </>
    );
  }
}

export default connect(({ StudentStatus }) => ({
  StudentStatus
}))(Page);
