import React from "react";
import { connect } from "dva";
import "react-datepicker/dist/react-datepicker.css";
import { Modal, Button, Table } from "react-bootstrap";
import { TRUNG_TAM } from "../../../mock/dropdown";
import Rating from "react-rating";

import {
  HEADER_MODAL_CHI_TIET_NHAN_XET,
  DATA_MODAL_CHI_TIET_NHAN_XET,
  HEADER_TONG_QUAN,
  DATA_TONG_QUAN
} from "./data";
import PageHeader from "../../../components/PageHeader";
class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  onClear = () => {
    this.setState({});
  };

  componentWillMount() {}

  render() {
    const bre = {
      title: "Thực hiện dự giờ - đánh giá",
      breadcrums: [
        {
          title: "Home",
          path: "/"
        },
        {
          title: "Thực hiện dự giờ - đánh giá",
          path: ""
        }
      ]
    };
    console.log("render tiep_nhan>>>");
    return (
      <div>
        <PageHeader {...bre}></PageHeader>
        <div className="iq-card">
          <div className=" iq-card-header d-flex justify-content-between">
            <div className="iq-header-title">
              <h6>
                <b>Thông tin chung</b>
              </h6>
            </div>
          </div>
          <div className="iq-card-body">
            <div className="row">
              <div className="col-6">
                <div className="form-group">
                  <label htmlFor="giao_vien">Giáo viên:</label>
                  <input
                    type="text"
                    name="giao_vien"
                    className="form-control form-control-lg"
                    id="giao_vien"
                    placeholder="Nhập tên giáo viên"
                    // ref={register({ required: true, minLength: 8 })}
                  ></input>
                </div>
              </div>
              <div className="col-6">
                <div className="form-group">
                  <label htmlFor="trung_tam">Trung tâm:</label>
                  <select
                    className="form-control form-control-lg"
                    id="trung_tam"
                    name="trung_tam"
                  >
                    {TRUNG_TAM.map(item => (
                      <option
                        value={item.code}
                        disabled={item.code !== "" ? false : true}
                        selected={item.code !== "" ? false : true}
                        hidden={item.code !== "" ? false : true}
                      >
                        {item.text}
                      </option>
                    ))}
                  </select>
                </div>
              </div>
              <div className="col-6">
                <div className="form-group">
                  <label htmlFor="nguoi_tham_gia">Người tham gia :</label>
                  <select
                    className="form-control form-control-lg"
                    id="nguoi_tham_gia"
                    name="nguoi_tham_gia"
                  >
                    {TRUNG_TAM.map(item => (
                      <option
                        value={item.code}
                        disabled={item.code !== "" ? false : true}
                        selected={item.code !== "" ? false : true}
                        hidden={item.code !== "" ? false : true}
                      >
                        {item.text}
                      </option>
                    ))}
                  </select>
                </div>
              </div>
              <div className="col-6">
                <div className="form-group">
                  <label htmlFor="lop">Lớp:</label>
                  <select
                    className="form-control form-control-lg"
                    id="lop"
                    name="lop"
                  >
                    {TRUNG_TAM.map(item => (
                      <option
                        value={item.code}
                        disabled={item.code !== "" ? false : true}
                        selected={item.code !== "" ? false : true}
                        hidden={item.code !== "" ? false : true}
                      >
                        {item.text}
                      </option>
                    ))}
                  </select>
                </div>
              </div>
              <div className="col-6">
                <div className="form-group">
                  <label htmlFor="ngay_thuc_hien">Ngày thực hiện:</label>
                  <select
                    className="form-control form-control-lg"
                    id="ngay_thuc_hien"
                    name="ngay_thuc_hien"
                  >
                    {TRUNG_TAM.map(item => (
                      <option
                        value={item.code}
                        disabled={item.code !== "" ? false : true}
                        selected={item.code !== "" ? false : true}
                        hidden={item.code !== "" ? false : true}
                      >
                        {item.text}
                      </option>
                    ))}
                  </select>
                </div>
              </div>

              <div className="col-6">
                <div className="form-group">
                  <label htmlFor="loai_hinh_danh_gia">
                    Loại hình đánh giá:
                  </label>
                  <select
                    className="form-control form-control-lg"
                    id="loai_hinh_danh_gia"
                    name="loai_hinh_danh_gia"
                  >
                    {TRUNG_TAM.map(item => (
                      <option
                        value={item.code}
                        disabled={item.code !== "" ? false : true}
                        selected={item.code !== "" ? false : true}
                        hidden={item.code !== "" ? false : true}
                      >
                        {item.text}
                      </option>
                    ))}
                  </select>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="iq-card">
          <div className=" iq-card-header d-flex justify-content-between">
            <div className="iq-header-title">
              <h6>
                <b>Tiêu chí đánh giá</b>
              </h6>
            </div>
          </div>
          <div className="iq-card-body">
            <div className="row">
              <div className="col-12">
                <div style={{ width: "100%" }}>
                  <div style={{ overflowX: "auto", width: "100%" }}>
                    <Table
                      bordered
                      hover
                      // style={{ minWidth: 1500 }}
                      className="table m-0"
                    >
                      <thead>
                        <tr>
                          <th></th>
                          {HEADER_TONG_QUAN.map(item => (
                            <th
                              className={`${item.class} ${
                                item.hasSort ? "sort" : ""
                              } ${
                                this.state.sortField === item.key
                                  ? "active"
                                  : ""
                              }`}
                              key={item.key}
                            >
                              <span>{item.title}</span>
                            </th>
                          ))}
                        </tr>
                      </thead>
                      <tbody className="text-left">
                        {DATA_TONG_QUAN.map((item, index) => (
                          <React.Fragment>
                            <tr key={`account_${item.id}`}>
                              <td>
                                {!item.isParent ? (
                                  <span>{item.id}</span>
                                ) : (
                                  <span>
                                    <b>{item.id}</b>
                                  </span>
                                )}
                              </td>
                              <td
                                style={{
                                  paddingLeft: item.isParent ? "" : 30
                                }}
                              >
                                {!item.isParent ? (
                                  <span>{item.tieu_chi}</span>
                                ) : (
                                  <span>
                                    <b>{item.tieu_chi}</b>
                                  </span>
                                )}
                              </td>
                              <td style={{ textAlign: "center" }}>
                                {!item.isParent ? (
                                  <span>{item.diem_so}</span>
                                ) : (
                                  <span>
                                    <b>{item.diem_so}</b>
                                  </span>
                                )}
                              </td>
                              <td>
                                {!item.isParent ? (
                                  <span>{item.ghi_chu}</span>
                                ) : (
                                  <span>
                                    <b>{item.ghi_chu}</b>
                                  </span>
                                )}
                              </td>
                            </tr>
                          </React.Fragment>
                        ))}
                      </tbody>
                    </Table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="iq-card">
          <div className=" iq-card-header d-flex justify-content-between">
            <div className="iq-header-title">
              <h6>
                <b>Tổng kết</b>
              </h6>
            </div>
          </div>
          <div className="iq-card-body">
            <div className="row">
              <div className="col-12">
                <div style={{ width: "100%" }}>
                  <div style={{ overflowX: "auto", width: "100%" }}>
                    <Table
                      bordered
                      hover
                      // style={{ minWidth: 1500 }}
                      className="table m-0"
                    >
                      <thead>
                        <tr>
                          {HEADER_MODAL_CHI_TIET_NHAN_XET.map(item => (
                            <th
                              className={`${item.class} ${
                                item.hasSort ? "sort" : ""
                              } ${
                                this.state.sortField === item.key
                                  ? "active"
                                  : ""
                              }`}
                              key={item.key}
                            >
                              <span>{item.title}</span>
                            </th>
                          ))}
                        </tr>
                      </thead>
                      <tbody className="text-left">
                        {DATA_MODAL_CHI_TIET_NHAN_XET.map((item, index) => (
                          <React.Fragment>
                            <tr key={`account_${item.id}`}>
                              <td>
                                <b>{item.id}</b>
                              </td>
                              <td>
                                <b>{item.hang_muc}</b>
                              </td>
                              <td>{item.ket_qua}</td>
                            </tr>
                          </React.Fragment>
                        ))}
                        <tr>
                          <td>
                            <b>#</b>
                          </td>
                          <td>
                            <b>Total</b>
                          </td>
                          <td>15/20</td>
                        </tr>
                      </tbody>
                    </Table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="iq-card">
          <div className=" iq-card-header d-flex justify-content-between">
            <div className="iq-header-title">
              <h6>
                <b>Kết quả tổng hợp</b>
              </h6>
            </div>
          </div>
          <div className="iq-card-body">
            <div className="row">
              <div className="col-2">Đánh giá</div>
              <div className="col-10">
                <Rating
                  className="rating"
                  emptySymbol="far fa-star"
                  fullSymbol="fa fa-star"
                  fractions={2}
                  readonly={true}
                  initialRating={4}
                />
              </div>

              <div className="m-t-2 col-2">Điểm tốt</div>
              <div className="m-t-2 col-10">
                <textarea
                  className="textarea form-control"
                  rows="4"
                  name="diem_tot"
                ></textarea>
              </div>
              <div className="m-t-2 col-2">Cần cải tiến</div>
              <div className="m-t-2 col-10">
                <textarea
                  className="textarea form-control"
                  rows="4"
                  name="can_cai_tien"
                ></textarea>
              </div>
              <div className="m-t-2 col-2">Nội dung khác</div>
              <div className="m-t-2 col-10">
                <textarea
                  className="textarea form-control"
                  rows="4"
                  name="noi_dung_khac"
                ></textarea>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(({ Mock }) => ({
  Mock
}))(Page);
