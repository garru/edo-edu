export const HEADER = [
  {
    title: "Đơn giá buổi",
    key: "don_gia_buoi",
    class: "tb-width-200"
  },
  {
    title: "Thời gian áp dụng",
    key: "thoi_gian_ap_dung",
    class: "tb-width-200"
  },
  {
    title: "Thời gian kết thúc",
    key: "thoi_gian_ket_thuc",
    class: "tb-width-200"
  },
  {
    title: "Người chỉnh sửa",
    key: "nguoi_chinh_sua",
    class: "tb-width-200"
  }
];

export const DATA = [
  {
    don_gia_buoi: "200.000",
    thoi_gian_ap_dung: "21/1/2021",
    thoi_gian_ket_thuc: "21/1/2022",
    nguoi_chinh_sua: "Nguyen Van A"
  },
  {
    don_gia_buoi: "200.000",
    thoi_gian_ap_dung: "21/1/2021",
    thoi_gian_ket_thuc: "21/1/2022",
    nguoi_chinh_sua: "Nguyen Van A"
  },
  {
    don_gia_buoi: "200.000",
    thoi_gian_ap_dung: "21/1/2021",
    thoi_gian_ket_thuc: "21/1/2022",
    nguoi_chinh_sua: "Nguyen Van A"
  },
  {
    don_gia_buoi: "200.000",
    thoi_gian_ap_dung: "21/1/2021",
    thoi_gian_ket_thuc: "21/1/2022",
    nguoi_chinh_sua: "Nguyen Van A"
  }
];

export const HEADER_TONG_QUAN = [
  {
    title: "Tiêu chí",
    key: "tieu_chi",
    class: "tb-width-300"
  },
  {
    title: "Điểm số",
    key: "diem_so",
    class: "tb-width-100"
  },
  {
    title: "Ghi chú",
    key: "ghi_chu",
    class: "tb-width-300"
  }
];

export const DATA_TONG_QUAN = [
  {
    id: "1",
    tieu_chi: "It is a long established fact that a reader will be distracted",
    diem_so: "14.5",
    ghi_chu: "",
    isParent: true
  },
  {
    id: "1.1",
    tieu_chi:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been",
    diem_so: "4.5",
    ghi_chu:
      "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC",
    isParent: false
  },
  {
    id: "1.1",
    tieu_chi:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been",
    diem_so: "4.5",
    ghi_chu:
      "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC",
    isParent: false
  },
  {
    id: "1.1",
    tieu_chi:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been",
    diem_so: "4.5",
    ghi_chu:
      "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC",
    isParent: false
  },
  {
    id: "2",
    tieu_chi: "It is a long established fact that a reader will be distracted",
    diem_so: "14.5",
    ghi_chu: "",
    isParent: true
  },
  {
    id: "2.1",
    tieu_chi:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been",
    diem_so: "4.5",
    ghi_chu:
      "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC",
    isParent: false
  },
  {
    id: "2.1",
    tieu_chi:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been",
    diem_so: "4.5",
    ghi_chu:
      "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC",
    isParent: false
  },
  {
    id: "2.1",
    tieu_chi:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been",
    diem_so: "4.5",
    ghi_chu:
      "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC",
    isParent: false
  }
];

export const HEADER_LICH_SU_NHAN_XET_DU_GIO = [
  {
    title: "No",
    key: "id",
    class: "tb-width-50"
  },
  {
    title: "Ngày thực hiện",
    key: "ngay_thuc_hien",
    class: "tb-width-150"
  },
  {
    title: "Loại",
    key: "loai",
    class: "tb-width-150"
  },
  {
    title: "Người thực hiện",
    key: "nguoi_thuc_hien",
    class: "tb-width-200"
  },
  {
    title: "Kết quả đánh giá chung",
    key: "rating",
    class: "tb-width-200"
  },
  {
    title: "",
    key: "action",
    class: "tb-width-50"
  }
];

export const DATA_LICH_SU_NHAN_XET_DU_GIO = [
  {
    id: "1",
    ngay_thuc_hien: "20/01/2021",
    loai: "Dự giờ nhanh",
    nguoi_thuc_hien: "Nguyễn Minh Trang",
    rating: "3.5"
  },
  {
    id: "1",
    ngay_thuc_hien: "20/01/2021",
    loai: "Dự giờ nhanh",
    nguoi_thuc_hien: "Nguyễn Minh Trang",
    rating: "3.5"
  },
  {
    id: "1",
    ngay_thuc_hien: "20/01/2021",
    loai: "Dự giờ nhanh",
    nguoi_thuc_hien: "Nguyễn Minh Trang",
    rating: "3.5"
  }
];

export const HEADER_LICH_NHAN_XET_DU_GIO = [
  {
    title: "No",
    key: "id",
    class: "tb-width-50"
  },
  {
    title: "Ngày thực hiện",
    key: "ngay_thuc_hien",
    class: "tb-width-150"
  },
  {
    title: "Loại",
    key: "loai",
    class: "tb-width-150"
  },
  {
    title: "Người thực hiện",
    key: "nguoi_thuc_hien",
    class: "tb-width-200"
  },
  {
    title: "Trạng thái",
    key: "trang_thai",
    class: "tb-width-200"
  },
  {
    title: "",
    key: "action",
    class: "tb-width-50"
  }
];

export const DATA_LICH_NHAN_XET_DU_GIO = [
  {
    id: "1",
    ngay_thuc_hien: "20/01/2021",
    loai: "Dự giờ nhanh",
    nguoi_thuc_hien: "Nguyễn Minh Trang",
    trang_thai: "Chưa thực hiện"
  },
  {
    id: "1",
    ngay_thuc_hien: "20/01/2021",
    loai: "Dự giờ nhanh",
    nguoi_thuc_hien: "Nguyễn Minh Trang",
    trang_thai: "Chưa thực hiện"
  },
  {
    id: "1",
    ngay_thuc_hien: "20/01/2021",
    loai: "Dự giờ nhanh",
    nguoi_thuc_hien: "Nguyễn Minh Trang",
    trang_thai: "Chưa thực hiện"
  }
];

export const HEADER_NHAN_XET_TU_PHU_HUYNH = [
  {
    title: "No",
    key: "id",
    class: "tb-width-50"
  },
  {
    title: "Họ tên",
    key: "ho_ten",
    class: "tb-width-200"
  },
  {
    title: "Phụ huynh của",
    key: "phu_huynh_cua",
    class: "tb-width-200"
  },
  {
    title: "Lớp",
    key: "lop",
    class: "tb-width-100"
  },
  {
    title: "Cơ sở",
    key: "co_so",
    class: "tb-width-200"
  },
  {
    title: "Ngày thực hiện",
    key: "ngay_thuc_hien",
    class: "tb-width-150"
  },
  {
    title: "",
    key: "action",
    class: "tb-width-50"
  }
];

export const DATA_NHAN_XET_TU_PHU_HUYNH = [
  {
    id: "1",
    ho_ten: "Hoàng Duy Nguyễn",
    phu_huynh_cua: "Hoàng Duy Phong",
    lop: "Lớp A",
    co_so: "Hoàng Ngân",
    ngay_thuc_hien: "12/2/2021"
  },
  {
    id: "1",
    ho_ten: "Hoàng Duy Nguyễn",
    phu_huynh_cua: "Hoàng Duy Phong",
    lop: "Lớp A",
    co_so: "Hoàng Ngân",
    ngay_thuc_hien: "12/2/2021"
  },
  {
    id: "1",
    ho_ten: "Hoàng Duy Nguyễn",
    phu_huynh_cua: "Hoàng Duy Phong",
    lop: "Lớp A",
    co_so: "Hoàng Ngân",
    ngay_thuc_hien: "12/2/2021"
  },
  {
    id: "1",
    ho_ten: "Hoàng Duy Nguyễn",
    phu_huynh_cua: "Hoàng Duy Phong",
    lop: "Lớp A",
    co_so: "Hoàng Ngân",
    ngay_thuc_hien: "12/2/2021"
  }
];

export const HEADER_MODAL_CHI_TIET_NHAN_XET = [
  {
    title: "No",
    key: "id",
    class: "tb-width-50"
  },
  {
    title: "Hạng mục",
    key: "hang_muc",
    class: "tb-width-500"
  },
  {
    title: "Kết quả",
    key: "ket_qua",
    class: "tb-width-100"
  }
];

export const DATA_MODAL_CHI_TIET_NHAN_XET = [
  {
    id: 1,
    hang_muc: "It is a long established fact that a reader will be distracted",
    ket_qua: "9/15"
  },
  {
    id: 1,
    hang_muc: "It is a long established fact that a reader will be distracted",
    ket_qua: "9/15"
  },
  {
    id: 1,
    hang_muc: "It is a long established fact that a reader will be distracted",
    ket_qua: "9/15"
  },
  {
    id: 1,
    hang_muc: "It is a long established fact that a reader will be distracted",
    ket_qua: "9/15"
  },
];
