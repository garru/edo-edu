import React from "react";
import {connect} from "dva";
import {Button} from "react-bootstrap";
import { Formik } from "formik";
import Input from "../../../../components/Form/Input";
import Select from "../../../../components/Form/Select";
import useCoursePrice from "../../../../hooks/useCoursePrice";

const ProgramHeader = (props) => {
  const { search } = useCoursePrice({ dispatch: props.dispatch });
  return (
    <fieldset>
      <legend>Tìm kiếm</legend>
      <Formik
        initialValues={{keyword: "", center_Id: ""}}
        onSubmit={(values) => {
          search(values);
        }}>
        {
          ({
             values, handleSubmit, handleChange, handleBlur
           }) => (
            <form onSubmit={handleSubmit}>
              <div className="row">
                <div className="col-12">
                  <div className="row align-end">
                    <div className="col-md-3">
                      <Input
                        id="keyword"
                        name="keyword"
                        label="Thông tin"
                        placeholder="Nhập thông tin tìm kiếm"
                        value={values.keyword}
                        onChange={handleChange}
                        onBlur={handleBlur}
                      />
                    </div>
                    <div className="col-md-3">
                      <Select
                        id="center_Id"
                        name="center_Id"
                        label="Trung tâm"
                        options={props.Center.list_center}
                        value={values.center_Id}
                        onChange={handleChange}
                        onBlur={handleBlur}
                      />
                    </div>
                    <div className="col-2">
                      <div className="form-group">
                        <Button variant="primary" type="submit">
                          Tìm kiếm
                        </Button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          )
        }
      </Formik>
    </fieldset>

  );
}

export default connect(({CoursePrice, Center}) => ({
  CoursePrice,
  Center
}))(ProgramHeader);
