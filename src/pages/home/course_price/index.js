import React from "react";
import { connect } from "dva";
import CoursePriceHeader from "./course_price_header";
import CoursePriceBody from "./course_price_body";
import useCoursePrice from "../../../hooks/useCoursePrice";
import PageHeader from "../../../components/PageHeader";

class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  hooks = useCoursePrice({ dispatch: this.props.dispatch });
  componentWillMount() {
    this.hooks.list();
    this.hooks.listDependencies();
  }
  render() {
    return (
      <React.Fragment>
        <PageHeader
          title="Quản lý học phí"
          subTitle="Quản lý các gói học phí trong chuỗi trung tâm"
          breadcrums={[{
            title: "Home",
            path: "/"
          },
          {
            title: "Quản lý học phí",
            path: "/course_price"
          }]}
        />
        <div className="iq-card no-shadow pb-0">
          <div className="iq-card-body">
            <div className="bg-white border-radius">
              <CoursePriceHeader />
              <CoursePriceBody />
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default connect(({ CoursePrice }) => ({
  CoursePrice
}))(Page);
