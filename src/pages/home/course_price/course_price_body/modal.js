import React, {useState} from "react";
import {Button, Modal} from "react-bootstrap";
import MultiSelect from "react-multi-select-component";
import {ErrorMessage, Formik} from "formik";
import {SketchPicker} from "react-color";
import useCoursePrice from "../../../../hooks/useCoursePrice";
import Input from "../../../../components/Form/Input";
import TextArea from "../../../../components/Form/TextArea";
import Select from "../../../../components/Form/Select";
import Checkbox from "../../../../components/Form/Checkbox";
import Radio from "../../../../components/Form/Radio";
import {connect} from "dva";

function CustomModal(props) {
  const handleClose = () => {
    // setShow(false);
    props.onClose();
  };
  const [showColor, setShowColor] = useState(false);
  const [background, setbackGround] = useState("#af6e6e");
  const {isCreateNew} = props;
  const {initialValues, validationSchema, submit} = useCoursePrice({
    dispatch: props.dispatch,
    onClose: props.onClose,
    isCreateNew
  })

  const handleChangeComplete = (color, setFieldValue) => {
    setbackGround(color.hex);
    setFieldValue("colorCode", color.hex);
  };

  function handleMultiSelectChange(values, setFieldValue, setFieldTouched) {
    setFieldValue("centerIds", values.map(center => center.value));
    // setFieldTouched("centerIds", true, true);
  }

  return (
    <Modal
      size={"lg"}
      show
      onHide={handleClose}
      backdrop="static"
      keyboard={false}
      id="AddNew"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Formik
        initialValues={!isCreateNew ? props.CoursePrice?.current_cource_price : initialValues}
        validationSchema={validationSchema}
        onSubmit={submit}
      >
        {({
            values, handleChange, handleBlur, handleSubmit, setFieldValue, setFieldTouched
          }) => (
          <form onSubmit={handleSubmit}>
            <Modal.Header closeButton>
              <Modal.Title>
                {isCreateNew ? "Thêm mới gói học phí" : "Chỉnh sửa gói học phí"}
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <div>
                <div className="iq-card no-shadow">
                  <div className="iq-card-body">
                    <div className="row m-b-2">
                      <div className="col-12">
                        <div className="row font-size-14">
                          <div className="col-6">
                            <div className="form-group">
                              <label htmlFor="ten">Cơ sở:</label>
                              <MultiSelect
                                options={
                                  (props.CoursePrice.list_center || [])
                                    .map(center => ({label: center.name, value: center.id}))
                                }
                                value={
                                  (props.CoursePrice.list_center || [])
                                    .map(center => ({label: center.name, value: center.id}))
                                    .filter(center => (values.centerIds || []).includes(center.value))
                                }
                                onChange={values => handleMultiSelectChange(values, setFieldValue, setFieldTouched)}
                                labelledBy="Select"
                                overrideStrings={{
                                  allItemsAreSelected: "Tất cả",
                                  clearSearch: "Clear Search",
                                  noOptions: "Không tìm thấy",
                                  search: "Search",
                                  selectAll: "Select All",
                                  selectSomeItems: "Có thể chọn nhiều cơ sở"
                                }}
                                ArrowRenderer={() => (
                                  <i className="fa fa-chevron-down"/>
                                )}
                              />
                              <ErrorMessage component="p" className="error-message" name="centerIds" />
                            </div>
                            <Input
                              id="name"
                              name="name"
                              label="Tên gói học phí:"
                              value={values.name}
                              onChange={handleChange}
                              onBlur={handleBlur}
                            />
                          </div>
                          <div className="col-6 ">
                            <TextArea
                              id="description"
                              name="description"
                              label="Mô tả"
                              value={values.description}
                              onChange={handleChange}
                              onBlur={handleBlur}
                            />
                          </div>
                          <div className="col-4">
                            <Select
                              id="programId"
                              name="programId"
                              label="Giáo trình dự tính"
                              options={props.CoursePrice.list_chuong_trinh}
                              value={values.programId}
                              onChange={handleChange}
                              onBlur={handleBlur}
                            />
                          </div>
                          <div className="col-2">
                            <Input
                              id="so_buoi_du_kien"
                              name="so_buoi_du_kien"
                              label="Số buổi:"
                              value={values.so_buoi_du_kien}
                              onChange={handleChange}
                              onBlur={handleBlur}
                            />
                          </div>
                          <div className="col-6 ">
                            <div className="form-group">
                              <label htmlFor="mau_sac">Mầu sắc:</label>
                              <div
                                style={{
                                  width: 50,
                                  height: 36,
                                  backgroundColor: background,
                                  cursor: "pointer"
                                }}
                                onClick={() => setShowColor(true)}
                              />
                              {showColor ? (
                                <div className="color-popover">
                                  <div
                                    className="color-block"
                                    onClick={() => setShowColor(false)}
                                  />
                                  <SketchPicker
                                    color={background}
                                    onChangeComplete={color => handleChangeComplete(color, setFieldValue)}
                                  />
                                </div>
                              ) : null}
                            </div>
                          </div>
                          <div className="col-6">
                            <Input
                              id="amount"
                              name="amount"
                              label="Số tiền:"
                              value={values.amount}
                              onChange={handleChange}
                              onBlur={handleBlur}
                            />
                            <div className="form-group">
                              <div className="p-0">
                                <label>Hình thức thu:</label>
                              </div>
                              <div className="row">

                                {(props.CoursePrice.list_hinh_thuc_thu || []).map(item => (
                                  <div className="col-6">
                                    <Radio
                                      id={`collectType-${item.id}`}
                                      name="collectType"
                                      value={item.id}
                                      label={item.name}
                                      onChange={handleChange}
                                      onBlur={handleBlur}
                                    />
                                  </div>
                                ))}
                              </div>
                            </div>
                          </div>
                          <div className="col-6">
                            <Checkbox
                              id="status"
                              name="status"
                              label="Hoạt động"
                              value={values.status}
                              onChange={handleChange}
                              onBlur={handleBlur}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </Modal.Body>
            <Modal.Footer>
              <Button type="submit" variant="primary" style={{marginRight: 15}}>
                {isCreateNew ? "Tạo mới" : "Cập nhật"}
              </Button>
              <Button variant={"secondary"} onClick={handleClose}>
                Hủy
              </Button>
            </Modal.Footer>
          </form>
        )
        }
      </Formik>
    </Modal>
  );
}

export default connect(({CoursePrice}) => ({
  CoursePrice
}))(CustomModal);