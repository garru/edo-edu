import React from "react";
import {connect} from "dva";
import {Button} from "react-bootstrap";
import CustomModal from "./modal";
import CustomTable from "../../../../components/Table";
import CustomTooltip from "../../../../components/CustomTooltip";
import CoursePrice from "../../../../models/CoursePrice";

class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id: null,
      tooltipIndex: null,
      show: false,
      target: null,
      activeIndex: null
    };
  }

  columns = [{
      title: "Tên khoá học",
      key: "name",
      class: "tb-width-400"
    },
    {
      title: "Trung tâm",
      key: "centerName",
      class: "tb-width-200"
    },
    {
      title: "Giáo viên nước ngoài",
      key: "foreignRate",
      class: "tb-width-200"
    },
    {
      title: "Giá tiền",
      key: "amount",
      class: "tb-width-150"
    },
    {
      title: "Kiểu thu",
      key: "collectName",
      class: "tb-width-150"
    },
    {
      title: "Số buổi dự kiến",
      key: "lessonCount",
      class: "tb-width-150"
    },
    {
      title: "Đơn giá trên buổi",
      key: "lessonAmount",
      class: "tb-width-150"
    },
    {
      title: "Trạng thái",
      key: "isActive",
      class: "tb-width-150"
    }, {
      title: "",
      key: "action",
      class: "tb-width-50",
      render: (col, index) => (
        <i className="fa fa-ellipsis-h" onClick={e => {
          e.preventDefault();
          this.handleClick(e, index);
          this.setState({ id: col.id, tooltipIndex: index})
        }}/>
      )
    }]

  renderTooltip() {
    const { tooltipIndex } = this.state;
    const { dispatch } = this.props;
    const dataSource = [{
      label: "Chỉnh sửa",
      className: "fa fa-edit",
      onClick: e => {
        dispatch({
          type: "CoursePrice/view",
          payload: { id: this.state.id }
        }).then(() => {
          this.setState({ isCreatNew: false, showModal: true});
        });
        this.handleClick(e, tooltipIndex, true);
      }
    }]
    return (
      <CustomTooltip
        show={this.state.show}
        onHide={e => this.handleClick(e, tooltipIndex)}
        dataSource={dataSource}
        target={this.state.target}
      />
    );
  }

  handleClick(e, index, isHide) {
    this.setState({
      show: !(index === this.state.activeIndex),
      target: isHide ? null : e.target,
      activeIndex: index === this.state.activeIndex ? null : index
    });
  }

  onClose = () => {
    this.setState({ isCreateNew: false, showModal: false });
  };

  render() {
    return (
      <div className="row">
        <div className="col-12 d-flex justify-content-end m-t-2">
          <div className="form-group">
            <Button
              variant="primary"
              type="submit"
              onClick={() =>
                this.setState({ isCreateNew: true, showModal: true })
              }
            >
              Tạo mới
            </Button>
          </div>
        </div>
        <div className="col-12">
          <CustomTable
            columns={this.columns}
            dataSource={this.props.CoursePrice.list_course_price}
            total={this.props.CoursePrice.total_record}
            onChange={(data) => this.updateOptions(data)}
          />
        </div>
        {this.state.showModal && (
          <CustomModal
            isCreateNew={this.state.isCreateNew}
            onClose={this.onClose}
            selectedItem={this.state.selectedItem}
          />
        )}
        {this.renderTooltip()}
      </div>
    );
  }
}

export default connect(({ CoursePrice }) => ({
  CoursePrice
}))(Page);
