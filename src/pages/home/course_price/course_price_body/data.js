export const HEADER = [
  {
    title: "Tên khoá học",
    key: "ten_khoa_hoc",
    class: "tb-width-400"
  },
  {
    title: "Trung tâm",
    key: "trung_tam",
    class: "tb-width-200"
  },
  {
    title: "Giáo viên nước ngoài",
    key: "giao_vien_nuoc_ngoai",
    class: "tb-width-200"
  },
  {
    title: "Giá tiền",
    key: "gia_tien",
    class: "tb-width-150"
  },
  {
    title: "Kiểu thu",
    key: "kieu_thu",
    class: "tb-width-150"
  },
  {
    title: "Số buổi dự kiến",
    key: "so_buoi_du_kien",
    class: "tb-width-150"
  },
  {
    title: "Đơn giá trên buổi",
    key: "don_gia_tren_buoi",
    class: "tb-width-150"
  },
  {
    title: "Trạng thái",
    key: "trang_thai",
    class: "tb-width-150"
  }
];

export const DATA = [
  {
    id: 1,
    ten_khoa_hoc: "BIBOP 1",
    trung_tam: "Mai Hắc Đế",
    giao_vien_nuoc_ngoai: "50%",
    gia_tien: "3.000.000",
    kieu_thu: "Toàn khóa",
    so_buoi_du_kien: 10,
    don_gia_tren_buoi: "300.000",
    trang_thai: "active"
  },
  {
    id: 1,
    ten_khoa_hoc: "BIBOP 1",
    trung_tam: "Mai Hắc Đế",
    giao_vien_nuoc_ngoai: "50%",
    gia_tien: "3.000.000",
    kieu_thu: "Toàn khóa",
    so_buoi_du_kien: 10,
    don_gia_tren_buoi: "300.000",
    trang_thai: "inactive"
  }
];

export const TRUNG_TAM = [
  {
      label: "Vui lòng chọn 1 giá trị",
      value: "1"
  },
  {
      label: "Hội sở chính",
      value: "2"
  },
  {
      label: "Hoàng Ngân",
      value: "3"
  },
  {
      label: "Văn Điển",
      value: "4"
  },
  
]

export const GIAO_TRINH = [
  {
    text: "Chọn một giáo trình",
    code: ""
  },
  {
    text: "BIBOP",
    code: "1"
  },
  {
    text: "BIG ENGLISH PLUS ONE",
    code: "2"
  },
  
]