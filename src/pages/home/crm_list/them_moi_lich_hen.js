import React, { Component, useState, useRef, useEffect } from "react";
import { connect } from "dva";
import { Modal, Button } from "react-bootstrap";
import { STATUS } from "./data";
import { Formik, ErrorMessage } from "formik";
import * as yup from "yup";
import Input from "../../../components/Form/Input";
import Select from "../../../components/Form/Select";
import TextArea from "../../../components/Form/TextArea";
import moment from "moment";

function CustomModal(props) {
  const [show, setShow] = useState(true);
  const handleClose = data => {
    // setShow(false);
    props.onClose(data);
  };
  const { isCreateNew, dispatch, partnerId } = props;
  const selectedItem = props.Appointment.current_lich_hen;
  const initialValues = {
    appointmentTitle: !isCreateNew ? selectedItem.appointmentTitle : "",
    performBy: !isCreateNew ? `${selectedItem.performBy}` : "",
    appointmentDate: !isCreateNew
      ? moment(selectedItem.appointmentDate).format("YYYY-MM-DD")
      : "",
    timeBegin: !isCreateNew ? selectedItem.timeBegin : "",
    timeEnd: !isCreateNew ? selectedItem.timeEnd : "",
    appointmentContent: !isCreateNew ? selectedItem.appointmentContent : "",
    isPrioritize: !isCreateNew ? selectedItem.isPrioritize : false,
    nextDate: !isCreateNew ? selectedItem.nextDate : "",
    nextBegin: !isCreateNew ? selectedItem.nextBegin : "",
    nextEnd: !isCreateNew ? selectedItem.nextEnd : "",
    status: !isCreateNew ? selectedItem.status : ""
  };
  console.log("initialValues", initialValues);
  return (
    <Modal
      size={"md"}
      show={show}
      onHide={handleClose}
      backdrop="static"
      keyboard={false}
      id="AddNew"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Formik
        enableReinitialize={true}
        initialValues={initialValues}
        validationSchema={yup.object().shape({
          appointmentTitle: yup.string().required("Trường bắt buộc"),
          performBy: yup.string().required("Trường bắt buộc"),
          appointmentDate: yup.string().required("Trường bắt buộc"),
          timeBegin: yup.string().required("Trường bắt buộc"),
          timeEnd: yup.string().required("Trường bắt buộc"),
          appointmentContent: yup.string().required("Trường bắt buộc"),
          nextDate: isCreateNew
            ? yup.string()
            : yup.string().required("Trường bắt buộc"),
          nextBegin: isCreateNew
            ? yup.string()
            : yup.string().required("Trường bắt buộc"),
          nextEnd: isCreateNew
            ? yup.string()
            : yup.string().required("Trường bắt buộc"),
          status: isCreateNew
            ? yup.string()
            : yup.string().required("Trường bắt buộc")
        })}
        onSubmit={(values, { setSubmitting }) => {
          const payload = isCreateNew
            ? {
                status: values.status,
                nextDate: values.nextDate,
                nextBegin: values.nextBegin,
                nextEnd: values.nextEnd,
                appointmentTitle: values.appointmentTitle,
                performBy: +values.performBy,
                appointmentDate: values.appointmentDate,
                timeBegin: values.timeBegin,
                timeEnd: values.timeEnd,
                appointmentContent: values.appointmentContent,
                isPrioritize: values.isPrioritize,
                partnerId
              }
            : {
                ...selectedItem,
                ...values,
                performBy: +values.performBy,
                status: +values.status
              };
          dispatch({
            type: "Global/showLoading"
          });
          dispatch({
            type: isCreateNew ? "Appointment/insert" : "Appointment/update",
            payload: payload
          }).then(res => {
            dispatch({
              type: "Global/hideLoading"
            });
            if (res) {
              handleClose(true);
            } else {
              dispatch({
                type: "Global/showError"
              });
            }
          });
        }}
      >
        {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
          setFieldValue
          /* and other goodies */
        }) => (
          <form onSubmit={handleSubmit}>
            <Modal.Header closeButton>
              <Modal.Title>
                {isCreateNew ? "Thêm mới lịch hẹn" : "Chỉnh sửa lịch hẹn"}
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <div className="row ">
                <div className="col-12 ">
                  <Input
                    id="appointmentTitle"
                    name="appointmentTitle"
                    label="Tiêu đề"
                    value={values.appointmentTitle}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    placeholder={"Nhập Tiêu đề"}
                  />
                </div>
                {/* <div className="col-12 ">
                  <Select
                    id="partnerId"
                    name="partnerId"
                    label="Đối tượng:"
                    options={props.Appointment?.list_nguoi_thuc_hien}
                    value={values.partnerId}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                  
                </div> */}
                <div className="col-12 ">
                  <Select
                    id="performBy"
                    name="performBy"
                    label="Người thực hiện:"
                    options={props.Appointment?.list_nguoi_thuc_hien}
                    value={values.performBy}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                </div>

                <div className="col-12 ">
                  <div className="row">
                    <div className="col-6">
                      <Input
                        id="appointmentDate"
                        type="date"
                        name="appointmentDate"
                        label="Ngày:"
                        value={values.appointmentDate}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        placeholder={"Chọn ngày"}
                        format="YYYY-MM-DD"
                        min={moment().format("YYYY-MM-DD")}
                      />
                    </div>
                    <div className="col-3">
                      <Input
                        id="timeBegin"
                        type="time"
                        name="timeBegin"
                        label="Bắt đầu:"
                        value={values.timeBegin}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        min="00:00"
                        max="23:59"
                      />
                    </div>
                    <div className="col-3">
                      <Input
                        id="timeEnd"
                        type="time"
                        name="timeEnd"
                        label="Kết thúc:"
                        value={values.timeEnd}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        min="00:00"
                        max="23:59"
                      />
                    </div>
                  </div>
                </div>
                <div className="col-12">
                  <TextArea
                    className="textarea form-control"
                    rows="4"
                    label="Nội dung:"
                    name="appointmentContent"
                    value={values.appointmentContent}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  ></TextArea>
                </div>
                <div className="col-12">
                  <div className="form-group">
                    <input
                      type="checkbox"
                      name="isPrioritize"
                      id="isPrioritize"
                      value={values.isPrioritize}
                      onChange={handleChange}
                      onBlur={handleBlur}
                    />
                    <label htmlFor="isPrioritize">&nbsp;&nbsp; Ưu tiên</label>
                  </div>
                </div>
                {!isCreateNew && (
                  <>
                    <div className="col-12 ">
                      <Select
                        id="status"
                        name="status"
                        label="Kết quả:"
                        options={STATUS}
                        value={values.status}
                        onChange={handleChange}
                        onBlur={handleBlur}
                      />
                    </div>
                    <div className="col-12 ">
                      <div className="row">
                        <div className="col-6">
                          <Input
                            id="nextDate"
                            type="date"
                            name="nextDate"
                            label="Ngày:"
                            value={values.nextDate}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            placeholder={"Chọn ngày"}
                            format="YYYY-MM-DD"
                            min={moment().format("YYYY-MM-DD")}
                          />
                        </div>
                        <div className="col-3">
                          <Input
                            id="nextBegin"
                            type="time"
                            name="nextBegin"
                            label="Bắt đầu:"
                            value={values.nextBegin}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            min="00:00"
                            max="23:59"
                          />
                        </div>
                        <div className="col-3">
                          <Input
                            id="nextEnd"
                            type="time"
                            name="nextEnd"
                            label="Kết thúc:"
                            value={values.nextEnd}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            min="00:00"
                            max="23:59"
                          />
                        </div>
                      </div>
                    </div>
                  </>
                )}
              </div>
            </Modal.Body>
            <Modal.Footer>
              <Button
                variant="primary"
                style={{ marginRight: 15 }}
                type="submit"
              >
                {isCreateNew ? "Tạo mới" : "Cập nhật"}
              </Button>
              <Button variant={"secondary"} onClick={() => handleClose()}>
                Hủy
              </Button>
            </Modal.Footer>
          </form>
        )}
      </Formik>
    </Modal>
  );
}
export default connect(({ Invoice, Appointment, Global }) => ({
  Invoice,
  Appointment,
  Global
}))(CustomModal);
