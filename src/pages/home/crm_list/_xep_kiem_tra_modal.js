import React from "react";
import moment from "moment";
import { Modal, Button } from "react-bootstrap";
import { connect } from "dva";
import { Formik, ErrorMessage } from "formik";
import Input from "../../../components/Form/Input";
import Select from "../../../components/Form/Select";
import * as yup from "yup";

const CustomModal = ({ handleClose, StudentTest, id, dispatch }) => {
  return (
    <Modal show size="lg">
      <Modal.Header closeButton>
        <Modal.Title>Xếp lịch kiểm tra</Modal.Title>
      </Modal.Header>
      <Formik
        enableReinitialize={true}
        validationSchema={yup.object().shape({
          testDate: yup.string().required("Trường bắt buộc"),
          teacherId: yup.string().required("Trường bắt buộc"),
          centerId: yup.string().required("Trường bắt buộc")
        })}
        initialValues={{
          testDate: "",
          teacherId: "",
          centerId: ""
        }}
        onSubmit={(values, { setSubmitting }) => {
          dispatch({
            type: "Global/showLoading"
          });
          dispatch({
            type: "StudentTest/insert",
            payload: {
              partnerId: id,
              studentId: StudentTest.current_student_test.id,
              testDate: values.testDate,
              teacherId: +values.teacherId,
              centerId: +values.centerId
            }
          }).then(data => {
            dispatch({
              type: "Global/hideLoading"
            });
            if (data) {
              dispatch({
                type: "Global/showSuccess"
              });
              handleClose();
            } else {
              dispatch({
                type: "Global/showError"
              });
            }
          });
        }}
      >
        {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting
          /* and other goodies */
        }) => (
          <form style={{ width: "100%" }} onSubmit={handleSubmit}>
            <Modal.Body>
              <div className="row m-b-1">
                <div className="col-12">
                  <fieldset>
                    <legend>Thông tin khách hàng</legend>
                    <div className="col-12">
                      <div className="row">
                        <div className="col-md-4 col-sm-6 col-xs-12">
                          <p>
                            Mã khách hàng:
                            <b>{StudentTest?.current_student_test?.code}</b>
                          </p>
                        </div>
                        <div className="col-md-4 col-sm-6 col-xs-12">
                          <p>
                            Tên:
                            <b>{StudentTest?.current_student_test?.lastName}</b>
                          </p>
                        </div>
                        <div className="col-md-4 col-sm-6 col-xs-12">
                          <p>
                            Họ Đệm:
                            <b>
                              {StudentTest?.current_student_test?.firstName}
                            </b>
                          </p>
                        </div>
                        <div className="col-md-4 col-sm-6 col-xs-12">
                          <p>
                            Ngày tháng năm sinh:
                            <b>
                              {moment(
                                StudentTest?.current_student_test?.birthDate
                              ).format("DD/MM/YYYY")}
                            </b>
                          </p>
                        </div>
                        <div className="col-md-4 col-sm-6 col-xs-12">
                          <p>
                            Giới tính:
                            <b>{StudentTest?.current_student_test?.gender}</b>
                          </p>
                        </div>
                        <div className="col-md-4 col-sm-6 col-xs-12">
                          <p>
                            Phụ Huynh:
                            <b>
                              {StudentTest?.current_student_test?.studentName}
                            </b>
                          </p>
                        </div>
                      </div>
                    </div>
                  </fieldset>
                </div>
              </div>

              <div className="row">
                <div className="col-sm-6 col-xs-12">
                  <div className="form-group">
                    <Input
                      id="testDate"
                      type="date"
                      name="testDate"
                      label="Thời gian:"
                      value={values.testDate}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      placeholder={"Chọn ngày"}
                      format="YYYY-MM-DD"
                      min={moment().format("YYYY-MM-DD")}
                    />
                  </div>
                </div>
                <div className="col-sm-6 col-xs-12">
                  <div className="form-group">
                    <Select
                      id="centerId"
                      name="centerId"
                      label="Địa điểm:"
                      options={StudentTest.list_center}
                      value={values.centerId}
                      onChange={handleChange}
                      onBlur={handleBlur}
                    />
                  </div>
                </div>
                <div className="col-sm-6 col-xs-12">
                  <div className="form-group">
                    <label htmlFor="giao_vien"></label>
                    <Select
                      id="teacherId"
                      name="teacherId"
                      label="Giáo viên:"
                      options={StudentTest.list_giao_vien}
                      value={values.teacherId}
                      onChange={handleChange}
                      onBlur={handleBlur}
                    />
                  </div>
                </div>
              </div>
            </Modal.Body>
            <Modal.Footer>
              <Button
                variant="primary"
                style={{ marginRight: 15 }}
                type="submit"
              >
                Lưu
              </Button>
              <Button variant={"secondary"} onClick={handleClose}>
                Đóng
              </Button>
            </Modal.Footer>
          </form>
        )}
      </Formik>
    </Modal>
  );
};

export default connect(({ StudentTest, Global }) => ({
  StudentTest,
  Global
}))(CustomModal);
