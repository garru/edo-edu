export const STATUS = [
  {
    id: 1,
    name: "Chưa thực hiện"
  },
  {
    id: 2,
    name: "Đã thực hiện"
  },
  {
    id: 3,
    name: "Cần chăm sóc tiếp"
  },
  {
    id: 4,
    name: "Có vấn đề"
  },
]