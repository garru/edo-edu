import React, { useState, useEffect } from "react";
import { Overlay, Popover, Table } from "react-bootstrap";
import moment from "moment";
import { connect } from "dva";
import XepHocThuModal from "./_xep_hoc_thu_modal";
import XepKiemTraModal from "./_xep_kiem_tra_modal";
import "react-datepicker/dist/react-datepicker.css";
import PageHeader from "../../../components/PageHeader";
import { Link } from "react-router-dom";
import CustomModal from "./them_moi_lich_hen";
import CustomTable from "../../../components/Table";
import isEmpty from "lodash/isEmpty";

let timeoutSearch;

const Page = props => {
  const bre = {
    title: "Danh sách khách hàng",
    breadcrums: [
      {
        title: "Home",
        path: "/"
      },
      {
        title: "Danh sách khách hàng",
        path: ""
      }
    ]
  };
  const [_xep_kiem_tra, setXepKiemTra] = useState(false);
  const [_xep_hoc_thu, setXepHocThu] = useState(false);
  const [_show_tooltip, setShowTooltip] = useState(false);
  const [_tooltip_target, setTooltipTarget] = useState();
  const [selected, setSelected] = useState();
  const [showModal, setShowModal] = useState(false);
  const [count, setCount] = useState(0);
  const [partnerId, setPartnerId] = useState(null);
  const [isCreateNew, setIsCreateNew] = useState(false);
  const [selectedAppointment, setSelectedAppointment] = useState();

  const Status = ({ value, index, id }) => {
    console.log("props", props, value, index, id);
    return (
      <div className="form-group m-0">
        <select
          name={`trangthai_${index}`}
          id={`trangthai_${index}`}
          onChange={e => onChangeTrangThai(e, id)}
          defaultValue={value}
        >
          {props?.Customer?.list_trang_thai?.map((item, idx) => (
            <option key={`status_option_${idx}`} value={item.id}>
              {item.name}
            </option>
          ))}
        </select>
      </div>
    );
  };

  const RelationShip = ({ value, index, id }) => (
    <div className="form-group m-0">
      <select
        name={`quanhe_${index}`}
        id={`quanhe_${index}`}
        onChange={e => onChangeQuanhe(e, id)}
        defaultValue={value}
      >
        {props?.Customer?.list_quan_he.map((item, idx) => (
          <option value={item.id} key={`quanhe_option_${idx}`}>
            {item.name}
          </option>
        ))}
      </select>
    </div>
  );

  const onChangeQuanhe = (e, id) => {
    const { dispatch } = props;
    dispatch({
      type: "Global/showLoading"
    });
    dispatch({
      type: "Customer/updateclassify",
      payload: {
        id: id,
        classify: +e.target.value
      }
    }).then(res => {
      dispatch({
        type: "Global/hideLoading"
      });
      !res &&
        dispatch({
          type: "Global/showError"
        });
    });
  };

  const onChangeTrangThai = (e, id) => {
    const { dispatch } = props;
    dispatch({
      type: "Global/showLoading"
    });
    dispatch({
      type: "Customer/updatestatus",
      payload: {
        id: id,
        status: +e.target.value
      }
    }).then(res => {
      dispatch({
        type: "Global/hideLoading"
      });
      !res &&
        dispatch({
          type: "Global/showError"
        });
    });
  };

  const Appointment = ({ partnerId, item, col }) => {
    const empty = isEmpty(item);
    const { appointmentStatus, appointmentDate, title, id } = item;
    if (empty) {
      return (
        <div className="d-flex justify-content-center">
          <div
            className="btn-icon ml-2"
            onClick={() => {
              setShowModal(true);
              setIsCreateNew(true);
              setPartnerId(partnerId);
            }}
          >
            <i className="fa fa-plus" />
          </div>
        </div>
      );
    }
    return (
      <div className="d-flex justify-content-between align-items-center">
        <div className="text-left">
          <div>{title}</div>
          <div>{moment(appointmentDate).format("DD/MM/YYYY")}</div>
        </div>
        {/* <p className="m-0"></p> */}
        {appointmentStatus === 0 ? (
          <i
            style={{ cursor: "pointer", color: "#0f01ac" }}
            className="font-24 far fa-clock"
            onClick={() => {
              onUpdateAppointment(id);
            }}
          />
        ) : appointmentStatus === 2 ? (
          <i style={{ color: "#ac0000" }} className="font-24 fas fa-check" />
        ) : appointmentStatus === 4 ? (
          <i style={{ color: "#01b312" }} className="font-24 fas fa-times" />
        ) : null}
      </div>
    );
  };

  const onUpdateAppointment = id => {
    const { dispatch } = props;
    dispatch({
      type: "Global/showLoading"
    });
    dispatch({
      type: "Appointment/view",
      payload: {
        id: id
      }
    }).then(res => {
      dispatch({
        type: "Global/hideLoading"
      });
      if (res) {
        setShowModal(true);
        setIsCreateNew(false);
        setPartnerId(partnerId);
        setSelectedAppointment(res.content);
      } else {
        dispatch({
          type: "Global/showError"
        });
      }
    });
  };

  const Tooltip = ({
    show,
    target,
    handleMenuClick,
    setShowTooltip,
    id,
    data
  }) => {
    return (
      <Overlay
        show={show}
        target={target}
        placement="bottom"
        containerPadding={20}
        rootClose={true}
        rootCloseEvent={"click"}
        onHide={e => setShowTooltip(false)}
      >
        <Popover id="popover-contained">
          <Popover.Content>
            <ul>
              <li onClick={e => (e.preventDefault(), e.stopPropagation())}>
                <Link to={`/cham_soc/${id}`}>Chi tiết khách hàng</Link>
              </li>
              <li onClick={e => (e.preventDefault(), e.stopPropagation())}>
                <Link to={`/hoa_don/${id}`}>Xuất hoá đơn</Link>
              </li>
              <li onClick={() => handleMenuClick(2, id)}>Xếp kiểm tra</li>
              <li onClick={() => handleMenuClick(3, id)}>Xếp học thứ</li>
              <li onClick={() => handleMenuClick(4)}>Lưu trữ</li>
            </ul>
          </Popover.Content>
        </Popover>
      </Overlay>
    );
  };

  useEffect(() => {
    const { dispatch } = props;
    dispatch({
      type: "Global/showLoading"
    });
    Promise.all([
      dispatch({
        type: "StudentTest/listcenter",
        payload: {}
      }),
      // dispatch({
      //   type: "StudentTest/listteacher",
      //   payload: {}
      // }),
      dispatch({
        type: "StudentTrial/listcenter",
        payload: {}
      }),
      dispatch({
        type: "StudentTrial/listprogram",
        payload: {}
      }),
      dispatch({
        type: "Customer/listtrangthai",
        payload: {}
      }),
      dispatch({
        type: "Customer/listquanhe",
        payload: {}
      }),
      dispatch({
        type: "Appointment/listperform",
        payload: {}
      })
    ]).then(data => {
      dispatch({
        type: "Global/hideLoading"
      });
      if (!data) {
        dispatch({
          type: "Global/showError"
        });
      }
    });
  }, [count]);

  const handleClickTooltipMenu = (menu, id) => {
    setSelected(id);
    const { dispatch } = props;
    switch (menu) {
      case 0:
        break;
      case 1:
        break;
      case 2:
        dispatch({
          type: "Global/showLoading"
        });
        Promise.all([
          dispatch({
            type: "StudentTest/view",
            payload: { id }
          })
        ]).then(data => {
          dispatch({
            type: "Global/hideLoading"
          });
          if (data) {
            setXepKiemTra(true);
          } else {
            dispatch({
              type: "Global/showError"
            });
          }
        });

        break;
      case 3:
        dispatch({
          type: "Global/showLoading"
        });
        Promise.all([
          dispatch({
            type: "StudentTrial/view",
            payload: { id }
          })
        ]).then(data => {
          dispatch({
            type: "Global/hideLoading"
          });
          if (data) {
            setXepHocThu(true);
          } else {
            dispatch({
              type: "Global/showError"
            });
          }
        });

        break;
      case 4:
        break;
      default:
        break;
    }
    setShowTooltip(false);
  };
  const handleClickTooltip = e => {
    setShowTooltip(true);
    setTooltipTarget(e.target);
  };
  const handleModalClose = () => {
    setXepKiemTra(false);
    setXepHocThu(false);
  };
  const onClose = data => {
    if (data) {
      const { dispatch } = props;
      dispatch({
        type: "Global/showLoading"
      });
      dispatch({
        type: "Customer/updateoptions",
        payload: {}
      }).then(res => {
        dispatch({
          type: "Global/showSuccess"
        });
        dispatch({
          type: "Global/hideLoading"
        });
        setShowModal(false);
        if (!res) {
          dispatch({
            type: "Global/showError"
          });
        }
      });
    } else {
      setShowModal(false);
    }
  };

  const onSearch = (e, props) => {
    clearTimeout(timeoutSearch);
    const value = e.target?.value;
    timeoutSearch = setTimeout(() => {
      const { dispatch } = props;
      // setKeyword(value);
      dispatch({
        type: "Global/showLoading"
      });
      dispatch({
        type: "Customer/updateoptions",
        payload: {
          keyword: value
        }
      }).then(data => {
        dispatch({
          type: "Global/hideLoading"
        });
        if (!data) {
          dispatch({
            type: "Global/showError"
          });
        }
      });
    }, 500);
  };

  const updateOptions = (data, props) => {
    const { dispatch } = props;
    dispatch({
      type: "Global/showLoading"
    });
    dispatch({
      type: "Customer/updateoptions",
      payload: data
    }).then(res => {
      dispatch({
        type: "Global/hideLoading"
      });
      !res &&
        dispatch({
          type: "Global/showError"
        });
    });
  };

  const column_customer = [
    {
      title: "Mã",
      key: "id",
      class: "tb-width-100"
    },
    {
      title: "Tên",
      key: "lastName",
      class: "tb-width-100"
    },
    {
      title: "Họ Đệm",
      key: "firstName",
      class: "tb-width-150"
    },
    {
      title: "Điện thoại",
      key: "phone",
      class: "tb-width-150"
    },
    {
      title: "Email",
      key: "email",
      class: "tb-width-200"
    },
    {
      title: "Trạng thái",
      key: "trang_thai",
      class: "tb-width-100",
      render: (col, index) => (
        <Status
          key={`status_${index}`}
          props={props}
          value={col.status}
          index={index}
          id={col.id}
          col={col}
        ></Status>
      )
    },
    {
      title: "Mối quan hệ",
      key: "moi_quan_he",
      class: "tb-width-100",
      render: (col, index) => (
        <RelationShip
          key={`moi_quan_he_${index}`}
          props={props}
          value={col.classify}
          index={index}
          id={col.id}
        ></RelationShip>
      )
    },
    {
      title: "Saler",
      key: "salerName",
      class: "tb-width-150"
    },
    {
      title: "Lịch hẹn",
      key: "lich_hen1",
      class: "tb-width-150",
      render: (col, index) => (
        <Appointment
          partnerId={col.id}
          col={col}
          item={col.appointmentList[0] || {}}
        />
      )
    },
    {
      title: "Lịch hẹn",
      key: "lich_hen2",
      class: "tb-width-150",
      render: (col, index) => (
        <Appointment
          partnerId={col.id}
          col={col}
          item={col?.appointmentList[1] || {}}
        />
      )
    },
    {
      title: "Lịch hẹn",
      key: "lich_hen3",
      class: "tb-width-150",
      render: (col, index) => (
        <Appointment
          partnerId={col.id}
          col={col}
          item={col?.appointmentList[2] || {}}
        />
      )
    },
    {
      title: "Tham gia lớp",
      key: "tham_gia_lop",
      class: "tb-width-150"
    },
    {
      title: "Giáo viên phụ trách",
      key: "giao_vien_phu_trach",
      class: "tb-width-200"
    },
    {
      title: "Thao tác",
      key: "thao_tac",
      class: "tb-width-100"
    }
  ];

  const column_parent = [
    {
      title: "Mã",
      key: "id",
      class: "tb-width-100"
    },
    {
      title: "Tên",
      key: "lastName",
      class: "tb-width-100"
    },
    {
      title: "Họ Đệm",
      key: "firstName",
      class: "tb-width-150"
    },
    {
      title: "Điện thoại",
      key: "phone",
      class: "tb-width-150"
    },
    {
      title: "Email",
      key: "email",
      class: "tb-width-200"
    },
    {
      title: "Trạng thái",
      key: "trang_thai",
      class: "tb-width-100",
      render: (col, index) => (
        <Status
          props={props}
          value={col.status}
          index={index}
          id={col.id}
        ></Status>
      )
    },
    {
      title: "Mối quan hệ",
      key: "moi_quan_he",
      class: "tb-width-100",
      render: (col, index) => (
        <RelationShip
          props={props}
          value={col.classify}
          index={index}
          id={col.id}
        ></RelationShip>
      )
    },
    {
      title: "Saler",
      key: "salerName",
      class: "tb-width-150"
    },
    {
      title: "Lịch hẹn",
      key: "lich_hen1",
      class: "tb-width-150",
      render: (col, index) => (
        <Appointment
          isCreateNew={true}
          partnerId={col.id}
          item={col.appointmentList[0] || {}}
        />
      )
    },
    {
      title: "Lịch hẹn",
      key: "lich_hen2",
      class: "tb-width-150",
      render: (col, index) => (
        <Appointment
          isCreateNew={true}
          partnerId={col.id}
          item={col.appointmentList[0] || {}}
        />
      )
    },
    {
      title: "Lịch hẹn",
      key: "lich_hen3",
      class: "tb-width-150",
      render: (col, index) => (
        <Appointment
          isCreateNew={true}
          partnerId={col.id}
          item={col.appointmentList[0] || {}}
        />
      )
    },
    {
      title: "Phụ huynh của",
      key: "studentName",
      class: "tb-width-200"
    },
    {
      title: "Thao tác",
      key: "thao_tac",
      class: "tb-width-100",
      render: (col, index) => (
        <>
          <i className="fa fa-ellipsis-h" onClick={handleClickTooltip} />
          <Tooltip
            data={col}
            show={_show_tooltip}
            target={_tooltip_target}
            handleMenuClick={handleClickTooltipMenu}
            setShowTooltip={setShowTooltip}
            id={col.id}
          />
        </>
      )
    }
  ];

  return (
    <div>
      <PageHeader {...bre}></PageHeader>
      <div className="iq-card">
        <div className=" iq-card-header d-flex justify-content-between">
          <div className="iq-header-title">
            <h6>
              <b>Thông tin cơ bản</b>
            </h6>
          </div>
        </div>
        <div className="iq-card-body">
          <div className="">
            <div className="row ">
              <div className="col-12 m-b-2">
                <fieldset>
                  <legend>Tìm kiếm</legend>
                  <div className="row align-end">
                    <div className="col-4">
                      <div className="form-group">
                        <input
                          type="text"
                          name="ma_nhan_vien"
                          className="form-control form-control-lg"
                          id="ma_nhan_vien"
                          placeholder="Nhập tên/SDT/Email/Mã khách hàng"
                          onChange={e => onSearch(e, props)}
                          // value={keyword}
                        ></input>
                      </div>
                    </div>
                  </div>
                </fieldset>
              </div>
              <div className="col-12 m-b-2">
                <Link to="/them_phu_huynh" style={{ marginRight: 30 }}>
                  <button className="btn btn-primary">Thêm phụ huynh</button>
                </Link>
                <Link to="/them_hoc_vien" style={{ marginRight: 30 }}>
                  <button className="btn btn-primary">Thêm học viên</button>
                </Link>
                {props.Customer?.summary?.map((item, index) => (
                  <button
                    disabled
                    className={`btn ${
                      item.code === "Appointment" ? "btn-primary" : "btn-orange"
                    }`}
                    key={`summary_${index}`}
                    style={{
                      marginRight: 30,
                      opacity: 1,
                      position: "relative"
                    }}
                  >
                    {item.name}
                    <span className="bagde">{item.count}</span>
                  </button>
                ))}
              </div>
              <div className="col-12">
                {props.Customer.total_customer ? (
                  <>
                    <h2 className="m-b-1 font-bold">Khách hàng</h2>
                    <CustomTable
                      dataSource={props.Customer?.list_customer}
                      total={props.Customer.total_customer}
                      columns={column_customer}
                      onChange={data => updateOptions(data, props)}
                    ></CustomTable>
                  </>
                ) : (
                  ""
                )}
                {props.Customer.total_parent ? (
                  <>
                    <h2 className="m-b-1 font-bold">Phụ huynh/Học viên</h2>
                    <CustomTable
                      dataSource={props.Customer?.list_parent}
                      total={props.Customer.total_parent}
                      columns={column_parent}
                      onChange={data => updateOptions(data, props)}
                    ></CustomTable>
                  </>
                ) : (
                  ""
                )}
                {_xep_hoc_thu && (
                  <XepHocThuModal
                    id={selected}
                    handleClose={handleModalClose}
                  />
                )}
                {_xep_kiem_tra && (
                  <XepKiemTraModal
                    id={selected}
                    handleClose={handleModalClose}
                  />
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
      {showModal && (
        <CustomModal
          onClose={onClose}
          isCreateNew={isCreateNew}
          partnerId={partnerId}
          selectedAppointment={selectedAppointment}
        ></CustomModal>
      )}
    </div>
  );
};

export default connect(
  ({ Customer, Appointment, StudentTrial, StudentTest, Global }) => ({
    Customer,
    Appointment,
    StudentTrial,
    StudentTest,
    Global
  })
)(Page);
