import React, { useState, useEffect } from "react";
import { Nav } from "react-bootstrap";
import moment from "moment";
import { connect } from "dva";
import "react-datepicker/dist/react-datepicker.css";
import { useParams } from "react-router-dom";
import TextArea from "../../../components/Form/TextArea";
import UploadAvatar from "../../../components/UploadAvatar";
import { Formik, ErrorMessage } from "formik";
import * as yup from "yup";
import PageHeader from "../../../components/PageHeader";

const Comment = ({ avatar, name, created_at, comment }) => {
  return (
    <div className="row p-2">
      <div className="col-1 d-flex justify-content-center align-items-start p-0">
        <img src={avatar} alt="" className="rounded-circle w-75" />
      </div>
      <div className="col-11">
        <div className="d-flex">
          <div className="text-nowrap font-weight-bold text-primary">
            {name}
          </div>
          <span className="text-nowrap font-italic ml-2">
            {moment(created_at).fromNow()}
          </span>
        </div>
        <div className="nowrap">{comment}</div>
      </div>
    </div>
  );
};

const Page = props => {
  const [count, setCount] = useState(0);
  const { id } = useParams();
  const { dispatch } = props;

  const bre = {
    title: "Chi tiết chăm sóc khách hàng",
    breadcrums: [
      {
        title: "Home",
        path: "/"
      },
      {
        title: "Chi tiết chăm sóc khách hàng",
        path: ""
      }
    ]
  };

  useEffect(() => {
    dispatch({
      type: "Global/showLoading"
    });
    Promise.all([
      dispatch({
        type: "CustomerDetail/view",
        payload: { id }
      }),
      dispatch({
        type: "CustomerDetail/listdiscussion",
        payload: { id }
      })
    ]).then(data => {
      dispatch({
        type: "Global/hideLoading"
      });
    });
  }, [count]);
  return (
    <>
      <PageHeader {...bre} />
      <div className="iq-card ">
        <div className="iq-card-body">
          <div className="">
            <div className="row">
              <div className="col-12">
                <Nav fill variant="pills" className="m-b-2" defaultActiveKey={`/trao_doi/${id}`}>
                  <Nav.Item>
                    <Nav.Link key="trao_doi" href={`/trao_doi/${id}`}>
                      Thảo luận
                    </Nav.Link>
                  </Nav.Item>
                  <Nav.Item>
                    <Nav.Link key="cham_soc" href={`/cham_soc/${id}`}>
                      Lịch sử chăm sóc
                    </Nav.Link>
                  </Nav.Item>
                  <Nav.Item>
                    <Nav.Link key="hoa_don" href={`/hoa_don/${id}`}>
                      Hóa đơn
                    </Nav.Link>
                  </Nav.Item>
                  <Nav.Item>
                    <Nav.Link key="dd_nhan_xet" href={`/dd_nhan_xet/${id}`}>
                      Điểm danh/ nhận xét
                    </Nav.Link>
                  </Nav.Item>
                  <Nav.Item>
                    <Nav.Link key="lich_su_hoc" href={`/lich_su_hoc/${id}`}>
                      Lịch sử khác
                    </Nav.Link>
                  </Nav.Item>
                  {/*<Nav.Item>*/}
                  {/*  <Nav.Link href="/kiem_tra_hoc_thu">Kiểm tra/học thử</Nav.Link>*/}
                  {/*</Nav.Item>*/}
                </Nav>
              </div>
              <div className="col-lg-3 col-md-12 col-sm-12 text-center">
                <UploadAvatar
                  src={props.curent_customer_detail?.avatar}
                ></UploadAvatar>
              </div>
              <div className="col-lg-9 col-md-12 mt-4 p-4">
                <Formik
                  enableReinitialize={true}
                  initialValues={{
                    discussionContent: ""
                  }}
                  validationSchema={yup.object().shape({
                    discussionContent: yup.string().required("Trường bắt buộc")
                  })}
                  onSubmit={(values, { resetForm }) => {
                    dispatch({
                      type: "Global/showLoading"
                    });
                    dispatch({
                      type: "CustomerDetail/insertdiscussion",
                      payload: {
                        discussionContent: values.discussionContent,
                        partnerId: id
                      }
                    }).then(data => {
                      if (data) {
                        setCount(Math.random());
                        resetForm({ discussionContent: "" });
                        dispatch({
                          type: "Global/showSuccess"
                        });
                      } else {
                        dispatch({
                          type: "Global/hideLoading"
                        });
                        dispatch({
                          type: "Global/showError"
                        });
                      }
                    });
                  }}
                >
                  {({
                    values,
                    errors,
                    touched,
                    handleChange,
                    handleBlur,
                    handleSubmit,
                    isSubmitting,
                    setFieldValue,
                    resetForm
                    /* and other goodies */
                  }) => (
                    <form onSubmit={handleSubmit}>
                      <div className="row algin-center">
                        <div className="col-lg-10 col-md-10 col-sm-12">
                          <TextArea
                            id="discussionContent"
                            name="discussionContent"
                            label="Thảo luận:"
                            rows="4"
                            value={values.discussionContent}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            placeholder="Nhập Thảo luận"
                          />
                        </div>
                        <div className="col-lg-2 col-md-2 col-sm-12 text-right">
                          <button className="btn btn-primary" type="submit">
                            Gởi thảo luận
                          </button>
                        </div>
                      </div>
                    </form>
                  )}
                </Formik>
                <div className="col-12">
                  <div className="mt-4">
                    {props.CustomerDetail?.list_noi_dung_thao_luan.map(item => (
                      <Comment
                        avatar="/images/u439.svg"
                        name={item.createBy}
                        created_at={item.createSince}
                        comment={item.discussionContent}
                      />
                    ))}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default connect(({ CustomerDetail, Global }) => ({
  CustomerDetail,
  Global
}))(Page);
