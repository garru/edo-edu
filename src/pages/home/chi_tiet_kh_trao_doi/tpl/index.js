import React from 'react';
import './index.css';
import './ax.css';

class Page extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
		<div class="" id="base" style={{"margin-left":"-150px"}}>
		   <div class="ax_default shape" id="u11080">
		      <div class="" id="u11080_div">
		      </div>
		      <div class="text" id="u11080_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default shape" id="u11081">
		      <div class="" id="u11081_div">
		      </div>
		      <div class="text" id="u11081_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default" data-height="40" data-label="account" data-left="13" data-top="895" data-width="192" id="u11083">
		      <div class="ax_default shape" data-label="accountLable" id="u11084">
		         <img class="img" id="u11084_img" src="images/account_information/accountlable_u55.svg"/>
		         <div class="text" id="u11084_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Nguyen Van A
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u11085">
		         <img class="img" id="u11085_img" src="images/login/u4.svg"/>
		         <div class="text" id="u11085_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="settingMenu" data-left="11" data-top="826" data-width="125" id="u11086">
		      <div class="ax_default shape" data-label="gearIconLable" id="u11087">
		         <img class="img" id="u11087_img" src="images/account_information/geariconlable_u58.svg"/>
		         <div class="text" id="u11087_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Cài đặt
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="44" data-left="11" data-top="826" data-width="44" id="u11088">
		         <div class="ax_default shape" data-label="gearIconBG" id="u11089">
		            <div class="" id="u11089_div">
		            </div>
		            <div class="text" id="u11089_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default shape" data-label="gearIconBG2" id="u11090">
		            <div class="" id="u11090_div">
		            </div>
		            <div class="text" id="u11090_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" data-label="gearIcon" id="u11091">
		            <img class="img" id="u11091_img" src="images/account_information/gearicon_u62.svg"/>
		            <div class="text" id="u11091_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="customerMenu" data-left="11" data-top="145" data-width="143" id="u11092">
		      <div class="ax_default shape" data-label="customerIconLable" id="u11093">
		         <img class="img" id="u11093_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u11093_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Khách hàng
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="customerIconBG" id="u11094">
		         <div class="" id="u11094_div">
		         </div>
		         <div class="text" id="u11094_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="customerIconBG2" id="u11095">
		         <div class="" id="u11095_div">
		         </div>
		         <div class="text" id="u11095_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" data-label="customer" id="u11096">
		         <img class="img" id="u11096_img" src="images/account_information/customer_u67.svg"/>
		         <div class="text" id="u11096_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="classMenu" data-left="11" data-top="198" data-width="143" id="u11097">
		      <div class="ax_default shape" data-label="classIconLable" id="u11098">
		         <img class="img" id="u11098_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u11098_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Lớp học
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG" id="u11099">
		         <div class="" id="u11099_div">
		         </div>
		         <div class="text" id="u11099_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG2" id="u11100">
		         <div class="" id="u11100_div">
		         </div>
		         <div class="text" id="u11100_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" data-label="classicon" id="u11101">
		         <img class="img" id="u11101_img" src="images/account_information/classicon_u72.svg"/>
		         <div class="text" id="u11101_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="dashboardMenu" data-left="11" data-top="91" data-width="143" id="u11102">
		      <div class="ax_default shape" data-label="dashboardIconLable" id="u11103">
		         <img class="img" id="u11103_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u11103_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Dashnoard
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="dashboardIconBG" id="u11104">
		         <div class="" id="u11104_div">
		         </div>
		         <div class="text" id="u11104_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="dashboardIconBG2" id="u11105">
		         <div class="" id="u11105_div">
		         </div>
		         <div class="text" id="u11105_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" data-label="dashboard" id="u11106">
		         <img class="img" id="u11106_img" src="images/account_information/dashboard_u77.svg"/>
		         <div class="text" id="u11106_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="129" data-label="AccountMenu" data-left="58" data-top="806" data-width="246" id="u11107" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default shape" id="u11108">
		         <div class="" id="u11108_div">
		         </div>
		         <div class="text" id="u11108_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u11109">
		         <div class="" id="u11109_div">
		         </div>
		         <div class="text" id="u11109_text">
		            <p>
		               <span style={{"font-family":"ArialMT, Arial","font-weight":"400","text-decoration":"none"}}>
		                  Xin chào:
		               </span>
		               <span style={{"font-family":"Arial Bold, Arial","font-weight":"700","text-decoration":"none"}}>
		                  Nguyen Van A
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11110">
		         <div class="ax_default image" id="u11111">
		            <img class="img" id="u11111_img" src="images/account_information/u82.png"/>
		            <div class="text" id="u11111_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u11112">
		            <div class="" id="u11112_div">
		            </div>
		            <div class="text" id="u11112_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Đăng xuất
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default line" id="u11113">
		         <img class="img" id="u11113_img" src="images/account_information/u84.svg"/>
		         <div class="text" id="u11113_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11114">
		         <div class="ax_default paragraph" id="u11115">
		            <div class="" id="u11115_div">
		            </div>
		            <div class="text" id="u11115_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tài khoản
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default image" id="u11116">
		            <img class="img" id="u11116_img" src="images/account_information/u87.png"/>
		            <div class="text" id="u11116_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="339" data-label="SettingPannel" data-left="58" data-top="525" data-width="339" id="u11117" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default shape" id="u11118">
		         <div class="" id="u11118_div">
		         </div>
		         <div class="text" id="u11118_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u11119">
		         <div class="" id="u11119_div">
		         </div>
		         <div class="text" id="u11119_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Hệ thống
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11120">
		         <div class="ax_default icon" id="u11121">
		            <img class="img" id="u11121_img" src="images/account_information/u92.svg"/>
		            <div class="text" id="u11121_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u11122">
		            <div class="" id="u11122_div">
		            </div>
		            <div class="text" id="u11122_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tài khoản
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11123">
		         <div class="ax_default paragraph" id="u11124">
		            <div class="" id="u11124_div">
		            </div>
		            <div class="text" id="u11124_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phân quyền
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u11125">
		            <img class="img" id="u11125_img" src="images/account_information/u96.svg"/>
		            <div class="text" id="u11125_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u11126">
		         <div class="" id="u11126_div">
		         </div>
		         <div class="text" id="u11126_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Đào tạo
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11127">
		         <div class="ax_default paragraph" id="u11128">
		            <div class="" id="u11128_div">
		            </div>
		            <div class="text" id="u11128_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Thương hiệu
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u11129">
		            <img class="img" id="u11129_img" src="images/account_information/u100.svg"/>
		            <div class="text" id="u11129_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11130">
		         <div class="ax_default paragraph" id="u11131">
		            <div class="" id="u11131_div">
		            </div>
		            <div class="text" id="u11131_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trung tâm
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u11132">
		            <img class="img" id="u11132_img" src="images/account_information/u103.svg"/>
		            <div class="text" id="u11132_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11133">
		         <div class="ax_default icon" id="u11134">
		            <img class="img" id="u11134_img" src="images/account_information/u105.svg"/>
		            <div class="text" id="u11134_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u11135">
		            <div class="" id="u11135_div">
		            </div>
		            <div class="text" id="u11135_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Ngày nghỉ
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11136">
		         <div class="ax_default icon" id="u11137">
		            <img class="img" id="u11137_img" src="images/account_information/u108.svg"/>
		            <div class="text" id="u11137_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u11138">
		            <div class="" id="u11138_div">
		            </div>
		            <div class="text" id="u11138_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phòng ban - Vai trò
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11139">
		         <div class="ax_default paragraph" id="u11140">
		            <div class="" id="u11140_div">
		            </div>
		            <div class="text" id="u11140_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trạng thái học viên
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u11141">
		            <img class="img" id="u11141_img" src="images/account_information/u112.svg"/>
		            <div class="text" id="u11141_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11142">
		         <div class="ax_default paragraph" id="u11143">
		            <div class="" id="u11143_div">
		            </div>
		            <div class="text" id="u11143_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tiêu chí đánh giá
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u11144">
		            <img class="img" id="u11144_img" src="images/account_information/u115.svg"/>
		            <div class="text" id="u11144_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11145">
		         <div class="ax_default paragraph" id="u11146">
		            <div class="" id="u11146_div">
		            </div>
		            <div class="text" id="u11146_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Chương trình học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11147">
		            <div class="ax_default icon" id="u11148">
		               <img class="img" id="u11148_img" src="images/account_information/u119.svg"/>
		               <div class="text" id="u11148_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11149">
		         <div class="ax_default icon" id="u11150">
		            <img class="img" id="u11150_img" src="images/account_information/u121.svg"/>
		            <div class="text" id="u11150_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u11151">
		            <div class="" id="u11151_div">
		            </div>
		            <div class="text" id="u11151_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Bài học
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11152">
		         <div class="ax_default paragraph" id="u11153">
		            <div class="" id="u11153_div">
		            </div>
		            <div class="text" id="u11153_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phòng học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u11154">
		            <img class="img" id="u11154_img" src="images/account_information/u125.svg"/>
		            <div class="text" id="u11154_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11155">
		         <div class="ax_default paragraph" id="u11156">
		            <div class="" id="u11156_div">
		            </div>
		            <div class="text" id="u11156_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trường học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u11157">
		            <img class="img" id="u11157_img" src="images/account_information/u128.svg"/>
		            <div class="text" id="u11157_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default line" id="u11158">
		         <img class="img" id="u11158_img" src="images/account_information/u129.svg"/>
		         <div class="text" id="u11158_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u11159">
		         <div class="" id="u11159_div">
		         </div>
		         <div class="text" id="u11159_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Tài chính
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11160">
		         <div class="ax_default paragraph" id="u11161">
		            <div class="" id="u11161_div">
		            </div>
		            <div class="text" id="u11161_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Học phí
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u11162">
		            <img class="img" id="u11162_img" src="images/account_information/u133.svg"/>
		            <div class="text" id="u11162_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11163">
		         <div class="ax_default paragraph" id="u11164">
		            <div class="" id="u11164_div">
		            </div>
		            <div class="text" id="u11164_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Khuyến mại
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u11165">
		            <img class="img" id="u11165_img" src="images/account_information/u136.svg"/>
		            <div class="text" id="u11165_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="classMenu" data-left="11" data-top="252" data-width="143" id="u11166">
		      <div class="ax_default shape" data-label="classIconLable" id="u11167">
		         <img class="img" id="u11167_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u11167_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Giáo viên
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG" id="u11168">
		         <div class="" id="u11168_div">
		         </div>
		         <div class="text" id="u11168_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG2" id="u11169">
		         <div class="" id="u11169_div">
		         </div>
		         <div class="text" id="u11169_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u11170">
		         <img class="img" id="u11170_img" src="images/account_information/u141.svg"/>
		         <div class="text" id="u11170_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u11171">
		      <img class="img" id="u11171_img" src="images/account_information/u142.svg"/>
		      <div class="text" id="u11171_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u11172">
		      <img class="img" id="u11172_img" src="images/account_information/u143.svg"/>
		      <div class="text" id="u11172_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div id="u11079" style={{"display":"none","visibility":"hidden"}}>
		   </div>
		   <div class="ax_default paragraph" id="u11173">
		      <div class="" id="u11173_div">
		      </div>
		      <div class="text" id="u11173_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Chi tiết chăm sóc khách hàng
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default shape" id="u11174">
		      <div class="" id="u11174_div">
		      </div>
		      <div class="text" id="u11174_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default" id="u11175">
		      <div class="ax_default table_cell" id="u11176">
		         <img class="img" id="u11176_img" src="images/chi_ti_t_kh-trao___i/u11176.png"/>
		         <div class="text" id="u11176_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Thảo luận
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11177">
		         <img class="img" id="u11177_img" src="images/chi_ti_t_kh-trao___i/u11177.png"/>
		         <div class="text" id="u11177_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Lịch sử chăm sóc
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11178">
		         <img class="img" id="u11178_img" src="images/chi_ti_t_kh-trao___i/u11177.png"/>
		         <div class="text" id="u11178_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Hóa đơn
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11179">
		         <img class="img" id="u11179_img" src="images/chi_ti_t_kh-trao___i/u11177.png"/>
		         <div class="text" id="u11179_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Điểm danh/ nhận xét
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11180">
		         <img class="img" id="u11180_img" src="images/chi_ti_t_kh-trao___i/u11177.png"/>
		         <div class="text" id="u11180_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Lịch sử khác
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11181">
		         <img class="img" id="u11181_img" src="images/chi_ti_t_kh-trao___i/u11181.png"/>
		         <div class="text" id="u11181_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Kiểm tra/học thử
		               </span>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default shape" id="u11182">
		      <img class="img" id="u11182_img" src="images/chi_ti_t_kh-trao___i/u11182.svg"/>
		      <div class="text" id="u11182_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default" data-height="128" data-left="1581" data-top="130" data-width="178" id="u11183">
		      <div class="ax_default label" id="u11184">
		         <div class="" id="u11184_div">
		         </div>
		         <div class="text" id="u11184_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Hoàng Văn Huy
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default label" id="u11185">
		         <div class="" id="u11185_div">
		         </div>
		         <div class="text" id="u11185_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  HV-000002
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default label" id="u11186">
		         <div class="" id="u11186_div">
		         </div>
		         <div class="text" id="u11186_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  0384455522
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default label" id="u11187">
		         <div class="" id="u11187_div">
		         </div>
		         <div class="text" id="u11187_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  test@qa/team
		               </span>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default shape" id="u11188">
		      <div class="" id="u11188_div">
		      </div>
		      <div class="text" id="u11188_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u11189">
		      <div class="" id="u11189_div">
		      </div>
		      <div class="text" id="u11189_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Gừi thảo luận
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default shape" id="u11190">
		      <div class="" id="u11190_div">
		      </div>
		      <div class="text" id="u11190_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u11191">
		      <img class="img" id="u11191_img" src="images/chi_ti_t_kh-trao___i/u11191.svg"/>
		      <div class="text" id="u11191_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default text_area" id="u11192">
		      <div class="" id="u11192_div">
		      </div>
		      <textarea class="u11192_input" id="u11192_input"></textarea>
		   </div>
		   <div class="ax_default label" id="u11193">
		      <div class="" id="u11193_div">
		      </div>
		      <div class="text" id="u11193_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Hoàng Dung (CS)
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default paragraph" id="u11194">
		      <div class="" id="u11194_div">
		      </div>
		      <div class="text" id="u11194_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget odio. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default label" id="u11195">
		      <div class="" id="u11195_div">
		      </div>
		      <div class="text" id="u11195_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               1 giờ trước
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u11196">
		      <img class="img" id="u11196_img" src="images/chi_ti_t_kh-trao___i/u11191.svg"/>
		      <div class="text" id="u11196_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default label" id="u11197">
		      <div class="" id="u11197_div">
		      </div>
		      <div class="text" id="u11197_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Phạm Minh
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default paragraph" id="u11198">
		      <div class="" id="u11198_div">
		      </div>
		      <div class="text" id="u11198_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget odio. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default label" id="u11199">
		      <div class="" id="u11199_div">
		      </div>
		      <div class="text" id="u11199_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               2 giờ trước
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u11200">
		      <img class="img" id="u11200_img" src="images/chi_ti_t_kh-trao___i/u11191.svg"/>
		      <div class="text" id="u11200_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default label" id="u11201">
		      <div class="" id="u11201_div">
		      </div>
		      <div class="text" id="u11201_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Hoang Thùy
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default paragraph" id="u11202">
		      <div class="" id="u11202_div">
		      </div>
		      <div class="text" id="u11202_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget odio. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default label" id="u11203">
		      <div class="" id="u11203_div">
		      </div>
		      <div class="text" id="u11203_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               1 giờ trước
		            </span>
		         </p>
		      </div>
		   </div>
		</div>
		
      </div>
    );
  }
}

export default Page;
