import React, { useState } from "react";
import { connect } from "dva";
import "react-datepicker/dist/react-datepicker.css";
import { Button, Table } from "react-bootstrap";
import coso from "../../../mock/coso";
import danhmuc from "../../../mock/danhmuc";
import kho from "../../../mock/kho";
import noinhan from "../../../mock/noinhan";
import nhacungcap from "../../../mock/nhacungcap";
import donvi from "../../../mock/donvi";
import kichco from "../../../mock/kichco";
import mausac from "../../../mock/mausac";
import { useHistory } from "react-router-dom";
import "./index.css";
import PageHeader from "../../../components/PageHeader";

const Page = () => {
  const [isCreateNew, setIsCreateNew] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [selectedItem, setSelectedItem] = useState({});
  const [fromDate, setFromDate] = useState(new Date());
  const [toDate, setToDate] = useState(new Date());
  const history = useHistory();
  const handleButtonClick = () => {
    setIsCreateNew(true);
    setShowModal(true);
  };
  const handleViewButtonClick = () => {
    setShowModal(true);
  };
  const handleCloseModal = () => {
    setShowModal(false);
  };
  const handleImportButtonClick = () => {
    history.push("/nhap_hang");
  };
  const handleExportButtonClick = () => {
    history.push("/xuat_hang");
  };
  return (
    <div>
      <PageHeader
        title="Xuất hàng"
        breadcrums={[{
          title: "Home",
          path: "/"
        }, {
          title: "Xuất hàng",
          path: "/xuat_hang"
        }]}
        subTitle="Cho phép xuất sản phẩm với số lượng, giá thành khác nhau trong một lần xuất kho"
      />
      <div className="iq-card n-pb-0">
        <div className="iq-card-body">
          <div className="">
              <form action="" className="w-100">
              <div className="col-12">
                <div className="row font-size-14">
                  <div className="col-md-3 ">
                    <div className="form-group">
                      <label htmlFor="coso">Cơ sở</label>
                      <select name="co_so" className="form-control form-control-lg" id="co_so">
                        {coso.map(item => (
                            <option value={item.value}>{item.name}</option>
                        ))}
                      </select>
                    </div>
                  </div>
                  <div className="col-md-3 ">
                    <div className="form-group">
                      <label htmlFor="coso">Danh mục</label>
                      <select
                          name="danhmuc"
                          className="form-control form-control-lg"
                          id="danhmuc"
                      >
                        {danhmuc.map(item => (
                            <option value={item.value}>{item.name}</option>
                        ))}
                      </select>
                    </div>
                  </div>
                  <div className="col-md-3 ">
                    <div className="form-group">
                      <label htmlFor="coso">Kho</label>
                      <select name="kho" className="form-control form-control-lg" id="kho">
                        {kho.map(item => (
                            <option value={item.value}>{item.name}</option>
                        ))}
                      </select>
                    </div>
                  </div>
                  <div className="col-md-3 ">
                    <div className="form-group">
                      <label htmlFor="coso">Nơi nhận</label>
                      <select
                          name="noinhan"
                          className="form-control form-control-lg"
                          id="noinhan"
                      >
                        {noinhan.map(item => (
                            <option value={item.value}>{item.name}</option>
                        ))}
                      </select>
                    </div>
                  </div>
                  <div className="col-md-3 ">
                    <div className="form-group">
                      <label htmlFor="coso">Người nhận</label>
                      <input
                          type="text"
                          name="nguoinhan"
                          className="form-control form-control-lg"
                          id="nguoinhan"
                      />
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-12">
                    <label htmlFor="dien_giai">Diễn giải</label>
                    <textarea
                        className="w-100"
                        name="dien_giai"
                        id="dien_giai"
                        cols="30"
                        rows="10"
                    ></textarea>
                  </div>
                </div>
                <div className="row">
                  <div className="col-4">
                    <div className="form-group">
                      <label htmlFor="ma_san_pham">Mã sản phẩm</label>
                      <input
                          type="text"
                          id="ma_san_pham"
                          name="ma_san_pham"
                          className="form-control form-control-lg"
                      />
                    </div>
                  </div>
                  <div className="col-4">
                    <div className="form-group">
                      <label htmlFor="ten_san_pham">Tên sản phẩm</label>
                      <input
                          type="text"
                          id="ten_san_pham"
                          name="ten_san_pham"
                          className="form-control form-control-lg"
                      />
                    </div>
                  </div>
                  <div className="col-4">
                    <div className="form-group">
                      <label htmlFor="nha_cung_cap">Nhà cung cấp</label>
                      <select
                          name="nha_cung_cap"
                          className="form-control form-control-lg"
                          id="nha_cung_cap"
                      >
                        {nhacungcap.map(item => (
                            <option value={item.value}>{item.name}</option>
                        ))}
                      </select>
                    </div>
                  </div>
                  <div className="col-4">
                    <div className="form-group">
                      <label htmlFor="gia_nhap">Giá nhập</label>
                      <input
                          type="text"
                          id="gia_nhap"
                          name="gia_nhap"
                          className="form-control form-control-lg"
                      />
                    </div>
                  </div>
                  <div className="col-4">
                    <div className="form-group">
                      <label htmlFor="nha_cung_cap">Đơn vị</label>
                      <select name="don_vi" className="form-control form-control-lg" id="don_vi">
                        {donvi.map(item => (
                            <option value={item.value}>{item.name}</option>
                        ))}
                      </select>
                    </div>
                  </div>
                  <div className="col-4">
                    <div className="form-group">
                      <label htmlFor="nha_cung_cap">Kích cỡ</label>
                      <select
                          name="kich_co"
                          className="form-control form-control-lg"
                          id="kich_co"
                      >
                        {kichco.map(item => (
                            <option value={item.value}>{item.name}</option>
                        ))}
                      </select>
                    </div>
                  </div>
                  <div className="col-4">
                    <div className="form-group">
                      <label htmlFor="so_luong">Số lượng</label>
                      <input
                          type="text"
                          id="so_luong"
                          name="so_luong"
                          className="form-control form-control-lg"
                      />
                    </div>
                  </div>
                  <div className="col-4">
                    <div className="form-group">
                      <label htmlFor="gia_xuat">Giá xuất dự kiến</label>
                      <input
                          type="text"
                          id="gia_xuat"
                          name="gia_xuat"
                          className="form-control form-control-lg"
                      />
                    </div>
                  </div>
                  <div className="col-4">
                    <div className="form-group">
                      <label htmlFor="nha_cung_cap">Màu sắc</label>
                      <select
                          name="mau_sac"
                          className="form-control form-control-lg"
                          id="mau_sac"
                      >
                        {mausac.map(item => (
                            <option value={item.value}>{item.name}</option>
                        ))}
                      </select>
                    </div>
                  </div>
                  <div className="col-12">
                    <Button
                        variant="primary"
                        type="submit"
                        className="float-right"
                        onClick={handleButtonClick}
                    >
                      Thêm sản phẩm
                    </Button>
                  </div>
                </div>
              </div>
            </form>
            <hr/>
            <div className="row  mt-4">
              <div className="col-12">
                <div className="w-100">
                  <Table bordered hover className="table">
                    <thead>
                    <tr>
                      <th>Mã sản phẩm</th>
                      <th>Tên sản phẩm</th>
                      <th>Số lượng</th>
                      <th>Giá xuất</th>
                      <th>Tổng cộng</th>
                      <th>Danh mục</th>
                      <th>Đơn vị</th>
                      <th>Kích cỡ</th>
                      <th>Màu sắc</th>
                      <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                      <td>123</td>
                      <td>Sách Tiếng Anh 1</td>
                      <td>50</td>
                      <td>50.000 VNĐ</td>
                      <td>2.500.000 VNĐ</td>
                      <td>Dụng cụ học tập</td>
                      <td>Quyển</td>
                      <td></td>
                      <td></td>
                      <td>
                        <i className="far fa-trash-alt"></i>
                      </td>
                    </tr>
                    <tr>
                      <td colSpan="10">2.500.000 VNĐ</td>
                    </tr>
                    </tbody>
                  </Table>
                  <div className="not-found">
                    <i className="fas fa-inbox"></i>
                    <span>Không tìm thấy kết quả</span>
                  </div>
                  {/*{showModal && (<CustomModal isCreateNew={isCreateNew} selectedItem={selectedItem} handleCloseModal={handleCloseModal}/>)}*/}
                </div>
              </div>
              <div className="col-12 m-t-2">
                <Button type="primary" className="float-right">
                  Tạo phiếu xuất kho
                </Button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default connect(({ Mock }) => ({
  Mock
}))(Page);
