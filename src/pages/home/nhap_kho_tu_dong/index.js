import React, { useState } from "react";
import {connect} from "dva";
import "react-datepicker/dist/react-datepicker.css";
import {Button, Table} from "react-bootstrap";
import coso from "../../../mock/coso";
import danhmuc from "../../../mock/danhmuc";
import kho from "../../../mock/kho";
import noinhan from "../../../mock/noinhan";
import nhacungcap from "../../../mock/nhacungcap";
import donvi from "../../../mock/donvi";
import kichco from "../../../mock/kichco";
import mausac from "../../../mock/mausac";
import { useHistory } from 'react-router-dom';
import './index.css';

const Page = () => {
  const [isCreateNew, setIsCreateNew] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [selectedItem, setSelectedItem] = useState({});
  const [fromDate, setFromDate] = useState(new Date());
  const [toDate, setToDate] = useState(new Date());
  const history = useHistory();
  const handleButtonClick = () => {
    setIsCreateNew(true);
    setShowModal(true);
  };
  const handleViewButtonClick = () => {
    setShowModal(true);
  }
  const handleCloseModal = () => {
    setShowModal(false);
  }
  const handleImportButtonClick = () => {
    history.push('/nhap_hang');
  }
  const handleExportButtonClick = () => {
    history.push('/xuat_hang');
  }
  return (
    <div className="iq-card n-pb-0">
      <div className="iq-card-body">
        <div className="row ">
          <div className="col-12">
            <h1 className="card-title font-bold">Nhập kho tự động</h1>
            <p>Cho phép nhập kho nhanh từ những tư liệu đã có sẵn.</p>
          </div>
        </div>
        <div className="">
          <form action="" className="w-100">
            <div className="col-12">
              <div className="row font-size-14">
                <div className="col-md-4 ">
                  <div className="form-group">
                    <label htmlFor="coso">Cơ sở</label>
                    <select
                      name="co_so"
                      className="form-control form-control-lg"
                      id="co_so">
                      {coso.map(item =>  (
                        <option value={item.value}>{item.name}</option>
                      ))}
                    </select>
                  </div>
                </div>
                <div className="col-md-4 ">
                  <div className="form-group">
                    <label htmlFor="coso">Danh mục</label>
                    <select
                      name="danhmuc"
                      className="form-control form-control-lg"
                      id="danhmuc">
                      {danhmuc.map(item =>  (
                        <option value={item.value}>{item.name}</option>
                      ))}
                    </select>
                  </div>
                </div>
                <div className="col-md-4 ">
                  <div className="form-group">
                    <label htmlFor="coso">Kho</label>
                    <select
                      name="kho"
                      className="form-control form-control-lg"
                      id="kho">
                      {kho.map(item =>  (
                        <option value={item.value}>{item.name}</option>
                      ))}
                    </select>
                  </div>
                </div>
                <div className="col-md-4 ">
                  <div className="form-group">
                    <label htmlFor="nha_cung_cap">Nhà cung cấp</label>
                    <select
                      name="nha_cung_cap"
                      className="form-control form-control-lg"
                      id="nha_cung_cap">
                      {nhacungcap.map(item =>  (
                        <option value={item.value}>{item.name}</option>
                      ))}
                    </select>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-12">
                  <label htmlFor="dien_giai">Diễn giải</label>
                  <textarea className="w-100" name="dien_giai" id="dien_giai" cols="30" rows="10"></textarea>
                </div>
                <div className="col-12">
                  <div className="form-group">
                    <label htmlFor="inputGroupFile02">Tải lên</label>
                    <div className="input-group mb-3">
                      <div className="custom-file">
                        <input type="file" className="custom-file-input" id="inputGroupFile02" />
                        <label className="custom-file-label" htmlFor="inputGroupFile02">Choose file</label>
                      </div>
                      {/*<div className="input-group-append">*/}
                      {/*  <span className="input-group-text" id="">Upload</span>*/}
                      {/*</div>*/}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
          <div className="row  mt-4">
            <div className="col-12">
              <Button type="primary" className="float-right">Tạo phiếu nhập kho</Button>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default connect(({Mock}) => ({
  Mock
}))(Page);
