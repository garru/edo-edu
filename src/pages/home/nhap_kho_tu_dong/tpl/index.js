import React from 'react';
import './index.css';
import './ax.css';

class Page extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
		<div class="" id="base" style={{"margin-left":"-150px"}}>
		   <div class="ax_default shape" id="u5140">
		      <div class="" id="u5140_div">
		      </div>
		      <div class="text" id="u5140_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default shape" id="u5141">
		      <div class="" id="u5141_div">
		      </div>
		      <div class="text" id="u5141_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default" data-height="40" data-label="account" data-left="10" data-top="895" data-width="192" id="u5143">
		      <div class="ax_default shape" data-label="accountLable" id="u5144">
		         <img class="img" id="u5144_img" src="images/account_information/accountlable_u55.svg"/>
		         <div class="text" id="u5144_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Nguyen Van A
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u5145">
		         <img class="img" id="u5145_img" src="images/login/u4.svg"/>
		         <div class="text" id="u5145_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="settingMenu" data-left="8" data-top="826" data-width="125" id="u5146">
		      <div class="ax_default shape" data-label="gearIconLable" id="u5147">
		         <img class="img" id="u5147_img" src="images/account_information/geariconlable_u58.svg"/>
		         <div class="text" id="u5147_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Cài đặt
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="44" data-left="8" data-top="826" data-width="44" id="u5148">
		         <div class="ax_default shape" data-label="gearIconBG" id="u5149">
		            <div class="" id="u5149_div">
		            </div>
		            <div class="text" id="u5149_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default shape" data-label="gearIconBG2" id="u5150">
		            <div class="" id="u5150_div">
		            </div>
		            <div class="text" id="u5150_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" data-label="gearIcon" id="u5151">
		            <img class="img" id="u5151_img" src="images/account_information/gearicon_u62.svg"/>
		            <div class="text" id="u5151_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="customerMenu" data-left="8" data-top="145" data-width="143" id="u5152">
		      <div class="ax_default shape" data-label="customerIconLable" id="u5153">
		         <img class="img" id="u5153_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u5153_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Khách hàng
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="customerIconBG" id="u5154">
		         <div class="" id="u5154_div">
		         </div>
		         <div class="text" id="u5154_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="customerIconBG2" id="u5155">
		         <div class="" id="u5155_div">
		         </div>
		         <div class="text" id="u5155_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" data-label="customer" id="u5156">
		         <img class="img" id="u5156_img" src="images/account_information/customer_u67.svg"/>
		         <div class="text" id="u5156_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="classMenu" data-left="8" data-top="198" data-width="143" id="u5157">
		      <div class="ax_default shape" data-label="classIconLable" id="u5158">
		         <img class="img" id="u5158_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u5158_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Lớp học
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG" id="u5159">
		         <div class="" id="u5159_div">
		         </div>
		         <div class="text" id="u5159_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG2" id="u5160">
		         <div class="" id="u5160_div">
		         </div>
		         <div class="text" id="u5160_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" data-label="classicon" id="u5161">
		         <img class="img" id="u5161_img" src="images/account_information/classicon_u72.svg"/>
		         <div class="text" id="u5161_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="dashboardMenu" data-left="8" data-top="91" data-width="143" id="u5162">
		      <div class="ax_default shape" data-label="dashboardIconLable" id="u5163">
		         <img class="img" id="u5163_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u5163_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Dashnoard
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="dashboardIconBG" id="u5164">
		         <div class="" id="u5164_div">
		         </div>
		         <div class="text" id="u5164_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="dashboardIconBG2" id="u5165">
		         <div class="" id="u5165_div">
		         </div>
		         <div class="text" id="u5165_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" data-label="dashboard" id="u5166">
		         <img class="img" id="u5166_img" src="images/account_information/dashboard_u77.svg"/>
		         <div class="text" id="u5166_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="129" data-label="AccountMenu" data-left="55" data-top="806" data-width="246" id="u5167" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default shape" id="u5168">
		         <div class="" id="u5168_div">
		         </div>
		         <div class="text" id="u5168_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u5169">
		         <div class="" id="u5169_div">
		         </div>
		         <div class="text" id="u5169_text">
		            <p>
		               <span style={{"font-family":"ArialMT, Arial","font-weight":"400","text-decoration":"none"}}>
		                  Xin chào:
		               </span>
		               <span style={{"font-family":"Arial Bold, Arial","font-weight":"700","text-decoration":"none"}}>
		                  Nguyen Van A
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="-3" data-top="0" data-width="0" id="u5170">
		         <div class="ax_default image" id="u5171">
		            <img class="img" id="u5171_img" src="images/account_information/u82.png"/>
		            <div class="text" id="u5171_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u5172">
		            <div class="" id="u5172_div">
		            </div>
		            <div class="text" id="u5172_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Đăng xuất
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default line" id="u5173">
		         <img class="img" id="u5173_img" src="images/account_information/u84.svg"/>
		         <div class="text" id="u5173_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="-3" data-top="0" data-width="0" id="u5174">
		         <div class="ax_default paragraph" id="u5175">
		            <div class="" id="u5175_div">
		            </div>
		            <div class="text" id="u5175_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tài khoản
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default image" id="u5176">
		            <img class="img" id="u5176_img" src="images/account_information/u87.png"/>
		            <div class="text" id="u5176_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="339" data-label="SettingPannel" data-left="55" data-top="525" data-width="339" id="u5177" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default shape" id="u5178">
		         <div class="" id="u5178_div">
		         </div>
		         <div class="text" id="u5178_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u5179">
		         <div class="" id="u5179_div">
		         </div>
		         <div class="text" id="u5179_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Hệ thống
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="-3" data-top="0" data-width="0" id="u5180">
		         <div class="ax_default icon" id="u5181">
		            <img class="img" id="u5181_img" src="images/account_information/u92.svg"/>
		            <div class="text" id="u5181_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u5182">
		            <div class="" id="u5182_div">
		            </div>
		            <div class="text" id="u5182_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tài khoản
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="-3" data-top="0" data-width="0" id="u5183">
		         <div class="ax_default paragraph" id="u5184">
		            <div class="" id="u5184_div">
		            </div>
		            <div class="text" id="u5184_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phân quyền
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u5185">
		            <img class="img" id="u5185_img" src="images/account_information/u96.svg"/>
		            <div class="text" id="u5185_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u5186">
		         <div class="" id="u5186_div">
		         </div>
		         <div class="text" id="u5186_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Đào tạo
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="-3" data-top="0" data-width="0" id="u5187">
		         <div class="ax_default paragraph" id="u5188">
		            <div class="" id="u5188_div">
		            </div>
		            <div class="text" id="u5188_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Thương hiệu
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u5189">
		            <img class="img" id="u5189_img" src="images/account_information/u100.svg"/>
		            <div class="text" id="u5189_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="-3" data-top="0" data-width="0" id="u5190">
		         <div class="ax_default paragraph" id="u5191">
		            <div class="" id="u5191_div">
		            </div>
		            <div class="text" id="u5191_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trung tâm
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u5192">
		            <img class="img" id="u5192_img" src="images/account_information/u103.svg"/>
		            <div class="text" id="u5192_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="-3" data-top="0" data-width="0" id="u5193">
		         <div class="ax_default icon" id="u5194">
		            <img class="img" id="u5194_img" src="images/account_information/u105.svg"/>
		            <div class="text" id="u5194_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u5195">
		            <div class="" id="u5195_div">
		            </div>
		            <div class="text" id="u5195_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Ngày nghỉ
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="-3" data-top="0" data-width="0" id="u5196">
		         <div class="ax_default icon" id="u5197">
		            <img class="img" id="u5197_img" src="images/account_information/u108.svg"/>
		            <div class="text" id="u5197_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u5198">
		            <div class="" id="u5198_div">
		            </div>
		            <div class="text" id="u5198_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phòng ban - Vai trò
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="-3" data-top="0" data-width="0" id="u5199">
		         <div class="ax_default paragraph" id="u5200">
		            <div class="" id="u5200_div">
		            </div>
		            <div class="text" id="u5200_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trạng thái học viên
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u5201">
		            <img class="img" id="u5201_img" src="images/account_information/u112.svg"/>
		            <div class="text" id="u5201_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="-3" data-top="0" data-width="0" id="u5202">
		         <div class="ax_default paragraph" id="u5203">
		            <div class="" id="u5203_div">
		            </div>
		            <div class="text" id="u5203_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tiêu chí đánh giá
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u5204">
		            <img class="img" id="u5204_img" src="images/account_information/u115.svg"/>
		            <div class="text" id="u5204_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="-3" data-top="0" data-width="0" id="u5205">
		         <div class="ax_default paragraph" id="u5206">
		            <div class="" id="u5206_div">
		            </div>
		            <div class="text" id="u5206_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Chương trình học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default" data-height="0" data-left="-3" data-top="0" data-width="0" id="u5207">
		            <div class="ax_default icon" id="u5208">
		               <img class="img" id="u5208_img" src="images/account_information/u119.svg"/>
		               <div class="text" id="u5208_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="-3" data-top="0" data-width="0" id="u5209">
		         <div class="ax_default icon" id="u5210">
		            <img class="img" id="u5210_img" src="images/account_information/u121.svg"/>
		            <div class="text" id="u5210_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u5211">
		            <div class="" id="u5211_div">
		            </div>
		            <div class="text" id="u5211_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Bài học
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="-3" data-top="0" data-width="0" id="u5212">
		         <div class="ax_default paragraph" id="u5213">
		            <div class="" id="u5213_div">
		            </div>
		            <div class="text" id="u5213_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phòng học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u5214">
		            <img class="img" id="u5214_img" src="images/account_information/u125.svg"/>
		            <div class="text" id="u5214_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="-3" data-top="0" data-width="0" id="u5215">
		         <div class="ax_default paragraph" id="u5216">
		            <div class="" id="u5216_div">
		            </div>
		            <div class="text" id="u5216_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trường học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u5217">
		            <img class="img" id="u5217_img" src="images/account_information/u128.svg"/>
		            <div class="text" id="u5217_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default line" id="u5218">
		         <img class="img" id="u5218_img" src="images/account_information/u129.svg"/>
		         <div class="text" id="u5218_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u5219">
		         <div class="" id="u5219_div">
		         </div>
		         <div class="text" id="u5219_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Tài chính
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="-3" data-top="0" data-width="0" id="u5220">
		         <div class="ax_default paragraph" id="u5221">
		            <div class="" id="u5221_div">
		            </div>
		            <div class="text" id="u5221_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Học phí
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u5222">
		            <img class="img" id="u5222_img" src="images/account_information/u133.svg"/>
		            <div class="text" id="u5222_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="-3" data-top="0" data-width="0" id="u5223">
		         <div class="ax_default paragraph" id="u5224">
		            <div class="" id="u5224_div">
		            </div>
		            <div class="text" id="u5224_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Khuyến mại
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u5225">
		            <img class="img" id="u5225_img" src="images/account_information/u136.svg"/>
		            <div class="text" id="u5225_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="classMenu" data-left="8" data-top="252" data-width="143" id="u5226">
		      <div class="ax_default shape" data-label="classIconLable" id="u5227">
		         <img class="img" id="u5227_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u5227_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Giáo viên
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG" id="u5228">
		         <div class="" id="u5228_div">
		         </div>
		         <div class="text" id="u5228_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG2" id="u5229">
		         <div class="" id="u5229_div">
		         </div>
		         <div class="text" id="u5229_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u5230">
		         <img class="img" id="u5230_img" src="images/account_information/u141.svg"/>
		         <div class="text" id="u5230_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u5231">
		      <img class="img" id="u5231_img" src="images/account_information/u142.svg"/>
		      <div class="text" id="u5231_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u5232">
		      <img class="img" id="u5232_img" src="images/account_information/u143.svg"/>
		      <div class="text" id="u5232_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div id="u5139" style={{"display":"none","visibility":"hidden"}}>
		   </div>
		   <div class="ax_default" data-height="55" data-label="Header" data-left="133" data-top="17" data-width="528" id="u5233">
		      <div class="ax_default paragraph" id="u5234">
		         <div class="" id="u5234_div">
		         </div>
		         <div class="text" id="u5234_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Nhập kho tự động
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u5235">
		         <div class="" id="u5235_div">
		         </div>
		         <div class="text" id="u5235_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Cho phép nhập kho nhanh từ những tư liệu đã có sẵn.
		               </span>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="134" data-left="182" data-top="98" data-width="1184" id="u5236">
		      <div class="ax_default flow_shape" id="u5237">
		         <div class="" id="u5237_div">
		         </div>
		         <div class="text" id="u5237_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="22" data-left="251" data-top="133" data-width="288" id="u5238">
		         <div class="ax_default heading_3" id="u5239">
		            <div class="" id="u5239_div">
		            </div>
		            <div class="text" id="u5239_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Cơ sở
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default droplist" id="u5240">
		            <div class="" id="u5240_div">
		            </div>
		            <select class="u5240_input" id="u5240_input">
		               <option class="u5240_input_option" selected="" value="Chọn 1 giá trị">
		                  Chọn 1 giá trị
		               </option>
		               <option class="u5240_input_option" value="Hoàng Ngân">
		                  Hoàng Ngân
		               </option>
		               <option class="u5240_input_option" value="Hội sở">
		                  Hội sở
		               </option>
		               <option class="u5240_input_option" value="Văn Điển">
		                  Văn Điển
		               </option>
		            </select>
		         </div>
		      </div>
		      <div class="ax_default" data-height="35" data-left="251" data-top="177" data-width="290" id="u5241">
		         <div class="ax_default heading_3" id="u5242">
		            <div class="" id="u5242_div">
		            </div>
		            <div class="text" id="u5242_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Nhà cung cấp
		                  </span>
		               </p>
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     <br/>
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default droplist" id="u5243">
		            <div class="" id="u5243_div">
		            </div>
		            <select class="u5243_input" id="u5243_input">
		               <option class="u5243_input_option" selected="" value="Chọn 1 giá trị">
		                  Chọn 1 giá trị
		               </option>
		               <option class="u5243_input_option" value="Nhà cung cấp 1">
		                  Nhà cung cấp 1
		               </option>
		               <option class="u5243_input_option" value="Nhà cung cấp 2">
		                  Nhà cung cấp 2
		               </option>
		            </select>
		         </div>
		      </div>
		      <div class="ax_default" data-height="22" data-left="1011" data-top="131" data-width="290" id="u5244">
		         <div class="ax_default heading_3" id="u5245">
		            <div class="" id="u5245_div">
		            </div>
		            <div class="text" id="u5245_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Kho
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default droplist" id="u5246">
		            <div class="" id="u5246_div">
		            </div>
		            <select class="u5246_input" id="u5246_input">
		               <option class="u5246_input_option" selected="" value="Chọn 1 giá trị">
		                  Chọn 1 giá trị
		               </option>
		               <option class="u5246_input_option" value="Hoàng Ngân">
		                  Hoàng Ngân
		               </option>
		               <option class="u5246_input_option" value="Hội sở">
		                  Hội sở
		               </option>
		               <option class="u5246_input_option" value="Văn Điển">
		                  Văn Điển
		               </option>
		            </select>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="260" data-left="182" data-top="249" data-width="1185" id="u5247">
		      <div class="ax_default flow_shape" id="u5248">
		         <div class="" id="u5248_div">
		         </div>
		         <div class="text" id="u5248_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default heading_3" id="u5249">
		         <div class="" id="u5249_div">
		         </div>
		         <div class="text" id="u5249_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Diễn giải
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default text_field" id="u5250">
		         <div class="" id="u5250_div">
		         </div>
		         <input class="u5250_input" id="u5250_input" type="text" value=""/>
		      </div>
		   </div>
		   <div class="ax_default" data-height="107" data-left="182" data-top="526" data-width="1185" id="u5251">
		      <div class="ax_default flow_shape" id="u5252">
		         <div class="" id="u5252_div">
		         </div>
		         <div class="text" id="u5252_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default heading_3" id="u5253">
		         <div class="" id="u5253_div">
		         </div>
		         <div class="text" id="u5253_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Tải lên
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default text_field" id="u5254">
		         <div class="" id="u5254_div">
		         </div>
		         <input class="u5254_input" id="u5254_input" type="text" value=""/>
		      </div>
		      <div class="ax_default button" id="u5255">
		         <div class="" id="u5255_div">
		         </div>
		         <div class="text" id="u5255_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Tìm
		               </span>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default heading_3" id="u5256">
		      <div class="" id="u5256_div">
		      </div>
		      <div class="text" id="u5256_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Chỉ chấp nhận với file Excel xlsx hoặc xls
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u5257">
		      <div class="" id="u5257_div">
		      </div>
		      <div class="text" id="u5257_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Tạo phiếu nhập kho
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default" data-height="25" data-left="631" data-top="131" data-width="292" id="u5258">
		      <div class="ax_default heading_3" id="u5259">
		         <div class="" id="u5259_div">
		         </div>
		         <div class="text" id="u5259_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Danh mục
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default droplist" id="u5260">
		         <div class="" id="u5260_div">
		         </div>
		         <select class="u5260_input" id="u5260_input">
		            <option class="u5260_input_option" selected="" value="Chọn 1 giá trị">
		               Chọn 1 giá trị
		            </option>
		            <option class="u5260_input_option" value="Dụng cụ học tập">
		               Dụng cụ học tập
		            </option>
		            <option class="u5260_input_option" value="Cơ sở vật chất">
		               Cơ sở vật chất
		            </option>
		         </select>
		      </div>
		   </div>
		</div>
		
      </div>
    );
  }
}

export default Page;
