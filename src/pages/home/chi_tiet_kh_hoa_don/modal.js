import React from "react";
import { Modal, Button } from "react-bootstrap";
import { connect } from "dva";
import moment from "moment";

const CustomModal = ({ handleClose, CustomerDetail }) => {
  return (
    <Modal show size="lg">
      <Modal.Header closeButton>
        <Modal.Title>Thông tin hóa đơn</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="row">
          <div className="col-4"></div>
          <div className="col-4 text-center">
            <h2 className="font-bold">Phiếu Thu</h2>
            <p>
              {moment(CustomerDetail?.current_hoa_don?.issueDate).format(
                "DD/MM/YYYY"
              )}
            </p>
          </div>
          <div className="col-4 text-right">
            <div className="text-nowrap">
              Số kế toán:{CustomerDetail?.current_hoa_don?.code}
            </div>
            <div className="text-nowrap">
              Số phiếu:{CustomerDetail?.current_hoa_don?.code}
            </div>
            <div className="text-nowrap">
              Tài khoản nợ:{CustomerDetail?.current_hoa_don?.code}
            </div>
            <div className="text-nowrap">
              Tài khoản có:{CustomerDetail?.current_hoa_don?.code}
            </div>
          </div>
        </div>
        <div className="row mt-4 g-4">
          <div className="col-4">
            <p>Họ và tên</p>
          </div>
          <div className="col-8">
            <p>
              <b>{CustomerDetail?.current_hoa_don?.partnerName}</b>
            </p>
          </div>
          <div className="col-4">
            <p>Về khoản</p>
          </div>
          <div className="col-8">
            <p>
              <b>{CustomerDetail?.current_hoa_don?.description}</b>
            </p>
          </div>
          <div className="col-4">
            <p>Tổng số tiền</p>
          </div>
          <div className="col-8">
            <p>
              <b>{CustomerDetail?.current_hoa_don?.invoiceAmount}</b>
            </p>
          </div>
          <div className="col-4">
            <p>Đã đặt cọc</p>
          </div>
          <div className="col-8">
            <p>
              <b>{CustomerDetail?.current_hoa_don?.depositAmount}</b>
            </p>
          </div>
          <div className="col-4">
            <p>Tổng tiền thanh toán</p>
          </div>
          <div className="col-8">
            <p>
              <b>{CustomerDetail?.current_hoa_don?.collectAmount}</b>
            </p>
          </div>
          <div className="col-4">
            <p>Bằng chữ</p>
          </div>
          <div className="col-8">
            <p>
              <b>{CustomerDetail?.current_hoa_don?.amountByWord}</b>
            </p>
          </div>
        </div>
        <div className="row justify-content-between mt-4 text-center">
          <div className="col-3 text-uppercase">
            <p>&nbsp;</p>
            <p>
              <b>Người lập phiếu</b>
            </p>
          </div>
          <div className="col-2 text-uppercase">
            <p>&nbsp;</p>
            <p>
              <b>Kế toán</b>
            </p>
          </div>
          <div className="col-2 text-uppercase">
            <p>&nbsp;</p>
            <p>
              <b>Thủ quỹ</b>
            </p>
          </div>
          <div className="col-2 text-uppercase">
            <p>&nbsp;</p>
            <p>
              <b>Giám đốc</b>
            </p>
          </div>
          <div className="col-3">
            <p>{moment().format("DD/MM/YYYY")}</p>
            <p className="text-uppercase"><b>Người nộp tiền</b></p>
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <div className="col-12 text-right">
          <Button
            variant={"primary"}
            onClick={() => handleClose()}
            style={{ marginRight: 15 }}
          >
            In lại
          </Button>
          <Button variant={"secondary"} onClick={() => handleClose()}>
            Đóng
          </Button>
        </div>
      </Modal.Footer>
    </Modal>
  );
};

export default connect(({ CustomerDetail, Global }) => ({
  CustomerDetail,
  Global
}))(CustomModal);
