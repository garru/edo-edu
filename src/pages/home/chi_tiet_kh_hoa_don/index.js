import React, { useState, useEffect } from "react";
import { Nav, Overlay, Popover } from "react-bootstrap";
import { connect } from "dva";
import CustomModal from "./modal";
import "react-datepicker/dist/react-datepicker.css";
import { useParams } from "react-router-dom";
import CustomTable from "../../../components/Table";
import UploadAvatar from "../../../components/UploadAvatar";
import PageHeader from "../../../components/PageHeader";

const Tooltip = ({ show, target, handleMenuClick, setShowModal, id }) => {
  console.log("Tooltip", show, target, id);
  return (
    <Overlay
      show={show}
      target={target}
      placement="bottom"
      containerPadding={20}
      rootClose={true}
      rootCloseEvent={"click"}
      onHide={() => setShowModal(false)}
    >
      <Popover id="popover-contained">
        <Popover.Content>
          <ul>
            <li onClick={() => handleMenuClick(0, id)}>
              <i className="fa fa-info"></i>&nbsp; Chi tiết
            </li>
            <li onClick={() => handleMenuClick(1, id)}>
              <i className="fa fa-edit"></i>&nbsp; Thanh toán/chỉnh sửa
            </li>
          </ul>
        </Popover.Content>
      </Popover>
    </Overlay>
  );
};

const Page = props => {
  const [_show_modal, setShowModal] = useState(false);
  const [_show_tooltip, setShowTooltip] = useState(false);
  const [_tooltip_target, setTooltipTarget] = useState();
  const [count, setCount] = useState(0);
  const { id } = useParams();
  const { dispatch } = props;

  const bre = {
    title: "Chi tiết chăm sóc khách hàng",
    breadcrums: [
      {
        title: "Home",
        path: "/"
      },
      {
        title: "Chi tiết chăm sóc khách hàng",
        path: ""
      }
    ]
  };

  useEffect(() => {
    dispatch({
      type: "Global/showLoading"
    });
    Promise.all([
      dispatch({
        type: "CustomerDetail/view",
        payload: { id }
      }),
      dispatch({
        type: "CustomerDetail/listinvoice",
        payload: { id }
      })
    ]).then(data => {
      dispatch({
        type: "Global/hideLoading"
      });
    });
  }, [count]);

  const handleClickTooltipMenu = (menu, id) => {
    switch (menu) {
      case 0:
        dispatch({
          type: "CustomerDetail/printinvoice",
          payload: { invoiceId: id }
        }).then(data => {
          dispatch({
            type: "Global/hideLoading"
          });
          if (data) {
            setShowModal(true);
          } else {
            dispatch({
              type: "Global/showError"
            });
          }
        });

        break;
      case 1:
        break;
      default:
        break;
    }
    setShowTooltip(false);
  };
  const handleClickTooltip = e => {
    setShowTooltip(true);
    setTooltipTarget(e.target);
  };
  const handleModalClose = () => {
    setShowModal(false);
  };
  const columns = [
    {
      title: "Mã hóa đơn",
      key: "code",
      class: "tb-width-100"
    },
    {
      title: "Người đóng",
      key: "partnerName",
      class: "tb-width-100"
    },
    {
      title: "Nội dung",
      key: "description",
      class: "tb-width-300"
    },
    {
      title: "Tổng tiền",
      key: "invoiceAmount",
      class: "tb-width-100"
    },
    {
      title: "Trạng thái",
      key: "status",
      class: "tb-width-100"
    },
    {
      title: "Thao tác",
      key: "lastName",
      class: "tb-width-100",
      render: (col, index) => (
        <>
          <i className="fa fa-ellipsis-h" onClick={handleClickTooltip} />
          <Tooltip
            show={_show_tooltip}
            target={_tooltip_target}
            handleMenuClick={handleClickTooltipMenu}
            setShowTooltip={setShowTooltip}
            setShowModal={setShowModal}
            id={col.id}
          />
        </>
      )
    }
  ];
  return (
    <>
      <PageHeader {...bre} />
      <div className="iq-card ">
        <div className="iq-card-body">
          <div className="">
            <div className="row">
              <div className="col-12">
                <Nav fill variant="pills" className="m-b-2" defaultActiveKey={`/hoa_don/${id}`}>
                  <Nav.Item>
                    <Nav.Link key="trao_doi" href={`/trao_doi/${id}`}>
                      Thảo luận
                    </Nav.Link>
                  </Nav.Item>
                  <Nav.Item>
                    <Nav.Link key="cham_soc" href={`/cham_soc/${id}`}>
                      Lịch sử chăm sóc
                    </Nav.Link>
                  </Nav.Item>
                  <Nav.Item>
                    <Nav.Link key="hoa_don" href={`/hoa_don/${id}`}>
                      Hóa đơn
                    </Nav.Link>
                  </Nav.Item>
                  <Nav.Item>
                    <Nav.Link key="dd_nhan_xet" href={`/dd_nhan_xet/${id}`}>
                      Điểm danh/ nhận xét
                    </Nav.Link>
                  </Nav.Item>
                  <Nav.Item>
                    <Nav.Link key="lich_su_hoc" href={`/lich_su_hoc/${id}`}>
                      Lịch sử khác
                    </Nav.Link>
                  </Nav.Item>
                  {/*<Nav.Item>*/}
                  {/*  <Nav.Link href="/kiem_tra_hoc_thu">Kiểm tra/học thử</Nav.Link>*/}
                  {/*</Nav.Item>*/}
                </Nav>
              </div>
              <div className="col-12">
                <div className="row ">
                  <div className="col-lg-3 col-md-12 col-sm-12 text-center">
                    <UploadAvatar
                      // onChange={onChangeAvatar}
                      className="m-t-2"
                      src={props.curent_customer_detail?.avatar}
                    ></UploadAvatar>
                  </div>
                  <div className="col-lg-9 col-md-12 m-t-4">
                    <CustomTable
                      dataSource={props.CustomerDetail?.list_hoa_don}
                      total={0}
                      columns={columns}
                      onChange={data => {}}
                      noPaging
                    ></CustomTable>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {_show_modal && <CustomModal handleClose={handleModalClose} />}
      </div>
    </>
  );
};

export default connect(({ CustomerDetail, Global }) => ({
  CustomerDetail,
  Global
}))(Page);
