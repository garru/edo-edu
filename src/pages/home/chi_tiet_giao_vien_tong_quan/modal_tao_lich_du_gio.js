import React, { Component, useState, useRef, useEffect } from "react";
import { useForm } from "react-hook-form";

import { Modal, Button, Table } from "react-bootstrap";
import {
  NOI_SINH,
  NGUYEN_QUAN,
  QUOC_TICH,
  HON_NHAN,
  TRUNG_TAM
} from "../../../mock/dropdown";

export default function CustomModal(props) {
  const [show, setShow] = useState(true);
  const handleClose = () => {
    // setShow(false);
    props.onClose();
  };
  const { register, handleSubmit, errors } = useForm();
  const emailReg = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  const { isCreateNew, selectedItem } = props;
  return (
    <Modal
      size={"lg"}
      show={show}
      onHide={handleClose}
      backdrop="static"
      keyboard={false}
      id="AddNew"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title>Chi tiết nhận xét của phụ huynh</Modal.Title>
      </Modal.Header>
      <Modal.Body style={{ padding: 30}}>
        <div className="row">
          <div className="col-lg-6 col-md-6 col-sm-12">
            <div className="form-group">
              <label htmlFor="giao_vien">Giáo viên:</label>
              <input
                type="text"
                name="giao_vien"
                className="form-control form-control-lg"
                id="giao_vien"
                placeholder="Nhập tên giáo viên"
                // ref={register({ required: true, minLength: 8 })}
              ></input>
            </div>
          </div>
          <div className="col-lg-6 col-md-6 col-sm-12">
            <div className="form-group">
              <label htmlFor="trung_tam">Trung tâm:</label>
              <select className="form-control form-control-lg" id="trung_tam" name="trung_tam">
                {TRUNG_TAM.map(item => (
                  <option
                    value={item.code}
                    disabled={item.code !== "" ? false : true}
                    selected={item.code !== "" ? false : true}
                    hidden={item.code !== "" ? false : true}
                  >
                    {item.text}
                  </option>
                ))}
              </select>
            </div>
          </div>
          <div className="col-lg-6 col-md-6 col-sm-12">
            <div className="form-group">
              <label htmlFor="nguoi_tham_gia">Người tham gia :</label>
              <select
                className="form-control form-control-lg"
                id="nguoi_tham_gia"
                name="nguoi_tham_gia"
              >
                {TRUNG_TAM.map(item => (
                  <option
                    value={item.code}
                    disabled={item.code !== "" ? false : true}
                    selected={item.code !== "" ? false : true}
                    hidden={item.code !== "" ? false : true}
                  >
                    {item.text}
                  </option>
                ))}
              </select>
            </div>
          </div>
          <div className="col-lg-6 col-md-6 col-sm-12">
            <div className="form-group">
              <label htmlFor="lop">Lớp:</label>
              <select className="form-control form-control-lg" id="lop" name="lop">
                {TRUNG_TAM.map(item => (
                  <option
                    value={item.code}
                    disabled={item.code !== "" ? false : true}
                    selected={item.code !== "" ? false : true}
                    hidden={item.code !== "" ? false : true}
                  >
                    {item.text}
                  </option>
                ))}
              </select>
            </div>
          </div>
          <div className="col-lg-6 col-md-6 col-sm-12">
            <div className="form-group">
              <label htmlFor="ngay_thuc_hien">Ngày thực hiện:</label>
              <select
                className="form-control form-control-lg"
                id="ngay_thuc_hien"
                name="ngay_thuc_hien"
              >
                {TRUNG_TAM.map(item => (
                  <option
                    value={item.code}
                    disabled={item.code !== "" ? false : true}
                    selected={item.code !== "" ? false : true}
                    hidden={item.code !== "" ? false : true}
                  >
                    {item.text}
                  </option>
                ))}
              </select>
            </div>
          </div>

          <div className="col-lg-6 col-md-6 col-sm-12">
            <div className="form-group">
              <label htmlFor="loai_hinh_danh_gia">Loại hình đánh giá:</label>
              <select
                className="form-control form-control-lg "
                id="loai_hinh_danh_gia"
                name="loai_hinh_danh_gia"
              >
                {TRUNG_TAM.map(item => (
                  <option
                    value={item.code}
                    disabled={item.code !== "" ? false : true}
                    selected={item.code !== "" ? false : true}
                    hidden={item.code !== "" ? false : true}
                  >
                    {item.text}
                  </option>
                ))}
              </select>
            </div>
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button variant={"secondary"} onClick={handleClose}>
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  );
}
