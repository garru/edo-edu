import React from "react";
import { connect } from "dva";
import "./index.css";
import "react-datepicker/dist/react-datepicker.css";
import Rating from "react-rating";
import moment from "moment";
import { Button, Table, Overlay, Popover } from "react-bootstrap";
import {
  DATA_LICH_SU_NHAN_XET_DU_GIO,
  HEADER_LICH_SU_NHAN_XET_DU_GIO
} from "./data";
import CustomModal from "./modal_chi_tiet_danh_gia_giao_vien";
import * as _ from "lodash";
import IconTable from "../../../components/IconTable";
import Paging from "../../../components/Paging";
class Page extends React.Component {
  constructor(props) {
    super(props);
    const initCheckedItem = DATA_LICH_SU_NHAN_XET_DU_GIO.map(item => false);
    this.state = {
      show: _.cloneDeep(initCheckedItem),
      target: false,
      activeIndex: null,
      showModal: false,
      listData: DATA_LICH_SU_NHAN_XET_DU_GIO
    };
  }
  onClear = () => {
    this.setState({});
  };

  onClose = () => {
    this.setState({ isCreateNew: false, showModal: false });
  };

  renderTooltip(index) {
    return (
      <Overlay
        show={this.state.show[index]}
        target={this.state.target}
        placement="bottom"
        containerPadding={20}
        rootClose={true}
        rootCloseEvent={"click"}
        onHide={e => this.handleClick(e, index)}
      >
        <Popover id="popover-contained">
          <Popover.Content>
            <ul>
              <li
                onClick={e => {
                  this.handleClick(e, index, true);
                }}
              >
                Chỉnh sửa
              </li>
              <li
                onClick={e => {
                  this.handleClick(e, index, true);
                }}
              >
                Thực hiện
              </li>
              <li
                onClick={e => {
                  this.handleClick(e, index, true);
                }}
              >
                Xoá
              </li>
            </ul>
          </Popover.Content>
        </Popover>
      </Overlay>
    );
  }

  handleClick(e, index, isHide) {
    let { show, activeIndex, listItem } = this.state;
    show[index] = !show[index];
    index !== activeIndex && (show[activeIndex] = false);
    this.setState({
      show: show,
      target: isHide ? null : e.target,
      activeIndex: index
    });
  }

  componentWillMount() {}

  render() {
    const bre = {
      title: "Thêm mới lớp học",
      breadcrums: [
        {
          title: "Home",
          path: "/"
        },
        {
          title: "Thêm mới lớp học",
          path: ""
        }
      ]
    };
    console.log("render tiep_nhan>>>");
    return (
      <div className="row">
        <div className="col-12">
          <div className="form-group">
            <label htmlFor="thoi_gian_ap_dung">Thời gian áp dụng: </label>
            <div className="row align-center">
              <div className="col-3">
                <input
                  type="date"
                  className="form-control form-control-lg"
                  id="thoi_gian_ap_dung"
                  defaultValue={moment().format("YYYY-MM-DD")}
                />
              </div>
              <div className="col-3">
                <input
                  type="date"
                  className="form-control form-control-lg"
                  id="thoi_gian_ap_dung"
                  defaultValue={moment().format("YYYY-MM-DD")}
                />
              </div>
            </div>
          </div>
        </div>
        <div className="col-12 ">
          <div style={{ width: "100%" }}>
            <div style={{ overflowX: "auto", width: "100%" }}>
              <Table
                bordered
                hover
                // style={{ minWidth: 1500 }}
                className="table m-0"
              >
                <thead>
                  <tr>
                    {HEADER_LICH_SU_NHAN_XET_DU_GIO.map(item => (
                      <th
                        className={`${item.class} ${
                          item.hasSort ? "sort" : ""
                        } ${this.state.sortField === item.key ? "active" : ""}`}
                        key={item.key}
                      >
                        <span>{item.title}</span>
                      </th>
                    ))}
                  </tr>
                </thead>
                <tbody>
                  {this.state.listData.map((item, index) => (
                    <React.Fragment>
                      <tr key={`account_${item.id}`}>
                        <td>{item.id}</td>
                        <td>{item.ngay_thuc_hien}</td>
                        <td>{item.loai}</td>
                        <td>{item.nguoi_thuc_hien}</td>
                        <td>
                          <Rating
                            className="rating"
                            emptySymbol="far fa-star"
                            fullSymbol="fa fa-star"
                            fractions={2}
                            readonly={true}
                            initialRating={item.rating}
                          />
                        </td>
                        <td>
                          <IconTable
                            classes="fa fa-eye"
                            onClick={() => {
                              this.setState({ showModal: true });
                              console.log(123);
                            }}
                            text="View"
                          ></IconTable>
                        </td>
                      </tr>
                    </React.Fragment>
                  ))}
                </tbody>
              </Table>
              {this.state.listData && this.state.listData.length ? (
                ""
              ) : (
                <div className="not-found">
                  <i class="fas fa-inbox"></i>
                  <span>Không tìm thấy kết quả</span>
                </div>
              )}
            </div>
          </div>
          <Paging
            pageNumber={10}
            total={550}
            pageSize={10}
            onChange={console.log}
          ></Paging>
        </div>
        {this.state.showModal && (
          <CustomModal onClose={this.onClose}></CustomModal>
        )}
      </div>
    );
  }
}

export default connect(({ Mock }) => ({
  Mock
}))(Page);
