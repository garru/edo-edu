import React from "react";
import { connect } from "dva";
import "./index.css";
import "react-datepicker/dist/react-datepicker.css";
import { Link } from "react-router-dom";
import { Button, Table, Overlay, Popover } from "react-bootstrap";
import { DATA_LICH_NHAN_XET_DU_GIO, HEADER_LICH_NHAN_XET_DU_GIO } from "./data";
import * as _ from "lodash";
import Paging from "../../../components/Paging";
import CustomModal from "./modal_tao_lich_du_gio";

class Page extends React.Component {
  constructor(props) {
    super(props);
    const initCheckedItem = DATA_LICH_NHAN_XET_DU_GIO.map(item => false);
    this.state = {
      show: _.cloneDeep(initCheckedItem),
      target: false,
      activeIndex: null,
      showModal: false,
      listData: DATA_LICH_NHAN_XET_DU_GIO
    };
  }
  onClear = () => {
    this.setState({});
  };

  onClose = () => {
    this.setState({ isCreateNew: false, showModal: false });
  };

  renderTooltip(index) {
    return (
      <Overlay
        show={this.state.show[index]}
        target={this.state.target}
        placement="bottom"
        containerPadding={20}
        rootClose={true}
        rootCloseEvent={"click"}
        onHide={e => this.handleClick(e, index)}
      >
        <Popover id="popover-contained">
          <Popover.Content>
            <ul>
              <li
                onClick={e => {
                  this.handleClick(e, index, true);
                  this.setState({ showModal: true });
                }}
              >
                Chỉnh sửa
              </li>
              <li>
                <Link className="not-link" to="danh_gia_du_gio">
                  Thực hiện
                </Link>
              </li>
              <li
                onClick={e => {
                  this.handleClick(e, index, true);
                }}
              >
                Xoá
              </li>
            </ul>
          </Popover.Content>
        </Popover>
      </Overlay>
    );
  }

  handleClick(e, index, isHide) {
    let { show, activeIndex, listItem } = this.state;
    show[index] = !show[index];
    index !== activeIndex && (show[activeIndex] = false);
    this.setState({
      show: show,
      target: isHide ? null : e.target,
      activeIndex: index
    });
  }

  componentWillMount() {}

  render() {
    const bre = {
      title: "Thêm mới lớp học",
      breadcrums: [
        {
          title: "Home",
          path: "/"
        },
        {
          title: "Thêm mới lớp học",
          path: ""
        }
      ]
    };
    console.log("render tiep_nhan>>>");
    return (
      <div className="row">
        <div className="col-12 m-b-2 text-right">
          <Button
            variant="primary"
            onClick={() => this.setState({ showModal: true })}
          >
            Tạo mới
          </Button>
        </div>
        <div className="col-12 ">
          <div style={{ width: "100%" }}>
            <div style={{ overflowX: "auto", width: "100%" }}>
              <Table
                bordered
                hover
                // style={{ minWidth: 1500 }}
                className="table m-0"
              >
                <thead>
                  <tr>
                    {HEADER_LICH_NHAN_XET_DU_GIO.map(item => (
                      <th
                        className={`${item.class} ${
                          item.hasSort ? "sort" : ""
                        } ${this.state.sortField === item.key ? "active" : ""}`}
                        key={item.key}
                      >
                        <span>{item.title}</span>
                      </th>
                    ))}
                  </tr>
                </thead>
                <tbody>
                  {this.state.listData.map((item, index) => (
                    <React.Fragment>
                      <tr key={`account_${item.id}`}>
                        <td>{item.id}</td>
                        <td>{item.ngay_thuc_hien}</td>
                        <td>{item.loai}</td>
                        <td>{item.nguoi_thuc_hien}</td>
                        <td>{item.trang_thai}</td>
                        <td>
                          <i
                            className="fa fa-ellipsis-h"
                            style={{
                              marginRight: 0,
                              marginLeft: "auto"
                            }}
                            onClick={e => {
                              e.preventDefault();
                              this.handleClick(e, index);
                            }}
                          />
                          {this.renderTooltip(index)}
                        </td>
                      </tr>
                    </React.Fragment>
                  ))}
                </tbody>
              </Table>
              {this.state.listData && this.state.listData.length ? (
                ""
              ) : (
                <div className="not-found">
                  <i class="fas fa-inbox"></i>
                  <span>Không tìm thấy kết quả</span>
                </div>
              )}
            </div>
          </div>
          <Paging
            pageNumber={10}
            total={550}
            pageSize={10}
            onChange={console.log}
          ></Paging>
        </div>
        {this.state.showModal && (
          <CustomModal onClose={this.onClose}></CustomModal>
        )}
      </div>
    );
  }
}

export default connect(({ Mock }) => ({
  Mock
}))(Page);
