import React from "react";
import { connect } from "dva";
import "./index.css";
import "react-datepicker/dist/react-datepicker.css";
import Rating from "react-rating";
import moment from "moment";
import { Button, Table } from "react-bootstrap";
import { HEADER_TONG_QUAN, DATA_TONG_QUAN } from "./data";

class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      listData: DATA_TONG_QUAN
    };
  }
  onClear = () => {
    this.setState({});
  };

  componentWillMount() {}

  render() {
    return (
      <div style={{ width: "100%" }}>
        <div style={{ overflowX: "auto", width: "100%" }}>
          <Table
            bordered
            hover
            className="table m-0"
          >
            <thead>
              <tr>
                <th></th>
                {HEADER_TONG_QUAN.map(item => (
                  <th
                    className={`${item.class} ${item.hasSort ? "sort" : ""} ${
                      this.state.sortField === item.key ? "active" : ""
                    }`}
                    key={item.key}
                  >
                    <span>{item.title}</span>
                  </th>
                ))}
              </tr>
            </thead>
            <tbody className="text-left">
              {this.state.listData.map((item, index) => (
                <React.Fragment>
                  <tr key={`account_${item.id}`}>
                    <td>
                      {!item.isParent ? (
                        <span>{item.id}</span>
                      ) : (
                        <span>
                          <b>{item.id}</b>
                        </span>
                      )}
                    </td>
                    <td style={{ paddingLeft: item.isParent ? "" : 30 }}>
                      {!item.isParent ? (
                        <span>{item.tieu_chi}</span>
                      ) : (
                        <span>
                          <b>{item.tieu_chi}</b>
                        </span>
                      )}
                    </td>
                    <td style={{ textAlign: "center" }}>
                      {!item.isParent ? (
                        <span>{item.diem_so}</span>
                      ) : (
                        <span>
                          <b>{item.diem_so}</b>
                        </span>
                      )}
                    </td>
                    <td>
                      {!item.isParent ? (
                        <span>{item.ghi_chu}</span>
                      ) : (
                        <span>
                          <b>{item.ghi_chu}</b>
                        </span>
                      )}
                    </td>
                  </tr>
                </React.Fragment>
              ))}
            </tbody>
          </Table>
          {this.state.listData && this.state.listData.length ? (
            ""
          ) : (
            <div className="not-found">
              <i class="fas fa-inbox"></i>
              <span>Không tìm thấy kết quả</span>
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default connect(({ Mock }) => ({
  Mock
}))(Page);
