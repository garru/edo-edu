import React from "react";
import { connect } from "dva";
import "./index.css";
import "react-datepicker/dist/react-datepicker.css";
import Rating from "react-rating";
import TongQuan from "./tong_quan";
import LichSuNhanXetDuGio from "./lich_su_nhan_xet_du_gio";
import LichNhanXetDuGio from "./lich_nhan_xet_du_gio";
import NhanXetTuPhuHuynh from "./nhan_xet_tu_phu_huynh";
import { Tab, Tabs } from "react-bootstrap";
import PageHeader from "../../../components/PageHeader";

class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  onClear = () => {
    this.setState({});
  };

  componentWillMount() {}

  render() {
    const bre = {
      title: "Chi tiết lịch sử đánh giá giáo viên",
      breadcrums: [
        {
          title: "Home",
          path: "/"
        },
        {
          title: "Chi tiết lịch sử đánh giá giáo viên",
          path: ""
        }
      ]
    };
    console.log("render tiep_nhan>>>");
    return (
      <div>
        <PageHeader {...bre}></PageHeader>
        <div className="iq-card">
          <div className=" iq-card-header d-flex justify-content-between">
            <div className="iq-header-title">
              <h6>
                <b>Thông tin cơ bản</b>
              </h6>
            </div>
          </div>
          <div className="iq-card-body">
            <div className="row">
              <div className="col-12">
                <div className="row align-center">
                <div className="col-lg-3 col-md-4 col-sm-12 text-center">
                    <div className="profile-img-edit m-b-2">
                      <img
                        alt="profile-pic"
                        className="profile-pic"
                        src={this.state.img_user || "images/user/11.png"}
                      />
                      <div className="p-image">
                        <i className="fas fa-camera upload-button"></i>
                        <input
                          accept="image/*"
                          className="file-upload"
                          type="file"
                        />
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-9 col-md-8 col-sm-12">
                    <div className="row">
                      <div className="col-lg-6 col-md-12">
                        <div className="row">
                          <div className="col-4">
                            <p>Họ và tên:</p>
                          </div>
                          <div className="col-8">
                            <p>
                              <b>Hoàng Văn Huy</b>
                            </p>
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-4">
                            <p>Mã số:</p>
                          </div>
                          <div className="col-8">
                            <p>
                              <b>HV-000002</b>
                            </p>
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-4">
                            <p>Số điện thoại:</p>
                          </div>
                          <div className="col-8">
                            <p>
                              <b>0384455522</b>
                            </p>
                          </div>
                        </div>
                      </div>
                      <div className="col-lg-6 col-md-12">
                        <div className="row">
                          <div className="col-4">
                            <p>Email:</p>
                          </div>
                          <div className="col-8">
                            <p>
                              <b>test@qa/team</b>
                            </p>
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-4">
                            <p>Loại:</p>
                          </div>
                          <div className="col-8">
                            <p>
                              <b>Full time</b>
                            </p>
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-4">
                            <p>Thời gian công tác:</p>
                          </div>
                          <div className="col-8">
                            <p>
                              <b>4 năm</b>
                            </p>
                          </div>
                        </div>
                      </div>

                      <div className="col-lg-6 col-md-12">
                        <div className="row">
                          <div className="col-4">
                            <p>Đang dậy các lớp:</p>
                          </div>
                          <div className="col-8">
                            <ul>
                              <li>
                                <p>
                                  <b>Lớp A - Hoàng Ngân</b>
                                </p>
                              </li>
                              <li>
                                <p>
                                  <b>Lớp B - Hoàng Ngân</b>
                                </p>
                              </li>
                            </ul>
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-4">
                            <p>Lần đánh giá gần nhất:</p>
                          </div>
                          <div className="col-8">
                            <p>
                              <b>2 tháng trước</b>
                            </p>
                          </div>
                        </div>
                      </div>

                      <div className="col-lg-6 col-md-12">
                        <div className="row">
                          <div className="col-4">
                            <p>Nhận xét từ phụ huynh:</p>
                          </div>
                          <div className="col-8">
                            <p>
                              <b>20</b>
                            </p>
                          </div>
                        </div>

                        <div className="row">
                          <div className="col-4">
                            <p>Đánh giá hiện tại:</p>
                          </div>
                          <div className="col-8">
                            <Rating
                              className="rating"
                              emptySymbol="far fa-star"
                              fullSymbol="fa fa-star"
                              fractions={2}
                              readonly={true}
                              initialRating={4}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="iq-card">
          <div className=" iq-card-header d-flex justify-content-between">
            <div className="iq-header-title">
              <h6>
                <b>Thông tin chi tiết</b>
              </h6>
            </div>
          </div>
          <div className="iq-card-body">
            <Tabs defaultActiveKey="tong_quan" id="uncontrolled-tab-example">
              <Tab eventKey="tong_quan" title="Tổng quan">
                <TongQuan></TongQuan>
              </Tab>
              <Tab eventKey="lich_nhan_xet" title="Lịch nhận xét - Dự giờ">
                <LichNhanXetDuGio></LichNhanXetDuGio>
              </Tab>
              <Tab eventKey="lich_su" title="Lịch sử nhận xét - Dự giờ">
                <LichSuNhanXetDuGio></LichSuNhanXetDuGio>
              </Tab>
              <Tab eventKey="nhan_xet" title="Nhận xét từ phụ huynh">
                <NhanXetTuPhuHuynh></NhanXetTuPhuHuynh>
              </Tab>
            </Tabs>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(({ Mock }) => ({
  Mock
}))(Page);
