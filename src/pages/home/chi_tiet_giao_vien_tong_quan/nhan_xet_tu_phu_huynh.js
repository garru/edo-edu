import React from "react";
import { connect } from "dva";
import "./index.css";
import "react-datepicker/dist/react-datepicker.css";
import { Button, Table } from "react-bootstrap";
import {
  DATA_NHAN_XET_TU_PHU_HUYNH,
  HEADER_NHAN_XET_TU_PHU_HUYNH
} from "./data";
import Paging from "../../../components/Paging";
import CustomModal from "./modal_nhan_xet_tu_phu_huynh";

class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
      listData: DATA_NHAN_XET_TU_PHU_HUYNH
    };
  }
  onClear = () => {
    this.setState({});
  };

  onClose = () => {
    this.setState({ isCreateNew: false, showModal: false });
  };

  componentWillMount() {}

  render() {
    const bre = {
      title: "Thêm mới lớp học",
      breadcrums: [
        {
          title: "Home",
          path: "/"
        },
        {
          title: "Thêm mới lớp học",
          path: ""
        }
      ]
    };
    console.log("render tiep_nhan>>>");
    return (
      <div className="row">
        <div className="col-12">
          <div style={{ width: "100%" }}>
            <div style={{ overflowX: "auto", width: "100%" }}>
              <Table
                bordered
                hover
                // style={{ minWidth: 1500 }}
                className="table"
              >
                <thead>
                  <tr>
                    {HEADER_NHAN_XET_TU_PHU_HUYNH.map(item => (
                      <th
                        className={`${item.class} ${
                          item.hasSort ? "sort" : ""
                        } ${this.state.sortField === item.key ? "active" : ""}`}
                        key={item.key}
                      >
                        <span>{item.title}</span>
                      </th>
                    ))}
                  </tr>
                </thead>
                <tbody>
                  {this.state.listData.map((item, index) => (
                    <React.Fragment>
                      <tr key={`account_${item.id}`}>
                        <td>{item.id}</td>
                        <td>{item.ho_ten}</td>
                        <td>{item.phu_huynh_cua}</td>
                        <td>{item.lop}</td>
                        <td>{item.co_so}</td>
                        <td>{item.ngay_thuc_hien}</td>
                        <td>
                          <i
                            className="far fa-eye"
                            style={{
                              marginRight: 0
                            }}
                            onClick={() => this.setState({ showModal: true })}
                          />
                        </td>
                      </tr>
                    </React.Fragment>
                  ))}
                </tbody>
              </Table>
              {this.state.listData && this.state.listData.length ? (
                ""
              ) : (
                <div className="not-found">
                  <i class="fas fa-inbox"></i>
                  <span>Không tìm thấy kết quả</span>
                </div>
              )}
            </div>
          </div>
          <Paging
            pageNumber={10}
            total={550}
            pageSize={10}
            onChange={console.log}
          ></Paging>
        </div>
        {this.state.showModal && (
          <CustomModal onClose={this.onClose}></CustomModal>
        )}
      </div>
    );
  }
}

export default connect(({ Mock }) => ({
  Mock
}))(Page);
