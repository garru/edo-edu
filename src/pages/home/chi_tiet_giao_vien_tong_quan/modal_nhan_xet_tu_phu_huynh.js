import React, { Component, useState, useRef, useEffect } from "react";
import { useForm } from "react-hook-form";

import { Modal, Button, Table } from "react-bootstrap";
import Rating from "react-rating";
import {
  HEADER_MODAL_CHI_TIET_NHAN_XET,
  DATA_MODAL_CHI_TIET_NHAN_XET,
  HEADER_TONG_QUAN,
  DATA_TONG_QUAN
} from "./data";

export default function CustomModal(props) {
  const [show, setShow] = useState(true);
  const handleClose = () => {
    // setShow(false);
    props.onClose();
  };
  const { register, handleSubmit, errors } = useForm();
  const emailReg = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  const { isCreateNew, selectedItem } = props;
  return (
    <Modal
      size={"lg"}
      show={show}
      onHide={handleClose}
      backdrop="static"
      keyboard={false}
      id="AddNew"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title>Chi tiết nhận xét của phụ huynh</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <fieldset>
          <legend>Thông tin cơ bản</legend>
          <div className="row">
            <div className="col-lg-2 col-md-4">
              <p> Giáo viên</p>
            </div>
            <div className="col-lg-4 col-md-8">
              <p>
                <b>Hoàng văn Phi</b>
              </p>
            </div>
            <div className="col-lg-2 col-md-4">
              <p> Trung tâm</p>
            </div>
            <div className="col-lg-4 col-md-8">
              <p>
                <b>Hoàng Ngân</b>
              </p>
            </div>
            <div className="col-lg-2 col-md-4">
              <p> Người dự giờ</p>
            </div>
            <div className="col-lg-4 col-md-8">
              <p>
                <b>Trang Nguyễn</b>
              </p>
            </div>
            <div className="col-lg-2 col-md-4">
              <p> Lớp</p>
            </div>
            <div className="col-lg-4 col-md-8">
              <p>
                <b>Bibop 123</b>
              </p>
            </div>
            <div className="col-lg-2 col-md-4">
              <p className="m-0"> Ngày thực hiện</p>
            </div>
            <div className="col-lg-4 col-md-8">
              <p className="m-0">
                <b>Ngày thực hiện</b>
              </p>
            </div>
            <div className="col-lg-2 col-md-4">
              <p className="m-0"> Số học sinh</p>
            </div>
            <div className="col-lg-4 col-md-8">
              <p className="m-0">
                <b>12</b>
              </p>
            </div>
          </div>
        </fieldset>
        <fieldset className="m-t-2">
          <legend>Kết quả tổng hợp</legend>

          <div className="row">
            <div className="col-2 m-b-2">Rating</div>
            <div className="col-10">
              <Rating
                className="rating  m-b-2"
                emptySymbol="far fa-star"
                fullSymbol="fa fa-star"
                fractions={2}
                readonly={true}
                initialRating={4}
              />
            </div>

            <div className="col-lg-2 col-md-4">
              <p> Điểm tốt</p>
            </div>
            <div className="col-10">
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean
                euismod bibendum laoreet. Proin gravida dolor sit amet lacus
                accumsan et viverra justo commodo. Proin sodales pulvinar
                tempor. Cum sociis natoque penatibus et magnis dis parturient
              </p>
            </div>
            <div className="col-lg-2 col-md-4">
              <p> Cần cải tiến</p>
            </div>
            <div className="col-10">
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean
                euismod bibendum laoreet. Proin gravida dolor sit amet lacus
                accumsan et viverra justo commodo. Proin sodales pulvinar
                tempor. Cum sociis natoque penatibus et magnis dis parturient
              </p>
            </div>
            <div className="col-lg-2 col-md-4">
              <p className="m-0"> Nội dung khác</p>
            </div>
            <div className="col-10">
              <p className="m-0">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean
                euismod bibendum laoreet. Proin gravida dolor sit amet lacus
                accumsan et viverra justo commodo. Proin sodales pulvinar
                tempor. Cum sociis natoque penatibus et magnis dis parturient
              </p>
            </div>
          </div>
        </fieldset>
      </Modal.Body>
      <Modal.Footer>
        <Button variant={"secondary"} onClick={handleClose}>
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  );
}
