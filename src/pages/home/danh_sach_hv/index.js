import React, { useState, useEffect } from "react";
import { Overlay, Popover, Table } from "react-bootstrap";
import moment from "moment";
import { connect } from "dva";
import ChuyenLop from "./chuyen_lop";
import XepLop from "./xep_lop";
import "react-datepicker/dist/react-datepicker.css";
import PageHeader from "../../../components/PageHeader";
import { Link } from "react-router-dom";
import CustomTable from "../../../components/Table";
import isEmpty from "lodash/isEmpty";
import Select from "../../../components/Form/Select";
import Input from "../../../components/Form/Input";

let timeoutSearch;

const Page = props => {
  const bre = {
    title: "Danh sách học viên",
    breadcrums: [
      {
        title: "Home",
        path: "/"
      },
      {
        title: "Danh sách học viên",
        path: ""
      }
    ]
  };
  const [xepLop, setXepLop] = useState(false);
  const [chuyenLop, setChuyenLop] = useState(false);
  const [_show_tooltip, setShowTooltip] = useState(false);
  const [_tooltip_target, setTooltipTarget] = useState();
  const [selected, setSelected] = useState();
  const [showModal, setShowModal] = useState(false);
  const [count, setCount] = useState(0);
  const [centerId, setCenterId] = useState("");
  const [classId, setClassId] = useState("");
  const [programId, setProgramId] = useState("");
  const [isCreateNew, setIsCreateNew] = useState(false);

  const onChangeCenter = id => {
    const { dispatch } = props;
    setCenterId(id);
    dispatch({
      type: "Global/showLoading"
    });
    const listApi = [
      dispatch({
        type: "Student/updateoptions",
        payload: {
          center_Id: id
        }
      })
    ];
    programId &&
      listApi.push(
        dispatch({
          type: "Student/listclass",
          payload: {
            centerId: id,
            programId
          }
        })
      );
    Promise.all(listApi).then(
      () => {
        dispatch({
          type: "Global/hideLoading"
        });
      },
      error => {
        dispatch({
          type: "Global/hideLoading"
        });
        dispatch({
          type: "Global/showError"
        });
      }
    );
  };

  const onChangeProgram = id => {
    const { dispatch } = props;
    setProgramId(id);
    dispatch({
      type: "Global/showLoading"
    });
    centerId &&
      dispatch({
        type: "Student/listclass",
        payload: { programId: id, centerId }
      }).then(data => {
        dispatch({
          type: "Global/hideLoading"
        });
        !data &&
          dispatch({
            type: "Global/showError"
          });
      });
  };

  const onChangeClass = id => {
    const { dispatch } = props;
    setClassId(id);
    dispatch({
      type: "Global/showLoading"
    });
    centerId &&
      dispatch({
        type: "Student/updateoptions",
        payload: { class_Id: id }
      }).then(data => {
        dispatch({
          type: "Global/hideLoading"
        });
        !data &&
          dispatch({
            type: "Global/showError"
          });
      });
  };

  const Tooltip = ({
    show,
    target,
    handleMenuClick,
    setShowTooltip,
    id,
    data
  }) => {
    return (
      <Overlay
        show={show}
        target={target}
        placement="bottom"
        containerPadding={20}
        rootClose={true}
        rootCloseEvent={"click"}
        onHide={e => setShowTooltip(false)}
      >
        <Popover id="popover-contained">
          <Popover.Content>
            <ul>
              <li onClick={e => (e.preventDefault(), e.stopPropagation())}>
                <Link to={`/diem_danh_hv/${data.id}/${data.classId}`}>
                  Chi tiết
                </Link>
              </li>
              <li onClick={() => handleMenuClick(1, data)}>Xếp lớp</li>
              <li onClick={() => handleMenuClick(2, data)}>Chuyển lớp</li>
            </ul>
          </Popover.Content>
        </Popover>
      </Overlay>
    );
  };

  useEffect(() => {
    const { dispatch } = props;
    dispatch({
      type: "Global/showLoading"
    });
    Promise.all([
      dispatch({
        type: "Student/updateoptions",
        payload: {}
      }),
      dispatch({
        type: "Student/listprogram",
        payload: {}
      }),
      dispatch({
        type: "Student/listcenter",
        payload: {}
      })
    ]).then(data => {
      dispatch({
        type: "Global/hideLoading"
      });
      if (!data) {
        dispatch({
          type: "Global/showError"
        });
      }
    });
  }, [count]);

  const handleClickTooltipMenu = (menu, data) => {
    setSelected(data.id);
    const { dispatch } = props;
    switch (menu) {
      case 1:
        dispatch({
          type: "Global/showLoading"
        });
        dispatch({
          type: "Student/view",
          payload: {
            id: data.id,
            classId: data.classId
          }
        }).then(data => {
          dispatch({
            type: "Global/hideLoading"
          });
          if (data) {
            setXepLop(true);
          } else {
            dispatch({
              type: "Global/showError"
            });
          }
        });
        break;
      case 2:
        dispatch({
          type: "Global/showLoading"
        });
        dispatch({
          type: "Student/view",
          payload: {
            id: data.id,
            classId: data.classId
          }
        }).then(data => {
          dispatch({
            type: "Global/hideLoading"
          });
          if (data) {
            setChuyenLop(true);
          } else {
            dispatch({
              type: "Global/showError"
            });
          }
        });
        break;
      default:
        break;
    }
    setShowTooltip(false);
  };
  const handleClickTooltip = e => {
    setShowTooltip(true);
    setTooltipTarget(e.target);
  };
  const handleModalClose = isRefresh => {
    setXepLop(false);
    setChuyenLop(false);
    onClose(isRefresh);
  };
  const onClose = data => {
    if (data) {
      const { dispatch } = props;
      dispatch({
        type: "Global/showLoading"
      });
      dispatch({
        type: "Student/updateoptions",
        payload: {}
      }).then(res => {
        dispatch({
          type: "Global/showSuccess"
        });
        dispatch({
          type: "Global/hideLoading"
        });
        setShowModal(false);
        if (!res) {
          dispatch({
            type: "Global/showError"
          });
        }
      });
    } else {
      setShowModal(false);
    }
  };

  const onSearch = (e, props) => {
    clearTimeout(timeoutSearch);
    const value = e.target?.value;
    timeoutSearch = setTimeout(() => {
      const { dispatch } = props;
      // setKeyword(value);
      dispatch({
        type: "Global/showLoading"
      });
      dispatch({
        type: "Student/updateoptions",
        payload: {
          codeOrName: value
        }
      }).then(data => {
        dispatch({
          type: "Global/hideLoading"
        });
        if (!data) {
          dispatch({
            type: "Global/showError"
          });
        }
      });
    }, 500);
  };

  const updateOptions = (data, props) => {
    const { dispatch } = props;
    dispatch({
      type: "Global/showLoading"
    });
    dispatch({
      type: "Customer/updateoptions",
      payload: data
    }).then(res => {
      dispatch({
        type: "Global/hideLoading"
      });
      !res &&
        dispatch({
          type: "Global/showError"
        });
    });
  };

  const column_customer = [
    {
      title: "Mã học viên",
      key: "code",
      class: "tb-width-100"
    },
    {
      title: "Tên",
      key: "lastName",
      class: "tb-width-100"
    },
    {
      title: "Họ Đệm",
      key: "firstName",
      class: "tb-width-150"
    },
    {
      title: "Trung tâm",
      key: "centerName",
      class: "tb-width-150"
    },
    {
      title: "Lớp",
      key: "className",
      class: "tb-width-200"
    },
    {
      title: "Giáo trình",
      key: "programName",
      class: "tb-width-200"
    },
    {
      title: "Ngày kết thúc",
      key: "finishDate",
      class: "tb-width-200"
    },
    {
      title: "Trạng thái",
      key: "status",
      class: "tb-width-100"
    },
    {
      title: "Thao tác",
      key: "thao_tac",
      class: "tb-width-100",
      render: (col, index) => (
        <>
          <i className="fa fa-ellipsis-h" onClick={handleClickTooltip} />
          <Tooltip
            data={col}
            show={_show_tooltip}
            target={_tooltip_target}
            handleMenuClick={handleClickTooltipMenu}
            setShowTooltip={setShowTooltip}
            id={col.id}
          />
        </>
      )
    }
  ];

  return (
    <div>
      <PageHeader {...bre}></PageHeader>
      <div className="iq-card">
        <div className=" iq-card-header d-flex justify-content-between">
          <div className="iq-header-title">
            <h6>
              <b>Thông tin cơ bản</b>
            </h6>
          </div>
        </div>
        <div className="iq-card-body">
          <div className="">
            <div className="row ">
              <div className="col-12 m-b-2">
                <fieldset>
                  <legend>Tìm kiếm</legend>
                  <div className="row align-end">
                    <div className="col-md-3 col-sx-6">
                      <div className="form-group">
                        <Input
                          type="text"
                          name="ma_nhan_vien"
                          className="form-control form-control-lg"
                          id="ma_nhan_vien"
                          placeholder="Nhập tên/SDT/Email/Mã khách hàng"
                          onChange={e => onSearch(e, props)}
                          notFormik={true}
                          defaultValue=""
                          label="Tên/SDT/Email/Mã"
                          // value={keyword}
                        ></Input>
                      </div>
                    </div>
                    <div className="col-md-3 col-sx-6">
                      <div className="form-group">
                        <Select
                          id="centerId"
                          name="centerId"
                          label="Cơ sở"
                          value={centerId}
                          options={props.Student?.listcenter}
                          onChange={e => {
                            const value = e.target.value;
                            onChangeCenter(value);
                          }}
                          notFormik={true}
                          defaultValue=""
                        />
                      </div>
                    </div>
                    <div className="col-md-3 col-sx-6">
                      <div className="form-group">
                        <Select
                          id="programId"
                          name="programId"
                          label="Chương trình học"
                          value={programId}
                          options={props.Student?.listprogram}
                          onChange={e => {
                            const value = e.target.value;
                            onChangeProgram(value);
                          }}
                          notFormik={true}
                          defaultValue=""
                        />
                      </div>
                    </div>
                    <div className="col-md-3 col-sx-6">
                      <div className="form-group">
                        <Select
                          id="classId"
                          name="classId"
                          label="Lớp học"
                          value={classId}
                          options={props.Student?.listclass}
                          onChange={e => {
                            const value = e.target.value;
                            onChangeClass(value);
                          }}
                          notFormik={true}
                          defaultValue=""
                        />
                      </div>
                    </div>
                  </div>
                </fieldset>
              </div>
              <div className="col-12 m-b-2">
                {props.Student?.summary?.map((item, index) => (
                  <button
                    disabled
                    className={`btn btn-primary`}
                    key={`summary_${index}`}
                    style={{
                      marginRight: 30,
                      opacity: 1,
                      position: "relative"
                    }}
                  >
                    {item.name}
                    <span className="bagde">{item.count}</span>
                  </button>
                ))}
              </div>
              <div className="col-12">
                {props.Student.list_hoc_sinh ? (
                  <>
                    <CustomTable
                      dataSource={props.Student?.list_hoc_sinh}
                      total={props.Student.total_record}
                      columns={column_customer}
                      onChange={data => updateOptions(data, props)}
                    ></CustomTable>
                  </>
                ) : (
                  ""
                )}

                {chuyenLop && (
                  <ChuyenLop id={selected} handleClose={handleModalClose} />
                )}
                {xepLop && (
                  <XepLop id={selected} handleClose={handleModalClose} />
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default connect(({ Student, StudentDetail, Global }) => ({
  Student,
  StudentDetail,
  Global
}))(Page);
