import React, { useState } from "react";
import moment from "moment";
import { Modal, Button } from "react-bootstrap";
import { connect } from "dva";
import { Formik, ErrorMessage } from "formik";
import Input from "../../../components/Form/Input";
import Select from "../../../components/Form/Select";
import TextArea from "../../../components/Form/TextArea";
import * as yup from "yup";

const CustomModal = ({ handleClose, Student, id, dispatch, Class }) => {
  const [listClass, setListClass] = useState([]);
  const [currentClass, setCurrentClass] = useState(null);
  const onChangeCenter = (value, programId) => {
    dispatch({
      type: "Global/showLoading"
    });
    const listApi = [
      dispatch({
        type: "Class/listsurchage",
        payload: {
          centerId: value
        }
      })
    ];
    programId &&
      listApi.push(
        dispatch({
          type: "Student/listclassModal",
          payload: {
            centerId: value,
            programId
          }
        })
      );
    Promise.all(listApi).then(
      data => {
        data[1]?.content && setListClass(data.content);
        dispatch({
          type: "Global/hideLoading"
        });
      },
      error => {
        dispatch({
          type: "Global/hideLoading"
        });
        dispatch({
          type: "Global/showError"
        });
      }
    );
  };

  const onChangeProgram = (value, centerId) => {
    dispatch({
      type: "Global/showLoading"
    });
    centerId &&
      dispatch({
        type: "Student/listclassModal",
        payload: { programId: value, centerId }
      }).then(
        data => {
          dispatch({
            type: "Global/hideLoading"
          });
          data?.content && setListClass(data.content);
          !data?.content &&
            dispatch({
              type: "Global/showError"
            });
        },
        error => {
          dispatch({
            type: "Global/hideLoading"
          });
          dispatch({
            type: "Global/showError"
          });
        }
      );
  };

  const onChangeClass = value => {
    dispatch({
      type: "Global/showLoading"
    });

    dispatch({
      type: "Student/viewModal",
      payload: { classId: value, id }
    }).then(
      data => {
        dispatch({
          type: "Global/hideLoading"
        });
        data?.content && setCurrentClass(data.content);
        !data?.content &&
          dispatch({
            type: "Global/showError"
          });
      },
      error => {
        dispatch({
          type: "Global/hideLoading"
        });
        dispatch({
          type: "Global/showError"
        });
      }
    );
  };

  return (
    <Modal show size="xl">
      <Modal.Header closeButton>
        <Modal.Title>Xếp lớp</Modal.Title>
      </Modal.Header>
      <Formik
        enableReinitialize={true}
        validationSchema={yup.object().shape({
          changeDate: yup.string().required("Trường bắt buộc"),
          centerId: yup.string().required("Trường bắt buộc"),
          programId: yup.string().required("Trường bắt buộc"),
          classId: yup.string().required("Trường bắt buộc"),
          surchageFee: yup.string().required("Trường bắt buộc")
        })}
        initialValues={{
          changeDate: "",
          centerId: "",
          programId: "",
          classId: "",
          surchageFee: ""
        }}
        onSubmit={(values, { setSubmitting }) => {
          dispatch({
            type: "Global/showLoading"
          });
          dispatch({
            type: "Student/changeclass",
            payload: {
              changeDate: values.changeDate,
              centerId: +values.centerId,
              programId: +values.programId,
              classId: +values.classId,
              surchageFee: +values.surchageFee,
              id
            }
          }).then(data => {
            dispatch({
              type: "Global/hideLoading"
            });
            if (!data.code) {
              dispatch({
                type: "Global/showSuccess"
              });
              handleClose(true);
            } else {
              dispatch({
                type: "Global/showError"
              });
            }
          });
        }}
      >
        {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
          setFieldValue
          /* and other goodies */
        }) => (
          <form style={{ width: "100%" }} onSubmit={handleSubmit}>
            <Modal.Body>
              <div className="row m-b-2">
                <div className="col-12">
                  <fieldset>
                    <legend>Thông tin lớp hiện tại</legend>
                    <div className="col-12">
                      <div className="row">
                        <div className="col-md-4 col-sm-6 col-xs-12">
                          <p>
                            Trung tâm:
                            <b> {Student?.current_hoc_sinh?.centerName}</b>
                          </p>
                        </div>
                        <div className="col-md-4 col-sm-6 col-xs-12">
                          <p>
                            Phòng học:
                            <b> {Student?.current_hoc_sinh?.roomName}</b>
                          </p>
                        </div>
                        <div className="col-md-4 col-sm-6 col-xs-12">
                          <p>
                            Lịch học:
                            <b> {Student?.current_hoc_sinh?.classSchedule}</b>
                          </p>
                        </div>
                        <div className="col-md-4 col-sm-6 col-xs-12">
                          <p>
                            Giáo viên Việt Nam:
                            <b> {Student?.current_hoc_sinh?.teacherDomestic}</b>
                          </p>
                        </div>
                        <div className="col-md-4 col-sm-6 col-xs-12">
                          <p>
                            Giáo viên nước ngoài:
                            <b> {Student?.current_hoc_sinh?.teacherForeign}</b>
                          </p>
                        </div>
                      </div>
                    </div>
                  </fieldset>
                </div>
              </div>

              <div className="row m-b-1">
                <div className="col-12">
                  <fieldset>
                    <legend>Lớp muốn đổi</legend>
                    <div className="col-12">
                      <div className="row">
                        <div className="col-md-4 col-sm-6 col-xs-12">
                          <div className="form-group">
                            <Select
                              id="centerId"
                              name="centerId"
                              label="Trung tâm:"
                              options={Student.listcenter}
                              value={values.centerId}
                              onChange={e => {
                                const value = e.target.value;
                                console.log(value, values);
                                setFieldValue("centerId", value);
                                onChangeCenter(value, values.programId);
                              }}
                              onBlur={handleBlur}
                            />
                          </div>
                        </div>
                        <div className="col-md-4 col-sm-6 col-xs-12">
                          <div className="form-group">
                            <Select
                              id="programId"
                              name="programId"
                              label="Chương trình:"
                              options={Student.listprogram}
                              value={values.programId}
                              onChange={e => {
                                const value = e.target.value;
                                console.log(value, values);
                                setFieldValue("programId", value);
                                onChangeProgram(value, values.centerId);
                              }}
                              onBlur={handleBlur}
                            />
                          </div>
                        </div>
                        <div className="col-md-4 col-sm-6 col-xs-12">
                          <div className="form-group">
                            <Select
                              id="classId"
                              name="classId"
                              label="Lớp học:"
                              options={listClass}
                              value={values.classId}
                              onChange={e => {
                                const value = e.target.value;
                                console.log(value, values);
                                setFieldValue("classId", value);
                                onChangeClass(value);
                              }}
                              onBlur={handleBlur}
                            />
                          </div>
                        </div>
                        <div className="col-md-4 col-sm-6 col-xs-12">
                          <div className="form-group">
                            <Select
                              id="surchageFee"
                              name="surchageFee"
                              label="Phụ thu:"
                              options={Class.list_phu_thu}
                              value={values.surchageFee}
                              onChange={handleChange}
                              onBlur={handleBlur}
                            />
                          </div>
                        </div>
                        <div className="col-md-4 col-sm-6 col-xs-12">
                          <div className="form-group">
                            <Input
                              id="changeDate"
                              type="date"
                              name="changeDate"
                              label="Chuyển từ ngày:"
                              value={values.changeDate}
                              onChange={handleChange}
                              onBlur={handleBlur}
                              placeholder={"Chọn ngày"}
                              format="YYYY-MM-DD"
                              min={moment().format("YYYY-MM-DD")}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                    {currentClass && (
                      <fieldset>
                        <legend>Thông tin lớp học</legend>
                        <div className="col-12 m-t-1">
                          <div className="row">
                            <div className="col-md-4 col-sm-6 col-xs-12">
                              <p>
                                Trung tâm:
                                <b> {currentClass?.centerName}</b>
                              </p>
                            </div>
                            <div className="col-md-4 col-sm-6 col-xs-12">
                              <p>
                                Phòng học:
                                <b> {currentClass?.roomName}</b>
                              </p>
                            </div>
                            <div className="col-md-4 col-sm-6 col-xs-12">
                              <p>
                                Lịch học:
                                <b> {currentClass?.classSchedule}</b>
                              </p>
                            </div>
                            <div className="col-md-4 col-sm-6 col-xs-12">
                              <p>
                                Giáo viên Việt Nam:
                                <b> {currentClass?.teacherDomestic}</b>
                              </p>
                            </div>
                            <div className="col-md-4 col-sm-6 col-xs-12">
                              <p>
                                Giáo viên nước ngoài:
                                <b> {currentClass?.teacherForeign}</b>
                              </p>
                            </div>
                          </div>
                        </div>
                      </fieldset>
                    )}
                  </fieldset>
                </div>
              </div>
            </Modal.Body>
            <Modal.Footer>
              <Button
                variant="primary"
                style={{ marginRight: 15 }}
                type="submit"
              >
                Lưu
              </Button>
              <Button variant={"secondary"} onClick={handleClose}>
                Đóng
              </Button>
            </Modal.Footer>
          </form>
        )}
      </Formik>
    </Modal>
  );
};

export default connect(({ Student, Class, Global }) => ({
  Student,
  Class,
  Global
}))(CustomModal);
