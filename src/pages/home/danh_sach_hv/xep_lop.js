import React, { useState } from "react";
import moment from "moment";
import { Modal, Button } from "react-bootstrap";
import { connect } from "dva";
import { Formik, ErrorMessage } from "formik";
import Input from "../../../components/Form/Input";
import Select from "../../../components/Form/Select";
import TextArea from "../../../components/Form/TextArea";
import * as yup from "yup";
import isNumber from "lodash/isNumber";

const CustomModal = ({ handleClose, Student, id, dispatch }) => {
  const [listClass, setListClass] = useState([]);
  const onChangeCenter = (value, programId) => {
    dispatch({
      type: "Global/showLoading"
    });

    isNumber(+programId) &&
      dispatch({
        type: "Student/listclassModal",
        payload: {
          centerId: value,
          programId
        }
      }).then(
        data => {
          data?.content && setListClass(data.content);
          !data?.content &&
            dispatch({
              type: "Global/showError"
            });
          dispatch({
            type: "Global/hideLoading"
          });
        },
        error => {
          dispatch({
            type: "Global/hideLoading"
          });
          dispatch({
            type: "Global/showError"
          });
        }
      );
  };

  const onChangeProgram = (value, centerId) => {
    dispatch({
      type: "Global/showLoading"
    });
    isNumber(+centerId) &&
      dispatch({
        type: "Student/listclassModal",
        payload: { programId: value, centerId }
      }).then(
        data => {
          dispatch({
            type: "Global/hideLoading"
          });
          data?.content && setListClass(data.content);
          !data?.content &&
            dispatch({
              type: "Global/showError"
            });
        },
        error => {
          dispatch({
            type: "Global/hideLoading"
          });
          dispatch({
            type: "Global/showError"
          });
        }
      );
  };

  // const onChangeClass = id => {
  //   const { dispatch } = props;
  //   setClassId(id);
  //   dispatch({
  //     type: "Global/showLoading"
  //   });
  //   centerId &&
  //     dispatch({
  //       type: "Student/updateoptions",
  //       payload: { class_Id: id }
  //     }).then(data => {
  //       dispatch({
  //         type: "Global/hideLoading"
  //       });
  //       !data &&
  //         dispatch({
  //           type: "Global/showError"
  //         });
  //     });
  // };

  return (
    <Modal show size="xl">
      <Modal.Header closeButton>
        <Modal.Title>Xếp lớp</Modal.Title>
      </Modal.Header>
      <Formik
        enableReinitialize={true}
        validationSchema={yup.object().shape({
          startDate: yup.string().required("Trường bắt buộc"),
          centerId: yup.string().required("Trường bắt buộc"),
          programId: yup.string().required("Trường bắt buộc"),
          classId: yup.string().required("Trường bắt buộc")
        })}
        initialValues={{
          startDate: "",
          centerId: "",
          programId: "",
          classId: ""
        }}
        onSubmit={(values, { setSubmitting }) => {
          dispatch({
            type: "Global/showLoading"
          });
          dispatch({
            type: "Student/insert",
            payload: {}
          }).then(data => {
            dispatch({
              type: "Global/hideLoading"
            });
            if (data) {
              dispatch({
                type: "Global/showSuccess"
              });
              handleClose();
            } else {
              dispatch({
                type: "Global/showError"
              });
            }
          });
        }}
      >
        {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
          setFieldValue
          /* and other goodies */
        }) => (
          <form style={{ width: "100%" }} onSubmit={handleSubmit}>
            <Modal.Body>
              <div className="row m-b-1">
                <div className="col-12">
                  <fieldset>
                    <legend>Thông tin lớp học</legend>

                    <div className="row">
                      <div className="col-md-4 col-sm-6 col-xs-12">
                        <p>
                          Trung tâm:
                          <b> {Student?.current_hoc_sinh?.centerName}</b>
                        </p>
                      </div>
                      <div className="col-md-4 col-sm-6 col-xs-12">
                        <p>
                          Phòng học:
                          <b> {Student?.current_hoc_sinh?.roomName}</b>
                        </p>
                      </div>
                      <div className="col-md-4 col-sm-6 col-xs-12">
                        <p>
                          Lịch học:
                          <b> {Student?.current_hoc_sinh?.classSchedule}</b>
                        </p>
                      </div>
                      <div className="col-md-4 col-sm-6 col-xs-12">
                        <p>
                          Giáo viên Việt Nam:
                          <b> {Student?.current_hoc_sinh?.teacherDomestic}</b>
                        </p>
                      </div>
                      <div className="col-md-4 col-sm-6 col-xs-12">
                        <p>
                          Giáo viên nước ngoài:
                          <b> {Student?.current_hoc_sinh?.teacherForeign}</b>
                        </p>
                      </div>
                    </div>
                  </fieldset>
                </div>
              </div>

              <div className="row">
                <div className="col-md-3 col-sm-6 col-xs-12">
                  <div className="form-group">
                    <Select
                      id="centerId"
                      name="centerId"
                      label="Trung tâm:"
                      options={Student.listcenter}
                      value={values.centerId}
                      onChange={e => {
                        const value = e.target.value;
                        setFieldValue("centerId", +value);
                        onChangeCenter(+value, +values.programId);
                      }}
                      onBlur={handleBlur}
                    />
                  </div>
                </div>
                <div className="col-md-3 col-sm-6 col-xs-12">
                  <div className="form-group">
                    <Select
                      id="programId"
                      name="programId"
                      label="Chương trình:"
                      options={Student.listprogram}
                      value={values.programId}
                      onChange={e => {
                        const value = e.target.value;
                        setFieldValue("programId", +value);
                        onChangeProgram(+value, +values.centerId);
                      }}
                      onBlur={handleBlur}
                    />
                  </div>
                </div>
                <div className="col-md-3 col-sm-6 col-xs-12">
                  <div className="form-group">
                    <Select
                      id="classId"
                      name="classId"
                      label="Lớp học:"
                      options={listClass}
                      value={values.classId}
                      onChange={handleChange}
                      onBlur={handleBlur}
                    />
                  </div>
                </div>
                <div className="col-md-3 col-sm-6 col-xs-12">
                  <div className="form-group">
                    <Input
                      id="startDate"
                      type="date"
                      name="startDate"
                      label="Ngày bắt đầu:"
                      value={values.startDate}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      placeholder={"Chọn ngày"}
                      format="YYYY-MM-DD"
                      min={moment().format("YYYY-MM-DD")}
                    />
                  </div>
                </div>
                <div className="col-md-6 col-sm-12 col-xs-12">
                  <div className="form-group">
                    <TextArea
                      id="rateTestResult"
                      name="rateTestResult"
                      label="Chi tiết kiểm tra đầu vào"
                      value={values.rateTestResult}
                      disabled={true}
                    />
                  </div>
                </div>
                <div className="col-md-6 col-sm-12 col-xs-12">
                  <div className="form-group">
                    <TextArea
                      id="rateTest"
                      name="rateTest"
                      label="Đánh giá kiểm tra"
                      value={values.rateTest}
                      onChange={handleChange}
                      onBlur={handleBlur}
                    />
                  </div>
                </div>
                <div className="col-md-6 col-sm-12 col-xs-12">
                  <div className="form-group">
                    <TextArea
                      id="rateTrialResult"
                      name="rateTrialResult"
                      label="Chi tiết đánh giá học thử"
                      value={values.rateTrialResult}
                      disabled={true}
                    />
                  </div>
                </div>
                <div className="col-md-6 col-sm-12 col-xs-12">
                  <div className="form-group">
                    <TextArea
                      id="rateTrial"
                      name="rateTrial"
                      label="Đánh giá học thử"
                      value={values.rateTrial}
                      onChange={handleChange}
                      onBlur={handleBlur}
                    />
                  </div>
                </div>
              </div>
            </Modal.Body>
            <Modal.Footer>
              <Button
                variant="primary"
                style={{ marginRight: 15 }}
                type="submit"
              >
                Lưu
              </Button>
              <Button variant={"secondary"} onClick={handleClose}>
                Đóng
              </Button>
            </Modal.Footer>
          </form>
        )}
      </Formik>
    </Modal>
  );
};

export default connect(({ Student, Global }) => ({
  Student,
  Global
}))(CustomModal);
