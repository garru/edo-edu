import React, { Component, useState, useRef, useEffect } from "react";
import { useForm } from "react-hook-form";
import { Modal, Button } from "react-bootstrap";
import { TRUNG_TAM, GIO_HOC } from "../../../mock/dropdown";
import moment from "moment";

export default function CustomModal(props) {
  const [show, setShow] = useState(true);
  const handleClose = () => {
    // setShow(false);
    props.onClose();
  };
  const { isCreateNew, selectedItem } = props;
  return (
    <Modal
      size={"lg"}
      show={show}
      onHide={handleClose}
      backdrop="static"
      keyboard={false}
      id="AddNew"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title>
          {isCreateNew ? "Tạo giờ học mới" : "Chỉnh sửa giờ học"}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div>
          <form>
            <div className="row">
              <div className="col-6">
                <div className="form-group">
                  <label htmlFor="ngay_hoc">Ngày học:</label>
                  <input
                    type="date"
                    className="form-control form-control-lg"
                    id="ngay_hoc"
                  />
                </div>
              </div>
              <div className="col-6">
                <div className="form-group">
                  <label htmlFor="gio_hoc">Giờ học:</label>
                  <input
                    type="date"
                    className="form-control form-control-lg"
                    id="gio_hoc"
                  />
                </div>
              </div>
              <div className="col-6">
                <div className="form-group">
                  <label htmlFor="phong_hoc">Phòng học:</label>
                  <select
                    className="form-control form-control-lg"
                    id="phong_hoc"
                    name="phong_hoc"
                  >
                    {TRUNG_TAM.map(item => (
                      <option
                        value={item.code}
                        disabled={item.code !== "" ? false : true}
                        selected={item.code !== "" ? false : true}
                        hidden={item.code !== "" ? false : true}
                      >
                        {item.text}
                      </option>
                    ))}
                  </select>
                </div>
              </div>
              <div className="col-6">
                <div className="form-group">
                  <label htmlFor="gv_viet_nam">Giáo viên người việt:</label>
                  <select
                    className="form-control form-control-lg"
                    id="gv_viet_nam"
                    name="gv_viet_nam"
                  >
                    {TRUNG_TAM.map(item => (
                      <option
                        value={item.code}
                        disabled={item.code !== "" ? false : true}
                        selected={item.code !== "" ? false : true}
                        hidden={item.code !== "" ? false : true}
                      >
                        {item.text}
                      </option>
                    ))}
                  </select>
                </div>
              </div>
              <div className="col-6">
                <div className="form-group">
                  <label htmlFor="gv_nuoc_ngoai">Giáo viên nước ngoài:</label>
                  <select
                    className="form-control form-control-lg"
                    id="gv_nuoc_ngoai"
                    name="gv_nuoc_ngoai"
                  >
                    {TRUNG_TAM.map(item => (
                      <option
                        value={item.code}
                        disabled={item.code !== "" ? false : true}
                        selected={item.code !== "" ? false : true}
                        hidden={item.code !== "" ? false : true}
                      >
                        {item.text}
                      </option>
                    ))}
                  </select>
                </div>
              </div>
            </div>
          </form>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="primary" style={{marginRight: 15}}>Thiết lập lịch học</Button>
        <Button variant={"secondary"} onClick={handleClose}>
          Hủy
        </Button>
      </Modal.Footer>
    </Modal>
  );
}
