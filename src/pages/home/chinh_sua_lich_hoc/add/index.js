import React, { Component, useState, useRef, useEffect } from "react";
import { useForm } from "react-hook-form";
import { useDropzone } from "react-dropzone";
import { Modal, Button, Table } from "react-bootstrap";
import moment from "moment";
import {
  DANG_BAI,
  TIEU_CHI_DANH_GIA,
  CHUONG_TRINH_HOC,
  PHAN_PHOI
} from "../../../../mock/dropdown";
import { HEADER, DATA } from "./data";
import PageHeader from "../../../components/PageHeader";

export default function CustomModal(props) {
  const [show, setShow] = useState(true);
  const [listItem, setListItem] = useState(DATA);
  const handleClose = () => {
    // setShow(false);
    props.onClose();
  };
  const { acceptedFiles, getRootProps, getInputProps } = useDropzone({});
  const files = acceptedFiles.map(file => (
    <li key={file.name}>
      {file.name} - {file.size} bytes
    </li>
  ));

  const bre = {
    title: "Thêm bài học",
    subTitle: "Thêm mới bài học vào chương trình học",
    breadcrums: [
      {
        title: "Home",
        path: "/"
      },
      {
        title: "Thêm bài học",
        path: ""
      }
    ]
  };
  return (
    <>
      <PageHeader {...bre} />
      <div className="iq-card">
        <div className=" iq-card-header d-flex justify-content-between">
          <div className="iq-header-title">
            <h6>
              <b>Thông tin chung</b>
            </h6>
          </div>
        </div>
        <div className="iq-card-body">
          <form>
            <div className="row m-b-2">
              <div className="col-12">
                <div className="row font-size-14">
                  <div className="col-6 ">
                    <div className="form-group">
                      <label htmlFor="chuong_trinh">Chương trình:</label>
                      <select
                        className="form-control form-control-lg"
                        id="chuong_trinh"
                        name="chuong_trinh"
                      >
                        {CHUONG_TRINH_HOC.map(item => (
                          <option
                            value={item.code}
                            disabled={item.code !== "" ? false : true}
                            selected={item.code !== "" ? false : true}
                            hidden={item.code !== "" ? false : true}
                          >
                            {item.text}
                          </option>
                        ))}
                      </select>
                    </div>
                    <div className="form-group">
                      <label htmlFor="ten_bai_hoc">Tên bài học:</label>
                      <input
                        type="text"
                        name="ten_bai_hoc"
                        className="form-control form-control-lg"
                        id="ten_bai_hoc"

                        // ref={register({ required: true, minLength: 8 })}
                      ></input>
                    </div>
                    <div className="form-group">
                      <label htmlFor="dang_bai">Dạng bài:</label>
                      <select
                        className="form-control form-control-lg"
                        id="dang_bai"
                        name="dang_bai"
                      >
                        {DANG_BAI.map(item => (
                          <option
                            value={item.code}
                            disabled={item.code !== "" ? false : true}
                            selected={item.code !== "" ? false : true}
                            hidden={item.code !== "" ? false : true}
                          >
                            {item.text}
                          </option>
                        ))}
                      </select>
                    </div>
                  </div>
                  <div className="col-6 ">
                    <div className="form-group">
                      <label htmlFor="mo_ta_ngan">Mô tả ngắn:</label>
                      <textarea
                        className="textarea form-control"
                        rows="4"
                        name="mo_ta_ngan"
                      ></textarea>
                    </div>
                    <div className="form-group">
                      <input
                        type="checkbox"
                        name="giao_vien_nuoc_ngoai"
                        id="giao_vien_nuoc_ngoai"
                      />
                      <label htmlFor="giao_vien_nuoc_ngoai">
                        &nbsp;&nbsp; Giáo viên nước ngoài
                      </label>
                    </div>
                  </div>
                  <div className="col-6 ">
                    <div className="form-group">
                      <label htmlFor="dang_bai">Các tiêu chí đánh giá:</label>
                      <ul className="vertical">
                        {TIEU_CHI_DANH_GIA.map(item => (
                          <li>
                            <div className="tag">
                              <span>{item.text} &nbsp;</span>
                              <i className="fa fa-times" />
                            </div>
                          </li>
                        ))}
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
      <form>
        <div className="iq-card">
          <div className=" iq-card-header d-flex justify-content-between">
            <div className="iq-header-title">
              <h6>
                <b>Thông tin làm việc</b>
              </h6>
            </div>
          </div>
          <div className="d-flex iq-card-body pt-0">
            <div className="row">
              <div className="col-12 ">
                <div className="form-group">
                  <label htmlFor="noi_dung">Nội dung:</label>
                  <textarea
                    className="textarea form-control"
                    rows="4"
                    name="noi_dung"
                  ></textarea>
                </div>
                <div className="form-group">
                  <label htmlFor="bai_ve_nha">Bài về nhà:</label>
                  <textarea
                    className="textarea form-control"
                    rows="4"
                    name="bai_ve_nha"
                  ></textarea>
                </div>
                <div className="form-group">
                  <label htmlFor="file_su_dung">Các file sử dụng:</label>
                  <section>
                    <div
                      {...getRootProps({
                        className: "dropzone"
                      })}
                    >
                      <input {...getInputProps()} />
                      <h5>Thêm file bằng cách kéo thả</h5>
                      <p>
                        Chấp nhận file doc, docx, xls, xlsx, pdf, img, png. Dung
                        lượng tối đa 150MB
                      </p>
                    </div>
                    <aside>
                      <h4>Files</h4>
                      <ul>{files}</ul>
                    </aside>
                  </section>
                </div>
                <div className="form-group">
                  <label htmlFor="file_su_dung">Các file sử dụng:</label>
                  <div style={{ overflowX: "auto", width: "100%" }}>
                    <Table
                      bordered
                      hover
                      // style={{ minWidth: 1500 }}
                      className="table"
                    >
                      <thead>
                        <tr>
                          {HEADER.map(item => (
                            <th
                              className={`${item.class} ${
                                item.hasSort ? "sort" : ""
                              } ${
                                this.state.sortField === item.key
                                  ? "active"
                                  : ""
                              }`}
                              key={item.key}
                            >
                              <span>{item.title}</span>
                            </th>
                          ))}
                        </tr>
                      </thead>
                      <tbody>
                        {listItem.map((item, index) => (
                          <React.Fragment>
                            <tr key={`account_${item.id}`}>
                              <td>{item.hoc_lieu}</td>
                              <td>{item.so_luong}</td>
                              <td>
                                <select
                                  className="form-control form-control-lg"
                                  id={`dang_bai${index}`}
                                  name={`dang_bai${index}`}
                                >
                                  {PHAN_PHOI.map(item => (
                                    <option value={item.code}>
                                      {item.text}
                                    </option>
                                  ))}
                                </select>
                              </td>
                              <td>
                                <input type="checkbox" name={`hoan${index}`} />
                              </td>
                              <td></td>
                            </tr>
                          </React.Fragment>
                        ))}
                      </tbody>
                    </Table>
                    {listItem || listItem.length ? (
                      ""
                    ) : (
                      <div className="not-found">
                        <i class="fas fa-inbox"></i>
                        <span>Không tìm thấy kết quả</span>
                      </div>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>
    </>
  );
}
