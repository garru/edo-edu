import React from "react";
import { connect } from "dva";
import { Button, Table, Overlay, Popover } from "react-bootstrap";
import { DATA, HEADER } from "./data";
import * as _ from "lodash";
import CustomModal from "./chinh_sua";
import ModalNgoaiKhoa from "./ngoai_khoa";
import {
  CHUONG_TRINH_HOC,
  KIEU_LAP,
  DK_KET_THUC,
  GIO_HOC,
  TRUNG_TAM,
  PHONG_HOC
} from "../../../mock/dropdown";
import moment from "moment";
import PageHeader from "../../../components/PageHeader";
import Paging from "../../../components/Paging";
class Page extends React.Component {
  constructor(props) {
    super(props);
    const initCheckedItem = DATA.map(item => false);
    this.state = {
      show: initCheckedItem,
      target: false,
      listItem: [],
      activeIndex: null,
      showModal: false,
      isCreateNew: false,
      selectedItem: null,
      selectedItem: null,
      showModalNgoaiKhoa: false
    };
  }

  renderTooltip(index) {
    return (
      <Overlay
        show={this.state.show[index]}
        target={this.state.target}
        placement="bottom"
        containerPadding={20}
        rootClose={true}
        rootCloseEvent={"click"}
        onHide={e => this.handleClick(e, index)}
      >
        <Popover id="popover-contained">
          <Popover.Content>
            <ul>
              <li
                onClick={e => {
                  this.handleClick(e, index, true);
                  this.setState({ isCreateNew: false, showModal: true });
                }}
              >
                <i
                  className="fa fa-edit"
                  style={{
                    marginRight: 0,
                    marginLeft: "auto"
                  }}
                />
                &nbsp; Chỉnh sửa
              </li>

              <li
                onClick={e => {
                  this.handleClick(e, index, true);
                }}
              >
                <i
                  className="fa fa-trash-alt"
                  style={{
                    marginRight: 0,
                    marginLeft: "auto"
                  }}
                />
                &nbsp; Xoá
              </li>
            </ul>
          </Popover.Content>
        </Popover>
      </Overlay>
    );
  }

  handleClick(e, index, isHide) {
    let { show, activeIndex, listItem } = this.state;
    show[index] = !show[index];
    index !== activeIndex && (show[activeIndex] = false);
    this.setState({
      show: show,
      target: isHide ? null : e.target,
      activeIndex: index,
      selectedItem: listItem[index]
    });
  }

  renderIcon(type) {
    switch (type) {
      case "school":
        return (
          <i
            className="fa fa-school"
            style={{ color: "#c10101", fontSize: 17 }}
            onClick={e => {
              this.setState({ showModal: true });
            }}
          />
        );
      case "teacher":
        return (
          <i
            className="fa fa-chalkboard-teacher"
            style={{ color: "#c10101", fontSize: 17 }}
          />
        );
      default:
        return (
          <i
            className="fa fa-check-circle"
            style={{ color: "#2e9000", fontSize: 17 }}
          />
        );
    }
  }

  onClose = () => {
    this.setState({
      isCreateNew: false,
      showModal: false,
      showModalNgoaiKhoa: false
    });
  };

  render() {
    const bre = {
      title: "Chỉnh sửa lớp học",
      breadcrums: [
        {
          title: "Home",
          path: "/"
        },
        {
          title: "Chỉnh sửa lớp học",
          path: ""
        }
      ]
    };
    console.log("render account_body>>>");
    return (
      <React.Fragment>
        <PageHeader {...bre}></PageHeader>

        <div className="iq-card">
          <div className="iq-card-header d-flex justify-content-between">
            <div className="iq-header-title">
              <h6>
                <b>Thông tin buổi học</b>
              </h6>
            </div>
          </div>
          <div className="iq-card-body">
            <div className="row">
              <div className="col-3">
                <div className="form-group">
                  <p>
                    Cơ sở: <b> Hoàng Ngân</b>
                  </p>
                </div>
              </div>
              <div className="col-3">
                <div className="form-group">
                  <p>
                    Tên lớp: <b> BIBOP 123</b>
                  </p>
                </div>
              </div>
              <div className="col-3">
                <div className="form-group">
                  <p>
                    Ngày học: <b> 11/01/2021</b>
                  </p>
                </div>
              </div>
              <div className="col-3">
                <div className="form-group">
                  <p>
                    Giáo viên Việt Nam: <b> Nguyễn Văn A</b>
                  </p>
                </div>
              </div>
              <div className="col-3">
                <div className="form-group m-0">
                  <p className="m-0">
                    Mã lớp: <b> XXXXX</b>
                  </p>
                </div>
              </div>
              <div className="col-3">
                <div className="form-group m-0">
                  <p className="m-0">
                    Phòng học: <b> Mickey</b>
                  </p>
                </div>
              </div>
              <div className="col-3">
                <div className="form-group m-0">
                  <p className="m-0">
                    Giờ học: <b> 17h30</b>
                  </p>
                </div>
              </div>
              <div className="col-3">
                <div className="form-group m-0">
                  <p className="m-0">
                    Giáo viên Nước ngoài: <b> Jame</b>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="iq-card">
          <div className="iq-card-header d-flex justify-content-between">
            <div className="iq-header-title">
              <h6>
                <b>Thông tin buổi học</b>
              </h6>
            </div>
          </div>
          <div className="iq-card-body">
            <div className="row">
              <div className="col-6">
                <div className="form-group">
                  <label htmlFor="chuong_trinh_hoc">Chương trình học:</label>
                  <select
                    className="form-control form-control-lg"
                    id="chuong_trinh_hoc"
                    name="chuong_trinh_hoc"
                  >
                    {CHUONG_TRINH_HOC.map(item => (
                      <option
                        value={item.code}
                        disabled={item.code !== "" ? false : true}
                        selected={item.code !== "" ? false : true}
                        hidden={item.code !== "" ? false : true}
                      >
                        {item.text}
                      </option>
                    ))}
                  </select>
                </div>
              </div>
              <div className="col-6">
                <p>
                  Số buổi học theo chương trình: <b>70</b>
                </p>
              </div>
              <div className="col-6">
                <div className="form-group">
                  <label htmlFor="kieu_lap">Kiểu lập:</label>
                  <select
                    className="form-control form-control-lg"
                    id="kieu_lap"
                    name="kieu_lap"
                  >
                    {KIEU_LAP.map(item => (
                      <option
                        value={item.code}
                        disabled={item.code !== "" ? false : true}
                        selected={item.code !== "" ? false : true}
                        hidden={item.code !== "" ? false : true}
                      >
                        {item.text}
                      </option>
                    ))}
                  </select>
                </div>
              </div>
              <div className="col-6">
                <div className="form-group">
                  <label htmlFor="ngay_bat_dau">Ngày bắt đầu:</label>
                  <input
                    type="date"
                    className="form-control form-control-lg"
                    id="exampleInputdate"
                    defaultValue={moment().format("YYYY-MM-DD")}
                  />
                </div>
              </div>
              <div className="col-6">
                <div className="form-group">
                  <label htmlFor="dk_ket_thuc">Điều kiện kết thúc:</label>
                  <select
                    className="form-control form-control-lg"
                    id="dk_ket_thuc"
                    name="dk_ket_thuc"
                  >
                    {DK_KET_THUC.map(item => (
                      <option
                        value={item.code}
                        disabled={item.code !== "" ? false : true}
                        selected={item.code !== "" ? false : true}
                        hidden={item.code !== "" ? false : true}
                      >
                        {item.text}
                      </option>
                    ))}
                  </select>
                </div>
              </div>
              <div className="col-6">
                <div className="form-group">
                  <label htmlFor="so_buoi">Số buổi:</label>
                  <input
                    type="text"
                    name="so_buoi"
                    className="form-control form-control-lg"
                    id="so_buoi"
                  ></input>
                </div>
              </div>
              <div className="col-12">
                <ul className="vertical m-t-1">
                  {GIO_HOC.map((item, index) => {
                    return (
                      <li>
                        <input type="checkbox" name={`checkbox-${index}`} />
                        &nbsp; {item.text}
                      </li>
                    );
                  })}
                </ul>
              </div>
              <div className="col-6">
                <div className="form-group">
                  <label htmlFor="gio_bat_dau">Thời gian:</label>
                  <div className="row">
                    <div className="col-5">
                      <div className="form-group">
                        <input
                          type="text"
                          name="gio_bat_dau"
                          className="form-control form-control-lg"
                          id="gio_bat_dau"
                          // ref={register({ required: true, minLength: 8 })}
                        ></input>
                      </div>
                    </div>
                    <div className="col-5">
                      <div className="form-group">
                        <input
                          type="text"
                          name="gio_ket_thuc"
                          className="form-control form-control-lg"
                          id="gio_ket_thuc"
                          // ref={register({ required: true, minLength: 8 })}
                        ></input>
                      </div>
                    </div>
                    <div className="col-2">
                      <div className="btn-icon">
                        <i className="fa fa-plus" />
                      </div>
                    </div>
                  </div>
                </div>
                <div>
                  <p style={{ marginBottom: 0 }}>
                    <i>
                      Tạo nhiều ngày lập khác nhau nếu giờ học khác nhau theo
                      từng thứ trong tuần
                    </i>
                  </p>
                  <p style={{ marginBottom: 0 }}>
                    <i>Lập 1: Thứ 2, thứ 6 hàng tuần từ 15h30 tới 17h30</i>
                  </p>
                  <p style={{ marginBottom: 0 }}>
                    <i>Lập 2: Thứ 4 hàng tuần từ 17h30 tới 19h00</i>
                  </p>
                </div>
              </div>
              <div className="col-6"></div>
            </div>
          </div>
        </div>

        <div className="iq-card">
          <div className="iq-card-header d-flex justify-content-between">
            <div className="iq-header-title">
              <h6>
                <b>Địa điểm</b>
              </h6>
            </div>
          </div>
          <div className="iq-card-body">
            <div className="row">
              <div className="col-6">
                <div className="form-group">
                  <label htmlFor="co_so">Cơ sở:</label>
                  <select
                    className="form-control form-control-lg"
                    id="co_so"
                    name="co_so"
                  >
                    {TRUNG_TAM.map(item => (
                      <option
                        value={item.code}
                        disabled={item.code !== "" ? false : true}
                        selected={item.code !== "" ? false : true}
                        hidden={item.code !== "" ? false : true}
                      >
                        {item.text}
                      </option>
                    ))}
                  </select>
                </div>
              </div>
              <div className="col-6">
                <div className="form-group">
                  <label htmlFor="phong_hoc">Phòng học:</label>
                  <select
                    className="form-control form-control-lg"
                    id="phong_hoc"
                    name="phong_hoc"
                  >
                    {PHONG_HOC.map(item => (
                      <option
                        value={item.code}
                        disabled={item.code !== "" ? false : true}
                        selected={item.code !== "" ? false : true}
                        hidden={item.code !== "" ? false : true}
                      >
                        {item.text}
                      </option>
                    ))}
                  </select>
                </div>
              </div>
              <div className="col-12">
                <div className="form-group">
                  <div className="p-0">
                    <label>Kiểu tạo:</label>
                  </div>
                  <div className="row">
                    <div className="col-12">
                      <div className="custom-control custom-radio">
                        <input
                          className="custom-control-input"
                          id="tao_lich_trong"
                          name="tao_lich"
                          type="radio"
                          value="trong"
                          defaultChecked
                        />
                        <label
                          className="custom-control-label"
                          htmlFor="tao_lich_trong"
                        >
                          Sử dụng phòng này để tạo lịch, chấp nhận trùng lịch
                        </label>
                      </div>
                    </div>
                    <div className="col-12">
                      <div className="custom-control custom-radio">
                        <input
                          className="custom-control-input"
                          id="tao_lich_trung"
                          name="tao_lich"
                          type="radio"
                          value="trung"
                        />
                        <label
                          className="custom-control-label"
                          htmlFor="tao_lich_trung"
                        >
                          Tạo lịch nhưng để trống phòng nếu trùng (Khuyến cáo)
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="iq-card">
          <div className="iq-card-header d-flex justify-content-between">
            <div className="iq-header-title">
              <h6>
                <b>Giáo viên</b>
              </h6>
            </div>
          </div>
          <div className="iq-card-body">
            <div className="row">
              <div className="col-6">
                <div className="form-group">
                  <label htmlFor="co_so">Giáo viên người việt:</label>
                  <select
                    className="form-control form-control-lg"
                    id="co_so"
                    name="co_so"
                  >
                    {TRUNG_TAM.map(item => (
                      <option
                        value={item.code}
                        disabled={item.code !== "" ? false : true}
                        selected={item.code !== "" ? false : true}
                        hidden={item.code !== "" ? false : true}
                      >
                        {item.text}
                      </option>
                    ))}
                  </select>
                </div>
              </div>
              <div className="col-6">
                <div className="form-group">
                  <label htmlFor="phong_hoc">Giáo viên nước ngoài:</label>
                  <select
                    className="form-control form-control-lg"
                    id="phong_hoc"
                    name="phong_hoc"
                  >
                    {PHONG_HOC.map(item => (
                      <option
                        value={item.code}
                        disabled={item.code !== "" ? false : true}
                        selected={item.code !== "" ? false : true}
                        hidden={item.code !== "" ? false : true}
                      >
                        {item.text}
                      </option>
                    ))}
                  </select>
                </div>
              </div>
              <div className="col-12">
                <div className="form-group">
                  <div className="p-0">
                    <label>Kiểu tạo:</label>
                  </div>
                  <div className="row">
                    <div className="col-12">
                      <div className="custom-control custom-radio">
                        <input
                          className="custom-control-input"
                          id="su_dung_trung"
                          name="su_dung_gv"
                          type="radio"
                          value="trung"
                          defaultChecked
                        />
                        <label
                          className="custom-control-label"
                          htmlFor="su_dung_trung"
                        >
                          Sử dụng giáo viên chấp nhận trùng hoặc bận
                        </label>
                      </div>
                    </div>
                    <div className="col-12">
                      <div className="custom-control custom-radio">
                        <input
                          className="custom-control-input"
                          id="su_dung_trong"
                          name="su_dung_gv"
                          type="radio"
                          value="trong"
                        />
                        <label
                          className="custom-control-label"
                          htmlFor="su_dung_trong"
                        >
                          Tạo lịch nhưng để trống phòng nếu trùng hoặc bận
                          (Khuyến cáo)
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-12" style={{ textAlign: "right" }}>
                <div className="form-group">
                  <Button
                    variant="primary"
                    type="submit"
                    onClick={() =>
                      this.setState({ isCreateNew: true, showModal: true })
                    }
                  >
                    Tạo lịch
                  </Button>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="iq-card">
          <div className="iq-card-header d-flex justify-content-between">
            <div className="iq-header-title">
              <h6>
                <b>Thông tin buổi học</b>
              </h6>
            </div>
          </div>
          <div className="iq-card-body">
            <div className="row">
              <div className="col-12">
                <ul className="vertical" style={{ justifyContent: "flex-end" }}>
                  <li>
                    <i
                      className="fa fa-school"
                      style={{ color: "#c10101", fontSize: 17 }}
                    />
                    &nbsp; 3
                  </li>
                  <li>
                    <i
                      className="fa fa-chalkboard-teacher"
                      style={{ color: "#c10101", fontSize: 17 }}
                    />
                    &nbsp; 3
                  </li>
                </ul>
              </div>
              <div className="col-12">
                <div style={{ width: "100%" }}>
                  <div style={{ overflowX: "auto", width: "100%" }}>
                    <Table
                      bordered
                      hover
                      // style={{ minWidth: 1500 }}
                      className="table"
                    >
                      <thead>
                        <tr>
                          {HEADER.map(item => (
                            <th
                              className={`${item.class} ${
                                item.hasSort ? "sort" : ""
                              } ${
                                this.state.sortField === item.key
                                  ? "active"
                                  : ""
                              }`}
                              key={item.key}
                            >
                              <span>{item.title}</span>
                            </th>
                          ))}
                          <th></th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        {this.state.listItem.map((item, index) => (
                          <React.Fragment>
                            <tr key={`account_${item.id}`}>
                              <td>{item.id}</td>
                              <td>{item.ten_buoi_hoc}</td>
                              <td>{item.ngay_hoc}</td>
                              <td>{item.gio_hoc}</td>
                              <td>{item.phong_hoc}</td>
                              <td>{item.gv_viet_nam}</td>
                              <td>{item.gv_nuoc_ngoai}</td>
                              <td>{this.renderIcon(item.type)}</td>
                              <td>
                                <i
                                  className="fa fa-plus-circle"
                                  style={{ fontSize: 17 }}
                                  onClick={() =>
                                    this.setState({
                                      showModalNgoaiKhoa: true
                                    })
                                  }
                                />
                              </td>
                            </tr>
                          </React.Fragment>
                        ))}
                      </tbody>
                    </Table>
                    {this.state.listItem && this.state.listItem.length ? (
                      ""
                    ) : (
                      <div className="not-found">
                        <i class="fas fa-inbox"></i>
                        <span>Không tìm thấy kết quả</span>
                      </div>
                    )}
                  </div>
                </div>
                <Paging
                  pageNumber={10}
                  total={550}
                  pageSize={10}
                  onChange={console.log}
                ></Paging>
              </div>
            </div>
          </div>
        </div>
        {this.state.showModal && (
          <CustomModal
            isCreateNew={this.state.isCreateNew}
            onClose={this.onClose}
            selectedItem={this.state.selectedItem}
          ></CustomModal>
        )}
        {this.state.showModalNgoaiKhoa && (
          <ModalNgoaiKhoa onClose={this.onClose}></ModalNgoaiKhoa>
        )}
      </React.Fragment>
    );
  }
}

export default connect(({ HealthcareService }) => ({
  HealthcareService
}))(Page);
