import React, { useState } from "react";
import { Modal, Button } from "react-bootstrap";
import Rating from "react-rating";
import { connect } from "dva";
import CustomTable from "../../../components/Table";

const CustomModal = ({ handleClose, CustomerDetail }) => {
  const [show, setShow] = useState(true);
  // const updateRating = (value, index) => {
  //   console.log("updateRating", value, index);
  //   const { dispatch } = props;
  //   dispatch({
  //     type: "ScheduleAttendance/updaterating",
  //     payload: {
  //       rating: value * 2,
  //       index: index
  //     }
  //   });
  // };

  const columns = [
    {
      title: "Kỹ năng - Tiêu chí",
      key: "criteriaName",
      class: "tb-width-200",
      render: (col, index) => (
        <div className="text-left">
          <span style={{ paddingLeft: col.isChild ? 15 : 0 }}>
            {col.criteriaName}
          </span>
        </div>
      )
    },
    {
      title: "Đánh giá",
      key: "rating",
      class: "tb-width-300",
      render: (col, index) => (
        <Rating
          className="rating"
          emptySymbol="far fa-star"
          fullSymbol="fa fa-star"
          fractions={2}
          // onChange={value => updateRating(value, index)}
          readonly
          initialRating={col.point / 2}
        />
      )
    },
    {
      title: "Nhận xét",
      key: "comment",
      class: "tb-width-200"
    }
  ];

  return (
    <Modal
      size={"lg"}
      show={show}
      onHide={handleClose}
      backdrop="static"
      keyboard={false}
      id="ForgotPassword"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title>
          Nhận xét học viên {CustomerDetail?.current_student.name}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="row g-2">
          <div className="col-lg-4 col-md-6 col-sm-12">
            <p>
              Tên buổi học: <b>{CustomerDetail?.current_lesson?.lessonName}</b>
            </p>
          </div>
          <div className="col-lg-4 col-md-6 col-sm-12">
            <p>
              Phòng học: <b>{CustomerDetail?.current_lesson?.roomName}</b>
            </p>
          </div>
          <div className="col-lg-4 col-md-6 col-sm-12">
            <p>
              GVNN: <b>{CustomerDetail?.current_lesson?.teacherForeign}</b>
            </p>
          </div>
          <div className="col-lg-4 col-md-6 col-sm-12">
            <p>
              Ngày học: <b>{CustomerDetail?.current_lesson?.lessonDate}</b>
            </p>
          </div>

          <div className="col-lg-4 col-md-6 col-sm-12">
            <p>
              GV Việt: <b>{CustomerDetail?.current_lesson?.teacherDomestic}</b>
            </p>
          </div>

          <div className="col-4">
            <p>
              Trạng thái: <b>{CustomerDetail?.current_lesson?.status}</b>
            </p>
          </div>
        </div>

        <CustomTable
          dataSource={CustomerDetail?.current_buoi_hoc?.commentDetail}
          columns={columns}
          onChange={data => {}}
          noPaging
        ></CustomTable>
        <div className="row m-t-2">
          <div className="col-2">Nhận xét chung</div>
          <div className="col-10">
            {CustomerDetail?.current_buoi_hoc?.generalComment}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button
          variant={"primary"}
          style={{ marginRight: 15 }}
          onClick={handleClose}
        >
          Lưu
        </Button>
        <Button variant={"secondary"} onClick={handleClose}>
          Đóng
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default connect(({ CustomerDetail, ScheduleAttendance, Global }) => ({
  CustomerDetail,
  ScheduleAttendance,
  Global
}))(CustomModal);
