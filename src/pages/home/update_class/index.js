import React from "react";
import { connect } from "dva";
import AddClassSearch from "./update_class_header";
import PageHeader from "../../../components/PageHeader";

class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillMount() {
    const { dispatch } = this.props;
    const { id } = this.props.match.params;
    dispatch({
      type: "Global/showLoading"
    });
    Promise.all([
      dispatch({
        type: "Class/view",
        payload: {
          id
        }
      }),
      dispatch({
        type: "Class/listcenter"
      }),
      dispatch({
        type: "Class/listprogram"
      })
    ]).then(
      () => {
        dispatch({
          type: "Global/hideLoading"
        });
      },
      error => {
        dispatch({
          type: "Global/hideLoading"
        });
        dispatch({
          type: "Global/showError"
        });
      }
    );
  }

  render() {
    const bre = {
      title: "Thêm mới lớp học",
      breadcrums: [
        {
          title: "Home",
          path: "/"
        },
        {
          title: "Thêm mới lớp học",
          path: ""
        }
      ]
    };
    console.log("render danh_sach_lop>>>");

    return (
      <React.Fragment>
        <PageHeader {...bre}></PageHeader>
        <AddClassSearch />
      </React.Fragment>
    );
  }
}

export default connect(({ Class, Global }) => ({
  Class,
  Global
}))(Page);
