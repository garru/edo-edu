import React from 'react';
import './index.css';
import './ax.css';

class Page extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
		<div class="" id="base" style={{"margin-left":"-150px"}}>
		   <div class="ax_default shape" id="u11878">
		      <div class="" id="u11878_div">
		      </div>
		      <div class="text" id="u11878_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default shape" id="u11879">
		      <div class="" id="u11879_div">
		      </div>
		      <div class="text" id="u11879_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default" data-height="40" data-label="account" data-left="13" data-top="895" data-width="192" id="u11881">
		      <div class="ax_default shape" data-label="accountLable" id="u11882">
		         <img class="img" id="u11882_img" src="images/account_information/accountlable_u55.svg"/>
		         <div class="text" id="u11882_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Nguyen Van A
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u11883">
		         <img class="img" id="u11883_img" src="images/login/u4.svg"/>
		         <div class="text" id="u11883_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="settingMenu" data-left="11" data-top="826" data-width="125" id="u11884">
		      <div class="ax_default shape" data-label="gearIconLable" id="u11885">
		         <img class="img" id="u11885_img" src="images/account_information/geariconlable_u58.svg"/>
		         <div class="text" id="u11885_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Cài đặt
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="44" data-left="11" data-top="826" data-width="44" id="u11886">
		         <div class="ax_default shape" data-label="gearIconBG" id="u11887">
		            <div class="" id="u11887_div">
		            </div>
		            <div class="text" id="u11887_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default shape" data-label="gearIconBG2" id="u11888">
		            <div class="" id="u11888_div">
		            </div>
		            <div class="text" id="u11888_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" data-label="gearIcon" id="u11889">
		            <img class="img" id="u11889_img" src="images/account_information/gearicon_u62.svg"/>
		            <div class="text" id="u11889_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="customerMenu" data-left="11" data-top="145" data-width="143" id="u11890">
		      <div class="ax_default shape" data-label="customerIconLable" id="u11891">
		         <img class="img" id="u11891_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u11891_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Khách hàng
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="customerIconBG" id="u11892">
		         <div class="" id="u11892_div">
		         </div>
		         <div class="text" id="u11892_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="customerIconBG2" id="u11893">
		         <div class="" id="u11893_div">
		         </div>
		         <div class="text" id="u11893_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" data-label="customer" id="u11894">
		         <img class="img" id="u11894_img" src="images/account_information/customer_u67.svg"/>
		         <div class="text" id="u11894_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="classMenu" data-left="11" data-top="198" data-width="143" id="u11895">
		      <div class="ax_default shape" data-label="classIconLable" id="u11896">
		         <img class="img" id="u11896_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u11896_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Lớp học
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG" id="u11897">
		         <div class="" id="u11897_div">
		         </div>
		         <div class="text" id="u11897_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG2" id="u11898">
		         <div class="" id="u11898_div">
		         </div>
		         <div class="text" id="u11898_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" data-label="classicon" id="u11899">
		         <img class="img" id="u11899_img" src="images/account_information/classicon_u72.svg"/>
		         <div class="text" id="u11899_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="dashboardMenu" data-left="11" data-top="91" data-width="143" id="u11900">
		      <div class="ax_default shape" data-label="dashboardIconLable" id="u11901">
		         <img class="img" id="u11901_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u11901_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Dashnoard
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="dashboardIconBG" id="u11902">
		         <div class="" id="u11902_div">
		         </div>
		         <div class="text" id="u11902_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="dashboardIconBG2" id="u11903">
		         <div class="" id="u11903_div">
		         </div>
		         <div class="text" id="u11903_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" data-label="dashboard" id="u11904">
		         <img class="img" id="u11904_img" src="images/account_information/dashboard_u77.svg"/>
		         <div class="text" id="u11904_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="129" data-label="AccountMenu" data-left="58" data-top="806" data-width="246" id="u11905" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default shape" id="u11906">
		         <div class="" id="u11906_div">
		         </div>
		         <div class="text" id="u11906_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u11907">
		         <div class="" id="u11907_div">
		         </div>
		         <div class="text" id="u11907_text">
		            <p>
		               <span style={{"font-family":"ArialMT, Arial","font-weight":"400","text-decoration":"none"}}>
		                  Xin chào:
		               </span>
		               <span style={{"font-family":"Arial Bold, Arial","font-weight":"700","text-decoration":"none"}}>
		                  Nguyen Van A
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11908">
		         <div class="ax_default image" id="u11909">
		            <img class="img" id="u11909_img" src="images/account_information/u82.png"/>
		            <div class="text" id="u11909_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u11910">
		            <div class="" id="u11910_div">
		            </div>
		            <div class="text" id="u11910_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Đăng xuất
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default line" id="u11911">
		         <img class="img" id="u11911_img" src="images/account_information/u84.svg"/>
		         <div class="text" id="u11911_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11912">
		         <div class="ax_default paragraph" id="u11913">
		            <div class="" id="u11913_div">
		            </div>
		            <div class="text" id="u11913_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tài khoản
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default image" id="u11914">
		            <img class="img" id="u11914_img" src="images/account_information/u87.png"/>
		            <div class="text" id="u11914_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="339" data-label="SettingPannel" data-left="58" data-top="525" data-width="339" id="u11915" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default shape" id="u11916">
		         <div class="" id="u11916_div">
		         </div>
		         <div class="text" id="u11916_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u11917">
		         <div class="" id="u11917_div">
		         </div>
		         <div class="text" id="u11917_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Hệ thống
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11918">
		         <div class="ax_default icon" id="u11919">
		            <img class="img" id="u11919_img" src="images/account_information/u92.svg"/>
		            <div class="text" id="u11919_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u11920">
		            <div class="" id="u11920_div">
		            </div>
		            <div class="text" id="u11920_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tài khoản
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11921">
		         <div class="ax_default paragraph" id="u11922">
		            <div class="" id="u11922_div">
		            </div>
		            <div class="text" id="u11922_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phân quyền
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u11923">
		            <img class="img" id="u11923_img" src="images/account_information/u96.svg"/>
		            <div class="text" id="u11923_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u11924">
		         <div class="" id="u11924_div">
		         </div>
		         <div class="text" id="u11924_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Đào tạo
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11925">
		         <div class="ax_default paragraph" id="u11926">
		            <div class="" id="u11926_div">
		            </div>
		            <div class="text" id="u11926_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Thương hiệu
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u11927">
		            <img class="img" id="u11927_img" src="images/account_information/u100.svg"/>
		            <div class="text" id="u11927_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11928">
		         <div class="ax_default paragraph" id="u11929">
		            <div class="" id="u11929_div">
		            </div>
		            <div class="text" id="u11929_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trung tâm
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u11930">
		            <img class="img" id="u11930_img" src="images/account_information/u103.svg"/>
		            <div class="text" id="u11930_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11931">
		         <div class="ax_default icon" id="u11932">
		            <img class="img" id="u11932_img" src="images/account_information/u105.svg"/>
		            <div class="text" id="u11932_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u11933">
		            <div class="" id="u11933_div">
		            </div>
		            <div class="text" id="u11933_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Ngày nghỉ
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11934">
		         <div class="ax_default icon" id="u11935">
		            <img class="img" id="u11935_img" src="images/account_information/u108.svg"/>
		            <div class="text" id="u11935_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u11936">
		            <div class="" id="u11936_div">
		            </div>
		            <div class="text" id="u11936_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phòng ban - Vai trò
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11937">
		         <div class="ax_default paragraph" id="u11938">
		            <div class="" id="u11938_div">
		            </div>
		            <div class="text" id="u11938_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trạng thái học viên
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u11939">
		            <img class="img" id="u11939_img" src="images/account_information/u112.svg"/>
		            <div class="text" id="u11939_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11940">
		         <div class="ax_default paragraph" id="u11941">
		            <div class="" id="u11941_div">
		            </div>
		            <div class="text" id="u11941_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tiêu chí đánh giá
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u11942">
		            <img class="img" id="u11942_img" src="images/account_information/u115.svg"/>
		            <div class="text" id="u11942_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11943">
		         <div class="ax_default paragraph" id="u11944">
		            <div class="" id="u11944_div">
		            </div>
		            <div class="text" id="u11944_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Chương trình học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11945">
		            <div class="ax_default icon" id="u11946">
		               <img class="img" id="u11946_img" src="images/account_information/u119.svg"/>
		               <div class="text" id="u11946_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11947">
		         <div class="ax_default icon" id="u11948">
		            <img class="img" id="u11948_img" src="images/account_information/u121.svg"/>
		            <div class="text" id="u11948_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u11949">
		            <div class="" id="u11949_div">
		            </div>
		            <div class="text" id="u11949_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Bài học
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11950">
		         <div class="ax_default paragraph" id="u11951">
		            <div class="" id="u11951_div">
		            </div>
		            <div class="text" id="u11951_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phòng học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u11952">
		            <img class="img" id="u11952_img" src="images/account_information/u125.svg"/>
		            <div class="text" id="u11952_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11953">
		         <div class="ax_default paragraph" id="u11954">
		            <div class="" id="u11954_div">
		            </div>
		            <div class="text" id="u11954_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trường học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u11955">
		            <img class="img" id="u11955_img" src="images/account_information/u128.svg"/>
		            <div class="text" id="u11955_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default line" id="u11956">
		         <img class="img" id="u11956_img" src="images/account_information/u129.svg"/>
		         <div class="text" id="u11956_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u11957">
		         <div class="" id="u11957_div">
		         </div>
		         <div class="text" id="u11957_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Tài chính
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11958">
		         <div class="ax_default paragraph" id="u11959">
		            <div class="" id="u11959_div">
		            </div>
		            <div class="text" id="u11959_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Học phí
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u11960">
		            <img class="img" id="u11960_img" src="images/account_information/u133.svg"/>
		            <div class="text" id="u11960_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11961">
		         <div class="ax_default paragraph" id="u11962">
		            <div class="" id="u11962_div">
		            </div>
		            <div class="text" id="u11962_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Khuyến mại
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u11963">
		            <img class="img" id="u11963_img" src="images/account_information/u136.svg"/>
		            <div class="text" id="u11963_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="classMenu" data-left="11" data-top="252" data-width="143" id="u11964">
		      <div class="ax_default shape" data-label="classIconLable" id="u11965">
		         <img class="img" id="u11965_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u11965_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Giáo viên
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG" id="u11966">
		         <div class="" id="u11966_div">
		         </div>
		         <div class="text" id="u11966_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG2" id="u11967">
		         <div class="" id="u11967_div">
		         </div>
		         <div class="text" id="u11967_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u11968">
		         <img class="img" id="u11968_img" src="images/account_information/u141.svg"/>
		         <div class="text" id="u11968_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u11969">
		      <img class="img" id="u11969_img" src="images/account_information/u142.svg"/>
		      <div class="text" id="u11969_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u11970">
		      <img class="img" id="u11970_img" src="images/account_information/u143.svg"/>
		      <div class="text" id="u11970_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div id="u11877" style={{"display":"none","visibility":"hidden"}}>
		   </div>
		   <div class="ax_default paragraph" id="u11971">
		      <div class="" id="u11971_div">
		      </div>
		      <div class="text" id="u11971_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Chi tiết chăm sóc khách hàng
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default shape" id="u11972">
		      <div class="" id="u11972_div">
		      </div>
		      <div class="text" id="u11972_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default" id="u11973">
		      <div class="ax_default table_cell" id="u11974">
		         <img class="img" id="u11974_img" src="images/chi_ti_t_kh-trao___i/u11177.png"/>
		         <div class="text" id="u11974_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Thảo luận
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11975">
		         <img class="img" id="u11975_img" src="images/chi_ti_t_kh-trao___i/u11177.png"/>
		         <div class="text" id="u11975_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Lịch sử chăm sóc
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11976">
		         <img class="img" id="u11976_img" src="images/chi_ti_t_kh-trao___i/u11177.png"/>
		         <div class="text" id="u11976_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Hóa đơn
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11977">
		         <img class="img" id="u11977_img" src="images/chi_ti_t_kh-trao___i/u11177.png"/>
		         <div class="text" id="u11977_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Điểm danh/ nhận xét
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11978">
		         <img class="img" id="u11978_img" src="images/chi_ti_t_kh-l_ch_s__h_c/u11978.png"/>
		         <div class="text" id="u11978_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Lịch sử học
		               </span>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default shape" id="u11979">
		      <img class="img" id="u11979_img" src="images/chi_ti_t_kh-trao___i/u11182.svg"/>
		      <div class="text" id="u11979_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default" data-height="128" data-left="1581" data-top="130" data-width="178" id="u11980">
		      <div class="ax_default label" id="u11981">
		         <div class="" id="u11981_div">
		         </div>
		         <div class="text" id="u11981_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Hoàng Văn Huy
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default label" id="u11982">
		         <div class="" id="u11982_div">
		         </div>
		         <div class="text" id="u11982_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  HV-000002
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default label" id="u11983">
		         <div class="" id="u11983_div">
		         </div>
		         <div class="text" id="u11983_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  0384455522
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default label" id="u11984">
		         <div class="" id="u11984_div">
		         </div>
		         <div class="text" id="u11984_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  test@qa/team
		               </span>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default shape" id="u11985">
		      <div class="" id="u11985_div">
		      </div>
		      <div class="text" id="u11985_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default" id="u11986">
		      <div class="ax_default table_cell" id="u11987">
		         <img class="img" id="u11987_img" src="images/chi_ti_t_kh-l_ch_s__h_c/u11987.png"/>
		         <div class="text" id="u11987_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Tên khóa học
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11988">
		         <img class="img" id="u11988_img" src="images/chi_ti_t_kh-l_ch_s__h_c/u11988.png"/>
		         <div class="text" id="u11988_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Ngày bắt đầu
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11989">
		         <img class="img" id="u11989_img" src="images/chi_ti_t_kh-l_ch_s__h_c/u11989.png"/>
		         <div class="text" id="u11989_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Ngày kết thúc
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11990">
		         <img class="img" id="u11990_img" src="images/chi_ti_t_kh-l_ch_s__h_c/u11990.png"/>
		         <div class="text" id="u11990_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Số buổi tham gia
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11991">
		         <img class="img" id="u11991_img" src="images/chi_ti_t_kh-l_ch_s__h_c/u11991.png"/>
		         <div class="text" id="u11991_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  BIBBOP 2
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11992">
		         <img class="img" id="u11992_img" src="images/chi_ti_t_kh-l_ch_s__h_c/u11992.png"/>
		         <div class="text" id="u11992_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  11/01/2020
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11993">
		         <img class="img" id="u11993_img" src="images/chi_ti_t_kh-l_ch_s__h_c/u11993.png"/>
		         <div class="text" id="u11993_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  11/01/2021
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11994">
		         <img class="img" id="u11994_img" src="images/chi_ti_t_kh-l_ch_s__h_c/u11994.png"/>
		         <div class="text" id="u11994_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  98/100
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11995">
		         <img class="img" id="u11995_img" src="images/chi_ti_t_kh-l_ch_s__h_c/u11995.png"/>
		         <div class="text" id="u11995_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  BIBBOP 1
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11996">
		         <img class="img" id="u11996_img" src="images/chi_ti_t_kh-l_ch_s__h_c/u11996.png"/>
		         <div class="text" id="u11996_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  11/01/2019
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11997">
		         <img class="img" id="u11997_img" src="images/chi_ti_t_kh-l_ch_s__h_c/u11997.png"/>
		         <div class="text" id="u11997_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  11/01/2020
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11998">
		         <img class="img" id="u11998_img" src="images/chi_ti_t_kh-l_ch_s__h_c/u11998.png"/>
		         <div class="text" id="u11998_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  100/100
		               </span>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default label" id="u11999">
		      <div class="" id="u11999_div">
		      </div>
		      <div class="text" id="u11999_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Con của phụ huynh
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default droplist" id="u12000">
		      <div class="" id="u12000_div">
		      </div>
		      <select class="u12000_input" id="u12000_input">
		      </select>
		   </div>
		   <div class="ax_default label" id="u12001">
		      <div class="" id="u12001_div">
		      </div>
		      <div class="text" id="u12001_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Lớp học tham gia
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default droplist" id="u12002">
		      <div class="" id="u12002_div">
		      </div>
		      <select class="u12002_input" id="u12002_input">
		      </select>
		   </div>
		   <div class="ax_default primary_button" id="u12003">
		      <div class="" id="u12003_div">
		      </div>
		      <div class="text" id="u12003_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Lọc
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="950" data-label="nhận xét" data-left="7" data-top="1039" data-width="1920" id="u12004" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default" data-height="0" data-label="Nhận xét" data-left="0" data-top="0" data-width="0" id="u12005">
		         <div class="ax_default shape" id="u12006">
		            <div class="" id="u12006_div">
		            </div>
		            <div class="text" id="u12006_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u12007">
		            <div class="ax_default shape" id="u12008">
		               <div class="" id="u12008_div">
		               </div>
		               <div class="text" id="u12008_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default paragraph" id="u12009">
		               <div class="" id="u12009_div">
		               </div>
		               <div class="text" id="u12009_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Nhận xét học viên Bùi Văn Tích
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default" id="u12010">
		               <div class="ax_default table_cell" id="u12011">
		                  <img class="img" id="u12011_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11679.png"/>
		                  <div class="text" id="u12011_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Kỹ năng - Tiêu chí
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u12012">
		                  <img class="img" id="u12012_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11680.png"/>
		                  <div class="text" id="u12012_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Đánh giá
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u12013">
		                  <img class="img" id="u12013_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11681.png"/>
		                  <div class="text" id="u12013_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Thông điệp gửi tới phụ huynh
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u12014">
		                  <img class="img" id="u12014_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11682.png"/>
		                  <div class="text" id="u12014_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Kiến thức
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u12015">
		                  <img class="img" id="u12015_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11683.png"/>
		                  <div class="text" id="u12015_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u12016">
		                  <img class="img" id="u12016_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11684.png"/>
		                  <div class="text" id="u12016_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u12017">
		                  <img class="img" id="u12017_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11685.png"/>
		                  <div class="text" id="u12017_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Ngữ pháp
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u12018">
		                  <img class="img" id="u12018_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11686.png"/>
		                  <div class="text" id="u12018_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u12019">
		                  <img class="img" id="u12019_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11687.png"/>
		                  <div class="text" id="u12019_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u12020">
		                  <img class="img" id="u12020_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11682.png"/>
		                  <div class="text" id="u12020_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Từ vựng
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u12021">
		                  <img class="img" id="u12021_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11683.png"/>
		                  <div class="text" id="u12021_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u12022">
		                  <img class="img" id="u12022_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11684.png"/>
		                  <div class="text" id="u12022_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u12023">
		                  <img class="img" id="u12023_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11691.png"/>
		                  <div class="text" id="u12023_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Kỹ năng
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u12024">
		                  <img class="img" id="u12024_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11692.png"/>
		                  <div class="text" id="u12024_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u12025">
		                  <img class="img" id="u12025_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11693.png"/>
		                  <div class="text" id="u12025_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
		                        </span>
		                     </p>
		                  </div>
		               </div>
		            </div>
		            <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u12026">
		               <div class="ax_default icon" id="u12027">
		                  <img class="img" id="u12027_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u12027_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u12028">
		                  <img class="img" id="u12028_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u12028_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u12029">
		                  <img class="img" id="u12029_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u12029_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u12030">
		                  <img class="img" id="u12030_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u12030_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u12031">
		                  <img class="img" id="u12031_img" src="images/_i_m_danh__nh_n_x_t/u7754.svg"/>
		                  <div class="text" id="u12031_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		            </div>
		            <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u12032">
		               <div class="ax_default icon" id="u12033">
		                  <img class="img" id="u12033_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u12033_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u12034">
		                  <img class="img" id="u12034_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u12034_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u12035">
		                  <img class="img" id="u12035_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u12035_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u12036">
		                  <img class="img" id="u12036_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u12036_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u12037">
		                  <img class="img" id="u12037_img" src="images/_i_m_danh__nh_n_x_t/u7754.svg"/>
		                  <div class="text" id="u12037_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		            </div>
		            <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u12038">
		               <div class="ax_default icon" id="u12039">
		                  <img class="img" id="u12039_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u12039_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u12040">
		                  <img class="img" id="u12040_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u12040_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u12041">
		                  <img class="img" id="u12041_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u12041_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u12042">
		                  <img class="img" id="u12042_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u12042_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u12043">
		                  <img class="img" id="u12043_img" src="images/_i_m_danh__nh_n_x_t/u7754.svg"/>
		                  <div class="text" id="u12043_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		            </div>
		            <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u12044">
		               <div class="ax_default icon" id="u12045">
		                  <img class="img" id="u12045_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u12045_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u12046">
		                  <img class="img" id="u12046_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u12046_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u12047">
		                  <img class="img" id="u12047_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u12047_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u12048">
		                  <img class="img" id="u12048_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u12048_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u12049">
		                  <img class="img" id="u12049_img" src="images/_i_m_danh__nh_n_x_t/u7754.svg"/>
		                  <div class="text" id="u12049_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		            </div>
		            <div class="ax_default primary_button" id="u12050">
		               <div class="" id="u12050_div">
		               </div>
		               <div class="text" id="u12050_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Lưu
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default button" id="u12051">
		               <div class="" id="u12051_div">
		               </div>
		               <div class="text" id="u12051_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Đóng
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u12052">
		               <div class="" id="u12052_div">
		               </div>
		               <div class="text" id="u12052_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Nhận xét chung
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default paragraph" id="u12053">
		               <div class="" id="u12053_div">
		               </div>
		               <div class="text" id="u12053_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget odio. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default shape" id="u12054">
		               <div class="" id="u12054_div">
		               </div>
		               <div class="text" id="u12054_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u12055">
		               <div class="" id="u12055_div">
		               </div>
		               <div class="text" id="u12055_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Tên buổi học
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u12056">
		               <div class="" id="u12056_div">
		               </div>
		               <div class="text" id="u12056_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Introduction-Greetings
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u12057">
		               <div class="" id="u12057_div">
		               </div>
		               <div class="text" id="u12057_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Ngày học
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u12058">
		               <div class="" id="u12058_div">
		               </div>
		               <div class="text" id="u12058_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        11/01/2021 17:30
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u12059">
		               <div class="" id="u12059_div">
		               </div>
		               <div class="text" id="u12059_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Phòng học
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u12060">
		               <div class="" id="u12060_div">
		               </div>
		               <div class="text" id="u12060_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Mickey
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u12061">
		               <div class="" id="u12061_div">
		               </div>
		               <div class="text" id="u12061_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        GV Việt
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u12062">
		               <div class="" id="u12062_div">
		               </div>
		               <div class="text" id="u12062_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Nguyễn Hoàng
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u12063">
		               <div class="" id="u12063_div">
		               </div>
		               <div class="text" id="u12063_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        GVNN
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u12064">
		               <div class="" id="u12064_div">
		               </div>
		               <div class="text" id="u12064_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Jame
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u12065">
		               <div class="" id="u12065_div">
		               </div>
		               <div class="text" id="u12065_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Trạng thái
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u12066">
		               <div class="" id="u12066_div">
		               </div>
		               <div class="text" id="u12066_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Có mặt
		                     </span>
		                  </p>
		               </div>
		            </div>
		         </div>
		      </div>
		   </div>
		</div>
		
      </div>
    );
  }
}

export default Page;
