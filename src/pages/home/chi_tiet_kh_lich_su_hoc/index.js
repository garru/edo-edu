import React, { useState, useEffect } from "react";
import { Badge, Nav } from "react-bootstrap";
import { connect } from "dva";
import "react-datepicker/dist/react-datepicker.css";
import CustomTable from "../../../components/Table";
import UploadAvatar from "../../../components/UploadAvatar";
import { useParams } from "react-router-dom";
import moment from "moment";
import PageHeader from "../../../components/PageHeader";

const Page = props => {
  const [showModal, setShowModal] = useState(false);
  const [count, setCount] = useState(0);
  const { id } = useParams();
  const { dispatch } = props;
  const [selectedStudent, setSelectedStudent] = useState(null);

  const bre = {
    title: "Chi tiết chăm sóc khách hàng",
    breadcrums: [
      {
        title: "Home",
        path: "/"
      },
      {
        title: "Chi tiết chăm sóc khách hàng",
        path: ""
      }
    ]
  };

  useEffect(() => {
    dispatch({
      type: "Global/showLoading"
    });
    Promise.all([
      dispatch({
        type: "CustomerDetail/view",
        payload: { id }
      }),
      dispatch({
        type: "CustomerDetail/liststudentbyid",
        payload: { id }
      })
    ]).then(data => {
      dispatch({
        type: "Global/hideLoading"
      });
    });
  }, [count]);

  const onChangeStudent = e => {
    const id = +e.target.value;
    setSelectedStudent(id);
    const student = props.CustomerDetail?.list_student?.find(
      item => item.id === id
    );
    dispatch({
      type: "Global/showLoading"
    });
    dispatch({
      type: "CustomerDetail/selectstudent",
      payload: student
    });
    dispatch({
      type: "CustomerDetail/liststudentbystudentid",
      payload: { id }
    }).then(data => {
      dispatch({
        type: "Global/hideLoading"
      });
    });
  };

  const onChangeClass = e => {
    const id = +e.target.value;
    dispatch({
      type: "Global/showLoading"
    });
    dispatch({
      type: "CustomerDetail/listcourse",
      payload: { id }
    }).then(data => {
      dispatch({
        type: "Global/hideLoading"
      });
    });
  };

  const handleCloseModal = () => {
    setShowModal(false);
  };

  const handleEyeIconClick = col => {
    dispatch({
      type: "Global/showLoading"
    });
    dispatch({
      type: "CustomerDetail/selectlesson",
      payload: col
    });
    dispatch({
      type: "CustomerDetail/viewlesson",
      payload: { studentId: selectedStudent, lessonId: col.lessonId }
    }).then(data => {
      dispatch({
        type: "Global/hideLoading"
      });
      if (data) {
        setShowModal(true);
      } else {
        dispatch({
          type: "Global/showError"
        });
      }
    });
  };

  const columns = [
    {
      title: "Tên khóa học",
      key: "courseName",
      class: "tb-width-150"
    },
    {
      title: "Ngày bắt đầu",
      key: "beginDate",
      class: "tb-width-100",
      render: (col, index) => (
        <span>{moment(col.beginDate).format("DD/MM/YYYY")}</span>
      )
    },
    {
      title: "Ngày kết thúc",
      key: "endDate",
      class: "tb-width-100",
      render: (col, index) => (
        <span>{moment(col.endDate).format("DD/MM/YYYY")}</span>
      )
    },
    {
      title: "Số buổi tham gia",
      key: "joinRate",
      class: "tb-width-100"
    }
  ];

  return (
    <>
    <PageHeader {...bre} />
    <div className="iq-card ">
      <div className="iq-card-body">
        <div className="">
          <div className="row">
            <div className="col-12">
              <Nav fill variant="pills" className="m-b-2" defaultActiveKey={`/lich_su_hoc/${id}`}>
                <Nav.Item>
                  <Nav.Link key="trao_doi" href={`/trao_doi/${id}`}>
                    Thảo luận
                  </Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link key="cham_soc" href={`/cham_soc/${id}`}>
                    Lịch sử chăm sóc
                  </Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link key="hoa_don" href={`/hoa_don/${id}`}>
                    Hóa đơn
                  </Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link key="dd_nhan_xet" href={`/dd_nhan_xet/${id}`}>
                    Điểm danh/ nhận xét
                  </Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link key="lich_su_hoc" href={`/lich_su_hoc/${id}`}>
                    Lịch sử khác
                  </Nav.Link>
                </Nav.Item>
                {/*<Nav.Item>*/}
                {/*  <Nav.Link href="/kiem_tra_hoc_thu">Kiểm tra/học thử</Nav.Link>*/}
                {/*</Nav.Item>*/}
              </Nav>
            </div>

            <div className="col-12 ">
              <div className="row">
                <div className="col-lg-3 col-md-12 col-sm-12 text-center">
                  <UploadAvatar
                    
                    src={props.curent_customer_detail?.avatar}
                  ></UploadAvatar>
                </div>
                <div className="col-lg-9 col-md-12 mt-4 p-4">
                  <div className="row">
                    <div className="col-4">
                      <div className="form-group">
                        <label htmlFor="hoc_sinh">Con của phụ huynh</label>
                        <select
                          className="form-control form-control-lg"
                          name="hoc_sinh"
                          id="hoc_sinh"
                          onChange={e => onChangeStudent(e)}
                          defaultValue={""}
                        >
                          <option value="" disabled hidden>
                            Vui lòng chọn giá trị
                          </option>
                          {props.CustomerDetail?.list_student?.map(
                            (item, index) => (
                              <option key={index} value={item.id}>
                                {item.name}
                              </option>
                            )
                          )}
                        </select>
                      </div>
                    </div>
                    <div className="col-4">
                      <div className="form-group">
                        <label htmlFor="lop_hoc">Lớp học tham gia</label>
                        <select
                          className="form-control form-control-lg"
                          name="lop_hoc"
                          id="lop_hoc"
                          defaultValue={""}
                          onChange={e => onChangeClass(e)}
                          disabled={!props.CustomerDetail?.list_class?.length}
                        >
                          <option value="" disabled hidden>
                            Vui lòng chọn giá trị
                          </option>
                          {props.CustomerDetail?.list_class?.map(
                            (item, index) => (
                              <option key={index} value={item.id}>
                                {item.name}
                              </option>
                            )
                          )}
                        </select>
                      </div>
                    </div>
                  </div>
                  <CustomTable
                    dataSource={props.CustomerDetail?.list_lich_su_hoc}
                    total={0}
                    columns={columns}
                    onChange={data => {}}
                    noPaging
                  ></CustomTable>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    </>
  );
};

export default connect(({ CustomerDetail, Global }) => ({
  CustomerDetail,
  Global
}))(Page);
