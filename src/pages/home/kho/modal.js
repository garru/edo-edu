import React from "react";
import { Modal, Button } from "react-bootstrap";
import coso from "../../../mock/coso";
import danhmuc from "../../../mock/danhmuc";
export default function CustomModal({ isCreateNew, selectedItem, handleCloseModal }) {
  // const [showModal, setShowModal] = useState(true);
  const handleClose = function() {
    handleCloseModal();
  };
  return (
    <Modal show>
      <Modal.Header closeButton>
        <Modal.Title>
          {isCreateNew ? "Thêm mới kho" : "Chỉnh sửa kho"}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div>
          <form>
            <div className="iq-card ">
              <div className="row m-b-2">
                <div className="col-12">
                  <div className="form-group">
                    <label htmlFor="ten_kho">Tên kho</label>
                    <input
                      type="text"
                      name="ten_kho"
                      className="form-control form-control-lg"
                      id="ten_kho"
                      defaultValue={!isCreateNew ? selectedItem.ten_kho : ""}/>
                  </div>
                  <div className="form-group">
                    <label htmlFor="co_so">Cơ sở</label>
                    <select
                      name="co_so"
                      className="form-control form-control-lg"
                      id="co_so"
                      defaultValue={!isCreateNew ? selectedItem.co_so : ""}>
                      {coso.map(item =>  (
                        <option value={item.value}>{item.name}</option>
                      ))}
                    </select>
                  </div>
                  <div className="form-group">
                    <label htmlFor="danh_muc_chuc_nang">Danh mục chức năng</label>
                    <select
                      name="danh_muc_chuc_nang"
                      className="form-control form-control-lg"
                      id="danh_muc_chuc_nang"
                      defaultValue={!isCreateNew ? selectedItem.danh_muc_chuc_nang : ""}>
                      {danhmuc.map(item =>  (
                        <option value={item.value}>{item.name}</option>
                      ))}
                    </select>
                  </div>
                  <div className="form-group">
                    <label htmlFor="ten_kho">Quản lý</label>
                    <input
                      type="text"
                      name="quan_ly"
                      className="form-control form-control-lg"
                      id="quan_ly"
                      defaultValue={!isCreateNew ? selectedItem.quan_ly : ""} />
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="primary" style={{marginRight: 15}}>
          {isCreateNew ? "Tạo mới" : "Cập nhật"}
        </Button>
        <Button variant={"secondary"} onClick={handleClose}>
          Hủy
        </Button>
      </Modal.Footer>
    </Modal>
  );
}
