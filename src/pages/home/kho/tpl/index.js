import React from 'react';
import './index.css';
import './ax.css';

class Page extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
		<div class="" id="base" style={{"margin-left":"-150px"}}>
		   <div class="ax_default shape" id="u4306">
		      <div class="" id="u4306_div">
		      </div>
		      <div class="text" id="u4306_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default shape" id="u4307">
		      <div class="" id="u4307_div">
		      </div>
		      <div class="text" id="u4307_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default" data-height="40" data-label="account" data-left="13" data-top="895" data-width="192" id="u4309">
		      <div class="ax_default shape" data-label="accountLable" id="u4310">
		         <img class="img" id="u4310_img" src="images/account_information/accountlable_u55.svg"/>
		         <div class="text" id="u4310_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Nguyen Van A
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u4311">
		         <img class="img" id="u4311_img" src="images/login/u4.svg"/>
		         <div class="text" id="u4311_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="settingMenu" data-left="11" data-top="826" data-width="125" id="u4312">
		      <div class="ax_default shape" data-label="gearIconLable" id="u4313">
		         <img class="img" id="u4313_img" src="images/account_information/geariconlable_u58.svg"/>
		         <div class="text" id="u4313_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Cài đặt
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="44" data-left="11" data-top="826" data-width="44" id="u4314">
		         <div class="ax_default shape" data-label="gearIconBG" id="u4315">
		            <div class="" id="u4315_div">
		            </div>
		            <div class="text" id="u4315_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default shape" data-label="gearIconBG2" id="u4316">
		            <div class="" id="u4316_div">
		            </div>
		            <div class="text" id="u4316_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" data-label="gearIcon" id="u4317">
		            <img class="img" id="u4317_img" src="images/account_information/gearicon_u62.svg"/>
		            <div class="text" id="u4317_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="customerMenu" data-left="11" data-top="145" data-width="143" id="u4318">
		      <div class="ax_default shape" data-label="customerIconLable" id="u4319">
		         <img class="img" id="u4319_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u4319_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Khách hàng
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="customerIconBG" id="u4320">
		         <div class="" id="u4320_div">
		         </div>
		         <div class="text" id="u4320_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="customerIconBG2" id="u4321">
		         <div class="" id="u4321_div">
		         </div>
		         <div class="text" id="u4321_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" data-label="customer" id="u4322">
		         <img class="img" id="u4322_img" src="images/account_information/customer_u67.svg"/>
		         <div class="text" id="u4322_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="classMenu" data-left="11" data-top="198" data-width="143" id="u4323">
		      <div class="ax_default shape" data-label="classIconLable" id="u4324">
		         <img class="img" id="u4324_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u4324_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Lớp học
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG" id="u4325">
		         <div class="" id="u4325_div">
		         </div>
		         <div class="text" id="u4325_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG2" id="u4326">
		         <div class="" id="u4326_div">
		         </div>
		         <div class="text" id="u4326_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" data-label="classicon" id="u4327">
		         <img class="img" id="u4327_img" src="images/account_information/classicon_u72.svg"/>
		         <div class="text" id="u4327_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="dashboardMenu" data-left="11" data-top="91" data-width="143" id="u4328">
		      <div class="ax_default shape" data-label="dashboardIconLable" id="u4329">
		         <img class="img" id="u4329_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u4329_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Dashnoard
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="dashboardIconBG" id="u4330">
		         <div class="" id="u4330_div">
		         </div>
		         <div class="text" id="u4330_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="dashboardIconBG2" id="u4331">
		         <div class="" id="u4331_div">
		         </div>
		         <div class="text" id="u4331_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" data-label="dashboard" id="u4332">
		         <img class="img" id="u4332_img" src="images/account_information/dashboard_u77.svg"/>
		         <div class="text" id="u4332_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="129" data-label="AccountMenu" data-left="58" data-top="806" data-width="246" id="u4333" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default shape" id="u4334">
		         <div class="" id="u4334_div">
		         </div>
		         <div class="text" id="u4334_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u4335">
		         <div class="" id="u4335_div">
		         </div>
		         <div class="text" id="u4335_text">
		            <p>
		               <span style={{"font-family":"ArialMT, Arial","font-weight":"400","text-decoration":"none"}}>
		                  Xin chào:
		               </span>
		               <span style={{"font-family":"Arial Bold, Arial","font-weight":"700","text-decoration":"none"}}>
		                  Nguyen Van A
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4336">
		         <div class="ax_default image" id="u4337">
		            <img class="img" id="u4337_img" src="images/account_information/u82.png"/>
		            <div class="text" id="u4337_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u4338">
		            <div class="" id="u4338_div">
		            </div>
		            <div class="text" id="u4338_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Đăng xuất
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default line" id="u4339">
		         <img class="img" id="u4339_img" src="images/account_information/u84.svg"/>
		         <div class="text" id="u4339_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4340">
		         <div class="ax_default paragraph" id="u4341">
		            <div class="" id="u4341_div">
		            </div>
		            <div class="text" id="u4341_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tài khoản
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default image" id="u4342">
		            <img class="img" id="u4342_img" src="images/account_information/u87.png"/>
		            <div class="text" id="u4342_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="339" data-label="SettingPannel" data-left="58" data-top="525" data-width="339" id="u4343" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default shape" id="u4344">
		         <div class="" id="u4344_div">
		         </div>
		         <div class="text" id="u4344_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u4345">
		         <div class="" id="u4345_div">
		         </div>
		         <div class="text" id="u4345_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Hệ thống
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4346">
		         <div class="ax_default icon" id="u4347">
		            <img class="img" id="u4347_img" src="images/account_information/u92.svg"/>
		            <div class="text" id="u4347_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u4348">
		            <div class="" id="u4348_div">
		            </div>
		            <div class="text" id="u4348_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tài khoản
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4349">
		         <div class="ax_default paragraph" id="u4350">
		            <div class="" id="u4350_div">
		            </div>
		            <div class="text" id="u4350_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phân quyền
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u4351">
		            <img class="img" id="u4351_img" src="images/account_information/u96.svg"/>
		            <div class="text" id="u4351_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u4352">
		         <div class="" id="u4352_div">
		         </div>
		         <div class="text" id="u4352_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Đào tạo
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4353">
		         <div class="ax_default paragraph" id="u4354">
		            <div class="" id="u4354_div">
		            </div>
		            <div class="text" id="u4354_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Thương hiệu
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u4355">
		            <img class="img" id="u4355_img" src="images/account_information/u100.svg"/>
		            <div class="text" id="u4355_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4356">
		         <div class="ax_default paragraph" id="u4357">
		            <div class="" id="u4357_div">
		            </div>
		            <div class="text" id="u4357_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trung tâm
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u4358">
		            <img class="img" id="u4358_img" src="images/account_information/u103.svg"/>
		            <div class="text" id="u4358_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4359">
		         <div class="ax_default icon" id="u4360">
		            <img class="img" id="u4360_img" src="images/account_information/u105.svg"/>
		            <div class="text" id="u4360_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u4361">
		            <div class="" id="u4361_div">
		            </div>
		            <div class="text" id="u4361_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Ngày nghỉ
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4362">
		         <div class="ax_default icon" id="u4363">
		            <img class="img" id="u4363_img" src="images/account_information/u108.svg"/>
		            <div class="text" id="u4363_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u4364">
		            <div class="" id="u4364_div">
		            </div>
		            <div class="text" id="u4364_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phòng ban - Vai trò
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4365">
		         <div class="ax_default paragraph" id="u4366">
		            <div class="" id="u4366_div">
		            </div>
		            <div class="text" id="u4366_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trạng thái học viên
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u4367">
		            <img class="img" id="u4367_img" src="images/account_information/u112.svg"/>
		            <div class="text" id="u4367_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4368">
		         <div class="ax_default paragraph" id="u4369">
		            <div class="" id="u4369_div">
		            </div>
		            <div class="text" id="u4369_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tiêu chí đánh giá
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u4370">
		            <img class="img" id="u4370_img" src="images/account_information/u115.svg"/>
		            <div class="text" id="u4370_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4371">
		         <div class="ax_default paragraph" id="u4372">
		            <div class="" id="u4372_div">
		            </div>
		            <div class="text" id="u4372_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Chương trình học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4373">
		            <div class="ax_default icon" id="u4374">
		               <img class="img" id="u4374_img" src="images/account_information/u119.svg"/>
		               <div class="text" id="u4374_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4375">
		         <div class="ax_default icon" id="u4376">
		            <img class="img" id="u4376_img" src="images/account_information/u121.svg"/>
		            <div class="text" id="u4376_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u4377">
		            <div class="" id="u4377_div">
		            </div>
		            <div class="text" id="u4377_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Bài học
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4378">
		         <div class="ax_default paragraph" id="u4379">
		            <div class="" id="u4379_div">
		            </div>
		            <div class="text" id="u4379_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phòng học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u4380">
		            <img class="img" id="u4380_img" src="images/account_information/u125.svg"/>
		            <div class="text" id="u4380_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4381">
		         <div class="ax_default paragraph" id="u4382">
		            <div class="" id="u4382_div">
		            </div>
		            <div class="text" id="u4382_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trường học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u4383">
		            <img class="img" id="u4383_img" src="images/account_information/u128.svg"/>
		            <div class="text" id="u4383_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default line" id="u4384">
		         <img class="img" id="u4384_img" src="images/account_information/u129.svg"/>
		         <div class="text" id="u4384_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u4385">
		         <div class="" id="u4385_div">
		         </div>
		         <div class="text" id="u4385_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Tài chính
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4386">
		         <div class="ax_default paragraph" id="u4387">
		            <div class="" id="u4387_div">
		            </div>
		            <div class="text" id="u4387_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Học phí
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u4388">
		            <img class="img" id="u4388_img" src="images/account_information/u133.svg"/>
		            <div class="text" id="u4388_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4389">
		         <div class="ax_default paragraph" id="u4390">
		            <div class="" id="u4390_div">
		            </div>
		            <div class="text" id="u4390_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Khuyến mại
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u4391">
		            <img class="img" id="u4391_img" src="images/account_information/u136.svg"/>
		            <div class="text" id="u4391_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="classMenu" data-left="11" data-top="252" data-width="143" id="u4392">
		      <div class="ax_default shape" data-label="classIconLable" id="u4393">
		         <img class="img" id="u4393_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u4393_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Giáo viên
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG" id="u4394">
		         <div class="" id="u4394_div">
		         </div>
		         <div class="text" id="u4394_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG2" id="u4395">
		         <div class="" id="u4395_div">
		         </div>
		         <div class="text" id="u4395_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u4396">
		         <img class="img" id="u4396_img" src="images/account_information/u141.svg"/>
		         <div class="text" id="u4396_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u4397">
		      <img class="img" id="u4397_img" src="images/account_information/u142.svg"/>
		      <div class="text" id="u4397_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u4398">
		      <img class="img" id="u4398_img" src="images/account_information/u143.svg"/>
		      <div class="text" id="u4398_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div id="u4305" style={{"display":"none","visibility":"hidden"}}>
		   </div>
		   <div class="ax_default" data-height="55" data-label="Header" data-left="133" data-top="17" data-width="516" id="u4399">
		      <div class="ax_default paragraph" id="u4400">
		         <div class="" id="u4400_div">
		         </div>
		         <div class="text" id="u4400_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Kho
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u4401">
		         <div class="" id="u4401_div">
		         </div>
		         <div class="text" id="u4401_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Thống kê, quản lý những kho hiện có hoặc thêm mới kho
		               </span>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u4402">
		      <div class="" id="u4402_div">
		      </div>
		      <div class="text" id="u4402_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Thêm mới
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default heading_3" id="u4403">
		      <div class="" id="u4403_div">
		      </div>
		      <div class="text" id="u4403_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Tìm kiếm
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default" data-height="25" data-left="248" data-top="105" data-width="182" id="u4404">
		      <div class="ax_default text_field" id="u4405">
		         <div class="" id="u4405_div">
		         </div>
		         <input class="u4405_input" id="u4405_input" type="text" value=""/>
		      </div>
		      <div class="ax_default icon" id="u4406">
		         <img class="img" id="u4406_img" src="images/kho/u4406.svg"/>
		         <div class="text" id="u4406_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" id="u4407">
		      <div class="ax_default table_cell" id="u4408">
		         <img class="img" id="u4408_img" src="images/kho/u4408.png"/>
		         <div class="text" id="u4408_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  STT
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4409">
		         <img class="img" id="u4409_img" src="images/kho/u4409.png"/>
		         <div class="text" id="u4409_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Tên kho
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4410">
		         <img class="img" id="u4410_img" src="images/kho/u4410.png"/>
		         <div class="text" id="u4410_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Cơ sở
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4411">
		         <img class="img" id="u4411_img" src="images/kho/u4411.png"/>
		         <div class="text" id="u4411_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Danh mục
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4412">
		         <img class="img" id="u4412_img" src="images/kho/u4412.png"/>
		         <div class="text" id="u4412_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Quản lý
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4413">
		         <img class="img" id="u4413_img" src="images/kho/u4413.png"/>
		         <div class="text" id="u4413_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4414">
		         <img class="img" id="u4414_img" src="images/kho/u4414.png"/>
		         <div class="text" id="u4414_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  1
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4415">
		         <img class="img" id="u4415_img" src="images/kho/u4415.png"/>
		         <div class="text" id="u4415_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Kho 1
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4416">
		         <img class="img" id="u4416_img" src="images/kho/u4416.png"/>
		         <div class="text" id="u4416_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Văn Quán
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4417">
		         <img class="img" id="u4417_img" src="images/kho/u4417.png"/>
		         <div class="text" id="u4417_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Dụng cụ học tập
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4418">
		         <img class="img" id="u4418_img" src="images/kho/u4418.png"/>
		         <div class="text" id="u4418_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Nguyễn Văn A
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4419">
		         <img class="img" id="u4419_img" src="images/kho/u4419.png"/>
		         <div class="text" id="u4419_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4420">
		         <img class="img" id="u4420_img" src="images/kho/u4420.png"/>
		         <div class="text" id="u4420_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  2
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4421">
		         <img class="img" id="u4421_img" src="images/kho/u4421.png"/>
		         <div class="text" id="u4421_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Kho 2
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4422">
		         <img class="img" id="u4422_img" src="images/kho/u4422.png"/>
		         <div class="text" id="u4422_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Văn Quán
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4423">
		         <img class="img" id="u4423_img" src="images/kho/u4423.png"/>
		         <div class="text" id="u4423_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Dụng cụ học tập
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4424">
		         <img class="img" id="u4424_img" src="images/kho/u4424.png"/>
		         <div class="text" id="u4424_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Nguyễn Văn B
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4425">
		         <img class="img" id="u4425_img" src="images/kho/u4425.png"/>
		         <div class="text" id="u4425_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4426">
		         <img class="img" id="u4426_img" src="images/kho/u4426.png"/>
		         <div class="text" id="u4426_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  3
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4427">
		         <img class="img" id="u4427_img" src="images/kho/u4427.png"/>
		         <div class="text" id="u4427_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Kho 3
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4428">
		         <img class="img" id="u4428_img" src="images/kho/u4428.png"/>
		         <div class="text" id="u4428_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Hoàng Ngân
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4429">
		         <img class="img" id="u4429_img" src="images/kho/u4429.png"/>
		         <div class="text" id="u4429_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Cơ sở vật chất
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4430">
		         <img class="img" id="u4430_img" src="images/kho/u4430.png"/>
		         <div class="text" id="u4430_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Nguyễn Văn C
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4431">
		         <img class="img" id="u4431_img" src="images/kho/u4431.png"/>
		         <div class="text" id="u4431_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u4432">
		      <img class="img" id="u4432_img" src="images/account/u429.svg"/>
		      <div class="text" id="u4432_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u4433">
		      <img class="img" id="u4433_img" src="images/account/u429.svg"/>
		      <div class="text" id="u4433_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u4434">
		      <img class="img" id="u4434_img" src="images/account/u429.svg"/>
		      <div class="text" id="u4434_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default" data-height="22" data-label="Paging" data-left="178" data-top="403" data-width="931" id="u4435">
		      <div class="ax_default paragraph" id="u4436">
		         <div class="" id="u4436_div">
		         </div>
		         <div class="text" id="u4436_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Hiển thị 1 đến 3 trong tổng số 3 kết quả
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u4437">
		         <div class="" id="u4437_div">
		         </div>
		         <div class="text" id="u4437_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Đi tới trang
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default droplist" id="u4438">
		         <div class="" id="u4438_div">
		         </div>
		         <select class="u4438_input" id="u4438_input">
		            <option class="u4438_input_option" selected="" value="1">
		               1
		            </option>
		            <option class="u4438_input_option" value="2">
		               2
		            </option>
		            <option class="u4438_input_option" value="3">
		               3
		            </option>
		            <option class="u4438_input_option" value="4">
		               4
		            </option>
		            <option class="u4438_input_option" value="5">
		               5
		            </option>
		            <option class="u4438_input_option" value="6">
		               6
		            </option>
		            <option class="u4438_input_option" value="7">
		               7
		            </option>
		            <option class="u4438_input_option" value="8">
		               8
		            </option>
		            <option class="u4438_input_option" value="9">
		               9
		            </option>
		            <option class="u4438_input_option" value="10">
		               10
		            </option>
		            <option class="u4438_input_option" value="11">
		               11
		            </option>
		            <option class="u4438_input_option" value="12">
		               12
		            </option>
		            <option class="u4438_input_option" value="13">
		               13
		            </option>
		            <option class="u4438_input_option" value="14">
		               14
		            </option>
		         </select>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="950" data-label="addAccount" data-left="0" data-top="0" data-width="1920" id="u4439" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default shape" data-label="alpha_Layer" id="u4440">
		         <div class="" id="u4440_div">
		         </div>
		         <div class="text" id="u4440_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-label="Add account" data-left="0" data-top="0" data-width="0" id="u4441">
		         <div class="ax_default shape" id="u4442">
		            <div class="" id="u4442_div">
		            </div>
		            <div class="text" id="u4442_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default box_1" id="u4443">
		            <div class="" id="u4443_div">
		            </div>
		            <div class="text" id="u4443_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u4444">
		            <div class="" id="u4444_div">
		            </div>
		            <div class="text" id="u4444_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Thêm mới kho
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4445">
		            <div class="ax_default paragraph" id="u4446">
		               <div class="" id="u4446_div">
		               </div>
		               <div class="text" id="u4446_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Tên kho
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default text_field" id="u4447">
		               <div class="" id="u4447_div">
		               </div>
		               <input class="u4447_input" id="u4447_input" type="text" value=""/>
		            </div>
		            <div class="ax_default paragraph" id="u4448">
		               <div class="" id="u4448_div">
		               </div>
		               <div class="text" id="u4448_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        *
		                     </span>
		                  </p>
		               </div>
		            </div>
		         </div>
		         <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4449">
		            <div class="ax_default paragraph" id="u4450">
		               <div class="" id="u4450_div">
		               </div>
		               <div class="text" id="u4450_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Cơ sở
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default paragraph" id="u4451">
		               <div class="" id="u4451_div">
		               </div>
		               <div class="text" id="u4451_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        *
		                     </span>
		                  </p>
		               </div>
		            </div>
		         </div>
		         <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4452">
		            <div class="ax_default paragraph" id="u4453">
		               <div class="" id="u4453_div">
		               </div>
		               <div class="text" id="u4453_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u4454">
		            <img class="img" id="u4454_img" src="images/login/u29.svg"/>
		            <div class="text" id="u4454_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default primary_button" id="u4455">
		            <div class="" id="u4455_div">
		            </div>
		            <div class="text" id="u4455_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Cập nhật
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default button" id="u4456">
		            <div class="" id="u4456_div">
		            </div>
		            <div class="text" id="u4456_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Hủy
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4457">
		            <div class="ax_default paragraph" id="u4458">
		               <div class="" id="u4458_div">
		               </div>
		               <div class="text" id="u4458_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Danh mục chức năng
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default paragraph" id="u4459">
		               <div class="" id="u4459_div">
		               </div>
		               <div class="text" id="u4459_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        *
		                     </span>
		                  </p>
		               </div>
		            </div>
		         </div>
		         <div class="ax_default droplist" id="u4460">
		            <div class="" id="u4460_div">
		            </div>
		            <select class="u4460_input" id="u4460_input">
		               <option class="u4460_input_option" selected="" value="Chọn 1 giá trị">
		                  Chọn 1 giá trị
		               </option>
		               <option class="u4460_input_option" value="Cơ sở vật chất">
		                  Cơ sở vật chất
		               </option>
		               <option class="u4460_input_option" value="Dụng cụ học tập">
		                  Dụng cụ học tập
		               </option>
		            </select>
		         </div>
		         <div class="ax_default droplist" id="u4461">
		            <div class="" id="u4461_div">
		            </div>
		            <select class="u4461_input" id="u4461_input">
		               <option class="u4461_input_option" selected="" value="Chọn 1 giá trị">
		                  Chọn 1 giá trị
		               </option>
		               <option class="u4461_input_option" value="Văn Quán">
		                  Văn Quán
		               </option>
		               <option class="u4461_input_option" value="Hoàng Ngân">
		                  Hoàng Ngân
		               </option>
		            </select>
		         </div>
		         <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4462">
		            <div class="ax_default paragraph" id="u4463">
		               <div class="" id="u4463_div">
		               </div>
		               <div class="text" id="u4463_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Quản lý
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default text_field" id="u4464">
		               <div class="" id="u4464_div">
		               </div>
		               <input class="u4464_input" id="u4464_input" type="text" value=""/>
		            </div>
		         </div>
		      </div>
		   </div>
		</div>
		
      </div>
    );
  }
}

export default Page;
