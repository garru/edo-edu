import React, { useState } from "react";
import { connect } from "dva";
import "react-datepicker/dist/react-datepicker.css";
import { Button, Table } from "react-bootstrap";
import CustomModal from "./modal";
import PageHeader from "../../../components/PageHeader";

const Page = () => {
  const [isCreateNew, setIsCreateNew] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [selectedItem, setSelectedItem] = useState({});
  const handleButtonClick = () => {
    setIsCreateNew(true);
    setShowModal(true);
  };
  const handleCloseModal = () => {
    setShowModal(false);
  };
  return (
    <div>
      <PageHeader
        title="Kho"
        breadcrums={[{
          title: "Home",
          path: "/"
        }, {
          title: "Kho",
          path: "/kho"
        }]}
        subTitle="Thống kê, quản lý những kho hiện có hoặc thêm mới kho"
      />
      <div className="iq-card n-pb-0">
        <div className="iq-card-body">
          <div className="">
            <fieldset className="m-b-2">
              <legend>Tìm kiếm</legend>
              <form action="" className="w-100">
                <div className="row align-end">
                  <div className="col-md-3">
                    <div className="form-group">
                      {/* <label htmlFor="search_input"></label> */}
                      <input
                          id="search_input"
                          type="text"
                          name="search_input"
                          className="form-control form-control-lg w-100"
                      />
                    </div>
                  </div>
                  <div className="col-md-3">
                    <div className="form-group">
                      <Button variant="primary" type="submit">
                        Tìm kiếm
                      </Button>
                    </div>
                  </div>
                </div>
              </form>
            </fieldset>
            <div className="row">
              <div className="col-12 m-b-1">
                <div className="form-group">
                  <Button
                      variant="primary"
                      className="float-right"
                      type="submit"
                      onClick={handleButtonClick}
                  >
                    Tạo mới
                  </Button>
                </div>
              </div>
              <div className="col-12 ">
                <div className="w-100">
                  <Table bordered hover className="table">
                    <thead>
                    <tr>
                      <th>STT</th>
                      <th>Tên kho</th>
                      <th>Cơ sở</th>
                      <th>Danh mục</th>
                      <th>Quản lý</th>
                      <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                      <td>1</td>
                      <td>Kho 1</td>
                      <td>Văn Quán</td>
                      <td>Dụng cụ học tập</td>
                      <td>Nguyễn Văn A</td>
                      <td></td>
                    </tr>
                    <tr>
                      <td>2</td>
                      <td>Kho 2</td>
                      <td>Văn Quán</td>
                      <td>Dụng cụ học tập</td>
                      <td>Nguyễn Văn B</td>
                      <td></td>
                    </tr>
                    <tr>
                      <td>3</td>
                      <td>Kho 3</td>
                      <td>Hoàng Ngân</td>
                      <td>Cơ sở vật chất</td>
                      <td>Nguyễn Văn C</td>
                      <td></td>
                    </tr>
                    </tbody>
                  </Table>
                  <div className="not-found">
                    <i className="fas fa-inbox"></i>
                    <span>Không tìm thấy kết quả</span>
                  </div>
                  {showModal && (
                      <CustomModal
                          isCreateNew={isCreateNew}
                          selectedItem={selectedItem}
                          handleCloseModal={handleCloseModal}
                      />
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default connect(({ Mock }) => ({
  Mock
}))(Page);
