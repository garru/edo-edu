import React, { Component, useState, useRef, useEffect } from "react";
import { useForm } from "react-hook-form";

import { Modal, Button, Table } from "react-bootstrap";
import { DATA_CHI_TIET, CHI_TIET_HEADER } from "./data";
import MultiSelect from "react-multi-select-component";

export default function ModalTaoMoi(props) {
  const [show, setShow] = useState(true);
  const [filterValue, setFilterValue] = useState([]);
  const handleClose = () => {
    // setShow(false);
    props.onClose();
  };
  const { register, handleSubmit, errors } = useForm();
  const emailReg = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  const { isCreateNew, selectedItem } = props;
  return (
    <Modal
      size={"lg"}
      show={show}
      onHide={handleClose}
      backdrop="static"
      keyboard={false}
      id="AddNew"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title>
          {isCreateNew ? "Thêm mới buổi học" : "Chỉnh sửa buổi học"}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body style={{ padding: "30px 30px 10px" }}>
        <div className="row">
          <div className="col-12 m-b-2">
            <fieldset>
              <legend>Thông tin lịch học đã có</legend>
              <div className="col-lg-3 col-md-4 col-sm-6">
                <div className="form-group">
                  <p>Mã lớp: xxxxx</p>
                </div>
              </div>
              <div className="col-lg-3 col-md-4 col-sm-6">
                <div className="form-group">
                  <p>Tên lớp: BIBOP 123</p>
                </div>
              </div>
              <div className="col-lg-3 col-md-4 col-sm-6">
                <div className="form-group">
                  <p>Chương trình học: BIBOP PROGRAM</p>
                </div>
              </div>
              <div className="col-lg-3 col-md-4 col-sm-6">
                <div className="form-group">
                  <p>Ngày bắt đầu: 10/01/2021</p>
                </div>
              </div>
              <div className="col-lg-3 col-md-4 col-sm-6">
                <div className="form-group">
                  <p>Số buổi đã sắp xếp: 20</p>
                </div>
              </div>
            </fieldset>
          </div>
          <div className="col-12 m-b-2">
            <fieldset>
              <legend>Thời gian</legend>
              <div className="col-6">
                <div className="form-group">
                  <label htmlFor="kieu_lap">Kiểu lập:</label>
                  <select
                    className="form-control form-control-lg"
                    id="kieu_lap"
                    name="kieu_lap"
                  >
                    {KIEU_LAP.map(item => (
                      <option
                        value={item.code}
                        disabled={item.code !== "" ? false : true}
                        selected={item.code !== "" ? false : true}
                        hidden={item.code !== "" ? false : true}
                      >
                        {item.text}
                      </option>
                    ))}
                  </select>
                </div>
              </div>
              <div className="col-6">
                <div className="form-group">
                  <label htmlFor="ngay_bat_dau">Ngày bắt đầu:</label>
                  <input
                    type="date"
                    className="form-control form-control-lg"
                    id="exampleInputdate"
                    defaultValue={moment().format("YYYY-MM-DD")}
                  />
                </div>
              </div>
              <div className="col-6">
                <div className="form-group">
                  <label htmlFor="dk_ket_thuc">Điều kiện kết thúc:</label>
                  <select
                    className="form-control form-control-lg"
                    id="dk_ket_thuc"
                    name="dk_ket_thuc"
                  >
                    {DK_KET_THUC.map(item => (
                      <option
                        value={item.code}
                        disabled={item.code !== "" ? false : true}
                        selected={item.code !== "" ? false : true}
                        hidden={item.code !== "" ? false : true}
                      >
                        {item.text}
                      </option>
                    ))}
                  </select>
                </div>
              </div>
              <div className="col-6">
                <div className="form-group">
                  <label htmlFor="so_buoi">Số buổi:</label>
                  <input
                    type="text"
                    name="so_buoi"
                    className="form-control form-control-lg"
                    id="so_buoi"
                  ></input>
                </div>
              </div>
              <div className="col-12">
                <ul className="vertical m-t-1">
                  {GIO_HOC.map((item, index) => {
                    return (
                      <li>
                        <input type="checkbox" name={`checkbox-${index}`} />
                        &nbsp; {item.text}
                      </li>
                    );
                  })}
                </ul>
              </div>
              <div className="col-6">
                <div className="form-group">
                  <label htmlFor="gio_bat_dau">Thời gian:</label>
                  <div className="row">
                    <div className="col-5">
                      <div className="form-group">
                        <input
                          type="text"
                          name="gio_bat_dau"
                          className="form-control form-control-lg"
                          id="gio_bat_dau"
                          // ref={register({ required: true, minLength: 8 })}
                        ></input>
                      </div>
                    </div>
                    <div className="col-5">
                      <div className="form-group">
                        <input
                          type="text"
                          name="gio_ket_thuc"
                          className="form-control form-control-lg"
                          id="gio_ket_thuc"
                          // ref={register({ required: true, minLength: 8 })}
                        ></input>
                      </div>
                    </div>
                    <div className="col-2">
                      <div className="btn-icon">
                        <i className="fa fa-plus" />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </fieldset>
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="primary" style={{marginRight: 15}}>
          {isCreateNew ? "Tạo mới" : "Cập nhật"}
        </Button>
        <Button variant={"secondary"} onClick={handleClose}>
          Hủy
        </Button>
      </Modal.Footer>
    </Modal>
  );
}
