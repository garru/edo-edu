import React from "react";
import { connect } from "dva";
import { Button, Table, Overlay, Popover } from "react-bootstrap";
import { DATA, HEADER, STATUS, DIEM_DANH } from "./data";
import * as _ from "lodash";
import CustomModal from "./modal";
import { Link } from "react-router-dom";
import Paging from "../../../../components/Paging";
import IconTable from "../../../../components/IconTable";

class Page extends React.Component {
  constructor(props) {
    super(props);
    const initCheckedItem = DATA.map(item => false);
    this.state = {
      checkedAll: false,
      selectedItem: null,
      checkedItems: _.cloneDeep(initCheckedItem),
      show: _.cloneDeep(initCheckedItem),
      listItem: DATA,
      target: false,
      activeIndex: null,
      filterValue: []
    };
  }

  checkAllCheckboxChecked() {
    const uncheckedCheckboxIndex = this.state.checkedItems.findIndex(
      item => !item
    );
    return uncheckedCheckboxIndex !== -1;
  }

  updateCheckedItem(e, index) {
    let newCheckedItems = this.state.checkedItems;
    const value = e.target.checked;
    newCheckedItems[index] = value;
    this.setState({ checkedItems: newCheckedItems });
    if (value) {
      const isCheckedAll = this.checkAllCheckboxChecked();
      isCheckedAll && this.setState({ checkedAll: true });
    } else {
      this.setState({ checkedAll: false });
    }
  }

  checkedAll(e) {
    const value = e.target.checked;
    let newCheckedItems = [];
    this.setState({ checkedAll: value });
    newCheckedItems = this.state.checkedItems.map(item => value);
    this.setState({ checkedItems: newCheckedItems });
  }

  renderTooltip(index) {
    return (
      <Overlay
        show={this.state.show[index]}
        target={this.state.target}
        placement="bottom"
        containerPadding={20}
        rootClose={true}
        rootCloseEvent={"click"}
        onHide={e => this.handleClick(e, index, true)}
      >
        <Popover id="popover-contained">
          <Popover.Content>
            <ul>
              <li
                onClick={e => {
                  this.handleClick(e, index, true);
                  this.setState({ isCreateNew: false, showModal: true });
                }}
              >
                &nbsp; Thông tin
              </li>

              <li
                onClick={e => {
                  this.handleClick(e, index, true);
                }}
              >
                &nbsp; Nhận xét
              </li>
              <li>
                <Link to="/diem_danh" className="not-link">
                  &nbsp; Điểm danh
                </Link>
              </li>
              <li
                onClick={e => {
                  this.handleClick(e, index, true);
                }}
              >
                &nbsp; Chỉnh sửa
              </li>
            </ul>
          </Popover.Content>
        </Popover>
      </Overlay>
    );
  }

  handleClick(e, index, isHide) {
    let { show, activeIndex, listItem } = this.state;
    show[index] = !show[index];
    index !== activeIndex && (show[activeIndex] = false);
    this.setState({
      show: show,
      activeIndex: index,
      selectedItem: listItem[index],
      target: isHide ? null : e.target
    });
  }

  onClose = () => {
    this.setState({ isCreateNew: false, showModal: false });
  };

  render() {
    const bre = {
      title: "Thêm mới lớp học",
      breadcrums: [
        {
          title: "Home",
          path: "/"
        },
        {
          title: "Thêm mới lớp học",
          path: ""
        }
      ]
    };
    console.log("render account_body>>>");

    const renderStatus = status => {
      switch (status) {
        case "da_hoc":
          return (
            <td
              style={{
                backgroundColor: "#b0b0b0",
                color: "white"
              }}
            >
              Đã học
            </td>
          );
        case "hoan":
          return (
            <td
              style={{
                backgroundColor: "#373a80",
                color: "white"
              }}
            >
              Hoãn
            </td>
          );
        case "bu_da_hoc":
          return (
            <td
              style={{
                backgroundColor: "#ac0000",
                color: "white"
              }}
            >
              Bù đã học
            </td>
          );
        default:
          return (
            <td
              style={{
                backgroundColor: "#006c14",
                color: "white"
              }}
            >
              Chưa học
            </td>
          );
      }
    };

    return (
      <div className="iq-card pb-0">
        <div className=" iq-card-header d-flex justify-content-between">
          <div className="iq-header-title">
            <h6>
              <b>Thông tin chung</b>
            </h6>
          </div>
        </div>
        <div className="iq-card-body">
          <div className="row">
            <div className="col-md-3 col-md-6 col-xs-12">
              <p>
                Tổng sỹ số buổi:<b> 18</b>
              </p>
            </div>
            <div className="col-md-3 col-md-6 col-xs-12">
              <p>
                Số học sinh bù/học thử:<b> 2</b>
              </p>
            </div>
            <div className="col-md-3 col-md-6 col-xs-12">
              <p>
                Số học sinh tại lớp:<b> 16</b>
              </p>
            </div>
            <div className="col-md-3 col-md-6 col-xs-12">
              <p>
                Có măt:<b> xx</b>
              </p>
            </div>
            <div className="col-md-3 col-md-6 col-xs-12">
              <p>
                Nghỉ có phép:<b> xx</b>
              </p>
            </div>
            <div className="col-md-3 col-md-6 col-xs-12">
              <p>
                Nghỉ không phép:<b> xx</b>
              </p>
            </div>
          </div>
          <div className="row align-end">
            <div className="col-12" style={{ textAlign: "right" }}>
              <Link to="/tao_moi_lich_hoc">
                <Button
                  className="m-b-2"
                  variant="primary"
                  style={{ marginRight: 15 }}
                >
                  Thêm mới
                </Button>
              </Link>

              <Link to="/chinh_sua_lich_hoc/">
                <Button
                  className="m-b-2"
                  variant="primary"
                  style={{ marginRight: 15 }}
                >
                  Chỉnh sửa
                </Button>
              </Link>
              <Link to="/lop_dang_lich">
                <Button className="m-b-2" variant="primary">
                  Lớp dạng lịch
                </Button>
              </Link>
            </div>
          </div>
          <div className="row">
            <div className="col-12">
              <div style={{ width: "100%" }}>
                <div style={{ overflowX: "auto", width: "100%" }}>
                  <Table
                    bordered
                    hover
                    // style={{ minWidth: 1500 }}
                    className="table"
                  >
                    <thead>
                      <tr>
                        <th>
                          <input
                            type="checkbox"
                            name="check-all"
                            checked={this.state.checkedAll || false}
                            onClick={e => this.checkedAll(e)}
                          />
                        </th>
                        {HEADER.map(item => (
                          <th
                            className={`${item.class} ${
                              item.hasSort ? "sort" : ""
                            } ${
                              this.state.sortField === item.key ? "active" : ""
                            }`}
                            key={item.key}
                          >
                            <span>{item.title}</span>
                          </th>
                        ))}
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                      {this.state.listItem.map((item, index) => (
                        <React.Fragment>
                          <tr key={`account_${item.id}`}>
                            <td>
                              <input
                                type="checkbox"
                                name={`account_${item.id}`}
                                checked={
                                  this.state.checkedItems[index] || false
                                }
                                onClick={e => this.updateCheckedItem(e, index)}
                              />
                            </td>
                            <td>{item.id}</td>
                            <td>{item.ten_bai_hoc}</td>
                            {renderStatus(item.trang_thai)}
                            <td>
                              <IconTable
                                classes={"fa fa-book"}
                                styles={{
                                  marginRight: 15,
                                  marginLeft: "auto"
                                }}
                                text={"Book"}
                                onClick={() => console.log(1)}
                              ></IconTable>
                              <IconTable
                                classes={"fa fa-chalkboard-teacher"}
                                text={"Teachers"}
                                onClick={() => console.log(1)}
                              ></IconTable>
                            </td>
                            <td>{item.ngay_hoc}</td>
                            <td>{item.gio_hoc}</td>
                            <td>{item.phong_hoc}</td>
                            <td>{item.gv_nguoi_viet}</td>
                            <td>{item.gv_nuoc_ngoai}</td>
                            <td>
                              <i
                                className="fa fa-ellipsis-h"
                                style={{
                                  marginRight: 0,
                                  marginLeft: "auto"
                                }}
                                onClick={e => {
                                  e.preventDefault();
                                  this.handleClick(e, index);
                                }}
                              />
                              {this.renderTooltip(index)}
                            </td>
                          </tr>
                        </React.Fragment>
                      ))}
                    </tbody>
                  </Table>
                  {this.state.listItem && this.state.listItem.length ? (
                    ""
                  ) : (
                    <div className="not-found">
                      <i class="fas fa-inbox"></i>
                      <span>Không tìm thấy kết quả</span>
                    </div>
                  )}
                </div>
              </div>
              <Paging
                pageNumber={10}
                total={550}
                pageSize={10}
                onChange={console.log}
              ></Paging>
            </div>
          </div>
          {this.state.showModal && (
            <CustomModal
              isCreateNew={this.state.isCreateNew}
              onClose={this.onClose}
              selectedItem={this.state.selectedItem}
            ></CustomModal>
          )}
        </div>
      </div>
    );
  }
}

export default connect(({ HealthcareService }) => ({
  HealthcareService
}))(Page);
