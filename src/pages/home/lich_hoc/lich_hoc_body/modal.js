import React, { Component, useState, useRef, useEffect } from "react";
import { useForm } from "react-hook-form";

import { Modal, Button, Table } from "react-bootstrap";
import { NGUON, DIEM_DANH_MUL, HEADER_MODAL, DATA_MODAL } from "./data";
import MultiSelect from "react-multi-select-component";
import ModalChiTiet from "./chi_tiet";

export default function CustomModal(props) {
  const [show, setShow] = useState(true);
  const [showModal, setShowModal] = useState(false);
  const [filterValue, setFilterValue] = useState([]);
  const handleClose = () => {
    // setShow(false);
    props.onClose();
  };
  const { register, handleSubmit, errors } = useForm();
  const emailReg = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  const { isCreateNew, selectedItem } = props;
  return (
    <Modal
      size={"lg"}
      show={show}
      onHide={handleClose}
      backdrop="static"
      keyboard={false}
      id="AddNew"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title>Thông tin buổi học</Modal.Title>
      </Modal.Header>
      <Modal.Body style={{ padding: "30px 30px 10px" }}>
        <div className="row">
          <div className="col-12" style={{ paddingBottom: 20 }}>
            <div className=" iq-card-header d-flex justify-content-between">
              <div className="iq-header-title">
                <h6>
                  <b>Thông tin chung</b>
                </h6>
              </div>
            </div>
          </div>
          <div className="col-3">
            <div className="form-group">
              <p>Tổng sỹ số buổi: 18</p>
            </div>
          </div>
          <div className="col-3">
            <div className="form-group">
              <p>Số học sinh bù/học thử: 2</p>
            </div>
          </div>
          <div className="col-3">
            <div className="form-group">
              <p>Số học sinh tại lớp: 16</p>
            </div>
          </div>
          <div className="col-3">
            <div className="form-group">
              <p>Có măt: xx</p>
            </div>
          </div>
          <div className="col-3">
            <div className="form-group">
              <p>Nghỉ có phép: xx</p>
            </div>
          </div>
          <div className="col-3">
            <div className="form-group">
              <p>Nghỉ không phép: xx</p>
            </div>
          </div>
        </div>
        <div className="row align-end">
          <div className="col-4">
            <div className="form-group">
              <label htmlFor="ma_hoc_vien">Tên/Mã học viên:</label>
              <input
                type="text"
                name="ma_hoc_vien"
                className="form-control form-control-lg"
                id="ma_hoc_vien"
                // ref={register({ required: true, minLength: 8 })}
              ></input>
            </div>
          </div>
          <div className="col-4">
            <div className="form-group">
              <label htmlFor="nguon">Nguồn</label>
              <select
                name="co_so"
                className="form-control form-control-lg"
                id="co_so"
              >
                {NGUON.map(item => (
                  <option value={item.value}>{item.name}</option>
                ))}
              </select>
            </div>
          </div>

          <div className="col-4">
            <div className="form-group">
              <label htmlFor="ten">Điểm danh:</label>
              <MultiSelect
                options={DIEM_DANH_MUL}
                value={filterValue}
                onChange={setFilterValue}
                labelledBy="Select"
                overrideStrings={{
                  allItemsAreSelected: "Tất cả",
                  clearSearch: "Clear Search",
                  noOptions: "Không tìm thấy",
                  search: "Search",
                  selectAll: "Select All",
                  selectSomeItems: "Có thể chọn nhiều giá trị"
                }}
                ArrowRenderer={() => <i className="fa fa-chevron-down" />}
                // ItemRenderer={customItemRenderer}
              />
            </div>
          </div>
        </div>
        <div className="row align-end">
          <div className="col-12">
            <div style={{ width: "100%" }}>
              <div style={{ overflowX: "auto", width: "100%" }}>
                <Table
                  bordered
                  hover
                  // style={{ minWidth: 1500 }}
                  className="table"
                >
                  <thead>
                    <tr>
                      {HEADER_MODAL.map(item => (
                        <th
                          className={`${item.class} ${
                            item.hasSort ? "sort" : ""
                          }`}
                          key={item.key}
                        >
                          <span>{item.title}</span>
                        </th>
                      ))}
                    </tr>
                  </thead>
                  <tbody>
                    {DATA_MODAL.map((item, index) => (
                      <React.Fragment>
                        <tr key={`account_${item.id}`}>
                          <td>{item.id}</td>
                          <td>{item.ma_hoc_vien}</td>
                          <td>{item.ten}</td>
                          <td>{item.ho_dem}</td>
                          <td>{item.nguon}</td>
                          <td>{item.diem_danh}</td>
                          <td>
                            <a
                              href="javascript:void(0)"
                              onClick={() => setShowModal(true)}
                            >
                              Chi tiết
                            </a>
                          </td>
                        </tr>
                      </React.Fragment>
                    ))}
                  </tbody>
                </Table>
                <div className="not-found">
                  <i class="fas fa-inbox"></i>
                  <span>Không tìm thấy kết quả</span>
                </div>
              </div>
            </div>
          </div>
        </div>
        {showModal && (
          <ModalChiTiet onClose={() => setShowModal(false)}></ModalChiTiet>
        )}
      </Modal.Body>
      <Modal.Footer>
        <Button variant="primary" style={{marginRight: 15}}>
          {isCreateNew ? "Tạo mới" : "Cập nhật"}
        </Button>
        <Button variant={"secondary"} onClick={handleClose}>
          Hủy
        </Button>
      </Modal.Footer>
    </Modal>
  );
}
