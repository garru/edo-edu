export const HEADER = [
  {
    title: "Bài học số",
    key: "id",
    class: "tb-width-100"
  },
  {
    title: "Tên bài học",
    key: "ten_bai_hoc",
    class: "tb-width-200"
  },
  {
    title: "Trạng thái",
    key: "trang_thai",
    class: "tb-width-150"
  },
  {
    title: "Dữ liệu",
    key: "du_lieu",
    class: "tb-width-100"
  },
  {
    title: "Ngày học",
    key: "ngay_hoc",
    class: "tb-width-150"
  },
  {
    title: "Giờ học",
    key: "gio_hoc",
    class: "tb-width-150"
  },
  {
    title: "Phòng học",
    key: "phong_hoc",
    class: "tb-width-200"
  },
  {
    title: "GV Người Việt",
    key: "gv_nguoi_viet",
    class: "tb-width-200"
  },
  {
    title: "GV Nước Ngoài",
    key: "gv_nuoc_ngoai",
    class: "tb-width-200"
  }
];

export const DIEM_DANH = [
  {
    code: "Chưa điểm danh",
    text: "Chưa điểm danh"
  },
  {
    code: "Có đi học",
    text: "Có đi học"
  },
  {
    code: "Nghỉ không phép",
    text: "Nghỉ không phép"
  },
  {
    code: "Nghỉ có phép",
    text: "Nghỉ có phép"
  }
];

export const STATUS = [
  {
    label: "Có mặt",
    value: "present"
  },
  {
    label: "Nghỉ không phép",
    value: "not_auth_absent"
  },
  {
    label: "Nghỉ có phép",
    value: "auth_absent"
  }
];

export const DATA = [
  {
    id: "1",
    ten_bai_hoc: "Introduction-Greetings",
    trang_thai: "da_hoc",
    du_lieu: "",
    ngay_hoc: "11/01/2021",
    gio_hoc: "17:30",
    phong_hoc: "Mickey",
    gv_nguoi_viet: "Nguyễn Hoàng",
    gv_nuoc_ngoai: "Diablo"
  },
  {
    id: "1",
    ten_bai_hoc: "Introduction-Greetings",
    trang_thai: "hoan",
    du_lieu: "",
    ngay_hoc: "11/01/2021",
    gio_hoc: "17:30",
    phong_hoc: "Mickey",
    gv_nguoi_viet: "Nguyễn Hoàng",
    gv_nuoc_ngoai: "Diablo"
  },
  {
    id: "1",
    ten_bai_hoc: "Introduction-Greetings",
    trang_thai: "bu_da_hoc",
    du_lieu: "",
    ngay_hoc: "11/01/2021",
    gio_hoc: "17:30",
    phong_hoc: "Mickey",
    gv_nguoi_viet: "Nguyễn Hoàng",
    gv_nuoc_ngoai: "Diablo"
  },
  {
    id: "1",
    ten_bai_hoc: "Introduction-Greetings",
    trang_thai: "chua_hoc",
    du_lieu: "",
    ngay_hoc: "11/01/2021",
    gio_hoc: "17:30",
    phong_hoc: "Mickey",
    gv_nguoi_viet: "Nguyễn Hoàng",
    gv_nuoc_ngoai: "Diablo"
  },
  {
    id: "1",
    ten_bai_hoc: "Introduction-Greetings",
    trang_thai: "da_hoc",
    du_lieu: "",
    ngay_hoc: "11/01/2021",
    gio_hoc: "17:30",
    phong_hoc: "Mickey",
    gv_nguoi_viet: "Nguyễn Hoàng",
    gv_nuoc_ngoai: "Diablo"
  },
  {
    id: "1",
    ten_bai_hoc: "Introduction-Greetings",
    trang_thai: "da_hoc",
    du_lieu: "",
    ngay_hoc: "11/01/2021",
    gio_hoc: "17:30",
    phong_hoc: "Mickey",
    gv_nguoi_viet: "Nguyễn Hoàng",
    gv_nuoc_ngoai: "Diablo"
  }
];

export const HEADER_MODAL = [
  {
    title: "STT",
    key: "id",
    class: "tb-width-50"
  },
  {
    title: "Mã Học viên",
    key: "ma_hoc_vien",
    class: "tb-width-200"
  },
  {
    title: "Tên",
    key: "ten",
    class: "tb-width-200"
  },
  {
    title: "Họ Đêm",
    key: "ho_dem",
    class: "tb-width-200"
  },
  {
    title: "Nguồn",
    key: "nguon",
    class: "tb-width-200"
  },
  {
    title: "Điểm danh",
    key: "diem_danh",
    class: "tb-width-200"
  },
  {
    title: "Đánh giá",
    key: "danh_gia",
    class: "tb-width-200"
  }
];

export const TIEU_CHI = [
  {
    id: 1,
    tieu_chi: "Kiến thức",
    rating: 3.5
  },
  {
    id: 1,
    tieu_chi: "Ngữ pháp",
    rating: 3.5
  },
  {
    id: 1,
    tieu_chi: "Từ vựng",
    rating: 3.5
  },
  {
    id: 1,
    tieu_chi: "Kỹ năng",
    rating: 3.5
  }
];

export const DIEM_DANH_MUL = [
  {
    label: "Chưa điểm danh",
    value: "Chưa điểm danh"
  },
  {
    label: "Có đi học",
    value: "Có đi học"
  },
  {
    label: "Nghỉ không phép",
    value: "Nghỉ không phép"
  },
  {
    label: "Nghỉ có phép",
    value: "Nghỉ có phép"
  }
];

export const NGUON = [
  {
    code: "Tại lớp",
    text: ""
  },
  {
    code: "Bù",
    text: "Có đi học"
  }
];

export const DATA_MODAL = [
  {
    id: "1",
    ma_hoc_vien: "HV-00001",
    ten: "Dung",
    ho_dem: "Hoang",
    nguon: "Tại lớp",
    diem_danh: "Có học",
    danh_gia: ""
  },
  {
    id: "1",
    ma_hoc_vien: "HV-00001",
    ten: "Dung",
    ho_dem: "Hoang",
    nguon: "Tại lớp",
    diem_danh: "Có học",
    danh_gia: ""
  },
  {
    id: "1",
    ma_hoc_vien: "HV-00001",
    ten: "Dung",
    ho_dem: "Hoang",
    nguon: "Tại lớp",
    diem_danh: "Có học",
    danh_gia: ""
  },
  {
    id: "1",
    ma_hoc_vien: "HV-00001",
    ten: "Dung",
    ho_dem: "Hoang",
    nguon: "Tại lớp",
    diem_danh: "Có học",
    danh_gia: ""
  },
  {
    id: "1",
    ma_hoc_vien: "HV-00001",
    ten: "Dung",
    ho_dem: "Hoang",
    nguon: "Tại lớp",
    diem_danh: "Có học",
    danh_gia: ""
  },
];


export const  CHI_TIET_HEADER = [
  {
    title: "Kỹ năng - Tiêu chí",
    key: "ky_nang",
    class: "tb-width-200"
  },
  {
    title: "Đánh giá",
    key: "danh_gia",
    class: "tb-width-100"
  },
  {
    title: "Giải trình đã gửi phụ huynh",
    key: "giai_trinh",
    class: "tb-width-400"
  }
];


export const DATA_CHI_TIET = [
  {
    tieu_chi: "Kiến thức",
    isParent: true
  },
  {
    tieu_chi: "Ngữ pháp",
    isParent: false,
    danh_gia: "3/5",
    giai_trinh: "Lorem Ipsum is simply dummy text of the printing and"
  },
  {
    tieu_chi: "Từ vựng",
    isParent: false,
    danh_gia: "3/5",
    giai_trinh: "Lorem Ipsum is simply dummy text of the printing and"
  },
  {
    tieu_chi: "Kỹ năng",
    isParent: true
  },
  {
    tieu_chi: "Nghe",
    isParent: false,
    danh_gia: "3/5",
    giai_trinh: "Lorem Ipsum is simply dummy text of the printing and"
  },
  {
    tieu_chi: "Nói",
    isParent: false,
    danh_gia: "3/5",
    giai_trinh: "Lorem Ipsum is simply dummy text of the printing and"
  },
  {
    tieu_chi: "Đọc",
    isParent: false,
    danh_gia: "3/5",
    giai_trinh: "Lorem Ipsum is simply dummy text of the printing and"
  },
  {
    tieu_chi: "Viết",
    isParent: false,
    danh_gia: "3/5",
    giai_trinh: "Lorem Ipsum is simply dummy text of the printing and"
  },
  {
    tieu_chi: "Phản xạ",
    isParent: false,
    danh_gia: "3/5",
    giai_trinh: "Lorem Ipsum is simply dummy text of the printing and"
  },
]







