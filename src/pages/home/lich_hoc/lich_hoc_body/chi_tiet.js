import React, { Component, useState, useRef, useEffect } from "react";
import { useForm } from "react-hook-form";

import { Modal, Button, Table } from "react-bootstrap";
import { DATA_CHI_TIET, CHI_TIET_HEADER } from "./data";
import MultiSelect from "react-multi-select-component";

export default function ModalChiTiet(props) {
  const [show, setShow] = useState(true);
  const [filterValue, setFilterValue] = useState([]);
  const handleClose = () => {
    // setShow(false);
    props.onClose();
  };
  const { register, handleSubmit, errors } = useForm();
  const emailReg = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  const { isCreateNew, selectedItem } = props;
  return (
    <Modal
      size={"lg"}
      show={show}
      onHide={handleClose}
      backdrop="static"
      keyboard={false}
      id="AddNew"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title>Đánh giá học viên</Modal.Title>
      </Modal.Header>
      <Modal.Body style={{ padding: "30px 30px 10px" }}>
        <div className="row">
          <div className="col-12" style={{ paddingBottom: 20 }}>
            <div className=" iq-card-header d-flex justify-content-between">
              <div className="iq-header-title">
                <h6>
                  <b>Thông tin chung</b>
                </h6>
              </div>
            </div>
          </div>
          <div className="col-3">
            <div className="form-group">
              <p>Mã Học viên: HV-00001</p>
            </div>
          </div>
          <div className="col-3">
            <div className="form-group">
              <p>Họ và tên: Hoàng Dung</p>
            </div>
          </div>
          <div className="col-3">
            <div className="form-group">
              <p>Ngày học: 11/01/2021</p>
            </div>
          </div>
          <div className="col-3">
            <div className="form-group">
              <p>Thời gian: 17:30</p>
            </div>
          </div>
          <div className="col-3">
            <div className="form-group">
              <p>GV Việt Nam: Nguyễn Hoàng</p>
            </div>
          </div>
          <div className="col-3">
            <div className="form-group">
              <p>GV Nước ngoài: Diablo</p>
            </div>
          </div>
        </div>
        <div className="row align-end">
          <div className="col-12">
            <div style={{ width: "100%" }}>
              <div style={{ overflowX: "auto", width: "100%" }}>
                <Table
                  bordered
                  hover
                  // style={{ minWidth: 1500 }}
                  className="table"
                >
                  <thead>
                    <tr>
                      {CHI_TIET_HEADER.map(item => (
                        <th className={`${item.class} ${item.hasSort ? "sort" : ""} ${this.state.sortField === item.key ? "active" : ""}`} key={item.key}>
                        <span>{item.title}</span>
                        </th>
                      ))}
                    </tr>
                  </thead>
                  <tbody>
                    {DATA_CHI_TIET.map((item, index) => (
                      <React.Fragment>
                        <tr key={`account_${item.id}`}>
                          <td
                            style={{
                              paddingLeft: item.isParent ? 15 : 45,
                              textAlign: "left"
                            }}
                          >
                            {item.tieu_chi}
                          </td>
                          <td>{item.danh_gia}</td>
                          <td>{item.giai_trinh}</td>
                        </tr>
                      </React.Fragment>
                    ))}
                  </tbody>
                </Table>
<div className="not-found">
                <i class="fas fa-inbox"></i>
                <span>Không tìm thấy kết quả</span>
              </div>
              </div>
            </div>
          </div>
          <div className="col-12 ">
            <div className="form-group">
              <label htmlFor="mo_ta">Mô tả</label>
              <textarea
                className="textarea form-control"
                rows="4"
                name="mo_ta"
              ></textarea>
            </div>
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="primary" style={{marginRight: 15}}>
          {isCreateNew ? "Tạo mới" : "Cập nhật"}
        </Button>
        <Button variant={"secondary"} onClick={handleClose}>
          Hủy
        </Button>
      </Modal.Footer>
    </Modal>
  );
}
