import React from "react";
import { connect } from "dva";
import { Button } from "react-bootstrap";
class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillMount() {}

  render() {
    const bre = {
      title: "Thêm mới lớp học",
      breadcrums: [
        {
          title: "Home",
          path: "/"
        },
        {
          title: "Thêm mới lớp học",
          path: ""
        }
      ]
    };
    console.log("render account_header>>>");
    return (
      <div className="iq-card">
        <div className=" iq-card-header d-flex justify-content-between">
          <div className="iq-header-title">
            <h6>
              <b>Thông tin buổi học</b>
            </h6>
          </div>
        </div>
        <div className="iq-card-body">
          <div className="row">
            <div className="col-lg-3 col-sm-6 col-xs-12">
              <p>
                Cơ sở: <b>Hoàng Ngân</b>
              </p>
            </div>
            <div className="col-lg-3 col-sm-6 col-xs-12">
              <p>
                Tên lớp: <b>BIBOP 123</b>
              </p>
            </div>
            <div className="col-lg-3 col-sm-6 col-xs-12">
              <p>
                Ngày bắt đầu: <b>20/02/2021</b>
              </p>
            </div>
            <div className="col-lg-3 col-sm-6 col-xs-12">
              <p>
                Số buổi còn lại: <b>15</b>
              </p>
            </div>
            <div className="col-lg-3 col-sm-6 col-xs-12">
              <p>
                Sỹ số hiện tại: <b>18</b>
              </p>
            </div>
            <div className="col-lg-3 col-sm-6 col-xs-12">
              <p>
                Mã lớp: <b>XXXXXX</b>
              </p>
            </div>
            <div className="col-lg-3 col-sm-6 col-xs-12">
              <p>
                Chương trình: <b>BIBOP Cơ bản</b>
              </p>
            </div>
            <div className="col-lg-3 col-sm-6 col-xs-12">
              <p>
                Số buổi học: <b>20</b>
              </p>
            </div>
            <div className="col-lg-3 col-sm-6 col-xs-12">
              <p>
                Sỹ số ban đầu: <b>20</b>
              </p>
            </div>
            <div className="col-lg-3 col-sm-6 col-xs-12">
              <p>
                Phụ trách: <b>Nguyễn Thị Hương</b>
              </p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(({ HealthcareService }) => ({
  HealthcareService
}))(Page);
