import React from "react";
import { connect } from "dva";
import LichHocSearch from "./lich_hoc_search";
import LichHocBody from "./lich_hoc_body";
import PageHeader from "../../../components/PageHeader";
class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillMount() {}

  render() {
    const bre = {
      title: "Điều chỉnh buổi học",
      breadcrums: [
        {
          title: "Home",
          path: "/"
        },
        {
          title: "Điều chỉnh buổi học",
          path: ""
        }
      ]
    };
    console.log("render danh_sach_lop>>>");
    return (
      <React.Fragment>
        <PageHeader {...bre}></PageHeader>
        <LichHocSearch />
        <LichHocBody />
      </React.Fragment>
    );
  }
}

export default connect(({ HealthcareService }) => ({
  HealthcareService
}))(Page);
