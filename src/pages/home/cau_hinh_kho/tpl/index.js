import React from 'react';
import './index.css';
import './ax.css';

class Page extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
		<div class="" id="base" style={{"margin-left":"-150px"}}>
		   <div class="ax_default shape" id="u4166">
		      <div class="" id="u4166_div">
		      </div>
		      <div class="text" id="u4166_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default shape" id="u4167">
		      <div class="" id="u4167_div">
		      </div>
		      <div class="text" id="u4167_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default" data-height="40" data-label="account" data-left="13" data-top="895" data-width="192" id="u4169">
		      <div class="ax_default shape" data-label="accountLable" id="u4170">
		         <img class="img" id="u4170_img" src="images/account_information/accountlable_u55.svg"/>
		         <div class="text" id="u4170_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Nguyen Van A
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u4171">
		         <img class="img" id="u4171_img" src="images/login/u4.svg"/>
		         <div class="text" id="u4171_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="settingMenu" data-left="11" data-top="826" data-width="125" id="u4172">
		      <div class="ax_default shape" data-label="gearIconLable" id="u4173">
		         <img class="img" id="u4173_img" src="images/account_information/geariconlable_u58.svg"/>
		         <div class="text" id="u4173_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Cài đặt
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="44" data-left="11" data-top="826" data-width="44" id="u4174">
		         <div class="ax_default shape" data-label="gearIconBG" id="u4175">
		            <div class="" id="u4175_div">
		            </div>
		            <div class="text" id="u4175_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default shape" data-label="gearIconBG2" id="u4176">
		            <div class="" id="u4176_div">
		            </div>
		            <div class="text" id="u4176_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" data-label="gearIcon" id="u4177">
		            <img class="img" id="u4177_img" src="images/account_information/gearicon_u62.svg"/>
		            <div class="text" id="u4177_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="customerMenu" data-left="11" data-top="145" data-width="143" id="u4178">
		      <div class="ax_default shape" data-label="customerIconLable" id="u4179">
		         <img class="img" id="u4179_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u4179_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Khách hàng
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="customerIconBG" id="u4180">
		         <div class="" id="u4180_div">
		         </div>
		         <div class="text" id="u4180_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="customerIconBG2" id="u4181">
		         <div class="" id="u4181_div">
		         </div>
		         <div class="text" id="u4181_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" data-label="customer" id="u4182">
		         <img class="img" id="u4182_img" src="images/account_information/customer_u67.svg"/>
		         <div class="text" id="u4182_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="classMenu" data-left="11" data-top="198" data-width="143" id="u4183">
		      <div class="ax_default shape" data-label="classIconLable" id="u4184">
		         <img class="img" id="u4184_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u4184_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Lớp học
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG" id="u4185">
		         <div class="" id="u4185_div">
		         </div>
		         <div class="text" id="u4185_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG2" id="u4186">
		         <div class="" id="u4186_div">
		         </div>
		         <div class="text" id="u4186_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" data-label="classicon" id="u4187">
		         <img class="img" id="u4187_img" src="images/account_information/classicon_u72.svg"/>
		         <div class="text" id="u4187_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="dashboardMenu" data-left="11" data-top="91" data-width="143" id="u4188">
		      <div class="ax_default shape" data-label="dashboardIconLable" id="u4189">
		         <img class="img" id="u4189_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u4189_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Dashnoard
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="dashboardIconBG" id="u4190">
		         <div class="" id="u4190_div">
		         </div>
		         <div class="text" id="u4190_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="dashboardIconBG2" id="u4191">
		         <div class="" id="u4191_div">
		         </div>
		         <div class="text" id="u4191_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" data-label="dashboard" id="u4192">
		         <img class="img" id="u4192_img" src="images/account_information/dashboard_u77.svg"/>
		         <div class="text" id="u4192_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="129" data-label="AccountMenu" data-left="58" data-top="806" data-width="246" id="u4193" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default shape" id="u4194">
		         <div class="" id="u4194_div">
		         </div>
		         <div class="text" id="u4194_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u4195">
		         <div class="" id="u4195_div">
		         </div>
		         <div class="text" id="u4195_text">
		            <p>
		               <span style={{"font-family":"ArialMT, Arial","font-weight":"400","text-decoration":"none"}}>
		                  Xin chào:
		               </span>
		               <span style={{"font-family":"Arial Bold, Arial","font-weight":"700","text-decoration":"none"}}>
		                  Nguyen Van A
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4196">
		         <div class="ax_default image" id="u4197">
		            <img class="img" id="u4197_img" src="images/account_information/u82.png"/>
		            <div class="text" id="u4197_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u4198">
		            <div class="" id="u4198_div">
		            </div>
		            <div class="text" id="u4198_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Đăng xuất
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default line" id="u4199">
		         <img class="img" id="u4199_img" src="images/account_information/u84.svg"/>
		         <div class="text" id="u4199_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4200">
		         <div class="ax_default paragraph" id="u4201">
		            <div class="" id="u4201_div">
		            </div>
		            <div class="text" id="u4201_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tài khoản
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default image" id="u4202">
		            <img class="img" id="u4202_img" src="images/account_information/u87.png"/>
		            <div class="text" id="u4202_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="339" data-label="SettingPannel" data-left="58" data-top="525" data-width="339" id="u4203" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default shape" id="u4204">
		         <div class="" id="u4204_div">
		         </div>
		         <div class="text" id="u4204_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u4205">
		         <div class="" id="u4205_div">
		         </div>
		         <div class="text" id="u4205_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Hệ thống
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4206">
		         <div class="ax_default icon" id="u4207">
		            <img class="img" id="u4207_img" src="images/account_information/u92.svg"/>
		            <div class="text" id="u4207_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u4208">
		            <div class="" id="u4208_div">
		            </div>
		            <div class="text" id="u4208_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tài khoản
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4209">
		         <div class="ax_default paragraph" id="u4210">
		            <div class="" id="u4210_div">
		            </div>
		            <div class="text" id="u4210_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phân quyền
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u4211">
		            <img class="img" id="u4211_img" src="images/account_information/u96.svg"/>
		            <div class="text" id="u4211_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u4212">
		         <div class="" id="u4212_div">
		         </div>
		         <div class="text" id="u4212_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Đào tạo
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4213">
		         <div class="ax_default paragraph" id="u4214">
		            <div class="" id="u4214_div">
		            </div>
		            <div class="text" id="u4214_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Thương hiệu
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u4215">
		            <img class="img" id="u4215_img" src="images/account_information/u100.svg"/>
		            <div class="text" id="u4215_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4216">
		         <div class="ax_default paragraph" id="u4217">
		            <div class="" id="u4217_div">
		            </div>
		            <div class="text" id="u4217_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trung tâm
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u4218">
		            <img class="img" id="u4218_img" src="images/account_information/u103.svg"/>
		            <div class="text" id="u4218_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4219">
		         <div class="ax_default icon" id="u4220">
		            <img class="img" id="u4220_img" src="images/account_information/u105.svg"/>
		            <div class="text" id="u4220_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u4221">
		            <div class="" id="u4221_div">
		            </div>
		            <div class="text" id="u4221_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Ngày nghỉ
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4222">
		         <div class="ax_default icon" id="u4223">
		            <img class="img" id="u4223_img" src="images/account_information/u108.svg"/>
		            <div class="text" id="u4223_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u4224">
		            <div class="" id="u4224_div">
		            </div>
		            <div class="text" id="u4224_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phòng ban - Vai trò
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4225">
		         <div class="ax_default paragraph" id="u4226">
		            <div class="" id="u4226_div">
		            </div>
		            <div class="text" id="u4226_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trạng thái học viên
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u4227">
		            <img class="img" id="u4227_img" src="images/account_information/u112.svg"/>
		            <div class="text" id="u4227_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4228">
		         <div class="ax_default paragraph" id="u4229">
		            <div class="" id="u4229_div">
		            </div>
		            <div class="text" id="u4229_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tiêu chí đánh giá
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u4230">
		            <img class="img" id="u4230_img" src="images/account_information/u115.svg"/>
		            <div class="text" id="u4230_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4231">
		         <div class="ax_default paragraph" id="u4232">
		            <div class="" id="u4232_div">
		            </div>
		            <div class="text" id="u4232_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Chương trình học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4233">
		            <div class="ax_default icon" id="u4234">
		               <img class="img" id="u4234_img" src="images/account_information/u119.svg"/>
		               <div class="text" id="u4234_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4235">
		         <div class="ax_default icon" id="u4236">
		            <img class="img" id="u4236_img" src="images/account_information/u121.svg"/>
		            <div class="text" id="u4236_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u4237">
		            <div class="" id="u4237_div">
		            </div>
		            <div class="text" id="u4237_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Bài học
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4238">
		         <div class="ax_default paragraph" id="u4239">
		            <div class="" id="u4239_div">
		            </div>
		            <div class="text" id="u4239_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phòng học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u4240">
		            <img class="img" id="u4240_img" src="images/account_information/u125.svg"/>
		            <div class="text" id="u4240_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4241">
		         <div class="ax_default paragraph" id="u4242">
		            <div class="" id="u4242_div">
		            </div>
		            <div class="text" id="u4242_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trường học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u4243">
		            <img class="img" id="u4243_img" src="images/account_information/u128.svg"/>
		            <div class="text" id="u4243_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default line" id="u4244">
		         <img class="img" id="u4244_img" src="images/account_information/u129.svg"/>
		         <div class="text" id="u4244_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u4245">
		         <div class="" id="u4245_div">
		         </div>
		         <div class="text" id="u4245_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Tài chính
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4246">
		         <div class="ax_default paragraph" id="u4247">
		            <div class="" id="u4247_div">
		            </div>
		            <div class="text" id="u4247_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Học phí
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u4248">
		            <img class="img" id="u4248_img" src="images/account_information/u133.svg"/>
		            <div class="text" id="u4248_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4249">
		         <div class="ax_default paragraph" id="u4250">
		            <div class="" id="u4250_div">
		            </div>
		            <div class="text" id="u4250_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Khuyến mại
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u4251">
		            <img class="img" id="u4251_img" src="images/account_information/u136.svg"/>
		            <div class="text" id="u4251_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="classMenu" data-left="11" data-top="252" data-width="143" id="u4252">
		      <div class="ax_default shape" data-label="classIconLable" id="u4253">
		         <img class="img" id="u4253_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u4253_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Giáo viên
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG" id="u4254">
		         <div class="" id="u4254_div">
		         </div>
		         <div class="text" id="u4254_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG2" id="u4255">
		         <div class="" id="u4255_div">
		         </div>
		         <div class="text" id="u4255_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u4256">
		         <img class="img" id="u4256_img" src="images/account_information/u141.svg"/>
		         <div class="text" id="u4256_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u4257">
		      <img class="img" id="u4257_img" src="images/account_information/u142.svg"/>
		      <div class="text" id="u4257_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u4258">
		      <img class="img" id="u4258_img" src="images/account_information/u143.svg"/>
		      <div class="text" id="u4258_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div id="u4165" style={{"display":"none","visibility":"hidden"}}>
		   </div>
		   <div class="ax_default" data-height="79" data-left="343" data-top="104" data-width="124" id="u4259">
		      <div class="ax_default icon" id="u4260">
		         <img class="img" id="u4260_img" src="images/c_u_h_nh_kho/u4260.svg"/>
		         <div class="text" id="u4260_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default flow_shape" id="u4261">
		         <img class="img" id="u4261_img" src="images/c_u_h_nh_kho/u4261.svg"/>
		         <div class="text" id="u4261_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default heading_1" id="u4262">
		         <div class="" id="u4262_div">
		         </div>
		         <div class="text" id="u4262_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Phiếu nhập xuất
		               </span>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="79" data-left="500" data-top="104" data-width="124" id="u4263">
		      <div class="ax_default icon" id="u4264">
		         <img class="img" id="u4264_img" src="images/c_u_h_nh_kho/u4264.svg"/>
		         <div class="text" id="u4264_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default flow_shape" id="u4265">
		         <img class="img" id="u4265_img" src="images/c_u_h_nh_kho/u4261.svg"/>
		         <div class="text" id="u4265_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default heading_1" id="u4266">
		         <div class="" id="u4266_div">
		         </div>
		         <div class="text" id="u4266_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Tồn kho
		               </span>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="79" data-left="344" data-top="203" data-width="124" id="u4267">
		      <div class="ax_default icon" id="u4268">
		         <img class="img" id="u4268_img" src="images/c_u_h_nh_kho/u4268.svg"/>
		         <div class="text" id="u4268_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="79" data-left="344" data-top="203" data-width="124" id="u4269">
		         <div class="ax_default flow_shape" id="u4270">
		            <img class="img" id="u4270_img" src="images/c_u_h_nh_kho/u4261.svg"/>
		            <div class="text" id="u4270_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default heading_1" id="u4271">
		            <div class="" id="u4271_div">
		            </div>
		            <div class="text" id="u4271_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Nhập hàng
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="79" data-left="500" data-top="203" data-width="124" id="u4272">
		      <div class="ax_default icon" id="u4273">
		         <img class="img" id="u4273_img" src="images/c_u_h_nh_kho/u4273.svg"/>
		         <div class="text" id="u4273_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="79" data-left="500" data-top="203" data-width="124" id="u4274">
		         <div class="ax_default flow_shape" id="u4275">
		            <img class="img" id="u4275_img" src="images/c_u_h_nh_kho/u4261.svg"/>
		            <div class="text" id="u4275_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default heading_1" id="u4276">
		            <div class="" id="u4276_div">
		            </div>
		            <div class="text" id="u4276_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Xuất hàng
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="79" data-left="656" data-top="104" data-width="124" id="u4277">
		      <div class="ax_default icon" id="u4278">
		         <img class="img" id="u4278_img" src="images/c_u_h_nh_kho/u4278.svg"/>
		         <div class="text" id="u4278_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="79" data-left="656" data-top="104" data-width="124" id="u4279">
		         <div class="ax_default flow_shape" id="u4280">
		            <img class="img" id="u4280_img" src="images/c_u_h_nh_kho/u4261.svg"/>
		            <div class="text" id="u4280_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default heading_1" id="u4281">
		            <div class="" id="u4281_div">
		            </div>
		            <div class="text" id="u4281_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Kiểm kho
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="79" data-left="656" data-top="308" data-width="124" id="u4282">
		      <div class="ax_default icon" id="u4283">
		         <img class="img" id="u4283_img" src="images/c_u_h_nh_kho/u4283.svg"/>
		         <div class="text" id="u4283_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default flow_shape" id="u4284">
		         <img class="img" id="u4284_img" src="images/c_u_h_nh_kho/u4261.svg"/>
		         <div class="text" id="u4284_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default heading_1" id="u4285">
		         <div class="" id="u4285_div">
		         </div>
		         <div class="text" id="u4285_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Quản lý
		               </span>
		            </p>
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  sản phẩm
		               </span>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="79" data-left="656" data-top="203" data-width="124" id="u4286">
		      <div class="ax_default icon" id="u4287">
		         <img class="img" id="u4287_img" src="images/c_u_h_nh_kho/u4287.svg"/>
		         <div class="text" id="u4287_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="79" data-left="656" data-top="203" data-width="124" id="u4288">
		         <div class="ax_default flow_shape" id="u4289">
		            <img class="img" id="u4289_img" src="images/c_u_h_nh_kho/u4261.svg"/>
		            <div class="text" id="u4289_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default heading_1" id="u4290">
		            <div class="" id="u4290_div">
		            </div>
		            <div class="text" id="u4290_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Nhà cung cấp
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="55" data-label="Header" data-left="133" data-top="17" data-width="516" id="u4291">
		      <div class="ax_default paragraph" id="u4292">
		         <div class="" id="u4292_div">
		         </div>
		         <div class="text" id="u4292_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Cấu hình kho
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u4293">
		         <div class="" id="u4293_div">
		         </div>
		         <div class="text" id="u4293_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Thống kê, quản lý kho
		               </span>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="79" data-left="344" data-top="308" data-width="124" id="u4294">
		      <div class="ax_default" data-height="79" data-left="344" data-top="308" data-width="124" id="u4295">
		         <div class="ax_default flow_shape" id="u4296">
		            <img class="img" id="u4296_img" src="images/c_u_h_nh_kho/u4261.svg"/>
		            <div class="text" id="u4296_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default heading_1" id="u4297">
		            <div class="" id="u4297_div">
		            </div>
		            <div class="text" id="u4297_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Kho
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u4298">
		         <img class="img" id="u4298_img" src="images/c_u_h_nh_kho/u4298.svg"/>
		         <div class="text" id="u4298_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="79" data-left="500" data-top="308" data-width="124" id="u4299">
		      <div class="ax_default" data-height="79" data-left="500" data-top="308" data-width="124" id="u4300">
		         <div class="ax_default" data-height="79" data-left="500" data-top="308" data-width="124" id="u4301">
		            <div class="ax_default flow_shape" id="u4302">
		               <img class="img" id="u4302_img" src="images/c_u_h_nh_kho/u4261.svg"/>
		               <div class="text" id="u4302_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default heading_1" id="u4303">
		               <div class="" id="u4303_div">
		               </div>
		               <div class="text" id="u4303_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Chuyển kho
		                     </span>
		                  </p>
		               </div>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u4304">
		         <img class="img" id="u4304_img" src="images/c_u_h_nh_kho/u4304.svg"/>
		         <div class="text" id="u4304_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		</div>
		
      </div>
    );
  }
}

export default Page;
