import React from "react";
import { connect } from "dva";
import { Button } from "react-bootstrap";
import { Formik } from "formik";
import Input from "../../../../components/Form/Input";
import Select from "../../../../components/Form/Select";
import { Link } from "react-router-dom";
class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      filterValue: []
    };
  }

  onChangeCenter = id => {
    const { dispatch } = this.props;
    dispatch({
      type: "Global/showLoading"
    });
    Promise.all([
      dispatch({
        type: "Class/listprogram",
        payload: {
          centerId: id
        }
      }),
      dispatch({
        type: "Class/listsurchage",
        payload: {
          centerId: id
        }
      }),
      dispatch({
        type: "Class/listmanage",
        payload: {
          centerId: id
        }
      }),
      dispatch({
        type: "Class/updateoptions",
        payload: {
          center_Id: id
        }
      })
    ]).then(
      () => {
        dispatch({
          type: "Global/hideLoading"
        });
      },
      error => {
        dispatch({
          type: "Global/hideLoading"
        });
        dispatch({
          type: "Global/showError"
        });
      }
    );
  };

  render() {
    const { Class } = this.props;
    console.log("Class", Class);
    return (
      <Formik
        enableReinitialize={true}
        initialValues={{
          codeOrName: "",
          center_Id: "",
          program_Id: "",
          manage_Id: ""
        }}
        onSubmit={(values, { setSubmitting }) => {
          const { dispatch } = this.props;
          dispatch({
            type: "Global/showLoading"
          });
          dispatch({
            type: "Class/updateoptions",
            payload: values
          }).then(() => {
            dispatch({
              type: "Global/hideLoading"
            });
          });
        }}
      >
        {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
          setFieldValue
          /* and other goodies */
        }) => (
          <form style={{ width: "100%" }} onSubmit={handleSubmit}>
            <div className="row align-end">
              <div className="col-lg-3 col-md-4 col-sm-6">
                <div className="form-group">
                  <Input
                    id="codeOrName"
                    name="codeOrName"
                    label="Tên / Mã lớp"
                    value={values.codeOrName}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    placeholder={"Nhập Tên / Mã lớp"}
                  />
                </div>
              </div>
              <div className="col-lg-2 col-md-4 col-sm-6">
                <div className="form-group">
                  <Select
                    id="center_Id"
                    name="center_Id"
                    label="Trung tâm"
                    options={Class?.list_trung_tam}
                    value={values.center_Id}
                    onChange={e => {
                      const value = e.target.value;
                      setFieldValue("center_Id", value);
                      this.onChangeCenter(value);
                    }}
                    onBlur={handleBlur}
                  />
                </div>
              </div>
              <div className="col-lg-2 col-md-4 col-sm-6">
                <div className="form-group">
                  <Select
                    id="manage_Id"
                    name="manage_Id"
                    label="Phụ trách"
                    options={Class?.list_quan_ly}
                    value={values.manage_Id}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                </div>
              </div>
              <div className="col-lg-2 col-md-4 col-sm-6">
                <div className="form-group">
                  <Select
                    id="program_Id"
                    name="program_Id"
                    label="Chương trình học"
                    options={Class?.list_chuong_trinh}
                    value={values.program_Id}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                </div>
              </div>
              {/* <div className="col-lg-2 col-md-4 col-sm-6">
                <div className="form-group">
                  <label htmlFor="ten">Tên chương trình</label>
                  <MultiSelect
                    options={STATUS}
                    value={this.state.filterValue}
                    onChange={data => this.setState({ filterValue: data })}
                    labelledBy="Select"
                    overrideStrings={{
                      allItemsAreSelected: "Tất cả",
                      clearSearch: "Clear Search",
                      noOptions: "Không tìm thấy",
                      search: "Search",
                      selectAll: "Select All",
                      selectSomeItems: "Có thể chọn nhiều giá trị"
                    }}
                    ArrowRenderer={() => <i className="fa fa-chevron-down" />}
                    ItemRenderer={customItemRenderer}
                  />
                </div>
              </div> */}
              <div className="col-lg-3 col-md-4 col-sm-6 text-right">
                <div className="form-group">
                  <Button
                    variant="primary"
                    type="submit"
                    style={{ marginRight: 15 }}
                  >
                    Tìm kiếm
                  </Button>
                  <Link to="/them_lop">
                    <Button variant="primary">Tạo mới</Button>
                  </Link>
                </div>
              </div>
            </div>
          </form>
        )}
      </Formik>
    );
  }
}

export default connect(({ Class }) => ({
  Class
}))(Page);
