import React, { useState, useForm } from "react";
import { Modal, Button, Table } from "react-bootstrap";
import CustomTable from "../../../../components/Table";
import moment from "moment";
import { connect } from "dva";

function CustomModal(props) {
  const [show, setShow] = useState(true);
  const { Class } = props;

  const handleClose = () => {
    // setShow(false);
    props.onClose();
  };
  const columns = [
    {
      title: "Mã học viên",
      key: "code",
      class: "tb-width-100"
    },
    {
      title: "Tên",
      key: "firstName",
      class: "tb-width-100"
    },
    {
      title: "Họ Đệm",
      key: "lastName",
      class: "tb-width-150"
    },
    {
      title: "Số điện thoại",
      key: "phone",
      class: "tb-width-150"
    },
    {
      title: "Email",
      key: "email",
      class: "tb-width-200"
    },
    {
      title: "Đã thanh toán",
      key: "collectAmount",
      class: "tb-width-150",
      render: (col, index) => (
        <span>
          {col.collectAmount?.toLocaleString("it-IT", {
            style: "currency",
            currency: "VND"
          })}
        </span>
      )
    },

    {
      title: "Nợ",
      key: "debtAmount",
      class: "tb-width-150",
      render: (col, index) => (
        <span>
          {col.debtAmount?.toLocaleString("it-IT", {
            style: "currency",
            currency: "VND"
          })}
        </span>
      )
    }
  ];

  console.log("props", props);

  return (
    <Modal
      size={"lg"}
      show={show}
      onHide={handleClose}
      backdrop="static"
      keyboard={false}
      id="AddNew"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title>Thông tin lớp</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div>
          <form>
            <fieldset className="m-b-2">
              <legend>Thông tin lớp học</legend>
              <div className="row">
                <div className="col-6">
                  <p>
                    Cơ sở: <b>{Class?.current_account?.centerName}</b>
                  </p>
                  <p>
                    Mã lớp: <b>{Class?.current_account?.code}</b>
                  </p>
                  <p>
                    Bắt đầu dự kiến:{" "}
                    <b>{Class?.current_account?.expectedBegin}</b>
                  </p>
                  <p>
                    Chương trình: <b>{Class?.current_account?.programName}</b>
                  </p>
                  <p>
                    PH dự định: <b>{Class?.current_account?.name}</b>
                  </p>
                  <p>
                    Sỹ số tối thiểu: <b>{Class?.current_account?.studentMin}</b>
                  </p>
                  <p>
                    Gói học phí: <b>{Class?.current_account?.priceName}</b>
                  </p>
                </div>
                <div className="col-6">
                  <p>
                    Phụ trách: <b>{Class?.current_account?.programName}</b>
                  </p>
                  <p>
                    Tên lớp: <b>{Class?.current_account?.roomName}</b>
                  </p>
                  <p>
                    Kết thúc dự kiến:{" "}
                    <b>{Class?.current_account?.expectedEnd}</b>
                  </p>
                  <p>
                    Số buổi: <b>{Class?.current_account?.lessonCount}</b>
                  </p>
                  <p>
                    Sức chứa: <b>{Class?.current_account?.roomCapacity}</b>
                  </p>
                  <p>
                    Ss tối đa: <b>{Class?.current_account?.studentMax}</b>
                  </p>
                  <div style={{ display: "flex" }}>
                    <p>Phụ thu:</p>
                    <ul className="list-default">
                      {Class?.current_account?.surchageNames?.map(
                        (sur, index) => (
                          <li key={`surchageNames${index}`}>{sur}</li>
                        )
                      )}
                    </ul>
                  </div>
                </div>
                <div className="col-12">
                  <div className="form-group">
                    <label htmlFor="bai_ve_nha">Mô tả</label>
                    <textarea
                      className="textarea form-control"
                      rows="4"
                      name="bai_ve_nha"
                      defaultValue={Class?.current_account?.description}
                      disabled
                    ></textarea>
                  </div>
                </div>
              </div>
            </fieldset>
            <fieldset>
              <legend>Thông tin học viên</legend>
              <div className="row">
                <div className="col-12">
                  <CustomTable
                    dataSource={Class?.current_account?.studentList}
                    total={0}
                    columns={columns}
                    onChange={data => {}}
                    noPaging
                  ></CustomTable>
                </div>
              </div>
            </fieldset>
          </form>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button variant={"secondary"} onClick={handleClose}>
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  );
}

export default connect(({ Class }) => ({ Class }))(CustomModal);
