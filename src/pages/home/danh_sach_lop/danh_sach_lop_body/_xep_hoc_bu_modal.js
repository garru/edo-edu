import React from "react";
import moment from "moment";
import { Modal, Button, Table } from "react-bootstrap";
import { connect } from "dva";
import { Formik, Field, Form } from "formik";
import Input from "../../../../components/Form/Input";
import Select from "../../../../components/Form/Select";
import * as yup from "yup";

const ModalHocBu = ({ handleClose, Class, ClassOffset, dispatch, id }) => {
  const onClose = () => {
    // dispatch({
    //   type: "Class/reset"
    // });
    handleClose();
  };
  const onChangeProgram = id => {
    dispatch({
      type: "Global/showLoading"
    });
    dispatch({
      type: "ClassOffset/listclass",
      payload: { programId: id }
    }).then(data => {
      dispatch({
        type: "Global/hideLoading"
      });
      !data &&
        dispatch({
          type: "Global/showError"
        });
    });
  };

  const onChangeClass = id => {
    // dispatch({
    //   type: "Global/showLoading"
    // });
    // dispatch({
    //   type: "Class/listschedule",
    //   payload: { classId: id }
    // }).then(data => {
    //   dispatch({
    //     type: "Global/hideLoading"
    //   });
    //   !data &&
    //     dispatch({
    //       type: "Global/showError"
    //     });
    // });
  };

  return (
    <Modal show size="lg">
      <Modal.Header closeButton>
        <Modal.Title>Xếp lich học thử</Modal.Title>
      </Modal.Header>
      <Formik
        enableReinitialize={true}
        validationSchema={yup.object().shape({
          programId: yup.string().required("Trường bắt buộc"),
          classId: yup.string().required("Trường bắt buộc"),
          scheduleId: yup.string().required("Trường bắt buộc")
        })}
        initialValues={{
          programId: "",
          classId: "",
          scheduleId: ""
        }}
        onSubmit={(values, { setSubmitting }) => {
          dispatch({
            type: "Global/showLoading"
          });
          dispatch({
            type: "Class/insert",
            payload: {
              partnerId: id,
              studentId: Class.current_student_trial.id,
              programId: +values.programId,
              classId: +values.classId,
              scheduleId: +values.scheduleId
            }
          }).then(data => {
            dispatch({
              type: "Global/hideLoading"
            });
            if (data) {
              dispatch({
                type: "Global/showSuccess"
              });
              onClose();
            } else {
              dispatch({
                type: "Global/showError"
              });
            }
          });
        }}
      >
        {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
          setFieldValue
          /* and other goodies */
        }) => (
          <Form style={{ width: "100%" }} onSubmit={handleSubmit}>
            <Modal.Body>
              <div className="row m-b-1">
                <div className="col-12">
                  <fieldset>
                    <legend>Thông tin khách hàng</legend>
                    <div className="col-12">
                      <div className="row">
                        <div className="col-md-4 col-sm-6 col-xs-12">
                          <p>
                            Mã khách hàng:
                            <b>{Class?.current_student_trial?.code}</b>
                          </p>
                        </div>
                        <div className="col-md-4 col-sm-6 col-xs-12">
                          <p>
                            Tên:
                            <b>
                              {Class?.current_student_trial?.lastName}
                            </b>
                          </p>
                        </div>
                        <div className="col-md-4 col-sm-6 col-xs-12">
                          <p>
                            Họ Đệm:
                            <b>
                              {Class?.current_student_trial?.firstName}
                            </b>
                          </p>
                        </div>
                        <div className="col-md-4 col-sm-6 col-xs-12">
                          <p>
                            Ngày tháng năm sinh:
                            <b>
                              {moment(
                                Class?.current_student_trial?.birthDate
                              ).format("DD/MM/YYYY")}
                            </b>
                          </p>
                        </div>
                        <div className="col-md-4 col-sm-6 col-xs-12">
                          <p>
                            Giới tính:
                            <b>{Class?.current_student_trial?.gender}</b>
                          </p>
                        </div>
                        <div className="col-md-4 col-sm-6 col-xs-12">
                          <p>
                            Phụ Huynh:
                            <b>
                              {Class?.current_student_trial?.studentName}
                            </b>
                          </p>
                        </div>
                      </div>
                    </div>
                  </fieldset>
                </div>
              </div>

              <div className="row">
                <div className="col-6">
                  <div className="form-group">
                    <Select
                      id="programId"
                      name="programId"
                      label="Chương trình học:"
                      options={Class?.list_chuong_trinh}
                      value={values.programId}
                      onChange={e => {
                        const value = e.target.value;
                        setFieldValue("programId", value);
                        onChangeProgram(value);
                      }}
                      onBlur={handleBlur}
                    />
                  </div>
                </div>
                <div className="col-6">
                  <div className="form-group">
                    <Select
                      id="classId"
                      name="classId"
                      label="Lớp học:"
                      options={Class.list_lop}
                      value={values.classId}
                      onChange={e => {
                        const value = e.target.value;
                        setFieldValue("classId", value);
                        onChangeClass(value);
                      }}
                      onBlur={handleBlur}
                    />
                  </div>
                </div>
              </div>
              {Class.list_schedule.length ? (
                <div role="group" aria-labelledby="my-radio-group">
                  <Table bordered hover className="table">
                    <thead>
                      <tr>
                        <th>Buổi học</th>
                        <th>Nội dung</th>
                        <th>Ngày</th>
                        <th>GV Việt Nam</th>
                        <th>GV Nước ngoài</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                      {Class.list_schedule.map(item => (
                        <tr key={`list_schedule_${item.id}`}>
                          <td>{item.lessonNumber}</td>
                          <td>{item.lessonContent}</td>
                          <td>{item.lessonDate}</td>
                          <td>{item.teacherVietnamese}</td>
                          <td>{item.teacherForgein}</td>
                          <td>
                            <input
                              type="radio"
                              name="scheduleId"
                              value={values.scheduleId}
                              onChange={e =>
                                e.target.checked &&
                                setFieldValue("scheduleId", item.id)
                              }
                            />
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </Table>
                </div>
              ) : (
                ""
              )}
            </Modal.Body>
            <Modal.Footer>
              <Button
                variant="primary"
                style={{ marginRight: 15 }}
                type="submit"
              >
                Lưu
              </Button>
              <Button variant={"secondary"} onClick={onClose}>
                Đóng
              </Button>
            </Modal.Footer>
          </Form>
        )}
      </Formik>
    </Modal>
  );
};

export default connect(({ Class, ClassOffset, Global }) => ({
  Class,
  ClassOffset,
  Global
}))(ModalHocBu);
