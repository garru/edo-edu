import React from "react";
import { connect } from "dva";
import * as _ from "lodash";
import CustomTooltip from "../../../../components/CustomTooltip";
import CustomTable from "../../../../components/Table";
import moment from "moment";
import CustomModal from "./modal";
// import ModalHocBu from "./_xep_hoc_bu_modal";
import { Link } from "react-router-dom";

class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id: null,
      activeIndex: null,
      showModal: false,
      isCreateNew: false,
      target: null,
      show: false,
      tootlTipindex: null,
      showModalHocBu: false
    };
  }

  checkAllCheckboxChecked() {
    const uncheckedCheckboxIndex = this.state.checkedItems.findIndex(
      item => !item
    );
    return uncheckedCheckboxIndex !== -1;
  }

  updateCheckedItem(e, index) {
    let newCheckedItems = this.state.checkedItems;
    const value = e.target.checked;
    newCheckedItems[index] = value;
    this.setState({ checkedItems: newCheckedItems });
    if (value) {
      const isCheckedAll = this.checkAllCheckboxChecked();
      isCheckedAll && this.setState({ checkedAll: true });
    } else {
      this.setState({ checkedAll: false });
    }
  }

  checkedAll(e) {
    const value = e.target.checked;
    let newCheckedItems = [];
    this.setState({ checkedAll: value });
    newCheckedItems = this.state.checkedItems.map(item => value);
    this.setState({ checkedItems: newCheckedItems });
  }

  renderTooltip() {
    const { dispatch } = this.props;
    const { tootlTipindex } = this.state;
    const dataSource = [
      {
        label: "Thông tin",
        className: "fas fa-info",
        onClick: e => {
          dispatch({
            type: "Global/showLoading"
          });
          dispatch({
            type: "Class/view",
            payload: { id: this.state.id }
          }).then(() => {
            dispatch({
              type: "Global/hideLoading"
            });
            this.setState({ isCreateNew: false, showModal: true });
          });
          this.handleClick(e, tootlTipindex, true);
        }
      },
      {
        label: "Chỉnh sửa",
        className: "fa fa-edit",
        link: `/them_lop`,
        onClick: e => {
          this.handleClick(e, tootlTipindex, true);
        }
      },
      {
        label: "Điểm danh /nhận xét",
        className: "far fa-clipboard",
        link: `/diem_danh/${this.state.id}`,
        onClick: e => {
          this.handleClick(e, tootlTipindex, true);
        }
      },
      {
        label: "Tổng hợp điểm danh",
        className: "fas fa-users",
        link: `/tong_hop_diem_danh/${this.state.centerId}/${this.state.id}`,
        onClick: e => {
          this.handleClick(e, tootlTipindex, true);
        }
      },
      {
        label: "Ghép lớp",
        className: "fas fa-plus",
        link: `/ghep_lop`,
        onClick: e => {
          this.handleClick(e, tootlTipindex, true);
        }
      },
      {
        label: "Xếp bù",
        className: "fas fa-user-plus",
        onClick: e => {
          this.handleClick(e, tootlTipindex, true);
        }
      },
      {
        label: "Lưu trữ",
        className: "fa fa-file-archive",
        onClick: e => {
          this.handleClick(e, tootlTipindex, true);
        }
      },
      {
        label: "Xoá lớp",
        className: "fa fa-trash-alt",
        onClick: e => {
          dispatch({
            type: "Global/showLoading"
          });
          dispatch({
            type: "Class/delete",
            payload: { id: this.state.id }
          }).then(data => {
            if (data) {
              this.updateOptions({});
            } else {
              dispatch({
                type: "Global/hideLoading"
              });
              dispatch({
                type: "Global/showError"
              });
            }
          });
          this.handleClick(e, tootlTipindex, true);
        }
      }
    ];
    return (
      <CustomTooltip
        show={this.state.show}
        onHide={e => this.handleClick(e, tootlTipindex, true)}
        dataSource={dataSource}
        target={this.state.target}
      ></CustomTooltip>
    );
  }

  handleClick(e, index, isHide) {
    this.setState({
      show: !(isHide || index === this.state.activeIndex),
      target: isHide ? null : e.target,
      activeIndex: isHide || index === this.state.activeIndex ? null : index
    });
  }

  updateOptions = data => {
    const { dispatch } = this.props;
    dispatch({
      type: "Global/showLoading"
    });
    dispatch({
      type: "Class/updateoptions",
      payload: data
    }).then(res => {
      dispatch({
        type: "Global/hideLoading"
      });
      !res &&
        dispatch({
          type: "Global/showError"
        });
    });
  };

  onClose = isUpdate => {
    this.setState({ isCreateNew: false, showModal: false });
  };

  render() {
    const columns = [
      {
        title: "Mã lớp",
        key: "code",
        class: "tb-width-150"
      },
      {
        title: "Tên lớp",
        key: "name",
        class: "tb-width-150"
      },
      {
        title: "Giáo viên Việt",
        key: "teacherDomestic",
        class: "tb-width-200"
      },
      {
        title: "Giáo viên nước ngoài",
        key: "teacherForeign",
        class: "tb-width-200"
      },
      {
        title: "Trạng thái",
        key: "status",
        class: "tb-width-150",
        render: (col, index) => (
          <div className={`${col.trang_thai} status`}>
            <span>{col.status}</span>
          </div>
        )
      },
      {
        title: "Trung tâm",
        key: "centerName",
        class: "tb-width-200"
      },
      {
        title: "Bắt đầu dự kiến",
        key: "beginDate",
        class: "tb-width-200",
        render: (col, index) =>
          col.beginDate ? moment(col.beginDate).format("DD/MM/YYYY") : ""
      },
      {
        title: "Bắt đầu",
        key: "expectedBegin",
        class: "tb-width-150",
        render: (col, index) =>
          col.expectedBegin
            ? moment(col.expectedBegin).format("DD/MM/YYYY")
            : ""
      },
      {
        title: "Kết thúc",
        key: "endDate",
        class: "tb-width-150",
        render: (col, index) =>
          col.endDate ? moment(col.endDate).format("DD/MM/YYYY") : ""
      },
      {
        title: "Chương trình",
        key: "programName",
        class: "tb-width-200"
      },
      {
        title: "Sỹ số (Hiện tại)",
        key: "studentCount",
        class: "tb-width-200"
      },
      {
        title: "Đã xếp lớp",
        key: "hasArrange",
        class: "tb-width-200"
      },
      {
        title: "Cần xếp Bù",
        key: "needMakeup",
        class: "tb-width-200"
      },
      {
        title: "Tổng/đã học trình",
        key: "lessonRate",
        class: "tb-width-200"
      },
      {
        title: "Đã thu",
        key: "collectAmount",
        class: "tb-width-200",
        render: (col, index) => (
          <span>
            {col?.collectAmount?.toLocaleString("it-IT", {
              style: "currency",
              currency: "VND"
            })}
          </span>
        )
      },
      {
        title: "Công nợ",
        key: "debtAmount",
        class: "tb-width-200",
        render: (col, index) => {
          return (
            <span>
              {col?.debtAmount?.toLocaleString("it-IT", {
                style: "currency",
                currency: "VND"
              })}
            </span>
          );
        }
      },
      {
        title: "Lịch học",
        key: "scheduleConfig",
        class: "tb-width-200",
        render: (col, index) =>
          col.scheduleConfig ? (
            <Link to={`/lich_hoc/${col.id}`}>
              <div className={`configured status`}>
                <span>Đã cấu hình</span>
              </div>
            </Link>
          ) : (
            <Link to={`/tao_moi_lich_hoc/${col.id}`}>
              <div
                className={`${col.scheduleConfig ? "configured" : ""} status`}
              >
                <span>
                  {col.scheduleConfig ? "Đã cấu hình" : "Chưa cấu hình"}
                </span>
              </div>
            </Link>
          )
      },
      {
        title: "Quản lý",
        key: "quan_ly",
        class: "tb-width-100",
        render: (col, index) => (
          <>
            <i
              className="fa fa-ellipsis-h"
              style={{
                marginRight: 0,
                marginLeft: "auto"
              }}
              onClick={e => {
                this.handleClick(e, index);
                this.setState({
                  id: col.id,
                  tootlTipindex: index,
                  centerId: col.centerId || 1 // To be Check
                });
              }}
            />
          </>
        )
      }
    ];
    return (
      <div className="row">
        <div className="col-12">
          <CustomTable
            dataSource={this.props.Class?.list}
            total={this.props.Class?.total_record}
            columns={columns}
            onChange={data => this.updateOptions(data)}
          ></CustomTable>
        </div>
        {this.state.showModal && (
          <CustomModal onClose={this.onClose} id={this.state.id}></CustomModal>
        )}

        {this.renderTooltip()}
      </div>
    );
  }
}

export default connect(({ Class }) => ({
  Class
}))(Page);
