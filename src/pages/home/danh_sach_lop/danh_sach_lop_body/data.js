export const HEADER = [
  {
    title: "Mã lớp",
    key: "ma_lop",
    class: "tb-width-150",
    sortKey: true
  },
  {
    title: "Tên lớp",
    key: "ten_lop",
    class: "tb-width-150",
    sortKey: true
  },
  {
    title: "Giáo viên Việt",
    key: "giao_vien_viet",
    class: "tb-width-200",
    sortKey: true
  },
  {
    title: "Giáo viên nước ngoài",
    key: "giao_vien_nuoc_ngoai",
    class: "tb-width-200",
    sortKey: true
  },
  {
    title: "Trạng thái",
    key: "trang_thai",
    class: "tb-width-150"
  },
  {
    title: "Trung tâm",
    key: "trung_tam",
    class: "tb-width-200"
  },
  {
    title: "Bắt đầu dự kiến",
    key: "bat_dau_du_kien",
    class: "tb-width-200"
  },
  {
    title: "Bắt đầu",
    key: "bat_dau",
    class: "tb-width-150"
  },
  {
    title: "Kết thúc",
    key: "ket_thuc",
    class: "tb-width-150"
  },
  {
    title: "Chương trình",
    key: "chuong_trinh",
    class: "tb-width-200"
  },
  {
    title: "Sỹ số (Hiện tại)",
    key: "sy_so_hien_tai",
    class: "tb-width-200"
  },
  {
    title: "Đã xếp lớp",
    key: "da_xep_lop",
    class: "tb-width-200"
  },
  {
    title: "Cần xếp Bù",
    key: "can_xep_bu",
    class: "tb-width-200"
  },
  {
    title: "Tổng/đã học trình",
    key: "tong_da_hoc_trinh",
    class: "tb-width-200"
  },
  {
    title: "Đã thu",
    key: "da_thu",
    class: "tb-width-200"
  },
  {
    title: "Công nợ",
    key: "cong_no",
    class: "tb-width-200"
  },
  {
    title: "Lịch học",
    key: "lich_hoc",
    class: "tb-width-200"
  },
  {
    title: "Quản lý",
    key: "quan_ly",
    class: "tb-width-100"
  }
];

export const CHUONG_TRINH_HOC = [
  {
    code: "",
    text: "Chọn trung tâm"
  },
  {
    code: "BIBOB 1",
    text: "BIBOB 1"
  },
  {
    code: "BIBOB 2",
    text: "BIBOB 2"
  },
  {
    code: "BIG ENGLIST PLUS 1",
    text: "BIG ENGLIST PLUS 1"
  },
  {
    code: "BIG ENGLIST PLUS 2",
    text: "BIG ENGLIST PLUS 2"
  },
  {
    code: "BIG ENGLIST PLUS 3",
    text: "BIG ENGLIST PLUS 3"
  }
];

export const STATUS = [
  {
    label: "Tất cả",
    value: "all"
  },
  {
    label: "Chưa xếp lịch",
    value: "not_yet_set"
  },
  {
    label: "Chưa bắt đầu",
    value: "not_yet_start"
  },
  {
    label: "Đang học",
    value: "studying"
  },
  {
    label: "Sắp kết thúc",
    value: "finishing"
  },
  {
    label: "Đã kết thúc",
    value: "finished"
  },
  {
    label: "Hủy",
    value: "cancelled"
  }
];

export const DATA = [
  {
    ma_lop: "HNB21001",
    ten_lop: "BIBOB1-2021",
    giao_vien_viet: "Hoàng Dung",
    giao_vien_nuoc_ngoai: "Peter Lomia",
    trang_thai: "not_yet_set",
    trang_thai_text: "Chưa xếp lịch",
    trung_tam: "Hoàng Ngân",
    bat_dau_du_kien: "30/04/2021",
    bat_dau: "30/04/2021",
    ket_thuc: "30/04/2021",
    chuong_trinh: "BIBOP1",
    sy_so_hien_tai: 20,
    da_xep_lop: 10,
    can_xep_bu: 0,
    tong_da_hoc_trinh: "70/0",
    da_thu: "15.000.000",
    cong_no: "60.000.000",
    lich_hoc: "not_yet_configured",
    lich_hoc_text: "Chưa cấu hình"
  },
  {
    ma_lop: "HNB21002",
    ten_lop: "BIBOB1-2021",
    giao_vien_viet: "Hoàng Dung",
    giao_vien_nuoc_ngoai: "Peter Lomia",
    trang_thai: "not_yet_start",
    trang_thai_text: "Chưa bắt đầu",
    trung_tam: "Hoàng Ngân",
    bat_dau_du_kien: "30/04/2021",
    bat_dau: "30/04/2021",
    ket_thuc: "30/04/2021",
    chuong_trinh: "BIBOP1",
    sy_so_hien_tai: 20,
    da_xep_lop: 10,
    can_xep_bu: 0,
    tong_da_hoc_trinh: "70/0",
    da_thu: "15.000.000",
    cong_no: "60.000.000",
    lich_hoc: "configured",
    lich_hoc_text: "Đã cấu hình"
  },
  {
    ma_lop: "HNB21003",
    ten_lop: "BIBOB1-2021",
    giao_vien_viet: "Hoàng Dung",
    giao_vien_nuoc_ngoai: "Peter Lomia",
    trang_thai: "studying",
    trang_thai_text: "Đang học",
    trung_tam: "Hoàng Ngân",
    bat_dau_du_kien: "30/04/2021",
    bat_dau: "30/04/2021",
    ket_thuc: "30/04/2021",
    chuong_trinh: "BIBOP1",
    sy_so_hien_tai: 20,
    da_xep_lop: 10,
    can_xep_bu: 0,
    tong_da_hoc_trinh: "70/0",
    da_thu: "15.000.000",
    cong_no: "60.000.000",
    lich_hoc: "configuring",
    lich_hoc_text: "Đã cấu hình"
  },
  {
    ma_lop: "HNB21004",
    ten_lop: "BIBOB1-2021",
    giao_vien_viet: "Hoàng Dung",
    giao_vien_nuoc_ngoai: "Peter Lomia",
    trang_thai: "finishing",
    trang_thai_text: "Sắp kết thúc",
    trung_tam: "Hoàng Ngân",
    bat_dau_du_kien: "30/04/2021",
    bat_dau: "30/04/2021",
    ket_thuc: "30/04/2021",
    chuong_trinh: "BIBOP1",
    sy_so_hien_tai: 20,
    da_xep_lop: 10,
    can_xep_bu: 0,
    tong_da_hoc_trinh: "70/0",
    da_thu: "15.000.000",
    cong_no: "60.000.000",
    lich_hoc: "configuring",
    lich_hoc_text: "Đã cấu hình"
  },
  {
    ma_lop: "HNB21005",
    ten_lop: "BIBOB1-2021",
    giao_vien_viet: "Hoàng Dung",
    giao_vien_nuoc_ngoai: "Peter Lomia",
    trang_thai: "finished",
    trang_thai_text: "Đã kết thúc",
    trung_tam: "Hoàng Ngân",
    bat_dau_du_kien: "30/04/2021",
    bat_dau: "30/04/2021",
    ket_thuc: "30/04/2021",
    chuong_trinh: "BIBOP1",
    sy_so_hien_tai: 20,
    da_xep_lop: 10,
    can_xep_bu: 0,
    tong_da_hoc_trinh: "70/0",
    da_thu: "15.000.000",
    cong_no: "60.000.000",
    lich_hoc: "configuring",
    lich_hoc_text: "Đã cấu hình"
  },
  {
    ma_lop: "HNB21006",
    ten_lop: "BIBOB1-2021",
    giao_vien_viet: "Hoàng Dung",
    giao_vien_nuoc_ngoai: "Peter Lomia",
    trang_thai: "cancelled",
    trang_thai_text: "Huỷ",
    trung_tam: "Hoàng Ngân",
    bat_dau_du_kien: "30/04/2021",
    bat_dau: "30/04/2021",
    ket_thuc: "30/04/2021",
    chuong_trinh: "BIBOP1",
    sy_so_hien_tai: 20,
    da_xep_lop: 10,
    can_xep_bu: 0,
    tong_da_hoc_trinh: "70/0",
    da_thu: "15.000.000",
    cong_no: "60.000.000",
    lich_hoc: "configuring",
    lich_hoc_text: "Đã cấu hình"
  },
  {
    ma_lop: "HNB21007",
    ten_lop: "BIBOB1-2021",
    giao_vien_viet: "Hoàng Dung",
    giao_vien_nuoc_ngoai: "Peter Lomia",
    trang_thai: "not_yet_start",
    trang_thai_text: "Chưa bắt đầu",
    trung_tam: "Hoàng Ngân",
    bat_dau_du_kien: "30/04/2021",
    bat_dau: "30/04/2021",
    ket_thuc: "30/04/2021",
    chuong_trinh: "BIBOP1",
    sy_so_hien_tai: 20,
    da_xep_lop: 10,
    can_xep_bu: 0,
    tong_da_hoc_trinh: "70/0",
    da_thu: "15.000.000",
    cong_no: "60.000.000",
    lich_hoc: "configured",
    lich_hoc_text: "Đã cấu hình"
  },
  {
    ma_lop: "HNB21008",
    ten_lop: "BIBOB1-2021",
    giao_vien_viet: "Hoàng Dung",
    giao_vien_nuoc_ngoai: "Peter Lomia",
    trang_thai: "studying",
    trang_thai_text: "Đang học",
    trung_tam: "Hoàng Ngân",
    bat_dau_du_kien: "30/04/2021",
    bat_dau: "30/04/2021",
    ket_thuc: "30/04/2021",
    chuong_trinh: "BIBOP1",
    sy_so_hien_tai: 20,
    da_xep_lop: 10,
    can_xep_bu: 0,
    tong_da_hoc_trinh: "70/0",
    da_thu: "15.000.000",
    cong_no: "60.000.000",
    lich_hoc: "configuring",
    lich_hoc_text: "Đã cấu hình"
  },
  {
    ma_lop: "HNB21009",
    ten_lop: "BIBOB1-2021",
    giao_vien_viet: "Hoàng Dung",
    giao_vien_nuoc_ngoai: "Peter Lomia",
    trang_thai: "finishing",
    trang_thai_text: "Sắp kết thúc",
    trung_tam: "Hoàng Ngân",
    bat_dau_du_kien: "30/04/2021",
    bat_dau: "30/04/2021",
    ket_thuc: "30/04/2021",
    chuong_trinh: "BIBOP1",
    sy_so_hien_tai: 20,
    da_xep_lop: 10,
    can_xep_bu: 0,
    tong_da_hoc_trinh: "70/0",
    da_thu: "15.000.000",
    cong_no: "60.000.000",
    lich_hoc: "configuring",
    lich_hoc_text: "Đã cấu hình"
  },
  {
    ma_lop: "HNB210010",
    ten_lop: "BIBOB1-2021",
    giao_vien_viet: "Hoàng Dung",
    giao_vien_nuoc_ngoai: "Peter Lomia",
    trang_thai: "finished",
    trang_thai_text: "Đã kết thúc",
    trung_tam: "Hoàng Ngân",
    bat_dau_du_kien: "30/04/2021",
    bat_dau: "30/04/2021",
    ket_thuc: "30/04/2021",
    chuong_trinh: "BIBOP1",
    sy_so_hien_tai: 20,
    da_xep_lop: 10,
    can_xep_bu: 0,
    tong_da_hoc_trinh: "70/0",
    da_thu: "15.000.000",
    cong_no: "60.000.000",
    lich_hoc: "configuring",
    lich_hoc_text: "Đã cấu hình"
  },
  {
    ma_lop: "HNB210011",
    ten_lop: "BIBOB1-2021",
    giao_vien_viet: "Hoàng Dung",
    giao_vien_nuoc_ngoai: "Peter Lomia",
    trang_thai: "cancelled",
    trang_thai_text: "Huỷ",
    trung_tam: "Hoàng Ngân",
    bat_dau_du_kien: "30/04/2021",
    bat_dau: "30/04/2021",
    ket_thuc: "30/04/2021",
    chuong_trinh: "BIBOP1",
    sy_so_hien_tai: 20,
    da_xep_lop: 10,
    can_xep_bu: 0,
    tong_da_hoc_trinh: "70/0",
    da_thu: "15.000.000",
    cong_no: "60.000.000",
    lich_hoc: "configuring",
    lich_hoc_text: "Đã cấu hình"
  }
];

export const DATA2 = [
  {
    ma_lop: "HNB21002",
    ten_lop: "BIBOB1-2021",
    giao_vien_viet: "Hoàng Dung",
    giao_vien_nuoc_ngoai: "Peter Lomia",
    trang_thai: "not_yet_set",
    trang_thai_text: "Chưa xếp lịch",
    trung_tam: "Hoàng Ngân",
    bat_dau_du_kien: "30/04/2021",
    bat_dau: "30/04/2021",
    ket_thuc: "30/04/2021",
    chuong_trinh: "BIBOP1",
    sy_so_hien_tai: 20,
    da_xep_lop: 10,
    can_xep_bu: 0,
    tong_da_hoc_trinh: "70/0",
    da_thu: "15.000.000",
    cong_no: "60.000.000",
    lich_hoc: "not_yet_configured",
    lich_hoc_text: "Chưa cấu hình"
  }
];
