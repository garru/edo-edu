import React from "react";
import { connect } from "dva";
import DanhSachLopSearch from "./danh_sach_lop_search";
import DanhSachLopBody from "./danh_sach_lop_body";
import PageHeader from "../../../components/PageHeader";
class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: "Global/showLoading"
    });
    Promise.all([
      dispatch({
        type: "Class/search"
      }),
      dispatch({
        type: "Class/listcenter"
      }),
      dispatch({
        type: "ClassOffset/listprogram"
      })
    ]).then(
      () => {
        dispatch({
          type: "Global/hideLoading"
        });
      },
      error => {
        dispatch({
          type: "Global/hideLoading"
        });
        dispatch({
          type: "Global/showError"
        });
      }
    );
  }

  render() {
    console.log(this.props);
    const bre = {
      title: "Quản trị lớp học trong hệ thống",
      breadcrums: [
        {
          title: "Home",
          path: "/"
        },
        {
          title: "Quản trị lớp học trong hệ thống",
          path: ""
        }
      ]
    };
    return (
      <React.Fragment>
        <PageHeader {...bre} />
        <div className="iq-card  pb-0">
          <div className="iq-card-body">
            <DanhSachLopSearch />
            <DanhSachLopBody />
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default connect(({ Center, ClassOffset }) => ({
  Center,
  ClassOffset
}))(Page);
