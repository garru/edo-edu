const now = new Date()

export default [
  {
    id: 0,
    title: 'Giờ học 1 17h30-19h00',
    allDay: true,
    start: new Date(2021, 3, 3),
    end: new Date(2021, 3, 3),
  },
  {
    id: 1,
    title: 'Giờ học 2 17h30-19h00',
    start: new Date(2021, 3, 3),
    end: new Date(2021, 3, 3),
  },
  {
    id: 0,
    title: 'Giờ học 1 17h30-19h00',
    allDay: true,
    start: new Date(2021, 3, 4),
    end: new Date(2021, 3, 4),
  },
  {
    id: 1,
    title: 'Giờ học 2 17h30-19h00',
    start: new Date(2021, 3, 4),
    end: new Date(2021, 3, 4),
  },

  {
    id: 0,
    title: '17h30-19h00',
    allDay: true,
    start: new Date(2021, 5, 6),
    end: new Date(2021, 5, 6),
  },
  {
    id: 1,
    title: 'Giờ học 2 17h30-19h00',
    start: new Date(2021, 3, 5),
    end: new Date(2021, 3, 5),
  },

]