export const HEADER = [
  {
    title: "Học liệu",
    key: "hoc_lieu",
    class: "tb-width-400"
  },
  {
    title: "Số lượng",
    key: "so_luong",
    class: "tb-width-100"
  },
  {
    title: "Phân phối",
    key: "phan_phoi",
    class: "tb-width-150"
  },
  {
    title: "Hoàn",
    key: "hoan",
    class: "tb-width-50"
  }
];


export const DATA = [
    {
        hoc_lieu: "Truyện ABC",
        so_luong: 1,
        phan_phoi: "Cả lớp",
        hoan: true
    },
    {
        hoc_lieu: "Truyện ABC",
        so_luong: 1,
        phan_phoi: "Cả lớp",
        hoan: true
    },
]
