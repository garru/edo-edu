import React from "react";
import { Calendar, momentLocalizer } from "react-big-calendar";
import moment from "moment";
import events from "./events";
import { connect } from "dva";
import CustomModal from "./modal";
import PageHeader from "../../../components/PageHeader";
import Select from "../../../components/Form/Select";
import { Formik } from "formik";
import CustomToolbar from "../../../components/CustomToolbar";

const ColoredDateCellWrapper = ({ children }) =>
  React.cloneElement(React.Children.only(children), {
    style: {
      backgroundColor: "lightblue"
    }
  });
const localizer = momentLocalizer(moment);

class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
      isCreateNew: false,
      selectedEvent: null,
      trung_tam: []
    };
  }

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: "Global/showLoading"
    });
    Promise.all([
      dispatch({
        type: "ScheduleCalendar/listcenter"
      }),
      dispatch({
        type: "ScheduleCalendar/updateoptions",
        payload: {}
      })
    ]).then(res => {
      dispatch({
        type: "Global/hideLoading"
      });
      !res &&
        dispatch({
          type: "Global/showError"
        });
    });
  }

  updateOptions(data) {
    const { dispatch } = this.props;
    dispatch({
      type: "Global/showLoading"
    });
    dispatch({
      type: "ScheduleCalendar/updateoptions",
      payload: data
    }).then(res => {
      dispatch({
        type: "Global/hideLoading"
      });
      !res &&
        dispatch({
          type: "Global/showError"
        });
    });
  }

  onChangeCenter = id => {
    const { dispatch } = this.props;
    dispatch({
      type: "Global/showLoading"
    });
    Promise.all([
      dispatch({
        type: "ScheduleCalendar/listclass",
        payload: {
          centerId: id
        }
      }),
      dispatch({
        type: "ScheduleCalendar/listteacher",
        payload: {
          centerId: id
        }
      }),
      dispatch({
        type: "ScheduleCalendar/updateoptions",
        payload: {
          center_Id: id
        }
      })
    ]).then(
      () => {
        dispatch({
          type: "Global/hideLoading"
        });
      },
      error => {
        dispatch({
          type: "Global/hideLoading"
        });
        dispatch({
          type: "Global/showError"
        });
      }
    );
  };

  onSelectEvent(event) {
    const { id } = event;
    const { dispatch } = this.props;
    dispatch({
      type: "Global/showLoading"
    });
    dispatch({
      type: "ScheduleCalendar/view",
      payload: {
        id
      }
    }).then(res => {
      dispatch({
        type: "Global/hideLoading"
      });
      if (res) {
        this.setState({
          showModal: true,
          isCreateNew: false,
          selectedEvent: res.content
        });
        console.log("selectedEvent", res.content);
      } else {
        dispatch({
          type: "Global/showError"
        });
      }
    });

    // this.setState({
    //   showModal: true,
    //   isCreateNew: false,
    //   selectedEvent: event
    // });
  }

  render() {
    const onClose = () => {
      this.setState({ isCreateNew: false, showModal: false });
    };

    const { ScheduleCalendar } = this.props;

    const bre = {
      title: "Điều chỉnh buổi học",
      breadcrums: [
        {
          title: "Home",
          path: "/"
        },
        {
          title: "Điều chỉnh buổi học",
          path: ""
        }
      ]
    };

    const initialValues = {
      center_Id: "",
      teacher_Id: "",
      class_Id: ""
    };
    console.log("events", events);
    return (
      <React.Fragment>
        <PageHeader {...bre}></PageHeader>
        <Formik enableReinitialize={true} initialValues={initialValues}>
          {({
            values,
            handleBlur,
            handleSubmit,
            setFieldValue
            /* and other goodies */
          }) => (
            <form onSubmit={handleSubmit}>
              <div className="iq-card ">
                <div className="iq-card-body">
                  <div className="row align-end m-b-1">
                    <div className="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                      <div className="form-group">
                        <Select
                          id="center_Id"
                          name="center_Id"
                          label="Cơ sở"
                          options={ScheduleCalendar?.list_trung_tam}
                          value={values.center_Id}
                          onChange={e => {
                            const value = e.target.value;
                            setFieldValue("center_Id", value);
                            this.onChangeCenter(value);
                          }}
                          onBlur={handleBlur}
                        />
                      </div>
                    </div>
                    <div className="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                      <div className="form-group">
                        <Select
                          id="teacher_Id"
                          name="teacher_Id"
                          label="Giáo viên"
                          options={ScheduleCalendar?.list_giao_vien}
                          value={values.teacher_Id}
                          onChange={e => {
                            const value = e.target.value;
                            setFieldValue("teacher_Id", value);
                            this.updateOptions({
                              teacher_Id: value
                            });
                          }}
                          onBlur={handleBlur}
                        />
                      </div>
                    </div>
                    <div className="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                      <div className="form-group">
                        <Select
                          id="class_Id"
                          name="class_Id"
                          label="Cơ sở"
                          options={ScheduleCalendar?.list_lop_hoc}
                          value={values.class_Id}
                          onChange={e => {
                            const value = e.target.value;
                            setFieldValue("class_Id", value);
                            this.updateOptions({
                              class_Id: value
                            });
                          }}
                          onBlur={handleBlur}
                        />
                      </div>
                    </div>
                  </div>
                  <Calendar
                    selectable
                    events={ScheduleCalendar.list}
                    views={["month"]}
                    step={60}
                    showMultiDayTimes
                    // max={dates.add(dates.endOf(new Date(2015, 17, 1), 'day'), -1, 'hours')}
                    defaultDate={new Date()}
                    components={{
                      timeSlotWrapper: ColoredDateCellWrapper
                    }}
                    onNavigate={data => {
                      const year = moment(data).years();
                      const month = moment(data).months() + 1;
                      this.updateOptions({
                        year,
                        month
                      });
                    }}
                    localizer={localizer}
                    onSelecting={abc => console.log("abc", abc)}
                    onSelectSlot={range => {
                      this.setState({
                        showModal: true,
                        isCreateNew: true,
                        range: range
                      });
                    }}
                    onSelectEvent={event => this.onSelectEvent(event)}
                    components={{ toolbar: CustomToolbar }}
                  />
                  {this.state.showModal && (
                    <CustomModal
                      range={this.state.range}
                      isCreateNew={this.state.isCreateNew}
                      onClose={onClose}
                      selectedEvent={this.state.selectedEvent}
                    ></CustomModal>
                  )}
                </div>
              </div>
            </form>
          )}
        </Formik>
      </React.Fragment>
    );
  }
}

export default connect(({ ScheduleCalendar, Global }) => ({
  ScheduleCalendar,
  Global
}))(Page);
