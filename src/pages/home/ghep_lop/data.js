export const TRUNG_TAM = [
  {
    code: "",
    text: "Trung tâm hoạt động"
  },
  {
    code: "2",
    text: "Hội sở"
  },
  {
    code: "3",
    text: "Hoàng Ngân"
  },
  {
    code: "4",
    text: "Văn Điển"
  }
];


export const CHUONG_TRINH_HOC = [
  {
    code: "",
    text: "Chọn chương trình học"
  },
  {
    code: "BIBOB 1",
    text: "BIBOB 1"
  },
  {
    code: "BIBOB 2",
    text: "BIBOB 2"
  },
  {
    code: "BIG ENGLIST PLUS 1",
    text: "BIG ENGLIST PLUS 1"
  },
  {
    code: "BIG ENGLIST PLUS 2",
    text: "BIG ENGLIST PLUS 2"
  },
  {
    code: "BIG ENGLIST PLUS 3",
    text: "BIG ENGLIST PLUS 3"
  }
];
