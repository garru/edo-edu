import React from "react";
import { connect } from "dva";
import { Button, Table, Overlay, Popover } from "react-bootstrap";
import * as _ from "lodash";
import PageHeader from "../../../components/PageHeader";
import * as yup from "yup";
import { Formik, ErrorMessage } from "formik";
import Input from "../../../components/Form/Input";
import Select from "../../../components/Form/Select";
import TextArea from "../../../components/Form/TextArea";

class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      lop_nguon: {},
      lop_ghep: {},
      lop_dich: {}
    };
  }

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: "Global/showLoading"
    });
    dispatch({
      type: "MergeClass/listtrungtam"
    }).then(res => {
      dispatch({
        type: "Global/hideLoading"
      });
      if (!res) {
        dispatch({
          type: "Global/showError"
        });
      }
    });
  }

  onChangeCenter = (id, type) => {
    const { dispatch } = this.props;
    let fetchType = "";
    switch (type) {
      case "chinh":
        fetchType = "MergeClass/listlophocchinh";
        break;
      case "phu":
        fetchType = "MergeClass/listlophocphu";
        break;
      default:
        fetchType = "MergeClass/listlophocghep";
        break;
    }
    dispatch({
      type: "Global/showLoading"
    });
    dispatch({
      type: fetchType,
      payload: {
        centerId: id
      }
    }).then(res => {
      dispatch({
        type: "Global/hideLoading"
      });
      if (!res) {
        dispatch({
          type: "Global/showError"
        });
      }
    });
  };

  reset(resetForm, initialValues) {
    const { dispatch } = this.props;
    dispatch({
      type: "MergeClass/reset"
    });
    resetForm && resetForm(initialValues);
    this.setState({
      lop_nguon: {},
      lop_ghep: {},
      lop_dich: {}
    });
    this.forceUpdate()
  }

  render() {
    const bre = {
      title: "Ghép lớp",
      breadcrums: [
        {
          title: "Home",
          path: "/"
        },
        {
          title: "Ghép lớp",
          path: ""
        }
      ]
    };

    const initialValues = {
      trung_tam_chinh: "",
      sourceClass: "",
      trung_tam_phu: "",
      destClass: "",
      trung_tam_ghep: "",
      mergeClass: ""
    }

    return (
      <div>
        <PageHeader {...bre} />
        <div>
          <Formik
            enableReinitialize={true}
            initialValues={initialValues}
            validationSchema={yup.object().shape({
              trung_tam_chinh: yup.string().required("Trường bắt buộc"),
              sourceClass: yup.string().required("Trường bắt buộc"),
              trung_tam_phu: yup.string().required("Trường bắt buộc"),
              destClass: yup.string().required("Trường bắt buộc"),
              trung_tam_ghep: yup.string().required("Trường bắt buộc"),
              mergeClass: yup.string().required("Trường bắt buộc")
            })}
            onSubmit={async (values, { setSubmitting, resetForm }) => {
              const { dispatch } = this.props;
              dispatch({
                type: "Global/showLoading"
              });
              dispatch({
                type: "MergeClass/merge",
                payload: {
                  sourceClass: +values.sourceClass,
                  destClass: +values.destClass,
                  mergeClass: +values.mergeClass
                }
              }).then(res => {
                dispatch({
                  type: "Global/hideLoading"
                });
                if (res) {
                  dispatch({
                    type: "Global/showSuccess"
                  });
                  this.reset(resetForm, initialValues);
                } else {
                  dispatch({
                    type: "Global/showError"
                  });
                }
              });
            }}
          >
            {({
              values,
              errors,
              touched,
              handleChange,
              handleBlur,
              handleSubmit,
              isSubmitting,
              setFieldValue,
              resetForm
              /* and other goodies */
            }) => (
              <form onSubmit={handleSubmit}>
                <div className="row">
                  <div className="col-md-6 col-sm-12">
                    <div className="iq-card">
                      <div className=" iq-card-header d-flex justify-content-between">
                        <div className="iq-header-title">
                          <h6>
                            <b>Lớp gốc</b>
                          </h6>
                        </div>
                      </div>
                      <div className="iq-card-body">
                        <div className="row align-center">
                          <div className="col-5">
                            <div className="form-group">
                              <label htmlFor="trung_tam">Trung tâm:</label>
                            </div>
                          </div>
                          <div className="col-7">
                            <div className="form-group">
                              <Select
                                id="trung_tam_chinh"
                                name="trung_tam_chinh"
                                options={this.props.MergeClass?.list_trung_tam}
                                value={values.trung_tam_chinh}
                                onChange={e => {
                                  const value = e.target.value;
                                  setFieldValue("trung_tam_chinh", value);
                                  this.onChangeCenter(value, "chinh");
                                }}
                                onBlur={handleBlur}
                              />
                            </div>
                          </div>
                        </div>

                        <div className="row align-center">
                          <div className="col-5">
                            <div className="form-group">
                              <label htmlFor="lop">Lớp:</label>
                            </div>
                          </div>
                          <div className="col-7">
                            <div className="form-group">
                              <Select
                                id="sourceClass"
                                name="sourceClass"
                                options={
                                  this.props.MergeClass?.list_lop_hoc_chinh
                                }
                                value={values.sourceClass}
                                onChange={e => {
                                  const value = e.target.value;
                                  setFieldValue("sourceClass", value);
                                  const selectedClass = this.props.MergeClass?.list_lop_hoc_chinh.find(
                                    item => item.id == value
                                  );
                                  this.setState({
                                    lop_nguon: selectedClass
                                  });
                                }}
                                onBlur={handleBlur}
                              />
                            </div>
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-5">
                            <div className="form-group">
                              <label htmlFor="lop">
                                Thời khóa biểu của lớp:
                              </label>
                            </div>
                          </div>
                          <div className="col-7">
                            <ul>
                              {this.state?.lop_nguon?.scheduleTimes?.map(
                                (item, index) => (
                                  <li
                                    className="pb-2"
                                    key={`lop_nguon_${index}`}
                                  >
                                    <b>{item}</b>
                                  </li>
                                )
                              )}
                            </ul>
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-5">
                            <div className="form-group m-0">
                              <label htmlFor="lop">
                                Sỹ số hiện tại của lớp:
                              </label>
                            </div>
                          </div>
                          <div className="col-7">
                            <b>{this.state?.lop_nguon?.studentCount}</b>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-6 col-sm-12">
                    <div className="iq-card">
                      <div className=" iq-card-header d-flex justify-content-between">
                        <div className="iq-header-title">
                          <h6>
                            <b>Lớp đích</b>
                          </h6>
                        </div>
                      </div>
                      <div className="iq-card-body">
                        <div className="row align-center">
                          <div className="col-5">
                            <div className="form-group">
                              <label htmlFor="trung_tam_phu">Trung tâm:</label>
                            </div>
                          </div>
                          <div className="col-7">
                            <div className="form-group">
                              <Select
                                id="trung_tam_phu"
                                name="trung_tam_phu"
                                options={this.props.MergeClass?.list_trung_tam}
                                value={values.trung_tam_phu}
                                onChange={e => {
                                  const value = e.target.value;
                                  setFieldValue("trung_tam_phu", value);
                                  this.onChangeCenter(value, "phu");
                                }}
                                onBlur={handleBlur}
                              />
                            </div>
                          </div>
                        </div>

                        <div className="row align-center">
                          <div className="col-5">
                            <div className="form-group">
                              <label htmlFor="destClass">Lớp:</label>
                            </div>
                          </div>
                          <div className="col-7">
                            <div className="form-group">
                              <Select
                                id="destClass"
                                name="destClass"
                                options={
                                  this.props.MergeClass?.list_lop_hoc_phu
                                }
                                value={values.destClass}
                                onChange={e => {
                                  const value = e.target.value;
                                  setFieldValue("destClass", value);
                                  const selectedClass = this.props.MergeClass?.list_lop_hoc_phu.find(
                                    item => item.id == value
                                  );
                                  this.setState({
                                    lop_ghep: selectedClass
                                  });
                                }}
                                onBlur={handleBlur}
                              />
                            </div>
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-5">
                            <div className="form-group">
                              <label htmlFor="lop">
                                Thời khóa biểu của lớp:
                              </label>
                            </div>
                          </div>
                          <div className="col-7">
                            <ul>
                              {this.state?.lop_ghep?.scheduleTimes?.map(
                                (item, index) => (
                                  <li
                                    className="pb-2"
                                    key={`lop_ghep_${index}`}
                                  >
                                    <b>{item}</b>
                                  </li>
                                )
                              )}
                            </ul>
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-5">
                            <div className="form-group m-0">
                              <label htmlFor="lop">
                                Sỹ số hiện tại của lớp:
                              </label>
                            </div>
                          </div>
                          <div className="col-7">
                            <b>{this.state?.lop_ghep?.studentCount}</b>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="row justify-content-center">
                  <div className="col-md-6 col-sm-12 ">
                    <div className="iq-card">
                      <div className=" iq-card-header d-flex justify-content-between">
                        <div className="iq-header-title">
                          <h6>
                            <b>Lớp sau ghép</b>
                          </h6>
                        </div>
                      </div>

                      <div className="iq-card-body">
                        <div className="row align-center">
                          <div className="col-5">
                            <div className="form-group">
                              <label htmlFor="trung_tam_ghep">Trung tâm:</label>
                            </div>
                          </div>
                          <div className="col-7">
                            <div className="form-group">
                              <Select
                                id="trung_tam_ghep"
                                name="trung_tam_ghep"
                                options={this.props.MergeClass?.list_trung_tam}
                                value={values.trung_tam_ghep}
                                onChange={e => {
                                  const value = e.target.value;
                                  setFieldValue("trung_tam_ghep", value);
                                  this.onChangeCenter(value);
                                }}
                                onBlur={handleBlur}
                              />
                            </div>
                          </div>
                        </div>

                        <div className="row align-center">
                          <div className="col-5">
                            <div className="form-group">
                              <label htmlFor="mergeClass">Lớp:</label>
                            </div>
                          </div>
                          <div className="col-7">
                            <div className="form-group">
                              <Select
                                id="mergeClass"
                                name="mergeClass"
                                options={
                                  this.props.MergeClass?.list_lop_hoc_ghep
                                }
                                value={values.mergeClass}
                                onChange={e => {
                                  const value = e.target.value;
                                  setFieldValue("mergeClass", value);
                                  const selectedClass = this.props.MergeClass?.list_lop_hoc_ghep.find(
                                    item => item.id == value
                                  );
                                  this.setState({
                                    lop_dich: selectedClass
                                  });
                                }}
                                onBlur={handleBlur}
                              />
                            </div>
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-5">
                            <div className="form-group">
                              <label htmlFor="lop">
                                Thời khóa biểu của lớp:
                              </label>
                            </div>
                          </div>
                          <div className="col-7">
                            <ul>
                              {this.state?.lop_dich?.scheduleTimes?.map(
                                (item, index) => (
                                  <li
                                    className="pb-2"
                                    key={`lop_dich_${index}`}
                                  >
                                    <b>{item}</b>
                                  </li>
                                )
                              )}
                            </ul>
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-5">
                            <div className="form-group m-0">
                              <label htmlFor="lop">
                                Sỹ số hiện tại của lớp:
                              </label>
                            </div>
                          </div>
                          <div className="col-7">
                            <b>{this.state?.lop_dich?.studentCount}</b>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-12 text-center m-b-3">
                    <Button
                      variant="primary"
                      type="submit"
                      style={{ marginRight: 30 }}
                    >
                      Thực hiện ghép
                    </Button>
                    <Button
                      variant="secondary"
                      onClick={() => {
                        this.reset(resetForm, initialValues);
                      }}
                    >
                      Huỷ
                    </Button>
                  </div>
                </div>
              </form>
            )}
          </Formik>
        </div>
      </div>
    );
  }
}

export default connect(({ MergeClass, Global }) => ({
  MergeClass,
  Global
}))(Page);
