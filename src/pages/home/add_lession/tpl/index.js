import React from 'react';
import './index.css';
import './ax.css';

class Page extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
		<div class="" id="base" style={{"margin-left":"-150px"}}>
		   <div class="ax_default shape" id="u3177">
		      <div class="" id="u3177_div">
		      </div>
		      <div class="text" id="u3177_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default shape" id="u3178">
		      <div class="" id="u3178_div">
		      </div>
		      <div class="text" id="u3178_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default" data-height="40" data-label="account" data-left="13" data-top="895" data-width="192" id="u3180">
		      <div class="ax_default shape" data-label="accountLable" id="u3181">
		         <img class="img" id="u3181_img" src="images/account_information/accountlable_u55.svg"/>
		         <div class="text" id="u3181_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Nguyen Van A
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u3182">
		         <img class="img" id="u3182_img" src="images/login/u4.svg"/>
		         <div class="text" id="u3182_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="settingMenu" data-left="11" data-top="826" data-width="125" id="u3183">
		      <div class="ax_default shape" data-label="gearIconLable" id="u3184">
		         <img class="img" id="u3184_img" src="images/account_information/geariconlable_u58.svg"/>
		         <div class="text" id="u3184_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Cài đặt
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="44" data-left="11" data-top="826" data-width="44" id="u3185">
		         <div class="ax_default shape" data-label="gearIconBG" id="u3186">
		            <div class="" id="u3186_div">
		            </div>
		            <div class="text" id="u3186_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default shape" data-label="gearIconBG2" id="u3187">
		            <div class="" id="u3187_div">
		            </div>
		            <div class="text" id="u3187_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" data-label="gearIcon" id="u3188">
		            <img class="img" id="u3188_img" src="images/account_information/gearicon_u62.svg"/>
		            <div class="text" id="u3188_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="customerMenu" data-left="11" data-top="145" data-width="143" id="u3189">
		      <div class="ax_default shape" data-label="customerIconLable" id="u3190">
		         <img class="img" id="u3190_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u3190_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Khách hàng
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="customerIconBG" id="u3191">
		         <div class="" id="u3191_div">
		         </div>
		         <div class="text" id="u3191_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="customerIconBG2" id="u3192">
		         <div class="" id="u3192_div">
		         </div>
		         <div class="text" id="u3192_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" data-label="customer" id="u3193">
		         <img class="img" id="u3193_img" src="images/account_information/customer_u67.svg"/>
		         <div class="text" id="u3193_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="classMenu" data-left="11" data-top="198" data-width="143" id="u3194">
		      <div class="ax_default shape" data-label="classIconLable" id="u3195">
		         <img class="img" id="u3195_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u3195_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Lớp học
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG" id="u3196">
		         <div class="" id="u3196_div">
		         </div>
		         <div class="text" id="u3196_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG2" id="u3197">
		         <div class="" id="u3197_div">
		         </div>
		         <div class="text" id="u3197_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" data-label="classicon" id="u3198">
		         <img class="img" id="u3198_img" src="images/account_information/classicon_u72.svg"/>
		         <div class="text" id="u3198_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="dashboardMenu" data-left="11" data-top="91" data-width="143" id="u3199">
		      <div class="ax_default shape" data-label="dashboardIconLable" id="u3200">
		         <img class="img" id="u3200_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u3200_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Dashnoard
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="dashboardIconBG" id="u3201">
		         <div class="" id="u3201_div">
		         </div>
		         <div class="text" id="u3201_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="dashboardIconBG2" id="u3202">
		         <div class="" id="u3202_div">
		         </div>
		         <div class="text" id="u3202_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" data-label="dashboard" id="u3203">
		         <img class="img" id="u3203_img" src="images/account_information/dashboard_u77.svg"/>
		         <div class="text" id="u3203_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="129" data-label="AccountMenu" data-left="58" data-top="806" data-width="246" id="u3204" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default shape" id="u3205">
		         <div class="" id="u3205_div">
		         </div>
		         <div class="text" id="u3205_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u3206">
		         <div class="" id="u3206_div">
		         </div>
		         <div class="text" id="u3206_text">
		            <p>
		               <span style={{"font-family":"ArialMT, Arial","font-weight":"400","text-decoration":"none"}}>
		                  Xin chào:
		               </span>
		               <span style={{"font-family":"Arial Bold, Arial","font-weight":"700","text-decoration":"none"}}>
		                  Nguyen Van A
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3207">
		         <div class="ax_default image" id="u3208">
		            <img class="img" id="u3208_img" src="images/account_information/u82.png"/>
		            <div class="text" id="u3208_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u3209">
		            <div class="" id="u3209_div">
		            </div>
		            <div class="text" id="u3209_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Đăng xuất
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default line" id="u3210">
		         <img class="img" id="u3210_img" src="images/account_information/u84.svg"/>
		         <div class="text" id="u3210_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3211">
		         <div class="ax_default paragraph" id="u3212">
		            <div class="" id="u3212_div">
		            </div>
		            <div class="text" id="u3212_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tài khoản
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default image" id="u3213">
		            <img class="img" id="u3213_img" src="images/account_information/u87.png"/>
		            <div class="text" id="u3213_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="339" data-label="SettingPannel" data-left="58" data-top="525" data-width="339" id="u3214" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default shape" id="u3215">
		         <div class="" id="u3215_div">
		         </div>
		         <div class="text" id="u3215_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u3216">
		         <div class="" id="u3216_div">
		         </div>
		         <div class="text" id="u3216_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Hệ thống
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3217">
		         <div class="ax_default icon" id="u3218">
		            <img class="img" id="u3218_img" src="images/account_information/u92.svg"/>
		            <div class="text" id="u3218_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u3219">
		            <div class="" id="u3219_div">
		            </div>
		            <div class="text" id="u3219_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tài khoản
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3220">
		         <div class="ax_default paragraph" id="u3221">
		            <div class="" id="u3221_div">
		            </div>
		            <div class="text" id="u3221_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phân quyền
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u3222">
		            <img class="img" id="u3222_img" src="images/account_information/u96.svg"/>
		            <div class="text" id="u3222_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u3223">
		         <div class="" id="u3223_div">
		         </div>
		         <div class="text" id="u3223_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Đào tạo
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3224">
		         <div class="ax_default paragraph" id="u3225">
		            <div class="" id="u3225_div">
		            </div>
		            <div class="text" id="u3225_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Thương hiệu
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u3226">
		            <img class="img" id="u3226_img" src="images/account_information/u100.svg"/>
		            <div class="text" id="u3226_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3227">
		         <div class="ax_default paragraph" id="u3228">
		            <div class="" id="u3228_div">
		            </div>
		            <div class="text" id="u3228_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trung tâm
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u3229">
		            <img class="img" id="u3229_img" src="images/account_information/u103.svg"/>
		            <div class="text" id="u3229_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3230">
		         <div class="ax_default icon" id="u3231">
		            <img class="img" id="u3231_img" src="images/account_information/u105.svg"/>
		            <div class="text" id="u3231_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u3232">
		            <div class="" id="u3232_div">
		            </div>
		            <div class="text" id="u3232_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Ngày nghỉ
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3233">
		         <div class="ax_default icon" id="u3234">
		            <img class="img" id="u3234_img" src="images/account_information/u108.svg"/>
		            <div class="text" id="u3234_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u3235">
		            <div class="" id="u3235_div">
		            </div>
		            <div class="text" id="u3235_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phòng ban - Vai trò
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3236">
		         <div class="ax_default paragraph" id="u3237">
		            <div class="" id="u3237_div">
		            </div>
		            <div class="text" id="u3237_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trạng thái học viên
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u3238">
		            <img class="img" id="u3238_img" src="images/account_information/u112.svg"/>
		            <div class="text" id="u3238_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3239">
		         <div class="ax_default paragraph" id="u3240">
		            <div class="" id="u3240_div">
		            </div>
		            <div class="text" id="u3240_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tiêu chí đánh giá
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u3241">
		            <img class="img" id="u3241_img" src="images/account_information/u115.svg"/>
		            <div class="text" id="u3241_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3242">
		         <div class="ax_default paragraph" id="u3243">
		            <div class="" id="u3243_div">
		            </div>
		            <div class="text" id="u3243_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Chương trình học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3244">
		            <div class="ax_default icon" id="u3245">
		               <img class="img" id="u3245_img" src="images/account_information/u119.svg"/>
		               <div class="text" id="u3245_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3246">
		         <div class="ax_default icon" id="u3247">
		            <img class="img" id="u3247_img" src="images/account_information/u121.svg"/>
		            <div class="text" id="u3247_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u3248">
		            <div class="" id="u3248_div">
		            </div>
		            <div class="text" id="u3248_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Bài học
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3249">
		         <div class="ax_default paragraph" id="u3250">
		            <div class="" id="u3250_div">
		            </div>
		            <div class="text" id="u3250_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phòng học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u3251">
		            <img class="img" id="u3251_img" src="images/account_information/u125.svg"/>
		            <div class="text" id="u3251_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3252">
		         <div class="ax_default paragraph" id="u3253">
		            <div class="" id="u3253_div">
		            </div>
		            <div class="text" id="u3253_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trường học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u3254">
		            <img class="img" id="u3254_img" src="images/account_information/u128.svg"/>
		            <div class="text" id="u3254_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default line" id="u3255">
		         <img class="img" id="u3255_img" src="images/account_information/u129.svg"/>
		         <div class="text" id="u3255_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u3256">
		         <div class="" id="u3256_div">
		         </div>
		         <div class="text" id="u3256_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Tài chính
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3257">
		         <div class="ax_default paragraph" id="u3258">
		            <div class="" id="u3258_div">
		            </div>
		            <div class="text" id="u3258_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Học phí
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u3259">
		            <img class="img" id="u3259_img" src="images/account_information/u133.svg"/>
		            <div class="text" id="u3259_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3260">
		         <div class="ax_default paragraph" id="u3261">
		            <div class="" id="u3261_div">
		            </div>
		            <div class="text" id="u3261_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Khuyến mại
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u3262">
		            <img class="img" id="u3262_img" src="images/account_information/u136.svg"/>
		            <div class="text" id="u3262_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="classMenu" data-left="11" data-top="252" data-width="143" id="u3263">
		      <div class="ax_default shape" data-label="classIconLable" id="u3264">
		         <img class="img" id="u3264_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u3264_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Giáo viên
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG" id="u3265">
		         <div class="" id="u3265_div">
		         </div>
		         <div class="text" id="u3265_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG2" id="u3266">
		         <div class="" id="u3266_div">
		         </div>
		         <div class="text" id="u3266_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u3267">
		         <img class="img" id="u3267_img" src="images/account_information/u141.svg"/>
		         <div class="text" id="u3267_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u3268">
		      <img class="img" id="u3268_img" src="images/account_information/u142.svg"/>
		      <div class="text" id="u3268_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u3269">
		      <img class="img" id="u3269_img" src="images/account_information/u143.svg"/>
		      <div class="text" id="u3269_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div id="u3176" style={{"display":"none","visibility":"hidden"}}>
		   </div>
		   <div class="ax_default" data-height="55" data-label="Header" data-left="133" data-top="17" data-width="752" id="u3270">
		      <div class="ax_default paragraph" id="u3271">
		         <div class="" id="u3271_div">
		         </div>
		         <div class="text" id="u3271_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Thêm bài học
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u3272">
		         <div class="" id="u3272_div">
		         </div>
		         <div class="text" id="u3272_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Thêm mới bài học vào chương trình học
		               </span>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="224" data-left="133" data-top="106" data-width="1682" id="u3273">
		      <div class="ax_default box_1" id="u3274">
		         <div class="" id="u3274_div">
		         </div>
		         <div class="text" id="u3274_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default label" id="u3275">
		         <div class="" id="u3275_div">
		         </div>
		         <div class="text" id="u3275_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Thông tin chung
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="24" data-left="215" data-top="143" data-width="495" id="u3276">
		         <div class="ax_default label" id="u3277">
		            <img class="img" id="u3277_img" src="images/add_lession/u3277.svg"/>
		            <div class="text" id="u3277_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Chương trình
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default droplist" id="u3278">
		            <div class="" id="u3278_div">
		            </div>
		            <select class="u3278_input" id="u3278_input">
		               <option class="u3278_input_option" value=" ">
		               </option>
		               <option class="u3278_input_option" value="BIBOB 1">
		                  BIBOB 1
		               </option>
		               <option class="u3278_input_option" value="BIBOB 2">
		                  BIBOB 2
		               </option>
		               <option class="u3278_input_option" value="BIG ENGLIST PLUS 1">
		                  BIG ENGLIST PLUS 1
		               </option>
		               <option class="u3278_input_option" value="BIG ENGLIST PLUS 2">
		                  BIG ENGLIST PLUS 2
		               </option>
		               <option class="u3278_input_option" value="BIG ENGLIST PLUS 3">
		                  BIG ENGLIST PLUS 3
		               </option>
		            </select>
		         </div>
		      </div>
		      <div class="ax_default" data-height="25" data-left="215" data-top="185" data-width="495" id="u3279">
		         <div class="ax_default label" id="u3280">
		            <img class="img" id="u3280_img" src="images/add_lession/u3280.svg"/>
		            <div class="text" id="u3280_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tên bài học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default text_field" id="u3281">
		            <div class="" id="u3281_div">
		            </div>
		            <input class="u3281_input" id="u3281_input" type="text" value=""/>
		         </div>
		      </div>
		      <div class="ax_default label" id="u3282">
		         <img class="img" id="u3282_img" src="images/add_lession/u3282.svg"/>
		         <div class="text" id="u3282_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Mô tả ngắn
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default text_area" id="u3283">
		         <div class="" id="u3283_div">
		         </div>
		         <textarea class="u3283_input" id="u3283_input"></textarea>
		      </div>
		      <div class="ax_default checkbox" id="u3284">
		         <label for="u3284_input" id="u3284_input_label" style={{"position":" absolute","left":" 0px"}}>
		            <img class="img" id="u3284_img" src="images/add_lession/u3284.svg"/>
		            <div class="text" id="u3284_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Là buổi kiểm tra
		                  </span>
		               </p>
		            </div>
		         </label>
		         <input id="u3284_input" type="checkbox" value="checkbox"/>
		      </div>
		      <div class="ax_default" data-height="16" data-left="215" data-top="270" data-width="131" id="u3285">
		         <div class="ax_default label" id="u3286">
		            <img class="img" id="u3286_img" src="images/add_lession/u3286.svg"/>
		            <div class="text" id="u3286_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Các tiêu chi đánh giá
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default checkbox" id="u3287">
		         <label for="u3287_input" id="u3287_input_label" style={{"position":" absolute","left":" 0px"}}>
		            <img class="img" id="u3287_img" src="images/add_lession/u3287.svg"/>
		            <div class="text" id="u3287_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Giáo viên nước ngoài
		                  </span>
		               </p>
		            </div>
		         </label>
		         <input id="u3287_input" type="checkbox" value="checkbox"/>
		      </div>
		   </div>
		   <div class="ax_default image" id="u3288">
		      <img class="img" id="u3288_img" src="images/add_lession/u3288.png"/>
		      <div class="text" id="u3288_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default" data-height="454" data-left="133" data-top="350" data-width="1682" id="u3289">
		      <div class="ax_default box_1" id="u3290">
		         <div class="" id="u3290_div">
		         </div>
		         <div class="text" id="u3290_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default label" id="u3291">
		         <div class="" id="u3291_div">
		         </div>
		         <div class="text" id="u3291_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Chi tiết bài học
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default label" id="u3292">
		         <img class="img" id="u3292_img" src="images/add_lession/u3292.svg"/>
		         <div class="text" id="u3292_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Nội dung
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default text_area" id="u3293">
		         <div class="" id="u3293_div">
		         </div>
		         <textarea class="u3293_input" id="u3293_input"></textarea>
		      </div>
		      <div class="ax_default label" id="u3294">
		         <img class="img" id="u3294_img" src="images/add_lession/u3294.svg"/>
		         <div class="text" id="u3294_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Các file sử dụng
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" id="u3295">
		         <div class="" id="u3295_div">
		         </div>
		         <div class="text" id="u3295_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u3296">
		         <div class="" id="u3296_div">
		         </div>
		         <div class="text" id="u3296_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Thêm file bằng cách kéo thả
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default label" id="u3297">
		         <img class="img" id="u3297_img" src="images/add_lession/u3297.svg"/>
		         <div class="text" id="u3297_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Học liệu
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u3298">
		         <div class="" id="u3298_div">
		         </div>
		         <div class="text" id="u3298_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Chấp nhận file doc, docx, xls, xlsx, pdf, img, png. Dung lượng tối đa 150MB
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="64" data-left="410" data-top="704" data-width="1324" id="u3299">
		         <div class="ax_default" id="u3300">
		            <div class="ax_default table_cell" id="u3301">
		               <img class="img" id="u3301_img" src="images/add_lession/u3301.png"/>
		               <div class="text" id="u3301_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Học liệu
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u3302">
		               <img class="img" id="u3302_img" src="images/add_lession/u3302.png"/>
		               <div class="text" id="u3302_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Số lượng
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u3303">
		               <img class="img" id="u3303_img" src="images/add_lession/u3303.png"/>
		               <div class="text" id="u3303_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Phân phối
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u3304">
		               <img class="img" id="u3304_img" src="images/add_lession/u3304.png"/>
		               <div class="text" id="u3304_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Hoàn
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u3305">
		               <img class="img" id="u3305_img" src="images/add_lession/u3305.png"/>
		               <div class="text" id="u3305_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Thao tác
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u3306">
		               <img class="img" id="u3306_img" src="images/add_lession/u3306.png"/>
		               <div class="text" id="u3306_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u3307">
		               <img class="img" id="u3307_img" src="images/add_lession/u3307.png"/>
		               <div class="text" id="u3307_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u3308">
		               <img class="img" id="u3308_img" src="images/add_lession/u3308.png"/>
		               <div class="text" id="u3308_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u3309">
		               <img class="img" id="u3309_img" src="images/add_lession/u3309.png"/>
		               <div class="text" id="u3309_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u3310">
		               <img class="img" id="u3310_img" src="images/add_lession/u3310.png"/>
		               <div class="text" id="u3310_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		         </div>
		         <div class="ax_default checkbox" id="u3311">
		            <label for="u3311_input" id="u3311_input_label" style={{"position":" absolute","left":" 0px"}}>
		               <img class="img" id="u3311_img" src="images/add_lession/u3311.svg"/>
		               <div class="text" id="u3311_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </label>
		            <input id="u3311_input" type="checkbox" value="checkbox"/>
		         </div>
		         <div class="ax_default droplist" id="u3312">
		            <div class="" id="u3312_div">
		            </div>
		            <select class="u3312_input" id="u3312_input">
		               <option class="u3312_input_option" selected="" value="Vui lòng chọn 1 giá trị">
		                  Vui lòng chọn 1 giá trị
		               </option>
		               <option class="u3312_input_option" value="Cả lớp">
		                  Cả lớp
		               </option>
		               <option class="u3312_input_option" value="Theo học viên">
		                  Theo học viên
		               </option>
		            </select>
		         </div>
		      </div>
		      <div class="ax_default label" id="u3313">
		         <img class="img" id="u3313_img" src="images/add_lession/u3313.svg"/>
		         <div class="text" id="u3313_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Bài về nhà
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default text_area" id="u3314">
		         <div class="" id="u3314_div">
		         </div>
		         <textarea class="u3314_input" id="u3314_input"></textarea>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u3315">
		      <div class="" id="u3315_div">
		      </div>
		      <div class="text" id="u3315_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Lưu
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default button" id="u3316">
		      <div class="" id="u3316_div">
		      </div>
		      <div class="text" id="u3316_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Hủy
		            </span>
		         </p>
		      </div>
		   </div>
		</div>
		
      </div>
    );
  }
}

export default Page;
