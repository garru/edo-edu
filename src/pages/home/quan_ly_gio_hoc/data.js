export const TITLE = "Quản lý giờ học";
export const BUTTON = [
  "Tạo giờ học"
];

export const HEADER = [
  {
    title: "Tên",
    key: "ten",
    class: "tb-width-200"
  },
  {
    title: "Ngày trong tuần",
    key: "ngay_trong_tuan",
    class: "tb-width-350"
  },
  {
    title: "Giờ bắt đầu",
    key: "gio_bat_dau",
    class: "tb-width-150"
  },
  {
    title: "Giờ kết thúc",
    key: "gio_ket_thuc",
    class: "tb-width-150"
  }
];


export const DATA = [
  {
    id: "1",
    ten: "Giờ học 1 - trong tuần",
    ngay_trong_tuan: "Thứ 2, Thứ 3, Thứ 4, Thứ 5, Thứ 6",
    gio_bat_dau: "17:00",
    gio_ket_thuc: "18:30"
  },
  {
    id: "1",
    ten: "Giờ học 1 - trong tuần",
    ngay_trong_tuan: "Thứ 2, Thứ 3, Thứ 4, Thứ 5, Thứ 6",
    gio_bat_dau: "17:00",
    gio_ket_thuc: "18:30"
  },
  {
    id: "1",
    ten: "Giờ học 1 - trong tuần",
    ngay_trong_tuan: "Thứ 2, Thứ 3, Thứ 4, Thứ 5, Thứ 6",
    gio_bat_dau: "17:00",
    gio_ket_thuc: "18:30"
  },
  {
    id: "1",
    ten: "Giờ học 1 - trong tuần",
    ngay_trong_tuan: "Thứ 2, Thứ 3, Thứ 4, Thứ 5, Thứ 6",
    gio_bat_dau: "17:00",
    gio_ket_thuc: "18:30"
  },
  {
    id: "1",
    ten: "Giờ học 1 - trong tuần",
    ngay_trong_tuan: "Thứ 2, Thứ 3, Thứ 4, Thứ 5, Thứ 6",
    gio_bat_dau: "17:00",
    gio_ket_thuc: "18:30"
  },
];
