import React from "react";
import {connect} from "dva";
import {Button} from "react-bootstrap";
import CustomModal from "./modal";
import PageHeader from "../../../components/PageHeader";
import CustomTable from "../../../components/Table";
import CustomTooltip from "../../../components/CustomTooltip";
import useTeachingTime from "../../../hooks/useTechingTime";

class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id: null,
      tooltipIndex: null,
      show: false,
      target: null,
      activeIndex: null
    };
  }

  hooks = useTeachingTime({ dispatch: this.props.dispatch })

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: "TeachingTime/list",
    })
    dispatch({
      type: "TeachingTime/listcenter",
    })
  }

  renderTooltip(index) {
    const {tooltipIndex} = this.state;
    const {dispatch} = this.props;
    const dataSource = [{
      label: "Chỉnh sửa",
      className: "fa fa-edit",
      onClick: e => {
        Promise.all([dispatch({
          type: "TeachingTime/view",
          payload: {id: this.state.id}
        })]).then(() => {
          this.setState({isCreatNew: false, showModal: true});
        });
        this.handleClick(e, tooltipIndex, true);
      }
    }, {
      label: "Xóa",
      className: "fa fa-trash-alt",
      onClick: e => this.hooks._delete(this.state.id)
    }]
    return (
      <CustomTooltip
        show={this.state.show}
        onHide={e => this.handleClick(e, tooltipIndex)}
        dataSource={dataSource}
        target={this.state.target}
      />
    );
  }

  handleClick(e, index, isHide) {
    this.setState({
      show: !(index === this.state.activeIndex),
      target: isHide ? null : e.target,
      activeIndex: index === this.state.activeIndex ? null : index
    });
  }

  onClose = () => {
    this.setState({isCreateNew: false, showModal: false});
  };

  columns = [{
      title: "Tên",
      key: "name",
      class: "tb-width-200"
    },
    {
      title: "Ngày trong tuần",
      key: "weekDay",
      class: "tb-width-350"
    },
    {
      title: "Giờ bắt đầu",
      key: "timeBegin",
      class: "tb-width-150"
    },
    {
      title: "Giờ kết thúc",
      key: "timeEnd",
      class: "tb-width-150"
    }, {
      title: "",
      key: "action",
      class: "tb-width-50",
      render: (col, index) => (
        <i className="fa fa-ellipsis-h" onClick={e => {
          e.preventDefault();
          this.handleClick(e, index);
          this.setState({id: col.id, tooltipIndex: index})
        }}/>
      )
    }]

  bre = {};

  render() {
    return (
      <>
        <PageHeader
          title="Quản lý giờ học"
          breadcrums={[
            {
              title: "Home",
              path: "/"
            },
            {
              title: "Quản lý giờ học",
              path: ""
            }
          ]}/>
        <div className="iq-card">
          <div className="iq-card-body">
            <div className="row">
              <div className="col-12 text-right">
                <div className="form-group">
                  <Button
                    variant="primary"
                    type="submit"
                    onClick={() =>
                      this.setState({isCreateNew: true, showModal: true})
                    }>
                    Tạo giờ học
                  </Button>
                </div>
              </div>
              <div className="col-12">
                <CustomTable
                  dataSource={this.props.TeachingTime.list_teaching_time}
                  total={this.props.TeachingTime.total_record}
                  columns={this.columns}
                  onChange={data => this.updateOptions(data)}
                  checkbox
                />
              </div>
            </div>
          </div>
          {this.state.showModal && (
            <CustomModal
              isCreateNew={this.state.isCreateNew}
              onClose={this.onClose}
              selectedItem={this.state.selectedItem}
              TeachingTime={this.props.TeachingTime}
            />
          )}
          {this.renderTooltip()}
        </div>
      </>
    );
  }
}

export default connect(({TeachingTime}) => ({
  TeachingTime
}))(Page);
