import React from "react";
import {Button, Modal} from "react-bootstrap";
import {GIO_HOC} from "../../../mock/dropdown";
import {ErrorMessage, Formik} from "formik";
import Select from "../../../components/Form/Select";
import useTeachingTime from "../../../hooks/useTechingTime";
import Input from "../../../components/Form/Input";
import Checkbox from "../../../components/Form/Checkbox";
import {connect} from "dva";

function CustomModal(props) {
  const handleClose = () => {
    props.onClose();
  };
  const {isCreateNew} = props;
  const {initialValues, validationSchema, submit} = useTeachingTime({
    dispatch: props.dispatch,
    onClose: props.onClose,
    isCreateNew
  })
  return (
    <Modal
      size={"lg"}
      show
      onHide={handleClose}
      backdrop="static"
      keyboard={false}
      id="AddNew"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Formik
        initialValues={!isCreateNew ? props.TeachingTime?.current_teaching_time : initialValues}
        validationSchema={validationSchema}
        onSubmit={submit}
      >
        {({
            values, handleChange, handleBlur, handleSubmit, setFieldValue, setFieldTouched
          }) => (
          <form onSubmit={handleSubmit}>
            <Modal.Header closeButton>
              <Modal.Title>
                {isCreateNew ? "Tạo giờ học mới" : "Chỉnh sửa giờ học"}
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <div>
                <form>
                  <div className="row">
                    <div className="col-12">
                      <Select
                        id="centerId"
                        name="centerId"
                        value={values.centerId}
                        options={props.TeachingTime.list_center}
                        onChange={handleChange}
                        onBlur={handleBlur}
                      />
                      <Input
                        id="name"
                        name="name"
                        label="Tên giờ học:"
                        value={values.name}
                        onChange={handleChange}
                        onBlur={handleBlur}
                      />
                      <div className="row">
                        <div className="col-2">
                          <div className="form-group">
                            <label htmlFor="gio_bat_dau">Thời gian:</label>
                          </div>
                        </div>
                        <div className="col-3">
                          <div className="form-group">
                            <input
                              type="time"
                              name="timeBegin"
                              className="form-control form-control-lg"
                              id="timeBegin"
                              min="00:00"
                              max="23:59"
                              value={values.timeBegin}
                              onChange={handleChange}
                              onBlur={handleBlur}
                            />
                            <ErrorMessage component="p" className="error-message" name="timeBegin" />
                          </div>
                        </div>
                        <div className="col-3">
                          <div className="form-group">
                            <input
                              type="time"
                              name="timeEnd"
                              className="form-control form-control-lg"
                              id="timeEnd"
                              min="00:00"
                              max="23:59"
                              value={values.timeEnd}
                              onChange={handleChange}
                              onBlur={handleBlur}
                            />
                            <ErrorMessage component="p" className="error-message" name="timeEnd" />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-12">
                      <ul className="vertical m-0">
                        {GIO_HOC.map((item) => {
                          return (
                            <li>
                              <Checkbox
                                id={item.name}
                                name={item.name}
                                label={item.text}
                                value={values[item.name]}
                                onChange={handleChange}
                                onBlur={handleBlur}
                              />
                            </li>
                          );
                        })}
                      </ul>
                    </div>
                  </div>
                </form>
              </div>
            </Modal.Body>
            <Modal.Footer>
              <Button type="submit" variant="primary" style={{marginRight: 15}}>
                {isCreateNew ? "Tạo mới" : "Cập nhật"}
              </Button>
              <Button type="button" variant={"secondary"} onClick={handleClose}>
                Hủy
              </Button>
            </Modal.Footer>
          </form>
        )}
      </Formik>
    </Modal>
  );
}

export default connect(({TeachingTime}) => ({
  TeachingTime
}))(CustomModal);