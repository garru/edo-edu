import React from "react";
import { connect } from "dva";
import trungtam from "../../../mock/trungtam";
import loaihoadon from "../../../mock/loaihoadon";
import mucdich from "../../../mock/mucdich";
import goihocphi from "../../../mock/goihocphi";
import taikhoanthu from "../../../mock/taikhoanthu";
import nguoithu from "../../../mock/nguoithu";
import lophoc from "../../../mock/lophoc";
import buoihoc from "../../../mock/buoihoc";
import moment from "moment";
import PageHeader from "../../../components/PageHeader";

const Page = () => {
  return (
    <div>
      <PageHeader
        title=""
        breadcrums={[{

        }]}
        subTitle=""/>
      <div className="iq-card ">
        <div className="iq-card-body">
          <div className="row ">
            <div className="col-12">
              <h1 className="card-title font-bold">Chi tiết hóa đơn</h1>
            </div>
          </div>
          <div className="">
            <div className="row ">
              <div className="col-8">
                <div className="row">
                  <div className="col-12 mb-2">
                    <div className=" iq-card-header d-flex justify-content-between">
                      <div className="iq-header-title">
                        <h6>
                          <b>Trung tâm</b>
                        </h6>
                      </div>
                    </div>
                  </div>
                  <div className="col-4">
                    <div className="form-group">
                      <label htmlFor="co_so">Trung tâm</label>
                      <select className="form-control form-control-lg" id="co_so" name="co_so">
                        {trungtam.map(item => (
                          <option value={item.value}>{item.name}</option>
                        ))}
                      </select>
                    </div>
                  </div>
                  <div className="col-4">
                    <div className="form-group">
                      <label htmlFor="co_so">Loại hóa đơn</label>
                      <select
                        className="form-control form-control-lg"
                        id="loai_hoa_don"
                        name="loai_hoa_don"
                      >
                        {loaihoadon.map(item => (
                          <option value={item.value}>{item.name}</option>
                        ))}
                      </select>
                    </div>
                  </div>
                  <div className="col-4">
                    <div className="form-group">
                      <label htmlFor="co_so">Mục đích</label>
                      <select
                        className="form-control form-control-lg"
                        id="muc_dich"
                        name="muc_dich"
                      >
                        {mucdich.map(item => (
                          <option value={item.value}>{item.name}</option>
                        ))}
                      </select>
                    </div>
                  </div>
                  <div className="col-4">
                    <div className="form-group">
                      <label htmlFor="email">Mã hóa đơn liên quan</label>
                      <input
                        type="text"
                        id="email"
                        name="email"
                        className="form-control form-control-lg"
                      />
                      <div className="text-nowrap">Sử dụng khi có đặt cọc</div>
                    </div>
                  </div>
                  <div className="col-4">
                    <div className="form-group">
                      <label htmlFor="co_so">Gói học phí</label>
                      <select
                        className="form-control form-control-lg"
                        id="goi_hoc_phi"
                        name="goi_hoc_phi"
                      >
                        {goihocphi.map(item => (
                          <option value={item.value}>{item.name}</option>
                        ))}
                      </select>
                    </div>
                  </div>
                  <div className="col-4">
                    <div className="form-group">
                      <label htmlFor="email">Người nộp</label>
                      <input
                        type="text"
                        id="nguoi_nop"
                        name="nguoi_nop"
                        className="form-control form-control-lg"
                      />
                    </div>
                  </div>
                  <div className="col-4">
                    <div className="form-group">
                      <label htmlFor="tai_khoan_thu">Tài khoản thu</label>
                      <select
                        className="form-control form-control-lg"
                        id="tai_khoan_thu"
                        name="tai_khoan_thu"
                      >
                        {taikhoanthu.map(item => (
                          <option value={item.value}>{item.name}</option>
                        ))}
                      </select>
                    </div>
                  </div>
                  <div className="col-12">
                    <div className="form-group">
                      <label htmlFor="noi_dung">Nội dung</label>
                      <textarea
                        id="noi_dung"
                        name="noi_dung"
                        className="form-control form-control-lg"
                        rows="4"
                      />
                    </div>
                  </div>
                  <div className="col-4">
                    <div className="form-group">
                      <label htmlFor="nguoi_thu">Người thu</label>
                      <select
                        className="form-control form-control-lg"
                        id="nguoi_thu"
                        name="nguoi_thu"
                      >
                        {nguoithu.map(item => (
                          <option value={item.value}>{item.name}</option>
                        ))}
                      </select>
                    </div>
                  </div>
                  <div className="col-4">
                    <div className="form-group">
                      <label htmlFor="lop_hoc">Lớp học</label>
                      <select
                        className="form-control form-control-lg"
                        id="lop_hoc"
                        name="lop_hoc"
                      >
                        {lophoc.map(item => (
                          <option value={item.value}>{item.name}</option>
                        ))}
                      </select>
                    </div>
                  </div>
                  <div className="col-4">
                    <div className="form-group">
                      <label htmlFor="buoi_hoc">Buồi học</label>
                      <select
                        className="form-control form-control-lg"
                        id="buoi_hoc"
                        name="buoi_hoc"
                      >
                        {buoihoc.map(item => (
                          <option value={item.value}>{item.name}</option>
                        ))}
                      </select>
                    </div>
                  </div>
                  <div className="col-4">
                    <div className="form-group">
                      <label htmlFor="ngay_tao">Ngày tạo</label>
                      <input
                        type="date"
                        id="ngay_tao"
                        name="ngay_tao"
                        className="form-control form-control-lg"
                        defaultValue={moment().format("YYYY-MM-DD")}
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-4">
                <div className="row">
                  <div className="col-12 mb-2">
                    <div className=" iq-card-header d-flex justify-content-between">
                      <div className="iq-header-title">
                        <h6>
                          <b>Loại hóa đơn</b>
                        </h6>
                      </div>
                    </div>
                  </div>
                  <div className="col-12">
                    <div className="form-group row">
                      <label htmlFor="so_tien" className="col-sm-4">
                        Số tiền
                      </label>
                      <div className="col-sm-8">
                        <input
                          type="text"
                          id="so_tien"
                          name="so_tien"
                          className="form-control form-control-lg"
                        />
                      </div>
                    </div>
                    <div className="form-group row">
                      <label htmlFor="giam_tru" className="col-sm-3">
                        Giảm trừ
                      </label>
                      <div className="col-sm-3 text-nowrap">Số buổi: 5</div>
                      <div className="col-sm-3 d-flex justify-content-end">
                        200.000
                      </div>
                      <div className="col-sm-3 d-flex justify-content-end">
                        1.000.000
                      </div>
                      <div className="col-sm-6"></div>
                      <div className="col-sm-3 d-flex justify-content-between">
                        <i className="fas fa-gift" />
                        <select
                          name="loai_giam_tru"
                          id="loai_giam_tru"
                          className="w-75"
                        >
                          <option value="phan_tram">%</option>
                          <option value="vnd">VND</option>
                        </select>
                      </div>
                      <div className="col-sm-3">
                        <input
                          type="text"
                          id="giam_tru"
                          name="giam_tru"
                          className="form-control form-control-lg"
                        />
                      </div>
                    </div>
                    <div className="form-group row">
                      <label htmlFor="phu_phi" className="col-sm-6">
                        Phụ phí
                      </label>
                      <div className="col-sm-3 d-flex justify-content-between">
                        <i className="fas fa-dollar-sign" />
                        <select
                          name="loai_phu_phi"
                          id="loai_giam_tru"
                          className="w-75"
                        >
                          <option value="phan_tram">%</option>
                          <option value="vnd">VND</option>
                        </select>
                      </div>
                      <div className="col-sm-3">
                        <input
                          type="text"
                          id="phu_phi"
                          name="phu_phi"
                          className="form-control form-control-lg"
                        />
                      </div>
                    </div>
                    <div className="form-group row">
                      <label htmlFor="so_tien_can_thu" className="col-sm-4">
                        Số tiền cần thu
                      </label>
                      <div className="col-sm-8">
                        <input
                          type="text"
                          id="so_tien_can_thu"
                          name="so_tien_can_thu"
                          className="form-control form-control-lg"
                        />
                      </div>
                    </div>
                    <div className="form-group row">
                      <label htmlFor="so_tien_da_thu" className="col-sm-4">
                        Số tiền đã thu
                      </label>
                      <div className="col-sm-8">
                        <input
                          type="text"
                          id="so_tien_da_thu"
                          name="so_tien_da_thu"
                          className="form-control form-control-lg"
                        />
                      </div>
                    </div>
                    <div className="form-group row">
                      <label htmlFor="so_tien_thuc_thu" className="col-sm-4">
                        Số tiền thực thu
                      </label>
                      <div className="col-sm-8">
                        <input
                          type="text"
                          id="so_tien_thuc_thu"
                          name="so_tien_thuc_thu"
                          className="form-control form-control-lg"
                        />
                      </div>
                    </div>
                  </div>
                  <div className="col-12 d-flex justify-content-end">
                    <button className="btn btn-primary">Đóng</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default connect(({ Mock }) => ({
  Mock
}))(Page);
