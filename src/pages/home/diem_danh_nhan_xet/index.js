import React from "react";
import { connect } from "dva";
import DiemDanhNhanXetSearch from "./diem_danh_nhan_xet_search";
import DiemDanhNhanXetBody from "./diem_danh_nhan_xet_body";
import PageHeader from "../../../components/PageHeader";
import isEmpty from "lodash/isEmpty";

class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    console.log("useParams", this.props.match.params.id);
  }

  componentWillMount() {
    const id = this.props.match.params.id;
    const { dispatch } = this.props;
    dispatch({
      type: "Global/showLoading"
    });
    Promise.all([
      dispatch({
        type: "ScheduleAttendance/view",
        payload: {
          id
        }
      }),
      dispatch({
        type: "ScheduleAttendance/liststatus"
      })
    ]).then(data => {
      dispatch({
        type: "Global/hideLoading"
      });
      if (!data) {
        dispatch({
          type: "Global/hideLoading"
        });
      }
    });
  }

  render() {
    const { current } = this.props.ScheduleAttendance;
    const id = this.props.match.params.id;
    const empty = isEmpty(current);
    const title = `Điểm danh của lớp ${this.props.ScheduleAttendance?.current.className}`;
    const bre = {
      title: title,
      breadcrums: [
        {
          title: "Home",
          path: "/"
        },
        {
          title: title,
          path: ""
        }
      ]
    };
    return (
      <React.Fragment>
        {!empty && (
          <>
            <PageHeader {...bre}></PageHeader>
            <DiemDanhNhanXetSearch />
            <DiemDanhNhanXetBody id={id}/>
          </>
        )}
      </React.Fragment>
    );
  }
}

export default connect(({ ScheduleAttendance, Global }) => ({
  ScheduleAttendance,
  Global
}))(Page);
