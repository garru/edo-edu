import React, { Component, useState, useRef, useEffect } from "react";
import { Modal, Button, Table } from "react-bootstrap";
import Rating from "react-rating";
import { connect } from "dva";
import CustomTable from "../../../../components/Table";

function CustomModal(props) {
  let timer;
  const [show, setShow] = useState(true);

  const handleClose = () => {
    // setShow(false);
    props.onClose();
  };

  const updateComment = (e, index) => {
    const { dispatch } = props;
    e.persist();
    console.log(e);
    clearTimeout(timer);
    timer = setTimeout(() => {
      dispatch({
        type: "ScheduleAttendance/updatecommentstudent",
        payload: {
          comment: e.target?.value,
          index: index
        }
      });
    }, 300);
  };

  const updateRating = (value, index) => {
    console.log("updateRating", value, index);
    const { dispatch } = props;
    dispatch({
      type: "ScheduleAttendance/updaterating",
      payload: {
        rating: value * 2,
        index: index
      }
    });
  };

  const updateStudent = () => {
    const { dispatch, ScheduleAttendance, studentId, id } = props;
    dispatch({
      type: "Global/showLoading"
    });

    const criteria = ScheduleAttendance.criteria_list?.map(
      ({ criteriaId, point, comment }) => ({
        criteriaId,
        point,
        comment
      })
    );

    dispatch({
      type: "ScheduleAttendance/commentupdate",
      payload: {
        criteria_List: criteria,
        id: +id,
        student_Id: +studentId
      }
    }).then(data => {
      dispatch({
        type: "Global/hideLoading"
      });
      if (data) {
        dispatch({
          type: "Global/showSuccess"
        });
        props.onClose();
      } else {
        dispatch({
          type: "Global/showError"
        });
      }
    });
  };

  const columns = [
    {
      title: "Kỹ năng - Tiêu chí",
      key: "criteriaName",
      class: "tb-width-200",
      render: (col, index) => (
        <div className="text-left">
          <span style={{ paddingLeft: col.isChild ? 15 : 0 }}>
            {col.criteriaName}
          </span>
        </div>
      )
    },
    {
      title: "Đánh giá",
      key: "rating",
      class: "tb-width-200",
      render: (col, index) => (
        <Rating
          className="rating"
          emptySymbol="far fa-star"
          fullSymbol="fa fa-star"
          fractions={2}
          onChange={value => updateRating(value, index)}
          initialRating={col.point / 2}
        />
      )
    },
    {
      title: "Nhận xét",
      key: "nhan_xet",
      class: "tb-width-200",
      render: (col, index) => (
        <div className="form-group text-left m-0">
          <textarea
            style={{ width: "100%" }}
            id={`nhan_xet_${index}`}
            name={`nhan_xet_${index}`}
            rows="3"
            defaultValue={col.comment}
            onChange={e => updateComment(e, index)}
            placeholder="Nhập nhận xét"
          />
        </div>
      )
    }
  ];
  return (
    <Modal
      size={"lg"}
      show={show}
      onHide={handleClose}
      backdrop="static"
      keyboard={false}
      id="AddNew"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title>
          Nhận xét học viên {props.ScheduleAttendance?.student_name}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <CustomTable
          dataSource={props.ScheduleAttendance?.criteria_list}
          columns={columns}
          onChange={data => {}}
          noPaging
        ></CustomTable>
      </Modal.Body>
      <Modal.Footer>
        <Button
          variant="primary"
          style={{ marginRight: 15 }}
          onClick={() => updateStudent()}
        >
          Cập nhật
        </Button>
        <Button variant={"secondary"} onClick={handleClose}>
          Hủy
        </Button>
      </Modal.Footer>
    </Modal>
  );
}

export default connect(({ ScheduleAttendance, Global }) => ({
  ScheduleAttendance,
  Global
}))(CustomModal);
