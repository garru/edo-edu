import React from "react";
import { connect } from "dva";
import { Button } from "react-bootstrap";
import Rating from "react-rating";
import * as _ from "lodash";
import CustomModal from "./modal";
import CustomTable from "../../../../components/Table";

let timer;
class Page extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      id: null,
      activeIndex: null,
      isCreateNew: false,
      target: null,
      show: false,
      tootlTipindex: null,
      showModal: false
    };
  }

  handleClick = (e, index, isHide) => {
    let { show, activeIndex, listItem } = this.state;
    show[index] = !show[index];
    index !== activeIndex && (show[activeIndex] = false);
    this.setState({
      show: show,
      target: isHide ? null : e.target,
      activeIndex: index,
      selectedItem: listItem[index]
    });
  };

  onFilter = code => {
    const { dispatch } = this.props;
    dispatch({
      type: "ScheduleAttendance/filtersearch",
      payload: {
        status: code
      }
    });
  };

  onClose = () => {
    this.setState({ showModal: false, studentId: "" });
  };

  updateValue = (e, student) => {
    const id = this.props.id;
    const { dispatch } = this.props;
    dispatch({
      type: "Global/showLoading"
    });
    dispatch({
      type: "ScheduleAttendance/updatestatus",
      payload: {
        id: +id,
        studentId: +student.id,
        attendanceStatus: +e.target.value
      }
    }).then(data => {
      if (!data) {
        dispatch({
          type: "Global/hideLoading"
        });
      } else {
        dispatch({
          type: "ScheduleAttendance/view",
          payload: {
            id: +id
          }
        }).then(res => {
          dispatch({
            type: "Global/hideLoading"
          });
          if (!res) {
            dispatch({
              type: "Global/showError"
            });
          } else {
            dispatch({
              type: "Global/showSuccess"
            });
            dispatch({
              type: "ScheduleAttendance/filtersearch",
              payload: {}
            });
          }
        });
      }
    });
  };

  onSearch = text => {
    clearTimeout(timer);
    timer = setTimeout(() => {
      const { dispatch } = this.props;
      dispatch({
        type: "ScheduleAttendance/filtersearch",
        payload: {
          search: text
        }
      });
    }, 300);
  };

  onRating = (value, col) => {
    const id = this.props.id;
    const { dispatch } = this.props;
    dispatch({
      type: "Global/showLoading"
    });
    dispatch({
      type: "ScheduleAttendance/commentupdate",
      payload: {
        id
        // studentId: col.id,
        // attendanceStatus: +e.target.value
      }
    }).then(data => {
      dispatch({
        type: "Global/hideLoading"
      });
      if (!data) {
        dispatch({
          type: "Global/hideLoading"
        });
      }
    });
  };

  onShowModal = studentId => {
    const id = this.props.id;
    const { dispatch } = this.props;
    this.setState({ studentId: studentId}); // To be Check
    dispatch({
      type: "Global/showLoading"
    });
    dispatch({
      type: "ScheduleAttendance/commentview",
      payload: {
        id,
        studentId: studentId // To be Check
      }
    }).then(data => {
      dispatch({
        type: "Global/hideLoading"
      });
      if (!data) {
        dispatch({
          type: "Global/hideLoading"
        });
      } else {
        this.setState({ showModal: true });
      }
    });
  };

  onFinish = () => {
    const id = this.props.id;
    const { dispatch } = this.props;
    dispatch({
      type: "Global/showLoading"
    });
    dispatch({
      type: "ScheduleAttendance/finish",
      payload: {
        id
      }
    }).then(data => {
      dispatch({
        type: "Global/hideLoading"
      });
      if (!data) {
        dispatch({
          type: "Global/showError"
        });
      } else {
        dispatch({
          type: "Global/showSuccess"
        });
        window.location.pathname = "/danh_sach_lop";
      }
    });
  };

  render = () => {
    const { ScheduleAttendance } = this.props;
    const columns = [
      //   {
      //     title: "No.",
      //     key: "code",
      //     class: "tb-width-50",
      //     render: (col, index) => <span>{index}</span>
      //   },
      {
        title: "Họ đệm",
        key: "firstName",
        class: "tb-width-150"
      },
      {
        title: "Tên",
        key: "lastName",
        class: "tb-width-150"
      },
      {
        title: "Nguồn",
        key: "sourceType",
        class: "tb-width-100"
      },
      {
        title: "Điểm danh",
        key: "diem_danh",
        class: "tb-width-200",
        render: (col, index) => {
          return (
            <select
              className="form-control form-control-lg"
              id={`diem_danh${index}`}
              name={`diem_danh${index}`}
              // defaultChecked={col.attendanceStatus}
              value={col.attendanceStatus}
              onChange={e => this.updateValue(e, col)}
              key={`diem_danh${index}`}
            >
              {ScheduleAttendance.list_status?.map(item => (
                <option value={item.id} key={`list_status_${item.id}`}>
                  {item.name}
                </option>
              ))}
            </select>
          );
        }
      },
      {
        title: "Nhận xét",
        key: "commentPoint",
        class: "tb-width-200",
        render: (col, index) => (
          <Rating
            key={`comemntPoint_${index}`}
            className="rating"
            emptySymbol="far fa-star"
            fullSymbol="fa fa-star"
            fractions={2}
            initialRating={col.commentPoint}
            // onChange={value => this.onRating(value, col)}
            // readonly={true}
            onChange={() => this.onShowModal(col.id)}
          />
        )
      }
    ];

    return (
      <div className="iq-card">
        <div className=" iq-card-header d-flex justify-content-between">
          <div className="iq-header-title">
            <h6>
              <b>Thông tin chung</b>
            </h6>
          </div>
        </div>
        <div className="iq-card-body">
          <div className="row">
            <div className="col-3">
              <div className="form-group">
                <p>
                  Tổng sỹ số buổi:{" "}
                  <b>{ScheduleAttendance.student_summary?.totalStudent}</b>
                </p>
              </div>
            </div>
            <div className="col-3">
              <div className="form-group">
                <p>
                  Số học sinh bù/học thử:
                  <b>{ScheduleAttendance.student_summary?.notInClass}</b>
                </p>
              </div>
            </div>
            <div className="col-3">
              <div className="form-group">
                <p>
                  Số học sinh tại lớp:{" "}
                  <b>{ScheduleAttendance.student_summary?.inClass}</b>
                </p>
              </div>
            </div>
            {ScheduleAttendance.student_summary?.statusSummary?.map(
              (item, index) => (
                <div className="col-3" key={`statusSummary_${index}`}>
                  <div className="form-group">
                    <p>
                      {item.name}:<b>{item.value}</b>
                    </p>
                  </div>
                </div>
              )
            )}
          </div>
          <div className="row align-center">
            <div className="col-lg-2 col-md-4 col-sm-12">
              <div className="form-group">
                <label htmlFor="ma_hoc_vien">Tên/Mã học viên:</label>
                <input
                  type="text"
                  name="ma_hoc_vien"
                  className="form-control form-control-lg"
                  id="ma_hoc_vien"
                  // ref={register({ required: true, minLength: 8 })}
                ></input>
              </div>
            </div>
            <div className="col-lg-7 col-md-8 col-sm-12">
              <div className="status-group">
                <div className={`All status`} onClick={() => this.onFilter("")}>
                  <span>Toàn bộ</span>
                </div>
                {ScheduleAttendance?.list_status?.map(item => (
                  <div
                    className={`${item.code} status`}
                    key={`list_status_${item.id}`}
                    onClick={() => this.onFilter(item.id)}
                  >
                    <span>{item.name}</span>
                  </div>
                ))}
              </div>
            </div>

            <div
              className="col-lg-3 col-md-12 col-sm-12"
              style={{ textAlign: "right" }}
            >
              <Button variant="primary" onClick={() => this.onFinish()}>
                Hoàn thành buổi học
              </Button>
            </div>
          </div>
          <div className="row align-end">
            <div className="col-12">
              <CustomTable
                dataSource={
                  ScheduleAttendance?.onFilter
                    ? ScheduleAttendance?.student_list_filtered
                    : ScheduleAttendance?.student_list
                }
                total={0}
                columns={columns}
                onChange={data => {}}
                checkbox
                noPaging
              ></CustomTable>
            </div>
          </div>
        </div>
        {this.state.showModal && (
          <CustomModal
            isCreateNew={this.state.isCreateNew}
            onClose={this.onClose}
            id={this.props.id}
            studentId={this.state.studentId}
          ></CustomModal>
        )}
      </div>
    );
  };
}

export default connect(({ ScheduleAttendance, Global }) => ({
  ScheduleAttendance,
  Global
}))(Page);
