import React from "react";
import { connect } from "dva";

class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillMount() {}

  render() {
    return (
      <div className="iq-card pb-0">
        <div className="iq-card-header d-flex justify-content-between">
          <div className="iq-header-title">
            <h6>
              <b>Thông tin buổi học</b>
            </h6>
          </div>
        </div>
        <div className="iq-card-body">
          <div className="row">
            <div className="col-lg-3 col-md-4 col-sm-6 col-xs-6">
              <div className="form-group">
                <p>
                  Cơ sở: <b>Hoàng Ngân</b>
                </p>
              </div>
            </div>
            <div className="col-lg-3 col-md-4 col-sm-6 col-xs-6">
              <div className="form-group">
                <p>
                  Tên lớp: <b>BIBOP 123</b>
                </p>
              </div>
            </div>
            <div className="col-lg-3 col-md-4 col-sm-6 col-xs-6">
              <div className="form-group">
                <p>
                  Ngày học: <b>11/01/2021</b>
                </p>
              </div>
            </div>
            <div className="col-lg-3 col-md-4 col-sm-6 col-xs-6">
              <div className="form-group">
                <p>
                  Giáo viên Việt Nam: <b>Nguyễn Văn A</b>
                </p>
              </div>
            </div>
            <div className="col-lg-3 col-md-4 col-sm-6 col-xs-6">
              <div className="form-group m-0">
                <p className="m-0">
                  Mã lớp: <b>XXXXX</b>
                </p>
              </div>
            </div>
            <div className="col-lg-3 col-md-4 col-sm-6 col-xs-6">
              <div className="form-group m-0">
                <p className="m-0">
                  Phòng học: <b>Mickey</b>
                </p>
              </div>
            </div>
            <div className="col-lg-3 col-md-4 col-sm-6 col-xs-6">
              <div className="form-group m-0">
                <p className="m-0">
                  Giờ học: <b>17h30</b>
                </p>
              </div>
            </div>
            <div className="col-lg-3 col-md-4 col-sm-6 col-xs-6">
              <div className="form-group m-0">
                <p className="m-0">
                  Giáo viên Nước ngoài: <b>Jame</b>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(({ Global }) => ({
  Global
}))(Page);
