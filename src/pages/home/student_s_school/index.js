import React from "react";
import { connect } from "dva";
import StudentSSchoolHeader from "./student_s_school_header";
import StudentSSchoolBody from "./student_s_school_body";
import PageHeader from "../../../components/PageHeader";

class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillMount() {}

  render() {
    const bre = {
      title: "Quản lý trường học của học viên",
      subTitle:
        "Quản lý các trường học của học viên trong trung tâm để có thể đưa ra các phân tích phù hợp",
      breadcrums: [
        {
          title: "Home",
          path: "/"
        },
        {
          title: "Quản lý trường học của học viên",
          path: ""
        }
      ]
    };
    console.log("render account>>>");
    return (
      <>
        <PageHeader {...bre}></PageHeader>
        <div className="iq-card">
          <div className="iq-card-body">
            <StudentSSchoolHeader />
            <StudentSSchoolBody />
          </div>
        </div>
      </>
    );
  }
}

export default connect(({ HealthcareService }) => ({
  HealthcareService
}))(Page);
