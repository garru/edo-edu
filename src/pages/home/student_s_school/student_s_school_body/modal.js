import React, { Component, useState, useRef, useEffect } from "react";
import { useForm } from "react-hook-form";

import { Modal, Button } from "react-bootstrap";
export default function CustomModal(props) {
  const [show, setShow] = useState(true);
  const handleClose = () => {
    // setShow(false);
    props.onClose();
  };
  const { isCreateNew, selectedItem } = props;
  return (
    <Modal
      size={"md"}
      show={show}
      onHide={handleClose}
      backdrop="static"
      keyboard={false}
      id="AddNew"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title>
          {isCreateNew ? "Thêm mới trường học" : "Chỉnh sửa trường học"}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="row">
          <div className="col-12">
            <div className="form-group m-0">
              <label htmlFor="ten_truong">Tên trường học:</label>
              <input
                type="text"
                name="ten_truong"
                className="form-control form-control-lg"
                id="ten_truong"
                defaultValue={!isCreateNew ? selectedItem.ten_truong : ""}
                // ref={register({ required: true, minLength: 8 })}
              ></input>
            </div>
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="primary" style={{ marginRight: 15 }}>
          {isCreateNew ? "Tạo mới" : "Cập nhật"}
        </Button>
        <Button variant={"secondary"} onClick={handleClose}>
          Hủy
        </Button>
      </Modal.Footer>
    </Modal>
  );
}
