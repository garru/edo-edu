import React, {useState, useForm} from "react";
import { Calendar, momentLocalizer, Views } from "react-big-calendar";
import moment from "moment";
import events from "./events";
import * as dates from "./date";
import { connect } from "dva";
import { Modal, Button } from "react-bootstrap";

let allViews = Object.keys(Views).map(k => Views[k]);

const ColoredDateCellWrapper = ({ children }) =>
  React.cloneElement(React.Children.only(children), {
    style: {
      backgroundColor: "lightblue"
    }
  });
const localizer = momentLocalizer(moment);

export default function CustomModal(props) {
  const [show, setShow] = useState(true);
  const handleClose = () => {
    // setShow(false);
    props.onClose();
  };
  const { isCreateNew, selectedEvent, range } = props;
  return (
    <Modal
      size={"lg"}
      show={show}
      onHide={handleClose}
      backdrop="static"
      keyboard={false}
      id="AddNew"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title>
          {isCreateNew ? "Tạo thời gian nghỉ mới" : "Chỉnh sửa thời gian nghỉ"}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div>
          <form>
            <div className="iq-card ">
              <div className="iq-card-body">
                <div className="row m-b-2">
                  <div className="col-12">
                    <div className="form-group">
                      <label htmlFor="thoi_gian_nghi">Tên thời gian nghỉ:</label>
                      <input
                        type="text"
                        name="thoi_gian_nghi"
                        className="form-control form-control-lg"
                        id="thoi_gian_nghi"
                        defaultValue={
                          !isCreateNew ? selectedEvent.title : ""
                        }
                        // ref={register({ required: true, minLength: 8 })}
                      ></input>
                    </div>
                  </div>
                  <div className="col-6">
                    <div className="form-group">
                      <label htmlFor="bat_dau">Bắt đầu:</label>
                      <input
                            type="date"
                            className="form-control form-control-lg"
                            id="bat_dau"
                            defaultValue={
                              !isCreateNew
                                ? moment(selectedEvent.bat_dau).format(
                                    "YYYY-MM-DD"
                                  )
                                : moment(range.start || null).format(
                                  "YYYY-MM-DD"
                                )
                            }
                            
                          />
                    </div>
                  </div>
                  <div className="col-6">
                  <div className="form-group">
                      <label htmlFor="ket_thuc">Kết thúc:</label>
                      <input
                            type="date"
                            className="form-control form-control-lg"
                            id="ket_thuc"
                            defaultValue={
                              !isCreateNew
                                ? moment(selectedEvent.ket_thuc).format(
                                    "YYYY-MM-DD"
                                  )
                                : moment(range.end || null).format(
                                  "YYYY-MM-DD"
                                )
                            }
                            
                          />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="primary" style={{marginRight: 15}}>
          {isCreateNew ? "Tạo mới" : "Cập nhật"}
        </Button>
        <Button variant={"secondary"} onClick={handleClose}>
          Hủy
        </Button>
      </Modal.Footer>
    </Modal>
  );
}
