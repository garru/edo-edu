import React from "react";
import { connect } from "dva";
import { Button, Table, Overlay, Popover } from "react-bootstrap";
import { DATA, HEADER } from "./data";
import * as _ from "lodash";
import CustomModal from "./modal";
import { Link } from "react-router-dom";
import moment from "moment";
import PageHeader from "../../../components/PageHeader";

class Page extends React.Component {
  constructor(props) {
    super(props);
    const initCheckedItem = DATA.map(item => false);
    this.state = {
      show: initCheckedItem,
      target: false,
      listItem: DATA,
      activeIndex: null,
      showModal: false,
      isCreateNew: false,
      selectedItem: null,
      selectedItem: null
    };
  }

  renderTooltip(index) {
    return (
      <Overlay
        show={this.state.show[index]}
        target={this.state.target}
        placement="bottom"
        containerPadding={20}
        rootClose={true}
        rootCloseEvent={"click"}
        onHide={e => this.handleClick(e, index)}
      >
        <Popover id="popover-contained">
          <Popover.Content>
            <ul>
              <li>
                <Link to="thoi_gian_ranh_list/view">
                  <i
                    className="fa fa-edit"
                    style={{
                      marginRight: 0,
                      marginLeft: "auto"
                    }}
                  />
                  &nbsp; Chỉnh sửa
                </Link>
              </li>
              <li
                onClick={e => {
                  this.handleClick(e, index, true);
                }}
              >
                <i
                  className="fa fa-trash-alt"
                  style={{
                    marginRight: 0,
                    marginLeft: "auto"
                  }}
                />
                &nbsp; Xoá
              </li>
            </ul>
          </Popover.Content>
        </Popover>
      </Overlay>
    );
  }

  handleClick(e, index, isHide) {
    let { show, activeIndex, listItem } = this.state;
    show[index] = !show[index];
    index !== activeIndex && (show[activeIndex] = false);
    this.setState({
      show: show,
      target: isHide ? null : e.target,
      activeIndex: index,
      selectedItem: listItem[index]
    });
  }

  onClose = () => {
    this.setState({ isCreateNew: false, showModal: false });
  };

  render() {
    const bre = {
      title: "Thời gian rảnh",
      breadcrums: [
        {
          title: "Home",
          path: "/"
        },
        {
          title: "Thời gian rảnh",
          path: ""
        }
      ]
    };
    console.log("render account_body>>>");
    return (
      <div>
        <PageHeader {...bre}></PageHeader>
        <div className="iq-card">
          <div className="iq-card-body">
            <div className="row">
              <div className="col-12">
                <div className="form-group">
                  <Link to="thoi_gian_ranh_list/view">
                    <Button
                      variant="primary"
                      type="submit"
                      style={{ marginRight: 30 }}
                    >
                      Khai báo
                    </Button>
                  </Link>
                </div>
              </div>
              <div className="col-12">
                <div style={{ width: "100%" }}>
                  <div style={{ overflowX: "auto", width: "100%" }}>
                    <Table
                      bordered
                      hover
                      // style={{ minWidth: 1500 }}
                      className="table"
                    >
                      <thead>
                        <tr>
                          {HEADER.map(item => (
                            <th
                              className={`${item.class} ${
                                item.hasSort ? "sort" : ""
                              } ${
                                this.state.sortField === item.key
                                  ? "active"
                                  : ""
                              }`}
                              key={item.key}
                            >
                              <span>{item.title}</span>
                            </th>
                          ))}
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        {this.state.listItem.map((item, index) => (
                          <React.Fragment>
                            <tr key={`account_${item.id}`}>
                              <td>{item.title}</td>
                              <td>{moment(item.start).format("YYYY-MM-DD")}</td>
                              <td>{moment(item.end).format("YYYY-MM-DD")}</td>
                              <td>{item.trang_thai}</td>
                              <td>
                                <i
                                  className="fa fa-ellipsis-h"
                                  style={{
                                    marginRight: 0,
                                    marginLeft: "auto"
                                  }}
                                  onClick={e => this.handleClick(e, index)}
                                />
                                {this.renderTooltip(index)}
                              </td>
                            </tr>
                          </React.Fragment>
                        ))}
                      </tbody>
                    </Table>
                    {this.state.listItem && this.state.listItem.length ? (
                      ""
                    ) : (
                      <div className="not-found">
                        <i class="fas fa-inbox"></i>
                        <span>Không tìm thấy kết quả</span>
                      </div>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(({ HealthcareService }) => ({
  HealthcareService
}))(Page);
