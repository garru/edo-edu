import React from "react";
import { Calendar, momentLocalizer, Views } from "react-big-calendar";
import moment from "moment";
import events from "./events";
import { connect } from "dva";
import CustomModal from "./modal";
import CustomToolbar from "../../../components/CustomToolbar";

const ColoredDateCellWrapper = ({ children }) =>
  React.cloneElement(React.Children.only(children), {
    style: {
      backgroundColor: "lightblue"
    }
  });
const localizer = momentLocalizer(moment);

class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
      isCreateNew: false,
      selectedEvent: null
    };
  }

  render() {
    const onClose = () => {
      this.setState({ isCreateNew: false, showModal: false });
    };

    return (
      <React.Fragment>
        <Calendar
          selectable
          events={events}
          views={["month"]}
          step={60}
          showMultiDayTimes
          // max={dates.add(dates.endOf(new Date(2015, 17, 1), 'day'), -1, 'hours')}
          defaultDate={new Date()}
          components={{
            timeSlotWrapper: ColoredDateCellWrapper
          }}
          localizer={localizer}
          onSelecting={abc => console.log("abc", abc)}
          onSelectSlot={range => {
            this.setState({ showModal: true, isCreateNew: true, range: range });
          }}
          onSelectEvent={event =>
            this.setState({
              showModal: true,
              isCreateNew: false,
              selectedEvent: event
            })
          }
          components={{ toolbar: CustomToolbar }}
        />
        {this.state.showModal && (
          <CustomModal
            range={this.state.range}
            isCreateNew={this.state.isCreateNew}
            onClose={onClose}
            selectedEvent={this.state.selectedEvent}
          ></CustomModal>
        )}
      </React.Fragment>
    );
  }
}

export default connect(({ Mock }) => ({
  Mock
}))(Page);
