export const HEADER = [
    {
      title: "Tên thời gian nghỉ",
      key: "ten_thoi_gian_nghi",
      class: "tb-width-300"
    },
    {
      title: "Ngày bắt đầu",
      key: "start",
      class: "tb-width-100"
    },
    {
      title: "Ngày kết thúc",
      key: "end",
      class: "tb-width-100"
    },
    {
      title: "Trạng thái",
      key: "trang_thai",
      class: "tb-width-200"
    }
  ];
  
  
  export const DATA = [
    {
      id: 0,
      title: 'Giờ học 1 17h30-19h00',
      allDay: true,
      start: "2021-04-03",
      end: "2021-04-03",
    },
    {
      id: 1,
      title: 'Giờ học 2 17h30-19h00',
      start: "2021-04-03",
      end: "2021-04-03",
    },
    {
      id: 0,
      title: 'Giờ học 1 17h30-19h00',
      allDay: true,
      start: "2021-04-04",
      end: "2021-04-04",
    },
    {
      id: 1,
      title: 'Giờ học 2 17h30-19h00',
      start: "2021-04-04",
      end: "2021-04-04",
    },
  
    {
      id: 0,
      title: 'Giờ học 1 17h30-19h00',
      allDay: true,
      start: "2021-04-05",
      end: "2021-04-05",
    },
    {
      id: 1,
      title: 'Giờ học 2 17h30-19h00',
      start: "2021-04-05",
      end: "2021-04-05",
    },
  
  ]