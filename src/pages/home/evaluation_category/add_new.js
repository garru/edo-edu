import React from "react";
import { Formik } from "formik";
import { Modal, Button } from "react-bootstrap";
import { connect } from "dva";
import useCriteria from "../../../hooks/useCriteria";
import Input from "../../../components/Form/Input";
import Select from "../../../components/Form/Select";

function ModalAddUpdate(props) {
  const { initialValues, validationSchema } = useCriteria();
  const handleClose = () => {
    // setShow(false);
    props.onClose();
  };
  const onSubmit = (values) => {
    if (props.isCreateNew) {
      props.push(values);
    } else {
      props.replace(props.activeIndex, values);
    }
    props.onClose();
  }
  return (
    <Modal
      size={"md"}
      show
      onHide={handleClose}
      backdrop="static"
      keyboard={false}
      id="AddNew"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title>
          {props.isCreateNew ? "Thêm mới tiêu chí" : "Chỉnh sửa tiêu chí"}
        </Modal.Title>
      </Modal.Header>
      <Formik
        initialValues={props.isCreateNew ? initialValues : props.selectedItem}
        validationSchema={validationSchema}
        onSubmit={onSubmit}
      >
        {
          ({values, handleChange, handleBlur, handleSubmit}) => (
            <form onSubmit={handleSubmit}>
              <Modal.Body>
                <div>
                  <div className="row">
                    <div className="col-12">
                      <div className="row font-size-14">
                        <div className="col-md-6">
                          <Input
                            id="name"
                            name="name"
                            label="Kỹ năng - tiêu chí:"
                            value={values.name}
                            onChange={handleChange}
                            onBlur={handleBlur}
                          />
                        </div>
                        <div className="col-md-6">
                          <Select
                            id="parentId"
                            name="parentId"
                            label="Tiêu trí cha:"
                            value={values.parentId}
                            empty
                            options={
                              props.criteria
                                .filter(c => props.isCreateNew || (c && props.selectedItem && c.name !== props.selectedItem.name))
                                .map((c, index) => ({code: index, text: c.name}))
                            }
                            onChange={handleChange}
                            onBlur={handleBlur}
                          />
                        </div>
                        <div className="col-md-6 ">
                          <Input
                            id="weight"
                            name="weight"
                            label="Trọng số:"
                            value={values.weight}
                            onChange={handleChange}
                            onBlur={handleBlur}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </Modal.Body>
              <Modal.Footer>
                <Button type="submit" variant="primary" style={{ marginRight: 15 }}>
                  {props.isCreateNew ? "Tạo mới" : "Cập nhật"}
                </Button>
                <Button type="button" variant={"secondary"} onClick={handleClose}>
                  Hủy
                </Button>
              </Modal.Footer>
            </form>
          )
        }
      </Formik>
    </Modal>
  );
}

export default connect()(ModalAddUpdate);