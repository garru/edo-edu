import React, { useState } from "react";
import { Modal, Button, Table, Overlay, Popover } from "react-bootstrap";
import {FieldArray, Formik} from "formik";
import { connect } from "dva";
import { HEADER_MODAL } from "./data";
import ModalAddUpdate from "./add_new";
import TuDienNhatXet from "./tudiennhanxet";
import useEvaluation from "../../../hooks/useEvaluation";
import Input from "../../../components/Form/Input";
import Select from "../../../components/Form/Select";
import Checkbox from "../../../components/Form/Checkbox";

function CustomModal(props) {
  const { isCreateNew } = props;
  const [show, setShow] = useState(true);
  const [isNew, setNew] = useState(true);
  // criteria modal
  const [showModal, setShowModal] = useState(false);
  // dictionary modal
  const [showModalTuDien, setShowModalTuDien] = useState(false);
  const [showTooltip, setShowTooltip] = useState([]);
  const [target, setTarget] = useState(false);
  const [activeIndex, setActiveIndex] = useState(null);
  const [selectedEva, setSelectedEva] = useState(null);
  const { initialValues, validationSchema, onSubmit } = useEvaluation({ dispatch: props.dispatch});

  const handleClose = () => {
    // setShow(false);
    props.onClose();
  };

  function renderTooltip({item, index, remove}) {
    return (
      <Overlay
        show={showTooltip[index]}
        target={target}
        placement="bottom"
        containerPadding={20}
        rootClose={true}
        rootCloseEvent={"click"}
        onHide={e => handleClick(e, index)}
      >
        <Popover id="popover-contained">
          <Popover.Content>
            <ul>
              <li
                onClick={e => {
                  handleClick(e, index);
                  setNew(false);
                  setSelectedEva(item);
                  setShowModal(true);
                }}>
                <i className="fa fa-edit" style={{marginRight: 0, marginLeft: "auto"}}/>&nbsp; Chỉnh sửa
              </li>
              <li
                onClick={e => {
                  handleClick(e, index);
                  setShowModalTuDien(true);
                }}>
                <i className="fa fa-edit" style={{marginRight: 0, marginLeft: "auto"}}/>&nbsp; Từ điển
              </li>
              <li
                onClick={e => {
                  remove(index);
                  handleClick(e, index, true);
                }}>
                <i className="fa fa-trash-alt" style={{marginRight: 0, marginLeft: "auto"}}/>&nbsp; Xoá
              </li>
            </ul>
          </Popover.Content>
        </Popover>
      </Overlay>
    );
  }

  function handleClick(e, index, isHide) {
    // enable/disable showTooltip at selected index
    showTooltip[index] = !showTooltip[index];
    // if index difference from active index set showTooltip at active index to false
    if (index !== activeIndex) {
      showTooltip[activeIndex] = false
    }
    // set new show tooltip
    setShowTooltip(showTooltip);
    setTarget(e.target);
    // set new active index
    setActiveIndex(index);
  }

  const onClose = () => {
    setNew(false);
    setShowModal(false);
  };

  const onCloseTudien = () => {
    setShowModalTuDien(false);
  };

  return (
    <Modal
      size={"lg"}
      show={show}
      onHide={handleClose}
      backdrop="static"
      keyboard={false}
      id="AddNew"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title>
          {isCreateNew ? "Khởi tạo nhóm tiêu chí" : "Chỉnh sửa nhóm tiêu chí"}
        </Modal.Title>
      </Modal.Header>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={onSubmit}>
        {
          ({values, handleChange, handleBlur, handleSubmit}) => (
            <form onSubmit={handleSubmit}>
              <Modal.Body>
                <div>
                  <div className="row">
                    <div className="col-12">
                      <div className="row font-size-14">
                        <div className="col-md-6">
                          <Input
                            id="name"
                            name="name"
                            label="Tên tiêu chí:"
                            values={values.name}
                            onBlur={handleBlur}
                            onChange={handleChange}
                          />
                        </div>
                        <div className="col-md-6 ">
                          <Select
                            id="evaluationObject"
                            name="evaluationObject"
                            label="Đối tượng:"
                            options={props.Evaluation.list_doi_tuong_danh_gia.map(o => ({ code: o.id, text: o.name}))}
                            value={values.evaluationObject}
                            onBlur={handleBlur}
                            onChange={handleChange}
                          />
                        </div>
                        <div className="col-md-6 ">
                          <Checkbox
                            id="usingDictionary"
                            name="usingDictionary"
                            label="Sử dụng từ điển"
                            value={values.usingDictionary}
                            onBlur={handleBlur}
                            onChange={handleChange}
                          />
                        </div>
                        <div className="col-md-6">
                          <Checkbox
                            id="lastComment"
                            name="lastComment"
                            label="Đưa kết quả nhận xét cuối"
                            value={values.lastComment}
                            onBlur={handleBlur}
                            onChange={handleChange}
                          />
                        </div>
                      </div>
                    </div>
                    <div className="col-12">
                      <div className="form-group">
                        <Button
                          variant="primary"
                          type="button"
                          style={{ marginRight: 30 }}
                          onClick={() => {
                            setNew(true);
                            setShowModal(true);
                          }}
                        >
                          Tạo mới
                        </Button>
                      </div>
                    </div>
                    <div className="col-12">
                      <div style={{ width: "100%" }}>
                        <div style={{ overflowX: "auto", width: "100%" }}>
                          <Table
                            bordered
                            hover
                            // style={{ minWidth: 1500 }}
                            className="table"
                          >
                            <thead>
                            <tr>
                              {HEADER_MODAL.map(item => (
                                <th
                                  className={`${item.class} ${
                                    item.hasSort ? "sort" : ""
                                  }`}
                                  key={item.key}
                                >
                                  <span>{item.title}</span>
                                </th>
                              ))}
                              <th></th>
                            </tr>
                            </thead>
                            <tbody>
                              <FieldArray name="criteria">
                                {({ insert, remove, push, replace }) => (
                                  <>
                                    {values.criteria.map((item, index) => {
                                      return (
                                        <tr key={index}>
                                          <td>{item.name}</td>
                                          <td>{item.weight}</td>
                                          <td>
                                            <i className="fa fa-ellipsis-h"
                                               style={{marginRight: 0, marginLeft: "auto"}}
                                               onClick={e => handleClick(e, index)}
                                            />
                                            {renderTooltip({item, index, remove})}
                                          </td>
                                        </tr>
                                      )
                                    })}
                                    {showModal && (
                                      <ModalAddUpdate
                                        isCreateNew={isNew}
                                        onClose={onClose}
                                        selectedItem={selectedEva}
                                        activeIndex={activeIndex}
                                        criteria={values.criteria}
                                        push={push}
                                        replace={replace}
                                      />
                                    )}
                                    {showModalTuDien && (
                                      <TuDienNhatXet onClose={onCloseTudien} />
                                    )}
                                  </>
                                )}
                              </FieldArray>
                            </tbody>
                          </Table>
                          {values.criteria && values.criteria.length ? (
                            ""
                          ) : (
                            <div className="not-found">
                              <i className="fas fa-inbox" />
                              <span>Không tìm thấy kết quả</span>
                            </div>
                          )}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </Modal.Body>
              <Modal.Footer>
                <Button type="submit" variant="primary" style={{ marginRight: 15 }}>
                  {isCreateNew ? "Tạo mới" : "Cập nhật"}
                </Button>
                <Button type="button" variant={"secondary"} onClick={handleClose}>
                  Hủy
                </Button>
              </Modal.Footer>
            </form>
          )
        }
      </Formik>
    </Modal>
  );
}

export default connect()(CustomModal);
