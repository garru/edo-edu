import React from "react";
import {connect} from "dva";
import {Button} from "react-bootstrap";
import {DATA} from "./data";
import CustomModal from "./modal";
import PageHeader from "../../../components/PageHeader";
import CustomTable from "../../../components/Table";

class Page extends React.Component {
  constructor(props) {
    super(props);
    const initCheckedItem = DATA.map(item => false);
    this.state = {
      show: initCheckedItem,
      target: false,
      listItem: DATA,
      activeIndex: null,
      isCreateNew: true
    };
  }

  componentDidMount() {
    this.props.dispatch({
      type: "Evaluation/list"
    })
  }

  renderTooltip(index) {

  }

  handleClick(e, index, isHide) {
    let {show, activeIndex, listItem} = this.state;
    show[index] = !show[index];
    index !== activeIndex && (show[activeIndex] = false);
    this.setState({
      show: show,
      target: isHide ? null : e.target,
      activeIndex: index,
      selectedItem: listItem[index]
    });
  }

  columns = [
    {
      title: "Nhóm tiêu chí",
      key: "name",
      class: "tb-width-200"
    },
    {
      title: "Đối tượng",
      key: "evaluationObject",
      class: "tb-width-200"
    },
    {
      title: "Số lượng tiêu chí",
      key: "criteriaCount",
      class: "tb-width-200"
    }]

  onClose = () => {
    this.setState({isCreateNew: false, showModal: false});
  };

  render() {
    return (
      <React.Fragment>
        <PageHeader
          title="Tiêu chí đánh giá"
          subTitle="Các trạng thái của học viên và giáo viên theo buổi học hoặc khóa học"
          breadcrums={[{
            title: "Home",
            path: "/"
          }, {
            title: "Tiêu chí đánh giá",
            path: "/evaluation_category"
          }]}
        />
        <div className="iq-card">
          <div className="iq-card-body">
            <div className="row">
              <div className="col-12 text-right">
                <div className="form-group">
                  <Button
                    variant="primary"
                    type="submit"
                    onClick={() =>
                      this.setState({isCreateNew: true, showModal: true})
                    }
                  >
                    Tạo mới
                  </Button>
                </div>
              </div>
              <div className="col-12">
                <CustomTable
                  dataSource={this.props.Evaluation.list_evaluation}
                  total={this.props.Evaluation.total_records}
                  columns={this.columns}
                  onChange={data => this.updateOptions(data)}
                  checkbox
                />
              </div>
            </div>
          </div>
          {this.state.showModal && (
            <CustomModal
              isCreateNew={this.state.isCreateNew}
              onClose={this.onClose}
              selectedItem={this.state.selectedItem}
              Evaluation={this.props.Evaluation}
            />
          )}
        </div>
      </React.Fragment>
    );
  }
}

export default connect(({Evaluation}) => ({
  Evaluation
}))(Page);
