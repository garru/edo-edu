import React, { useState } from "react";
import { Modal, Button, Table } from "react-bootstrap";
import { DATA_TUDIEN, HEADER_TUDIEN } from "./data";
import Rating from "react-rating";
import { Formik } from "formik";
import useDictionary from "../../../hooks/useDictionary";

export default function TuDienNhatXet(props) {
  const [listItem, setListItem] = useState(DATA_TUDIEN);
  const { initialValues, validationSchema } = useDictionary();

  const handleClose = () => {
    // setShow(false);
    props.onClose();
  };

  const onSubmit = (values) => {
    alert(JSON.stringify(values));
  }

  return (
    <Modal
      size={"lg"}
      show
      onHide={handleClose}
      backdrop="static"
      keyboard={false}
      id="AddNew"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title>Từ điển nhận xét</Modal.Title>
      </Modal.Header>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={onSubmit}
      >
        {
          ({values, handleChange, handleBlur, handleSubmit}) => (
            <form onSubmit={handleSubmit}>
              <Modal.Body>
                <div>
                  <div className="row">
                    <div className="col-12">
                      <div className="row font-size-14">
                        <div className="col-md-6 ">
                          <div className="form-group">
                            Tiêu chí cha: <b>Kiến thức</b>
                          </div>
                        </div>
                        <div className="col-md-6 ">
                          <div className="form-group">
                            Tiêu chí: <b>Ngữ pháp</b>
                          </div>
                        </div>
                        <div className="col-md-6 ">
                          <div className="form-group">
                            <div className="row">
                              <div className="col-4">Cấp độ</div>
                              <div className="col-8">
                                <Rating
                                  className="rating"
                                  emptySymbol="far fa-star"
                                  fullSymbol="fa fa-star"
                                  fractions={2}
                                  readonly={true}
                                  initialRating={5}
                                />
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="col-md-6 ">
                          <div className="form-group">
                            <label htmlFor="noi_dung">Nội dung</label>
                            <textarea
                              id="noi_dung"
                              name="noi_dung"
                              className="form-control form-control-lg"
                              rows="4"
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-12">
                      <div style={{ width: "100%" }}>
                        <div style={{ overflowX: "auto", width: "100%" }}>
                          <Table
                            bordered
                            hover
                            // style={{ minWidth: 1500 }}
                            className="table"
                          >
                            <thead>
                            <tr>
                              {HEADER_TUDIEN.map(item => (
                                <th
                                  className={`${item.class} ${
                                    item.hasSort ? "sort" : ""
                                  }`}
                                  key={item.key}
                                >
                                  <span>{item.title}</span>
                                </th>
                              ))}
                            </tr>
                            </thead>
                            <tbody>
                            {listItem.map((item, index) => (
                              <React.Fragment>
                                <tr key={`account_${item.id}`}>
                                  <td>
                                    <Rating
                                      className="rating"
                                      emptySymbol="far fa-star"
                                      fullSymbol="fa fa-star"
                                      fractions={2}
                                      readonly={true}
                                      initialRating={item.cap_do}
                                    />
                                  </td>
                                  <td>{item.nhan_xet}</td>
                                  <td>
                                    <i
                                      className="fa fa-edit"
                                      style={{
                                        marginRight: 15,
                                        marginLeft: "auto"
                                      }}
                                    />
                                    <i
                                      className="fa fa-trash-alt"
                                      style={{
                                        marginRight: 0,
                                        marginLeft: "auto"
                                      }}
                                    />
                                  </td>
                                </tr>
                              </React.Fragment>
                            ))}
                            </tbody>
                          </Table>
                          {listItem && listItem.length ? (
                            ""
                          ) : (
                            <div className="not-found">
                              <i className="fas fa-inbox" />
                              <span>Không tìm thấy kết quả</span>
                            </div>
                          )}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </Modal.Body>
              <Modal.Footer>
                <Button variant="primary" style={{ marginRight: 15 }}>
                  Lưu
                </Button>
                <Button variant={"secondary"} onClick={handleClose}>
                  Hủy
                </Button>
              </Modal.Footer>
            </form>
          )
        }
      </Formik>
    </Modal>
  );
}
