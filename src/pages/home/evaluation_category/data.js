export const TITLE = "Tiêu chí đánh giá";
export const DESCRIPTION = "Các trạng thái của học viên và giáo viên theo buổi học hoặc khóa học"

export const HEADER = [
  {
    title: "Nhóm tiêu chí",
    key: "nhom_tieu_chi",
    class: "tb-width-200"
  },
  {
    title: "Đối tượng",
    key: "doi_tuong",
    class: "tb-width-200"
  },
  {
    title: "Số lượng tiêu chí",
    key: "so_luong_tieu_chi",
    class: "tb-width-200"
  }
];


export const DATA = [
  {
    id: "1",
    nhom_tieu_chi: "Đánh giá phụ học sinh hàng ngày",
    doi_tuong: "Học sinh",
    so_luong_tieu_chi: "15"
  },
  {
    id: "1",
    nhom_tieu_chi: "Đánh giá phụ học sinh hàng ngày",
    doi_tuong: "Học sinh",
    so_luong_tieu_chi: "15"
  },
  {
    id: "1",
    nhom_tieu_chi: "Đánh giá phụ học sinh hàng ngày",
    doi_tuong: "Học sinh",
    so_luong_tieu_chi: "15"
  },
  {
    id: "1",
    nhom_tieu_chi: "Đánh giá phụ học sinh hàng ngày",
    doi_tuong: "Học sinh",
    so_luong_tieu_chi: "15"
  },
  {
    id: "1",
    nhom_tieu_chi: "Đánh giá phụ học sinh hàng ngày",
    doi_tuong: "Học sinh",
    so_luong_tieu_chi: "15"
  },
];


export const HEADER_MODAL = [
  {
    title: "Kỹ năng - Tiêu chí",
    key: "tieu_chi",
    class: "tb-width-300"
  },
  {
    title: "Trọng số",
    key: "trong_so",
    class: "tb-width-200"
  }
];

export const DATA_MODAL = [
  {
    id: "1",
    tieu_chi: "Kiến thức",
    trong_so: "10"
  },
  {
    id: "1",
    tieu_chi: "Kiến thức",
    trong_so: "10"
  },
  {
    id: "1",
    tieu_chi: "Kiến thức",
    trong_so: "10"
  },
  {
    id: "1",
    tieu_chi: "Kiến thức",
    trong_so: "10"
  },
  {
    id: "1",
    tieu_chi: "Kiến thức",
    trong_so: "10"
  },
];

export const HEADER_TUDIEN = [
  {
    title: "Cấp độ",
    key: "cap_do",
    class: "tb-width-150"
  },
  {
    title: "Nhận xét",
    key: "nhan_xet",
    class: "tb-width-400"
  },
  {
    title: "Thao tác",
    key: "thao_tac",
    class: "tb-width-100"
  }
];

export const DATA_TUDIEN = [
  {
    cap_do: 1,
    nhan_xet: "Lorem Ipsum is simply dummy text of the printing and"
  },
  {
    cap_do: 2,
    nhan_xet: "Lorem Ipsum is simply dummy text of the printing and"
  },
  {
    cap_do: 3,
    nhan_xet: "Lorem Ipsum is simply dummy text of the printing and"
  },
]
