export const HEADER = [
  {
    title: "Học liệu",
    key: "hoc_lieu",
    class: "tb-width-300"
  },
  {
    title: "Số lượng",
    key: "so_luong",
    class: "tb-width-100"
  },
  {
    title: "Phân phối",
    key: "phan_phoi",
    class: "tb-width-200"
  },
  {
    title: "Hoàn",
    key: "hoan",
    class: "tb-width-100"
  },
  {
    title: "Thao tác",
    key: "thao_tac",
    class: "tb-width-100"
  }
];

export const DATA = [
  {
    hoc_lieu: "",
    so_luong: "",
    phan_phoi: "",
    hoan: "",
    thao_tac: ""
  }
];
