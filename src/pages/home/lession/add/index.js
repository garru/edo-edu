import React, {useEffect} from "react";
import { connect } from "dva";
import { useDropzone } from "react-dropzone";
import { Button, Table } from "react-bootstrap";
import {
  DANG_BAI,
  TIEU_CHI_DANH_GIA,
  PHAN_PHOI
} from "../../../../mock/dropdown";
import { HEADER, DATA } from "./data";
import { Formik } from "formik";
import useLesson from "../../../../hooks/useLesson";
import PageHeader from "../../../../components/PageHeader";
import Input from "../../../../components/Form/Input";
import TextArea from "../../../../components/Form/TextArea";
import Select from "../../../../components/Form/Select";
import Checkbox from "../../../../components/Form/Checkbox";

function CustomModal(props) {
  const { acceptedFiles, getRootProps, getInputProps } = useDropzone({});
  const { initialValues, validationSchema, onSubmit } = useLesson({ dispatch: props.dispatch});
  const { Program } = props;

  useEffect(() => {
    props.dispatch({
      type: "Program/search"
    })
  }, []);

  // eslint-disable-next-line no-unused-vars
  const files = acceptedFiles.map(file => (
    <li key={file.name}>
      {file.name} - {file.size} bytes
    </li>
  ));

  return (
    <>
      <PageHeader
        title="Thêm bài học"
        subTitle="Thêm mới bài học vào chương trình học"
        breadcrums={[{
          title: "Home",
          path: "/"
        },{
          title: "Thêm bài học",
          path: ""
        }]}
      />
      <Formik
        validationSchema={validationSchema}
        initialValues={initialValues}
        onSubmit={onSubmit}>
        {({values, handleBlur, handleChange, handleSubmit}) => (
          <form onSubmit={handleSubmit}>
            <div className="iq-card">
              <div className=" iq-card-header d-flex justify-content-between">
                <div className="iq-header-title">
                  <h6>
                    <b>Thông tin chung</b>
                  </h6>
                </div>
              </div>
              <div className="iq-card-body">
                <div className="row">
                  <div className="col-lg-6 col-md-6 col-sm-12">
                    <Select
                      id="programId"
                      name="programId"
                      label="Chương trình:"
                      options={
                        Program.list_program.map(program => ({ code: program.id, text: program.name}))
                      }
                      value={values.programId}
                      onChange={handleChange}
                      onBlur={handleBlur}
                    />
                    <Input
                      id="name"
                      name="name"
                      label="Tên bài học:"
                      value={values.name}
                      onChange={handleChange}
                      onBlur={handleBlur}
                    />
                    <Select
                      id="lessonType"
                      name="lessonType"
                      label="Dạng bài:"
                      options={DANG_BAI}
                      value={values.lessonType}
                      onChange={handleChange}
                      onBlur={handleBlur}
                    />
                  </div>
                  <div className="col-lg-6 col-md-6 col-sm-12">
                    <TextArea
                      id="description"
                      name="description"
                      label="Mô tả ngắn:"
                      rows="4"
                      value={values.description}
                      onChange={handleChange}
                      onBlur={handleBlur}
                    />
                    <Checkbox
                      id="foreignTeacher"
                      name="foreignTeacher"
                      label="Giáo viên nước ngoài"
                      value={values.foreignTeacher}
                      onChange={handleChange}
                      onBlur={handleBlur}
                    />
                  </div>
                  <div className="col-12 ">
                    <div className="form-group">
                      <label htmlFor="dang_bai">Các tiêu chí đánh giá:</label>
                      <ul className="vertical">
                        {TIEU_CHI_DANH_GIA.map(item => (
                          <li>
                            <div className="tag">
                              <span>{item.text} &nbsp;</span>
                              <i className="fa fa-times" />
                            </div>
                          </li>
                        ))}
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="iq-card">
              <div className=" iq-card-header d-flex justify-content-between">
                <div className="iq-header-title">
                  <h6>
                    <b>Thông tin làm việc</b>
                  </h6>
                </div>
              </div>
              <div className="iq-card-body">
                <div className="row">
                  <div className="col-12">
                    <TextArea
                      id="lessonContent"
                      name="lessonContent"
                      label="Nội dung:"
                      rows="4"
                      valúe={values.lessonContent}
                      onChange={handleChange}
                      onBlur={handleBlur}
                    />
                    <TextArea
                      id="lessonHomework"
                      name="lessonHomework"
                      label="Bài về nhà:"
                      rows="4"
                      valúe={values.lessonHomework}
                      onChange={handleChange}
                      onBlur={handleBlur}
                    />
                    <div className="form-group">
                      <label htmlFor="file_su_dung">Các file sử dụng:</label>
                      <section>
                        <div
                          {...getRootProps({
                            className: "dropzone"
                          })}
                        >
                          <input {...getInputProps()} />
                          <h5>Thêm file bằng cách kéo thả</h5>
                          <p>
                            Chấp nhận file doc, docx, xls, xlsx, pdf, img, png. Dung
                            lượng tối đa 150MB
                          </p>
                        </div>
                      </section>
                    </div>
                    <div className="form-group">
                      <label htmlFor="file_su_dung">Học liệu:</label>
                      <div style={{ overflowX: "auto", width: "100%" }}>
                        <Table
                          bordered
                          hover
                          // style={{ minWidth: 1500 }}
                          className="table"
                        >
                          <thead>
                            <tr>
                              {HEADER.map(item => (
                                <th
                                  className={`${item.class} ${
                                    item.hasSort ? "sort" : ""
                                  }`}
                                  key={item.key}
                                >
                                  <span>{item.title}</span>
                                </th>
                              ))}
                            </tr>
                          </thead>
                          <tbody>
                            {DATA.map((item, index) => (
                              <React.Fragment>
                                <tr key={`account_${item.id}`}>
                                  <td>{item.hoc_lieu}</td>
                                  <td>{item.so_luong}</td>
                                  <td>
                                    <select
                                      className="form-control form-control-lg"
                                      id={`dang_bai${index}`}
                                      name={`dang_bai${index}`}
                                    >
                                      {PHAN_PHOI.map(item => (
                                        <option value={item.code}>{item.text}</option>
                                      ))}
                                    </select>
                                  </td>
                                  <td>
                                    <input type="checkbox" name={`hoan${index}`} />
                                  </td>
                                  <td></td>
                                </tr>
                              </React.Fragment>
                            ))}
                          </tbody>
                        </Table>
                        {DATA || DATA.length ? (
                          ""
                        ) : (
                          <div className="not-found">
                            <i class="fas fa-inbox" />
                            <span>Không tìm thấy kết quả</span>
                          </div>
                        )}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-12 m-b-2 text-right">
                <Button type="submit" variant="primary" style={{ marginRight: 15 }}>
                  Lưu
                </Button>
                <Button type="button" variant={"secondary"}>Hủy</Button>
              </div>
            </div>
          </form>
        )}
      </Formik>
    </>
  );
}

export default connect(({Program, Lesson}) => ({
  Program,
  Lesson
}))(CustomModal);
