import React from "react";
import { connect } from "dva";
import { Formik } from "formik";
import Select from "../../../../components/Form/Select";
import { CHUONG_TRINH_HOC } from "../lession_body/data";

class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <fieldset>
        <legend>Tìm kiếm</legend>
        <Formik
          initialValues={{ program_id: null}}
          onSubmit={(values) => {
            this.program.search(values);
          }}>
          {
            ({
               values, handleSubmit, handleChange, handleBlur
             }) => (
              <form style={{ width: "100%" }}>
                <div className="row">
                  <div className="col-3 ">
                    <Select
                      id="program_id"
                      name="program_id"
                      label="Chương trình học"
                      options={CHUONG_TRINH_HOC}
                      value={values.program_id}
                      onChange={handleChange}
                      onBlur={handleBlur}
                    />
                  </div>
                </div>
              </form>
            )
          }
        </Formik>
      </fieldset>
    );
  }
}

export default connect(({ HealthcareService }) => ({
  HealthcareService
}))(Page);
