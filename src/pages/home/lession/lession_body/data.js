export const HEADER = [
  {
    title: "Bài học số",
    key: "bai_hoc_so",
    class: "tb-width-150"
  },
  {
    title: "Tên bài học",
    key: "ten_bai_hoc",
    class: "tb-width-300"
  },
  {
    title: "Chương trình",
    key: "chuong_trinh",
    class: "tb-width-150"
  },
  {
    title: "Mô tả ngắn",
    key: "mo_ta_ngan",
    class: "tb-width-200"
  },
  {
    title: "Bài kiểm tra",
    key: "bai_kiem_tra",
    class: "tb-width-200"
  }
];

export const CHUONG_TRINH_HOC = [
  {
    code: "",
    text: ""
  },
  {
    code: "BIBOB 1",
    text: "BIBOB 1"
  },
  {
    code: "BIBOB 2",
    text: "BIBOB 2"
  },
  {
    code: "BIG ENGLIST PLUS 1",
    text: "BIG ENGLIST PLUS 1"
  },
  {
    code: "BIG ENGLIST PLUS 2",
    text: "BIG ENGLIST PLUS 2"
  },
  {
    code: "BIG ENGLIST PLUS 3",
    text: "BIG ENGLIST PLUS 3"
  }
];

export const DATA = [
  {
    id: "1",
    bai_hoc_so: "Mickey",
    ten_bai_hoc: "Phòng học",
    chuong_trinh: "Hoàng Ngân",
    mo_ta_ngan: "7",
    bai_kiem_tra: "14"
  },
  {
    id: "1",
    bai_hoc_so: "Mickey",
    ten_bai_hoc: "Phòng học",
    chuong_trinh: "Hoàng Ngân",
    mo_ta_ngan: "7",
    bai_kiem_tra: "14"
  },
  {
    id: "1",
    bai_hoc_so: "Mickey",
    ten_bai_hoc: "Phòng học",
    chuong_trinh: "Hoàng Ngân",
    mo_ta_ngan: "7",
    bai_kiem_tra: "14"
  },
  {
    id: "1",
    bai_hoc_so: "Mickey",
    ten_bai_hoc: "Phòng học",
    chuong_trinh: "Hoàng Ngân",
    mo_ta_ngan: "7",
    bai_kiem_tra: "14"
  },
  {
    id: "1",
    bai_hoc_so: "Mickey",
    ten_bai_hoc: "Phòng học",
    chuong_trinh: "Hoàng Ngân",
    mo_ta_ngan: "7",
    bai_kiem_tra: "14"
  },
  {
    id: "1",
    bai_hoc_so: "Mickey",
    ten_bai_hoc: "Phòng học",
    chuong_trinh: "Hoàng Ngân",
    mo_ta_ngan: "7",
    bai_kiem_tra: "14"
  },
  {
    id: "1",
    bai_hoc_so: "Mickey",
    ten_bai_hoc: "Phòng học",
    chuong_trinh: "Hoàng Ngân",
    mo_ta_ngan: "7",
    bai_kiem_tra: "14"
  },
  {
    id: "1",
    bai_hoc_so: "Mickey",
    ten_bai_hoc: "Phòng học",
    chuong_trinh: "Hoàng Ngân",
    mo_ta_ngan: "7",
    bai_kiem_tra: "14"
  },
  {
    id: "1",
    bai_hoc_so: "Mickey",
    ten_bai_hoc: "Phòng học",
    chuong_trinh: "Hoàng Ngân",
    mo_ta_ngan: "7",
    bai_kiem_tra: "14"
  }
];
