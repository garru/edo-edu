import React from "react";
import { connect } from "dva";
import { Button } from "react-bootstrap";
import { DATA } from "./data";
import * as _ from "lodash";
import { Link } from "react-router-dom";
import CustomTable from "../../../../components/Table";
import CustomTooltip from "../../../../components/CustomTooltip";

class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id: null,
      activeIndex: null,
      showModal: false,
      isCreateNew: false,
      target: null,
      show: false,
      tooltipIndex: null
    };
  }

  renderTooltip() {
    const { tooltipIndex } = this.state;
    const { dispatch } = this.props;
    const dataSource = [{
      label: "Chỉnh sửa",
      className: "fa fa-edit",
      onClick: e => {
        dispatch({
          type: "ProgramLesson/view",
          payload: { id: this.state.id }
        }).then(() => {
          this.setState({ isCreatNew: false, showModal: true});
        });
        this.handleClick(e, tooltipIndex, true);
      }
    }, {
      label: "Xoá",
      className: "fa fa-trash-alt",
      onClick: e => {

      }
    }]
    return (
      <CustomTooltip
        show={this.state.show}
        onHide={e => this.handleClick(e, tooltipIndex)}
        dataSource={dataSource}
        target={this.state.target}
      />
    );
  }

  columns = [
    {
      title: "Bài học số",
      key: "number",
      class: "tb-width-150"
    },
    {
      title: "Tên bài học",
      key: "name",
      class: "tb-width-300"
    },
    {
      title: "Chương trình",
      key: "programName",
      class: "tb-width-150"
    },
    {
      title: "Mô tả ngắn",
      key: "description",
      class: "tb-width-200"
    },
    {
      title: "Bài kiểm tra",
      key: "type",
      class: "tb-width-200"
    },
    {
      title: "",
      key: "action",
      class: "tb-width-50",
      render: (col, index) => (
        <i className="fa fa-ellipsis-h" onClick={e => {
          e.preventDefault();
          this.handleClick(e, index);
          this.setState({ id: col.id, tooltipIndex: index})
        }}/>
      )
    }]

  handleClick(e, index, isHide) {
    this.setState({
      show: !(index === this.state.activeIndex),
      target: isHide ? null : e.target,
      activeIndex: index === this.state.activeIndex ? null : index
    });
  }

  onClose = () => {
    this.setState({ isCreateNew: false, showModal: false });
  };

  render() {
    return (
      <div className="row">
        <div className="col-12 d-flex justify-content-end m-t-2">
          <div className="form-group">
            <Link to="/lession/add">
              <Button
                variant="primary"
                type="submit"
              >
                Tạo mới
              </Button>
            </Link>
          </div>
        </div>
        <div className="col-12">
          <CustomTable
            dataSource={this.props.ProgramLesson.list_program_lession}
            total={this.props.ProgramLesson.total_record}
            columns={this.columns}
            onChange={data => this.updateOptions(data)}
            checkbox
          />
        </div>
        {this.renderTooltip()}
      </div>
    );
  }
}

export default connect(({ ProgramLesson }) => ({
  ProgramLesson
}))(Page);
