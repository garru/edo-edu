import React from 'react';
import './index.css';
import './ax.css';

class Page extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
		<div class="" id="base" style={{"margin-left":"-150px"}}>
		   <div class="ax_default shape" id="u2978">
		      <div class="" id="u2978_div">
		      </div>
		      <div class="text" id="u2978_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default shape" id="u2979">
		      <div class="" id="u2979_div">
		      </div>
		      <div class="text" id="u2979_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default" data-height="40" data-label="account" data-left="13" data-top="895" data-width="192" id="u2981">
		      <div class="ax_default shape" data-label="accountLable" id="u2982">
		         <img class="img" id="u2982_img" src="images/account_information/accountlable_u55.svg"/>
		         <div class="text" id="u2982_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Nguyen Van A
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u2983">
		         <img class="img" id="u2983_img" src="images/login/u4.svg"/>
		         <div class="text" id="u2983_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="settingMenu" data-left="11" data-top="826" data-width="125" id="u2984">
		      <div class="ax_default shape" data-label="gearIconLable" id="u2985">
		         <img class="img" id="u2985_img" src="images/account_information/geariconlable_u58.svg"/>
		         <div class="text" id="u2985_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Cài đặt
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="44" data-left="11" data-top="826" data-width="44" id="u2986">
		         <div class="ax_default shape" data-label="gearIconBG" id="u2987">
		            <div class="" id="u2987_div">
		            </div>
		            <div class="text" id="u2987_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default shape" data-label="gearIconBG2" id="u2988">
		            <div class="" id="u2988_div">
		            </div>
		            <div class="text" id="u2988_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" data-label="gearIcon" id="u2989">
		            <img class="img" id="u2989_img" src="images/account_information/gearicon_u62.svg"/>
		            <div class="text" id="u2989_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="customerMenu" data-left="11" data-top="145" data-width="143" id="u2990">
		      <div class="ax_default shape" data-label="customerIconLable" id="u2991">
		         <img class="img" id="u2991_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u2991_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Khách hàng
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="customerIconBG" id="u2992">
		         <div class="" id="u2992_div">
		         </div>
		         <div class="text" id="u2992_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="customerIconBG2" id="u2993">
		         <div class="" id="u2993_div">
		         </div>
		         <div class="text" id="u2993_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" data-label="customer" id="u2994">
		         <img class="img" id="u2994_img" src="images/account_information/customer_u67.svg"/>
		         <div class="text" id="u2994_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="classMenu" data-left="11" data-top="198" data-width="143" id="u2995">
		      <div class="ax_default shape" data-label="classIconLable" id="u2996">
		         <img class="img" id="u2996_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u2996_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Lớp học
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG" id="u2997">
		         <div class="" id="u2997_div">
		         </div>
		         <div class="text" id="u2997_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG2" id="u2998">
		         <div class="" id="u2998_div">
		         </div>
		         <div class="text" id="u2998_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" data-label="classicon" id="u2999">
		         <img class="img" id="u2999_img" src="images/account_information/classicon_u72.svg"/>
		         <div class="text" id="u2999_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="dashboardMenu" data-left="11" data-top="91" data-width="143" id="u3000">
		      <div class="ax_default shape" data-label="dashboardIconLable" id="u3001">
		         <img class="img" id="u3001_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u3001_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Dashnoard
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="dashboardIconBG" id="u3002">
		         <div class="" id="u3002_div">
		         </div>
		         <div class="text" id="u3002_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="dashboardIconBG2" id="u3003">
		         <div class="" id="u3003_div">
		         </div>
		         <div class="text" id="u3003_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" data-label="dashboard" id="u3004">
		         <img class="img" id="u3004_img" src="images/account_information/dashboard_u77.svg"/>
		         <div class="text" id="u3004_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="129" data-label="AccountMenu" data-left="58" data-top="806" data-width="246" id="u3005" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default shape" id="u3006">
		         <div class="" id="u3006_div">
		         </div>
		         <div class="text" id="u3006_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u3007">
		         <div class="" id="u3007_div">
		         </div>
		         <div class="text" id="u3007_text">
		            <p>
		               <span style={{"font-family":"ArialMT, Arial","font-weight":"400","text-decoration":"none"}}>
		                  Xin chào:
		               </span>
		               <span style={{"font-family":"Arial Bold, Arial","font-weight":"700","text-decoration":"none"}}>
		                  Nguyen Van A
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3008">
		         <div class="ax_default image" id="u3009">
		            <img class="img" id="u3009_img" src="images/account_information/u82.png"/>
		            <div class="text" id="u3009_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u3010">
		            <div class="" id="u3010_div">
		            </div>
		            <div class="text" id="u3010_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Đăng xuất
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default line" id="u3011">
		         <img class="img" id="u3011_img" src="images/account_information/u84.svg"/>
		         <div class="text" id="u3011_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3012">
		         <div class="ax_default paragraph" id="u3013">
		            <div class="" id="u3013_div">
		            </div>
		            <div class="text" id="u3013_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tài khoản
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default image" id="u3014">
		            <img class="img" id="u3014_img" src="images/account_information/u87.png"/>
		            <div class="text" id="u3014_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="339" data-label="SettingPannel" data-left="58" data-top="525" data-width="339" id="u3015" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default shape" id="u3016">
		         <div class="" id="u3016_div">
		         </div>
		         <div class="text" id="u3016_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u3017">
		         <div class="" id="u3017_div">
		         </div>
		         <div class="text" id="u3017_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Hệ thống
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3018">
		         <div class="ax_default icon" id="u3019">
		            <img class="img" id="u3019_img" src="images/account_information/u92.svg"/>
		            <div class="text" id="u3019_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u3020">
		            <div class="" id="u3020_div">
		            </div>
		            <div class="text" id="u3020_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tài khoản
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3021">
		         <div class="ax_default paragraph" id="u3022">
		            <div class="" id="u3022_div">
		            </div>
		            <div class="text" id="u3022_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phân quyền
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u3023">
		            <img class="img" id="u3023_img" src="images/account_information/u96.svg"/>
		            <div class="text" id="u3023_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u3024">
		         <div class="" id="u3024_div">
		         </div>
		         <div class="text" id="u3024_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Đào tạo
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3025">
		         <div class="ax_default paragraph" id="u3026">
		            <div class="" id="u3026_div">
		            </div>
		            <div class="text" id="u3026_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Thương hiệu
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u3027">
		            <img class="img" id="u3027_img" src="images/account_information/u100.svg"/>
		            <div class="text" id="u3027_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3028">
		         <div class="ax_default paragraph" id="u3029">
		            <div class="" id="u3029_div">
		            </div>
		            <div class="text" id="u3029_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trung tâm
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u3030">
		            <img class="img" id="u3030_img" src="images/account_information/u103.svg"/>
		            <div class="text" id="u3030_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3031">
		         <div class="ax_default icon" id="u3032">
		            <img class="img" id="u3032_img" src="images/account_information/u105.svg"/>
		            <div class="text" id="u3032_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u3033">
		            <div class="" id="u3033_div">
		            </div>
		            <div class="text" id="u3033_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Ngày nghỉ
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3034">
		         <div class="ax_default icon" id="u3035">
		            <img class="img" id="u3035_img" src="images/account_information/u108.svg"/>
		            <div class="text" id="u3035_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u3036">
		            <div class="" id="u3036_div">
		            </div>
		            <div class="text" id="u3036_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phòng ban - Vai trò
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3037">
		         <div class="ax_default paragraph" id="u3038">
		            <div class="" id="u3038_div">
		            </div>
		            <div class="text" id="u3038_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trạng thái học viên
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u3039">
		            <img class="img" id="u3039_img" src="images/account_information/u112.svg"/>
		            <div class="text" id="u3039_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3040">
		         <div class="ax_default paragraph" id="u3041">
		            <div class="" id="u3041_div">
		            </div>
		            <div class="text" id="u3041_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tiêu chí đánh giá
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u3042">
		            <img class="img" id="u3042_img" src="images/account_information/u115.svg"/>
		            <div class="text" id="u3042_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3043">
		         <div class="ax_default paragraph" id="u3044">
		            <div class="" id="u3044_div">
		            </div>
		            <div class="text" id="u3044_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Chương trình học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3045">
		            <div class="ax_default icon" id="u3046">
		               <img class="img" id="u3046_img" src="images/account_information/u119.svg"/>
		               <div class="text" id="u3046_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3047">
		         <div class="ax_default icon" id="u3048">
		            <img class="img" id="u3048_img" src="images/account_information/u121.svg"/>
		            <div class="text" id="u3048_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u3049">
		            <div class="" id="u3049_div">
		            </div>
		            <div class="text" id="u3049_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Bài học
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3050">
		         <div class="ax_default paragraph" id="u3051">
		            <div class="" id="u3051_div">
		            </div>
		            <div class="text" id="u3051_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phòng học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u3052">
		            <img class="img" id="u3052_img" src="images/account_information/u125.svg"/>
		            <div class="text" id="u3052_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3053">
		         <div class="ax_default paragraph" id="u3054">
		            <div class="" id="u3054_div">
		            </div>
		            <div class="text" id="u3054_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trường học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u3055">
		            <img class="img" id="u3055_img" src="images/account_information/u128.svg"/>
		            <div class="text" id="u3055_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default line" id="u3056">
		         <img class="img" id="u3056_img" src="images/account_information/u129.svg"/>
		         <div class="text" id="u3056_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u3057">
		         <div class="" id="u3057_div">
		         </div>
		         <div class="text" id="u3057_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Tài chính
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3058">
		         <div class="ax_default paragraph" id="u3059">
		            <div class="" id="u3059_div">
		            </div>
		            <div class="text" id="u3059_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Học phí
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u3060">
		            <img class="img" id="u3060_img" src="images/account_information/u133.svg"/>
		            <div class="text" id="u3060_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3061">
		         <div class="ax_default paragraph" id="u3062">
		            <div class="" id="u3062_div">
		            </div>
		            <div class="text" id="u3062_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Khuyến mại
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u3063">
		            <img class="img" id="u3063_img" src="images/account_information/u136.svg"/>
		            <div class="text" id="u3063_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="classMenu" data-left="11" data-top="252" data-width="143" id="u3064">
		      <div class="ax_default shape" data-label="classIconLable" id="u3065">
		         <img class="img" id="u3065_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u3065_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Giáo viên
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG" id="u3066">
		         <div class="" id="u3066_div">
		         </div>
		         <div class="text" id="u3066_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG2" id="u3067">
		         <div class="" id="u3067_div">
		         </div>
		         <div class="text" id="u3067_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u3068">
		         <img class="img" id="u3068_img" src="images/account_information/u141.svg"/>
		         <div class="text" id="u3068_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u3069">
		      <img class="img" id="u3069_img" src="images/account_information/u142.svg"/>
		      <div class="text" id="u3069_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u3070">
		      <img class="img" id="u3070_img" src="images/account_information/u143.svg"/>
		      <div class="text" id="u3070_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div id="u2977" style={{"display":"none","visibility":"hidden"}}>
		   </div>
		   <div class="ax_default" data-height="55" data-label="Header" data-left="133" data-top="17" data-width="752" id="u3071">
		      <div class="ax_default paragraph" id="u3072">
		         <div class="" id="u3072_div">
		         </div>
		         <div class="text" id="u3072_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Quản trị bài học
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u3073">
		         <div class="" id="u3073_div">
		         </div>
		         <div class="text" id="u3073_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Quản lý các bài học trong các chương trình học hiện có
		               </span>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default label" id="u3074">
		      <div class="" id="u3074_div">
		      </div>
		      <div class="text" id="u3074_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Chương trình học
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default droplist" id="u3075">
		      <div class="" id="u3075_div">
		      </div>
		      <select class="u3075_input" id="u3075_input">
		         <option class="u3075_input_option" selected="" value=" ">
		         </option>
		         <option class="u3075_input_option" value="BIBOB 1">
		            BIBOB 1
		         </option>
		         <option class="u3075_input_option" value="BIBOB 2">
		            BIBOB 2
		         </option>
		         <option class="u3075_input_option" value="BIG ENGLIST PLUS 1">
		            BIG ENGLIST PLUS 1
		         </option>
		         <option class="u3075_input_option" value="BIG ENGLIST PLUS 2">
		            BIG ENGLIST PLUS 2
		         </option>
		         <option class="u3075_input_option" value="BIG ENGLIST PLUS 3">
		            BIG ENGLIST PLUS 3
		         </option>
		      </select>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="270" data-label="With data" data-left="133" data-top="218" data-width="1667" id="u3076" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default" data-label="Nodata" id="u3077">
		         <div class="ax_default table_cell" id="u3078">
		            <img class="img" id="u3078_img" src="images/lession/u3078.png"/>
		            <div class="text" id="u3078_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3079">
		            <img class="img" id="u3079_img" src="images/lession/u3079.png"/>
		            <div class="text" id="u3079_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Bài học số
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3080">
		            <img class="img" id="u3080_img" src="images/lession/u3080.png"/>
		            <div class="text" id="u3080_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tên bài học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3081">
		            <img class="img" id="u3081_img" src="images/lession/u3081.png"/>
		            <div class="text" id="u3081_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Chương trình
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3082">
		            <img class="img" id="u3082_img" src="images/lession/u3082.png"/>
		            <div class="text" id="u3082_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Mô tả ngắn
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3083">
		            <img class="img" id="u3083_img" src="images/lession/u3083.png"/>
		            <div class="text" id="u3083_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Bài kiểm tra
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3084">
		            <img class="img" id="u3084_img" src="images/lession/u3084.png"/>
		            <div class="text" id="u3084_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3085">
		            <img class="img" id="u3085_img" src="images/lession/u3085.png"/>
		            <div class="text" id="u3085_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3086">
		            <img class="img" id="u3086_img" src="images/lession/u3086.png"/>
		            <div class="text" id="u3086_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     1
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3087">
		            <img class="img" id="u3087_img" src="images/lession/u3087.png"/>
		            <div class="text" id="u3087_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Introduction-Greetings
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3088">
		            <img class="img" id="u3088_img" src="images/lession/u3088.png"/>
		            <div class="text" id="u3088_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     LOOK - Level 1
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3089">
		            <img class="img" id="u3089_img" src="images/lession/u3089.png"/>
		            <div class="text" id="u3089_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Introduce, greating and simple command
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3090">
		            <img class="img" id="u3090_img" src="images/lession/u3090.png"/>
		            <div class="text" id="u3090_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3091">
		            <img class="img" id="u3091_img" src="images/lession/u3091.png"/>
		            <div class="text" id="u3091_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3092">
		            <img class="img" id="u3092_img" src="images/lession/u3085.png"/>
		            <div class="text" id="u3092_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3093">
		            <img class="img" id="u3093_img" src="images/lession/u3086.png"/>
		            <div class="text" id="u3093_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     2
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3094">
		            <img class="img" id="u3094_img" src="images/lession/u3087.png"/>
		            <div class="text" id="u3094_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Introduction-Hello
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3095">
		            <img class="img" id="u3095_img" src="images/lession/u3088.png"/>
		            <div class="text" id="u3095_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     LOOK - Level 1
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3096">
		            <img class="img" id="u3096_img" src="images/lession/u3089.png"/>
		            <div class="text" id="u3096_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Practise saying "Hello!" and "Goodbye"
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3097">
		            <img class="img" id="u3097_img" src="images/lession/u3090.png"/>
		            <div class="text" id="u3097_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3098">
		            <img class="img" id="u3098_img" src="images/lession/u3091.png"/>
		            <div class="text" id="u3098_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3099">
		            <img class="img" id="u3099_img" src="images/lession/u3085.png"/>
		            <div class="text" id="u3099_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3100">
		            <img class="img" id="u3100_img" src="images/lession/u3086.png"/>
		            <div class="text" id="u3100_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     3
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3101">
		            <img class="img" id="u3101_img" src="images/lession/u3087.png"/>
		            <div class="text" id="u3101_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Hello! - Theme Introduction &amp; Words
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3102">
		            <img class="img" id="u3102_img" src="images/lession/u3088.png"/>
		            <div class="text" id="u3102_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     LOOK - Level 1
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3103">
		            <img class="img" id="u3103_img" src="images/lession/u3089.png"/>
		            <div class="text" id="u3103_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Review classroom rules. Identify and use greetings. Identify and follow simple commands.
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3104">
		            <img class="img" id="u3104_img" src="images/lession/u3090.png"/>
		            <div class="text" id="u3104_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3105">
		            <img class="img" id="u3105_img" src="images/lession/u3091.png"/>
		            <div class="text" id="u3105_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3106">
		            <img class="img" id="u3106_img" src="images/lession/u3085.png"/>
		            <div class="text" id="u3106_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3107">
		            <img class="img" id="u3107_img" src="images/lession/u3086.png"/>
		            <div class="text" id="u3107_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     4
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3108">
		            <img class="img" id="u3108_img" src="images/lession/u3087.png"/>
		            <div class="text" id="u3108_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Hello! - Grammar
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3109">
		            <img class="img" id="u3109_img" src="images/lession/u3088.png"/>
		            <div class="text" id="u3109_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     LOOK - Level 1
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3110">
		            <img class="img" id="u3110_img" src="images/lession/u3089.png"/>
		            <div class="text" id="u3110_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Use key language in a sentence: Is it a [book]? Yes, it is. No, it isn't
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3111">
		            <img class="img" id="u3111_img" src="images/lession/u3090.png"/>
		            <div class="text" id="u3111_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3112">
		            <img class="img" id="u3112_img" src="images/lession/u3091.png"/>
		            <div class="text" id="u3112_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3113">
		            <img class="img" id="u3113_img" src="images/lession/u3085.png"/>
		            <div class="text" id="u3113_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3114">
		            <img class="img" id="u3114_img" src="images/lession/u3086.png"/>
		            <div class="text" id="u3114_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     5
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3115">
		            <img class="img" id="u3115_img" src="images/lession/u3087.png"/>
		            <div class="text" id="u3115_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Hello! - Grammar &amp; Song
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3116">
		            <img class="img" id="u3116_img" src="images/lession/u3088.png"/>
		            <div class="text" id="u3116_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     LOOK - Level 1
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3117">
		            <img class="img" id="u3117_img" src="images/lession/u3089.png"/>
		            <div class="text" id="u3117_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Listen and sing to a song about introductions.
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3118">
		            <img class="img" id="u3118_img" src="images/lession/u3090.png"/>
		            <div class="text" id="u3118_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3119">
		            <img class="img" id="u3119_img" src="images/lession/u3091.png"/>
		            <div class="text" id="u3119_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3120">
		            <img class="img" id="u3120_img" src="images/lession/u3085.png"/>
		            <div class="text" id="u3120_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3121">
		            <img class="img" id="u3121_img" src="images/lession/u3086.png"/>
		            <div class="text" id="u3121_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     6
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3122">
		            <img class="img" id="u3122_img" src="images/lession/u3087.png"/>
		            <div class="text" id="u3122_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Hello! - Phonics
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3123">
		            <img class="img" id="u3123_img" src="images/lession/u3088.png"/>
		            <div class="text" id="u3123_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     LOOK - Level 1
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3124">
		            <img class="img" id="u3124_img" src="images/lession/u3089.png"/>
		            <div class="text" id="u3124_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Identify and pronounce sounds associated with the letters A (/æ/), B (/b/), and C (/k/).
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3125">
		            <img class="img" id="u3125_img" src="images/lession/u3090.png"/>
		            <div class="text" id="u3125_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3126">
		            <img class="img" id="u3126_img" src="images/lession/u3091.png"/>
		            <div class="text" id="u3126_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3127">
		            <img class="img" id="u3127_img" src="images/lession/u3085.png"/>
		            <div class="text" id="u3127_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3128">
		            <img class="img" id="u3128_img" src="images/lession/u3086.png"/>
		            <div class="text" id="u3128_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     7
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3129">
		            <img class="img" id="u3129_img" src="images/lession/u3087.png"/>
		            <div class="text" id="u3129_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Hello! - Video &amp; Story
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3130">
		            <img class="img" id="u3130_img" src="images/lession/u3088.png"/>
		            <div class="text" id="u3130_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     LOOK - Level 1
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3131">
		            <img class="img" id="u3131_img" src="images/lession/u3089.png"/>
		            <div class="text" id="u3131_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Watch a video of children introducing themselves.
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3132">
		            <img class="img" id="u3132_img" src="images/lession/u3090.png"/>
		            <div class="text" id="u3132_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3133">
		            <img class="img" id="u3133_img" src="images/lession/u3091.png"/>
		            <div class="text" id="u3133_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3134">
		            <img class="img" id="u3134_img" src="images/lession/u3134.png"/>
		            <div class="text" id="u3134_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3135">
		            <img class="img" id="u3135_img" src="images/lession/u3135.png"/>
		            <div class="text" id="u3135_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     8
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3136">
		            <img class="img" id="u3136_img" src="images/lession/u3136.png"/>
		            <div class="text" id="u3136_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Hello! - Unit Test
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3137">
		            <img class="img" id="u3137_img" src="images/lession/u3137.png"/>
		            <div class="text" id="u3137_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     LOOK - Level 1
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3138">
		            <img class="img" id="u3138_img" src="images/lession/u3138.png"/>
		            <div class="text" id="u3138_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Complete Unit 1 Test for student achievement assessment.
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3139">
		            <img class="img" id="u3139_img" src="images/lession/u3139.png"/>
		            <div class="text" id="u3139_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3140">
		            <img class="img" id="u3140_img" src="images/lession/u3140.png"/>
		            <div class="text" id="u3140_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u3141">
		         <img class="img" id="u3141_img" src="images/lession/u3141.svg"/>
		         <div class="text" id="u3141_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3142">
		         <div class="ax_default icon" id="u3143">
		            <img class="img" id="u3143_img" src="images/account/u635.svg"/>
		            <div class="text" id="u3143_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u3144">
		            <img class="img" id="u3144_img" src="images/evaluation_category/u2145.svg"/>
		            <div class="text" id="u3144_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3145">
		         <div class="ax_default icon" id="u3146">
		            <img class="img" id="u3146_img" src="images/account/u635.svg"/>
		            <div class="text" id="u3146_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u3147">
		            <img class="img" id="u3147_img" src="images/evaluation_category/u2145.svg"/>
		            <div class="text" id="u3147_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3148">
		         <div class="ax_default icon" id="u3149">
		            <img class="img" id="u3149_img" src="images/account/u635.svg"/>
		            <div class="text" id="u3149_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u3150">
		            <img class="img" id="u3150_img" src="images/evaluation_category/u2145.svg"/>
		            <div class="text" id="u3150_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3151">
		         <div class="ax_default icon" id="u3152">
		            <img class="img" id="u3152_img" src="images/account/u635.svg"/>
		            <div class="text" id="u3152_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u3153">
		            <img class="img" id="u3153_img" src="images/evaluation_category/u2145.svg"/>
		            <div class="text" id="u3153_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3154">
		         <div class="ax_default icon" id="u3155">
		            <img class="img" id="u3155_img" src="images/account/u635.svg"/>
		            <div class="text" id="u3155_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u3156">
		            <img class="img" id="u3156_img" src="images/evaluation_category/u2145.svg"/>
		            <div class="text" id="u3156_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3157">
		         <div class="ax_default icon" id="u3158">
		            <img class="img" id="u3158_img" src="images/account/u635.svg"/>
		            <div class="text" id="u3158_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u3159">
		            <img class="img" id="u3159_img" src="images/evaluation_category/u2145.svg"/>
		            <div class="text" id="u3159_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3160">
		         <div class="ax_default icon" id="u3161">
		            <img class="img" id="u3161_img" src="images/account/u635.svg"/>
		            <div class="text" id="u3161_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u3162">
		            <img class="img" id="u3162_img" src="images/evaluation_category/u2145.svg"/>
		            <div class="text" id="u3162_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3163">
		         <div class="ax_default icon" id="u3164">
		            <img class="img" id="u3164_img" src="images/account/u635.svg"/>
		            <div class="text" id="u3164_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u3165">
		            <img class="img" id="u3165_img" src="images/evaluation_category/u2145.svg"/>
		            <div class="text" id="u3165_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="30" data-label="No data" data-left="133" data-top="218" data-width="1667" id="u3166">
		      <div class="ax_default" data-label="No data" id="u3167">
		         <div class="ax_default table_cell" id="u3168">
		            <img class="img" id="u3168_img" src="images/lession/u3168.png"/>
		            <div class="text" id="u3168_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3169">
		            <img class="img" id="u3169_img" src="images/lession/u3169.png"/>
		            <div class="text" id="u3169_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Bài học số
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3170">
		            <img class="img" id="u3170_img" src="images/lession/u3170.png"/>
		            <div class="text" id="u3170_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tên bài học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3171">
		            <img class="img" id="u3171_img" src="images/lession/u3171.png"/>
		            <div class="text" id="u3171_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Chương trình
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3172">
		            <img class="img" id="u3172_img" src="images/lession/u3172.png"/>
		            <div class="text" id="u3172_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Mô tả ngắn
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3173">
		            <img class="img" id="u3173_img" src="images/lession/u3173.png"/>
		            <div class="text" id="u3173_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Bài kiểm tra
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u3174">
		            <img class="img" id="u3174_img" src="images/lession/u3174.png"/>
		            <div class="text" id="u3174_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u3175">
		      <div class="" id="u3175_div">
		      </div>
		      <div class="text" id="u3175_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Thêm mới
		            </span>
		         </p>
		      </div>
		   </div>
		</div>
		
      </div>
    );
  }
}

export default Page;
