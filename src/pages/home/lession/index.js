import React from "react";
import { connect } from "dva";
import LessionHeader from "./lession_header";
import LessionBody from "./lession_body";
import PageHeader from "../../../components/PageHeader";

class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: "ProgramLesson/list",
      payload: {
        programId: 4
      }
    });
  }

  render() {
    return (
      <>
        <PageHeader
          title="Quản trị bài học"
          breadcrums={[{
            title: "Home",
            path: "/"
          }, {
            title: "Quản trị bài học",
            path: "/lession"
          }]}
          subTitle="Quản lý các bài học trong các chương trình học hiện có"
        />
        <div className="iq-card  pb-0">
          <div className="iq-card-body">
            <div className="">
              <LessionHeader />
              <LessionBody />
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default connect(({ ProgramLesson }) => ({
  ProgramLesson
}))(Page);
