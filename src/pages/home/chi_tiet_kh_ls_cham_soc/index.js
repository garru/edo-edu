import React, { useState, useEffect } from "react";
import { Nav } from "react-bootstrap";
import { connect } from "dva";
import "react-datepicker/dist/react-datepicker.css";
import PageHeader from "../../../components/PageHeader";
import { useParams } from "react-router-dom";
import CustomTable from "../../../components/Table";
import UploadAvatar from "../../../components/UploadAvatar";

const Page = props => {
  const [count, setCount] = useState(0);
  const { id } = useParams();
  useEffect(() => {
    const { dispatch } = props;
    dispatch({
      type: "Global/showLoading"
    });
    Promise.all([
      dispatch({
        type: "CustomerDetail/view",
        payload: { id }
      }),
      dispatch({
        type: "CustomerDetail/listappointment",
        payload: { id }
      })
    ]).then(data => {
      dispatch({
        type: "Global/hideLoading"
      });
    });
  }, [count]);

  const bre = {
    title: "Chi tiết chăm sóc khách hàng",
    breadcrums: [
      {
        title: "Home",
        path: "/"
      },
      {
        title: "Chi tiết chăm sóc khách hàng",
        path: ""
      }
    ]
  };

  const columns = [
    {
      title: "No.",
      key: "id",
      class: "tb-width-50"
    },
    {
      title: "Tiêu đề",
      key: "appointmentTitle",
      class: "tb-width-100"
    },
    {
      title: "Đối tượng",
      key: "partnerName",
      class: "tb-width-100"
    },
    {
      title: "Người thực hiện",
      key: "performBy",
      class: "tb-width-150"
    },
    {
      title: "Thời gian",
      key: "appointmentTime",
      class: "tb-width-150"
    },
    {
      title: "Mô tả",
      key: "appointmentContent",
      class: "tb-width-300"
    },
    {
      title: "Trạng thái",
      key: "status",
      class: "tb-width-100"
    },
    {
      title: "Thao tác",
      key: "lastName",
      class: "tb-width-50",
      render: (col, index) => <i className="fa fa-info"></i>
    }
  ];

  return (
    <>
      <PageHeader {...bre} />
      <div className="iq-card">
        <div className="iq-card-body">
          <div className="">
            <div className="row">
              <div className="col-12">
                <Nav fill variant="pills" className="m-b-2" defaultActiveKey={`/cham_soc/${id}`}>
                  <Nav.Item>
                    <Nav.Link key="trao_doi" href={`/trao_doi/${id}`}>
                      Thảo luận
                    </Nav.Link>
                  </Nav.Item>
                  <Nav.Item>
                    <Nav.Link key="cham_soc" href={`/cham_soc/${id}`}>
                      Lịch sử chăm sóc
                    </Nav.Link>
                  </Nav.Item>
                  <Nav.Item>
                    <Nav.Link key="hoa_don" href={`/hoa_don/${id}`}>
                      Hóa đơn
                    </Nav.Link>
                  </Nav.Item>
                  <Nav.Item>
                    <Nav.Link key="dd_nhan_xet" href={`/dd_nhan_xet/${id}`}>
                      Điểm danh/ nhận xét
                    </Nav.Link>
                  </Nav.Item>
                  <Nav.Item>
                    <Nav.Link key="lich_su_hoc" href={`/lich_su_hoc/${id}`}>
                      Lịch sử khác
                    </Nav.Link>
                  </Nav.Item>
                  {/*<Nav.Item>*/}
                  {/*  <Nav.Link href="/kiem_tra_hoc_thu">Kiểm tra/học thử</Nav.Link>*/}
                  {/*</Nav.Item>*/}
                </Nav>
              </div>
              <div className="col-12 mt-4 p-4">
                <div className="row">
                  <div className="col-lg-3 col-md-12 col-sm-12 text-center">
                    <UploadAvatar
                      src={props.curent_customer_detail?.avatar}
                    ></UploadAvatar>
                  </div>
                  <div className="col-lg-9 col-md-12">
                    <CustomTable
                      dataSource={props.CustomerDetail?.list_lich_su_cham_soc}
                      total={0}
                      columns={columns}
                      onChange={data => {}}
                      noPaging
                    ></CustomTable>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default connect(({ CustomerDetail, Global }) => ({
  CustomerDetail,
  Global
}))(Page);
