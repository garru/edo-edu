import React from 'react';
import './index.css';
import './ax.css';

class Page extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
		<div class="" id="base" style={{"margin-left":"-150px"}}>
		   <div class="ax_default shape" id="u11205">
		      <div class="" id="u11205_div">
		      </div>
		      <div class="text" id="u11205_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default shape" id="u11206">
		      <div class="" id="u11206_div">
		      </div>
		      <div class="text" id="u11206_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default" data-height="40" data-label="account" data-left="13" data-top="895" data-width="192" id="u11208">
		      <div class="ax_default shape" data-label="accountLable" id="u11209">
		         <img class="img" id="u11209_img" src="images/account_information/accountlable_u55.svg"/>
		         <div class="text" id="u11209_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Nguyen Van A
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u11210">
		         <img class="img" id="u11210_img" src="images/login/u4.svg"/>
		         <div class="text" id="u11210_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="settingMenu" data-left="11" data-top="826" data-width="125" id="u11211">
		      <div class="ax_default shape" data-label="gearIconLable" id="u11212">
		         <img class="img" id="u11212_img" src="images/account_information/geariconlable_u58.svg"/>
		         <div class="text" id="u11212_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Cài đặt
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="44" data-left="11" data-top="826" data-width="44" id="u11213">
		         <div class="ax_default shape" data-label="gearIconBG" id="u11214">
		            <div class="" id="u11214_div">
		            </div>
		            <div class="text" id="u11214_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default shape" data-label="gearIconBG2" id="u11215">
		            <div class="" id="u11215_div">
		            </div>
		            <div class="text" id="u11215_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" data-label="gearIcon" id="u11216">
		            <img class="img" id="u11216_img" src="images/account_information/gearicon_u62.svg"/>
		            <div class="text" id="u11216_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="customerMenu" data-left="11" data-top="145" data-width="143" id="u11217">
		      <div class="ax_default shape" data-label="customerIconLable" id="u11218">
		         <img class="img" id="u11218_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u11218_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Khách hàng
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="customerIconBG" id="u11219">
		         <div class="" id="u11219_div">
		         </div>
		         <div class="text" id="u11219_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="customerIconBG2" id="u11220">
		         <div class="" id="u11220_div">
		         </div>
		         <div class="text" id="u11220_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" data-label="customer" id="u11221">
		         <img class="img" id="u11221_img" src="images/account_information/customer_u67.svg"/>
		         <div class="text" id="u11221_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="classMenu" data-left="11" data-top="198" data-width="143" id="u11222">
		      <div class="ax_default shape" data-label="classIconLable" id="u11223">
		         <img class="img" id="u11223_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u11223_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Lớp học
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG" id="u11224">
		         <div class="" id="u11224_div">
		         </div>
		         <div class="text" id="u11224_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG2" id="u11225">
		         <div class="" id="u11225_div">
		         </div>
		         <div class="text" id="u11225_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" data-label="classicon" id="u11226">
		         <img class="img" id="u11226_img" src="images/account_information/classicon_u72.svg"/>
		         <div class="text" id="u11226_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="dashboardMenu" data-left="11" data-top="91" data-width="143" id="u11227">
		      <div class="ax_default shape" data-label="dashboardIconLable" id="u11228">
		         <img class="img" id="u11228_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u11228_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Dashnoard
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="dashboardIconBG" id="u11229">
		         <div class="" id="u11229_div">
		         </div>
		         <div class="text" id="u11229_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="dashboardIconBG2" id="u11230">
		         <div class="" id="u11230_div">
		         </div>
		         <div class="text" id="u11230_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" data-label="dashboard" id="u11231">
		         <img class="img" id="u11231_img" src="images/account_information/dashboard_u77.svg"/>
		         <div class="text" id="u11231_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="129" data-label="AccountMenu" data-left="58" data-top="806" data-width="246" id="u11232" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default shape" id="u11233">
		         <div class="" id="u11233_div">
		         </div>
		         <div class="text" id="u11233_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u11234">
		         <div class="" id="u11234_div">
		         </div>
		         <div class="text" id="u11234_text">
		            <p>
		               <span style={{"font-family":"ArialMT, Arial","font-weight":"400","text-decoration":"none"}}>
		                  Xin chào:
		               </span>
		               <span style={{"font-family":"Arial Bold, Arial","font-weight":"700","text-decoration":"none"}}>
		                  Nguyen Van A
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11235">
		         <div class="ax_default image" id="u11236">
		            <img class="img" id="u11236_img" src="images/account_information/u82.png"/>
		            <div class="text" id="u11236_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u11237">
		            <div class="" id="u11237_div">
		            </div>
		            <div class="text" id="u11237_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Đăng xuất
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default line" id="u11238">
		         <img class="img" id="u11238_img" src="images/account_information/u84.svg"/>
		         <div class="text" id="u11238_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11239">
		         <div class="ax_default paragraph" id="u11240">
		            <div class="" id="u11240_div">
		            </div>
		            <div class="text" id="u11240_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tài khoản
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default image" id="u11241">
		            <img class="img" id="u11241_img" src="images/account_information/u87.png"/>
		            <div class="text" id="u11241_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="339" data-label="SettingPannel" data-left="58" data-top="525" data-width="339" id="u11242" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default shape" id="u11243">
		         <div class="" id="u11243_div">
		         </div>
		         <div class="text" id="u11243_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u11244">
		         <div class="" id="u11244_div">
		         </div>
		         <div class="text" id="u11244_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Hệ thống
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11245">
		         <div class="ax_default icon" id="u11246">
		            <img class="img" id="u11246_img" src="images/account_information/u92.svg"/>
		            <div class="text" id="u11246_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u11247">
		            <div class="" id="u11247_div">
		            </div>
		            <div class="text" id="u11247_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tài khoản
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11248">
		         <div class="ax_default paragraph" id="u11249">
		            <div class="" id="u11249_div">
		            </div>
		            <div class="text" id="u11249_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phân quyền
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u11250">
		            <img class="img" id="u11250_img" src="images/account_information/u96.svg"/>
		            <div class="text" id="u11250_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u11251">
		         <div class="" id="u11251_div">
		         </div>
		         <div class="text" id="u11251_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Đào tạo
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11252">
		         <div class="ax_default paragraph" id="u11253">
		            <div class="" id="u11253_div">
		            </div>
		            <div class="text" id="u11253_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Thương hiệu
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u11254">
		            <img class="img" id="u11254_img" src="images/account_information/u100.svg"/>
		            <div class="text" id="u11254_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11255">
		         <div class="ax_default paragraph" id="u11256">
		            <div class="" id="u11256_div">
		            </div>
		            <div class="text" id="u11256_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trung tâm
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u11257">
		            <img class="img" id="u11257_img" src="images/account_information/u103.svg"/>
		            <div class="text" id="u11257_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11258">
		         <div class="ax_default icon" id="u11259">
		            <img class="img" id="u11259_img" src="images/account_information/u105.svg"/>
		            <div class="text" id="u11259_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u11260">
		            <div class="" id="u11260_div">
		            </div>
		            <div class="text" id="u11260_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Ngày nghỉ
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11261">
		         <div class="ax_default icon" id="u11262">
		            <img class="img" id="u11262_img" src="images/account_information/u108.svg"/>
		            <div class="text" id="u11262_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u11263">
		            <div class="" id="u11263_div">
		            </div>
		            <div class="text" id="u11263_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phòng ban - Vai trò
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11264">
		         <div class="ax_default paragraph" id="u11265">
		            <div class="" id="u11265_div">
		            </div>
		            <div class="text" id="u11265_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trạng thái học viên
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u11266">
		            <img class="img" id="u11266_img" src="images/account_information/u112.svg"/>
		            <div class="text" id="u11266_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11267">
		         <div class="ax_default paragraph" id="u11268">
		            <div class="" id="u11268_div">
		            </div>
		            <div class="text" id="u11268_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tiêu chí đánh giá
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u11269">
		            <img class="img" id="u11269_img" src="images/account_information/u115.svg"/>
		            <div class="text" id="u11269_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11270">
		         <div class="ax_default paragraph" id="u11271">
		            <div class="" id="u11271_div">
		            </div>
		            <div class="text" id="u11271_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Chương trình học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11272">
		            <div class="ax_default icon" id="u11273">
		               <img class="img" id="u11273_img" src="images/account_information/u119.svg"/>
		               <div class="text" id="u11273_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11274">
		         <div class="ax_default icon" id="u11275">
		            <img class="img" id="u11275_img" src="images/account_information/u121.svg"/>
		            <div class="text" id="u11275_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u11276">
		            <div class="" id="u11276_div">
		            </div>
		            <div class="text" id="u11276_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Bài học
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11277">
		         <div class="ax_default paragraph" id="u11278">
		            <div class="" id="u11278_div">
		            </div>
		            <div class="text" id="u11278_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phòng học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u11279">
		            <img class="img" id="u11279_img" src="images/account_information/u125.svg"/>
		            <div class="text" id="u11279_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11280">
		         <div class="ax_default paragraph" id="u11281">
		            <div class="" id="u11281_div">
		            </div>
		            <div class="text" id="u11281_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trường học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u11282">
		            <img class="img" id="u11282_img" src="images/account_information/u128.svg"/>
		            <div class="text" id="u11282_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default line" id="u11283">
		         <img class="img" id="u11283_img" src="images/account_information/u129.svg"/>
		         <div class="text" id="u11283_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u11284">
		         <div class="" id="u11284_div">
		         </div>
		         <div class="text" id="u11284_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Tài chính
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11285">
		         <div class="ax_default paragraph" id="u11286">
		            <div class="" id="u11286_div">
		            </div>
		            <div class="text" id="u11286_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Học phí
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u11287">
		            <img class="img" id="u11287_img" src="images/account_information/u133.svg"/>
		            <div class="text" id="u11287_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11288">
		         <div class="ax_default paragraph" id="u11289">
		            <div class="" id="u11289_div">
		            </div>
		            <div class="text" id="u11289_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Khuyến mại
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u11290">
		            <img class="img" id="u11290_img" src="images/account_information/u136.svg"/>
		            <div class="text" id="u11290_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="classMenu" data-left="11" data-top="252" data-width="143" id="u11291">
		      <div class="ax_default shape" data-label="classIconLable" id="u11292">
		         <img class="img" id="u11292_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u11292_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Giáo viên
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG" id="u11293">
		         <div class="" id="u11293_div">
		         </div>
		         <div class="text" id="u11293_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG2" id="u11294">
		         <div class="" id="u11294_div">
		         </div>
		         <div class="text" id="u11294_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u11295">
		         <img class="img" id="u11295_img" src="images/account_information/u141.svg"/>
		         <div class="text" id="u11295_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u11296">
		      <img class="img" id="u11296_img" src="images/account_information/u142.svg"/>
		      <div class="text" id="u11296_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u11297">
		      <img class="img" id="u11297_img" src="images/account_information/u143.svg"/>
		      <div class="text" id="u11297_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div id="u11204" style={{"display":"none","visibility":"hidden"}}>
		   </div>
		   <div class="ax_default paragraph" id="u11298">
		      <div class="" id="u11298_div">
		      </div>
		      <div class="text" id="u11298_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Chi tiết chăm sóc khách hàng
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default shape" id="u11299">
		      <div class="" id="u11299_div">
		      </div>
		      <div class="text" id="u11299_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default" id="u11300">
		      <div class="ax_default table_cell" id="u11301">
		         <img class="img" id="u11301_img" src="images/chi_ti_t_kh-trao___i/u11177.png"/>
		         <div class="text" id="u11301_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Thảo luận
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11302">
		         <img class="img" id="u11302_img" src="images/chi_ti_t_kh-trao___i/u11176.png"/>
		         <div class="text" id="u11302_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Lịch sử chăm sóc
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11303">
		         <img class="img" id="u11303_img" src="images/chi_ti_t_kh-trao___i/u11177.png"/>
		         <div class="text" id="u11303_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Hóa đơn
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11304">
		         <img class="img" id="u11304_img" src="images/chi_ti_t_kh-trao___i/u11177.png"/>
		         <div class="text" id="u11304_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Điểm và nhận xét
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11305">
		         <img class="img" id="u11305_img" src="images/chi_ti_t_kh-trao___i/u11177.png"/>
		         <div class="text" id="u11305_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Điểm danh
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11306">
		         <img class="img" id="u11306_img" src="images/chi_ti_t_kh-trao___i/u11181.png"/>
		         <div class="text" id="u11306_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Lịch sử học
		               </span>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default shape" id="u11307">
		      <img class="img" id="u11307_img" src="images/chi_ti_t_kh-trao___i/u11182.svg"/>
		      <div class="text" id="u11307_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default" data-height="128" data-left="1581" data-top="130" data-width="178" id="u11308">
		      <div class="ax_default label" id="u11309">
		         <div class="" id="u11309_div">
		         </div>
		         <div class="text" id="u11309_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Hoàng Văn Huy
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default label" id="u11310">
		         <div class="" id="u11310_div">
		         </div>
		         <div class="text" id="u11310_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  HV-000002
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default label" id="u11311">
		         <div class="" id="u11311_div">
		         </div>
		         <div class="text" id="u11311_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  0384455522
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default label" id="u11312">
		         <div class="" id="u11312_div">
		         </div>
		         <div class="text" id="u11312_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  test@qa/team
		               </span>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default shape" id="u11313">
		      <div class="" id="u11313_div">
		      </div>
		      <div class="text" id="u11313_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default" id="u11314">
		      <div class="ax_default table_cell" id="u11315">
		         <img class="img" id="u11315_img" src="images/_i_m_danh__nh_n_x_t/u7420.png"/>
		         <div class="text" id="u11315_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  No.
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11316">
		         <img class="img" id="u11316_img" src="images/chi_ti_t_kh-ls_ch_m_s_c/u11316.png"/>
		         <div class="text" id="u11316_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Tiêu đề
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11317">
		         <img class="img" id="u11317_img" src="images/crm_list/u10727.png"/>
		         <div class="text" id="u11317_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Đối tượng
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11318">
		         <img class="img" id="u11318_img" src="images/chi_ti_t_kh-ls_ch_m_s_c/u11318.png"/>
		         <div class="text" id="u11318_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Người thực hiện
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11319">
		         <img class="img" id="u11319_img" src="images/course_price/u3922.png"/>
		         <div class="text" id="u11319_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Thời gian
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11320">
		         <img class="img" id="u11320_img" src="images/l_ch_h_c_t_o_m_i/u9250.png"/>
		         <div class="text" id="u11320_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Mô tả
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11321">
		         <img class="img" id="u11321_img" src="images/k_nh_thanh_to_n/u4090.png"/>
		         <div class="text" id="u11321_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Trạng thái
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11322">
		         <img class="img" id="u11322_img" src="images/chi_ti_t_kh-ls_ch_m_s_c/u11322.png"/>
		         <div class="text" id="u11322_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Thao tác
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11323">
		         <img class="img" id="u11323_img" src="images/chi_ti_t_kh-ls_ch_m_s_c/u11323.png"/>
		         <div class="text" id="u11323_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  1
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11324">
		         <img class="img" id="u11324_img" src="images/chi_ti_t_kh-ls_ch_m_s_c/u11324.png"/>
		         <div class="text" id="u11324_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Gọi điện chăm sóc lần 1
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11325">
		         <img class="img" id="u11325_img" src="images/chi_ti_t_kh-ls_ch_m_s_c/u11325.png"/>
		         <div class="text" id="u11325_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Hoàng Văn Huy
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11326">
		         <img class="img" id="u11326_img" src="images/chi_ti_t_kh-ls_ch_m_s_c/u11326.png"/>
		         <div class="text" id="u11326_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Hoàng Dung
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11327">
		         <img class="img" id="u11327_img" src="images/chi_ti_t_kh-ls_ch_m_s_c/u11327.png"/>
		         <div class="text" id="u11327_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  2021-02-07 21:25:00
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11328">
		         <img class="img" id="u11328_img" src="images/chi_ti_t_kh-ls_ch_m_s_c/u11328.png"/>
		         <div class="text" id="u11328_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries,
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11329">
		         <img class="img" id="u11329_img" src="images/chi_ti_t_kh-ls_ch_m_s_c/u11329.png"/>
		         <div class="text" id="u11329_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Có vấn đề
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11330">
		         <img class="img" id="u11330_img" src="images/chi_ti_t_kh-ls_ch_m_s_c/u11330.png"/>
		         <div class="text" id="u11330_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11331">
		         <img class="img" id="u11331_img" src="images/_i_m_danh__nh_n_x_t/u7539.png"/>
		         <div class="text" id="u11331_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  2
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11332">
		         <img class="img" id="u11332_img" src="images/chi_ti_t_kh-ls_ch_m_s_c/u11332.png"/>
		         <div class="text" id="u11332_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Gửi mail về đơn giá khóa học
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11333">
		         <img class="img" id="u11333_img" src="images/chi_ti_t_kh-ls_ch_m_s_c/u11333.png"/>
		         <div class="text" id="u11333_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Hoàng Văn Huy
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11334">
		         <img class="img" id="u11334_img" src="images/chi_ti_t_kh-ls_ch_m_s_c/u11334.png"/>
		         <div class="text" id="u11334_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Pham Minh
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11335">
		         <img class="img" id="u11335_img" src="images/chi_ti_t_kh-ls_ch_m_s_c/u11335.png"/>
		         <div class="text" id="u11335_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  2021-02-07 21:50:00
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11336">
		         <img class="img" id="u11336_img" src="images/l_ch_h_c_t_o_m_i/u9394.png"/>
		         <div class="text" id="u11336_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Đã gửi mail
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11337">
		         <img class="img" id="u11337_img" src="images/k_nh_thanh_to_n/u4115.png"/>
		         <div class="text" id="u11337_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Hoàn thành
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11338">
		         <img class="img" id="u11338_img" src="images/chi_ti_t_kh-ls_ch_m_s_c/u11338.png"/>
		         <div class="text" id="u11338_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u11339">
		      <img class="img" id="u11339_img" src="images/gi_o_vi_n/u9831.svg"/>
		      <div class="text" id="u11339_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		</div>
		
      </div>
    );
  }
}

export default Page;
