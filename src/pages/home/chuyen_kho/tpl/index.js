import React from 'react';
import './index.css';
import './ax.css';

class Page extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
		<div class="" id="base" style={{"margin-left":"-150px"}}>
		   <div class="ax_default shape" id="u6162">
		      <div class="" id="u6162_div">
		      </div>
		      <div class="text" id="u6162_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default shape" id="u6163">
		      <div class="" id="u6163_div">
		      </div>
		      <div class="text" id="u6163_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default" data-height="40" data-label="account" data-left="13" data-top="895" data-width="192" id="u6165">
		      <div class="ax_default shape" data-label="accountLable" id="u6166">
		         <img class="img" id="u6166_img" src="images/account_information/accountlable_u55.svg"/>
		         <div class="text" id="u6166_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Nguyen Van A
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u6167">
		         <img class="img" id="u6167_img" src="images/login/u4.svg"/>
		         <div class="text" id="u6167_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="settingMenu" data-left="11" data-top="826" data-width="125" id="u6168">
		      <div class="ax_default shape" data-label="gearIconLable" id="u6169">
		         <img class="img" id="u6169_img" src="images/account_information/geariconlable_u58.svg"/>
		         <div class="text" id="u6169_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Cài đặt
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="44" data-left="11" data-top="826" data-width="44" id="u6170">
		         <div class="ax_default shape" data-label="gearIconBG" id="u6171">
		            <div class="" id="u6171_div">
		            </div>
		            <div class="text" id="u6171_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default shape" data-label="gearIconBG2" id="u6172">
		            <div class="" id="u6172_div">
		            </div>
		            <div class="text" id="u6172_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" data-label="gearIcon" id="u6173">
		            <img class="img" id="u6173_img" src="images/account_information/gearicon_u62.svg"/>
		            <div class="text" id="u6173_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="customerMenu" data-left="11" data-top="145" data-width="143" id="u6174">
		      <div class="ax_default shape" data-label="customerIconLable" id="u6175">
		         <img class="img" id="u6175_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u6175_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Khách hàng
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="customerIconBG" id="u6176">
		         <div class="" id="u6176_div">
		         </div>
		         <div class="text" id="u6176_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="customerIconBG2" id="u6177">
		         <div class="" id="u6177_div">
		         </div>
		         <div class="text" id="u6177_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" data-label="customer" id="u6178">
		         <img class="img" id="u6178_img" src="images/account_information/customer_u67.svg"/>
		         <div class="text" id="u6178_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="classMenu" data-left="11" data-top="198" data-width="143" id="u6179">
		      <div class="ax_default shape" data-label="classIconLable" id="u6180">
		         <img class="img" id="u6180_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u6180_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Lớp học
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG" id="u6181">
		         <div class="" id="u6181_div">
		         </div>
		         <div class="text" id="u6181_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG2" id="u6182">
		         <div class="" id="u6182_div">
		         </div>
		         <div class="text" id="u6182_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" data-label="classicon" id="u6183">
		         <img class="img" id="u6183_img" src="images/account_information/classicon_u72.svg"/>
		         <div class="text" id="u6183_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="dashboardMenu" data-left="11" data-top="91" data-width="143" id="u6184">
		      <div class="ax_default shape" data-label="dashboardIconLable" id="u6185">
		         <img class="img" id="u6185_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u6185_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Dashnoard
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="dashboardIconBG" id="u6186">
		         <div class="" id="u6186_div">
		         </div>
		         <div class="text" id="u6186_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="dashboardIconBG2" id="u6187">
		         <div class="" id="u6187_div">
		         </div>
		         <div class="text" id="u6187_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" data-label="dashboard" id="u6188">
		         <img class="img" id="u6188_img" src="images/account_information/dashboard_u77.svg"/>
		         <div class="text" id="u6188_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="129" data-label="AccountMenu" data-left="58" data-top="806" data-width="246" id="u6189" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default shape" id="u6190">
		         <div class="" id="u6190_div">
		         </div>
		         <div class="text" id="u6190_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u6191">
		         <div class="" id="u6191_div">
		         </div>
		         <div class="text" id="u6191_text">
		            <p>
		               <span style={{"font-family":"ArialMT, Arial","font-weight":"400","text-decoration":"none"}}>
		                  Xin chào:
		               </span>
		               <span style={{"font-family":"Arial Bold, Arial","font-weight":"700","text-decoration":"none"}}>
		                  Nguyen Van A
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u6192">
		         <div class="ax_default image" id="u6193">
		            <img class="img" id="u6193_img" src="images/account_information/u82.png"/>
		            <div class="text" id="u6193_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u6194">
		            <div class="" id="u6194_div">
		            </div>
		            <div class="text" id="u6194_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Đăng xuất
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default line" id="u6195">
		         <img class="img" id="u6195_img" src="images/account_information/u84.svg"/>
		         <div class="text" id="u6195_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u6196">
		         <div class="ax_default paragraph" id="u6197">
		            <div class="" id="u6197_div">
		            </div>
		            <div class="text" id="u6197_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tài khoản
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default image" id="u6198">
		            <img class="img" id="u6198_img" src="images/account_information/u87.png"/>
		            <div class="text" id="u6198_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="339" data-label="SettingPannel" data-left="58" data-top="525" data-width="339" id="u6199" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default shape" id="u6200">
		         <div class="" id="u6200_div">
		         </div>
		         <div class="text" id="u6200_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u6201">
		         <div class="" id="u6201_div">
		         </div>
		         <div class="text" id="u6201_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Hệ thống
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u6202">
		         <div class="ax_default icon" id="u6203">
		            <img class="img" id="u6203_img" src="images/account_information/u92.svg"/>
		            <div class="text" id="u6203_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u6204">
		            <div class="" id="u6204_div">
		            </div>
		            <div class="text" id="u6204_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tài khoản
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u6205">
		         <div class="ax_default paragraph" id="u6206">
		            <div class="" id="u6206_div">
		            </div>
		            <div class="text" id="u6206_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phân quyền
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u6207">
		            <img class="img" id="u6207_img" src="images/account_information/u96.svg"/>
		            <div class="text" id="u6207_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u6208">
		         <div class="" id="u6208_div">
		         </div>
		         <div class="text" id="u6208_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Đào tạo
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u6209">
		         <div class="ax_default paragraph" id="u6210">
		            <div class="" id="u6210_div">
		            </div>
		            <div class="text" id="u6210_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Thương hiệu
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u6211">
		            <img class="img" id="u6211_img" src="images/account_information/u100.svg"/>
		            <div class="text" id="u6211_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u6212">
		         <div class="ax_default paragraph" id="u6213">
		            <div class="" id="u6213_div">
		            </div>
		            <div class="text" id="u6213_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trung tâm
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u6214">
		            <img class="img" id="u6214_img" src="images/account_information/u103.svg"/>
		            <div class="text" id="u6214_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u6215">
		         <div class="ax_default icon" id="u6216">
		            <img class="img" id="u6216_img" src="images/account_information/u105.svg"/>
		            <div class="text" id="u6216_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u6217">
		            <div class="" id="u6217_div">
		            </div>
		            <div class="text" id="u6217_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Ngày nghỉ
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u6218">
		         <div class="ax_default icon" id="u6219">
		            <img class="img" id="u6219_img" src="images/account_information/u108.svg"/>
		            <div class="text" id="u6219_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u6220">
		            <div class="" id="u6220_div">
		            </div>
		            <div class="text" id="u6220_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phòng ban - Vai trò
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u6221">
		         <div class="ax_default paragraph" id="u6222">
		            <div class="" id="u6222_div">
		            </div>
		            <div class="text" id="u6222_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trạng thái học viên
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u6223">
		            <img class="img" id="u6223_img" src="images/account_information/u112.svg"/>
		            <div class="text" id="u6223_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u6224">
		         <div class="ax_default paragraph" id="u6225">
		            <div class="" id="u6225_div">
		            </div>
		            <div class="text" id="u6225_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tiêu chí đánh giá
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u6226">
		            <img class="img" id="u6226_img" src="images/account_information/u115.svg"/>
		            <div class="text" id="u6226_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u6227">
		         <div class="ax_default paragraph" id="u6228">
		            <div class="" id="u6228_div">
		            </div>
		            <div class="text" id="u6228_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Chương trình học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u6229">
		            <div class="ax_default icon" id="u6230">
		               <img class="img" id="u6230_img" src="images/account_information/u119.svg"/>
		               <div class="text" id="u6230_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u6231">
		         <div class="ax_default icon" id="u6232">
		            <img class="img" id="u6232_img" src="images/account_information/u121.svg"/>
		            <div class="text" id="u6232_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u6233">
		            <div class="" id="u6233_div">
		            </div>
		            <div class="text" id="u6233_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Bài học
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u6234">
		         <div class="ax_default paragraph" id="u6235">
		            <div class="" id="u6235_div">
		            </div>
		            <div class="text" id="u6235_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phòng học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u6236">
		            <img class="img" id="u6236_img" src="images/account_information/u125.svg"/>
		            <div class="text" id="u6236_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u6237">
		         <div class="ax_default paragraph" id="u6238">
		            <div class="" id="u6238_div">
		            </div>
		            <div class="text" id="u6238_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trường học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u6239">
		            <img class="img" id="u6239_img" src="images/account_information/u128.svg"/>
		            <div class="text" id="u6239_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default line" id="u6240">
		         <img class="img" id="u6240_img" src="images/account_information/u129.svg"/>
		         <div class="text" id="u6240_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u6241">
		         <div class="" id="u6241_div">
		         </div>
		         <div class="text" id="u6241_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Tài chính
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u6242">
		         <div class="ax_default paragraph" id="u6243">
		            <div class="" id="u6243_div">
		            </div>
		            <div class="text" id="u6243_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Học phí
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u6244">
		            <img class="img" id="u6244_img" src="images/account_information/u133.svg"/>
		            <div class="text" id="u6244_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u6245">
		         <div class="ax_default paragraph" id="u6246">
		            <div class="" id="u6246_div">
		            </div>
		            <div class="text" id="u6246_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Khuyến mại
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u6247">
		            <img class="img" id="u6247_img" src="images/account_information/u136.svg"/>
		            <div class="text" id="u6247_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="classMenu" data-left="11" data-top="252" data-width="143" id="u6248">
		      <div class="ax_default shape" data-label="classIconLable" id="u6249">
		         <img class="img" id="u6249_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u6249_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Giáo viên
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG" id="u6250">
		         <div class="" id="u6250_div">
		         </div>
		         <div class="text" id="u6250_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG2" id="u6251">
		         <div class="" id="u6251_div">
		         </div>
		         <div class="text" id="u6251_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u6252">
		         <img class="img" id="u6252_img" src="images/account_information/u141.svg"/>
		         <div class="text" id="u6252_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u6253">
		      <img class="img" id="u6253_img" src="images/account_information/u142.svg"/>
		      <div class="text" id="u6253_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u6254">
		      <img class="img" id="u6254_img" src="images/account_information/u143.svg"/>
		      <div class="text" id="u6254_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div id="u6161" style={{"display":"none","visibility":"hidden"}}>
		   </div>
		   <div class="ax_default" data-height="77" data-label="Header" data-left="133" data-top="17" data-width="350" id="u6255">
		      <div class="ax_default paragraph" id="u6256">
		         <div class="" id="u6256_div">
		         </div>
		         <div class="text" id="u6256_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Chuyển kho
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u6257">
		         <div class="" id="u6257_div">
		         </div>
		         <div class="text" id="u6257_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Quản lý danh sách sản phẩm được luân chuyển giữa các kho và tạo báo cáo chuyển kho
		               </span>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="64" data-left="1366" data-top="109" data-width="279" id="u6259">
		      <div class="ax_default paragraph" id="u6260">
		         <div class="" id="u6260_div">
		         </div>
		         <div class="text" id="u6260_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Khoảng thời gian
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default text_field" id="u6261">
		         <div class="" id="u6261_div">
		         </div>
		         <input class="u6261_input" id="u6261_input" type="text" value=""/>
		      </div>
		      <div class="ax_default text_field" id="u6262">
		         <div class="" id="u6262_div">
		         </div>
		         <input class="u6262_input" id="u6262_input" type="text" value=""/>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u6263">
		      <div class="" id="u6263_div">
		      </div>
		      <div class="text" id="u6263_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Tìm kiếm
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default" data-height="22" data-left="778" data-top="131" data-width="239" id="u6264">
		      <div class="ax_default" data-height="15" data-left="778" data-top="134" data-width="60" id="u6265">
		         <div class="ax_default paragraph" id="u6266">
		            <div class="" id="u6266_div">
		            </div>
		            <div class="text" id="u6266_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Danh mục
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default droplist" id="u6267">
		         <div class="" id="u6267_div">
		         </div>
		         <select class="u6267_input" id="u6267_input">
		            <option class="u6267_input_option" value="Tất cả">
		               Tất cả
		            </option>
		            <option class="u6267_input_option" value="Hoàng Ngân">
		               Hoàng Ngân
		            </option>
		            <option class="u6267_input_option" value="Văn Quán">
		               Văn Quán
		            </option>
		         </select>
		      </div>
		   </div>
		   <div class="ax_default" data-height="64" data-left="1052" data-top="110" data-width="279" id="u6268">
		      <div class="ax_default" data-height="15" data-left="1052" data-top="135" data-width="75" id="u6269">
		         <div class="ax_default paragraph" id="u6270">
		            <div class="" id="u6270_div">
		            </div>
		            <div class="text" id="u6270_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Luân chuyển
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default droplist" id="u6271">
		         <div class="" id="u6271_div">
		         </div>
		         <select class="u6271_input" id="u6271_input">
		            <option class="u6271_input_option" value="Từ kho">
		               Từ kho
		            </option>
		            <option class="u6271_input_option" value="Kho 1">
		               Kho 1
		            </option>
		            <option class="u6271_input_option" value="Kho 2">
		               Kho 2
		            </option>
		            <option class="u6271_input_option" value="Kho 3">
		               Kho 3
		            </option>
		         </select>
		      </div>
		      <div class="ax_default droplist" id="u6272">
		         <div class="" id="u6272_div">
		         </div>
		         <select class="u6272_input" id="u6272_input">
		            <option class="u6272_input_option" value="Đến kho">
		               Đến kho
		            </option>
		            <option class="u6272_input_option" value="Kho 1">
		               Kho 1
		            </option>
		            <option class="u6272_input_option" value="Kho 2">
		               Kho 2
		            </option>
		            <option class="u6272_input_option" value="Kho 3">
		               Kho 3
		            </option>
		         </select>
		      </div>
		   </div>
		   <div class="ax_default" data-height="22" data-left="504" data-top="131" data-width="239" id="u6273">
		      <div class="ax_default droplist" id="u6274">
		         <div class="" id="u6274_div">
		         </div>
		         <select class="u6274_input" id="u6274_input">
		            <option class="u6274_input_option" value="Tất cả">
		               Tất cả
		            </option>
		            <option class="u6274_input_option" value="Hoàng Ngân">
		               Hoàng Ngân
		            </option>
		            <option class="u6274_input_option" value="Văn Quán">
		               Văn Quán
		            </option>
		         </select>
		      </div>
		      <div class="ax_default" data-height="15" data-left="504" data-top="135" data-width="70" id="u6275">
		         <div class="ax_default paragraph" id="u6276">
		            <div class="" id="u6276_div">
		            </div>
		            <div class="text" id="u6276_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Cơ sở nhập
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="22" data-left="230" data-top="131" data-width="239" id="u6277">
		      <div class="ax_default droplist" id="u6278">
		         <div class="" id="u6278_div">
		         </div>
		         <select class="u6278_input" id="u6278_input">
		            <option class="u6278_input_option" value="Tất cả">
		               Tất cả
		            </option>
		            <option class="u6278_input_option" value="Hoàng Ngân">
		               Hoàng Ngân
		            </option>
		            <option class="u6278_input_option" value="Văn Quán">
		               Văn Quán
		            </option>
		         </select>
		      </div>
		      <div class="ax_default" data-height="15" data-left="230" data-top="135" data-width="65" id="u6279">
		         <div class="ax_default paragraph" id="u6280">
		            <div class="" id="u6280_div">
		            </div>
		            <div class="text" id="u6280_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Cơ sở xuất
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u6281">
		      <div class="" id="u6281_div">
		      </div>
		      <div class="text" id="u6281_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Thêm mới
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default" id="u6282">
		      <div class="ax_default table_cell" id="u6283">
		         <img class="img" id="u6283_img" src="images/kho/u4408.png"/>
		         <div class="text" id="u6283_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  STT
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6284">
		         <img class="img" id="u6284_img" src="images/th_m_m_i/u5750.png"/>
		         <div class="text" id="u6284_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Mã phiếu
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6285">
		         <img class="img" id="u6285_img" src="images/chuy_n_kho/u6285.png"/>
		         <div class="text" id="u6285_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Cơ sở xuất
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6286">
		         <img class="img" id="u6286_img" src="images/xu_t_h_ng/u5408.png"/>
		         <div class="text" id="u6286_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Co sở nhận
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6287">
		         <img class="img" id="u6287_img" src="images/chuy_n_kho/u6287.png"/>
		         <div class="text" id="u6287_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Sản phẩm chuyển
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6288">
		         <img class="img" id="u6288_img" src="images/phi_u_nh_p_xu_t/u4651.png"/>
		         <div class="text" id="u6288_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Danh mục
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6289">
		         <img class="img" id="u6289_img" src="images/chuy_n_kho/u6289.png"/>
		         <div class="text" id="u6289_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Đơn vị
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6290">
		         <img class="img" id="u6290_img" src="images/nh_p_h_ng/u5105.png"/>
		         <div class="text" id="u6290_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Số lượng
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6291">
		         <img class="img" id="u6291_img" src="images/nh_p_h_ng/u5099.png"/>
		         <div class="text" id="u6291_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Kho chuyển
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6292">
		         <img class="img" id="u6292_img" src="images/chuy_n_kho/u6292.png"/>
		         <div class="text" id="u6292_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Kho nhận
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6293">
		         <img class="img" id="u6293_img" src="images/chuy_n_kho/u6293.png"/>
		         <div class="text" id="u6293_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Thời gian lập phiếu
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6294">
		         <img class="img" id="u6294_img" src="images/chuy_n_kho/u6294.png"/>
		         <div class="text" id="u6294_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Chi tiết
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6295">
		         <img class="img" id="u6295_img" src="images/chuy_n_kho/u6295.png"/>
		         <div class="text" id="u6295_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6296">
		         <img class="img" id="u6296_img" src="images/kho/u4414.png"/>
		         <div class="text" id="u6296_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  1
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6297">
		         <img class="img" id="u6297_img" src="images/chuy_n_kho/u6297.png"/>
		         <div class="text" id="u6297_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  A-12
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6298">
		         <img class="img" id="u6298_img" src="images/chuy_n_kho/u6298.png"/>
		         <div class="text" id="u6298_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Hoàng Ngân
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6299">
		         <img class="img" id="u6299_img" src="images/chuy_n_kho/u6299.png"/>
		         <div class="text" id="u6299_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Hoàng Ngân
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6300">
		         <img class="img" id="u6300_img" src="images/chuy_n_kho/u6300.png"/>
		         <div class="text" id="u6300_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Sách Tiếng Anh 1
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6301">
		         <img class="img" id="u6301_img" src="images/phi_u_nh_p_xu_t/u4658.png"/>
		         <div class="text" id="u6301_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Dụng cụ học tập
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6302">
		         <img class="img" id="u6302_img" src="images/chuy_n_kho/u6302.png"/>
		         <div class="text" id="u6302_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Quyển
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6303">
		         <img class="img" id="u6303_img" src="images/chuy_n_kho/u6303.png"/>
		         <div class="text" id="u6303_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  20
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6304">
		         <img class="img" id="u6304_img" src="images/chuy_n_kho/u6304.png"/>
		         <div class="text" id="u6304_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Kho 1
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6305">
		         <img class="img" id="u6305_img" src="images/chuy_n_kho/u6305.png"/>
		         <div class="text" id="u6305_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Kho 2
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6306">
		         <img class="img" id="u6306_img" src="images/chuy_n_kho/u6306.png"/>
		         <div class="text" id="u6306_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  2021-02-23
		               </span>
		            </p>
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  15:52
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6307">
		         <img class="img" id="u6307_img" src="images/chuy_n_kho/u6307.png"/>
		         <div class="text" id="u6307_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6308">
		         <img class="img" id="u6308_img" src="images/chuy_n_kho/u6308.png"/>
		         <div class="text" id="u6308_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6309">
		         <img class="img" id="u6309_img" src="images/kho/u4420.png"/>
		         <div class="text" id="u6309_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  2
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6310">
		         <img class="img" id="u6310_img" src="images/chuy_n_kho/u6310.png"/>
		         <div class="text" id="u6310_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  B-34
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6311">
		         <img class="img" id="u6311_img" src="images/chuy_n_kho/u6311.png"/>
		         <div class="text" id="u6311_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Văn Quán
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6312">
		         <img class="img" id="u6312_img" src="images/chuy_n_kho/u6312.png"/>
		         <div class="text" id="u6312_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Mai Dịch
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6313">
		         <img class="img" id="u6313_img" src="images/chuy_n_kho/u6313.png"/>
		         <div class="text" id="u6313_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Ghế học sinh
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6314">
		         <img class="img" id="u6314_img" src="images/chuy_n_kho/u6314.png"/>
		         <div class="text" id="u6314_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Cơ sở vật chất
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6315">
		         <img class="img" id="u6315_img" src="images/chuy_n_kho/u6315.png"/>
		         <div class="text" id="u6315_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Chiếc
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6316">
		         <img class="img" id="u6316_img" src="images/chuy_n_kho/u6316.png"/>
		         <div class="text" id="u6316_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  10
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6317">
		         <img class="img" id="u6317_img" src="images/chuy_n_kho/u6317.png"/>
		         <div class="text" id="u6317_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Kho 2
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6318">
		         <img class="img" id="u6318_img" src="images/chuy_n_kho/u6318.png"/>
		         <div class="text" id="u6318_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Kho 1
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6319">
		         <img class="img" id="u6319_img" src="images/chuy_n_kho/u6319.png"/>
		         <div class="text" id="u6319_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  2021-02-15
		               </span>
		            </p>
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  14:36
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6320">
		         <img class="img" id="u6320_img" src="images/chuy_n_kho/u6320.png"/>
		         <div class="text" id="u6320_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6321">
		         <img class="img" id="u6321_img" src="images/chuy_n_kho/u6321.png"/>
		         <div class="text" id="u6321_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6322">
		         <img class="img" id="u6322_img" src="images/kho/u4426.png"/>
		         <div class="text" id="u6322_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  3
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6323">
		         <img class="img" id="u6323_img" src="images/chuy_n_kho/u6323.png"/>
		         <div class="text" id="u6323_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  C-56
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6324">
		         <img class="img" id="u6324_img" src="images/chuy_n_kho/u6324.png"/>
		         <div class="text" id="u6324_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Mai Dịch
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6325">
		         <img class="img" id="u6325_img" src="images/xu_t_h_ng/u5418.png"/>
		         <div class="text" id="u6325_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Hoàng Ngân
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6326">
		         <img class="img" id="u6326_img" src="images/chuy_n_kho/u6326.png"/>
		         <div class="text" id="u6326_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Sách bài tập TA1
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6327">
		         <img class="img" id="u6327_img" src="images/chuy_n_kho/u6327.png"/>
		         <div class="text" id="u6327_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Dụng cụ học tập
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6328">
		         <img class="img" id="u6328_img" src="images/chuy_n_kho/u6328.png"/>
		         <div class="text" id="u6328_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Quyển
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6329">
		         <img class="img" id="u6329_img" src="images/nh_p_h_ng/u5116.png"/>
		         <div class="text" id="u6329_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  30
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6330">
		         <img class="img" id="u6330_img" src="images/nh_p_h_ng/u5110.png"/>
		         <div class="text" id="u6330_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Kho 1
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6331">
		         <img class="img" id="u6331_img" src="images/chuy_n_kho/u6331.png"/>
		         <div class="text" id="u6331_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Kho 3
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6332">
		         <img class="img" id="u6332_img" src="images/chuy_n_kho/u6332.png"/>
		         <div class="text" id="u6332_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  2021-02-14
		               </span>
		            </p>
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  14:36
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6333">
		         <img class="img" id="u6333_img" src="images/chuy_n_kho/u6333.png"/>
		         <div class="text" id="u6333_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u6334">
		         <img class="img" id="u6334_img" src="images/chuy_n_kho/u6334.png"/>
		         <div class="text" id="u6334_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="22" data-label="Paging" data-left="187" data-top="503" data-width="1682" id="u6335">
		      <div class="ax_default paragraph" id="u6336">
		         <div class="" id="u6336_div">
		         </div>
		         <div class="text" id="u6336_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Hiển thị 1 đến 3 trong tổng số 3 kết quả
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u6337">
		         <div class="" id="u6337_div">
		         </div>
		         <div class="text" id="u6337_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Đi tới trang
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default droplist" id="u6338">
		         <div class="" id="u6338_div">
		         </div>
		         <select class="u6338_input" id="u6338_input">
		            <option class="u6338_input_option" selected="" value="1">
		               1
		            </option>
		            <option class="u6338_input_option" value="2">
		               2
		            </option>
		            <option class="u6338_input_option" value="3">
		               3
		            </option>
		            <option class="u6338_input_option" value="4">
		               4
		            </option>
		            <option class="u6338_input_option" value="5">
		               5
		            </option>
		            <option class="u6338_input_option" value="6">
		               6
		            </option>
		            <option class="u6338_input_option" value="7">
		               7
		            </option>
		            <option class="u6338_input_option" value="8">
		               8
		            </option>
		            <option class="u6338_input_option" value="9">
		               9
		            </option>
		            <option class="u6338_input_option" value="10">
		               10
		            </option>
		            <option class="u6338_input_option" value="11">
		               11
		            </option>
		            <option class="u6338_input_option" value="12">
		               12
		            </option>
		            <option class="u6338_input_option" value="13">
		               13
		            </option>
		            <option class="u6338_input_option" value="14">
		               14
		            </option>
		         </select>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u6339">
		      <img class="img" id="u6339_img" src="images/account/u429.svg"/>
		      <div class="text" id="u6339_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u6340">
		      <img class="img" id="u6340_img" src="images/account/u429.svg"/>
		      <div class="text" id="u6340_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u6341">
		      <img class="img" id="u6341_img" src="images/account/u429.svg"/>
		      <div class="text" id="u6341_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u6342">
		      <div class="" id="u6342_div">
		      </div>
		      <div class="text" id="u6342_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Xem
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u6343">
		      <div class="" id="u6343_div">
		      </div>
		      <div class="text" id="u6343_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Xem
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u6344">
		      <div class="" id="u6344_div">
		      </div>
		      <div class="text" id="u6344_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Xem
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="950" data-label="ChiTiet3" data-left="0" data-top="0" data-width="1920" id="u6345" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default shape" data-label="alpha_Layer" id="u6346">
		         <div class="" id="u6346_div">
		         </div>
		         <div class="text" id="u6346_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-label="Add account" data-left="0" data-top="0" data-width="0" id="u6347">
		         <div class="ax_default shape" id="u6348">
		            <div class="" id="u6348_div">
		            </div>
		            <div class="text" id="u6348_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default box_1" id="u6349">
		            <div class="" id="u6349_div">
		            </div>
		            <div class="text" id="u6349_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u6350">
		            <div class="" id="u6350_div">
		            </div>
		            <div class="text" id="u6350_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Chi tiết phiếu 3 (C-56)
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u6351">
		            <div class="ax_default paragraph" id="u6352">
		               <div class="" id="u6352_div">
		               </div>
		               <div class="text" id="u6352_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u6353">
		            <img class="img" id="u6353_img" src="images/login/u29.svg"/>
		            <div class="text" id="u6353_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default button" id="u6354">
		            <div class="" id="u6354_div">
		            </div>
		            <div class="text" id="u6354_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Hủy
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default line" id="u6355">
		            <img class="img" id="u6355_img" src="images/chuy_n_kho/u6355.svg"/>
		            <div class="text" id="u6355_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default heading_3" id="u6356">
		            <div class="" id="u6356_div">
		            </div>
		            <div class="text" id="u6356_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Bên Xuất
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default heading_3" id="u6357">
		            <div class="" id="u6357_div">
		            </div>
		            <div class="text" id="u6357_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Cơ sở: Mai Dịch
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default heading_3" id="u6358">
		            <div class="" id="u6358_div">
		            </div>
		            <div class="text" id="u6358_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Kho: Kho 1
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default heading_3" id="u6359">
		            <div class="" id="u6359_div">
		            </div>
		            <div class="text" id="u6359_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Quản lý: Nguyễn Văn A
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default heading_3" id="u6360">
		            <div class="" id="u6360_div">
		            </div>
		            <div class="text" id="u6360_text">
		               <p>
		                  <span style={{"text-decoration":"underline"}}>
		                     Quản lý xác nhận:
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default checkbox selected" id="u6361">
		            <label for="u6361_input" id="u6361_input_label" style={{"position":" absolute","left":" 0px"}}>
		               <img class="img" id="u6361_img" src="images/chuy_n_kho/u6361_selected.svg"/>
		               <div class="text" id="u6361_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Đã xác nhận
		                     </span>
		                  </p>
		               </div>
		            </label>
		            <input checked="" id="u6361_input" type="checkbox" value="checkbox"/>
		         </div>
		         <div class="ax_default heading_3" id="u6362">
		            <div class="" id="u6362_div">
		            </div>
		            <div class="text" id="u6362_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     14 giờ 35 phút, ngày 1 tháng 3 năm 2021
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default heading_3" id="u6363">
		            <div class="" id="u6363_div">
		            </div>
		            <div class="text" id="u6363_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Bên Nhận
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default heading_3" id="u6364">
		            <div class="" id="u6364_div">
		            </div>
		            <div class="text" id="u6364_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Cơ sở: Hoàng Ngân
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default heading_3" id="u6365">
		            <div class="" id="u6365_div">
		            </div>
		            <div class="text" id="u6365_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Kho: Kho 3
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default heading_3" id="u6366">
		            <div class="" id="u6366_div">
		            </div>
		            <div class="text" id="u6366_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Quản lý: Nguyễn Văn B
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default heading_3" id="u6367">
		            <div class="" id="u6367_div">
		            </div>
		            <div class="text" id="u6367_text">
		               <p>
		                  <span style={{"text-decoration":"underline"}}>
		                     Quản lý xác nhận:
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default heading_3" id="u6368">
		            <div class="" id="u6368_div">
		            </div>
		            <div class="text" id="u6368_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     ... giờ ... phút, ngày ... tháng ... năm ...
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default checkbox" id="u6369">
		            <label for="u6369_input" id="u6369_input_label" style={{"position":" absolute","left":" 0px"}}>
		               <img class="img" id="u6369_img" src="images/chuy_n_kho/u6369.svg"/>
		               <div class="text" id="u6369_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Đợi xác nhận...
		                     </span>
		                  </p>
		               </div>
		            </label>
		            <input id="u6369_input" type="checkbox" value="checkbox"/>
		         </div>
		         <div class="ax_default heading_3" id="u6370">
		            <div class="" id="u6370_div">
		            </div>
		            <div class="text" id="u6370_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Người tạo phiếu: Nguyễn Văn C
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="950" data-label="ChiTiet1" data-left="0" data-top="0" data-width="1920" id="u6371" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default shape" data-label="alpha_Layer" id="u6372">
		         <div class="" id="u6372_div">
		         </div>
		         <div class="text" id="u6372_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-label="Add account" data-left="0" data-top="0" data-width="0" id="u6373">
		         <div class="ax_default shape" id="u6374">
		            <div class="" id="u6374_div">
		            </div>
		            <div class="text" id="u6374_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default box_1" id="u6375">
		            <div class="" id="u6375_div">
		            </div>
		            <div class="text" id="u6375_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u6376">
		            <div class="" id="u6376_div">
		            </div>
		            <div class="text" id="u6376_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Chi tiết phiếu 1 (A-12)
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u6377">
		            <div class="ax_default paragraph" id="u6378">
		               <div class="" id="u6378_div">
		               </div>
		               <div class="text" id="u6378_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u6379">
		            <img class="img" id="u6379_img" src="images/login/u29.svg"/>
		            <div class="text" id="u6379_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default button" id="u6380">
		            <div class="" id="u6380_div">
		            </div>
		            <div class="text" id="u6380_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Hủy
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default line" id="u6381">
		            <img class="img" id="u6381_img" src="images/chuy_n_kho/u6355.svg"/>
		            <div class="text" id="u6381_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default heading_3" id="u6382">
		            <div class="" id="u6382_div">
		            </div>
		            <div class="text" id="u6382_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Bên Xuất
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default heading_3" id="u6383">
		            <div class="" id="u6383_div">
		            </div>
		            <div class="text" id="u6383_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Cơ sở: Hoàng Ngân
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default heading_3" id="u6384">
		            <div class="" id="u6384_div">
		            </div>
		            <div class="text" id="u6384_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Kho: Kho 1
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default heading_3" id="u6385">
		            <div class="" id="u6385_div">
		            </div>
		            <div class="text" id="u6385_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Quản lý: Nguyễn Văn A
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default heading_3" id="u6386">
		            <div class="" id="u6386_div">
		            </div>
		            <div class="text" id="u6386_text">
		               <p>
		                  <span style={{"text-decoration":"underline"}}>
		                     Quản lý xác nhận:
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default checkbox selected" id="u6387">
		            <label for="u6387_input" id="u6387_input_label" style={{"position":" absolute","left":" 0px"}}>
		               <img class="img" id="u6387_img" src="images/chuy_n_kho/u6361_selected.svg"/>
		               <div class="text" id="u6387_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Đã xác nhận
		                     </span>
		                  </p>
		               </div>
		            </label>
		            <input checked="" id="u6387_input" type="checkbox" value="checkbox"/>
		         </div>
		         <div class="ax_default heading_3" id="u6388">
		            <div class="" id="u6388_div">
		            </div>
		            <div class="text" id="u6388_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     14 giờ 35 phút, ngày 1 tháng 3 năm 2021
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default heading_3" id="u6389">
		            <div class="" id="u6389_div">
		            </div>
		            <div class="text" id="u6389_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Bên Nhận
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default heading_3" id="u6390">
		            <div class="" id="u6390_div">
		            </div>
		            <div class="text" id="u6390_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Cơ sở: Hoàng Ngân
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default heading_3" id="u6391">
		            <div class="" id="u6391_div">
		            </div>
		            <div class="text" id="u6391_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Kho: Kho 2
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default heading_3" id="u6392">
		            <div class="" id="u6392_div">
		            </div>
		            <div class="text" id="u6392_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Quản lý: Nguyễn Văn B
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default heading_3" id="u6393">
		            <div class="" id="u6393_div">
		            </div>
		            <div class="text" id="u6393_text">
		               <p>
		                  <span style={{"text-decoration":"underline"}}>
		                     Quản lý xác nhận:
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default heading_3" id="u6394">
		            <div class="" id="u6394_div">
		            </div>
		            <div class="text" id="u6394_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     15 giờ 40 phút, ngày 1 tháng 3 năm 2021
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default checkbox selected" id="u6395">
		            <label for="u6395_input" id="u6395_input_label" style={{"position":" absolute","left":" 0px"}}>
		               <img class="img" id="u6395_img" src="images/chuy_n_kho/u6395_selected.svg"/>
		               <div class="text" id="u6395_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Đã xác nhận
		                     </span>
		                  </p>
		               </div>
		            </label>
		            <input checked="" id="u6395_input" type="checkbox" value="checkbox"/>
		         </div>
		      </div>
		      <div class="ax_default heading_3" id="u6396">
		         <div class="" id="u6396_div">
		         </div>
		         <div class="text" id="u6396_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Người tạo phiếu: Nguyễn Văn C
		               </span>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="950" data-label="ChiTiet2" data-left="0" data-top="0" data-width="1920" id="u6397" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default shape" data-label="alpha_Layer" id="u6398">
		         <div class="" id="u6398_div">
		         </div>
		         <div class="text" id="u6398_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-label="Add account" data-left="0" data-top="0" data-width="0" id="u6399">
		         <div class="ax_default shape" id="u6400">
		            <div class="" id="u6400_div">
		            </div>
		            <div class="text" id="u6400_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default box_1" id="u6401">
		            <div class="" id="u6401_div">
		            </div>
		            <div class="text" id="u6401_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u6402">
		            <div class="" id="u6402_div">
		            </div>
		            <div class="text" id="u6402_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Chi tiết phiếu 2 (B-34)
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u6403">
		            <div class="ax_default paragraph" id="u6404">
		               <div class="" id="u6404_div">
		               </div>
		               <div class="text" id="u6404_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u6405">
		            <img class="img" id="u6405_img" src="images/login/u29.svg"/>
		            <div class="text" id="u6405_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default button" id="u6406">
		            <div class="" id="u6406_div">
		            </div>
		            <div class="text" id="u6406_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Hủy
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default line" id="u6407">
		            <img class="img" id="u6407_img" src="images/chuy_n_kho/u6355.svg"/>
		            <div class="text" id="u6407_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default heading_3" id="u6408">
		            <div class="" id="u6408_div">
		            </div>
		            <div class="text" id="u6408_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Bên Xuất
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default heading_3" id="u6409">
		            <div class="" id="u6409_div">
		            </div>
		            <div class="text" id="u6409_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Cơ sở: Văn Quán
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default heading_3" id="u6410">
		            <div class="" id="u6410_div">
		            </div>
		            <div class="text" id="u6410_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Kho: Kho 2
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default heading_3" id="u6411">
		            <div class="" id="u6411_div">
		            </div>
		            <div class="text" id="u6411_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Quản lý: Nguyễn Văn A
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default heading_3" id="u6412">
		            <div class="" id="u6412_div">
		            </div>
		            <div class="text" id="u6412_text">
		               <p>
		                  <span style={{"text-decoration":"underline"}}>
		                     Quản lý xác nhận:
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default checkbox selected" id="u6413">
		            <label for="u6413_input" id="u6413_input_label" style={{"position":" absolute","left":" 0px"}}>
		               <img class="img" id="u6413_img" src="images/chuy_n_kho/u6361_selected.svg"/>
		               <div class="text" id="u6413_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Đã xác nhận
		                     </span>
		                  </p>
		               </div>
		            </label>
		            <input checked="" id="u6413_input" type="checkbox" value="checkbox"/>
		         </div>
		         <div class="ax_default heading_3" id="u6414">
		            <div class="" id="u6414_div">
		            </div>
		            <div class="text" id="u6414_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     14 giờ 35 phút, ngày 1 tháng 3 năm 2021
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default heading_3" id="u6415">
		            <div class="" id="u6415_div">
		            </div>
		            <div class="text" id="u6415_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Bên Nhận
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default heading_3" id="u6416">
		            <div class="" id="u6416_div">
		            </div>
		            <div class="text" id="u6416_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Cơ sở: Mai Dịch
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default heading_3" id="u6417">
		            <div class="" id="u6417_div">
		            </div>
		            <div class="text" id="u6417_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Kho: Kho 1
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default heading_3" id="u6418">
		            <div class="" id="u6418_div">
		            </div>
		            <div class="text" id="u6418_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Quản lý: Nguyễn Văn B
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default heading_3" id="u6419">
		            <div class="" id="u6419_div">
		            </div>
		            <div class="text" id="u6419_text">
		               <p>
		                  <span style={{"text-decoration":"underline"}}>
		                     Quản lý xác nhận:
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default heading_3" id="u6420">
		            <div class="" id="u6420_div">
		            </div>
		            <div class="text" id="u6420_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     15 giờ 40 phút, ngày 1 tháng 3 năm 2021
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default checkbox selected" id="u6421">
		            <label for="u6421_input" id="u6421_input_label" style={{"position":" absolute","left":" 0px"}}>
		               <img class="img" id="u6421_img" src="images/chuy_n_kho/u6421_selected.svg"/>
		               <div class="text" id="u6421_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Xác nhận chưa
		                     </span>
		                  </p>
		               </div>
		            </label>
		            <input checked="" id="u6421_input" type="checkbox" value="checkbox"/>
		         </div>
		         <div class="ax_default heading_3" id="u6422">
		            <div class="" id="u6422_div">
		            </div>
		            <div class="text" id="u6422_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Người tạo phiếu: Nguyễn Văn C
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="950" data-label="addAccount" data-left="0" data-top="0" data-width="1920" id="u6423" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default shape" data-label="alpha_Layer" id="u6424">
		         <div class="" id="u6424_div">
		         </div>
		         <div class="text" id="u6424_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-label="Add account" data-left="0" data-top="0" data-width="0" id="u6425">
		         <div class="ax_default shape" id="u6426">
		            <div class="" id="u6426_div">
		            </div>
		            <div class="text" id="u6426_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default box_1" id="u6427">
		            <div class="" id="u6427_div">
		            </div>
		            <div class="text" id="u6427_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u6428">
		            <div class="" id="u6428_div">
		            </div>
		            <div class="text" id="u6428_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phiếu chuyển kho
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u6429">
		            <div class="ax_default paragraph" id="u6430">
		               <div class="" id="u6430_div">
		               </div>
		               <div class="text" id="u6430_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Cơ sở xuất
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default paragraph" id="u6431">
		               <div class="" id="u6431_div">
		               </div>
		               <div class="text" id="u6431_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        *
		                     </span>
		                  </p>
		               </div>
		            </div>
		         </div>
		         <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u6432">
		            <div class="ax_default paragraph" id="u6433">
		               <div class="" id="u6433_div">
		               </div>
		               <div class="text" id="u6433_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u6434">
		            <img class="img" id="u6434_img" src="images/login/u29.svg"/>
		            <div class="text" id="u6434_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default primary_button" id="u6435">
		            <div class="" id="u6435_div">
		            </div>
		            <div class="text" id="u6435_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Cập nhật
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default button" id="u6436">
		            <div class="" id="u6436_div">
		            </div>
		            <div class="text" id="u6436_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Hủy
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u6437">
		            <div class="ax_default paragraph" id="u6438">
		               <div class="" id="u6438_div">
		               </div>
		               <div class="text" id="u6438_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Danh mục chức năng
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default paragraph" id="u6439">
		               <div class="" id="u6439_div">
		               </div>
		               <div class="text" id="u6439_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        *
		                     </span>
		                  </p>
		               </div>
		            </div>
		         </div>
		         <div class="ax_default droplist" id="u6440">
		            <div class="" id="u6440_div">
		            </div>
		            <select class="u6440_input" id="u6440_input">
		               <option class="u6440_input_option" selected="" value="Chọn 1 giá trị">
		                  Chọn 1 giá trị
		               </option>
		               <option class="u6440_input_option" value="Cơ sở vật chất">
		                  Cơ sở vật chất
		               </option>
		               <option class="u6440_input_option" value="Dụng cụ học tập">
		                  Dụng cụ học tập
		               </option>
		            </select>
		         </div>
		         <div class="ax_default droplist" id="u6441">
		            <div class="" id="u6441_div">
		            </div>
		            <select class="u6441_input" id="u6441_input">
		               <option class="u6441_input_option" selected="" value="Chọn 1 giá trị">
		                  Chọn 1 giá trị
		               </option>
		               <option class="u6441_input_option" value="Văn Quán">
		                  Văn Quán
		               </option>
		               <option class="u6441_input_option" value="Hoàng Ngân">
		                  Hoàng Ngân
		               </option>
		               <option class="u6441_input_option" value="Mai Dịch">
		                  Mai Dịch
		               </option>
		            </select>
		         </div>
		         <div class="ax_default droplist" id="u6442">
		            <div class="" id="u6442_div">
		            </div>
		            <select class="u6442_input" id="u6442_input">
		               <option class="u6442_input_option" selected="" value="Chọn 1 giá trị">
		                  Chọn 1 giá trị
		               </option>
		               <option class="u6442_input_option" value="Sách Tiếng Anh 1">
		                  Sách Tiếng Anh 1
		               </option>
		               <option class="u6442_input_option" value="Sách Tiếng Anh 2">
		                  Sách Tiếng Anh 2
		               </option>
		               <option class="u6442_input_option" value="Bàn">
		                  Bàn
		               </option>
		               <option class="u6442_input_option" value="Ghế">
		                  Ghế
		               </option>
		            </select>
		         </div>
		         <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u6443">
		            <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u6444">
		               <div class="ax_default paragraph" id="u6445">
		                  <div class="" id="u6445_div">
		                  </div>
		                  <div class="text" id="u6445_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Sản phẩm chuyển
		                        </span>
		                     </p>
		                  </div>
		               </div>
		            </div>
		            <div class="ax_default paragraph" id="u6446">
		               <div class="" id="u6446_div">
		               </div>
		               <div class="text" id="u6446_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        *
		                     </span>
		                  </p>
		               </div>
		            </div>
		         </div>
		         <div class="ax_default droplist" id="u6447">
		            <div class="" id="u6447_div">
		            </div>
		            <select class="u6447_input" id="u6447_input">
		               <option class="u6447_input_option" selected="" value="Chọn 1 giá trị">
		                  Chọn 1 giá trị
		               </option>
		               <option class="u6447_input_option" value="Kho 1">
		                  Kho 1
		               </option>
		               <option class="u6447_input_option" value="Kho 2">
		                  Kho 2
		               </option>
		               <option class="u6447_input_option" value="Kho 3">
		                  Kho 3
		               </option>
		            </select>
		         </div>
		         <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u6448">
		            <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u6449">
		               <div class="ax_default paragraph" id="u6450">
		                  <div class="" id="u6450_div">
		                  </div>
		                  <div class="text" id="u6450_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Kho xuất
		                        </span>
		                     </p>
		                  </div>
		               </div>
		            </div>
		            <div class="ax_default paragraph" id="u6451">
		               <div class="" id="u6451_div">
		               </div>
		               <div class="text" id="u6451_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        *
		                     </span>
		                  </p>
		               </div>
		            </div>
		         </div>
		         <div class="ax_default droplist" id="u6452">
		            <div class="" id="u6452_div">
		            </div>
		            <select class="u6452_input" id="u6452_input">
		               <option class="u6452_input_option" selected="" value="Chọn 1 giá trị">
		                  Chọn 1 giá trị
		               </option>
		               <option class="u6452_input_option" value="Kho 1">
		                  Kho 1
		               </option>
		               <option class="u6452_input_option" value="Kho 2">
		                  Kho 2
		               </option>
		               <option class="u6452_input_option" value="Kho 3">
		                  Kho 3
		               </option>
		            </select>
		         </div>
		         <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u6453">
		            <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u6454">
		               <div class="ax_default paragraph" id="u6455">
		                  <div class="" id="u6455_div">
		                  </div>
		                  <div class="text" id="u6455_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Kho nhập
		                        </span>
		                     </p>
		                  </div>
		               </div>
		            </div>
		            <div class="ax_default paragraph" id="u6456">
		               <div class="" id="u6456_div">
		               </div>
		               <div class="text" id="u6456_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        *
		                     </span>
		                  </p>
		               </div>
		            </div>
		         </div>
		         <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u6457">
		            <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u6458">
		               <div class="ax_default paragraph" id="u6459">
		                  <div class="" id="u6459_div">
		                  </div>
		                  <div class="text" id="u6459_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Người tạo phiếu
		                        </span>
		                     </p>
		                  </div>
		               </div>
		            </div>
		            <div class="ax_default paragraph" id="u6460">
		               <div class="" id="u6460_div">
		               </div>
		               <div class="text" id="u6460_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        *
		                     </span>
		                  </p>
		               </div>
		            </div>
		         </div>
		         <div class="ax_default text_field" id="u6461">
		            <div class="" id="u6461_div">
		            </div>
		            <input class="u6461_input" id="u6461_input" type="text" value=""/>
		         </div>
		         <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u6462">
		            <div class="ax_default paragraph" id="u6463">
		               <div class="" id="u6463_div">
		               </div>
		               <div class="text" id="u6463_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Cơ sở nhập
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default paragraph" id="u6464">
		               <div class="" id="u6464_div">
		               </div>
		               <div class="text" id="u6464_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        *
		                     </span>
		                  </p>
		               </div>
		            </div>
		         </div>
		         <div class="ax_default droplist" id="u6465">
		            <div class="" id="u6465_div">
		            </div>
		            <select class="u6465_input" id="u6465_input">
		               <option class="u6465_input_option" selected="" value="Chọn 1 giá trị">
		                  Chọn 1 giá trị
		               </option>
		               <option class="u6465_input_option" value="Văn Quán">
		                  Văn Quán
		               </option>
		               <option class="u6465_input_option" value="Hoàng Ngân">
		                  Hoàng Ngân
		               </option>
		               <option class="u6465_input_option" value="Mai Dịch">
		                  Mai Dịch
		               </option>
		            </select>
		         </div>
		         <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u6466">
		            <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u6467">
		               <div class="ax_default paragraph" id="u6468">
		                  <div class="" id="u6468_div">
		                  </div>
		                  <div class="text" id="u6468_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Số lượng
		                        </span>
		                     </p>
		                  </div>
		               </div>
		            </div>
		            <div class="ax_default paragraph" id="u6469">
		               <div class="" id="u6469_div">
		               </div>
		               <div class="text" id="u6469_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        *
		                     </span>
		                  </p>
		               </div>
		            </div>
		         </div>
		         <div class="ax_default text_field" id="u6470">
		            <div class="" id="u6470_div">
		            </div>
		            <input class="u6470_input" id="u6470_input" type="text" value=""/>
		         </div>
		      </div>
		   </div>
		</div>
		
      </div>
    );
  }
}

export default Page;
