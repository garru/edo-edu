import React, { useState } from "react";
import {connect} from "dva";
import "react-datepicker/dist/react-datepicker.css";
import {Button, Badge, Table} from "react-bootstrap";
import coso from "../../../mock/coso";
import danhmuc from "../../../mock/danhmuc";
import tukho from "../../../mock/tukho";
import denkho from "../../../mock/denkho";
import CustomModal from './modal';
import { useHistory } from 'react-router-dom';
import './index.css';
import DatePicker from "react-datepicker";
import PageHeader from "../../../components/PageHeader";

const Page = () => {
  const [isCreateNew, setIsCreateNew] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [selectedItem, setSelectedItem] = useState({});
  const [fromDate, setFromDate] = useState(new Date());
  const [toDate, setToDate] = useState(new Date());
  const history = useHistory();
  const handleButtonClick = () => {
    setIsCreateNew(true);
    setShowModal(true);
  };
  const handleViewButtonClick = () => {
    setShowModal(true);
  }
  const handleCloseModal = () => {
    setShowModal(false);
  }
  const handleImportButtonClick = () => {
    history.push('/nhap_hang');
  }
  const handleExportButtonClick = () => {
    history.push('/xuat_hang');
  }
  return (
    <div>
      <PageHeader
        title="Chuyển kho"
        breadcrums={[{
          title: "Home",
          path: "/"
        }, {
          title: "Chuyển kho",
          path: "/chuyen_kho"
        }]}
        subTitle="Quản lý danh sách sản phẩm được luân chuyển giữa các kho và tạo báo cáo chuyển kho"
      />
      <div className="iq-card n-pb-0">
        <div className="iq-card-body">
          <div className="">
            <fieldset className="m-b-2">
              <legend>Tìm kiếm</legend>
              <form action="" className="w-100">
                <div className="row">
                  <div className="col-md-4">
                    <div className="form-group">
                      <label htmlFor="coso">Cơ sở sản xuất</label>
                      <select
                        name="co_so"
                        className="form-control form-control-lg"
                        id="co_so">
                        {coso.map(item => (
                          <option value={item.value}>{item.name}</option>
                        ))}
                      </select>
                    </div>
                  </div>
                  <div className="col-md-4">
                    <div className="form-group">
                      <label htmlFor="coso">Cơ sở nhập</label>
                      <select
                        name="co_so"
                        className="form-control form-control-lg"
                        id="co_so">
                        {coso.map(item => (
                          <option value={item.value}>{item.name}</option>
                        ))}
                      </select>
                    </div>
                  </div>
                  <div className="col-md-4">
                    <div className="form-group">
                      <label htmlFor="coso">Danh mục</label>
                      <select
                        name="danhmuc"
                        className="form-control form-control-lg"
                        id="danhmuc">
                        {danhmuc.map(item => (
                          <option value={item.value}>{item.name}</option>
                        ))}
                      </select>
                    </div>
                  </div>
                  <div className="col-md-4">
                    <div className="form-group">
                      <label htmlFor="tu_kho">Luân chuyển</label>
                      <div className="row align-end">
                        <div className="col-md-6">
                          <select
                            name="tu_kho"
                            className="form-control form-control-lg"
                            id="tu_kho">
                            {tukho.map(item => (
                              <option value={item.value}>{item.name}</option>
                            ))}
                          </select>
                        </div>
                        <div className="col-md-6">
                          <select
                            name="den_kho"
                            className="form-control form-control-lg"
                            id="den_kho">
                            {denkho.map(item => (
                              <option value={item.value}>{item.name}</option>
                            ))}
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-4">
                    <div className="form-group">
                      <label htmlFor="tu_ngay">Khoảng thời gian</label>
                      <div className="row align-end">
                        <div className="col-md-6">
                          <input
                            type="date"
                            className="form-control form-control-lg"
                            id="bat_dau"
                          />
                        </div>
                        <div className="col-md-6">
                          <input
                            type="date"
                            className="form-control form-control-lg"
                            id="ket_thuc"
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-4">
                    <Button variant="primary" type="submit" onClick={handleButtonClick}>
                      Tìm kiếm
                    </Button>
                  </div>
                </div>
              </form>
            </fieldset>
            <div className="row">
              <div className="col-md-12 m-b-2">
                <Button variant="primary" className="float-right" type="submit" onClick={handleButtonClick}>
                  Thêm mới
                </Button>
              </div>
              <div className="col-md-12 ">
                <div className="w-100">
                  <Table bordered hover className="table">
                    <thead>
                    <tr>
                      <th>STT</th>
                      <th>Mã phiếu</th>
                      <th>Cơ sở xuất</th>
                      <th>Co sở nhận</th>
                      <th>Sản phẩm chuyển</th>
                      <th>Danh mục</th>
                      <th>Đơn vị</th>
                      <th>Số lượng</th>
                      <th>Kho chuyển</th>
                      <th>Kho nhận</th>
                      <th>Thời gian lập phiếu</th>
                      <th>Chi tiết</th>
                      <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                      <td>1</td>
                      <td>A-12</td>
                      <td>Hoàng Ngân</td>
                      <td>Hoàng Ngân</td>
                      <td>Sách Tiếng Anh 1</td>
                      <td>Dụng cụ học tập</td>
                      <td>Quyển</td>
                      <td>20</td>
                      <td>Kho 1</td>
                      <td>Kho 2</td>
                      <td>2021-02-23 15:52</td>
                      <td>
                        <Button variant="success">Xem</Button>
                      </td>
                      <td></td>
                    </tr>
                    <tr>
                      <td>1</td>
                      <td>A-12</td>
                      <td>Hoàng Ngân</td>
                      <td>Hoàng Ngân</td>
                      <td>Sách Tiếng Anh 1</td>
                      <td>Dụng cụ học tập</td>
                      <td>Quyển</td>
                      <td>20</td>
                      <td>Kho 1</td>
                      <td>Kho 2</td>
                      <td>2021-02-23 15:52</td>
                      <td>
                        <Button variant="success">Xem</Button>
                      </td>
                      <td></td>
                    </tr>
                    <tr>
                      <td>1</td>
                      <td>A-12</td>
                      <td>Hoàng Ngân</td>
                      <td>Hoàng Ngân</td>
                      <td>Sách Tiếng Anh 1</td>
                      <td>Dụng cụ học tập</td>
                      <td>Quyển</td>
                      <td>20</td>
                      <td>Kho 1</td>
                      <td>Kho 2</td>
                      <td>2021-02-23 15:52</td>
                      <td>
                        <Button variant="success" onClick={handleButtonClick}>Xem</Button>
                      </td>
                      <td></td>
                    </tr>
                    </tbody>
                  </Table>
                  <div className="not-found">
                    <i className="fas fa-inbox"></i>
                    <span>Không tìm thấy kết quả</span>
                  </div>
                  {showModal && (<CustomModal isCreateNew={isCreateNew} selectedItem={selectedItem}
                                              handleCloseModal={handleCloseModal}/>)}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default connect(({Mock}) => ({
  Mock
}))(Page);
