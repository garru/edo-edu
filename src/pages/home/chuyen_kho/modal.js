import React from "react";
import { Modal, Button } from "react-bootstrap";
import coso from "../../../mock/coso";
import danhmuc from "../../../mock/danhmuc";
export default function CustomModal({ isCreateNew, selectedItem, handleCloseModal }) {
  // const [showModal, setShowModal] = useState(true);
  const handleClose = function() {
    handleCloseModal();
  };
  return (
    <Modal show>
      <Modal.Header closeButton>
        <Modal.Title>
          {isCreateNew ? "Thêm mới" : "Chỉnh sửa"}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div>
          <form>
            <div className="iq-card ">
              <div className="row m-b-2">
                <div className="col-12">
                  <div className="form-group">
                    <label htmlFor="ten_kho">Tên</label>
                    <input
                      type="text"
                      name="ten"
                      className="form-control form-control-lg"
                      id="ten"
                      placeholder="Nhập tên"
                      defaultValue={!isCreateNew ? selectedItem.ten : ""}/>
                  </div>
                  <div className="form-group">
                    <label htmlFor="dia_chi">Địa chỉ</label>
                    <input
                      type="text"
                      name="dia_chi"
                      className="form-control form-control-lg"
                      id="dia_chi"
                      placeholder="Nhập"
                      defaultValue={!isCreateNew ? selectedItem.dia_chi : ""}/>
                  </div>
                  <div className="form-group">
                    <label htmlFor="ten_kho">Số điện thoại</label>
                    <input
                      type="text"
                      name="so_dien_thoai"
                      className="form-control form-control-lg"
                      id="so_dien_thoai"
                      placeholder="Nhập số điện thoại"
                      defaultValue={!isCreateNew ? selectedItem.so_dien_thoai : ""}/>
                  </div>
                  <div className="form-group">
                    <label htmlFor="ten_kho">Mã số thuế</label>
                    <input
                      type="text"
                      name="ma_so_thue"
                      className="form-control form-control-lg"
                      id="ma_so_thue"
                      placeholder="Nhập mã số thuế"
                      defaultValue={!isCreateNew ? selectedItem.ma_so_thue : ""}/>
                  </div>
                  <div className="form-group">
                    <label htmlFor="ten_kho">Số tài khoản</label>
                    <input
                      type="text"
                      name="so_tai_khoan"
                      className="form-control form-control-lg"
                      id="so_tai_khoan"
                      placeholder="Số tài khoản"
                      defaultValue={!isCreateNew ? selectedItem.so_tai_khoan : ""}/>
                  </div>
                  <div className="form-group">
                    <label htmlFor="ten_kho">Cung cấp</label>
                    <input
                      type="text"
                      name="cung_cap"
                      className="form-control form-control-lg"
                      id="cung_cap"
                      placeholder="Sản phẩm cung cấp"
                      defaultValue={!isCreateNew ? selectedItem.cung_cap : ""}/>
                  </div>
                  <div className="form-group">
                    <label htmlFor="danh_muc_chuc_nang">Danh mục</label>
                    <select
                      name="danh_muc_chuc_nang"
                      className="form-control form-control-lg"
                      id="danh_muc_chuc_nang"
                      defaultValue={!isCreateNew ? selectedItem.danh_muc_chuc_nang : ""}>
                      {danhmuc.map(item =>  (
                        <option value={item.value}>{item.name}</option>
                      ))}
                    </select>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="primary" style={{marginRight: 15}}>
          {isCreateNew ? "Tạo mới" : "Cập nhật"}
        </Button>
        <Button variant={"secondary"} onClick={handleClose}>
          Hủy
        </Button>
      </Modal.Footer>
    </Modal>
  );
}
