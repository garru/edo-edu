import React from "react";
import { connect } from "dva";
import { Button } from "react-bootstrap";
import {
  NOI_SINH,
  NGUYEN_QUAN,
  QUOC_TICH,
  HON_NHAN,
  TRUNG_TAM,
  PHONG_BAN,
  VI_TRI,
  HOP_DONG,
  LOAI
} from "../../../../mock/dropdown";
import MultiSelect from "react-multi-select-component";
import moment from "moment";

class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: []
    };
  }

  componentWillMount() {}

  render() {
    const options = [
      { label: "Hội sở chính", value: "Hội sở chính" },
      { label: "Hoàng Ngân", value: "Hoàng Ngân" },
      { label: "Văn Điển", value: "Văn Điển" }
    ];

    const ArrowRenderer = props => {
      console.log("this got called");
      const { isOpen } = props;
      return;
    };

    console.log("render account_header>>>");
    return (
      <div className="row">
        <form style={{ width: "100%" }}>
          <div className="col-12 m-b-2">
            <fieldset>
              <legend>Tìm kiếm</legend>
              <div className="row align-end">
                <div className="col-md-3 col-sm-6 col-xs-6 ">
                  <div className="form-group">
                    <label htmlFor="ten_phong">Tên phòng:</label>
                    <MultiSelect
                      options={options}
                      value={this.state.selected}
                      onChange={value => this.setState({ selected: value })}
                      labelledBy="Select"
                      overrideStrings={{
                        allItemsAreSelected: "Tất cả",
                        clearSearch: "Clear Search",
                        noOptions: "Không tìm thấy",
                        search: "Search",
                        selectAll: "Select All",
                        selectSomeItems: "Có thể chọn nhiều giá trị"
                      }}
                      ArrowRenderer={() => <i className="fa fa-chevron-down" />}
                    />
                  </div>
                </div>

                <div className="col-md-3 col-sm-6 col-xs-6 ">
                  <div className="form-group">
                    <label htmlFor="loai">Loại:</label>
                    <select
                      className="form-control form-control-lg"
                      id="loai"
                      name="loai"
                    >
                      {LOAI.map(item => (
                        <option
                          value={item.code}
                          disabled={item.code !== "" ? false : true}
                          selected={item.code !== "" ? false : true}
                          hidden={item.code !== "" ? false : true}
                        >
                          {item.text}
                        </option>
                      ))}
                    </select>
                  </div>
                </div>
                <div className="col-md-4 col-sm-6 ">
                  <div className="form-group">
                    <label>Khoảng thời gian:</label>
                    <div className="row">
                      <div className="col-6">
                        <input
                          type="date"
                          className="form-control form-control-lg"
                          id="exampleInputdate"
                          defaultValue={moment().format("YYYY-MM-DD")}
                        />
                      </div>
                      <div className="col-6">
                        <input
                          type="date"
                          className="form-control form-control-lg"
                          id="exampleInputdate"
                          defaultValue={moment().format("YYYY-MM-DD")}
                        />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-md-2 col-sm-6">
                  <div className="form-group">
                    <Button variant="primary" type="submit">
                      Tìm kiếm
                    </Button>
                  </div>
                </div>
              </div>
            </fieldset>
          </div>
        </form>
      </div>
    );
  }
}

export default connect(({ HealthcareService }) => ({
  HealthcareService
}))(Page);
