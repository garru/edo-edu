import React from "react";
import { connect } from "dva";
import RoomHeader from "./promotion_header";
import RoomBody from "./promotion_body";
import PageHeader from "../../../components/PageHeader";

class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillMount() {}

  render() {
    const bre = {
      title: "Các chương trình khuyến mại phụ thu",
      breadcrums: [
        {
          title: "Home",
          path: "/"
        },
        {
          title: "Các chương trình khuyến mại phụ thu",
          path: ""
        }
      ],
      subTitle: "Quản lý các gói khuyến mại, phụ thu được sử dụng trong chuỗi trung tâm và từng trung tâm"
    };
    console.log("render account>>>");
    return (
      <div>
        <PageHeader {...bre}></PageHeader>
        <div className="iq-card">
          <div className="iq-card-body">
            <RoomHeader />
            <RoomBody />
          </div>
        </div>
      </div>
    );
  }
}

export default connect(({ HealthcareService }) => ({
  HealthcareService
}))(Page);
