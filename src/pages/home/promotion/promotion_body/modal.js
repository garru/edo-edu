import React, { Component, useState, useRef, useEffect } from "react";
import { useForm } from "react-hook-form";
import { Modal, Button } from "react-bootstrap";
import moment from "moment";
import MultiSelect from "react-multi-select-component";
import { SAN_PHAM_DI_KEM } from "./data";

export default function CustomModal(props) {
  const [show, setShow] = useState(true);
  const [selected, setSelected] = useState([]);
  const handleClose = () => {
    // setShow(false);
    props.onClose();
  };
  const { register, handleSubmit, errors } = useForm();
  const emailReg = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  const { isCreateNew, selectedItem } = props;
  return (
    <Modal
      size={"lg"}
      show={show}
      onHide={handleClose}
      backdrop="static"
      keyboard={false}
      id="AddNew"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title>
          {isCreateNew
            ? "Thêm mới chương trình khuyến mại, phụ thu"
            : "Chỉnh sửa chương trình khuyến mại, phụ thu"}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <form>
          <div className="row m-b-2">
            <div className="col-12">
              <div className="row font-size-14">
                <div className="col-6 ">
                  <div className="form-group">
                    <label htmlFor="ten">Tên chương trình:</label>
                    <input
                      type="text"
                      name="ten"
                      className="form-control form-control-lg"
                      id="ten"
                      defaultValue={!isCreateNew ? selectedItem.ten : ""}
                      // ref={register({ required: true, minLength: 8 })}
                    ></input>
                  </div>
                  <div className="form-group">
                    <label htmlFor="loai_phong">Loại chương trình:</label>
                    <div className="row">
                      <div className="col-6">
                        <div className="custom-control custom-radio">
                          <input
                            className="custom-control-input"
                            id="khuyen_mai"
                            name="loai_chuong_trinh"
                            type="radio"
                            value="khuyen_mai"
                          />
                          <label
                            className="custom-control-label"
                            htmlFor="khuyen_mai"
                          >
                            Khuyến mãi
                          </label>
                        </div>
                      </div>
                      <div className="col-6">
                        <div className="custom-control custom-radio">
                          <input
                            className="custom-control-input"
                            id="phu_thu"
                            name="loai_chuong_trinh"
                            type="radio"
                            value="phu_thu"
                          />
                          <label
                            className="custom-control-label"
                            htmlFor="phu_thu"
                          >
                            Phụ thu
                          </label>
                        </div>
                      </div>
                    </div>
                  
                  </div>
                </div>
                <div className="col-6 ">
                  <div className="form-group">
                    <label htmlFor="mo_ta">Mô tả</label>
                    <textarea
                      className="textarea form-control"
                      rows="4"
                      name="mo_ta"
                      defaultValue={!isCreateNew ? selectedItem.mo_ta : ""}
                    ></textarea>
                  </div>
                </div>
                <div className="col-6 ">
                  <div className="form-group">
                    <div className="p-0">
                      <label>Mã khuyễn mại:</label>
                    </div>
                    <div className="row">
                      <div className="col-5">
                        <div className="custom-control custom-radio">
                          <input
                            className="custom-control-input"
                            id="nhieu_ma"
                            name="ma_khuyen_mai"
                            type="radio"
                            value="nhieu_ma"
                          />
                          <label
                            className="custom-control-label"
                            htmlFor="nhieu_ma"
                          >
                            Nhiều mã
                          </label>
                        </div>
                      </div>
                      <div className="col-4">
                        <div className="custom-control custom-radio">
                          <input
                            className="custom-control-input"
                            id="mot_ma"
                            name="ma_khuyen_mai"
                            type="radio"
                            value="mot_ma"
                          />
                          <label
                            className="custom-control-label"
                            htmlFor="mot_ma"
                          >
                            Một mã
                          </label>
                        </div>
                      </div>
                      <div className="col-3">
                        <div className="custom-control custom-radio">
                          <input
                            className="custom-control-input"
                            id="khong"
                            name="ma_khuyen_mai"
                            type="radio"
                            value="khong"
                          />
                          <label
                            className="custom-control-label"
                            htmlFor="khong"
                          >
                            Không
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="form-group">
                    <div className="p-0">
                      <label>Kiểu xuất mã:</label>
                    </div>
                    <div className="row">
                      <div className="col-6">
                        <div className="custom-control custom-radio">
                          <input
                            className="custom-control-input"
                            id="tu_nhap"
                            name="kieu_xuat_ma"
                            type="radio"
                            value="tu_nhap"
                          />
                          <label
                            className="custom-control-label"
                            htmlFor="tu_nhap"
                          >
                            Tự nhập
                          </label>
                        </div>
                      </div>
                      <div className="col-6">
                        <div className="custom-control custom-radio">
                          <input
                            className="custom-control-input"
                            id="khoi_tao_tu_dong"
                            name="kieu_xuat_ma"
                            type="radio"
                            value="khoi_tao_tu_dong"
                          />
                          <label
                            className="custom-control-label"
                            htmlFor="khoi_tao_tu_dong"
                          >
                            Khởi tạo tự động
                          </label>
                        </div>
                      </div>
                      <div className="col-12">
                        <p>
                          <i>
                            Với khởi tạo tự động, người dùng sẽ tải xuống danh
                            sách mã
                          </i>
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="form-group">
                    <label>Áp dụng:</label>
                    <div className="row">
                      <div className="col-6">
                        <input
                          type="date"
                          className="form-control form-control-lg"
                          id="exampleInputdate"
                          defaultValue={moment().format("YYYY-MM-DD")}
                        />
                      </div>
                      <div className="col-6">
                        <input
                          type="date"
                          className="form-control form-control-lg"
                          id="exampleInputdate"
                          defaultValue={moment().format("YYYY-MM-DD")}
                        />
                      </div>
                    </div>
                  </div>
                  <div className="form-group">
                    <div className="p-0">
                      <label>Loại chiết khấu:</label>
                    </div>
                    <div className="row">
                      <div className="col-6">
                        <div className="custom-control custom-radio">
                          <input
                            className="custom-control-input"
                            id="ty_le"
                            name="loai_chiet_khau"
                            type="radio"
                            value="ty_le"
                          />
                          <label
                            className="custom-control-label"
                            htmlFor="ty_le"
                          >
                            Tỷ lệ
                          </label>
                        </div>
                      </div>
                      <div className="col-6">
                        <div className="custom-control custom-radio">
                          <input
                            className="custom-control-input"
                            id="truc_tiep"
                            name="loai_chiet_khau"
                            type="radio"
                            value="truc_tiep"
                          />
                          <label
                            className="custom-control-label"
                            htmlFor="truc_tiep"
                          >
                            Trực tiếp
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="form-group">
                    <label>Giá trị:</label>
                    <input
                      type="text"
                      name="gia_tri"
                      className="form-control form-control-lg"
                      id="gia_tri"
                      defaultValue={!isCreateNew ? selectedItem.gia_tri : ""}
                      // ref={register({ required: true, minLength: 8 })}
                    ></input>
                  </div>
                </div>
                <div className="col-6 ">
                  <div className="form-group">
                    <label htmlFor="so_luong_ma">Số lượng mã:</label>
                    <input
                      type="text"
                      name="so_luong_ma"
                      className="form-control form-control-lg"
                      id="so_luong_ma"
                      defaultValue={
                        !isCreateNew ? selectedItem.so_luong_ma : ""
                      }
                      // ref={register({ required: true, minLength: 8 })}
                    ></input>
                  </div>
                  <div className="form-group">
                    <label htmlFor="danh_sach_ma">Danh sách mã</label>
                    <textarea
                      className="textarea form-control"
                      rows="4"
                      name="danh_sach_ma"
                      defaultValue={
                        !isCreateNew ? selectedItem.danh_sach_ma : ""
                      }
                    ></textarea>
                  </div>
                  <div className="form-group">
                    <label htmlFor="ten_phong">Sản phẩm kèm:</label>
                    <MultiSelect
                      options={SAN_PHAM_DI_KEM}
                      value={selected}
                      onChange={setSelected}
                      labelledBy="Select"
                      overrideStrings={{
                        allItemsAreSelected: "Tất cả",
                        clearSearch: "Clear Search",
                        noOptions: "Không tìm thấy",
                        search: "Search",
                        selectAll: "Select All",
                        selectSomeItems: "Có thể chọn nhiều sản phẩm"
                      }}
                      ArrowRenderer={() => <i className="fa fa-chevron-down" />}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="primary" style={{marginRight: 15}}>
          {isCreateNew ? "Tạo mới" : "Cập nhật"}
        </Button>
        <Button variant={"secondary"} onClick={handleClose}>
          Hủy
        </Button>
      </Modal.Footer>
    </Modal>
  );
}
