import React from "react";
import { connect } from "dva";
import { Button, Table, Overlay, Popover } from "react-bootstrap";
import { DATA, HEADER, LOAI_TRUNG_TAM } from "./data";
import CustomModal from "./modal";
import * as _ from "lodash";

class Page extends React.Component {
  constructor(props) {
    super(props);
    const initCheckedItem = DATA.map(item => false);
    this.state = {
      checkedAll: false,
      selectedItem: null,
      checkedItems: _.cloneDeep(initCheckedItem),
      show: _.cloneDeep(initCheckedItem),
      listItem: DATA,
      target: false,
      activeIndex: null,
      showModal: false
    };
  }

  checkAllCheckboxChecked() {
    const uncheckedCheckboxIndex = this.state.checkedItems.findIndex(
      item => !item
    );
    return uncheckedCheckboxIndex !== -1;
  }

  updateCheckedItem(e, index) {
    let newCheckedItems = this.state.checkedItems;
    const value = e.target.checked;
    newCheckedItems[index] = value;
    this.setState({ checkedItems: newCheckedItems });
    if (value) {
      const isCheckedAll = this.checkAllCheckboxChecked();
      isCheckedAll && this.setState({ checkedAll: true });
    } else {
      this.setState({ checkedAll: false });
    }
  }

  checkedAll(e) {
    const value = e.target.checked;
    let newCheckedItems = [];
    this.setState({ checkedAll: value });
    newCheckedItems = this.state.checkedItems.map(item => value);
    this.setState({ checkedItems: newCheckedItems });
  }

  renderTooltip(index) {
    return (
      <Overlay
        show={this.state.show[index]}
        target={this.state.target}
        placement="bottom"
        containerPadding={20}
        rootClose={true}
        rootCloseEvent={"click"}
        onHide={e => this.handleClick(e, index)}
      >
        <Popover id="popover-contained">
          <Popover.Content>
            <ul>
              <li
                onClick={e => {
                  this.handleClick(e, index, true);
                  this.setState({ isCreateNew: false, showModal: true });
                }}
              >
                <i
                  className="fa fa-edit"
                  style={{
                    marginRight: 0,
                    marginLeft: "auto"
                  }}
                />
                &nbsp; Chỉnh sửa
              </li>

              <li
                onClick={e => {
                  this.handleClick(e, index, true);
                }}
              >
                <i
                  className="fa fa-trash-alt"
                  style={{
                    marginRight: 0,
                    marginLeft: "auto"
                  }}
                />
                &nbsp; Xoá
              </li>
            </ul>
          </Popover.Content>
        </Popover>
      </Overlay>
    );
  }

  handleClick(e, index, isHide) {
    let { show, activeIndex, listItem } = this.state;
    show[index] = !show[index];
    index !== activeIndex && (show[activeIndex] = false);
    this.setState({
      show: show,
      target: isHide ? null : e.target,
      activeIndex: index,
      selectedItem: listItem[index]
    });
  }

  onClose = () => {
    this.setState({ isCreateNew: false, showModal: false });
  };

  render() {
    console.log("render account_body>>>");
    let listSelect = [];
    return (
      <div className="row">
        <div className="col-9">
          <div className="form-group">
            <Button
              variant="primary"
              type="submit"
              style={{ marginRight: 30 }}
              onClick={() =>
                this.setState({ isCreateNew: true, showModal: true })
              }
            >
              Thêm mới
            </Button>
          </div>
        </div>
        <div className="col-12">
          <div style={{ width: "100%" }}>
            <div style={{ overflowX: "auto", width: "100%" }}>
              <Table
                bordered
                hover
                // style={{ minWidth: 1500 }}
                className="table"
              >
                <thead>
                  <tr>
                    {HEADER.map(item => (
                      <th
                        className={`${item.class} ${
                          item.hasSort ? "sort" : ""
                        } ${this.state.sortField === item.key ? "active" : ""}`}
                        key={item.key}
                      >
                        <span>{item.title}</span>
                      </th>
                    ))}
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.listItem.map((item, index) => (
                    <React.Fragment>
                      <tr key={`account_${item.id}`}>
                        <td>{item.ten}</td>
                        <td>{item.trung_tam}</td>
                        <td>{item.ma_so}</td>
                        <td>
                          {item.ngay_bat_dau} - {item.ngay_ket_thuc}
                        </td>
                        <td>{item.gia_tri}</td>
                        <td>
                          <div className="status">Active</div>
                        </td>
                        <td>
                          <i
                            className="fa fa-ellipsis-h"
                            style={{
                              marginRight: 0,
                              marginLeft: "auto"
                            }}
                            onClick={e => this.handleClick(e, index)}
                          />
                          {this.renderTooltip(index)}
                        </td>
                      </tr>
                    </React.Fragment>
                  ))}
                </tbody>
              </Table>
              {this.state.listItem && this.state.listItem.length ? (
                ""
              ) : (
                <div className="not-found">
                  <i class="fas fa-inbox"></i>
                  <span>Không tìm thấy kết quả</span>
                </div>
              )}
            </div>
          </div>
        </div>
        {this.state.showModal && (
          <CustomModal
            isCreateNew={this.state.isCreateNew}
            onClose={this.onClose}
            selectedItem={this.state.selectedItem}
          ></CustomModal>
        )}
      </div>
    );
  }
}

export default connect(({ HealthcareService }) => ({
  HealthcareService
}))(Page);
