export const HEADER = [
  {
    title: "Tên",
    key: "ten",
    class: "tb-width-400"
  },
  {
    title: "Trung tâm áp dụng",
    key: "trung_tam",
    class: "tb-width-200"
  },
  {
    title: "Mã số",
    key: "ma_so",
    class: "tb-width-100"
  },
  {
    title: "Ngày áp dụng",
    key: "ngay_ap_dung",
    class: "tb-width-300"
  },
  {
    title: "Giá trị",
    key: "gia_tri",
    class: "tb-width-150"
  },
  {
    title: "Trạng thái",
    key: "trang_thai",
    class: "tb-width-150"
  }
];

export const DATA = [
  {
    id: "1",
    ten: "Mickey",
    trung_tam: "Hoàng Ngân",
    ma_so: "SUSAN0175",
    ngay_bat_dau: "2021-01-21",
    ngay_ket_thuc: "2021-01-22",
    gia_tri: "500000",
    trang_thai: "active"
  },
  {
    id: "1",
    ten: "Mickey",
    trung_tam: "Hoàng Ngân",
    ma_so: "SUSAN0175",
    ngay_bat_dau: "2021-01-21",
    ngay_ket_thuc: "2021-01-22",
    gia_tri: "500000",
    trang_thai: "active"
  },
  {
    id: "1",
    ten: "Mickey",
    trung_tam: "Hoàng Ngân",
    ma_so: "SUSAN0175",
    ngay_bat_dau: "2021-01-21",
    ngay_ket_thuc: "2021-01-22",
    gia_tri: "500000",
    trang_thai: "active"
  },
  {
    id: "1",
    ten: "Mickey",
    trung_tam: "Hoàng Ngân",
    ma_so: "SUSAN0175",
    ngay_bat_dau: "2021-01-21",
    ngay_ket_thuc: "2021-01-22",
    gia_tri: "500000",
    trang_thai: "active"
  },
  {
    id: "1",
    ten: "Mickey",
    trung_tam: "Hoàng Ngân",
    ma_so: "SUSAN0175",
    ngay_bat_dau: "2021-01-21",
    ngay_ket_thuc: "2021-01-22",
    gia_tri: "500000",
    trang_thai: "active"
  }
];

export const SAN_PHAM_DI_KEM = [
  {
    label: "Cặp sách",
    value: 1
  },
  {
    label: "Bộ sách học",
    value: 2
  }
];
