import React, { useState, useEffect } from "react";
import { Modal, Button } from "react-bootstrap";
import Rating from "react-rating";
import { connect } from "dva";
import CustomTable from "../../../components/Table";
import { Formik, Field, Form } from "formik";
import Input from "../../../components/Form/Input";
import Select from "../../../components/Form/Select";
import TextArea from "../../../components/Form/TextArea";
import * as yup from "yup";
import moment from "moment";

const CustomModal = ({
  handleClose,
  CustomerDetail,
  StudentReserve,
  dispatch,
  id,
  classId
}) => {
  const [show, setShow] = useState(true);

  useEffect(() => {
    dispatch({
      type: "Global/showLoading"
    });
    dispatch({
      type: "StudentReserve/viewreserve",
      payload: {
        id,
        classId
      }
    });
    dispatch({
      type: "StudentReserve/listmonth",
      payload: {}
    }).then(data => {
      dispatch({
        type: "Global/hideLoading"
      });
    });
  }, []);

  const getTime = time => {
    const now = moment();
    return time
      ? moment(now)
          .add(time, "M")
          .format("DD-MM-YYYY")
      : "";
  };

  return (
    <Modal
      size={"md"}
      show={show}
      onHide={handleClose}
      backdrop="static"
      keyboard={false}
      id="ForgotPassword"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title>Bảo lưu</Modal.Title>
      </Modal.Header>
      <Formik
        enableReinitialize={true}
        validationSchema={yup.object().shape({
          reserveReason: yup.string().required("Trường bắt buộc"),
          reserveMonth: yup.string().required("Trường bắt buộc")
        })}
        initialValues={{
          reserveReason: "",
          reserveMonth: ""
        }}
        onSubmit={(values, { setSubmitting }) => {
          dispatch({
            type: "Global/showLoading"
          });
          dispatch({
            type: "StudentReserve/insertreserve",
            payload: {
              studentId: +id,
              classId: +classId,
              id: +id,
              reserveReason: values.reserveReason,
              reserveMonth: +values.reserveMonth
            }
          }).then(data => {
            dispatch({
              type: "Global/hideLoading"
            });
            if (data) {
              dispatch({
                type: "Global/showSuccess"
              });
              handleClose(true);
            } else {
              dispatch({
                type: "Global/showError"
              });
            }
          });
        }}
      >
        {({
          values,
          handleChange,
          handleBlur,
          handleSubmit
          /* and other goodies */
        }) => (
          <Form style={{ width: "100%" }} onSubmit={handleSubmit}>
            <Modal.Body>
              <div className="row g-2">
                <div className="col-md-6 col-sm-12">
                  <p>
                    Số buổi còn lại:{" "}
                    <b>{StudentReserve?.viewreserve?.lessonRemain}</b>
                  </p>
                </div>
                <div className="col-md-6 col-sm-12">
                  <p>
                    Số credit còn lại:{" "}
                    <b>
                      {StudentReserve?.viewreserve?.creditRemain?.toLocaleString(
                        "it-IT",
                        {
                          style: "currency",
                          currency: "VND"
                        }
                      )}
                    </b>
                  </p>
                </div>
              </div>

              <div className="row ">
                <div className="col-12">
                  <TextArea
                    id="reserveReason"
                    name="reserveReason"
                    label="Lí do bảo lưu:"
                    value={values.reserveReason}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                </div>
                <div className="col-md-6 ">
                  <Select
                    id="reserveMonth"
                    name="reserveMonth"
                    label="Thời gian bảo lưu:"
                    options={StudentReserve?.listmonth}
                    value={values.reserveMonth}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                </div>
                <div className="col-md-6 ">
                  <p>
                    Dự định đi học lại:
                    <b> {getTime(values.reserveMonth)}</b>
                  </p>
                </div>
              </div>
            </Modal.Body>
            <Modal.Footer>
              <Button
                variant={"primary"}
                style={{ marginRight: 15 }}
                onClick={handleSubmit}
              >
                Lưu
              </Button>
              <Button variant={"secondary"} onClick={handleClose}>
                Đóng
              </Button>
            </Modal.Footer>
          </Form>
        )}
      </Formik>
    </Modal>
  );
};

export default connect(
  ({ CustomerDetail, StudentReserve, ScheduleAttendance, Global }) => ({
    CustomerDetail,
    StudentReserve,
    ScheduleAttendance,
    Global
  })
)(CustomModal);
