import React, { useState, useEffect } from "react";
import { Button, Nav } from "react-bootstrap";
import { connect } from "dva";
import "react-datepicker/dist/react-datepicker.css";
import CustomModal from "./modal";
import ModalBaoLuu from "./modal_bao_luu";
import ModalChuyenLop from "./modal_chuyen_lop";
import ModalHocBu from "./modal_hoc_bu";
import CustomTable from "../../../components/Table";
import UploadAvatar from "../../../components/UploadAvatar";
import { useParams } from "react-router-dom";
import PageHeader from "../../../components/PageHeader";
import moment from "moment";

const Page = props => {
  const [showModal, setShowModal] = useState(false);
  const [baoLuu, setbaoLuu] = useState(false);
  const [chuyenLop, setChuyenLop] = useState(false);
  const [hocBu, setHocBu] = useState(false);
  const [count, setCount] = useState(0);
  const { id, classId } = useParams();
  const { dispatch, StudentDetail } = props;
  const [selectedSession, setSelectedSession] = useState(null);
  const bre = {
    title: "Chi tiết học viên",
    breadcrums: [
      {
        title: "Home",
        path: "/"
      },
      {
        title: "Chi tiết học viên",
        path: ""
      }
    ]
  };
  useEffect(() => {
    dispatch({
      type: "Global/showLoading"
    });
    Promise.all([
      dispatch({
        type: "StudentDetail/currentstudent",
        payload: { id, classId }
      }),
      dispatch({
        type: "StudentDetail/currentschedule",
        payload: { id, classId }
      })
    ]).then(data => {
      dispatch({
        type: "Global/hideLoading"
      });
    });
  }, [count]);

  const handleCloseModal = data => {
    setShowModal(false);
    setbaoLuu(false);
    if (data) {
      dispatch({
        type: "Global/showLoading"
      });
      dispatch({
        type: "StudentDetail/currentschedule",
        payload: { id, classId }
      }).then(data => {
        dispatch({
          type: "Global/hideLoading"
        });
      });
    }
  };

  const columns = [
    {
      title: "Buổi học",
      key: "lessonName",
      class: "tb-width-150"
    },
    {
      title: "Nội dung",
      key: "lessonDesc",
      class: "tb-width-300"
    },
    {
      title: "Ngày học",
      key: "lessonDate",
      class: "tb-width-100",
      render: (col, index) => (
        <span>
          {col.lessonDate ? moment(col.lessonDate).format("DD-MM-YYYY") : ""}
        </span>
      )
    },
    {
      title: "Phòng học",
      key: "roomName",
      class: "tb-width-100"
    },
    {
      title: "Giáo viên VN",
      key: "teacherDomestic",
      class: "tb-width-150"
    },
    {
      title: "Giáo viên NN",
      key: "teacherForeign",
      class: "tb-width-150"
    },
    {
      title: "Trạng thái",
      key: "status",
      class: "tb-width-150",
      render: (col, index) =>
        col.isDone ? (
          <span>Đã học</span>
        ) : (
          <div
            className="status"
            onClick={() => {
              setSelectedSession(col);
              setHocBu(true);
            }}
          >
            Nghỉ/Học bù
          </div>
        )
    }
  ];

  return (
    <>
      <PageHeader {...bre} />
      <div className="iq-card ">
        <div className="iq-card-body">
          <div className="row">
            <div className="col-12">
              <fieldset>
                <legend>Thông tin cơ bản</legend>
                <div className="row">
                  <div className="col-md-4 col-sm-6 col-xs-12">
                    <p>
                      Họ và tên:
                      <b> {StudentDetail?.currentstudent?.fullName}</b>
                    </p>
                  </div>
                  <div className="col-md-4 col-sm-6 col-xs-12">
                    <p>
                      Lớp:
                      <b> {StudentDetail?.currentstudent?.className}</b>
                    </p>
                  </div>
                  <div className="col-md-4 col-sm-6 col-xs-12">
                    <p>
                      Số buổi còn lại :
                      <b> {StudentDetail?.currentstudent?.lessonRemain}</b>
                    </p>
                  </div>
                  <div className="col-md-4 col-sm-6 col-xs-12">
                    <p>
                      Trung tâm :
                      <b> {StudentDetail?.currentstudent?.centerName}</b>
                    </p>
                  </div>
                  <div className="col-md-4 col-sm-6 col-xs-12">
                    <p>
                      Giáo trình:
                      <b> {StudentDetail?.currentstudent?.programName}</b>
                    </p>
                  </div>
                  <div className="col-md-4 col-sm-6 col-xs-12">
                    <p>
                      Trạng thái:
                      <b> {StudentDetail?.currentstudent?.status}</b>
                    </p>
                  </div>
                  <div className="col-md-4 col-sm-6 col-xs-12">
                    <Button
                      variant="primary"
                      style={{ marginRight: 15 }}
                      onClick={() => setbaoLuu(true)}
                    >
                      Bảo lưu/Đi học lại
                    </Button>
                    <Button
                      variant="primary"
                      onClick={() => setChuyenLop(true)}
                    >
                      Chuyển lớp
                    </Button>
                  </div>
                </div>
              </fieldset>
            </div>
            <div className="col-12  m-t-2">
              <Nav
                fill
                variant="pills"
                className="m-b-2"
                defaultActiveKey={`/diem_danh_hv/${id}/${classId}`}
              >
                <Nav.Item>
                  <Nav.Link
                    key="diem_danh_hv"
                    href={`/diem_danh_hv/${id}/${classId}`}
                  >
                    Thảo luận Lịch học/điểm danh
                  </Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link
                    key="bao_luu_hv"
                    href={`/bao_luu_hv/${id}/${classId}`}
                  >
                    Bảo lưu
                  </Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link key="vi_hv" href={`/vi_hv/${id}/${classId}`}>
                    Thông tin ví
                  </Nav.Link>
                </Nav.Item>
              </Nav>
            </div>

            <div className="col-12">
              <div className="row ">
                <div className="col-lg-3 col-md-12 col-sm-12 text-center">
                  <UploadAvatar
                    src={StudentDetail?.currentstudent?.avatar}
                  ></UploadAvatar>
                </div>
                <div className="col-lg-9 col-md-12">
                  <CustomTable
                    dataSource={props.StudentDetail?.currentschedule}
                    total={0}
                    columns={columns}
                    onChange={data => {}}
                    noPaging
                  ></CustomTable>
                </div>
              </div>
              {baoLuu && (
                <ModalBaoLuu
                  handleClose={handleCloseModal}
                  id={id}
                  classId={classId}
                />
              )}
              {chuyenLop && (
                <ModalChuyenLop
                  handleClose={handleCloseModal}
                  id={id}
                  classId={classId}
                />
              )}
              {hocBu && (
                <ModalHocBu
                  handleClose={handleCloseModal}
                  id={id}
                  classId={classId}
                  selectedSession={selectedSession}
                />
              )}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default connect(
  ({ Student, StudentReserve, StudentDetail, Global }) => ({
    Student,
    StudentReserve,
    StudentDetail,
    Global
  })
)(Page);
