import React, { useState, useEffect } from "react";
import { Modal, Button } from "react-bootstrap";
import Rating from "react-rating";
import { connect } from "dva";
import CustomTable from "../../../components/Table";
import { Formik, Field, Form } from "formik";
import Input from "../../../components/Form/Input";
import Radio from "../../../components/Form/Radio";
import Select from "../../../components/Form/Select";
import TextArea from "../../../components/Form/TextArea";
import * as yup from "yup";
import moment from "moment";

const CustomModal = ({
  handleClose,
  CustomerDetail,
  StudentReserve,
  dispatch,
  id,
  classId,
  selectedSession
}) => {
  const [show, setShow] = useState(true);

  useEffect(() => {
    dispatch({
      type: "Global/showLoading"
    });
    Promise.all([
      dispatch({
        type: "StudentReserve/viewreserve",
        payload: {
          id,
          classId
        }
      }),
      dispatch({
        type: "StudentReserve/listmonth",
        payload: {}
      }),
      dispatch({
        type: "StudentReserve/listprogram",
        payload: {}
      })
    ]).then(data => {
      dispatch({
        type: "Global/hideLoading"
      });
    });
  }, []);

  const onChangeProgram = id => {
    dispatch({
      type: "Global/showLoading"
    });
    dispatch({
      type: "StudentReserve/listclass",
      payload: { programId: id, centerId: selectedSession.classId }
    }).then(data => {
      dispatch({
        type: "Global/hideLoading"
      });
      !data &&
        dispatch({
          type: "Global/showError"
        });
    });
  };

  const getTime = time => {
    const now = moment();
    return time
      ? moment(now)
          .add(time, "M")
          .format("DD-MM-YYYY")
      : "";
  };

  return (
    <Modal
      size={"md"}
      show={show}
      onHide={handleClose}
      backdrop="static"
      keyboard={false}
      id="ForgotPassword"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title>Bảo lưu</Modal.Title>
      </Modal.Header>
      <Formik
        enableReinitialize={true}
        validationSchema={yup.object().shape({
          reason: yup.string().required("Trường bắt buộc"),
          reserveMonth: yup.string().required("Trường bắt buộc")
        })}
        initialValues={{
          reason: "",
          reserveMonth: ""
        }}
        onSubmit={(values, { setSubmitting }) => {
          dispatch({
            type: "Global/showLoading"
          });
          dispatch({
            type: "StudentReserve/insertreserve",
            payload: {
              studentId: +id,
              classId: +classId,
              id: +id,
              reason: values.reason,
              reserveMonth: +values.reserveMonth
            }
          }).then(data => {
            dispatch({
              type: "Global/hideLoading"
            });
            if (data) {
              dispatch({
                type: "Global/showSuccess"
              });
              handleClose(true);
            } else {
              dispatch({
                type: "Global/showError"
              });
            }
          });
        }}
      >
        {({
          values,
          handleChange,
          handleBlur,
          handleSubmit
          /* and other goodies */
        }) => (
          <Form style={{ width: "100%" }} onSubmit={handleSubmit}>
            <Modal.Body>
              <fieldset>
                <legend>Thông tin cơ bản</legend>
                <div className="row">
                  <div className="col-md-4 col-sm-6">
                    <p>
                      Buổi học: <b>{selectedSession.lessonName}</b>
                    </p>
                  </div>
                  <div className="col-md-4 col-sm-6">
                    <p>
                      Ngày học: <b>{selectedSession.lessonDate}</b>
                    </p>
                  </div>
                  <div className="col-md-4 col-sm-6">
                    <p>
                      Giáo viên VN: <b>{selectedSession.teacherDomestic}</b>
                    </p>
                  </div>
                  <div className="col-md-4 col-sm-6">
                    <p>
                      Nội dung : <b>{selectedSession.lessonDesc}</b>
                    </p>
                  </div>
                  <div className="col-md-4 col-sm-6">
                    <p>
                      Phòng học: <b>{selectedSession.roomName}</b>
                    </p>
                  </div>
                  <div className="col-md-4 col-sm-6">
                    <p>
                      Giáo viên NN : <b>{selectedSession.teacherForeign}</b>
                    </p>
                  </div>
                </div>
              </fieldset>
              <div className="row g-2">
                <div className="col-md-6 col-sm-12">
                  <p>
                    Số buổi còn lại:{" "}
                    <b>{StudentReserve?.viewreserve?.lessonRemain}</b>
                  </p>
                </div>
                <div className="col-md-6 col-sm-12">
                  <p>
                    Số credit còn lại:{" "}
                    <b>
                      {StudentReserve?.viewreserve?.creditRemain?.toLocaleString(
                        "it-IT",
                        {
                          style: "currency",
                          currency: "VND"
                        }
                      )}
                    </b>
                  </p>
                </div>
              </div>

              <div className="row ">
                <div className="col-12">
                  <TextArea
                    id="reason"
                    name="reason"
                    label="Lí do nghỉ:"
                    value={values.reason}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                </div>
                <div className="col-md-6 col-sm-12">
                  <div className="col-6">
                    <Radio
                      id={`khong_xep_bu`}
                      name="xep_bu"
                      value={false}
                      label={"Không xếp bù"}
                      onChange={handleChange}
                      onBlur={handleBlur}
                    />
                  </div>
                </div>
                <div className="col-md-6 col-sm-12">
                  <div className="col-6">
                    <Radio
                      id={`xep_bu`}
                      name="xep_bu"
                      value={true}
                      label={"Thực hiện xếp bù"}
                      onChange={handleChange}
                      onBlur={handleBlur}
                    />
                  </div>
                </div>

                <div className="col-md-6 ">
                  <Select
                    id="programId"
                    name="programId"
                    label="Chương trình"
                    options={StudentReserve?.listmonth}
                    value={values.programId}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                </div>
              </div>
            </Modal.Body>
            <Modal.Footer>
              <Button
                variant={"primary"}
                style={{ marginRight: 15 }}
                onClick={handleSubmit}
              >
                Lưu
              </Button>
              <Button variant={"secondary"} onClick={handleClose}>
                Đóng
              </Button>
            </Modal.Footer>
          </Form>
        )}
      </Formik>
    </Modal>
  );
};

export default connect(
  ({ CustomerDetail, StudentReserve, ScheduleAttendance, Global }) => ({
    CustomerDetail,
    StudentReserve,
    ScheduleAttendance,
    Global
  })
)(CustomModal);
