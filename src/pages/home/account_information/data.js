export const noi_sinh = [
  {
    text: "Hà Nội",
    code: "1"
  },
  {
    text: "TP HCM",
    code: "2"
  },
  {
    text: "Bình Định",
    code: "3"
  },
  {
    text: "",
    code: "4"
  }
];

export const nguyen_quan = [
  {
    text: "Hà Nội",
    code: "1"
  },
  {
    text: "TP HCM",
    code: "2"
  },
  {
    text: "Bình Định",
    code: "3"
  },
  {
    text: "",
    code: "4"
  }
];

export const quoc_tich = [
  {
    text: "Việt Nam",
    code: "1"
  },
  {
    text: "Nước Ngoài",
    code: "2"
  }
];

export const gioi_tinh = [
  {
    text: "Việt Nam",
    code: "1"
  },
  {
    text: "Nước Ngoài",
    code: "2"
  }
];

export const hon_nhan = [
  {
    text: "Độc thân",
    code: "1"
  },
  {
    text: "Kết hôn",
    code: "2"
  },
  {
    text: "Ly hôn",
    code: "3"
  }
];
