import React from "react";
import { connect } from "dva";
import "./index.css";
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";
import { API_HOST } from "../../../services/serviceUri";
import { Button } from "react-bootstrap";
import PageHeader from "../../../components/PageHeader";
import UploadAvatar from "../../../components/UploadAvatar";
import { Formik, ErrorMessage } from "formik";

class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      avatarImage: ""
    };
  }

  onChangeAvatar = imageBase64 => {
    this.setState({ avatarImage: imageBase64 });
  };

  updateUser = data => {
    const { dispatch } = this.props;
    dispatch({
      type: "Global/showLoading",
      payload: data
    });
    dispatch({
      type: "User/updateavatar",
      payload: {
        avatarImage: this.state.avatarImage?.split(",")[1]
      }
    }).then(() => {
      dispatch({
        type: "User/updateinfo",
        payload: data
      }).then(res => {
        dispatch({
          type: "Global/hideLoading",
          payload: data
        });
        if (res) {
          this.setState({ avatarImage: "" });
          dispatch({
            type: "User/view",
            payload: data
          });
          dispatch({
            type: "Global/showSuccess",
            payload: data
          });
        }
        if (!res) {
          dispatch({
            type: "Global/showError",
            payload: data
          });
        }
      });
    });
  };

  render() {
    const bre = {
      title: "Thông tin tài khoản",
      breadcrums: [
        {
          title: "Home",
          path: "/"
        },
        {
          title: "Thông tin tài khoản",
          path: ""
        }
      ]
    };
    const {
      firstName,
      lastName,
      email,
      phone,
      gender,
      maritalStatus,
      birthDate,
      birthPlace,
      homeTown,
      nationality,
      permanentAddress,
      avatarImage
    } = this.props.User.curent_user || {
      firstName: "",
      lastName: "",
      email: "",
      phone: "",
      gender: "",
      maritalStatus: "",
      birthDate: "",
      birthPlace: "",
      homeTown: "",
      nationality: "",
      permanentAddress: ""
    };

    return (
      <div>
        <PageHeader {...bre} />
        <div>
          <Formik
            enableReinitialize={true}
            initialValues={{
              firstName: firstName,
              lastName: lastName,
              email: email,
              phone: phone,
              gender: gender,
              maritalStatus: maritalStatus,
              birthDate: birthDate,
              birthPlace: birthPlace,
              homeTown: homeTown,
              nationality: nationality,
              permanentAddress: permanentAddress
            }}
            validate={values => {
              const errors = {};
              if (!values.email) {
                errors.email = "Trường bắt buộc";
              } else if (
                !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
              ) {
                errors.email = "Email không hợp lệ";
              }
              !values.firstName && (errors.firstName = "Trường bắt buộc");
              !values.lastName && (errors.lastName = "Trường bắt buộc");
              !values.maritalStatus &&
                (errors.maritalStatus = "Trường bắt buộc");
              !values.birthPlace && (errors.birthPlace = "Trường bắt buộc");
              !values.homeTown && (errors.homeTown = "Trường bắt buộc");
              !values.nationality && (errors.nationality = "Trường bắt buộc");
              !values.permanentAddress &&
                (errors.permanentAddress = "Trường bắt buộc");
              if (!values.phone) {
                errors.phone = "Trường bắt buộc";
              } else if (
                !/(84|0[3|5|7|8|9])+([0-9]{8})\b/g.test(values.phone)
              ) {
                errors.phone = "Số điện thoại không hợp lệ";
              }

              return errors;
            }}
            onSubmit={async (values, { setSubmitting }) => {
              this.updateUser(values);
            }}
          >
            {({
              values,
              errors,
              touched,
              handleChange,
              handleBlur,
              handleSubmit,
              isSubmitting,
              setFieldValue
              /* and other goodies */
            }) => (
              <form onSubmit={handleSubmit}>
                <div className="iq-card">
                  <div className="iq-card-body">
                    <div className="row">
                      <div className="col-lg-3 col-md-4 col-sm-12 text-center">
                        <UploadAvatar
                          onChange={this.onChangeAvatar}
                          src={
                            this.state.avatarImage ||
                            `${API_HOST}/${avatarImage}`
                          }
                        ></UploadAvatar>
                      </div>

                      <div className="col-lg-9 col-md-8 col-sm-12">
                        <div className="row font-size-14">
                          <div className="col-md-6 col-sm-12">
                            <div className="form-group">
                              <label htmlFor="firstName">Họ và đệm:</label>
                              <input
                                type="text"
                                name="firstName"
                                className="form-control form-control-lg"
                                id="firstName"
                                // ref={register({ required: true, minLength: 8 })}
                                value={values.firstName}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                placeholder={"Nhập Họ và đệm"}
                              ></input>
                              <ErrorMessage
                                component="p"
                                className="error-message"
                                name="firstName"
                              ></ErrorMessage>
                            </div>
                            <div className="form-group">
                              <label htmlFor="lastName">Tên:</label>
                              <input
                                type="text"
                                name="lastName"
                                className="form-control form-control-lg"
                                id="lastName"
                                // ref={register({ required: true, minLength: 8 })}
                                value={values.lastName}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                placeholder={"Nhập Tên"}
                              ></input>
                              <ErrorMessage
                                component="p"
                                className="error-message"
                                name="lastName"
                              ></ErrorMessage>
                            </div>
                            <div className="form-group">
                              <label htmlFor="email">Email:</label>
                              <input
                                type="email"
                                name="email"
                                className="form-control form-control-lg"
                                id="email"
                                // ref={register({ required: true, minLength: 8 })}
                                value={values.email}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                placeholder={"Nhập Email"}
                              ></input>
                              <ErrorMessage
                                component="p"
                                className="error-message"
                                name="email"
                              ></ErrorMessage>
                            </div>
                            <div className="form-group">
                              <label htmlFor="phone">Số điện thoại:</label>
                              <input
                                type="text"
                                name="phone"
                                className="form-control form-control-lg"
                                id="phone"
                                // ref={register({ required: true, minLength: 8 })}
                                value={values.phone}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                placeholder={"Nhập Số điện thoại"}
                                minLength={9}
                              ></input>
                              <ErrorMessage
                                component="p"
                                className="error-message"
                                name="phone"
                              ></ErrorMessage>
                            </div>
                            <div className="form-group">
                              <div className="p-0">
                                <label>Giới tính:</label>
                              </div>
                              <div className="row">
                                {this.props.Account?.list_gioi_tinh?.map(
                                  gender => (
                                    <div
                                      className="col-md-6 col-sm-12"
                                      key={`gender_${gender.code}`}
                                    >
                                      <div className="custom-control custom-radio">
                                        <input
                                          className="custom-control-input"
                                          id={gender.code}
                                          name="gender"
                                          type="radio"
                                          value={gender.id}
                                          checked={values.gender === gender.id}
                                          onChange={() =>
                                            setFieldValue("gender", gender.id)
                                          }
                                          onBlur={handleBlur}
                                        />
                                        <label
                                          className="custom-control-label"
                                          htmlFor={gender.code}
                                        >
                                          {gender.name}
                                        </label>
                                      </div>
                                    </div>
                                  )
                                )}
                              </div>
                            </div>
                            <div className="form-group">
                              <label htmlFor="maritalStatus">
                                Tình trạng hôn nhân:
                              </label>
                              <select
                                className="form-control form-control-lg"
                                id="hon_nhan"
                                name="maritalStatus"
                                value={values.maritalStatus}
                                onChange={handleChange}
                                onBlur={handleBlur}
                              >
                                <>
                                  {this.props.Account?.list_tinh_trang_hon_nhan?.map(
                                    item => (
                                      <option
                                        key={`maritalStatus${item.code}`}
                                        value={item.code}
                                        disabled={
                                          item.code !== "" ? false : true
                                        }
                                        hidden={item.code !== "" ? false : true}
                                      >
                                        {item.name}
                                      </option>
                                    )
                                  )}
                                </>
                              </select>
                            </div>
                          </div>

                          <div className="col-md-6 col-sm-12">
                            <div className="form-group">
                              <label>Ngày sinh:</label>
                              <input
                                type="date"
                                className="form-control form-control-lg"
                                name="birthDate"
                                id="birthDate"
                                defaultValue={moment().format("YYYY-MM-DD")}
                                onChange={handleChange}
                                onBlur={handleBlur}
                              />
                            </div>
                            <div className="form-group">
                              <label htmlFor="birthPlace">Nơi sinh:</label>
                              <select
                                className="form-control form-control-lg"
                                id="birthPlace"
                                name="birthPlace"
                                value={values.birthPlace}
                                onChange={handleChange}
                                onBlur={handleBlur}
                              >
                                <>
                                  <option value="" disabled hidden>
                                    Vui lòng chọn giá trị
                                  </option>
                                  {this.props.Account?.list_noi_sinh?.map(
                                    item => (
                                      <option
                                        key={`birthPlace${item.code}`}
                                        value={item.code}
                                        disabled={
                                          item.code !== "" ? false : true
                                        }
                                        hidden={item.code !== "" ? false : true}
                                      >
                                        {item.name}
                                      </option>
                                    )
                                  )}
                                </>
                              </select>
                              <ErrorMessage
                                component="p"
                                className="error-message"
                                name="birthPlace"
                              ></ErrorMessage>
                            </div>
                            <div className="form-group">
                              <label htmlFor="homeTown">Nguyên quán:</label>
                              <select
                                className="form-control form-control-lg"
                                id="homeTown"
                                name="homeTown"
                                value={values.homeTown}
                                defaultValue={values.homeTown}
                                onChange={handleChange}
                                onBlur={handleBlur}
                              >
                                <>
                                  <option value="" disabled hidden>
                                    Vui lòng chọn giá trị
                                  </option>
                                  {this.props.Account?.list_noi_sinh?.map(
                                    item => (
                                      <option
                                        key={`homeTown${item.code}`}
                                        value={item.code}
                                        disabled={
                                          item.code !== "" ? false : true
                                        }
                                        hidden={item.code !== "" ? false : true}
                                      >
                                        {item.name}
                                      </option>
                                    )
                                  )}
                                </>
                              </select>
                              <ErrorMessage
                                component="p"
                                className="error-message"
                                name="homeTown"
                              ></ErrorMessage>
                            </div>
                            <div className="form-group">
                              <label htmlFor="nationality">Quốc tịch:</label>
                              <select
                                className="form-control form-control-lg"
                                id="nationality"
                                name="nationality"
                                defaultValue={values.homeTown}
                                value={values.nationality}
                                onChange={handleChange}
                                onBlur={handleBlur}
                              >
                                <>
                                  <option value="" disabled hidden>
                                    Vui lòng chọn giá trị
                                  </option>
                                  {this.props.Account?.list_quoc_tich?.map(
                                    item => (
                                      <option
                                        key={`nationality${item.code}`}
                                        value={item.code}
                                        disabled={
                                          item.code !== "" ? false : true
                                        }
                                        hidden={item.code !== "" ? false : true}
                                      >
                                        {item.name}
                                      </option>
                                    )
                                  )}
                                </>
                              </select>
                              <ErrorMessage
                                component="p"
                                className="error-message"
                                name="nationality"
                              ></ErrorMessage>
                            </div>
                            <div className="form-group">
                              <label htmlFor="permanentAddress">
                                Địa chỉ thường trú:
                              </label>
                              <textarea
                                className="textarea form-control"
                                rows="4"
                                name="permanentAddress"
                                id="permanentAddress"
                                value={values.permanentAddress}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                placeholder={"Nhập Địa chỉ thường trú"}
                              ></textarea>
                              <ErrorMessage
                                component="p"
                                className="error-message"
                                name="permanentAddress"
                              ></ErrorMessage>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="col-12" style={{ textAlign: "right" }}>
                        <div className="form-group m-0">
                          <Button
                            variant="primary"
                            type="submit"
                            // disabled={isSubmitting}
                          >
                            Lưu
                          </Button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            )}
          </Formik>
        </div>
      </div>
    );
  }
}

export default connect(({ Account, Center, User, Dictionary }) => ({
  Account,
  Center,
  User,
  Dictionary
}))(Page);
