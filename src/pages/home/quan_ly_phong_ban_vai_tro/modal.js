import React from "react";
import { Modal, Button } from "react-bootstrap";
import { connect } from "dva";
import { Formik, ErrorMessage } from "formik";

class CustomModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      show: true,
      count: 0,
      centers: []
    };
  }

  handleClose = isUpdate => {
    this.props.onClose(isUpdate);
  };
  render() {
    const { isCreateNew, DepartementRole, id } = this.props;
    const { name, departementId } = isCreateNew
      ? { departementName: "", name: "", departementId: "" }
      : DepartementRole?.curent_role;

    return (
      <Modal
        size={"md"}
        show={this.state.show}
        onHide={() => this.handleClose()}
        backdrop="static"
        keyboard={false}
        id="AddNew"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Formik
          enableReinitialize={true}
          initialValues={{
            name: name,
            departementId: departementId
          }}
          validate={values => {
            const errors = {};
            !values.departementId && (errors.departementId = "Trường bắt buộc");
            !values.name && (errors.name = "Trường bắt buộc");
            return errors;
          }}
          onSubmit={(values, { setSubmitting }) => {
            const { dispatch } = this.props;
            const selectedDepartment = DepartementRole.list_phong_ban.find(
              item => (item.id = departementId)
            );
            dispatch({
              type: "Global/showLoading"
            });
            // Upload Image here
            dispatch({
              type: isCreateNew
                ? "DepartementRole/insert"
                : "DepartementRole/update",
              payload: {
                departementName: selectedDepartment?.name,
                departementId: values.departementId,
                name: values.name,
                id: id || undefined
              }
            })
              .then(res => {
                dispatch({
                  type: "Global/hideLoading"
                });
                !res.code
                  ? dispatch({
                      type: "Global/showSuccess"
                    })
                  : dispatch({
                      type: "Global/showError"
                    });
                !res.code && this.props.onClose(true);
              })
              .catch(() => {
                dispatch({
                  type: "Global/showError"
                });
                dispatch({
                  type: "Global/hideLoading"
                });
              });
          }}
        >
          {({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
            setFieldValue
            /* and other goodies */
          }) => (
            <form style={{ width: "100%" }} onSubmit={handleSubmit}>
              <Modal.Header closeButton>
                <Modal.Title>
                  {isCreateNew ? "Tạo phòng ban mới" : "Chỉnh sửa phòng ban"}
                </Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <div className="row">
                  <div className="col-12">
                    <div className="form-group">
                      <label htmlFor="departementId">Tên phòng ban:</label>
                      <select
                        className="form-control form-control-lg"
                        id="departementId"
                        name="departementId"
                        value={values.departementId}
                        onChange={handleChange}
                        onBlur={handleBlur}
                      >
                        <option value="" disabled hidden>
                          Vui lòng chọn giá trị
                        </option>
                        {this.props.DepartementRole.list_phong_ban?.map(
                          item => (
                            <option
                              key={`departementId${item.id}`}
                              value={item.id}
                              disabled={item.id !== "" ? false : true}
                              selected={item.id !== "" ? false : true}
                              hidden={item.id !== "" ? false : true}
                            >
                              {item.name}
                            </option>
                          )
                        )}
                      </select>
                      <ErrorMessage
                        component="p"
                        className="error-message"
                        name="departementId"
                      ></ErrorMessage>
                    </div>
                    <div className="form-group">
                      <label htmlFor="name">Vị trí:</label>
                      <input
                        type="text"
                        name="name"
                        className="form-control form-control-lg"
                        id="name"
                        value={values.name}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        placeholder={"Nhập Vị trí"}
                      ></input>
                      <ErrorMessage
                        component="p"
                        className="error-message"
                        name="name"
                      ></ErrorMessage>
                    </div>
                  </div>
                </div>
              </Modal.Body>
              <Modal.Footer>
                <Button
                  variant="primary"
                  style={{ marginRight: 15 }}
                  type="submit"
                >
                  {isCreateNew ? "Tạo mới" : "Cập nhật"}
                </Button>
                <Button
                  variant={"secondary"}
                  onClick={() => this.handleClose()}
                >
                  Hủy
                </Button>
              </Modal.Footer>
            </form>
          )}
        </Formik>
      </Modal>
    );
  }
}
export default connect(({ DepartementRole, Global }) => ({
  DepartementRole,
  Global
}))(CustomModal);
