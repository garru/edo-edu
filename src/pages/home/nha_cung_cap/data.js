export const HEADER = [
  {
    title: "STT",
    key: "stt",
    class: "tb-width-50"
  },
  {
    title: "Tên",
    key: "ten",
    class: "tb-width-150"
  },
  {
    title: "Địa chỉ",
    key: "dia_chi",
    class: "tb-width-200"
  },
  {
    title: "Số điện thoại",
    key: "so_dien_thoai",
    class: "tb-width-150"
  },
  {
    title: "Mã số thuế",
    key: "ma_so_thue",
    class: "tb-width-150"
  },
  {
    title: "Số tài khoản",
    key: "so_tai_khoan",
    class: "tb-width-150"
  },
  {
    title: "Trạng thái",
    key: "trang_thai",
    class: "tb-width-150"
  },
  {
    title: "Cung cấp",
    key: "cung_cap",
    class: "tb-width-200"
  },
  {
    title: "Danh mục",
    key: "danh_muc",
    class: "tb-width-200"
  },
  {
    title: "Số tiền đã giao dịch",
    key: "so_tien_da_giao_dich",
    class: "tb-width-200"
  }
];

export const DATA = [
  {
    stt: "1",
    ten: "Nhà cung cấp 1",
    dia_chi: "Quận Ba Đình",
    so_dien_thoai: "09xxxxxxxxx",
    ma_so_thue: "111",
    so_tai_khoan: "123456789",
    trang_thai: "active",
    cung_cap: "Ghế, bàn học",
    danh_muc: "Cơ sở vật chất",
    so_tien_da_giao_dich: "10.000.000 VNĐ"
  },
  {
    stt: "1",
    ten: "Nhà cung cấp 2",
    dia_chi: "Quận Ba Đình",
    so_dien_thoai: "09xxxxxxxxx",
    ma_so_thue: "111",
    so_tai_khoan: "123456789",
    trang_thai: "",
    cung_cap: "Ghế, bàn học",
    danh_muc: "Cơ sở vật chất",
    so_tien_da_giao_dich: "10.000.000 VNĐ"
  }
];
