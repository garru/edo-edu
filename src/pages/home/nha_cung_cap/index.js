import React, { useState } from "react";
import { connect } from "dva";
import "react-datepicker/dist/react-datepicker.css";
import { Button, Badge, Table, Overlay, Popover } from "react-bootstrap";
import CustomModal from "./modal";
import { useHistory } from "react-router-dom";
import "./index.css";
import PageHeader from "../../../components/PageHeader";
import { DATA, HEADER } from "./data";
import Paging from "../../../components/Paging";

const Page = () => {
  const [isCreateNew, setIsCreateNew] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [fromDate, setFromDate] = useState(new Date());
  const [toDate, setToDate] = useState(new Date());
  const history = useHistory();
  const initCheckedItem = DATA.map(item => false);
  const [show, setShow] = useState(initCheckedItem);
  const [target, setTarget] = useState(false);
  const [activeIndex, setActiveIndex] = useState(null);
  const [selectedItem, setSelectedItem] = useState(null);
  const [listItem, setListItem] = useState(DATA);

  const handleButtonClick = () => {
    setIsCreateNew(true);
    setShowModal(true);
  };
  const handleViewButtonClick = () => {
    setShowModal(true);
  };
  const handleCloseModal = () => {
    setShowModal(false);
  };
  const handleImportButtonClick = () => {
    history.push("/nhap_hang");
  };
  const handleExportButtonClick = () => {
    history.push("/xuat_hang");
  };

  const handleClick = (e, index, isHide) => {
    show[index] = !show[index];
    index !== activeIndex && (show[activeIndex] = false);
    setShow(show);
    setTarget(isHide ? null : e.target);
    setActiveIndex(index);
    setSelectedItem(listItem[index]);
  };

  const renderTooltip = index => {
    return (
      <Overlay
        show={show[index]}
        target={target}
        placement="bottom"
        containerPadding={20}
        rootClose={true}
        rootCloseEvent={"click"}
        onHide={e => handleClick(e, index)}
      >
        <Popover id="popover-contained">
          <Popover.Content>
            <ul>
              <li
                onClick={e => {
                  handleClick(e, index, true);
                  setShow(true);
                }}
              >
                <i
                  className="fa fa-edit"
                  style={{
                    marginRight: 0,
                    marginLeft: "auto"
                  }}
                />
                &nbsp; Chỉnh sửa
              </li>
              <li
                onClick={e => {
                  handleClick(e, index, true);
                }}
              >
                <i
                  className="fa fa-trash-alt"
                  style={{
                    marginRight: 0,
                    marginLeft: "auto"
                  }}
                />
                &nbsp; Xoá
              </li>
            </ul>
          </Popover.Content>
        </Popover>
      </Overlay>
    );
  };

  const bre = {
    title: "Nhà cung cấp",
    breadcrums: [
      {
        title: "Home",
        path: "/"
      },
      {
        title: "Nhà cung cấp",
        path: ""
      }
    ],
    subTitle: "Danh sách các nhà cung cấp cho doanh nghiệp"
  };

  return (
    <div>
      <PageHeader {...bre}></PageHeader>
      <div className="iq-card">
        <div className="iq-card-body">
          <div className="row">
            <form style={{ width: "100%" }}>
              <div className="col-12">
                <fieldset>
                  <legend>Tìm kiếm</legend>
                  <div className="row align-end">
                    <div className="col-md-3">
                      <div className="form-group m-0">
                        <label htmlFor="ma_nhan_vien">Mã nhân viên:</label>
                        <input
                          type="text"
                          name="ma_nhan_vien"
                          className="form-control form-control-lg"
                          id="ma_nhan_vien"
                          // ref={register({ required: true, minLength: 8 })}
                        ></input>
                      </div>
                    </div>

                    <div className="col-2">
                      <div className="form-group  m-0">
                        <Button variant="primary" type="submit">
                          Tìm kiếm
                        </Button>
                      </div>
                    </div>
                  </div>
                </fieldset>
              </div>
            </form>
          </div>
          <div className="row">
            <div className="col-12 m-b-1 m-t-2">
              <Button
                variant="primary"
                className="float-right"
                type="submit"
                onClick={handleButtonClick}
              >
                Thêm mới
              </Button>
            </div>
            <div className="col-md-12 ">
              <div style={{ width: "100%" }}>
                <div style={{ overflowX: "auto", width: "100%" }}>
                  <Table bordered hover className="table">
                    <thead>
                      <tr>
                        {HEADER.map(item => (
                          <th
                            className={`${item.class} ${
                              item.hasSort ? "sort" : ""
                            }`}
                            key={item.key}
                          >
                            <span>{item.title}</span>
                          </th>
                        ))}

                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                      {listItem.map((item, index) => (
                        <React.Fragment>
                          <tr key={`account_${item.id}`}>
                            <td>{item.stt}</td>
                            <td>{item.ten}</td>
                            <td>{item.dia_chi}</td>
                            <td>{item.so_dien_thoai}</td>
                            <td>{item.ma_so_thue}</td>
                            <td>{item.so_tai_khoan}</td>
                            <td>
                              <Badge
                                className="p-2"
                                variant={
                                  item.trang_thai === "active"
                                    ? "success"
                                    : "secondary"
                                }
                              >
                                Hoạt động
                              </Badge>
                            </td>
                            <td>{item.cung_cap}</td>
                            <td>{item.danh_muc}</td>
                            <td>{item.so_tien_da_giao_dich}</td>
                            <td>
                              <i
                                className="fa fa-ellipsis-h"
                                style={{
                                  marginRight: 0,
                                  marginLeft: "auto"
                                }}
                                onClick={e => handleClick(e, index)}
                              />
                              {renderTooltip(index)}
                            </td>
                          </tr>
                        </React.Fragment>
                      ))}
                    </tbody>
                  </Table>
                  {listItem && listItem.length ? (
                    ""
                  ) : (
                    <div className="not-found">
                      <i class="fas fa-inbox"></i>
                      <span>Không tìm thấy kết quả</span>
                    </div>
                  )}
                </div>

                {showModal && (
                  <CustomModal
                    isCreateNew={isCreateNew}
                    selectedItem={selectedItem}
                    handleCloseModal={handleCloseModal}
                  />
                )}
              </div>
              <Paging
                pageNumber={10}
                total={550}
                pageSize={10}
                onChange={console.log}
              ></Paging>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default connect(({ Mock }) => ({
  Mock
}))(Page);
