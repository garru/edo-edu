import React from "react";
import { connect } from "dva";
import "./index.css";
import "react-datepicker/dist/react-datepicker.css";
import Rating from "react-rating";
import moment from "moment";
import { Button, Table } from "react-bootstrap";
import { DATA, HEADER } from "./data";
import PageHeader from "../../../components/PageHeader";
import IconTable from "../../../components/IconTable";
class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      listItem: DATA
    };
  }
  onClear = () => {
    this.setState({});
  };

  componentWillMount() {}

  render() {
    const bre = {
      title: "Đơn giá giáo viên",
      breadcrums: [
        {
          title: "Home",
          path: "/"
        },
        {
          title: "Đơn giá giáo viên",
          path: ""
        }
      ]
    };
    console.log("render tiep_nhan>>>");
    return (
      <div>
        <PageHeader {...bre}></PageHeader>
        
          <div className="iq-card">
            <div className=" iq-card-header d-flex justify-content-between">
              <div className="iq-header-title">
                <h6>
                  <b>Thông tin cơ bản</b>
                </h6>
              </div>
            </div>
            <div className="iq-card-body">
              <div className="row m-b-2">
                <div className="col-12">
                  <div className="row align-center">
                  <div className="col-lg-3 col-md-4 col-sm-12 text-center">
                      <div className="profile-img-edit m-b-2">
                        <img
                          alt="profile-pic"
                          className="profile-pic"
                          src={this.state.img_user || "images/user/11.png"}
                        />
                        <div className="p-image">
                          <i className="fas fa-camera upload-button"></i>
                          <input
                            accept="image/*"
                            className="file-upload"
                            type="file"
                          />
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-9 col-md-8 col-sm-12">
                      <div className="row">
                        <div className="col-md-6">
                          <div className="row">
                            <div className="col-4">
                              <p>Họ và tên:</p>
                            </div>
                            <div className="col-8">
                              <p>
                                <b>Hoàng Văn Huy</b>
                              </p>
                            </div>
                          </div>
                          <div className="row">
                            <div className="col-4">
                              <p>Mã số:</p>
                            </div>
                            <div className="col-8">
                              <p>
                                <b>HV-000002</b>
                              </p>
                            </div>
                          </div>
                          <div className="row">
                            <div className="col-4">
                              <p>Số điện thoại:</p>
                            </div>
                            <div className="col-8">
                              <p>
                                <b>0384455522</b>
                              </p>
                            </div>
                          </div>
                        </div>
                        <div className="col-md-6">
                          <div className="row">
                            <div className="col-4">
                              <p>Email:</p>
                            </div>
                            <div className="col-8">
                              <p>
                                <b>test@qa/team</b>
                              </p>
                            </div>
                          </div>
                          <div className="row">
                            <div className="col-4">
                              <p>Loại:</p>
                            </div>
                            <div className="col-8">
                              <p>
                                <b>Full time</b>
                              </p>
                            </div>
                          </div>
                          <div className="row">
                            <div className="col-4">
                              <p>Thời gian công tác:</p>
                            </div>
                            <div className="col-8">
                              <p>
                                <b>4 năm</b>
                              </p>
                            </div>
                          </div>
                        </div>

                        <div className="col-md-6 ">
                          <div className="row">
                            <div className="col-4">
                              <p>Đang dậy các lớp:</p>
                            </div>
                            <div className="col-8">
                              <ul>
                                <li>
                                  <p>
                                    <b>Lớp A - Hoàng Ngân</b>
                                  </p>
                                </li>
                                <li>
                                  <p>
                                    <b>Lớp B - Hoàng Ngân</b>
                                  </p>
                                </li>
                              </ul>
                            </div>
                          </div>
                          <div className="row">
                            <div className="col-4">
                              <p>Lần đánh giá gần nhất:</p>
                            </div>
                            <div className="col-8">
                              <p>
                                <b>2 tháng trước</b>
                              </p>
                            </div>
                          </div>
                        </div>

                        <div className="col-md-6 ">
                          <div className="row">
                            <div className="col-4">
                              <p>Nhận xét từ phụ huynh:</p>
                            </div>
                            <div className="col-8">
                              <p>
                                <b>20</b>
                              </p>
                            </div>
                          </div>

                          <div className="row">
                            <div className="col-4">
                              <p>Đánh giá hiện tại:</p>
                            </div>
                            <div className="col-8">
                              <Rating
                                className="rating"
                                emptySymbol="far fa-star"
                                fullSymbol="fa fa-star"
                                fractions={2}
                                readonly={true}
                                initialRating={4}
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        

        <div className="iq-card">
          <div className=" iq-card-header d-flex justify-content-between">
            <div className="iq-header-title">
              <h6>
                <b>Đơn giá hiện tại</b>
              </h6>
            </div>
          </div>
          <div className="iq-card-body">
            <div className="row ">
              <div className="col-6">
                <div className="form-group">
                  <label htmlFor="don_gia">Đơn giá(Đồng/giờ): </label>
                  <input
                    type="text"
                    name="don_gia"
                    className="form-control form-control-lg"
                    id="don_gia"
                    // ref={register({ required: true, minLength: 8 })}
                  ></input>
                </div>
              </div>
              <div className="col-6">
                <div className="form-group">
                  <label htmlFor="thoi_gian_ap_dung">Thời gian áp dụng: </label>
                  <input
                    type="date"
                    className="form-control form-control-lg"
                    id="thoi_gian_ap_dung"
                    defaultValue={moment().format("YYYY-MM-DD")}
                  />
                </div>
              </div>
              <div className="col-12" style={{ textAlign: "right" }}>
                <div className="form-group">
                  <Button variant="primary" type="submit">
                    Lưu
                  </Button>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="iq-card">
          <div className=" iq-card-header d-flex justify-content-between">
            <div className="iq-header-title">
              <h6>
                <b>Lịch sử đơn giá</b>
              </h6>
            </div>
          </div>
          <div className="iq-card-body">
            <div style={{ width: "100%" }}>
              <div style={{ overflowX: "auto", width: "100%" }}>
                <Table
                  bordered
                  hover
                  // style={{ minWidth: 1500 }}
                  className="table"
                >
                  <thead>
                    <tr>
                      {HEADER.map(item => (
                        <th
                          className={`${item.class} ${
                            item.hasSort ? "sort" : ""
                          } ${
                            this.state.sortField === item.key ? "active" : ""
                          }`}
                          key={item.key}
                        >
                          <span>{item.title}</span>
                        </th>
                      ))}
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.listItem.map((item, index) => (
                      <React.Fragment>
                        <tr key={`account_${item.id}`}>
                          <td>{item.don_gia_buoi}</td>
                          <td>{item.thoi_gian_ap_dung}</td>
                          <td>{item.thoi_gian_ket_thuc}</td>
                          <td>{item.nguoi_chinh_sua}</td>
                          <td>
                            <IconTable
                              classes="fa fa-edit"
                              styles={{
                                marginRight: 15
                              }}
                              onClick={e => {
                                console.log();
                              }}
                              text="Edit"
                            ></IconTable>
                            <IconTable
                              classes="fa fa-times"
                              onClick={e => {
                                console.log();
                              }}
                              text="Time"
                            ></IconTable>
                          </td>
                        </tr>
                      </React.Fragment>
                    ))}
                  </tbody>
                </Table>
                {this.state.listItem || this.state.listItem.length ? (
                  ""
                ) : (
                  <div className="not-found">
                    <i class="fas fa-inbox"></i>
                    <span>Không tìm thấy kết quả</span>
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(({ Mock }) => ({
  Mock
}))(Page);
