export const HEADER = [
  {
    title: "Đơn giá buổi",
    key: "don_gia_buoi",
    class: "tb-width-200"
  },
  {
    title: "Thời gian áp dụng",
    key: "thoi_gian_ap_dung",
    class: "tb-width-200"
  },
  {
    title: "Thời gian kết thúc",
    key: "thoi_gian_ket_thuc",
    class: "tb-width-200"
  },
  {
    title: "Người chỉnh sửa",
    key: "nguoi_chinh_sua",
    class: "tb-width-200"
  }
];

export const DATA = [
  {
    don_gia_buoi: "200.000",
    thoi_gian_ap_dung: "21/1/2021",
    thoi_gian_ket_thuc: "21/1/2022",
    nguoi_chinh_sua: "Nguyen Van A"
  },
  {
    don_gia_buoi: "200.000",
    thoi_gian_ap_dung: "21/1/2021",
    thoi_gian_ket_thuc: "21/1/2022",
    nguoi_chinh_sua: "Nguyen Van A"
  },
  {
    don_gia_buoi: "200.000",
    thoi_gian_ap_dung: "21/1/2021",
    thoi_gian_ket_thuc: "21/1/2022",
    nguoi_chinh_sua: "Nguyen Van A"
  },
  {
    don_gia_buoi: "200.000",
    thoi_gian_ap_dung: "21/1/2021",
    thoi_gian_ket_thuc: "21/1/2022",
    nguoi_chinh_sua: "Nguyen Van A"
  }
];
