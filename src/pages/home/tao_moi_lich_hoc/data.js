export const TITLE = "Quản lý giờ học";
export const BUTTON = ["Tạo giờ học"];
export const HEADER = [
  {
    title: "No",
    key: "id",
    class: "tb-width-50"
  },
  {
    title: "Tên buổi học",
    key: "ten_buoi_hoc",
    class: "tb-width-200"
  },
  {
    title: "Ngày học",
    key: "ngay_hoc",
    class: "tb-width-150"
  },
  {
    title: "Giờ học",
    key: "gio_hoc",
    class: "tb-width-150"
  },
  {
    title: "Phòng học",
    key: "phong_hoc",
    class: "tb-width-150"
  },
  {
    title: "Giáo viên Việt Nam",
    key: "gv_viet_nam",
    class: "tb-width-200"
  },
  {
    title: "Giáo viên nước ngoài",
    key: "gv_nuoc_ngoai",
    class: "tb-width-200"
  }
];

export const DATA = [
  {
    id: "1",
    ten_buoi_hoc: "Introduction-Greetings",
    ngay_hoc: "11/01/2021",
    gio_hoc: "17:30",
    phong_hoc: "Mickey",
    gv_viet_nam: "Nguyễn Hoàng",
    gv_nuoc_ngoai: "Peter",
    type: "school"
  },
  {
    id: "1",
    ten_buoi_hoc: "Introduction-Greetings",
    ngay_hoc: "11/01/2021",
    gio_hoc: "17:30",
    phong_hoc: "Mickey",
    gv_viet_nam: "Nguyễn Hoàng",
    gv_nuoc_ngoai: "Peter",
    type: "teacher"
  },
  {
    id: "1",
    ten_buoi_hoc: "Introduction-Greetings",
    ngay_hoc: "11/01/2021",
    gio_hoc: "17:30",
    phong_hoc: "Mickey",
    gv_viet_nam: "Nguyễn Hoàng",
    gv_nuoc_ngoai: "Peter"
  },
  {
    id: "1",
    ten_buoi_hoc: "Introduction-Greetings",
    ngay_hoc: "11/01/2021",
    gio_hoc: "17:30",
    phong_hoc: "Mickey",
    gv_viet_nam: "Nguyễn Hoàng",
    gv_nuoc_ngoai: "Peter"
  },
  {
    id: "1",
    ten_buoi_hoc: "Introduction-Greetings",
    ngay_hoc: "11/01/2021",
    gio_hoc: "17:30",
    phong_hoc: "Mickey",
    gv_viet_nam: "Nguyễn Hoàng",
    gv_nuoc_ngoai: "Peter"
  }
];


export const HEADER_NGOAI_KHOA = [
  {
    title: "Học liệu",
    key: "hoc_lieu",
    class: "tb-width-300"
  },
  {
    title: "Số lượng",
    key: "so_luong",
    class: "tb-width-100"
  },
  {
    title: "Phân phối",
    key: "phan_phoi",
    class: "tb-width-200"
  },
  {
    title: "Hoàn",
    key: "hoan",
    class: "tb-width-100"
  },
  {
    title: "Thao tác",
    key: "thao_tac",
    class: "tb-width-100"
  }
];

export const DATA_NGOAI_KHOA = [
{
  hoc_lieu: "",
  so_luong: "",
  phan_phoi: "",
  hoan: "",
  thao_tac: ""
}
]

