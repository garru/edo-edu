import React from 'react';
import './index.css';
import './ax.css';

class Page extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
		<div class="" id="base" style={{"margin-left":"-150px"}}>
		   <div class="ax_default shape" id="u3428">
		      <div class="" id="u3428_div">
		      </div>
		      <div class="text" id="u3428_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default shape" id="u3429">
		      <div class="" id="u3429_div">
		      </div>
		      <div class="text" id="u3429_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default" data-height="40" data-label="account" data-left="13" data-top="895" data-width="192" id="u3431">
		      <div class="ax_default shape" data-label="accountLable" id="u3432">
		         <img class="img" id="u3432_img" src="images/account_information/accountlable_u55.svg"/>
		         <div class="text" id="u3432_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Nguyen Van A
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u3433">
		         <img class="img" id="u3433_img" src="images/login/u4.svg"/>
		         <div class="text" id="u3433_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="settingMenu" data-left="11" data-top="826" data-width="125" id="u3434">
		      <div class="ax_default shape" data-label="gearIconLable" id="u3435">
		         <img class="img" id="u3435_img" src="images/account_information/geariconlable_u58.svg"/>
		         <div class="text" id="u3435_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Cài đặt
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="44" data-left="11" data-top="826" data-width="44" id="u3436">
		         <div class="ax_default shape" data-label="gearIconBG" id="u3437">
		            <div class="" id="u3437_div">
		            </div>
		            <div class="text" id="u3437_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default shape" data-label="gearIconBG2" id="u3438">
		            <div class="" id="u3438_div">
		            </div>
		            <div class="text" id="u3438_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" data-label="gearIcon" id="u3439">
		            <img class="img" id="u3439_img" src="images/account_information/gearicon_u62.svg"/>
		            <div class="text" id="u3439_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="customerMenu" data-left="11" data-top="145" data-width="143" id="u3440">
		      <div class="ax_default shape" data-label="customerIconLable" id="u3441">
		         <img class="img" id="u3441_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u3441_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Khách hàng
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="customerIconBG" id="u3442">
		         <div class="" id="u3442_div">
		         </div>
		         <div class="text" id="u3442_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="customerIconBG2" id="u3443">
		         <div class="" id="u3443_div">
		         </div>
		         <div class="text" id="u3443_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" data-label="customer" id="u3444">
		         <img class="img" id="u3444_img" src="images/account_information/customer_u67.svg"/>
		         <div class="text" id="u3444_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="classMenu" data-left="11" data-top="198" data-width="143" id="u3445">
		      <div class="ax_default shape" data-label="classIconLable" id="u3446">
		         <img class="img" id="u3446_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u3446_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Lớp học
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG" id="u3447">
		         <div class="" id="u3447_div">
		         </div>
		         <div class="text" id="u3447_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG2" id="u3448">
		         <div class="" id="u3448_div">
		         </div>
		         <div class="text" id="u3448_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" data-label="classicon" id="u3449">
		         <img class="img" id="u3449_img" src="images/account_information/classicon_u72.svg"/>
		         <div class="text" id="u3449_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="dashboardMenu" data-left="11" data-top="91" data-width="143" id="u3450">
		      <div class="ax_default shape" data-label="dashboardIconLable" id="u3451">
		         <img class="img" id="u3451_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u3451_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Dashnoard
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="dashboardIconBG" id="u3452">
		         <div class="" id="u3452_div">
		         </div>
		         <div class="text" id="u3452_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="dashboardIconBG2" id="u3453">
		         <div class="" id="u3453_div">
		         </div>
		         <div class="text" id="u3453_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" data-label="dashboard" id="u3454">
		         <img class="img" id="u3454_img" src="images/account_information/dashboard_u77.svg"/>
		         <div class="text" id="u3454_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="129" data-label="AccountMenu" data-left="58" data-top="806" data-width="246" id="u3455" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default shape" id="u3456">
		         <div class="" id="u3456_div">
		         </div>
		         <div class="text" id="u3456_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u3457">
		         <div class="" id="u3457_div">
		         </div>
		         <div class="text" id="u3457_text">
		            <p>
		               <span style={{"font-family":"ArialMT, Arial","font-weight":"400","text-decoration":"none"}}>
		                  Xin chào:
		               </span>
		               <span style={{"font-family":"Arial Bold, Arial","font-weight":"700","text-decoration":"none"}}>
		                  Nguyen Van A
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3458">
		         <div class="ax_default image" id="u3459">
		            <img class="img" id="u3459_img" src="images/account_information/u82.png"/>
		            <div class="text" id="u3459_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u3460">
		            <div class="" id="u3460_div">
		            </div>
		            <div class="text" id="u3460_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Đăng xuất
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default line" id="u3461">
		         <img class="img" id="u3461_img" src="images/account_information/u84.svg"/>
		         <div class="text" id="u3461_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3462">
		         <div class="ax_default paragraph" id="u3463">
		            <div class="" id="u3463_div">
		            </div>
		            <div class="text" id="u3463_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tài khoản
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default image" id="u3464">
		            <img class="img" id="u3464_img" src="images/account_information/u87.png"/>
		            <div class="text" id="u3464_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="339" data-label="SettingPannel" data-left="58" data-top="525" data-width="339" id="u3465" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default shape" id="u3466">
		         <div class="" id="u3466_div">
		         </div>
		         <div class="text" id="u3466_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u3467">
		         <div class="" id="u3467_div">
		         </div>
		         <div class="text" id="u3467_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Hệ thống
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3468">
		         <div class="ax_default icon" id="u3469">
		            <img class="img" id="u3469_img" src="images/account_information/u92.svg"/>
		            <div class="text" id="u3469_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u3470">
		            <div class="" id="u3470_div">
		            </div>
		            <div class="text" id="u3470_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tài khoản
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3471">
		         <div class="ax_default paragraph" id="u3472">
		            <div class="" id="u3472_div">
		            </div>
		            <div class="text" id="u3472_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phân quyền
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u3473">
		            <img class="img" id="u3473_img" src="images/account_information/u96.svg"/>
		            <div class="text" id="u3473_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u3474">
		         <div class="" id="u3474_div">
		         </div>
		         <div class="text" id="u3474_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Đào tạo
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3475">
		         <div class="ax_default paragraph" id="u3476">
		            <div class="" id="u3476_div">
		            </div>
		            <div class="text" id="u3476_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Thương hiệu
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u3477">
		            <img class="img" id="u3477_img" src="images/account_information/u100.svg"/>
		            <div class="text" id="u3477_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3478">
		         <div class="ax_default paragraph" id="u3479">
		            <div class="" id="u3479_div">
		            </div>
		            <div class="text" id="u3479_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trung tâm
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u3480">
		            <img class="img" id="u3480_img" src="images/account_information/u103.svg"/>
		            <div class="text" id="u3480_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3481">
		         <div class="ax_default icon" id="u3482">
		            <img class="img" id="u3482_img" src="images/account_information/u105.svg"/>
		            <div class="text" id="u3482_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u3483">
		            <div class="" id="u3483_div">
		            </div>
		            <div class="text" id="u3483_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Ngày nghỉ
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3484">
		         <div class="ax_default icon" id="u3485">
		            <img class="img" id="u3485_img" src="images/account_information/u108.svg"/>
		            <div class="text" id="u3485_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u3486">
		            <div class="" id="u3486_div">
		            </div>
		            <div class="text" id="u3486_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phòng ban - Vai trò
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3487">
		         <div class="ax_default paragraph" id="u3488">
		            <div class="" id="u3488_div">
		            </div>
		            <div class="text" id="u3488_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trạng thái học viên
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u3489">
		            <img class="img" id="u3489_img" src="images/account_information/u112.svg"/>
		            <div class="text" id="u3489_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3490">
		         <div class="ax_default paragraph" id="u3491">
		            <div class="" id="u3491_div">
		            </div>
		            <div class="text" id="u3491_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tiêu chí đánh giá
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u3492">
		            <img class="img" id="u3492_img" src="images/account_information/u115.svg"/>
		            <div class="text" id="u3492_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3493">
		         <div class="ax_default paragraph" id="u3494">
		            <div class="" id="u3494_div">
		            </div>
		            <div class="text" id="u3494_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Chương trình học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3495">
		            <div class="ax_default icon" id="u3496">
		               <img class="img" id="u3496_img" src="images/account_information/u119.svg"/>
		               <div class="text" id="u3496_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3497">
		         <div class="ax_default icon" id="u3498">
		            <img class="img" id="u3498_img" src="images/account_information/u121.svg"/>
		            <div class="text" id="u3498_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u3499">
		            <div class="" id="u3499_div">
		            </div>
		            <div class="text" id="u3499_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Bài học
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3500">
		         <div class="ax_default paragraph" id="u3501">
		            <div class="" id="u3501_div">
		            </div>
		            <div class="text" id="u3501_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phòng học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u3502">
		            <img class="img" id="u3502_img" src="images/account_information/u125.svg"/>
		            <div class="text" id="u3502_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3503">
		         <div class="ax_default paragraph" id="u3504">
		            <div class="" id="u3504_div">
		            </div>
		            <div class="text" id="u3504_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trường học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u3505">
		            <img class="img" id="u3505_img" src="images/account_information/u128.svg"/>
		            <div class="text" id="u3505_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default line" id="u3506">
		         <img class="img" id="u3506_img" src="images/account_information/u129.svg"/>
		         <div class="text" id="u3506_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u3507">
		         <div class="" id="u3507_div">
		         </div>
		         <div class="text" id="u3507_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Tài chính
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3508">
		         <div class="ax_default paragraph" id="u3509">
		            <div class="" id="u3509_div">
		            </div>
		            <div class="text" id="u3509_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Học phí
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u3510">
		            <img class="img" id="u3510_img" src="images/account_information/u133.svg"/>
		            <div class="text" id="u3510_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u3511">
		         <div class="ax_default paragraph" id="u3512">
		            <div class="" id="u3512_div">
		            </div>
		            <div class="text" id="u3512_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Khuyến mại
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u3513">
		            <img class="img" id="u3513_img" src="images/account_information/u136.svg"/>
		            <div class="text" id="u3513_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="classMenu" data-left="11" data-top="252" data-width="143" id="u3514">
		      <div class="ax_default shape" data-label="classIconLable" id="u3515">
		         <img class="img" id="u3515_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u3515_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Giáo viên
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG" id="u3516">
		         <div class="" id="u3516_div">
		         </div>
		         <div class="text" id="u3516_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG2" id="u3517">
		         <div class="" id="u3517_div">
		         </div>
		         <div class="text" id="u3517_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u3518">
		         <img class="img" id="u3518_img" src="images/account_information/u141.svg"/>
		         <div class="text" id="u3518_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u3519">
		      <img class="img" id="u3519_img" src="images/account_information/u142.svg"/>
		      <div class="text" id="u3519_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u3520">
		      <img class="img" id="u3520_img" src="images/account_information/u143.svg"/>
		      <div class="text" id="u3520_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div id="u3427" style={{"display":"none","visibility":"hidden"}}>
		   </div>
		   <div class="ax_default" data-height="32" data-label="Header" data-left="133" data-top="17" data-width="325" id="u3521">
		      <div class="ax_default paragraph" id="u3522">
		         <div class="" id="u3522_div">
		         </div>
		         <div class="text" id="u3522_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Khai báo thời gian rảnh
		               </span>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default label" id="u3523">
		      <div class="" id="u3523_div">
		      </div>
		      <div class="text" id="u3523_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Tháng khai báo
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default droplist" id="u3524">
		      <div class="" id="u3524_div">
		      </div>
		      <select class="u3524_input" id="u3524_input">
		      </select>
		   </div>
		   <div class="ax_default" id="u3525">
		      <div class="ax_default table_cell" id="u3526">
		         <img class="img" id="u3526_img" src="images/th_i_gian_r_nh-th_m/u3526.png"/>
		         <div class="text" id="u3526_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Thứ 2
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u3527">
		         <img class="img" id="u3527_img" src="images/th_i_gian_r_nh-th_m/u3526.png"/>
		         <div class="text" id="u3527_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Thứ 3
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u3528">
		         <img class="img" id="u3528_img" src="images/th_i_gian_r_nh-th_m/u3526.png"/>
		         <div class="text" id="u3528_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Thứ 4
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u3529">
		         <img class="img" id="u3529_img" src="images/th_i_gian_r_nh-th_m/u3526.png"/>
		         <div class="text" id="u3529_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Thứ 5
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u3530">
		         <img class="img" id="u3530_img" src="images/th_i_gian_r_nh-th_m/u3526.png"/>
		         <div class="text" id="u3530_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Thứ 6
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u3531">
		         <img class="img" id="u3531_img" src="images/th_i_gian_r_nh-th_m/u3526.png"/>
		         <div class="text" id="u3531_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Thứ 7
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u3532">
		         <img class="img" id="u3532_img" src="images/th_i_gian_r_nh-th_m/u3532.png"/>
		         <div class="text" id="u3532_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Chủ Nhật
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u3533">
		         <img class="img" id="u3533_img" src="images/th_i_gian_r_nh-th_m/u3533.png"/>
		         <div class="text" id="u3533_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u3534">
		         <img class="img" id="u3534_img" src="images/th_i_gian_r_nh-th_m/u3533.png"/>
		         <div class="text" id="u3534_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  1
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u3535">
		         <img class="img" id="u3535_img" src="images/th_i_gian_r_nh-th_m/u3533.png"/>
		         <div class="text" id="u3535_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u3536">
		         <img class="img" id="u3536_img" src="images/th_i_gian_r_nh-th_m/u3533.png"/>
		         <div class="text" id="u3536_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u3537">
		         <img class="img" id="u3537_img" src="images/th_i_gian_r_nh-th_m/u3533.png"/>
		         <div class="text" id="u3537_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u3538">
		         <img class="img" id="u3538_img" src="images/th_i_gian_r_nh-th_m/u3533.png"/>
		         <div class="text" id="u3538_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u3539">
		         <img class="img" id="u3539_img" src="images/th_i_gian_r_nh-th_m/u3539.png"/>
		         <div class="text" id="u3539_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u3540">
		         <img class="img" id="u3540_img" src="images/th_i_gian_r_nh-th_m/u3533.png"/>
		         <div class="text" id="u3540_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u3541">
		         <img class="img" id="u3541_img" src="images/th_i_gian_r_nh-th_m/u3533.png"/>
		         <div class="text" id="u3541_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u3542">
		         <img class="img" id="u3542_img" src="images/th_i_gian_r_nh-th_m/u3533.png"/>
		         <div class="text" id="u3542_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u3543">
		         <img class="img" id="u3543_img" src="images/th_i_gian_r_nh-th_m/u3533.png"/>
		         <div class="text" id="u3543_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u3544">
		         <img class="img" id="u3544_img" src="images/th_i_gian_r_nh-th_m/u3533.png"/>
		         <div class="text" id="u3544_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u3545">
		         <img class="img" id="u3545_img" src="images/th_i_gian_r_nh-th_m/u3533.png"/>
		         <div class="text" id="u3545_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u3546">
		         <img class="img" id="u3546_img" src="images/th_i_gian_r_nh-th_m/u3539.png"/>
		         <div class="text" id="u3546_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u3547">
		         <img class="img" id="u3547_img" src="images/th_i_gian_r_nh-th_m/u3533.png"/>
		         <div class="text" id="u3547_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u3548">
		         <img class="img" id="u3548_img" src="images/th_i_gian_r_nh-th_m/u3533.png"/>
		         <div class="text" id="u3548_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u3549">
		         <img class="img" id="u3549_img" src="images/th_i_gian_r_nh-th_m/u3533.png"/>
		         <div class="text" id="u3549_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u3550">
		         <img class="img" id="u3550_img" src="images/th_i_gian_r_nh-th_m/u3533.png"/>
		         <div class="text" id="u3550_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u3551">
		         <img class="img" id="u3551_img" src="images/th_i_gian_r_nh-th_m/u3533.png"/>
		         <div class="text" id="u3551_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u3552">
		         <img class="img" id="u3552_img" src="images/th_i_gian_r_nh-th_m/u3533.png"/>
		         <div class="text" id="u3552_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u3553">
		         <img class="img" id="u3553_img" src="images/th_i_gian_r_nh-th_m/u3539.png"/>
		         <div class="text" id="u3553_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u3554">
		         <img class="img" id="u3554_img" src="images/th_i_gian_r_nh-th_m/u3533.png"/>
		         <div class="text" id="u3554_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u3555">
		         <img class="img" id="u3555_img" src="images/th_i_gian_r_nh-th_m/u3533.png"/>
		         <div class="text" id="u3555_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u3556">
		         <img class="img" id="u3556_img" src="images/th_i_gian_r_nh-th_m/u3533.png"/>
		         <div class="text" id="u3556_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u3557">
		         <img class="img" id="u3557_img" src="images/th_i_gian_r_nh-th_m/u3533.png"/>
		         <div class="text" id="u3557_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u3558">
		         <img class="img" id="u3558_img" src="images/th_i_gian_r_nh-th_m/u3533.png"/>
		         <div class="text" id="u3558_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u3559">
		         <img class="img" id="u3559_img" src="images/th_i_gian_r_nh-th_m/u3533.png"/>
		         <div class="text" id="u3559_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u3560">
		         <img class="img" id="u3560_img" src="images/th_i_gian_r_nh-th_m/u3539.png"/>
		         <div class="text" id="u3560_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u3561">
		         <img class="img" id="u3561_img" src="images/th_i_gian_r_nh-th_m/u3561.png"/>
		         <div class="text" id="u3561_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u3562">
		         <img class="img" id="u3562_img" src="images/th_i_gian_r_nh-th_m/u3561.png"/>
		         <div class="text" id="u3562_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u3563">
		         <img class="img" id="u3563_img" src="images/th_i_gian_r_nh-th_m/u3561.png"/>
		         <div class="text" id="u3563_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u3564">
		         <img class="img" id="u3564_img" src="images/th_i_gian_r_nh-th_m/u3561.png"/>
		         <div class="text" id="u3564_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u3565">
		         <img class="img" id="u3565_img" src="images/th_i_gian_r_nh-th_m/u3561.png"/>
		         <div class="text" id="u3565_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u3566">
		         <img class="img" id="u3566_img" src="images/th_i_gian_r_nh-th_m/u3561.png"/>
		         <div class="text" id="u3566_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u3567">
		         <img class="img" id="u3567_img" src="images/th_i_gian_r_nh-th_m/u3567.png"/>
		         <div class="text" id="u3567_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u3568">
		      <div class="" id="u3568_div">
		      </div>
		      <div class="text" id="u3568_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Giờ học 1 17h30-19h00
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u3569">
		      <div class="" id="u3569_div">
		      </div>
		      <div class="text" id="u3569_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Giờ học 2 19h00-20h30
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u3570">
		      <div class="" id="u3570_div">
		      </div>
		      <div class="text" id="u3570_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Giờ học 1 17h30-19h00
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u3571">
		      <div class="" id="u3571_div">
		      </div>
		      <div class="text" id="u3571_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Giờ học 2 19h00-20h30
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u3572">
		      <div class="" id="u3572_div">
		      </div>
		      <div class="text" id="u3572_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Giờ học 1 17h30-19h00
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u3573">
		      <div class="" id="u3573_div">
		      </div>
		      <div class="text" id="u3573_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Giờ học 2 19h00-20h30
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u3574">
		      <div class="" id="u3574_div">
		      </div>
		      <div class="text" id="u3574_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Giờ học 1 17h30-19h00
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u3575">
		      <div class="" id="u3575_div">
		      </div>
		      <div class="text" id="u3575_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Giờ học 2 19h00-20h30
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u3576">
		      <div class="" id="u3576_div">
		      </div>
		      <div class="text" id="u3576_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Giờ học 1 17h30-19h00
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u3577">
		      <div class="" id="u3577_div">
		      </div>
		      <div class="text" id="u3577_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Giờ học 2 19h00-20h30
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u3578">
		      <div class="" id="u3578_div">
		      </div>
		      <div class="text" id="u3578_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Giờ học sáng 1 8h30-10h00
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u3579">
		      <div class="" id="u3579_div">
		      </div>
		      <div class="text" id="u3579_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Giờ học sáng 2 10h00-11h30
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u3580">
		      <div class="" id="u3580_div">
		      </div>
		      <div class="text" id="u3580_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Giờ học 1 17h30-19h00
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u3581">
		      <div class="" id="u3581_div">
		      </div>
		      <div class="text" id="u3581_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Giờ học 2 19h00-20h30
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u3582">
		      <div class="" id="u3582_div">
		      </div>
		      <div class="text" id="u3582_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Giờ học sáng 1 8h30-10h00
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u3583">
		      <div class="" id="u3583_div">
		      </div>
		      <div class="text" id="u3583_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Giờ học sáng 2 10h00-11h30
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u3584">
		      <div class="" id="u3584_div">
		      </div>
		      <div class="text" id="u3584_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Giờ học 1 17h30-19h00
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u3585">
		      <div class="" id="u3585_div">
		      </div>
		      <div class="text" id="u3585_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Giờ học 2 19h00-20h30
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u3586">
		      <div class="" id="u3586_div">
		      </div>
		      <div class="text" id="u3586_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Giờ học 1 17h30-19h00
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u3587">
		      <div class="" id="u3587_div">
		      </div>
		      <div class="text" id="u3587_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Giờ học 2 19h00-20h30
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u3588">
		      <div class="" id="u3588_div">
		      </div>
		      <div class="text" id="u3588_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Giờ học 1 17h30-19h00
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u3589">
		      <div class="" id="u3589_div">
		      </div>
		      <div class="text" id="u3589_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Giờ học 2 19h00-20h30
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u3590">
		      <div class="" id="u3590_div">
		      </div>
		      <div class="text" id="u3590_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Giờ học 1 17h30-19h00
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u3591">
		      <div class="" id="u3591_div">
		      </div>
		      <div class="text" id="u3591_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Giờ học 2 19h00-20h30
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u3592">
		      <div class="" id="u3592_div">
		      </div>
		      <div class="text" id="u3592_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Giờ học 1 17h30-19h00
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u3593">
		      <div class="" id="u3593_div">
		      </div>
		      <div class="text" id="u3593_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Giờ học 2 19h00-20h30
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u3594">
		      <div class="" id="u3594_div">
		      </div>
		      <div class="text" id="u3594_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Giờ học 1 17h30-19h00
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u3595">
		      <div class="" id="u3595_div">
		      </div>
		      <div class="text" id="u3595_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Giờ học 2 19h00-20h30
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u3596">
		      <div class="" id="u3596_div">
		      </div>
		      <div class="text" id="u3596_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Giờ học 1 17h30-19h00
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u3597">
		      <div class="" id="u3597_div">
		      </div>
		      <div class="text" id="u3597_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Giờ học 2 19h00-20h30
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u3598">
		      <div class="" id="u3598_div">
		      </div>
		      <div class="text" id="u3598_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Giờ học sáng 1 8h30-10h00
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u3599">
		      <div class="" id="u3599_div">
		      </div>
		      <div class="text" id="u3599_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Giờ học sáng 2 10h00-11h30
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u3600">
		      <div class="" id="u3600_div">
		      </div>
		      <div class="text" id="u3600_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Giờ học sáng 1 8h30-10h00
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u3601">
		      <div class="" id="u3601_div">
		      </div>
		      <div class="text" id="u3601_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Giờ học sáng 2 10h00-11h30
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u3602">
		      <div class="" id="u3602_div">
		      </div>
		      <div class="text" id="u3602_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Lưu
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default button" id="u3603">
		      <div class="" id="u3603_div">
		      </div>
		      <div class="text" id="u3603_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Hủy
		            </span>
		         </p>
		      </div>
		   </div>
		</div>
		
      </div>
    );
  }
}

export default Page;
