import React, { useEffect, useState } from "react";
import "./index.css";
import { connect } from "dva";
import { Switch, Route, Redirect } from "react-router-dom";
import HomeLayout from "../../layouts/HomeLayout";
import { ToastContainer, toast } from "react-toastify";
import account from "./account";

import lich_hoc from "./lich_hoc";
import tao_moi_lich_hoc from "./tao_moi_lich_hoc";
import chinh_sua_lich_hoc from "./chinh_sua_lich_hoc";
import tao_moi_lich_hoc_ngoai_khoa from "./tao_moi_lich_hoc/add";
import lop_dang_lich from "./lop_dang_lich";
import danh_sach_giao_vien from "./danh_sach_giao_vien";
import chi_tiet_giao_vien_rate_giao_vien from "./chi_tiet_giao_vien_rate_giao_vien";
import chi_tiet_giao_vien_tong_quan from "./chi_tiet_giao_vien_tong_quan";
import chi_tiet_giao_vien_lich_su_nhan_xet_du_gio from "./chi_tiet_giao_vien_lich_su_nhan_xet_du_gio";
import chi_tiet_giao_vien_lich_nhan_xet_du_gio from "./chi_tiet_giao_vien_lich_nhan_xet_du_gio";
import chi_tiet_giao_vien_nhan_xet_tu_phu_huynh from "./chi_tiet_giao_vien_nhan_xet_tu_phu_huynh";
import danh_gia_du_gio from "./danh_gia_du_gio";
import them_phu_huynh from "./them_phu_huynh";
import them_hoc_vien from "./them_hoc_vien";
import xuat_hoa_don from "./xuat_hoa_don";
import chinh_sua_hoa_don_chua_xuat from "./chinh_sua_hoa_don_chua_xuat";
import crm_list from "./crm_list";
import chi_tiet_kh_hoa_don from "./chi_tiet_kh_hoa_don";
import chi_tiet_kh_trao_doi from "./chi_tiet_kh_trao_doi";
import chi_tiet_kh_ls_cham_soc from "./chi_tiet_kh_ls_cham_soc";
import chi_tiet_kh_lich_su_hoc from "./chi_tiet_kh_lich_su_hoc";
import chi_tiet_kh_diem_danh_nhan_xet from "./chi_tiet_kh_diem_danh_nhan_xet";
import thong_ke_thu_nhap from "./thong_ke_thu_nhap";
import thanh_toan_hoa_don from "./thanh_toan_hoa_don";
import chi_tiet_hoa_don from "./chi_tiet_hoa_don";
import xoa_hoa_don from "./xoa_hoa_don";
import brand_management from "./brand_management";
import holiday from "./holiday";
import quan_ly_phan_quyen from "./quan_ly_phan_quyen";
import quan_ly_phong_ban_vai_tro from "./quan_ly_phong_ban_vai_tro";
import trung_tam from "./trung_tam";
import quan_ly_gio_hoc from "./quan_ly_gio_hoc";
import student_status from "./student_status";
import evaluation_category from "./evaluation_category";
import room_management from "./room_management";
import program from "./program";
import student_s_school from "./student_s_school";
import lession from "./lession";
import add_lession from "./lession/add";
import thoi_gian_ranh_list from "./thoi_gian_ranh_list";
import thoi_gian_ranh_them from "./thoi_gian_ranh_list/add";
import course_price from "./course_price";
import promotion from "./promotion";
import kenh_thanh_toan from "./kenh_thanh_toan";
import kho from "./kho";
import phieu_nhap_xuat from "./phieu_nhap_xuat";
import nhap_kho_tu_dong from "./nhap_kho_tu_dong";
import xuat_hang from "./xuat_hang";
import kiem_kho from "./kiem_kho";
import quan_ly_san_pham from "./quan_ly_san_pham";
import nha_cung_cap from "./nha_cung_cap";
import chuyen_kho from "./chuyen_kho";
import danh_sach_hv from "./danh_sach_hv";

import danh_sach_hoc_sinh_trong_lop from "./danh_sach_hoc_sinh_trong_lop";
import account_information from "./account_information";

import add_class from "./add_class";
import update_class from "./update_class";
// import login from "../auth/login";
// import AccountInformation from "./account_information";

import danh_sach_lop from "./danh_sach_lop";

import ghep_lop from "./ghep_lop";

import giao_vien from "./giao_vien";

import nhap_hang from "./nhap_hang";

import tong_hop_diem_danh_theo_lop from "./tong_hop_diem_danh_theo_lop";

import diem_danh_nhan_xet from "./diem_danh_nhan_xet";

import chi_tiet_kh_kiem_tra_hoc_thu from "./chi_tiet_kh_kiem_tra_hoc_thu";
import them_moi_kiem_kho from "./them_moi_kiem_kho";

import common from "../home/components/common";

import chi_tiet_hv_diem_danh from "./chi_tiet_hv_diem_danh";
import chi_tiet_hv_bao_luu from "./chi_tiet_hv_bao_luu";
import chi_tiet_hv_vi from "./chi_tiet_hv_vi";

function Page(props) {
  const [count, setCount] = useState(0);
  useEffect(() => {
    const { dispatch } = props;
    dispatch({
      type: "Global/showLoading"
    });
    Promise.all([
      dispatch({
        type: "User/view",
        payload: {}
      }),
      dispatch({
        type: "Account/gender",
        payload: {}
      }),
      dispatch({
        type: "Account/maritalstatus",
        payload: {}
      }),
      dispatch({
        type: "Account/contracttype",
        payload: {}
      }),
      dispatch({
        type: "Brand/list",
        payload: {}
      }),

      dispatch({
        type: "Center/list",
        payload: {}
      }),

      dispatch({
        type: "DepartementRole/departement",
        payload: {}
      }),

      dispatch({
        type: "DepartementRole/list",
        payload: {}
      }),

      dispatch({
        type: "Dictionary/listdic",
        payload: {}
      }),
      dispatch({
        type: "Account/getprovince",
        payload: {}
      }),
      dispatch({
        type: "Account/getnationality",
        payload: {}
      }),

      dispatch({
        type: "Evaluation/evaluationobject",
        payload: {}
      }),
      dispatch({
        type: "Group/permission",
        payload: {}
      }),
      dispatch({
        type: "Center/listbrand",
        payload: {}
      }),
      dispatch({
        type: "Center/listdirector",
        payload: {}
      })

      // dispatch({
      //   type: "Evaluation/evaluationobject",
      //   payload: {}
      // })
    ]).then(() => {
      dispatch({
        type: "Global/hideLoading"
      });
    });
  }, [count]);
  return (
    <HomeLayout>
      <Switch>
        {/* <Redirect from="/*" to="/account" component={account} /> */}
        <Route path="/my_account" component={account_information} />
        {/* End Login */}

        {/* Start Dashboard */}
        <Redirect exact from="/" to="/giao_vien" />
        <Route path="/giao_vien" component={giao_vien} />
        {/* End Dashboard */}

        {/* Start Class */}
        <Route path="/danh_sach_lop" component={danh_sach_lop} />
        <Route path="/ghep_lop" component={ghep_lop} />
        <Route path="/diem_danh/:id" component={diem_danh_nhan_xet} />
        <Route
          path="/tong_hop_diem_danh/:centerId/:id"
          component={tong_hop_diem_danh_theo_lop}
        />
        <Route
          path="/danh_sach_hoc_sinh_trong_lop"
          component={danh_sach_hoc_sinh_trong_lop}
        />
        <Route path="/lich_hoc/:id" component={lich_hoc} />
        <Route path="/tao_moi_lich_hoc" component={tao_moi_lich_hoc} />
        <Route path="/chinh_sua_lich_hoc/:id" component={chinh_sua_lich_hoc} />
        <Route
          path="/tao_moi_lich_hoc/add"
          component={tao_moi_lich_hoc_ngoai_khoa}
        />
        <Route path="/lop_dang_lich" component={lop_dang_lich} />
        {/* End Class */}

        {/* Start Teacher */}
        <Route path="/danh_sach_giao_vien" component={danh_sach_giao_vien} />
        <Route
          path="/chi_tiet_giao_vien_rate_giao_vien"
          component={chi_tiet_giao_vien_rate_giao_vien}
        />
        <Route
          path="/chi_tiet_giao_vien_tong_quan"
          component={chi_tiet_giao_vien_tong_quan}
        />
        <Route
          path="/chi_tiet_giao_vien_lich_su_nhan_xet_du_gio"
          component={chi_tiet_giao_vien_lich_su_nhan_xet_du_gio}
        />
        <Route
          path="/chi_tiet_giao_vien_lich_nhan_xet_du_gio"
          component={chi_tiet_giao_vien_lich_nhan_xet_du_gio}
        />
        <Route
          path="/chi_tiet_giao_vien_nhan_xet_tu_phu_huynh"
          component={chi_tiet_giao_vien_nhan_xet_tu_phu_huynh}
        />
        <Route path="/danh_gia_du_gio" component={danh_gia_du_gio} />
        {/* End Teacher */}

        {/* Start CRM */}

        <Route path="/them_phu_huynh" component={them_phu_huynh} />
        <Route path="/them_hoc_vien" component={them_hoc_vien} />
        <Route path="/xuat_hoa_don" component={xuat_hoa_don} />
        <Route path="/hd_chua_xuat" component={chinh_sua_hoa_don_chua_xuat} />
        <Route path="/crm_list" component={crm_list} />
        <Route path="/hoa_don/:id" component={chi_tiet_kh_hoa_don} />
        <Route path="/trao_doi/:id" component={chi_tiet_kh_trao_doi} />
        <Route path="/cham_soc/:id" component={chi_tiet_kh_ls_cham_soc} />
        <Route path="/lich_su_hoc/:id" component={chi_tiet_kh_lich_su_hoc} />
        <Route
          path="/dd_nhan_xet/:id"
          component={chi_tiet_kh_diem_danh_nhan_xet}
        />
        <Route
          path="/kiem_tra_hoc_thu"
          component={chi_tiet_kh_kiem_tra_hoc_thu}
        />
        {/* End CRM */}

        <Route path="/thong_ke_thu_nhap" component={thong_ke_thu_nhap} />

        {/* Start Hoa don */}
        <Route path="/thanh_toan" component={thanh_toan_hoa_don} />
        <Route path="/chi_tiet_hd" component={chi_tiet_hoa_don} />
        {/* End Hoa don */}

        {/* Start System Setting */}
        <Route path="/account" component={account} />
        <Route path="/brand_management" component={brand_management} />
        <Route path="/holiday" component={holiday} />
        <Route
          path="/quan_ly_phong_ban_vai_tro"
          component={quan_ly_phong_ban_vai_tro}
        />
        <Route path="/phan_quyen" component={quan_ly_phan_quyen} />
        <Route path="/trung_tam" component={trung_tam} />
        {/* End System Setting */}

        {/* Start Academy Setting */}
        <Route path="/quan_ly_gio_hoc" component={quan_ly_gio_hoc} />
        <Route path="/student_status" component={student_status} />
        <Route path="/evaluation_category" component={evaluation_category} />
        <Route path="/room_management" component={room_management} />
        <Route path="/program" component={program} />
        <Route path="/student_s_school" component={student_s_school} />
        {/* End Academy Setting */}

        {/* Start Lession */}
        <Route path="/lession" component={lession} />
        <Route path="/lession/add" component={add_lession} />
        {/* End Lession */}

        {/* Start Thoi gian ranh - List */}
        <Route path="/thoi_gian_ranh_list" component={thoi_gian_ranh_list} />
        <Route
          path="/thoi_gian_ranh_list/view"
          component={thoi_gian_ranh_them}
        />
        {/* End Thoi gian ranh - List */}

        {/* Start Finance */}
        <Route path="/course_price" component={course_price} />
        <Route path="/promotion" component={promotion} />
        <Route path="/kenh_thanh_toan" component={kenh_thanh_toan} />
        {/* End Finance */}

        {/* Start Kho */}
        <Route path="/kho" component={kho} />
        {/* End Kho */}

        {/* Start Add class */}
        <Route path="/them_lop" component={add_class} />
        <Route path="/sua_lop/:id" component={update_class} />
        {/* End Add class */}

        {/* Start Phieu nhap xuat */}
        <Route path="/phieu_nhap_xuat" component={phieu_nhap_xuat} />
        {/* End Phieu nhap xuat */}

        {/* Start Ton kho */}
        <Route
          path="/nhap_hang/nhap_kho_tu_dong"
          component={nhap_kho_tu_dong}
        />
        {/* End Ton kho */}

        {/* Start Xuat hang */}
        <Route path="/xuat_hang" component={xuat_hang} />
        {/* End Xuat hang */}

        {/* Start Kiem kho */}
        <Route path="/kiem_kho" component={kiem_kho}></Route>
        <Route
          path="/kiem_kho/them_moi_kiem_kho"
          component={them_moi_kiem_kho}
        />
        {/* End Kiem kho */}

        {/* Start Quan ly san pham */}
        <Route path="/quan_ly_san_pham" component={quan_ly_san_pham} />
        {/* End Quan ly san pham */}

        {/* Start Nha cung cap */}
        <Route path="/nha_cung_cap" component={nha_cung_cap} />
        {/* End Nha cung cap */}

        {/* Start Chuyen kho */}
        <Route path="/chuyen_kho" component={chuyen_kho} />
        {/* End Chuyen kho */}

        <Route path="/nhap_hang" component={nhap_hang} />

        {/* Start Hoc Vien */}
        <Route path="/danh_sach_hv" component={danh_sach_hv} />
        <Route
          path="/diem_danh_hv/:id/:classId"
          component={chi_tiet_hv_diem_danh}
        />
        <Route
          path="/bao_luu_hv/:id/:classId"
          component={chi_tiet_hv_bao_luu}
        />
        <Route path="/vi_hv/:id/:classId" component={chi_tiet_hv_vi} />

        {/* End Hoc Vien */}
      </Switch>
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
      {/* Same as */}
      <ToastContainer />
    </HomeLayout>
  );
}

export default connect(
  ({
    References,
    Account,
    DepartementRole,
    Group,
    Brand,
    Center,
    Evaluation,
    StudentStatus,
    User
  }) => ({
    References,
    Account,
    DepartementRole,
    Evaluation,
    Group,
    Brand,
    Center,
    StudentStatus,
    User
  })
)(Page);
