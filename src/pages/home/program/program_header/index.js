import React from "react";
import { connect } from "dva";
import { Button } from "react-bootstrap";
import { Formik } from "formik";
import Input from "../../../../components/Form/Input"
import useProgram from "../../../../hooks/useProgram";

class Page extends React.Component {
  constructor(props) {
    super(props);
  }
  // temporary solution to use hook model in class component
  program = useProgram({ dispatch: this.props.dispatch });
  render() {
    return (
      <fieldset>
        <legend>Tìm kiếm</legend>
        <Formik
          initialValues={{ keyword: ""}}
          onSubmit={(values) => {
            this.program.search(values);
          }}>
          {
            ({
               values, handleSubmit, handleChange, handleBlur
             }) => (
              <form onSubmit={handleSubmit}>
                <div className="row">
                  <div className="col-12">
                    <div className="row align-end">
                      <div className="col-md-3">
                        <Input
                          id="keyword"
                          name="keyword"
                          label="Thông tin"
                          value={values.keyword}
                          onChange={handleChange}
                          onBlur={handleBlur}
                        />
                      </div>
                      <div className="col-md-3">
                        <div className="form-group">
                          <Button variant="primary" type="submit">
                            Tìm kiếm
                          </Button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            )
          }
        </Formik>
      </fieldset>
    );
  }
}

export default connect()(Page);
