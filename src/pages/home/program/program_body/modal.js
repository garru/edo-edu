import React, {useState} from "react";
import {Button, Modal, Table} from "react-bootstrap";
import {BAI_KIEM_TRA, CAP_DO} from "../../../../mock/dropdown";
import Paging from "../../../../components/Paging";
import {Formik} from "formik";
import {DATA_MODAL, HEADER_MODAL} from "./data";
import { connect } from "dva";
import Input from "../../../../components/Form/Input";
import Select from "../../../../components/Form/Select";
import TextArea from "../../../../components/Form/TextArea";
import useProgram from "../../../../hooks/useProgram";

const CustomModal = (props) => {
  const { dispatch, onClose, isCreateNew, Program } = props;
  const { initialValues, validationSchema, submit } = useProgram({ dispatch, onClose, isCreateNew });
  return (
    <Modal
      size={"lg"}
      show
      backdrop="static"
      keyboard={false}
      id="AddNew"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Formik
        initialValues={!isCreateNew ? Program?.current_program : initialValues}
        validationSchema={validationSchema}
        onSubmit={submit}
      >
        {({
            values, handleChange, handleBlur, handleSubmit
        }) => (
          <form onSubmit={handleSubmit}>
            <Modal.Header closeButton>
              <Modal.Title>
                {isCreateNew
                  ? "Thêm mới chương trình học"
                  : "Chỉnh sửa chương trình học"}
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <div>
                  <div className="row">
                    <div className="col-12 m-b-2">
                      <fieldset>
                        <legend>Thông tin cơ bản</legend>
                        <div className="row ">
                          <div className="col-md-6 ">
                            <Input
                              id="name"
                              name="name"
                              label="Chương trình"
                              value={values.name}
                              onChange={handleChange}
                              onBlur={handleBlur}
                            />
                            <Select
                              id="level"
                              name="level"
                              label="Cấp độ:"
                              options={CAP_DO}
                              value={values.level}
                              onChange={handleChange}
                              onBlur={handleBlur}
                            />
                            <Input
                              id="lessonCount"
                              name="lessonCount"
                              label="Số buổi học:"
                              type="number"
                              value={values.lessonCount}
                              onChange={handleChange}
                              onBlur={handleBlur}
                            />
                          </div>
                          <div className="col-md-6 ">
                            <TextArea
                              id="description"
                              name="description"
                              label="Mô tả:"
                              value={values.description}
                              onChange={handleChange}
                              onBlur={handleBlur}
                            />
                          </div>
                          <div className="col-12">
                            <div className="form-group">
                              <label htmlFor="inputGroupFile02">
                                Upload buổi học
                              </label>
                              <div className="input-group mb-3">
                                <div className="custom-file">
                                  <input
                                    type="file"
                                    className="custom-file-input"
                                    id="inputGroupFile02"
                                  />
                                  <label
                                    className="custom-file-label"
                                    htmlFor="inputGroupFile02"
                                  >
                                    Choose file
                                  </label>
                                </div>
                              </div>
                              <p>
                                <i>
                                  Chỉ chấp nhận với file Excel có đuôi xlsx hoặc xls
                                </i>
                              </p>
                              <p>
                                <i>
                                  Quý khách có thể tải file mẫu <a href="">tại đây</a>
                                </i>
                              </p>
                            </div>
                          </div>
                        </div>
                      </fieldset>
                    </div>
                    <div className="col-12">
                      <fieldset>
                        <legend>Thông tin cơ bản</legend>
                        <div className="row ">
                          <div className="col-12">
                            <div style={{width: "100%"}}>
                              <div style={{overflowX: "auto", width: "100%"}}>
                                <Table
                                  bordered
                                  hover
                                  // style={{ minWidth: 1500 }}
                                  className="table"
                                >
                                  <thead>
                                  <tr>
                                    {HEADER_MODAL.map(item => (
                                      <th
                                        className={`${item.class}`}
                                        key={item.key}
                                      >
                                        <span>{item.title}</span>
                                      </th>
                                    ))}
                                  </tr>
                                  </thead>
                                  <tbody>
                                  {DATA_MODAL.map((item, index) => (
                                    <tr key={`account_${item.id}`}>
                                      <td>
                                        <Select
                                          id="loai_trung_tam"
                                          name="load_trung_tam"
                                          options={BAI_KIEM_TRA}
                                        />
                                      </td>
                                      <td>
                                        <Input
                                          id="buoi"
                                          name="buoi"
                                          defaultValue={item.buoi}
                                        />
                                      </td>
                                      <td>{item.so_buoi}</td>
                                    </tr>
                                  ))}
                                  </tbody>
                                </Table>
                                {DATA_MODAL && DATA_MODAL.length ? (
                                  ""
                                ) : (
                                  <div className="not-found">
                                    <i className="fas fa-inbox"></i>
                                    <span>Không tìm thấy kết quả</span>
                                  </div>
                                )}
                              </div>
                            </div>
                            <Paging
                              pageNumber={10}
                              total={550}
                              pageSize={10}
                              onChange={console.log}
                            />
                          </div>
                        </div>
                      </fieldset>
                    </div>
                  </div>
              </div>
            </Modal.Body>
            <Modal.Footer>
              <Button variant="primary" type="submit" style={{ marginRight: 15 }}>
                {isCreateNew ? "Tạo mới" : "Cập nhật"}
              </Button>
              <Button variant={"secondary"} onClick={() => onClose(true)}>
                Hủy
              </Button>
            </Modal.Footer>
          </form>
          )}
      </Formik>
    </Modal>
  );
}

export default connect(
  ({ Program }) => ({ Program })
)(CustomModal);