export const HEADER = [
  {
    title: "Chương trình",
    key: "chuong_trinh",
    class: "tb-width-200"
  },
  {
    title: "Trình độ",
    key: "trinh_do",
    class: "tb-width-150"
  },
  {
    title: "Mô tả",
    key: "mo_ta",
    class: "tb-width-500"
  },
  {
    title: "Số buổi học",
    key: "so_buoi_hoc",
    class: "tb-width-150"
  }
];

export const DATA = [
  {
    id: "1",
    chuong_trinh: "BIG ENGLIST PLUS 1",
    trinh_do: "Starter",
    mo_ta:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
    so_buoi_hoc: "33"
  },
  {
    id: "1",
    chuong_trinh: "BIG ENGLIST PLUS 1",
    trinh_do: "Starter",
    mo_ta:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
    so_buoi_hoc: "33"
  },
  {
    id: "1",
    chuong_trinh: "BIG ENGLIST PLUS 1",
    trinh_do: "Starter",
    mo_ta:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
    so_buoi_hoc: "33"
  },
  {
    id: "1",
    chuong_trinh: "BIG ENGLIST PLUS 1",
    trinh_do: "Starter",
    mo_ta:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
    so_buoi_hoc: "33"
  },
  {
    id: "1",
    chuong_trinh: "BIG ENGLIST PLUS 1",
    trinh_do: "Starter",
    mo_ta:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
    so_buoi_hoc: "33"
  },
  {
    id: "1",
    chuong_trinh: "BIG ENGLIST PLUS 1",
    trinh_do: "Starter",
    mo_ta:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
    so_buoi_hoc: "33"
  },
  {
    id: "1",
    chuong_trinh: "BIG ENGLIST PLUS 1",
    trinh_do: "Starter",
    mo_ta:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
    so_buoi_hoc: "33"
  }
];

export const HEADER_MODAL = [
  {
    title: "Bài kiểm tra",
    key: "bai_kiem_tra",
    class: "tb-width-200"
  },
  {
    title: "Buổi",
    key: "buoi",
    class: "tb-width-400"
  },
  {
    title: "Số buổi",
    key: "so_buoi",
    class: "tb-width-100"
  }
];

export const DATA_MODAL = [
  {
    id: "1",
    bai_kiem_tra: "1",
    buoi: "Buổi 3, buổi 6, buổi 9, buổi 12",
    so_buoi_hoc: "33"
  }
];
