import React from "react";
import { connect } from "dva";
import ProgramHeader from "./program_header";
import ProgramBody from "./program_body";
import PageHeader from "../../../components/PageHeader";

class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: "Program/search"
    })
  }

  render() {
    return (
      <>
        <PageHeader
          title="Quản trị chương trình học"
          breadcrums={[{
            title: "Home",
            path: "/"
          }, {
            title: "Quản trị chương trình học",
            path: "program"
          }]}
          subTitle="Quản trị khóa học và các bài học.bài kiểm tra trong khóa học"
        />
        <div className="iq-card  pb-0">
          <div className="iq-card-body">
            <div className="">
              <ProgramHeader />
              <ProgramBody />
            </div>
          </div>
        </div>
      </>

    );
  }
}

export default connect(({ Program, Global }) => ({
  Program,
  Global
}))(Page);
