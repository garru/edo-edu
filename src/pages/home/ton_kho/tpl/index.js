import React from 'react';
import './index.css';
import './ax.css';

class Page extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
		<div class="" id="base" style={{"margin-left":"-150px"}}>
		   <div class="ax_default shape" id="u4751">
		      <div class="" id="u4751_div">
		      </div>
		      <div class="text" id="u4751_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default shape" id="u4752">
		      <div class="" id="u4752_div">
		      </div>
		      <div class="text" id="u4752_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default" data-height="40" data-label="account" data-left="13" data-top="895" data-width="192" id="u4754">
		      <div class="ax_default shape" data-label="accountLable" id="u4755">
		         <img class="img" id="u4755_img" src="images/account_information/accountlable_u55.svg"/>
		         <div class="text" id="u4755_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Nguyen Van A
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u4756">
		         <img class="img" id="u4756_img" src="images/login/u4.svg"/>
		         <div class="text" id="u4756_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="settingMenu" data-left="11" data-top="826" data-width="125" id="u4757">
		      <div class="ax_default shape" data-label="gearIconLable" id="u4758">
		         <img class="img" id="u4758_img" src="images/account_information/geariconlable_u58.svg"/>
		         <div class="text" id="u4758_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Cài đặt
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="44" data-left="11" data-top="826" data-width="44" id="u4759">
		         <div class="ax_default shape" data-label="gearIconBG" id="u4760">
		            <div class="" id="u4760_div">
		            </div>
		            <div class="text" id="u4760_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default shape" data-label="gearIconBG2" id="u4761">
		            <div class="" id="u4761_div">
		            </div>
		            <div class="text" id="u4761_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" data-label="gearIcon" id="u4762">
		            <img class="img" id="u4762_img" src="images/account_information/gearicon_u62.svg"/>
		            <div class="text" id="u4762_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="customerMenu" data-left="11" data-top="145" data-width="143" id="u4763">
		      <div class="ax_default shape" data-label="customerIconLable" id="u4764">
		         <img class="img" id="u4764_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u4764_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Khách hàng
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="customerIconBG" id="u4765">
		         <div class="" id="u4765_div">
		         </div>
		         <div class="text" id="u4765_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="customerIconBG2" id="u4766">
		         <div class="" id="u4766_div">
		         </div>
		         <div class="text" id="u4766_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" data-label="customer" id="u4767">
		         <img class="img" id="u4767_img" src="images/account_information/customer_u67.svg"/>
		         <div class="text" id="u4767_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="classMenu" data-left="11" data-top="198" data-width="143" id="u4768">
		      <div class="ax_default shape" data-label="classIconLable" id="u4769">
		         <img class="img" id="u4769_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u4769_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Lớp học
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG" id="u4770">
		         <div class="" id="u4770_div">
		         </div>
		         <div class="text" id="u4770_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG2" id="u4771">
		         <div class="" id="u4771_div">
		         </div>
		         <div class="text" id="u4771_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" data-label="classicon" id="u4772">
		         <img class="img" id="u4772_img" src="images/account_information/classicon_u72.svg"/>
		         <div class="text" id="u4772_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="dashboardMenu" data-left="11" data-top="91" data-width="143" id="u4773">
		      <div class="ax_default shape" data-label="dashboardIconLable" id="u4774">
		         <img class="img" id="u4774_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u4774_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Dashnoard
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="dashboardIconBG" id="u4775">
		         <div class="" id="u4775_div">
		         </div>
		         <div class="text" id="u4775_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="dashboardIconBG2" id="u4776">
		         <div class="" id="u4776_div">
		         </div>
		         <div class="text" id="u4776_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" data-label="dashboard" id="u4777">
		         <img class="img" id="u4777_img" src="images/account_information/dashboard_u77.svg"/>
		         <div class="text" id="u4777_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="129" data-label="AccountMenu" data-left="58" data-top="806" data-width="246" id="u4778" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default shape" id="u4779">
		         <div class="" id="u4779_div">
		         </div>
		         <div class="text" id="u4779_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u4780">
		         <div class="" id="u4780_div">
		         </div>
		         <div class="text" id="u4780_text">
		            <p>
		               <span style={{"font-family":"ArialMT, Arial","font-weight":"400","text-decoration":"none"}}>
		                  Xin chào:
		               </span>
		               <span style={{"font-family":"Arial Bold, Arial","font-weight":"700","text-decoration":"none"}}>
		                  Nguyen Van A
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4781">
		         <div class="ax_default image" id="u4782">
		            <img class="img" id="u4782_img" src="images/account_information/u82.png"/>
		            <div class="text" id="u4782_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u4783">
		            <div class="" id="u4783_div">
		            </div>
		            <div class="text" id="u4783_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Đăng xuất
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default line" id="u4784">
		         <img class="img" id="u4784_img" src="images/account_information/u84.svg"/>
		         <div class="text" id="u4784_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4785">
		         <div class="ax_default paragraph" id="u4786">
		            <div class="" id="u4786_div">
		            </div>
		            <div class="text" id="u4786_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tài khoản
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default image" id="u4787">
		            <img class="img" id="u4787_img" src="images/account_information/u87.png"/>
		            <div class="text" id="u4787_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="339" data-label="SettingPannel" data-left="58" data-top="525" data-width="339" id="u4788" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default shape" id="u4789">
		         <div class="" id="u4789_div">
		         </div>
		         <div class="text" id="u4789_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u4790">
		         <div class="" id="u4790_div">
		         </div>
		         <div class="text" id="u4790_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Hệ thống
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4791">
		         <div class="ax_default icon" id="u4792">
		            <img class="img" id="u4792_img" src="images/account_information/u92.svg"/>
		            <div class="text" id="u4792_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u4793">
		            <div class="" id="u4793_div">
		            </div>
		            <div class="text" id="u4793_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tài khoản
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4794">
		         <div class="ax_default paragraph" id="u4795">
		            <div class="" id="u4795_div">
		            </div>
		            <div class="text" id="u4795_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phân quyền
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u4796">
		            <img class="img" id="u4796_img" src="images/account_information/u96.svg"/>
		            <div class="text" id="u4796_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u4797">
		         <div class="" id="u4797_div">
		         </div>
		         <div class="text" id="u4797_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Đào tạo
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4798">
		         <div class="ax_default paragraph" id="u4799">
		            <div class="" id="u4799_div">
		            </div>
		            <div class="text" id="u4799_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Thương hiệu
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u4800">
		            <img class="img" id="u4800_img" src="images/account_information/u100.svg"/>
		            <div class="text" id="u4800_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4801">
		         <div class="ax_default paragraph" id="u4802">
		            <div class="" id="u4802_div">
		            </div>
		            <div class="text" id="u4802_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trung tâm
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u4803">
		            <img class="img" id="u4803_img" src="images/account_information/u103.svg"/>
		            <div class="text" id="u4803_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4804">
		         <div class="ax_default icon" id="u4805">
		            <img class="img" id="u4805_img" src="images/account_information/u105.svg"/>
		            <div class="text" id="u4805_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u4806">
		            <div class="" id="u4806_div">
		            </div>
		            <div class="text" id="u4806_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Ngày nghỉ
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4807">
		         <div class="ax_default icon" id="u4808">
		            <img class="img" id="u4808_img" src="images/account_information/u108.svg"/>
		            <div class="text" id="u4808_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u4809">
		            <div class="" id="u4809_div">
		            </div>
		            <div class="text" id="u4809_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phòng ban - Vai trò
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4810">
		         <div class="ax_default paragraph" id="u4811">
		            <div class="" id="u4811_div">
		            </div>
		            <div class="text" id="u4811_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trạng thái học viên
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u4812">
		            <img class="img" id="u4812_img" src="images/account_information/u112.svg"/>
		            <div class="text" id="u4812_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4813">
		         <div class="ax_default paragraph" id="u4814">
		            <div class="" id="u4814_div">
		            </div>
		            <div class="text" id="u4814_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tiêu chí đánh giá
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u4815">
		            <img class="img" id="u4815_img" src="images/account_information/u115.svg"/>
		            <div class="text" id="u4815_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4816">
		         <div class="ax_default paragraph" id="u4817">
		            <div class="" id="u4817_div">
		            </div>
		            <div class="text" id="u4817_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Chương trình học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4818">
		            <div class="ax_default icon" id="u4819">
		               <img class="img" id="u4819_img" src="images/account_information/u119.svg"/>
		               <div class="text" id="u4819_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4820">
		         <div class="ax_default icon" id="u4821">
		            <img class="img" id="u4821_img" src="images/account_information/u121.svg"/>
		            <div class="text" id="u4821_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u4822">
		            <div class="" id="u4822_div">
		            </div>
		            <div class="text" id="u4822_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Bài học
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4823">
		         <div class="ax_default paragraph" id="u4824">
		            <div class="" id="u4824_div">
		            </div>
		            <div class="text" id="u4824_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phòng học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u4825">
		            <img class="img" id="u4825_img" src="images/account_information/u125.svg"/>
		            <div class="text" id="u4825_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4826">
		         <div class="ax_default paragraph" id="u4827">
		            <div class="" id="u4827_div">
		            </div>
		            <div class="text" id="u4827_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trường học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u4828">
		            <img class="img" id="u4828_img" src="images/account_information/u128.svg"/>
		            <div class="text" id="u4828_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default line" id="u4829">
		         <img class="img" id="u4829_img" src="images/account_information/u129.svg"/>
		         <div class="text" id="u4829_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u4830">
		         <div class="" id="u4830_div">
		         </div>
		         <div class="text" id="u4830_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Tài chính
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4831">
		         <div class="ax_default paragraph" id="u4832">
		            <div class="" id="u4832_div">
		            </div>
		            <div class="text" id="u4832_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Học phí
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u4833">
		            <img class="img" id="u4833_img" src="images/account_information/u133.svg"/>
		            <div class="text" id="u4833_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4834">
		         <div class="ax_default paragraph" id="u4835">
		            <div class="" id="u4835_div">
		            </div>
		            <div class="text" id="u4835_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Khuyến mại
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u4836">
		            <img class="img" id="u4836_img" src="images/account_information/u136.svg"/>
		            <div class="text" id="u4836_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="classMenu" data-left="11" data-top="252" data-width="143" id="u4837">
		      <div class="ax_default shape" data-label="classIconLable" id="u4838">
		         <img class="img" id="u4838_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u4838_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Giáo viên
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG" id="u4839">
		         <div class="" id="u4839_div">
		         </div>
		         <div class="text" id="u4839_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG2" id="u4840">
		         <div class="" id="u4840_div">
		         </div>
		         <div class="text" id="u4840_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u4841">
		         <img class="img" id="u4841_img" src="images/account_information/u141.svg"/>
		         <div class="text" id="u4841_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u4842">
		      <img class="img" id="u4842_img" src="images/account_information/u142.svg"/>
		      <div class="text" id="u4842_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u4843">
		      <img class="img" id="u4843_img" src="images/account_information/u143.svg"/>
		      <div class="text" id="u4843_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div id="u4750" style={{"display":"none","visibility":"hidden"}}>
		   </div>
		   <div class="ax_default" data-height="55" data-label="Header" data-left="133" data-top="17" data-width="516" id="u4844">
		      <div class="ax_default paragraph" id="u4845">
		         <div class="" id="u4845_div">
		         </div>
		         <div class="text" id="u4845_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Tồn kho
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u4846">
		         <div class="" id="u4846_div">
		         </div>
		         <div class="text" id="u4846_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Kiểm soát số liệu về hàng tồn kho
		               </span>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="87" data-label="Search" data-left="187" data-top="95" data-width="1209" id="u4847">
		      <div class="ax_default box_1" id="u4848">
		         <div class="" id="u4848_div">
		         </div>
		         <div class="text" id="u4848_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u4849">
		         <div class="" id="u4849_div">
		         </div>
		         <div class="text" id="u4849_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Tìm kiếm
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="64" data-left="257" data-top="109" data-width="1069" id="u4850">
		         <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4851">
		         </div>
		         <div class="ax_default" data-height="64" data-left="841" data-top="109" data-width="279" id="u4852">
		            <div class="ax_default paragraph" id="u4853">
		               <div class="" id="u4853_div">
		               </div>
		               <div class="text" id="u4853_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Khoảng thời gian
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default text_field" id="u4854">
		               <div class="" id="u4854_div">
		               </div>
		               <input class="u4854_input" id="u4854_input" type="text" value=""/>
		            </div>
		            <div class="ax_default text_field" id="u4855">
		               <div class="" id="u4855_div">
		               </div>
		               <input class="u4855_input" id="u4855_input" type="text" value=""/>
		            </div>
		         </div>
		         <div class="ax_default" data-height="15" data-left="257" data-top="134" data-width="37" id="u4856">
		            <div class="ax_default paragraph" id="u4857">
		               <div class="" id="u4857_div">
		               </div>
		               <div class="text" id="u4857_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Cơ sở
		                     </span>
		                  </p>
		               </div>
		            </div>
		         </div>
		         <div class="ax_default primary_button" id="u4858">
		            <div class="" id="u4858_div">
		            </div>
		            <div class="text" id="u4858_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tìm kiếm
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default droplist" id="u4859">
		      <div class="" id="u4859_div">
		      </div>
		      <select class="u4859_input" id="u4859_input">
		         <option class="u4859_input_option" selected="" value="Chọn 1 giá trị">
		            Chọn 1 giá trị
		         </option>
		         <option class="u4859_input_option" value="Tất cả">
		            Tất cả
		         </option>
		         <option class="u4859_input_option" value="Hoàng Ngân">
		            Hoàng Ngân
		         </option>
		         <option class="u4859_input_option" value="Văn Quán">
		            Văn Quán
		         </option>
		      </select>
		   </div>
		   <div class="ax_default" data-height="22" data-label="Paging" data-left="187" data-top="529" data-width="1209" id="u4860">
		      <div class="ax_default paragraph" id="u4861">
		         <div class="" id="u4861_div">
		         </div>
		         <div class="text" id="u4861_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Hiển thị 1 đến 4 trong tổng số 4 kết quả
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u4862">
		         <div class="" id="u4862_div">
		         </div>
		         <div class="text" id="u4862_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Đi tới trang
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default droplist" id="u4863">
		         <div class="" id="u4863_div">
		         </div>
		         <select class="u4863_input" id="u4863_input">
		            <option class="u4863_input_option" selected="" value="1">
		               1
		            </option>
		            <option class="u4863_input_option" value="2">
		               2
		            </option>
		            <option class="u4863_input_option" value="3">
		               3
		            </option>
		            <option class="u4863_input_option" value="4">
		               4
		            </option>
		            <option class="u4863_input_option" value="5">
		               5
		            </option>
		            <option class="u4863_input_option" value="6">
		               6
		            </option>
		            <option class="u4863_input_option" value="7">
		               7
		            </option>
		            <option class="u4863_input_option" value="8">
		               8
		            </option>
		            <option class="u4863_input_option" value="9">
		               9
		            </option>
		            <option class="u4863_input_option" value="10">
		               10
		            </option>
		            <option class="u4863_input_option" value="11">
		               11
		            </option>
		            <option class="u4863_input_option" value="12">
		               12
		            </option>
		            <option class="u4863_input_option" value="13">
		               13
		            </option>
		            <option class="u4863_input_option" value="14">
		               14
		            </option>
		         </select>
		      </div>
		   </div>
		   <div class="ax_default" id="u4864">
		      <div class="ax_default table_cell" id="u4865">
		         <img class="img" id="u4865_img" src="images/kho/u4408.png"/>
		         <div class="text" id="u4865_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  STT
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4866">
		         <img class="img" id="u4866_img" src="images/holiday/u896.png"/>
		         <div class="text" id="u4866_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  ID sản phẩm
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4867">
		         <img class="img" id="u4867_img" src="images/kho/u4410.png"/>
		         <div class="text" id="u4867_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Tên sản phẩm
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4868">
		         <img class="img" id="u4868_img" src="images/t_n_kho/u4868.png"/>
		         <div class="text" id="u4868_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Kho
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4869">
		         <img class="img" id="u4869_img" src="images/student_status/u1753.png"/>
		         <div class="text" id="u4869_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Số lượng nhập
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4870">
		         <img class="img" id="u4870_img" src="images/student_status/u1753.png"/>
		         <div class="text" id="u4870_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Số lượng còn lại
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4871">
		         <img class="img" id="u4871_img" src="images/t_n_kho/u4871.png"/>
		         <div class="text" id="u4871_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Thời điểm cập nhật
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4872">
		         <img class="img" id="u4872_img" src="images/t_n_kho/u4872.png"/>
		         <div class="text" id="u4872_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Thông tin chi tiết
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4873">
		         <img class="img" id="u4873_img" src="images/kho/u4414.png"/>
		         <div class="text" id="u4873_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  1
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4874">
		         <img class="img" id="u4874_img" src="images/t_n_kho/u4874.png"/>
		         <div class="text" id="u4874_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  123
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4875">
		         <img class="img" id="u4875_img" src="images/kho/u4416.png"/>
		         <div class="text" id="u4875_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Sách Tiếng Anh 1
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4876">
		         <img class="img" id="u4876_img" src="images/t_n_kho/u4876.png"/>
		         <div class="text" id="u4876_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Kho 1
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4877">
		         <img class="img" id="u4877_img" src="images/t_n_kho/u4877.png"/>
		         <div class="text" id="u4877_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  500
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4878">
		         <img class="img" id="u4878_img" src="images/t_n_kho/u4877.png"/>
		         <div class="text" id="u4878_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  250
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4879">
		         <img class="img" id="u4879_img" src="images/t_n_kho/u4879.png"/>
		         <div class="text" id="u4879_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  2021-02-23
		               </span>
		            </p>
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  15:52
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4880">
		         <img class="img" id="u4880_img" src="images/t_n_kho/u4880.png"/>
		         <div class="text" id="u4880_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4881">
		         <img class="img" id="u4881_img" src="images/kho/u4420.png"/>
		         <div class="text" id="u4881_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  2
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4882">
		         <img class="img" id="u4882_img" src="images/t_n_kho/u4882.png"/>
		         <div class="text" id="u4882_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  456
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4883">
		         <img class="img" id="u4883_img" src="images/kho/u4422.png"/>
		         <div class="text" id="u4883_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Sách Tiếng Anh 2
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4884">
		         <img class="img" id="u4884_img" src="images/t_n_kho/u4884.png"/>
		         <div class="text" id="u4884_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Kho 1
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4885">
		         <img class="img" id="u4885_img" src="images/t_n_kho/u4885.png"/>
		         <div class="text" id="u4885_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  1200
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4886">
		         <img class="img" id="u4886_img" src="images/t_n_kho/u4885.png"/>
		         <div class="text" id="u4886_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  400
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4887">
		         <img class="img" id="u4887_img" src="images/t_n_kho/u4887.png"/>
		         <div class="text" id="u4887_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  2021-02-23
		               </span>
		            </p>
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  14:36
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4888">
		         <img class="img" id="u4888_img" src="images/t_n_kho/u4888.png"/>
		         <div class="text" id="u4888_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4889">
		         <img class="img" id="u4889_img" src="images/kho/u4414.png"/>
		         <div class="text" id="u4889_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  3
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4890">
		         <img class="img" id="u4890_img" src="images/t_n_kho/u4874.png"/>
		         <div class="text" id="u4890_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  789
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4891">
		         <img class="img" id="u4891_img" src="images/kho/u4416.png"/>
		         <div class="text" id="u4891_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Sách bài tập TA1
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4892">
		         <img class="img" id="u4892_img" src="images/t_n_kho/u4876.png"/>
		         <div class="text" id="u4892_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Kho 1
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4893">
		         <img class="img" id="u4893_img" src="images/t_n_kho/u4877.png"/>
		         <div class="text" id="u4893_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  1000
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4894">
		         <img class="img" id="u4894_img" src="images/t_n_kho/u4877.png"/>
		         <div class="text" id="u4894_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  300
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4895">
		         <img class="img" id="u4895_img" src="images/t_n_kho/u4879.png"/>
		         <div class="text" id="u4895_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  2021-02-14
		               </span>
		            </p>
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  14:36
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4896">
		         <img class="img" id="u4896_img" src="images/t_n_kho/u4880.png"/>
		         <div class="text" id="u4896_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4897">
		         <img class="img" id="u4897_img" src="images/t_n_kho/u4897.png"/>
		         <div class="text" id="u4897_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  4
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4898">
		         <img class="img" id="u4898_img" src="images/t_n_kho/u4898.png"/>
		         <div class="text" id="u4898_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  890
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4899">
		         <img class="img" id="u4899_img" src="images/t_n_kho/u4899.png"/>
		         <div class="text" id="u4899_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Sách bài tập TA2
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4900">
		         <img class="img" id="u4900_img" src="images/t_n_kho/u4900.png"/>
		         <div class="text" id="u4900_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Kho 2
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4901">
		         <img class="img" id="u4901_img" src="images/t_n_kho/u4901.png"/>
		         <div class="text" id="u4901_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  500
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4902">
		         <img class="img" id="u4902_img" src="images/t_n_kho/u4901.png"/>
		         <div class="text" id="u4902_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  370
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4903">
		         <img class="img" id="u4903_img" src="images/t_n_kho/u4903.png"/>
		         <div class="text" id="u4903_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  2021-02-15
		               </span>
		            </p>
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  14:36
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4904">
		         <img class="img" id="u4904_img" src="images/t_n_kho/u4904.png"/>
		         <div class="text" id="u4904_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="15" data-left="535" data-top="134" data-width="60" id="u4905">
		      <div class="ax_default paragraph" id="u4906">
		         <div class="" id="u4906_div">
		         </div>
		         <div class="text" id="u4906_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Danh mục
		               </span>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default droplist" id="u4907">
		      <div class="" id="u4907_div">
		      </div>
		      <select class="u4907_input" id="u4907_input">
		         <option class="u4907_input_option" selected="" value="Chọn 1 giá trị">
		            Chọn 1 giá trị
		         </option>
		         <option class="u4907_input_option" value="Hoàng Ngân">
		            Hoàng Ngân
		         </option>
		         <option class="u4907_input_option" value="Văn Quán">
		            Văn Quán
		         </option>
		      </select>
		   </div>
		   <div class="ax_default primary_button" id="u4908">
		      <div class="" id="u4908_div">
		      </div>
		      <div class="text" id="u4908_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Xem
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u4909">
		      <div class="" id="u4909_div">
		      </div>
		      <div class="text" id="u4909_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Xem
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u4910">
		      <div class="" id="u4910_div">
		      </div>
		      <div class="text" id="u4910_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Xem
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u4911">
		      <div class="" id="u4911_div">
		      </div>
		      <div class="text" id="u4911_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Xem
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default heading_3" id="u4912">
		      <div class="" id="u4912_div">
		      </div>
		      <div class="text" id="u4912_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               *, Số liệu được cập nhật liên tục mỗi khi nhập hàng và xuất hàng
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="950" data-label="ChiTietTonKho" data-left="0" data-top="0" data-width="1909" id="u4913" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default shape" data-label="alpha_Layer" id="u4914">
		         <div class="" id="u4914_div">
		         </div>
		         <div class="text" id="u4914_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-label="Add account" data-left="0" data-top="0" data-width="0" id="u4915">
		         <div class="ax_default shape" id="u4916">
		            <div class="" id="u4916_div">
		            </div>
		            <div class="text" id="u4916_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u4917">
		            <div class="" id="u4917_div">
		            </div>
		            <div class="text" id="u4917_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Thông tin chi tiết
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4918">
		            <div class="ax_default paragraph" id="u4919">
		               <div class="" id="u4919_div">
		               </div>
		               <div class="text" id="u4919_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u4920">
		            <img class="img" id="u4920_img" src="images/login/u29.svg"/>
		            <div class="text" id="u4920_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4921">
		            <div class="ax_default primary_button" id="u4922">
		               <div class="" id="u4922_div">
		               </div>
		               <div class="text" id="u4922_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        IN PHIẾU
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default icon" id="u4923">
		               <img class="img" id="u4923_img" src="images/phi_u_nh_p_xu_t/u4647.svg"/>
		               <div class="text" id="u4923_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u4924">
		            <div class="" id="u4924_div">
		            </div>
		            <div class="text" id="u4924_text">
		               <p>
		                  <span style={{"text-decoration":"underline"}}>
		                     Phiếu đang xem:
		                  </span>
		                  <span style={{"text-decoration":"none"}}>
		                     123 - Sách Tiếng Anh 1/Kho 1
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default" id="u4925">
		            <div class="ax_default table_cell" id="u4926">
		               <img class="img" id="u4926_img" src="images/t_n_kho/u4926.png"/>
		               <div class="text" id="u4926_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Thứ tự lượt
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4927">
		               <img class="img" id="u4927_img" src="images/t_n_kho/u4927.png"/>
		               <div class="text" id="u4927_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Thao tác
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4928">
		               <img class="img" id="u4928_img" src="images/t_n_kho/u4928.png"/>
		               <div class="text" id="u4928_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Số lượng nhập
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4929">
		               <img class="img" id="u4929_img" src="images/t_n_kho/u4928.png"/>
		               <div class="text" id="u4929_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Số lượng xuất
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4930">
		               <img class="img" id="u4930_img" src="images/t_n_kho/u4930.png"/>
		               <div class="text" id="u4930_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Giá tiền nhập/xuất từng đơn vị
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4931">
		               <img class="img" id="u4931_img" src="images/t_n_kho/u4931.png"/>
		               <div class="text" id="u4931_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Tổng tiền nhập/xuất
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4932">
		               <img class="img" id="u4932_img" src="images/t_n_kho/u4932.png"/>
		               <div class="text" id="u4932_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Thời điểm
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4933">
		               <img class="img" id="u4933_img" src="images/phi_u_nh_p_xu_t/u4671.png"/>
		               <div class="text" id="u4933_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        1
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4934">
		               <img class="img" id="u4934_img" src="images/t_n_kho/u4934.png"/>
		               <div class="text" id="u4934_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Nhập
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4935">
		               <img class="img" id="u4935_img" src="images/account/u318.png"/>
		               <div class="text" id="u4935_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        300
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4936">
		               <img class="img" id="u4936_img" src="images/account/u318.png"/>
		               <div class="text" id="u4936_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4937">
		               <img class="img" id="u4937_img" src="images/t_n_kho/u4937.png"/>
		               <div class="text" id="u4937_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        30.000 VNĐ
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4938">
		               <img class="img" id="u4938_img" src="images/t_n_kho/u4938.png"/>
		               <div class="text" id="u4938_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        9.000.000 VNĐ
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4939">
		               <img class="img" id="u4939_img" src="images/t_n_kho/u4939.png"/>
		               <div class="text" id="u4939_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        2020-09-05
		                     </span>
		                  </p>
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        15:52
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4940">
		               <img class="img" id="u4940_img" src="images/phi_u_nh_p_xu_t/u4664.png"/>
		               <div class="text" id="u4940_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        2
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4941">
		               <img class="img" id="u4941_img" src="images/t_n_kho/u4941.png"/>
		               <div class="text" id="u4941_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Xuất
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4942">
		               <img class="img" id="u4942_img" src="images/account/u329.png"/>
		               <div class="text" id="u4942_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4943">
		               <img class="img" id="u4943_img" src="images/account/u329.png"/>
		               <div class="text" id="u4943_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        200
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4944">
		               <img class="img" id="u4944_img" src="images/t_n_kho/u4944.png"/>
		               <div class="text" id="u4944_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        50.000 VNĐ
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4945">
		               <img class="img" id="u4945_img" src="images/t_n_kho/u4945.png"/>
		               <div class="text" id="u4945_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        10.000.000 VNĐ
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4946">
		               <img class="img" id="u4946_img" src="images/t_n_kho/u4946.png"/>
		               <div class="text" id="u4946_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        2020-10-15
		                     </span>
		                  </p>
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        15:52
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4947">
		               <img class="img" id="u4947_img" src="images/phi_u_nh_p_xu_t/u4671.png"/>
		               <div class="text" id="u4947_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        3
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4948">
		               <img class="img" id="u4948_img" src="images/t_n_kho/u4934.png"/>
		               <div class="text" id="u4948_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Xuất
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4949">
		               <img class="img" id="u4949_img" src="images/account/u318.png"/>
		               <div class="text" id="u4949_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4950">
		               <img class="img" id="u4950_img" src="images/account/u318.png"/>
		               <div class="text" id="u4950_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        50
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4951">
		               <img class="img" id="u4951_img" src="images/t_n_kho/u4937.png"/>
		               <div class="text" id="u4951_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        50.000 VNĐ
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4952">
		               <img class="img" id="u4952_img" src="images/t_n_kho/u4938.png"/>
		               <div class="text" id="u4952_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        2.500.000 VNĐ
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4953">
		               <img class="img" id="u4953_img" src="images/t_n_kho/u4939.png"/>
		               <div class="text" id="u4953_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        2020-12-20
		                     </span>
		                  </p>
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        15:52
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4954">
		               <img class="img" id="u4954_img" src="images/phi_u_nh_p_xu_t/u4678.png"/>
		               <div class="text" id="u4954_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        4
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4955">
		               <img class="img" id="u4955_img" src="images/t_n_kho/u4955.png"/>
		               <div class="text" id="u4955_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Nhập
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4956">
		               <img class="img" id="u4956_img" src="images/t_n_kho/u4956.png"/>
		               <div class="text" id="u4956_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        200
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4957">
		               <img class="img" id="u4957_img" src="images/t_n_kho/u4956.png"/>
		               <div class="text" id="u4957_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4958">
		               <img class="img" id="u4958_img" src="images/t_n_kho/u4958.png"/>
		               <div class="text" id="u4958_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        30.000 VNĐ
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4959">
		               <img class="img" id="u4959_img" src="images/t_n_kho/u4959.png"/>
		               <div class="text" id="u4959_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        6.000.000 VNĐ
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4960">
		               <img class="img" id="u4960_img" src="images/t_n_kho/u4960.png"/>
		               <div class="text" id="u4960_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        2021-02-05
		                     </span>
		                  </p>
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        15:52
		                     </span>
		                  </p>
		               </div>
		            </div>
		         </div>
		         <div class="ax_default heading_3" id="u4961">
		            <div class="" id="u4961_div">
		            </div>
		            <div class="text" id="u4961_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Ghi chú:
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default heading_3" id="u4962">
		            <div class="" id="u4962_div">
		            </div>
		            <div class="text" id="u4962_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Hoàng Ngân, ngày 22 tháng 2 năm 2021
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default heading_3" id="u4963">
		            <div class="" id="u4963_div">
		            </div>
		            <div class="text" id="u4963_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Người tạo phiếu: Nguyễn Văn A
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default placeholder" id="u4964">
		            <img class="img" id="u4964_img" src="images/t_n_kho/u4964.svg"/>
		            <div class="text" id="u4964_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default placeholder" id="u4965">
		            <img class="img" id="u4965_img" src="images/t_n_kho/u4965.svg"/>
		            <div class="text" id="u4965_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default placeholder" id="u4966">
		            <img class="img" id="u4966_img" src="images/t_n_kho/u4966.svg"/>
		            <div class="text" id="u4966_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default placeholder" id="u4967">
		            <img class="img" id="u4967_img" src="images/t_n_kho/u4967.svg"/>
		            <div class="text" id="u4967_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		</div>
		
      </div>
    );
  }
}

export default Page;
