import React from "react";
import { connect } from "dva";
import DiemDanhTheoLopSearch from "./tong_hop_diem_danh_theo_lop_search";
import DiemDanhTheoLopBody from "./tong_hop_diem_danh_theo_lop_body";
import PageHeader from "../../../components/PageHeader";
import isEmpty from "lodash/isEmpty";

class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillMount() {
    const { id, centerId } = this.props.match.params;
    const { dispatch } = this.props;
    dispatch({
      type: "Global/showLoading"
    });
    Promise.all([
      dispatch({
        type: "ReportAttendance/updateoptions",
        payload: {
          class_Id: id
        }
      }),
      dispatch({
        type: "ReportAttendance/listtrungtam"
      }),
      dispatch({
        type: "ReportAttendance/listlophoc",
        payload: {
          centerId
        }
      })
    ]).then(data => {
      dispatch({
        type: "Global/hideLoading"
      });
      if (!data) {
        dispatch({
          type: "Global/hideLoading"
        });
      }
    });
  }

  render() {
    const { id, centerId } = this.props.match.params;
    const bre = {
      title: "Báo cáo điểm danh theo lớp",
      breadcrums: [
        {
          title: "Home",
          path: "/"
        },
        {
          title: "Báo cáo điểm danh theo lớp",
          path: ""
        }
      ]
    };
    const { list } = this.props.ReportAttendance;
    const empty = isEmpty(list);
    return (
      <React.Fragment>
        {!empty && (
          <>
            <PageHeader {...bre}></PageHeader>
            <div className="iq-card">
              <div className="iq-card-body">
                <DiemDanhTheoLopSearch id={id} centerId={centerId} />
                <DiemDanhTheoLopBody id={id} />
              </div>
            </div>
          </>
        )}
      </React.Fragment>
    );
  }
}

export default connect(({ ReportAttendance, Global }) => ({
  ReportAttendance,
  Global
}))(Page);
