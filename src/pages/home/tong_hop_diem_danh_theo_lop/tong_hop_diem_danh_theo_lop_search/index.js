import React from "react";
import { connect } from "dva";
import { Button } from "react-bootstrap";
import { Formik, ErrorMessage } from "formik";
import Select from "../../../../components/Form/Select";
import moment from "moment";

class Page extends React.Component {
  constructor(props) {
    super(props);
    const { id, centerId } = props;
    this.state = {
      centerId: centerId,
      classId: id
    };
  }

  componentWillMount() {}

  onChangeCenter = value => {
    const { dispatch } = this.props;
    dispatch({
      type: "Global/showLoading"
    });
    dispatch({
      type: "ReportAttendance/listlophoc",
      payload: {
        centerId: +value
      }
    }).then(res => {
      dispatch({
        type: "Global/hideLoading"
      });
      if (!res) {
        dispatch({
          type: "Global/showError"
        });
      }
    });
  };

  onChangeClass = value => {
    const { dispatch } = this.props;
    dispatch({
      type: "Global/showLoading"
    });
    dispatch({
      type: "ReportAttendance/updateoptions",
      payload: {
        class_Id: +value
      }
    }).then(res => {
      dispatch({
        type: "Global/hideLoading"
      });
      if (!res) {
        dispatch({
          type: "Global/showError"
        });
      }
    });
  };

  onDownload = () => {
    const { dispatch } = this.props;
    dispatch({
      type: "Global/showLoading"
    });
    dispatch({
      type: "ReportAttendance/download",
      payload: {
        classId: this.state.classId
      }
    }).then(res => {
      dispatch({
        type: "Global/hideLoading"
      });
      if (!res) {
        dispatch({
          type: "Global/showError"
        });
      }
    });
  };

  render() {
    const { ReportAttendance, id, centerId } = this.props;
    const selectedClass = ReportAttendance.list_lop_hoc.find(
      item => item.id == id
    );
    return (
      <div>
        <div className="row">
          <div className="col-6">
            <p>
              Lớp: <b>{selectedClass?.name}</b>
            </p>
            <p>
              Khai giảng:{" "}
              <b>
                {selectedClass?.beginDate
                  ? moment(selectedClass?.beginDate).format("DD/MM/YYYY")
                  : ""}
              </b>
            </p>
          </div>
          <div className="col-6">
            <p>
              Thời gian hoạt động: <b>{selectedClass?.learningTime}</b>
            </p>
            <p>
              Trạng thái: <b>{selectedClass?.status}</b>
            </p>
          </div>
        </div>
        <Formik
          enableReinitialize={true}
          initialValues={{
            centerId: centerId,
            classId: id
          }}
        >
          {({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
            resetForm,
            setFieldValue
            /* and other goodies */
          }) => (
            <form onSubmit={handleSubmit}>
              <div className="row align-center">
                <div className="col-4 ">
                  <div className="form-group">
                    <Select
                      id="centerId"
                      name="centerId"
                      label="Trung tâm"
                      options={this.props.ReportAttendance?.list_trung_tam}
                      value={values.centerId}
                      onChange={e => {
                        const value = e.target.value;
                        setFieldValue("centerId", value);
                        setFieldValue("classId", "");
                        this.setState({ centerId: value, classId: "" });
                        this.onChangeCenter(value);
                      }}
                      onBlur={handleBlur}
                    />
                  </div>
                </div>
                <div className="col-4 ">
                  <div className="form-group">
                    <Select
                      id="classId"
                      name="classId"
                      label="Lớp"
                      options={this.props.ReportAttendance?.list_lop_hoc}
                      value={values.classId}
                      onChange={e => {
                        const value = e.target.value;
                        setFieldValue("classId", value);
                        this.setState({ classId: value });
                        this.onChangeClass(value);
                      }}
                      onBlur={handleBlur}
                    />
                  </div>
                </div>

                <div className="col-4 text-right">
                  <Button variant="primary" onClick={() => this.onDownload()}>
                    Download
                  </Button>
                </div>
              </div>
            </form>
          )}
        </Formik>
      </div>
    );
  }
}

export default connect(({ ReportAttendance, Global }) => ({
  ReportAttendance,
  Global
}))(Page);
