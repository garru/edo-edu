export const HEADER = [
  {
    title: "No.",
    key: "id",
    class: "tb-width-50"
  },
  {
    title: "Họ đệm",
    key: "ho_dem",
    class: "tb-width-200"
  },
  {
    title: "Tên",
    key: "ten",
    class: "tb-width-100"
  },
  {
    title: "Nguồn",
    key: "nguon",
    class: "tb-width-100"
  },
  {
    title: "Điểm danh",
    key: "diem_danh",
    class: "tb-width-200"
  },
  {
    title: "Nhận xét",
    key: "nhan_xet",
    class: "tb-width-200"
  }
];

export const DIEM_DANH = [
  {
    code: "Chưa điểm danh",
    text: "Chưa điểm danh"
  },
  {
    code: "Có đi học",
    text: "Có đi học"
  },
  {
    code: "Nghỉ không phép",
    text: "Nghỉ không phép"
  },
  {
    code: "Nghỉ có phép",
    text: "Nghỉ có phép"
  }
];

export const STATUS = [
  {
    label: "Có mặt",
    value: "present"
  },
  {
    label: "Nghỉ không phép",
    value: "not_auth_absent"
  },
  {
    label: "Nghỉ có phép",
    value: "auth_absent"
  }
];

export const DANH_SACH_HOC_SINH = [
  {
    name: "Nguyễn Văn A"
  },
  {
    name: "Nguyễn Văn A"
  },
  {
    name: "Nguyễn Văn A"
  },
  {
    name: "Nguyễn Văn A"
  },
  {
    name: "Nguyễn Văn A"
  },
  {
    name: "Nguyễn Văn A"
  },
  {
    name: "Nguyễn Văn A"
  }
];

export const DATA = [
  {
    id: 1,
    ten_bai_hoc: "Introduction",
    si_so: "12/13",
    diem_danh: [
      {
        name: "Nguyễn Văn A",
        status: "co_mat",
      },
      {
        name: "Nguyễn Văn A",
        status: "co_mat"
      },
      {
        name: "Nguyễn Văn A",
        status: "co_mat"
      },
      {
        name: "Nguyễn Văn A",
        status: "co_mat"
      },
      {
        name: "Nguyễn Văn A",
        status: "co_mat"
      },
      {
        name: "Nguyễn Văn A",
        status: "co_mat"
      },
      {
        name: "Nguyễn Văn A",
        status: "co_mat"
      }
    ]
  },
  {
    id: 2,
    ten_bai_hoc: "Introduction",
    si_so: "12/13",
    diem_danh: [
      {
        name: "Nguyễn Văn A",
        status: "co_mat"
      },
      {
        name: "Nguyễn Văn A",
        status: "co_mat"
      },
      {
        name: "Nguyễn Văn A",
        status: "co_mat"
      },
      {
        name: "Nguyễn Văn A",
        status: "co_mat"
      },
      {
        name: "Nguyễn Văn A",
        status: "co_mat"
      },
      {
        name: "Nguyễn Văn A",
        status: "co_mat"
      },
      {
        name: "Nguyễn Văn A",
        status: "co_mat"
      }
    ]
  },
  {
    id: 3,
    ten_bai_hoc: "Introduction",
    si_so: "12/13",
    diem_danh: [
      {
        name: "Nguyễn Văn A",
        status: "co_mat"
      },
      {
        name: "Nguyễn Văn A",
        status: "co_mat"
      },
      {
        name: "Nguyễn Văn A",
        status: "co_mat"
      },
      {
        name: "Nguyễn Văn A",
        status: "co_mat"
      },
      {
        name: "Nguyễn Văn A",
        status: "co_mat"
      },
      {
        name: "Nguyễn Văn A",
        status: "co_mat"
      },
      {
        name: "Nguyễn Văn A",
        status: "co_mat"
      }
    ]
  },
  {
    id: 4,
    ten_bai_hoc: "Introduction",
    si_so: "12/13",
    diem_danh: [
      {
        name: "Nguyễn Văn A",
        status: "co_mat"
      },
      {
        name: "Nguyễn Văn A",
        status: "nghi_khong_bu"
      },
      {
        name: "Nguyễn Văn A",
        status: "nghi_da_bu"
      },
      {
        name: "Nguyễn Văn A",
        status: "bao_luu"
      },
      {
        name: "Nguyễn Văn A",
        status: "co_mat"
      },
      {
        name: "Nguyễn Văn A",
        status: "co_mat"
      },
      {
        name: "Nguyễn Văn A",
        status: "co_mat"
      }
    ]
  },
  {
    id: 5,
    ten_bai_hoc: "Introduction",
    si_so: "12/13",
    diem_danh: [
      {
        name: "Nguyễn Văn A",
        status: "co_mat"
      },
      {
        name: "Nguyễn Văn A",
        status: "co_mat"
      },
      {
        name: "Nguyễn Văn A",
        status: "co_mat"
      },
      {
        name: "Nguyễn Văn A",
        status: "co_mat"
      },
      {
        name: "Nguyễn Văn A",
        status: "co_mat"
      },
      {
        name: "Nguyễn Văn A",
        status: "co_mat"
      },
      {
        name: "Nguyễn Văn A",
        status: "co_mat"
      }
    ]
  }
];

export const HEADER_MODAL = [
  {
    title: "Kỹ năng - Tiêu chí",
    key: "id",
    class: "tb-width-400"
  },
  {
    title: "Đánh giá",
    key: "nhan_xet",
    class: "tb-width-200"
  }
];

export const TIEU_CHI = [
  {
    id: 1,
    tieu_chi: "Kiến thức",
    rating: 3.5
  },
  {
    id: 1,
    tieu_chi: "Ngữ pháp",
    rating: 3.5
  },
  {
    id: 1,
    tieu_chi: "Từ vựng",
    rating: 3.5
  },
  {
    id: 1,
    tieu_chi: "Kỹ năng",
    rating: 3.5
  }
];
