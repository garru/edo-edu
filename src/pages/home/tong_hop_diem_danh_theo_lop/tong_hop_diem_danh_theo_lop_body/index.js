import React from "react";
import { connect } from "dva";
import CustomTable from "../../../../components/Table";

class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id: null,
      activeIndex: null,
      showModal: false,
      isCreateNew: false,
      target: null,
      show: false,
      tootlTipindex: null
    };
  }

  updateOptions(data) {
    const { dispatch } = this.props;
    dispatch({
      type: "Global/showLoading"
    });
    dispatch({
      type: "ReportAttendance/updateoptions",
      payload: data
    }).then(res => {
      dispatch({
        type: "Global/hideLoading"
      });
      !res &&
        dispatch({
          type: "Global/showError"
        });
    });
  }

  renderStatus = code => {
    switch (code) {
      case "Reserve":
        return "#ac0000";
      case "UncompensatedLeave":
        return "#0f01b3";
      case "CompensatedLeave":
        return "#d56717";
      default:
        return "";
    }
  };

  render() {
    const { ReportAttendance } = this.props;
    const columns = [
      {
        title: "Buổi số",
        key: "id",
        class: "tb-width-150"
      },
      {
        title: "Tên bài học",
        key: "lessonName",
        class: "tb-width-200"
      }
    ];
    ReportAttendance.student_name &&
      ReportAttendance.student_name.forEach((name, idx) => {
        const column = {
          title: name,
          key: `name_${idx}`,
          class: "tb-width-200",
          overTd: true,
          render: (col, index) => {
            const item = col.studentAttendance[index];
            const color = this.renderStatus(item?.code);
            console.log(color);
            return (
              <td
                style={{
                  backgroundColor: color,
                  color: color ? "white" : ""
                }}
              >
                <span>{item?.text}</span>
              </td>
            );
          }
        };
        columns.push(column);
      });
    columns.push({
      title: "Sỹ số lớp",
      key: "studentNumber",
      class: "tb-width-200"
    });

    return (
      <div>
        <div className="row">
          <div className="col-12">
            <CustomTable
              dataSource={ReportAttendance.list}
              total={ReportAttendance.total_record}
              columns={columns}
              onChange={data => this.updateOptions(data)}
            ></CustomTable>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(({ ReportAttendance, Global }) => ({
  ReportAttendance,
  Global
}))(Page);
