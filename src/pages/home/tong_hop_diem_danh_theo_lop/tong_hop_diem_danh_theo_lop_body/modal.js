import React, { Component, useState, useRef, useEffect } from "react";
import { useForm } from "react-hook-form";

import { Modal, Button, Table } from "react-bootstrap";
import { DATA, HEADER_MODAL, STATUS, TIEU_CHI } from "./data";
import Rating from "react-rating";

export default function CustomModal(props) {
  const [show, setShow] = useState(true);
  const handleClose = () => {
    // setShow(false);
    props.onClose();
  };
  const { register, handleSubmit, errors } = useForm();
  const emailReg = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  const { isCreateNew, selectedItem } = props;
  return (
    <Modal
      size={"lg"}
      show={show}
      onHide={handleClose}
      backdrop="static"
      keyboard={false}
      id="AddNew"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title>Nhận xét học viên Bùi Văn Tích</Modal.Title>
      </Modal.Header>
      <Modal.Body style={{ padding: "30px 30px 10px" }}>
        <Table
          bordered
          hover
          // style={{ minWidth: 1500 }}
          className="table"
        >
          <thead>
            <tr>
              {HEADER_MODAL.map(item => (
                <th className={`${item.class} ${item.hasSort ? "sort" : ""} ${this.state.sortField === item.key ? "active" : ""}`} key={item.key}>
                <span>{item.title}</span>
                </th>
              ))}
            </tr>
          </thead>
          <tbody>
            {TIEU_CHI.map((item, index) => (
              <React.Fragment>
                <tr key={`account_${item.id}`}>
                  <td>{item.tieu_chi}</td>
                  <td>
                    <Rating
                      className="rating"
                      emptySymbol="far fa-star"
                      fullSymbol="fa fa-star"
                      fractions={2}
                      initialRating={item.rating}
                    />
                  </td>
                </tr>
              </React.Fragment>
            ))}
          </tbody>
        </Table>
<div className="not-found">
                <i class="fas fa-inbox"></i>
                <span>Không tìm thấy kết quả</span>
              </div>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="primary" style={{marginRight: 15}}>
          {isCreateNew ? "Tạo mới" : "Cập nhật"}
        </Button>
        <Button variant={"secondary"} onClick={handleClose}>
          Hủy
        </Button>
      </Modal.Footer>
    </Modal>
  );
}
