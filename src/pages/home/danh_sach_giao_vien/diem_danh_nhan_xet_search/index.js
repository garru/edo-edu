import React from "react";
import {connect} from "dva";
import {Formik} from "formik";
import useTeacher from "../../../../hooks/useTeacher";
import Input from "../../../../components/Form/Input";
import Select from "../../../../components/Form/Select";
import {Button} from "react-bootstrap";

const TeacherListHeader = (props) => {
  const { search } = useTeacher({ dispatch: props.dispatch });
  return (
    <Formik
      initialValues={{ fullName: "", email: "", center_Id: "" }}
      onSubmit={(values) => {
        search(values);
      }}>
      {
        ({
           values, handleSubmit, handleChange, handleBlur
         }) => (
          <form onSubmit={handleSubmit}>
            <div className="row align-end">
              <div className="col-md-3">
                <Input
                  id="fullName"
                  name="fullName"
                  label="Họ tên:"
                  placeholder="Nhập họ tên giáo viên"
                  value={values.fullName}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
              </div>
              <div className="col-md-3">
                <Input
                  id="email"
                  name="email"
                  label="Email:"
                  placeholder="Nhập email giáo viên"
                  value={values.email}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
              </div>
              <div className="col-md-3">
                <Select
                  id="center_Id"
                  name="center_Id"
                  label="Trung tâm:"
                  options={props.Center.list_center}
                  value={values.center_Id}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
              </div>
              <div className="col-md-3 text-right">
                <div className="form-group">
                  <Button
                    variant="primary"
                    type="submit"
                  >
                    Tìm kiếm
                  </Button>
                </div>
              </div>
            </div>
          </form>
        )
      }
    </Formik>
  );
}

export default connect(({ Teacher, Center }) => ({
  Teacher,
  Center
}))(TeacherListHeader);
