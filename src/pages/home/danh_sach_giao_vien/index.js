import React from "react";
import { connect } from "dva";
import DiemDanhNhanXetSearch from "./diem_danh_nhan_xet_search";
import DiemDanhNhanXetBody from "./diem_danh_nhan_xet_body";
import PageHeader from "../../../components/PageHeader";
import useTeacher from "../../../hooks/useTeacher";

class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  search = useTeacher({ dispatch: this.props.dispatch }).search;
  loadCenter = useTeacher({ dispatch: this.props.dispatch }).loadCenter;
  componentWillMount() {
    this.search();
    this.loadCenter();
  }
  render() {
    return (
      <React.Fragment>
        <PageHeader
          title="Danh sách giáo viên"
          subTitle=""
          breadcrums={[{
            title: "Home",
            path: "/"
          }, {
            title: "Danh sách giáo viên",
            path: "/danh_sach_giao_vien"
          }]}
        />
        <div className="iq-card">
          <div className="iq-card-body">
            <DiemDanhNhanXetSearch />
            <DiemDanhNhanXetBody />
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default connect(({ HealthcareService }) => ({
  HealthcareService
}))(Page);
