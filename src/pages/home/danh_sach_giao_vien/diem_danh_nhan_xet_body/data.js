export const HEADER = [
  {
    title: "No.",
    key: "id",
    class: "tb-width-50"
  },
  {
    title: "Họ đệm",
    key: "ho_dem",
    class: "tb-width-200"
  },
  {
    title: "Tên",
    key: "ten",
    class: "tb-width-200"
  },
  {
    title: "Trung tâm",
    key: "trung_tam",
    class: "tb-width-200"
  },
  {
    title: "Quốc tịch",
    key: "quoc_tich",
    class: "tb-width-200"
  },
  {
    title: "Rating hiện tại",
    key: "rating",
    class: "tb-width-200"
  },
  {
    title: "Đơn giá",
    key: "don_gia",
    class: "tb-width-200"
  }
];

export const DIEM_DANH = [
  {
    code: "Chưa điểm danh",
    text: "Chưa điểm danh"
  },
  {
    code: "Có đi học",
    text: "Có đi học"
  },
  {
    code: "Nghỉ không phép",
    text: "Nghỉ không phép"
  },
  {
    code: "Nghỉ có phép",
    text: "Nghỉ có phép"
  }
];

export const STATUS = [
  {
    label: "Có mặt",
    value: "present"
  },
  {
    label: "Nghỉ không phép",
    value: "not_auth_absent"
  },
  {
    label: "Nghỉ có phép",
    value: "auth_absent"
  }
];

export const DATA = [
  {
    id: "1",
    ten_lop: "BIBOB1-2021",
    ho_dem: "Hoàng Dung",
    ten: "Peter Lomia",
    trung_tam: "Mai Hắc Đế",
    quoc_tich: "Việt Nam",
    rating: "3.5",
    don_gia: "250.000"
  },
  {
    id: "1",
    ten_lop: "BIBOB1-2021",
    ho_dem: "Hoàng Dung",
    ten: "Peter Lomia",
    trung_tam: "Mai Hắc Đế",
    quoc_tich: "Việt Nam",
    rating: "3.5",
    don_gia: "250.000"
  },
  {
    id: "1",
    ten_lop: "BIBOB1-2021",
    ho_dem: "Hoàng Dung",
    ten: "Peter Lomia",
    trung_tam: "Mai Hắc Đế",
    quoc_tich: "Việt Nam",
    rating: "3.5",
    don_gia: "250.000"
  },
  {
    id: "1",
    ten_lop: "BIBOB1-2021",
    ho_dem: "Hoàng Dung",
    ten: "Peter Lomia",
    trung_tam: "Mai Hắc Đế",
    quoc_tich: "Việt Nam",
    rating: "3.5",
    don_gia: "250.000"
  },
  {
    id: "1",
    ten_lop: "BIBOB1-2021",
    ho_dem: "Hoàng Dung",
    ten: "Peter Lomia",
    trung_tam: "Mai Hắc Đế",
    quoc_tich: "Việt Nam",
    rating: "3.5",
    don_gia: "250.000"
  },
  {
    id: "1",
    ten_lop: "BIBOB1-2021",
    ho_dem: "Hoàng Dung",
    ten: "Peter Lomia",
    trung_tam: "Mai Hắc Đế",
    quoc_tich: "Việt Nam",
    rating: "3.5",
    don_gia: "250.000"
  },
  {
    id: "1",
    ten_lop: "BIBOB1-2021",
    ho_dem: "Hoàng Dung",
    ten: "Peter Lomia",
    trung_tam: "Mai Hắc Đế",
    quoc_tich: "Việt Nam",
    rating: "3.5",
    don_gia: "250.000"
  },
  {
    id: "1",
    ten_lop: "BIBOB1-2021",
    ho_dem: "Hoàng Dung",
    ten: "Peter Lomia",
    trung_tam: "Mai Hắc Đế",
    quoc_tich: "Việt Nam",
    rating: "3.5",
    don_gia: "250.000"
  }
];

export const HEADER_MODAL = [
  {
    title: "Kỹ năng - Tiêu chí",
    key: "id",
    class: "tb-width-400"
  },
  {
    title: "Đánh giá",
    key: "nhan_xet",
    class: "tb-width-200"
  }
];

export const TIEU_CHI = [
  {
    id: 1,
    tieu_chi: "Kiến thức",
    rating: 3.5
  },
  {
    id: 1,
    tieu_chi: "Ngữ pháp",
    rating: 3.5
  },
  {
    id: 1,
    tieu_chi: "Từ vựng",
    rating: 3.5
  },
  {
    id: 1,
    tieu_chi: "Kỹ năng",
    rating: 3.5
  }
];
