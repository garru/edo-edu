import React from "react";
import {connect} from "dva";
import {Overlay, Popover} from "react-bootstrap";
import {Link} from "react-router-dom";
import CustomModal from "./modal";
import CustomTable from "../../../../components/Table";

class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id: null,
      tooltipIndex: null,
      show: false,
      target: null,
      activeIndex: null
    };
  }

  columns = [{
    title: "No.",
    key: "id",
    class: "tb-width-50"
  },
    {
      title: "Họ đệm",
      key: "ho_dem",
      class: "tb-width-200"
    },
    {
      title: "Tên",
      key: "ten",
      class: "tb-width-200"
    },
    {
      title: "Trung tâm",
      key: "trung_tam",
      class: "tb-width-200"
    },
    {
      title: "Quốc tịch",
      key: "quoc_tich",
      class: "tb-width-200"
    },
    {
      title: "Rating hiện tại",
      key: "rating",
      class: "tb-width-200"
    },
    {
      title: "Đơn giá",
      key: "don_gia",
      class: "tb-width-200"
    },
    {
      title: "",
      key: "action",
      class: "tb-width-50",
      render: (col, index) => (
        <i className="fa fa-ellipsis-h" onClick={e => {
          e.preventDefault();
          this.handleClick(e, index);
          this.setState({ id: col.id, tooltipIndex: index})
        }}/>
      )
    }];

  renderTooltip(index) {
    return (
      <Overlay
        show={this.state.show[index]}
        target={this.state.target}
        placement="bottom"
        containerPadding={20}
        rootClose={true}
        rootCloseEvent={"click"}
        onHide={e => this.handleClick(e, index)}
      >
        <Popover id="popover-contained">
          <Popover.Content>
            <ul>
              <li>
                <Link className="not-link" to="/chi_tiet_giao_vien_tong_quan">
                  Đánh giá
                </Link>
              </li>
              <li>
                <Link
                  className="not-link"
                  to="/chi_tiet_giao_vien_rate_giao_vien"
                >
                  Đơn giá
                </Link>
              </li>
            </ul>
          </Popover.Content>
        </Popover>
      </Overlay>
    );
  }

  handleClick(e, index, isHide) {
    this.setState({
      show: !(index === this.state.activeIndex),
      target: isHide ? null : e.target,
      activeIndex: index === this.state.activeIndex ? null : index
    });
  }

  onClose = () => {
    this.setState({ isCreateNew: false, showModal: false });
  };

  render() {
    return (
      <div className="m-t-1">
        <div className="row align-end">
          <div className="col-12">
            <CustomTable
              columns={this.columns}
              dataSource={this.props.Teacher.list_giao_vien}
              total={this.props.Teacher.total_record}
              onChange={data => this.updateOptions(data)}
            />
          </div>
        </div>
        {this.state.showModal && (
          <CustomModal
            isCreateNew={this.state.isCreateNew}
            onClose={this.onClose}
            selectedItem={this.state.selectedItem}
          />
        )}
        {this.renderTooltip()}
      </div>
    );
  }
}

export default connect(({ Teacher }) => ({
  Teacher
}))(Page);
