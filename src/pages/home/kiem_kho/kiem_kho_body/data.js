export const HEADER = [
  {
    title: "STT",
    key: "id",
    class: "tb-width-50"
  },
  {
    title: "ID sản phẩm",
    key: "ma_san_pham",
    class: "tb-width-150"
  },
  {
    title: "Tên sản phẩm",
    key: "ten_san_pham",
    class: "tb-width-300"
  },
  {
    title: "Kho",
    key: "kho",
    class: "tb-width-200"
  },
  {
    title: "Số lượng nhập",
    key: "so_luong_nhap",
    class: "tb-width-200"
  },
  {
    title: "Giá nhập",
    key: "gia_nhap",
    class: "tb-width-200"
  },
  {
    title: "Tổng tiền đã nhập",
    key: "tong_tien_da_nhap",
    class: "tb-width-200"
  },
  {
    title: "Số lượng xuất",
    key: "so_luong_xuat",
    class: "tb-width-200"
  },
  {
    title: "Giá xuất",
    key: "gia_xuat",
    class: "tb-width-200"
  },
  {
    title: "Tổng tiền đã xuất",
    key: "tong_tien_da_xuat",
    class: "tb-width-200"
  },
  {
    title: "Thời điểm cập nhật",
    key: "thoi_diem_cap_nhat",
    class: "tb-width-200"
  }
];

export const DATA = [
  {
    id: "1",
    ma_san_pham: "123",
    ten_san_pham: "Sách Tiếng Anh 1",
    kho: "Kho 1",
    so_luong_nhap: "500",
    gia_nhap: "30.000 VNĐ",
    tong_tien_da_nhap: "15.000.000 VNĐ",
    so_luong_xuat: "250",
    gia_xuat: "50.000 VNĐ",
    tong_tien_da_xuat: "12.500.000 VNĐ",
    thoi_diem_cap_nhat: "2021-02-23 15:52"
  },
  {
    id: "2",
    ma_san_pham: "123",
    ten_san_pham: "Sách Tiếng Anh 1",
    kho: "Kho 2",
    so_luong_nhap: "500",
    gia_nhap: "30.000 VNĐ",
    tong_tien_da_nhap: "15.000.000 VNĐ",
    so_luong_xuat: "250",
    gia_xuat: "50.000 VNĐ",
    tong_tien_da_xuat: "12.500.000 VNĐ",
    thoi_diem_cap_nhat: "2021-02-23 15:52"
  },
  {
    id: "3",
    ma_san_pham: "123",
    ten_san_pham: "Sách Tiếng Anh 1",
    kho: "Kho 3",
    so_luong_nhap: "500",
    gia_nhap: "30.000 VNĐ",
    tong_tien_da_nhap: "15.000.000 VNĐ",
    so_luong_xuat: "250",
    gia_xuat: "50.000 VNĐ",
    tong_tien_da_xuat: "12.500.000 VNĐ",
    thoi_diem_cap_nhat: "2021-02-23 15:52"
  }
];
