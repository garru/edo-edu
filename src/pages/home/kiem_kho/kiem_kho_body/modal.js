import React, { Component, useState, useRef, useEffect } from "react";
import { useForm } from "react-hook-form";

import { Modal, Button } from "react-bootstrap";
import moment from "moment";
import {
  NOI_SINH,
  NGUYEN_QUAN,
  QUOC_TICH,
  HON_NHAN,
  TRUNG_TAM,
  PHONG_BAN,
  VI_TRI,
  HOP_DONG
} from "../../../../mock/dropdown";

export default function CustomModal(props) {
  const [show, setShow] = useState(true);
  const handleClose = () => {
    // setShow(false);
    props.onClose();
  };
  const { register, handleSubmit, errors } = useForm();
  const emailReg = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  const { isCreateNew, selectedItem } = props;
  return (
    <Modal
      size={"lg"}
      show={show}
      onHide={handleClose}
      backdrop="static"
      keyboard={false}
      id="AddNew"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title>
          {isCreateNew ? "Thêm mới tài khoản" : "Chỉnh sửa tài khoản"}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div>
          <form>
            <div className="iq-card ">
              <div className=" iq-card-header d-flex justify-content-between">
                <div className="iq-header-title">
                  <h6>
                    <b>Thông tin cơ bản</b>
                  </h6>
                </div>
              </div>
              <div className="iq-card-body">
                <div className="row m-b-2">
                  <div className="col-12">
                    <div className="row font-size-14">
                      <div className="col-md-6 ">
                        <div className="form-group">
                          <label htmlFor="firstName">Họ và đệm:</label>
                          <input
                            type="text"
                            name="firstName"
                            className="form-control form-control-lg"
                            id="firstName"
                            defaultValue={
                              !isCreateNew ? selectedItem.ho_va_dem : ""
                            }
                            // ref={register({ required: true, minLength: 8 })}
                          ></input>
                        </div>
                        <div className="form-group">
                          <label htmlFor="lastName">Tên:</label>
                          <input
                            type="text"
                            name="lastName"
                            className="form-control form-control-lg"
                            id="lastName"
                            defaultValue={!isCreateNew ? selectedItem.ten : ""}
                            // ref={register({ required: true, minLength: 8 })}
                          ></input>
                        </div>
                        <div className="form-group">
                          <label htmlFor="email">Email:</label>
                          <input
                            type="email"
                            name="email"
                            className="form-control form-control-lg"
                            id="email"
                            defaultValue={
                              !isCreateNew ? selectedItem.email : ""
                            }
                            // ref={register({ required: true, minLength: 8 })}
                          ></input>
                        </div>
                        <div className="form-group">
                          <label htmlFor="phone">Số điện thoại:</label>
                          <input
                            type="text"
                            name="phone"
                            className="form-control form-control-lg"
                            id="phone"
                            defaultValue={
                              !isCreateNew ? selectedItem.so_dien_thoai : ""
                            }
                            // ref={register({ required: true, minLength: 8 })}
                          ></input>
                        </div>
                        <div className="form-group">
                          <div className="p-0">
                            <label>Giới tính:</label>
                          </div>
                          <div className="row">
                            <div className="col-md-6">
                              <div className="custom-control custom-radio">
                                <input
                                  className="custom-control-input"
                                  id="nam"
                                  name="gender"
                                  type="radio"
                                  value="FEMALE"
                                  defaultChecked={
                                    !isCreateNew
                                      ? selectedItem.gioi_tinh === 1
                                      : true
                                  }
                                />
                                <label
                                  className="custom-control-label"
                                  htmlFor="nam"
                                >
                                  Nam
                                </label>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="custom-control custom-radio">
                                <input
                                  className="custom-control-input"
                                  id="nu"
                                  name="gender"
                                  type="radio"
                                  value="MALE"
                                  defaultChecked={
                                    !isCreateNew
                                      ? selectedItem.gioi_tinh === 2
                                      : true
                                  }
                                />
                                <label
                                  className="custom-control-label"
                                  htmlFor="nu"
                                >
                                  Nữ
                                </label>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="form-group">
                          <label htmlFor="hon_nhan">Tình trạng hôn nhân:</label>
                          <select
                            className="form-control form-control-lg"
                            id="hon_nhan"
                            name="hon_nhan"
                            defaultValue={
                              !isCreateNew
                                ? selectedItem.tinh_trang_hon_nhan
                                : ""
                            }
                          >
                            {HON_NHAN.map(item => (
                              <option
                    value={item.code}
                    disabled={item.code !== "" ? false : true}
                    selected={item.code !== "" ? false : true}
                    hidden={item.code !== "" ? false : true}
                  >
                    {item.text}
                  </option>
                            ))}
                          </select>
                        </div>
                      </div>

                      <div className="col-md-6 ">
                        <div className="form-group">
                          <label>Ngày sinh:</label>
                          <input
                            type="date"
                            className="form-control form-control-lg"
                            id="exampleInputdate"
                            defaultValue={
                              !isCreateNew
                                ? moment(selectedItem.ngay_sinh).format(
                                    "YYYY-MM-DD"
                                  )
                                : moment().format("YYYY-MM-DD")
                            }
                            onChange={date => {
                              let value = moment(date)
                                .format("YYYY-MM-DD")
                                .toString();
                              try {
                                this.setState({
                                  dateOfBirth: moment(value).format(
                                    "YYYY-MM-DD"
                                  )
                                });
                              } catch (e) {}
                            }}
                          />
                        </div>
                        <div className="form-group">
                          <label htmlFor="noi_sinh">Nơi sinh:</label>
                          <select
                            className="form-control form-control-lg"
                            id="noi_sinh"
                            name="noi_sinh"
                            defaultValue={
                              !isCreateNew ? selectedItem.noi_sinh : ""
                            }
                            // onChange={val =>
                            //   this.setState({
                            //     txt_nguoi_lien_he: val.target.value.trimLeft()
                            //   })
                            // }
                          >
                            {NOI_SINH.map(item => (
                              <option
                    value={item.code}
                    disabled={item.code !== "" ? false : true}
                    selected={item.code !== "" ? false : true}
                    hidden={item.code !== "" ? false : true}
                  >
                    {item.text}
                  </option>
                            ))}
                          </select>
                        </div>
                        <div className="form-group">
                          <label htmlFor="nguyen_quan">Nguyên quán:</label>
                          <select
                            className="form-control form-control-lg"
                            id="nguyen_quan"
                            name="nguyen_quan"
                            defaultValue={
                              !isCreateNew ? selectedItem.nguyen_quan : ""
                            }
                            // onChange={val =>
                            //   this.setState({
                            //     txt_nguoi_lien_he: val.target.value.trimLeft()
                            //   })
                            // }
                          >
                            {NGUYEN_QUAN.map(item => (
                              <option
                    value={item.code}
                    disabled={item.code !== "" ? false : true}
                    selected={item.code !== "" ? false : true}
                    hidden={item.code !== "" ? false : true}
                  >
                    {item.text}
                  </option>
                            ))}
                          </select>
                        </div>
                        <div className="form-group">
                          <label htmlFor="quoc_tich">Quốc tịch:</label>
                          <select
                            className="form-control form-control-lg"
                            id="quoc_tich"
                            name="quoc_tich"
                            defaultValue={
                              !isCreateNew ? selectedItem.quoc_tich : ""
                            }
                            // onChange={val =>
                            //   this.setState({
                            //     txt_nguoi_lien_he: val.target.value.trimLeft()
                            //   })
                            // }
                          >
                            {QUOC_TICH.map(item => (
                              <option
                    value={item.code}
                    disabled={item.code !== "" ? false : true}
                    selected={item.code !== "" ? false : true}
                    hidden={item.code !== "" ? false : true}
                  >
                    {item.text}
                  </option>
                            ))}
                          </select>
                        </div>
                        <div className="form-group">
                          <label htmlFor="dia_chi_thuong_tru">
                            Địa chỉ thường trú:
                          </label>
                          <textarea
                            className="textarea form-control"
                            rows="4"
                            name="dia_chi_thuong_tru"
                            defaultValue={
                              !isCreateNew
                                ? selectedItem.dia_chi_thuong_tru
                                : ""
                            }
                          ></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
        <div>
          <form>
            <div className="iq-card ">
              <div className="d-flex iq-card-body pt-0">
                <div className="row">
                  <div className="col-12">
                    <div className=" iq-card-header d-flex justify-content-between">
                      <div className="iq-header-title">
                        <h6>
                          <b>Thông tin làm việc</b>
                        </h6>
                      </div>
                    </div>
                    <div className="row font-size-14">
                      <div className="col-md-6 ">
                        <div className="form-group">
                          <label htmlFor="trung_tam">Trung tâm:</label>
                          <select
                            className="form-control form-control-lg"
                            id="trung_tam"
                            name="trung_tam"
                            // onChange={val =>
                            //   this.setState({
                            //     txt_nguoi_lien_he: val.target.value.trimLeft()
                            //   })
                            // }
                          >
                            {TRUNG_TAM.map(item => (
                              <option
                    value={item.code}
                    disabled={item.code !== "" ? false : true}
                    selected={item.code !== "" ? false : true}
                    hidden={item.code !== "" ? false : true}
                  >
                    {item.text}
                  </option>
                            ))}
                          </select>
                        </div>
                      </div>
                      <div className="col-md-6 ">
                        <div className="form-group">
                          <label htmlFor="vỉ_ti">Vị trí:</label>
                          <select
                            className="form-control form-control-lg"
                            id="vỉ_ti"
                            name="vỉ_ti"
                            // onChange={val =>
                            //   this.setState({
                            //     txt_nguoi_lien_he: val.target.value.trimLeft()
                            //   })
                            // }
                          >
                            {VI_TRI.map(item => (
                              <option
                    value={item.code}
                    disabled={item.code !== "" ? false : true}
                    selected={item.code !== "" ? false : true}
                    hidden={item.code !== "" ? false : true}
                  >
                    {item.text}
                  </option>
                            ))}
                          </select>
                        </div>
                      </div>
                      <div className="col-md-6 ">
                        <div className="form-group">
                          <label htmlFor="phong_ban">Trung tâm:</label>
                          <select
                            className="form-control form-control-lg"
                            id="phong_ban"
                            name="phong_ban"
                            // onChange={val =>
                            //   this.setState({
                            //     txt_nguoi_lien_he: val.target.value.trimLeft()
                            //   })
                            // }
                          >
                            {PHONG_BAN.map(item => (
                              <option
                    value={item.code}
                    disabled={item.code !== "" ? false : true}
                    selected={item.code !== "" ? false : true}
                    hidden={item.code !== "" ? false : true}
                  >
                    {item.text}
                  </option>
                            ))}
                          </select>
                        </div>
                      </div>
                      <div className="col-md-6 ">
                        <div className="form-group">
                          <label htmlFor="hop_dong">Hợp đồng:</label>
                          <select
                            className="form-control form-control-lg"
                            id="hop_dong"
                            name="hop_dong"
                            // onChange={val =>
                            //   this.setState({
                            //     txt_nguoi_lien_he: val.target.value.trimLeft()
                            //   })
                            // }
                          >
                            {HOP_DONG.map(item => (
                              <option
                    value={item.code}
                    disabled={item.code !== "" ? false : true}
                    selected={item.code !== "" ? false : true}
                    hidden={item.code !== "" ? false : true}
                  >
                    {item.text}
                  </option>
                            ))}
                          </select>
                        </div>
                      </div>
                      <div className="col-md-6 ">
                        <div className="form-group">
                          <label htmlFor="bat_dau_lam">Bắt đầu làm:</label>
                          <select
                            className="form-control form-control-lg"
                            id="bat_dau_lam"
                            name="bat_dau_lam"
                            // onChange={val =>
                            //   this.setState({
                            //     txt_nguoi_lien_he: val.target.value.trimLeft()
                            //   })
                            // }
                          >
                            {TRUNG_TAM.map(item => (
                              <option
                    value={item.code}
                    disabled={item.code !== "" ? false : true}
                    selected={item.code !== "" ? false : true}
                    hidden={item.code !== "" ? false : true}
                  >
                    {item.text}
                  </option>
                            ))}
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="primary" style={{marginRight: 15}}>
          {isCreateNew ? "Tạo mới" : "Cập nhật"}
        </Button>
        <Button variant={"secondary"} onClick={handleClose}>
          Hủy
        </Button>
      </Modal.Footer>
    </Modal>
  );
}
