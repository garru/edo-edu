import React from "react";
import { connect } from "dva";
import { Button, Table, Overlay, Popover } from "react-bootstrap";
import { DATA, HEADER } from "./data";
import CustomModal from "./modal";
import * as _ from "lodash";

class Page extends React.Component {
  constructor(props) {
    super(props);
    const initCheckedItem = DATA.map(item => false);
    this.state = {
      show: initCheckedItem,
      target: false,
      listItem: DATA,
      activeIndex: null
    };
  }

  renderTooltip(index) {
    return (
      <Overlay
        show={this.state.show[index]}
        target={this.state.target}
        placement="bottom"
        containerPadding={20}
        rootClose={true}
        rootCloseEvent={"click"}
        onHide={e => this.handleClick(e, index)}
      >
        <Popover id="popover-contained">
          <Popover.Content>
            <ul>
              <li
                onClick={e => {
                  this.handleClick(e, index, true);
                  this.setState({ isCreateNew: false, showModal: true });
                }}
              >
                <i
                  className="fa fa-edit"
                  style={{
                    marginRight: 0,
                    marginLeft: "auto"
                  }}
                />
                &nbsp; Chỉnh sửa
              </li>

              <li
                onClick={e => {
                  this.handleClick(e, index, true);
                }}
              >
                <i
                  className="fa fa-trash-alt"
                  style={{
                    marginRight: 0,
                    marginLeft: "auto"
                  }}
                />
                &nbsp; Xoá
              </li>
            </ul>
          </Popover.Content>
        </Popover>
      </Overlay>
    );
  }

  handleClick(e, index, isHide) {
    let { show, activeIndex, listItem } = this.state;
    show[index] = !show[index];
    index !== activeIndex && (show[activeIndex] = false);
    this.setState({
      show: show,
      target: isHide ? null : e.target,
      activeIndex: index,
      selectedItem: listItem[index]
    });
  }
  onClose = () => {
    this.setState({ isCreateNew: false, showModal: false });
  };
  render() {
    return (
      <div className="row">
        <div className="col-12 text-right m-b-1">
          <Button variant="primary" type="submit" href="/kiem_kho/them_moi_kiem_kho">
            Tạo mới
          </Button>
        </div>
        <div className="col-12">
          <div style={{ width: "100%" }}>
            <div style={{ overflowX: "auto", width: "100%" }}>
              <Table
                bordered
                hover
                // style={{ minWidth: 1500 }}
                className="table"
              >
                <thead>
                  <tr>
                    {HEADER.map(item => (
                      <th
                        className={`${item.class} ${
                          item.hasSort ? "sort" : ""
                        } ${this.state.sortField === item.key ? "active" : ""}`}
                        key={item.key}
                      >
                        <span>{item.title}</span>
                      </th>
                    ))}
                  </tr>
                </thead>
                <tbody>
                  {this.state.listItem.map((item, index) => (
                    <React.Fragment>
                      <tr key={`account_${item.id}`}>
                        <td>{item.id}</td>
                        <td>{item.ma_san_pham}</td>
                        <td>{item.ten_san_pham}</td>
                        <td>{item.kho}</td>
                        <td>{item.so_luong_nhap}</td>
                        <td>{item.gia_nhap}</td>
                        <td>{item.tong_tien_da_nhap}</td>
                        <td>{item.so_luong_xuat}</td>
                        <td>{item.gia_xuat}</td>
                        <td>{item.tong_tien_da_xuat}</td>
                        <td>{item.thoi_diem_cap_nhat}</td>
                      </tr>
                    </React.Fragment>
                  ))}
                </tbody>
              </Table>
              {this.state.listItem && this.state.listItem.length ? (
                ""
              ) : (
                <div className="not-found">
                  <i class="fas fa-inbox"></i>
                  <span>Không tìm thấy kết quả</span>
                </div>
              )}
              <p>
                <b>
                  *, Số liệu được cập nhật liên tục mỗi khi nhập hàng và xuất
                  hàng
                </b>
              </p>
              <p>Hiển thị 1 đến 1 trong tổng số 1 kết quả</p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(({ HealthcareService }) => ({
  HealthcareService
}))(Page);
