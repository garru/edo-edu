import React from "react";
import { connect } from "dva";
import KiemKhoHeader from "./kiem_kho_header";
import KiemKhoBody from "./kiem_kho_body";
import PageHeader from "../../../components/PageHeader";
class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillMount() {}

  render() {
    const bre = {
      title: "Kiểm kho",
      breadcrums: [
        {
          title: "Home",
          path: "/"
        },
        {
          title: "Kiểm kho",
          path: ""
        }
      ],
      subTitle:
        "Tất cả phiếu kiểm kho sẽ được lưu tại đây. Phiếu kiểm kho giúp kiểm soat chặt chẽ số liệu hàng hóa"
    };
    console.log("render account>>>");
    return (
      <div>
        <PageHeader {...bre}></PageHeader>
        <div className="iq-card">
          <div className="iq-card-body">
            <KiemKhoHeader />
            <KiemKhoBody />
          </div>
        </div>
      </div>
    );
  }
}

export default connect(({ HealthcareService }) => ({
  HealthcareService
}))(Page);
