import React, { Component, useState, useRef, useEffect } from "react";
import { useForm } from "react-hook-form";
import { Modal, Button } from "react-bootstrap";
import { LOAI_THU_PHI } from "../../../mock/dropdown";

export default function ModalThuPhi(props) {
  const [show, setShow] = useState(true);
  const handleClose = () => {
    // setShow(false);
    props.onClose();
  };
  const { register, handleSubmit, errors } = useForm();
  const emailReg = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  const { isCreateNew, selectedItem } = props;
  return (
    <Modal
      size={"lg"}
      show={show}
      onHide={handleClose}
      backdrop="static"
      keyboard={false}
      id="AddNew"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title>Khuyến mại</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div>
          <form>
            <div className="row">
              <div className="col-5">
                <div className="form-group">
                  <label htmlFor="loai_khuyen_mai">Chọn 1 loại thu phí:</label>
                  <select
                    className="form-control form-control-lg"
                    id="loai_khuyen_mai"
                    name="loai_khuyen_mai"
                  >
                    {LOAI_THU_PHI.map(item => (
                      <option
                        value={item.code}
                        disabled={item.code !== "" ? false : true}
                        selected={item.code !== "" ? false : true}
                        hidden={item.code !== "" ? false : true}
                      >
                        {item.text}
                      </option>
                    ))}
                  </select>
                </div>
              </div>
            </div>
          </form>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="primary" style={{ marginRight: 15 }}>
          {isCreateNew ? "Tạo mới" : "Cập nhật"}
        </Button>
        <Button variant={"secondary"} onClick={handleClose}>
          Hủy
        </Button>
      </Modal.Footer>
    </Modal>
  );
}
