import React, { Component, useState, useRef, useEffect } from "react";
import { useForm } from "react-hook-form";
import { Modal, Button } from "react-bootstrap";
import { MA_KHUYEN_MAI } from "../../../mock/dropdown";

export default function ModalKhuyenMai(props) {
  const [show, setShow] = useState(true);
  const handleClose = () => {
    // setShow(false);
    props.onClose();
  };
  const { register, handleSubmit, errors } = useForm();
  const emailReg = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  const { isCreateNew, selectedItem } = props;
  return (
    <Modal
      size={"lg"}
      show={show}
      onHide={handleClose}
      backdrop="static"
      keyboard={false}
      id="AddNew"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title>Khuyến mại</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div>
          <form>
            <div className="row">
              <div className="col-5">
                <div className="form-group">
                  <label htmlFor="loai_khuyen_mai">
                    Chọn một loại khuyến mại:
                  </label>
                  <select
                    className="form-control form-control-lg"
                    id="loai_khuyen_mai"
                    name="loai_khuyen_mai"
                  >
                    {MA_KHUYEN_MAI.map(item => (
                      <option
                        value={item.code}
                        disabled={item.code !== "" ? false : true}
                        selected={item.code !== "" ? false : true}
                        hidden={item.code !== "" ? false : true}
                      >
                        {item.text}
                      </option>
                    ))}
                  </select>
                </div>
              </div>
              <div className="col-2 text-center">Hoặc</div>
              <div className="col-5">
                <div className="form-group">
                  <label htmlFor="nhap_ma_khuyen_mai">
                    Chọn một loại khuyến mại:
                  </label>
                  <input
                    type="text"
                    name="nhap_ma_khuyen_mai"
                    className="form-control form-control-lg"
                    id="nhap_ma_khuyen_mai"
                    placeholder="Nhập mã khuyến mãi"
                    // ref={register({ required: true, minLength: 8 })}
                  ></input>
                </div>
              </div>
              <div className="col-6">
                <div className="form-group">
                  <p>
                    Tên chương trình: <b>Chương trình ABC</b>
                  </p>
                </div>
              </div>
              <div className="col-6">
                <div className="form-group">
                  <p>
                    Giá trị giảm: <b>10% Bộ sách giáo khoa</b>
                  </p>
                </div>
              </div>
            </div>
          </form>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="primary" style={{ marginRight: 15 }}>
          {isCreateNew ? "Tạo mới" : "Cập nhật"}
        </Button>
        <Button variant={"secondary"} onClick={handleClose}>
          Hủy
        </Button>
      </Modal.Footer>
    </Modal>
  );
}
