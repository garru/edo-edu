import React from "react";
import { connect } from "dva";
import moment from "moment";
import PageHeader from "../../../components/PageHeader";
import { Formik, ErrorMessage } from "formik";
import Input from "../../../components/Form/Input";
import Select from "../../../components/Form/Select";
import TextArea from "../../../components/Form/TextArea";
import ModalThuPhi from "./modal_thu_phi";
import ModalKhuyenMai from "./modal_khuyen_mai";
import * as yup from "yup";
class Page extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      id: null,
      activeIndex: null,
      showModal: false,
      isCreateNew: false,
      target: null,
      show: false,
      tootlTipindex: null,
      showThuPhi: false,
      showKhuyenMai: false,
      selectedPromo: null
    };
  }

  onShowThuPhi = () => {
    this.setState({ showThuPhi: true });
  };

  onShowKhuyenMai = () => {
    this.setState({ showKhuyenMai: true });
  };

  onCloseThuPhi = selectedSurchage => {
    this.setState({ showThuPhi: false, selectedSurchage });
  };

  onCloseKhuyenMai = selectedPromo => {
    this.setState({ showKhuyenMai: false, selectedPromo });
  };

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: "Global/showLoading"
    });
    Promise.all([
      dispatch({
        type: "Invoice/listcenter",
        payload: {}
      }),
      dispatch({
        type: "Invoice/listtype",
        payload: {}
      }),
      dispatch({
        type: "Invoice/listpromotion",
        payload: {}
      }),
      dispatch({
        type: "Invoice/listsurchage",
        payload: {}
      })
    ]).then(() => {
      dispatch({
        type: "Global/hideLoading"
      });
    });
  }

  onChangeCenter = id => {
    const { dispatch } = this.props;
    dispatch({
      type: "Global/showLoading"
    });
    Promise.all([
      dispatch({
        type: "Invoice/listclass",
        payload: {
          centerId: id
        }
      }),
      dispatch({
        type: "Invoice/listschedule",
        payload: {
          centerId: id
        }
      }),
      dispatch({
        type: "Invoice/listpayment",
        payload: {
          centerId: id
        }
      }),
      dispatch({
        type: "Invoice/listprice",
        payload: {
          centerId: id
        }
      }),
      dispatch({
        type: "Invoice/listissuer",
        payload: {
          centerId: id
        }
      })
    ]).then(
      () => {
        dispatch({
          type: "Global/hideLoading"
        });
      },
      error => {
        dispatch({
          type: "Global/hideLoading"
        });
        dispatch({
          type: "Global/showError"
        });
      }
    );
  };

  render() {
    const { Invoice } = this.props;
    const bre = {
      title: "Xuất hoá đơn",
      breadcrums: [
        {
          title: "Home",
          path: "/"
        },
        {
          title: "Xuất hoá đơn",
          path: ""
        }
      ]
    };
    return (
      <div>
        <PageHeader {...bre}></PageHeader>
        <Formik
          enableReinitialize={true}
          initialValues={{
            centerId: 1,
            invoiceType: 1,
            purpose: 1,
            referCode: "string",
            priceId: 1,
            partnerId: 1,
            partnerName: "string",
            paymentId: 1,
            description: "string",
            issueBy: 1,
            classId: 1,
            scheduleId: 1,
            issueDate: "2021-06-12",
            amount: 1,
            discountInfo: {
              quantity: 0,
              price: 0
            },
            promotionId: 0,
            surchageId: 0,
            collectAmount: 0,
            depositAmount: 0,
            invoiceAmount: 0
          }}
          validationSchema={yup.object().shape({
            centerId: yup.number().required("Trường bắt buộc"),
            invoiceType: yup.number().required("Trường bắt buộc"),
            priceId: yup.number().required("Trường bắt buộc"),
            partnerId: yup.number().required("Trường bắt buộc"),
            paymentId: yup.number().required("Trường bắt buộc"),
            issueBy: yup.number().required("Trường bắt buộc"),
            classId: yup.number().required("Trường bắt buộc"),
            scheduleId: yup.number().required("Trường bắt buộc"),
            classId: yup.number().required("Trường bắt buộc"),
            amount: yup.number().required("Trường bắt buộc"),
            collectAmount: yup.number().required("Trường bắt buộc"),
            depositAmount: yup.number().required("Trường bắt buộc"),
            invoiceAmount: yup.number().required("Trường bắt buộc")
          })}
          onSubmit={(values, { setSubmitting }) => {
            const { dispatch } = this.props;
            const submitData = {
              ...values,
              promotionId: this.selectedPromo?.id,
              surchageId: this.selectedSurchage?.id
            };
            dispatch({
              type: "Global/showLoading"
            });
            dispatch({
              type: "Invoice/insert",
              payload: { ...submitData }
            }).then(data => {
              dispatch({
                type: "Global/hideLoading"
              });
              if (data) {
                dispatch({
                  type: "Global/showSuccess"
                });
              } else {
                dispatch({
                  type: "Global/showError"
                });
              }
            });
          }}
        >
          {({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
            setFieldValue
            /* and other goodies */
          }) => (
            <form style={{ width: "100%" }} onSubmit={handleSubmit}>
              <div className="iq-card">
                <div className=" iq-card-header d-flex justify-content-between">
                  <div className="iq-header-title">
                    <h6>
                      <b>Trung tâm</b>
                    </h6>
                  </div>
                </div>
                <div className="iq-card-body">
                  <div className="row">
                    <div className="col-12 mb-2"></div>
                    <div className="col-4">
                      <div className="form-group">
                        <Select
                          id="centerId"
                          name="centerId"
                          label="Cơ sở"
                          options={Invoice?.listcenter}
                          value={values.centerId}
                          onChange={e => {
                            const value = e.target.value;
                            setFieldValue("centerId", value);
                            this.onChangeCenter(value);
                          }}
                          onBlur={handleBlur}
                        />
                      </div>
                    </div>
                    <div className="col-4">
                      <div className="form-group">
                        <Select
                          id="invoiceType"
                          name="invoiceType"
                          label="Loại hóa đơn"
                          options={Invoice?.listtype}
                          value={values.invoiceType}
                          onChange={handleChange}
                          onBlur={handleBlur}
                        />
                      </div>
                    </div>
                    <div className="col-4">
                      <div className="form-group">
                        <Select
                          id="purpose"
                          name="purpose"
                          label="Mục đích"
                          options={Invoice?.listtype}
                          value={values.purpose}
                          onChange={handleChange}
                          onBlur={handleBlur}
                        />
                      </div>
                    </div>
                    <div className="col-4">
                      <div className="form-group">
                        <Input
                          id="classId"
                          name="classId"
                          label="Mã hóa đơn liên quan:"
                          value={values.referCode}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          placeholder={"Nhập Mã hóa đơn liên quan"}
                        />
                        <div className="text-nowrap m-t-1">
                          <i>Sử dụng khi có đặt cọc</i>
                        </div>
                      </div>
                    </div>
                    <div className="col-4">
                      <div className="form-group">
                        <Select
                          id="priceId"
                          name="priceId"
                          label="Gói học phí"
                          options={Invoice?.listprice}
                          value={values.priceId}
                          onChange={handleChange}
                          onBlur={handleBlur}
                        />
                      </div>
                    </div>
                    <div className="col-4">
                      <div className="form-group">
                        <Input
                          id="partnerName"
                          name="partnerName"
                          label="Người nộp:"
                          value={values.partnerName}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          placeholder={"Nhập Người nộp"}
                        />
                      </div>
                    </div>
                    <div className="col-4">
                      <div className="form-group">
                        <Select
                          id="paymentId"
                          name="paymentId"
                          label="Tài khoản thu"
                          options={Invoice?.listpayment}
                          value={values.paymentId}
                          onChange={handleChange}
                          onBlur={handleBlur}
                        />
                      </div>
                    </div>
                    <div className="col-4">
                      <div className="form-group">
                        <Select
                          id="issueBy"
                          name="issueBy"
                          label="Người thu"
                          options={Invoice?.listissuer}
                          value={values.issueBy}
                          onChange={handleChange}
                          onBlur={handleBlur}
                        />
                      </div>
                    </div>
                    <div className="col-4">
                      <div className="form-group">
                        <Select
                          id="classId"
                          name="classId"
                          label="Lớp học"
                          options={Invoice?.listclass}
                          value={values.classId}
                          onChange={handleChange}
                          onBlur={handleBlur}
                        />
                      </div>
                    </div>
                    <div className="col-4">
                      <div className="form-group">
                        <Select
                          id="scheduleId"
                          name="scheduleId"
                          label="Buồi học"
                          options={Invoice?.listschedule}
                          value={values.scheduleId}
                          onChange={handleChange}
                          onBlur={handleBlur}
                        />
                      </div>
                    </div>
                    <div className="col-4">
                      <div className="form-group">
                        <Input
                          id="issueDate"
                          type="date"
                          name="issueDate"
                          label="Ngày tạo:"
                          value={values.issueDate}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          placeholder={"Nhập Ngày tạo"}
                          format="YYYY-MM-DD"
                          min={moment().format("YYYY-MM-DD")}
                          defaultValue={moment().format("YYYY-MM-DD")}
                        />
                      </div>
                    </div>

                    <div className="col-12">
                      <div className="form-group">
                        <TextArea
                          className="textarea form-control"
                          rows="4"
                          label="Nội dung:"
                          name="description"
                          value={values.description}
                          onChange={handleChange}
                          onBlur={handleBlur}
                        ></TextArea>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="iq-card ">
                <div className=" iq-card-header d-flex justify-content-between">
                  <div className="iq-header-title">
                    <h6>
                      <b>Loại hóa đơn</b>
                    </h6>
                  </div>
                </div>
                <div className="iq-card-body">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-12">
                      <Input
                        id="amount"
                        name="amount"
                        label="Số tiền:"
                        value={values.amount}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        placeholder={"Nhập Số tiền"}
                      />
                    </div>

                    <div className="col-lg-6 col-md-6 col-sm-12">
                      <Input
                        id="collectAmount"
                        name="collectAmount"
                        label="Số tiền cần thu:"
                        value={values.collectAmount}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        placeholder={"Nhập Số tiền cần thu"}
                      />
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-12">
                      <Input
                        id="depositAmount"
                        name="depositAmount"
                        label="Số tiền đã thu:"
                        value={values.depositAmount}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        placeholder={"Nhập Số tiền đã thu"}
                      />
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-12">
                      <Input
                        id="invoiceAmount"
                        name="invoiceAmount"
                        label="Số tiền thực thu:"
                        value={values.invoiceAmount}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        placeholder={"Nhập Số tiền thực thu"}
                      />
                    </div>
                    <div className="col-lg-6 col-md-12">
                      <div className="form-group row">
                        <label
                          htmlFor="giam_tru"
                          className="col-lg-2 col-md-4 col-sm-12"
                        >
                          Giảm trừ
                        </label>
                        <div className="col-lg-5 col-md-4 col-sm-6">
                          {this.state.selectedPromo && (
                            <p>
                              Tên chương trình:{" "}
                              <b>{this.state.selectedPromo.name}</b>
                            </p>
                          )}
                        </div>
                        <div className="col-lg-5 col-md-4 col-sm-6">
                          {this.state.selectedPromo && (
                            <p>
                              Giá trị giảm:{" "}
                              <b>
                                {this.state.selectedPromo.value}{" "}
                                {this.state.selectedPromo.valueType === 1
                                  ? "%"
                                  : "VND"}{" "}
                                {this.state.selectedPromo.productNames}{" "}
                              </b>
                            </p>
                          )}
                        </div>

                        <div className="col-sm-6"></div>
                        <div className="col-sm-3 d-flex justify-content-between">
                          <i
                            className="fas fa-gift font-24 icon-action"
                            onClick={() => this.onShowKhuyenMai()}
                          />
                          <select
                            name="loai_giam_tru"
                            id="loai_giam_tru"
                            className="w-75 form-control form-control-lg"
                            value={this.state.selectedPromo?.valueType}
                            disabled
                          >
                            <option value={1}>%</option>
                            <option value={2}>VND</option>
                          </select>
                        </div>
                        <div className="col-sm-3">
                          <input
                            type="text"
                            id="giam_tru"
                            name="giam_tru"
                            className="form-control form-control-lg"
                            value={this.state.selectedPromo?.value}
                            disabled
                          />
                        </div>
                      </div>
                      <div className="form-group row">
                        <label
                          htmlFor="giam_tru"
                          className="col-lg-2 col-md-4 col-sm-12"
                        >
                          Phụ phí
                        </label>
                        <div className="col-lg-5 col-md-4 col-sm-6">
                          {this.state.selectedSurchage && (
                            <p>
                              Tên phụ thu:{" "}
                              <b>{this.state.selectedSurchage.name}</b>
                            </p>
                          )}
                        </div>
                        <div className="col-lg-5 col-md-4 col-sm-6">
                          {this.state.selectedSurchage && (
                            <p>
                              Phí:{" "}
                              <b>
                                {this.state.selectedSurchage.value}{" "}
                                {this.state.selectedSurchage.valueType === 1
                                  ? "%"
                                  : "VND"}{" "}
                                {this.state.selectedSurchage.productNames}{" "}
                              </b>
                            </p>
                          )}
                        </div>
                        <div className="col-lg-6 col-md-6 col-sm-0"></div>
                        <div className="col-lg-3 col-md-3 col-sm-6 d-flex justify-content-between">
                          <i
                            className="fas fa-dollar-sign font-24 icon-action"
                            onClick={() => this.onShowThuPhi()}
                          />
                          <select
                            name="loai_phu_phi"
                            id="loai_giam_tru"
                            className="w-75 form-control form-control-lg"
                            value={this.state.selectedSurchage?.valueType}
                            disabled
                          >
                            <option value={1}>%</option>
                            <option value={2}>VND</option>
                          </select>
                        </div>
                        <div className="col-lg-3 col-md-3 col-sm-6">
                          <input
                            type="text"
                            id="phu_phi"
                            name="phu_phi"
                            className="form-control form-control-lg"
                            value={this.state.selectedSurchage?.value}
                            disabled
                          />
                        </div>
                      </div>
                    </div>

                    <div className="col-12 d-flex justify-content-end">
                      <button className="btn btn-primary" type="submit">
                        Tạo hoá đơn
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          )}
        </Formik>

        {this.state.showThuPhi && (
          <ModalThuPhi
            onClose={selectedSurchage => this.onCloseThuPhi(selectedSurchage)}
          ></ModalThuPhi>
        )}
        {this.state.showKhuyenMai && (
          <ModalKhuyenMai
            onClose={selectedPromo => this.onCloseKhuyenMai(selectedPromo)}
            listKhuyenMai={Invoice.listpromotion}
          ></ModalKhuyenMai>
        )}
      </div>
    );
  }
}

export default connect(({ Invoice, Global }) => ({
  Invoice,
  Global
}))(Page);
