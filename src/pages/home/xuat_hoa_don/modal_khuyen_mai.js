import React, { Component, useState, useRef, useEffect } from "react";
import { connect } from "dva";
import { Modal, Button } from "react-bootstrap";
import { Formik } from "formik";
import Input from "../../../components/Form/Input";
import Select from "../../../components/Form/Select";

function ModalKhuyenMai(props) {
  const [show, setShow] = useState(true);
  const [selectedPromo, setSelectedPromo] = useState();
  const handleClose = (data) => {
    // setShow(false);
    props.onClose(data);
  };
  let timer;
  const { isCreateNew, listKhuyenMai, dispatch } = props;

  const onChangeMaKhuyenMai = value => {
    clearTimeout(timer);

    timer = setTimeout(() => {
      dispatch({
        type: "Global/showLoading"
      });
      dispatch({
        type: "Invoice/getpromotion",
        payload: {
          code: value
        }
      }).then(data => {
        dispatch({
          type: "Global/hideLoading"
        });
        if (data) {
          setSelectedPromo(data.content);
        } else {
          dispatch({
            type: "Global/showError"
          });
        }
      });
    }, 300);
  };
  return (
    <Modal
      size={"lg"}
      show={show}
      onHide={handleClose}
      backdrop="static"
      keyboard={false}
      id="AddNew"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title>Khuyến mại</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div>
          <Formik
            enableReinitialize={true}
            initialValues={{
              promotionId: "",
              promotionCode: ""
            }}
            onSubmit={(values, { setSubmitting }) => {
              dispatch({
                type: "Global/showLoading"
              });
              dispatch({
                type: "Account/updateoptions",
                payload: values
              }).then(() => {
                dispatch({
                  type: "Global/hideLoading"
                });
              });
            }}
          >
            {({
              values,
              errors,
              touched,
              handleChange,
              handleBlur,
              handleSubmit,

              setFieldValue
              /* and other goodies */
            }) => (
              <form style={{ width: "100%" }} onSubmit={handleSubmit}>
                <div className="row">
                  <div className="col-5">
                    <div className="form-group">
                      <Select
                        id="promotionId"
                        name="promotionId"
                        label="Chọn một loại khuyến mại:"
                        options={listKhuyenMai}
                        value={values.promotionId}
                        onChange={e => {
                          const value = e.target.value;
                          setFieldValue("promotionId", value);
                          const selectedItem = listKhuyenMai.find(
                            item => item.id == value
                          );
                          setSelectedPromo(selectedItem);
                        }}
                        onBlur={handleBlur}
                      />
                    </div>
                  </div>
                  <div className="col-2 text-center">Hoặc</div>
                  <div className="col-5">
                    <div className="form-group">
                      <Input
                        id="promotionCode"
                        name="promotionCode"
                        label="Chọn một loại khuyến mại:"
                        value={values.promotionCode}
                        onChange={e => {
                          const value = e.target.value;
                          onChangeMaKhuyenMai(value);
                          setFieldValue("promotionCode", value);
                        }}
                        onBlur={handleBlur}
                        placeholder={"Nhập Chọn một loại khuyến mại"}
                      />
                    </div>
                  </div>
                  {selectedPromo && (
                    <>
                      <div className="col-6">
                        <p className="m-0">
                          Tên chương trình: <b>{selectedPromo.name}</b>
                        </p>
                      </div>
                      <div className="col-6">
                        <p className="m-0">
                          Giá trị giảm:{" "}
                          <b>
                            {selectedPromo.value}{" "}
                            {selectedPromo.valueType === 1 ? "%" : "VND"}{" "}
                            {selectedPromo.productNames}{" "}
                          </b>
                        </p>
                      </div>
                    </>
                  )}
                </div>
              </form>
            )}
          </Formik>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="primary" style={{ marginRight: 15 }} onClick={() => handleClose(selectedPromo)}>
          {isCreateNew ? "Tạo mới" : "Cập nhật"}
        </Button>
        <Button variant={"secondary"} onClick={handleClose}>
          Hủy
        </Button>
      </Modal.Footer>
    </Modal>
  );
}

export default connect(({ Invoice, Global }) => ({ Invoice, Global }))(
  ModalKhuyenMai
);
