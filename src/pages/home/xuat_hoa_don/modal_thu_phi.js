import React, { Component, useState, useRef, useEffect } from "react";
import { connect } from "dva";
import { Modal, Button } from "react-bootstrap";
import { Formik } from "formik";
import Input from "../../../components/Form/Input";
import Select from "../../../components/Form/Select";

function ModalThuPhi(props) {
  const [show, setShow] = useState(true);
  const [selectedSurchage, setSelectedSurchage] = useState();
  const handleClose = data => {
    // setShow(false);
    props.onClose(data);
  };
  const { isCreateNew, dispatch, Invoice } = props;

  return (
    <Modal
      size={"md"}
      show={show}
      onHide={handleClose}
      backdrop="static"
      keyboard={false}
      id="AddNew"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title>Phụ phí</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div>
          <Formik
            enableReinitialize={true}
            initialValues={{
              promotionId: "",
              promotionCode: ""
            }}
            onSubmit={(values, { setSubmitting }) => {
              dispatch({
                type: "Global/showLoading"
              });
              dispatch({
                type: "Account/updateoptions",
                payload: values
              }).then(() => {
                dispatch({
                  type: "Global/hideLoading"
                });
              });
            }}
          >
            {({
              values,
              errors,
              touched,
              handleChange,
              handleBlur,
              handleSubmit,

              setFieldValue
              /* and other goodies */
            }) => (
              <form style={{ width: "100%" }} onSubmit={handleSubmit}>
                <div className="row">
                  <div className="col-12">
                    <div className="form-group">
                      <Select
                        id="promotionId"
                        name="promotionId"
                        label="Chọn một loại phụ phí:"
                        options={Invoice.listsurchage}
                        value={values.promotionId}
                        onChange={e => {
                          const value = e.target.value;
                          setFieldValue("promotionId", value);
                          const selectedItem = Invoice.listsurchage.find(
                            item => item.id == value
                          );
                          setSelectedSurchage(selectedItem);
                        }}
                        onBlur={handleBlur}
                      />
                    </div>
                  </div>
                  {selectedSurchage && (
                    <>
                      <div className="col-6">
                        <p className="m-0">
                          Tên chương trình: <b>{selectedSurchage.name}</b>
                        </p>
                      </div>
                      <div className="col-6">
                        <p className="m-0">
                          Phí:{" "}
                          <b>
                            {selectedSurchage.value}{" "}
                            {selectedSurchage.valueType === 1 ? "%" : "VND"}{" "}
                            {selectedSurchage.productNames}{" "}
                          </b>
                        </p>
                      </div>
                    </>
                  )}
                </div>
              </form>
            )}
          </Formik>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button
          variant="primary"
          style={{ marginRight: 15 }}
          onClick={() => handleClose(selectedSurchage)}
        >
          {isCreateNew ? "Tạo mới" : "Cập nhật"}
        </Button>
        <Button variant={"secondary"} onClick={handleClose}>
          Hủy
        </Button>
      </Modal.Footer>
    </Modal>
  );
}

export default connect(({ Invoice, Global }) => ({ Invoice, Global }))(
  ModalThuPhi
);
