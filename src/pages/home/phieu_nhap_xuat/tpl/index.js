import React from 'react';
import './index.css';
import './ax.css';

class Page extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
		<div class="" id="base" style={{"margin-left":"-150px"}}>
		   <div class="ax_default shape" id="u4466">
		      <div class="" id="u4466_div">
		      </div>
		      <div class="text" id="u4466_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default shape" id="u4467">
		      <div class="" id="u4467_div">
		      </div>
		      <div class="text" id="u4467_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default" data-height="40" data-label="account" data-left="13" data-top="895" data-width="192" id="u4469">
		      <div class="ax_default shape" data-label="accountLable" id="u4470">
		         <img class="img" id="u4470_img" src="images/account_information/accountlable_u55.svg"/>
		         <div class="text" id="u4470_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Nguyen Van A
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u4471">
		         <img class="img" id="u4471_img" src="images/login/u4.svg"/>
		         <div class="text" id="u4471_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="settingMenu" data-left="11" data-top="826" data-width="125" id="u4472">
		      <div class="ax_default shape" data-label="gearIconLable" id="u4473">
		         <img class="img" id="u4473_img" src="images/account_information/geariconlable_u58.svg"/>
		         <div class="text" id="u4473_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Cài đặt
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="44" data-left="11" data-top="826" data-width="44" id="u4474">
		         <div class="ax_default shape" data-label="gearIconBG" id="u4475">
		            <div class="" id="u4475_div">
		            </div>
		            <div class="text" id="u4475_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default shape" data-label="gearIconBG2" id="u4476">
		            <div class="" id="u4476_div">
		            </div>
		            <div class="text" id="u4476_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" data-label="gearIcon" id="u4477">
		            <img class="img" id="u4477_img" src="images/account_information/gearicon_u62.svg"/>
		            <div class="text" id="u4477_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="customerMenu" data-left="11" data-top="145" data-width="143" id="u4478">
		      <div class="ax_default shape" data-label="customerIconLable" id="u4479">
		         <img class="img" id="u4479_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u4479_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Khách hàng
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="customerIconBG" id="u4480">
		         <div class="" id="u4480_div">
		         </div>
		         <div class="text" id="u4480_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="customerIconBG2" id="u4481">
		         <div class="" id="u4481_div">
		         </div>
		         <div class="text" id="u4481_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" data-label="customer" id="u4482">
		         <img class="img" id="u4482_img" src="images/account_information/customer_u67.svg"/>
		         <div class="text" id="u4482_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="classMenu" data-left="11" data-top="198" data-width="143" id="u4483">
		      <div class="ax_default shape" data-label="classIconLable" id="u4484">
		         <img class="img" id="u4484_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u4484_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Lớp học
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG" id="u4485">
		         <div class="" id="u4485_div">
		         </div>
		         <div class="text" id="u4485_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG2" id="u4486">
		         <div class="" id="u4486_div">
		         </div>
		         <div class="text" id="u4486_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" data-label="classicon" id="u4487">
		         <img class="img" id="u4487_img" src="images/account_information/classicon_u72.svg"/>
		         <div class="text" id="u4487_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="dashboardMenu" data-left="11" data-top="91" data-width="143" id="u4488">
		      <div class="ax_default shape" data-label="dashboardIconLable" id="u4489">
		         <img class="img" id="u4489_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u4489_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Dashnoard
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="dashboardIconBG" id="u4490">
		         <div class="" id="u4490_div">
		         </div>
		         <div class="text" id="u4490_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="dashboardIconBG2" id="u4491">
		         <div class="" id="u4491_div">
		         </div>
		         <div class="text" id="u4491_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" data-label="dashboard" id="u4492">
		         <img class="img" id="u4492_img" src="images/account_information/dashboard_u77.svg"/>
		         <div class="text" id="u4492_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="129" data-label="AccountMenu" data-left="58" data-top="806" data-width="246" id="u4493" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default shape" id="u4494">
		         <div class="" id="u4494_div">
		         </div>
		         <div class="text" id="u4494_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u4495">
		         <div class="" id="u4495_div">
		         </div>
		         <div class="text" id="u4495_text">
		            <p>
		               <span style={{"font-family":"ArialMT, Arial","font-weight":"400","text-decoration":"none"}}>
		                  Xin chào:
		               </span>
		               <span style={{"font-family":"Arial Bold, Arial","font-weight":"700","text-decoration":"none"}}>
		                  Nguyen Van A
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4496">
		         <div class="ax_default image" id="u4497">
		            <img class="img" id="u4497_img" src="images/account_information/u82.png"/>
		            <div class="text" id="u4497_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u4498">
		            <div class="" id="u4498_div">
		            </div>
		            <div class="text" id="u4498_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Đăng xuất
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default line" id="u4499">
		         <img class="img" id="u4499_img" src="images/account_information/u84.svg"/>
		         <div class="text" id="u4499_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4500">
		         <div class="ax_default paragraph" id="u4501">
		            <div class="" id="u4501_div">
		            </div>
		            <div class="text" id="u4501_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tài khoản
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default image" id="u4502">
		            <img class="img" id="u4502_img" src="images/account_information/u87.png"/>
		            <div class="text" id="u4502_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="339" data-label="SettingPannel" data-left="58" data-top="525" data-width="339" id="u4503" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default shape" id="u4504">
		         <div class="" id="u4504_div">
		         </div>
		         <div class="text" id="u4504_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u4505">
		         <div class="" id="u4505_div">
		         </div>
		         <div class="text" id="u4505_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Hệ thống
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4506">
		         <div class="ax_default icon" id="u4507">
		            <img class="img" id="u4507_img" src="images/account_information/u92.svg"/>
		            <div class="text" id="u4507_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u4508">
		            <div class="" id="u4508_div">
		            </div>
		            <div class="text" id="u4508_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tài khoản
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4509">
		         <div class="ax_default paragraph" id="u4510">
		            <div class="" id="u4510_div">
		            </div>
		            <div class="text" id="u4510_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phân quyền
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u4511">
		            <img class="img" id="u4511_img" src="images/account_information/u96.svg"/>
		            <div class="text" id="u4511_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u4512">
		         <div class="" id="u4512_div">
		         </div>
		         <div class="text" id="u4512_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Đào tạo
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4513">
		         <div class="ax_default paragraph" id="u4514">
		            <div class="" id="u4514_div">
		            </div>
		            <div class="text" id="u4514_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Thương hiệu
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u4515">
		            <img class="img" id="u4515_img" src="images/account_information/u100.svg"/>
		            <div class="text" id="u4515_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4516">
		         <div class="ax_default paragraph" id="u4517">
		            <div class="" id="u4517_div">
		            </div>
		            <div class="text" id="u4517_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trung tâm
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u4518">
		            <img class="img" id="u4518_img" src="images/account_information/u103.svg"/>
		            <div class="text" id="u4518_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4519">
		         <div class="ax_default icon" id="u4520">
		            <img class="img" id="u4520_img" src="images/account_information/u105.svg"/>
		            <div class="text" id="u4520_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u4521">
		            <div class="" id="u4521_div">
		            </div>
		            <div class="text" id="u4521_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Ngày nghỉ
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4522">
		         <div class="ax_default icon" id="u4523">
		            <img class="img" id="u4523_img" src="images/account_information/u108.svg"/>
		            <div class="text" id="u4523_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u4524">
		            <div class="" id="u4524_div">
		            </div>
		            <div class="text" id="u4524_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phòng ban - Vai trò
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4525">
		         <div class="ax_default paragraph" id="u4526">
		            <div class="" id="u4526_div">
		            </div>
		            <div class="text" id="u4526_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trạng thái học viên
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u4527">
		            <img class="img" id="u4527_img" src="images/account_information/u112.svg"/>
		            <div class="text" id="u4527_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4528">
		         <div class="ax_default paragraph" id="u4529">
		            <div class="" id="u4529_div">
		            </div>
		            <div class="text" id="u4529_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tiêu chí đánh giá
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u4530">
		            <img class="img" id="u4530_img" src="images/account_information/u115.svg"/>
		            <div class="text" id="u4530_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4531">
		         <div class="ax_default paragraph" id="u4532">
		            <div class="" id="u4532_div">
		            </div>
		            <div class="text" id="u4532_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Chương trình học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4533">
		            <div class="ax_default icon" id="u4534">
		               <img class="img" id="u4534_img" src="images/account_information/u119.svg"/>
		               <div class="text" id="u4534_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4535">
		         <div class="ax_default icon" id="u4536">
		            <img class="img" id="u4536_img" src="images/account_information/u121.svg"/>
		            <div class="text" id="u4536_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u4537">
		            <div class="" id="u4537_div">
		            </div>
		            <div class="text" id="u4537_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Bài học
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4538">
		         <div class="ax_default paragraph" id="u4539">
		            <div class="" id="u4539_div">
		            </div>
		            <div class="text" id="u4539_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phòng học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u4540">
		            <img class="img" id="u4540_img" src="images/account_information/u125.svg"/>
		            <div class="text" id="u4540_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4541">
		         <div class="ax_default paragraph" id="u4542">
		            <div class="" id="u4542_div">
		            </div>
		            <div class="text" id="u4542_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trường học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u4543">
		            <img class="img" id="u4543_img" src="images/account_information/u128.svg"/>
		            <div class="text" id="u4543_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default line" id="u4544">
		         <img class="img" id="u4544_img" src="images/account_information/u129.svg"/>
		         <div class="text" id="u4544_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u4545">
		         <div class="" id="u4545_div">
		         </div>
		         <div class="text" id="u4545_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Tài chính
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4546">
		         <div class="ax_default paragraph" id="u4547">
		            <div class="" id="u4547_div">
		            </div>
		            <div class="text" id="u4547_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Học phí
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u4548">
		            <img class="img" id="u4548_img" src="images/account_information/u133.svg"/>
		            <div class="text" id="u4548_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4549">
		         <div class="ax_default paragraph" id="u4550">
		            <div class="" id="u4550_div">
		            </div>
		            <div class="text" id="u4550_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Khuyến mại
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u4551">
		            <img class="img" id="u4551_img" src="images/account_information/u136.svg"/>
		            <div class="text" id="u4551_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="classMenu" data-left="11" data-top="252" data-width="143" id="u4552">
		      <div class="ax_default shape" data-label="classIconLable" id="u4553">
		         <img class="img" id="u4553_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u4553_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Giáo viên
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG" id="u4554">
		         <div class="" id="u4554_div">
		         </div>
		         <div class="text" id="u4554_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG2" id="u4555">
		         <div class="" id="u4555_div">
		         </div>
		         <div class="text" id="u4555_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u4556">
		         <img class="img" id="u4556_img" src="images/account_information/u141.svg"/>
		         <div class="text" id="u4556_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u4557">
		      <img class="img" id="u4557_img" src="images/account_information/u142.svg"/>
		      <div class="text" id="u4557_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u4558">
		      <img class="img" id="u4558_img" src="images/account_information/u143.svg"/>
		      <div class="text" id="u4558_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div id="u4465" style={{"display":"none","visibility":"hidden"}}>
		   </div>
		   <div class="ax_default" data-height="55" data-label="Header" data-left="133" data-top="17" data-width="516" id="u4559">
		      <div class="ax_default paragraph" id="u4560">
		         <div class="" id="u4560_div">
		         </div>
		         <div class="text" id="u4560_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Phiếu nhập xuất
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u4561">
		         <div class="" id="u4561_div">
		         </div>
		         <div class="text" id="u4561_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Dễ dàng quản lý hóa đơn, biên lai
		               </span>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="87" data-label="Search" data-left="187" data-top="95" data-width="1465" id="u4562">
		      <div class="ax_default box_1" id="u4563">
		         <div class="" id="u4563_div">
		         </div>
		         <div class="text" id="u4563_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u4564">
		         <div class="" id="u4564_div">
		         </div>
		         <div class="text" id="u4564_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Tìm kiếm
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="64" data-left="257" data-top="109" data-width="1329" id="u4565">
		         <div class="ax_default" data-height="15" data-left="841" data-top="134" data-width="24" id="u4566">
		            <div class="ax_default paragraph" id="u4567">
		               <div class="" id="u4567_div">
		               </div>
		               <div class="text" id="u4567_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Kho
		                     </span>
		                  </p>
		               </div>
		            </div>
		         </div>
		         <div class="ax_default" data-height="15" data-left="535" data-top="134" data-width="60" id="u4568">
		            <div class="ax_default paragraph" id="u4569">
		               <div class="" id="u4569_div">
		               </div>
		               <div class="text" id="u4569_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Danh mục
		                     </span>
		                  </p>
		               </div>
		            </div>
		         </div>
		         <div class="ax_default" data-height="64" data-left="1101" data-top="109" data-width="279" id="u4570">
		            <div class="ax_default paragraph" id="u4571">
		               <div class="" id="u4571_div">
		               </div>
		               <div class="text" id="u4571_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Khoảng thời gian
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default text_field" id="u4572">
		               <div class="" id="u4572_div">
		               </div>
		               <input class="u4572_input" id="u4572_input" type="text" value=""/>
		            </div>
		            <div class="ax_default text_field" id="u4573">
		               <div class="" id="u4573_div">
		               </div>
		               <input class="u4573_input" id="u4573_input" type="text" value=""/>
		            </div>
		         </div>
		         <div class="ax_default" data-height="15" data-left="257" data-top="134" data-width="37" id="u4574">
		            <div class="ax_default paragraph" id="u4575">
		               <div class="" id="u4575_div">
		               </div>
		               <div class="text" id="u4575_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Cơ sở
		                     </span>
		                  </p>
		               </div>
		            </div>
		         </div>
		         <div class="ax_default primary_button" id="u4576">
		            <div class="" id="u4576_div">
		            </div>
		            <div class="text" id="u4576_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tìm kiếm
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="40" data-label="ButtonFilter" data-left="187" data-top="211" data-width="494" id="u4577">
		      <div class="ax_default primary_button" id="u4578">
		         <div class="" id="u4578_div">
		         </div>
		         <div class="text" id="u4578_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Tạo mới phiếu nhập
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default button" id="u4579">
		         <div class="" id="u4579_div">
		         </div>
		         <div class="text" id="u4579_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Chỉnh sửa
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default primary_button" id="u4580">
		         <div class="" id="u4580_div">
		         </div>
		         <div class="text" id="u4580_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Tạo mới phiếu xuất
		               </span>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" id="u4581">
		      <div class="ax_default table_cell" id="u4582">
		         <img class="img" id="u4582_img" src="images/account/u306.png"/>
		         <div class="text" id="u4582_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4583">
		         <img class="img" id="u4583_img" src="images/phi_u_nh_p_xu_t/u4583.png"/>
		         <div class="text" id="u4583_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  ID
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4584">
		         <img class="img" id="u4584_img" src="images/phi_u_nh_p_xu_t/u4584.png"/>
		         <div class="text" id="u4584_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Số phiếu
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4585">
		         <img class="img" id="u4585_img" src="images/phi_u_nh_p_xu_t/u4585.png"/>
		         <div class="text" id="u4585_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Tên phiếu
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4586">
		         <img class="img" id="u4586_img" src="images/phi_u_nh_p_xu_t/u4586.png"/>
		         <div class="text" id="u4586_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Ngày tạo
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4587">
		         <img class="img" id="u4587_img" src="images/phi_u_nh_p_xu_t/u4587.png"/>
		         <div class="text" id="u4587_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Thông tin phiếu
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4588">
		         <img class="img" id="u4588_img" src="images/phi_u_nh_p_xu_t/u4588.png"/>
		         <div class="text" id="u4588_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4589">
		         <img class="img" id="u4589_img" src="images/phi_u_nh_p_xu_t/u4589.png"/>
		         <div class="text" id="u4589_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4590">
		         <img class="img" id="u4590_img" src="images/phi_u_nh_p_xu_t/u4590.png"/>
		         <div class="text" id="u4590_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  123
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4591">
		         <img class="img" id="u4591_img" src="images/phi_u_nh_p_xu_t/u4591.png"/>
		         <div class="text" id="u4591_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  1
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4592">
		         <img class="img" id="u4592_img" src="images/phi_u_nh_p_xu_t/u4592.png"/>
		         <div class="text" id="u4592_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Phiếu nhập kho số 1
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4593">
		         <img class="img" id="u4593_img" src="images/phi_u_nh_p_xu_t/u4593.png"/>
		         <div class="text" id="u4593_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  2021-02-05
		               </span>
		            </p>
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  15:52
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4594">
		         <img class="img" id="u4594_img" src="images/phi_u_nh_p_xu_t/u4594.png"/>
		         <div class="text" id="u4594_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4595">
		         <img class="img" id="u4595_img" src="images/phi_u_nh_p_xu_t/u4595.png"/>
		         <div class="text" id="u4595_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4596">
		         <img class="img" id="u4596_img" src="images/account/u328.png"/>
		         <div class="text" id="u4596_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4597">
		         <img class="img" id="u4597_img" src="images/phi_u_nh_p_xu_t/u4597.png"/>
		         <div class="text" id="u4597_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  456
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4598">
		         <img class="img" id="u4598_img" src="images/phi_u_nh_p_xu_t/u4598.png"/>
		         <div class="text" id="u4598_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  2
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4599">
		         <img class="img" id="u4599_img" src="images/phi_u_nh_p_xu_t/u4599.png"/>
		         <div class="text" id="u4599_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Phiếu nhập kho số 2
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4600">
		         <img class="img" id="u4600_img" src="images/phi_u_nh_p_xu_t/u4600.png"/>
		         <div class="text" id="u4600_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  2021-02-04
		               </span>
		            </p>
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  14:36
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4601">
		         <img class="img" id="u4601_img" src="images/phi_u_nh_p_xu_t/u4601.png"/>
		         <div class="text" id="u4601_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4602">
		         <img class="img" id="u4602_img" src="images/phi_u_nh_p_xu_t/u4602.png"/>
		         <div class="text" id="u4602_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4603">
		         <img class="img" id="u4603_img" src="images/account/u317.png"/>
		         <div class="text" id="u4603_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4604">
		         <img class="img" id="u4604_img" src="images/phi_u_nh_p_xu_t/u4604.png"/>
		         <div class="text" id="u4604_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  789
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4605">
		         <img class="img" id="u4605_img" src="images/phi_u_nh_p_xu_t/u4605.png"/>
		         <div class="text" id="u4605_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  3
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4606">
		         <img class="img" id="u4606_img" src="images/phi_u_nh_p_xu_t/u4606.png"/>
		         <div class="text" id="u4606_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Phiếu xuất kho số 1
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4607">
		         <img class="img" id="u4607_img" src="images/phi_u_nh_p_xu_t/u4607.png"/>
		         <div class="text" id="u4607_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  2021-02-04
		               </span>
		            </p>
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  14:36
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4608">
		         <img class="img" id="u4608_img" src="images/phi_u_nh_p_xu_t/u4608.png"/>
		         <div class="text" id="u4608_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4609">
		         <img class="img" id="u4609_img" src="images/phi_u_nh_p_xu_t/u4609.png"/>
		         <div class="text" id="u4609_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4610">
		         <img class="img" id="u4610_img" src="images/phi_u_nh_p_xu_t/u4610.png"/>
		         <div class="text" id="u4610_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4611">
		         <img class="img" id="u4611_img" src="images/phi_u_nh_p_xu_t/u4611.png"/>
		         <div class="text" id="u4611_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  246
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4612">
		         <img class="img" id="u4612_img" src="images/phi_u_nh_p_xu_t/u4612.png"/>
		         <div class="text" id="u4612_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  4
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4613">
		         <img class="img" id="u4613_img" src="images/phi_u_nh_p_xu_t/u4613.png"/>
		         <div class="text" id="u4613_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Phiếu xuất kho số 2
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4614">
		         <img class="img" id="u4614_img" src="images/phi_u_nh_p_xu_t/u4614.png"/>
		         <div class="text" id="u4614_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  2021-02-04
		               </span>
		            </p>
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  14:36
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4615">
		         <img class="img" id="u4615_img" src="images/phi_u_nh_p_xu_t/u4615.png"/>
		         <div class="text" id="u4615_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4616">
		         <img class="img" id="u4616_img" src="images/phi_u_nh_p_xu_t/u4616.png"/>
		         <div class="text" id="u4616_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default checkbox" id="u4617">
		      <label for="u4617_input" id="u4617_input_label" style={{"position":" absolute","left":" 0px"}}>
		         <img class="img" id="u4617_img" src="images/phi_u_nh_p_xu_t/u4617.svg"/>
		         <div class="text" id="u4617_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </label>
		      <input id="u4617_input" type="checkbox" value="checkbox"/>
		   </div>
		   <div class="ax_default checkbox" id="u4618">
		      <label for="u4618_input" id="u4618_input_label" style={{"position":" absolute","left":" 0px"}}>
		         <img class="img" id="u4618_img" src="images/phi_u_nh_p_xu_t/u4618.svg"/>
		         <div class="text" id="u4618_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </label>
		      <input id="u4618_input" type="checkbox" value="checkbox"/>
		   </div>
		   <div class="ax_default checkbox" id="u4619">
		      <label for="u4619_input" id="u4619_input_label" style={{"position":" absolute","left":" 0px"}}>
		         <img class="img" id="u4619_img" src="images/phi_u_nh_p_xu_t/u4619.svg"/>
		         <div class="text" id="u4619_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </label>
		      <input id="u4619_input" type="checkbox" value="checkbox"/>
		   </div>
		   <div class="ax_default checkbox" id="u4620">
		      <label for="u4620_input" id="u4620_input_label" style={{"position":" absolute","left":" 0px"}}>
		         <img class="img" id="u4620_img" src="images/phi_u_nh_p_xu_t/u4620.svg"/>
		         <div class="text" id="u4620_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </label>
		      <input id="u4620_input" type="checkbox" value="checkbox"/>
		   </div>
		   <div class="ax_default checkbox" id="u4621">
		      <label for="u4621_input" id="u4621_input_label" style={{"position":" absolute","left":" 0px"}}>
		         <img class="img" id="u4621_img" src="images/phi_u_nh_p_xu_t/u4621.svg"/>
		         <div class="text" id="u4621_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </label>
		      <input id="u4621_input" type="checkbox" value="checkbox"/>
		   </div>
		   <div class="ax_default droplist" id="u4622">
		      <div class="" id="u4622_div">
		      </div>
		      <select class="u4622_input" id="u4622_input">
		         <option class="u4622_input_option" selected="" value="Chọn 1 giá trị">
		            Chọn 1 giá trị
		         </option>
		         <option class="u4622_input_option" value="Tất cả">
		            Tất cả
		         </option>
		         <option class="u4622_input_option" value="Hoàng Ngân">
		            Hoàng Ngân
		         </option>
		         <option class="u4622_input_option" value="Văn Quán">
		            Văn Quán
		         </option>
		      </select>
		   </div>
		   <div class="ax_default droplist" id="u4623">
		      <div class="" id="u4623_div">
		      </div>
		      <select class="u4623_input" id="u4623_input">
		         <option class="u4623_input_option" selected="" value="Chọn 1 giá trị">
		            Chọn 1 giá trị
		         </option>
		         <option class="u4623_input_option" value="Tất cả">
		            Tất cả
		         </option>
		         <option class="u4623_input_option" value="Kho 1">
		            Kho 1
		         </option>
		         <option class="u4623_input_option" value="Kho 2">
		            Kho 2
		         </option>
		      </select>
		   </div>
		   <div class="ax_default droplist" id="u4624">
		      <div class="" id="u4624_div">
		      </div>
		      <select class="u4624_input" id="u4624_input">
		         <option class="u4624_input_option" selected="" value="Chọn 1 giá trị">
		            Chọn 1 giá trị
		         </option>
		         <option class="u4624_input_option" value="Dụng cụ học tập">
		            Dụng cụ học tập
		         </option>
		         <option class="u4624_input_option" value="Cơ sở vật chất">
		            Cơ sở vật chất
		         </option>
		      </select>
		   </div>
		   <div class="ax_default icon" id="u4625">
		      <img class="img" id="u4625_img" src="images/account/u429.svg"/>
		      <div class="text" id="u4625_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u4626">
		      <img class="img" id="u4626_img" src="images/account/u429.svg"/>
		      <div class="text" id="u4626_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u4627">
		      <img class="img" id="u4627_img" src="images/account/u429.svg"/>
		      <div class="text" id="u4627_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u4628">
		      <img class="img" id="u4628_img" src="images/account/u429.svg"/>
		      <div class="text" id="u4628_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u4629">
		      <div class="" id="u4629_div">
		      </div>
		      <div class="text" id="u4629_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Xem
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u4630">
		      <div class="" id="u4630_div">
		      </div>
		      <div class="text" id="u4630_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Xem
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u4631">
		      <div class="" id="u4631_div">
		      </div>
		      <div class="text" id="u4631_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Xem
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u4632">
		      <div class="" id="u4632_div">
		      </div>
		      <div class="text" id="u4632_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Xem
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default" data-height="22" data-label="Paging" data-left="182" data-top="584" data-width="1470" id="u4633">
		      <div class="ax_default paragraph" id="u4634">
		         <div class="" id="u4634_div">
		         </div>
		         <div class="text" id="u4634_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Hiển thị 1 đến 4 trong tổng số 4 kết quả
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u4635">
		         <div class="" id="u4635_div">
		         </div>
		         <div class="text" id="u4635_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Đi tới trang
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default droplist" id="u4636">
		         <div class="" id="u4636_div">
		         </div>
		         <select class="u4636_input" id="u4636_input">
		            <option class="u4636_input_option" selected="" value="1">
		               1
		            </option>
		            <option class="u4636_input_option" value="2">
		               2
		            </option>
		            <option class="u4636_input_option" value="3">
		               3
		            </option>
		            <option class="u4636_input_option" value="4">
		               4
		            </option>
		            <option class="u4636_input_option" value="5">
		               5
		            </option>
		            <option class="u4636_input_option" value="6">
		               6
		            </option>
		            <option class="u4636_input_option" value="7">
		               7
		            </option>
		            <option class="u4636_input_option" value="8">
		               8
		            </option>
		            <option class="u4636_input_option" value="9">
		               9
		            </option>
		            <option class="u4636_input_option" value="10">
		               10
		            </option>
		            <option class="u4636_input_option" value="11">
		               11
		            </option>
		            <option class="u4636_input_option" value="12">
		               12
		            </option>
		            <option class="u4636_input_option" value="13">
		               13
		            </option>
		            <option class="u4636_input_option" value="14">
		               14
		            </option>
		         </select>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="950" data-label="ChiTietNhap" data-left="11" data-top="0" data-width="1909" id="u4637" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default shape" data-label="alpha_Layer" id="u4638">
		         <div class="" id="u4638_div">
		         </div>
		         <div class="text" id="u4638_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-label="Add account" data-left="0" data-top="0" data-width="0" id="u4639">
		         <div class="ax_default shape" id="u4640">
		            <div class="" id="u4640_div">
		            </div>
		            <div class="text" id="u4640_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u4641">
		            <div class="" id="u4641_div">
		            </div>
		            <div class="text" id="u4641_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Thông tin phiếu
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4642">
		            <div class="ax_default paragraph" id="u4643">
		               <div class="" id="u4643_div">
		               </div>
		               <div class="text" id="u4643_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u4644">
		            <img class="img" id="u4644_img" src="images/login/u29.svg"/>
		            <div class="text" id="u4644_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4645">
		            <div class="ax_default primary_button" id="u4646">
		               <div class="" id="u4646_div">
		               </div>
		               <div class="text" id="u4646_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        IN PHIẾU
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default icon" id="u4647">
		               <img class="img" id="u4647_img" src="images/phi_u_nh_p_xu_t/u4647.svg"/>
		               <div class="text" id="u4647_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u4648">
		            <div class="" id="u4648_div">
		            </div>
		            <div class="text" id="u4648_text">
		               <p>
		                  <span style={{"text-decoration":"underline"}}>
		                     Tên phiếu: Phiếu nhập kho số 1
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default" id="u4649">
		            <div class="ax_default table_cell" id="u4650">
		               <img class="img" id="u4650_img" src="images/phi_u_nh_p_xu_t/u4650.png"/>
		               <div class="text" id="u4650_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Mã sản phẩm
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4651">
		               <img class="img" id="u4651_img" src="images/phi_u_nh_p_xu_t/u4651.png"/>
		               <div class="text" id="u4651_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Tên sản phẩm
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4652">
		               <img class="img" id="u4652_img" src="images/phi_u_nh_p_xu_t/u4650.png"/>
		               <div class="text" id="u4652_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Đơn vị
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4653">
		               <img class="img" id="u4653_img" src="images/phi_u_nh_p_xu_t/u4653.png"/>
		               <div class="text" id="u4653_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Số lượng
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4654">
		               <img class="img" id="u4654_img" src="images/phi_u_nh_p_xu_t/u4654.png"/>
		               <div class="text" id="u4654_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Nhà cung cấp
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4655">
		               <img class="img" id="u4655_img" src="images/phi_u_nh_p_xu_t/u4655.png"/>
		               <div class="text" id="u4655_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Giá nhập từng đơn vị
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4656">
		               <img class="img" id="u4656_img" src="images/phi_u_nh_p_xu_t/u4656.png"/>
		               <div class="text" id="u4656_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Tổng tiền nhập
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4657">
		               <img class="img" id="u4657_img" src="images/phi_u_nh_p_xu_t/u4657.png"/>
		               <div class="text" id="u4657_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        123
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4658">
		               <img class="img" id="u4658_img" src="images/phi_u_nh_p_xu_t/u4658.png"/>
		               <div class="text" id="u4658_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Bàn học sinh
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4659">
		               <img class="img" id="u4659_img" src="images/phi_u_nh_p_xu_t/u4657.png"/>
		               <div class="text" id="u4659_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Chiếc
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4660">
		               <img class="img" id="u4660_img" src="images/phi_u_nh_p_xu_t/u4660.png"/>
		               <div class="text" id="u4660_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        10
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4661">
		               <img class="img" id="u4661_img" src="images/phi_u_nh_p_xu_t/u4661.png"/>
		               <div class="text" id="u4661_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Nhà cung cấp 1
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4662">
		               <img class="img" id="u4662_img" src="images/phi_u_nh_p_xu_t/u4662.png"/>
		               <div class="text" id="u4662_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        50.000 VNĐ
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4663">
		               <img class="img" id="u4663_img" src="images/phi_u_nh_p_xu_t/u4663.png"/>
		               <div class="text" id="u4663_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        500.000 VNĐ
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4664">
		               <img class="img" id="u4664_img" src="images/phi_u_nh_p_xu_t/u4664.png"/>
		               <div class="text" id="u4664_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        456
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4665">
		               <img class="img" id="u4665_img" src="images/phi_u_nh_p_xu_t/u4665.png"/>
		               <div class="text" id="u4665_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Bút viết bảng
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4666">
		               <img class="img" id="u4666_img" src="images/phi_u_nh_p_xu_t/u4664.png"/>
		               <div class="text" id="u4666_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Hộp
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4667">
		               <img class="img" id="u4667_img" src="images/phi_u_nh_p_xu_t/u4667.png"/>
		               <div class="text" id="u4667_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        20
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4668">
		               <img class="img" id="u4668_img" src="images/phi_u_nh_p_xu_t/u4668.png"/>
		               <div class="text" id="u4668_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Nhà cung cấp 2
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4669">
		               <img class="img" id="u4669_img" src="images/phi_u_nh_p_xu_t/u4669.png"/>
		               <div class="text" id="u4669_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        20.000 VNĐ
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4670">
		               <img class="img" id="u4670_img" src="images/phi_u_nh_p_xu_t/u4670.png"/>
		               <div class="text" id="u4670_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        400.000 VNĐ
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4671">
		               <img class="img" id="u4671_img" src="images/phi_u_nh_p_xu_t/u4671.png"/>
		               <div class="text" id="u4671_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        789
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4672">
		               <img class="img" id="u4672_img" src="images/phi_u_nh_p_xu_t/u4672.png"/>
		               <div class="text" id="u4672_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Nước uống
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4673">
		               <img class="img" id="u4673_img" src="images/phi_u_nh_p_xu_t/u4671.png"/>
		               <div class="text" id="u4673_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Thùng
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4674">
		               <img class="img" id="u4674_img" src="images/phi_u_nh_p_xu_t/u4674.png"/>
		               <div class="text" id="u4674_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        10
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4675">
		               <img class="img" id="u4675_img" src="images/phi_u_nh_p_xu_t/u4675.png"/>
		               <div class="text" id="u4675_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Nhà cung cấp 2
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4676">
		               <img class="img" id="u4676_img" src="images/phi_u_nh_p_xu_t/u4676.png"/>
		               <div class="text" id="u4676_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        20.000 VNĐ
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4677">
		               <img class="img" id="u4677_img" src="images/phi_u_nh_p_xu_t/u4677.png"/>
		               <div class="text" id="u4677_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        200.000 VNĐ
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4678">
		               <img class="img" id="u4678_img" src="images/phi_u_nh_p_xu_t/u4678.png"/>
		               <div class="text" id="u4678_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        246
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4679">
		               <img class="img" id="u4679_img" src="images/phi_u_nh_p_xu_t/u4679.png"/>
		               <div class="text" id="u4679_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Ghế học sinh
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4680">
		               <img class="img" id="u4680_img" src="images/phi_u_nh_p_xu_t/u4678.png"/>
		               <div class="text" id="u4680_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Chiếc
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4681">
		               <img class="img" id="u4681_img" src="images/phi_u_nh_p_xu_t/u4681.png"/>
		               <div class="text" id="u4681_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        15
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4682">
		               <img class="img" id="u4682_img" src="images/phi_u_nh_p_xu_t/u4682.png"/>
		               <div class="text" id="u4682_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Nhà cung cấp 3
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4683">
		               <img class="img" id="u4683_img" src="images/phi_u_nh_p_xu_t/u4683.png"/>
		               <div class="text" id="u4683_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        35.000 VNĐ
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4684">
		               <img class="img" id="u4684_img" src="images/phi_u_nh_p_xu_t/u4684.png"/>
		               <div class="text" id="u4684_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        525.000 VNĐ
		                     </span>
		                  </p>
		               </div>
		            </div>
		         </div>
		         <div class="ax_default heading_3" id="u4685">
		            <div class="" id="u4685_div">
		            </div>
		            <div class="text" id="u4685_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Ghi chú:
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default heading_3" id="u4686">
		            <div class="" id="u4686_div">
		            </div>
		            <div class="text" id="u4686_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Hoàng Ngân, ngày 22 tháng 2 năm 2021
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default heading_3" id="u4687">
		            <div class="" id="u4687_div">
		            </div>
		            <div class="text" id="u4687_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Người tạo phiếu: Nguyễn Văn A
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="950" data-label="ChiTietXuat" data-left="0" data-top="0" data-width="1909" id="u4688" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default shape" data-label="alpha_Layer" id="u4689">
		         <div class="" id="u4689_div">
		         </div>
		         <div class="text" id="u4689_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-label="Add account" data-left="0" data-top="0" data-width="0" id="u4690">
		         <div class="ax_default shape" id="u4691">
		            <div class="" id="u4691_div">
		            </div>
		            <div class="text" id="u4691_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u4692">
		            <div class="" id="u4692_div">
		            </div>
		            <div class="text" id="u4692_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Thông tin phiếu
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4693">
		            <div class="ax_default paragraph" id="u4694">
		               <div class="" id="u4694_div">
		               </div>
		               <div class="text" id="u4694_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u4695">
		            <img class="img" id="u4695_img" src="images/login/u29.svg"/>
		            <div class="text" id="u4695_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4696">
		            <div class="ax_default primary_button" id="u4697">
		               <div class="" id="u4697_div">
		               </div>
		               <div class="text" id="u4697_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        IN PHIẾU
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default icon" id="u4698">
		               <img class="img" id="u4698_img" src="images/phi_u_nh_p_xu_t/u4647.svg"/>
		               <div class="text" id="u4698_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u4699">
		            <div class="" id="u4699_div">
		            </div>
		            <div class="text" id="u4699_text">
		               <p>
		                  <span style={{"text-decoration":"underline"}}>
		                     Tên phiếu: Phiếu xuất kho số 1
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default" id="u4700">
		            <div class="ax_default table_cell" id="u4701">
		               <img class="img" id="u4701_img" src="images/phi_u_nh_p_xu_t/u4650.png"/>
		               <div class="text" id="u4701_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Mã sản phẩm
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4702">
		               <img class="img" id="u4702_img" src="images/phi_u_nh_p_xu_t/u4651.png"/>
		               <div class="text" id="u4702_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Tên sản phẩm
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4703">
		               <img class="img" id="u4703_img" src="images/phi_u_nh_p_xu_t/u4703.png"/>
		               <div class="text" id="u4703_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Đơn vị
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4704">
		               <img class="img" id="u4704_img" src="images/phi_u_nh_p_xu_t/u4704.png"/>
		               <div class="text" id="u4704_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Số lượng
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4705">
		               <img class="img" id="u4705_img" src="images/phi_u_nh_p_xu_t/u4705.png"/>
		               <div class="text" id="u4705_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Nơi nhận
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4706">
		               <img class="img" id="u4706_img" src="images/phi_u_nh_p_xu_t/u4706.png"/>
		               <div class="text" id="u4706_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Giá nhập từng đơn vị
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4707">
		               <img class="img" id="u4707_img" src="images/phi_u_nh_p_xu_t/u4707.png"/>
		               <div class="text" id="u4707_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Tổng tiền nhập
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4708">
		               <img class="img" id="u4708_img" src="images/holiday/u898.png"/>
		               <div class="text" id="u4708_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Giá xuất từng đơn vị
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4709">
		               <img class="img" id="u4709_img" src="images/phi_u_nh_p_xu_t/u4709.png"/>
		               <div class="text" id="u4709_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Tổng tiền xuất
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4710">
		               <img class="img" id="u4710_img" src="images/phi_u_nh_p_xu_t/u4657.png"/>
		               <div class="text" id="u4710_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        890
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4711">
		               <img class="img" id="u4711_img" src="images/phi_u_nh_p_xu_t/u4658.png"/>
		               <div class="text" id="u4711_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Sách tiếng Anh 1
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4712">
		               <img class="img" id="u4712_img" src="images/phi_u_nh_p_xu_t/u4712.png"/>
		               <div class="text" id="u4712_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Quyển
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4713">
		               <img class="img" id="u4713_img" src="images/phi_u_nh_p_xu_t/u4713.png"/>
		               <div class="text" id="u4713_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        30
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4714">
		               <img class="img" id="u4714_img" src="images/phi_u_nh_p_xu_t/u4714.png"/>
		               <div class="text" id="u4714_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Phòng học A1
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4715">
		               <img class="img" id="u4715_img" src="images/phi_u_nh_p_xu_t/u4715.png"/>
		               <div class="text" id="u4715_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        30.000 VNĐ
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4716">
		               <img class="img" id="u4716_img" src="images/phi_u_nh_p_xu_t/u4716.png"/>
		               <div class="text" id="u4716_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        900.000 VNĐ
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4717">
		               <img class="img" id="u4717_img" src="images/phi_u_nh_p_xu_t/u4717.png"/>
		               <div class="text" id="u4717_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        50.000 VNĐ
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4718">
		               <img class="img" id="u4718_img" src="images/phi_u_nh_p_xu_t/u4718.png"/>
		               <div class="text" id="u4718_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        1.500.000 VNĐ
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4719">
		               <img class="img" id="u4719_img" src="images/phi_u_nh_p_xu_t/u4664.png"/>
		               <div class="text" id="u4719_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        098
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4720">
		               <img class="img" id="u4720_img" src="images/phi_u_nh_p_xu_t/u4665.png"/>
		               <div class="text" id="u4720_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Rèm cửa
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4721">
		               <img class="img" id="u4721_img" src="images/phi_u_nh_p_xu_t/u4721.png"/>
		               <div class="text" id="u4721_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Chiếc
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4722">
		               <img class="img" id="u4722_img" src="images/phi_u_nh_p_xu_t/u4722.png"/>
		               <div class="text" id="u4722_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        2
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4723">
		               <img class="img" id="u4723_img" src="images/phi_u_nh_p_xu_t/u4723.png"/>
		               <div class="text" id="u4723_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Phòng học A2
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4724">
		               <img class="img" id="u4724_img" src="images/phi_u_nh_p_xu_t/u4724.png"/>
		               <div class="text" id="u4724_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        100.000 VNĐ
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4725">
		               <img class="img" id="u4725_img" src="images/phi_u_nh_p_xu_t/u4725.png"/>
		               <div class="text" id="u4725_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        400.000 VNĐ
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4726">
		               <img class="img" id="u4726_img" src="images/holiday/u910.png"/>
		               <div class="text" id="u4726_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        0 VNĐ
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4727">
		               <img class="img" id="u4727_img" src="images/phi_u_nh_p_xu_t/u4727.png"/>
		               <div class="text" id="u4727_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        0 VNĐ
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4728">
		               <img class="img" id="u4728_img" src="images/phi_u_nh_p_xu_t/u4671.png"/>
		               <div class="text" id="u4728_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        980
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4729">
		               <img class="img" id="u4729_img" src="images/phi_u_nh_p_xu_t/u4672.png"/>
		               <div class="text" id="u4729_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Ghế giáo viên
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4730">
		               <img class="img" id="u4730_img" src="images/phi_u_nh_p_xu_t/u4730.png"/>
		               <div class="text" id="u4730_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Chiếc
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4731">
		               <img class="img" id="u4731_img" src="images/phi_u_nh_p_xu_t/u4731.png"/>
		               <div class="text" id="u4731_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        1
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4732">
		               <img class="img" id="u4732_img" src="images/phi_u_nh_p_xu_t/u4732.png"/>
		               <div class="text" id="u4732_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Phòng học B1
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4733">
		               <img class="img" id="u4733_img" src="images/phi_u_nh_p_xu_t/u4733.png"/>
		               <div class="text" id="u4733_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        250.000 VNĐ
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4734">
		               <img class="img" id="u4734_img" src="images/phi_u_nh_p_xu_t/u4734.png"/>
		               <div class="text" id="u4734_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        200.000 VNĐ
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4735">
		               <img class="img" id="u4735_img" src="images/holiday/u904.png"/>
		               <div class="text" id="u4735_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        0 VNĐ
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4736">
		               <img class="img" id="u4736_img" src="images/phi_u_nh_p_xu_t/u4736.png"/>
		               <div class="text" id="u4736_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        0 VNĐ
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4737">
		               <img class="img" id="u4737_img" src="images/phi_u_nh_p_xu_t/u4678.png"/>
		               <div class="text" id="u4737_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        089
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4738">
		               <img class="img" id="u4738_img" src="images/phi_u_nh_p_xu_t/u4679.png"/>
		               <div class="text" id="u4738_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Quà thưởng
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4739">
		               <img class="img" id="u4739_img" src="images/phi_u_nh_p_xu_t/u4739.png"/>
		               <div class="text" id="u4739_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Phần
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4740">
		               <img class="img" id="u4740_img" src="images/phi_u_nh_p_xu_t/u4740.png"/>
		               <div class="text" id="u4740_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        5
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4741">
		               <img class="img" id="u4741_img" src="images/phi_u_nh_p_xu_t/u4741.png"/>
		               <div class="text" id="u4741_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Học sinh
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4742">
		               <img class="img" id="u4742_img" src="images/phi_u_nh_p_xu_t/u4742.png"/>
		               <div class="text" id="u4742_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        25.000 VNĐ
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4743">
		               <img class="img" id="u4743_img" src="images/phi_u_nh_p_xu_t/u4743.png"/>
		               <div class="text" id="u4743_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        525.000 VNĐ
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4744">
		               <img class="img" id="u4744_img" src="images/phi_u_nh_p_xu_t/u4744.png"/>
		               <div class="text" id="u4744_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        0 VNĐ
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default table_cell" id="u4745">
		               <img class="img" id="u4745_img" src="images/phi_u_nh_p_xu_t/u4745.png"/>
		               <div class="text" id="u4745_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        0 VNĐ
		                     </span>
		                  </p>
		               </div>
		            </div>
		         </div>
		         <div class="ax_default heading_3" id="u4746">
		            <div class="" id="u4746_div">
		            </div>
		            <div class="text" id="u4746_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Ghi chú:
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default heading_3" id="u4747">
		            <div class="" id="u4747_div">
		            </div>
		            <div class="text" id="u4747_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Hoàng Ngân, ngày 22 tháng 2 năm 2021
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default heading_3" id="u4748">
		            <div class="" id="u4748_div">
		            </div>
		            <div class="text" id="u4748_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Người tạo phiếu: Nguyễn Văn A
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default heading_3" id="u4749">
		            <div class="" id="u4749_div">
		            </div>
		            <div class="text" id="u4749_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Rèm cửa phòng học A2 bị rách ngày 19 tháng 2 năm 2021, ghế giáo viên phòng học B1 hỏng chân ghế, quà thưởng cho học sinh có thành tích
		                  </span>
		               </p>
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     học tập tốt tuần 2 (2 học sinh A1, 3 học sinh B2)
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		</div>
		
      </div>
    );
  }
}

export default Page;
