import React from "react";
import {Modal, Button, Table} from "react-bootstrap";
import coso from "../../../mock/coso";
import danhmuc from "../../../mock/danhmuc";
export default function CustomModal({ isCreateNew, selectedItem, handleCloseModal }) {
  // const [showModal, setShowModal] = useState(true);
  const handleClose = function() {
    handleCloseModal();
  };
  return (
    <Modal
      size={"lg"}
      show
      onHide={handleClose}
    >
      <Modal.Header closeButton>
        {/*<Modal.Title>Modal title</Modal.Title>*/}
      </Modal.Header>
      <Modal.Body>
        <div className="container">
          <div className="row">
            <div className="col-12">
              <h1 className="font-bold">Thông tin phiếu</h1>
              <p>Tên phiếu: Phiếu nhập kho số 1</p>
              {/*<span className="position-absolute top-0 end-0">x</span>*/}
            </div>
            <div className="col-12">
              <div className="w-100">
                <Table bordered hover className="table">
                  <thead>
                  <tr>
                    <th>Mã sản phẩm</th>
                    <th>Tên sản phẩm</th>
                    <th>Đơn vị</th>
                    <th>Số lượng</th>
                    <th>Nhà cung cấp</th>
                    <th>Giá nhập từng đơn vị</th>
                    <th>Tổng tiền nhập</th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                    <td>123</td>
                    <td>Bàn học sinh</td>
                    <td>Chiếc</td>
                    <td>10</td>
                    <td>Nhà cung cấp 1</td>
                    <td>50.000 VNĐ</td>
                    <td>500.000 VNĐ</td>
                  </tr>
                  <tr>
                    <td>123</td>
                    <td>Bàn học sinh</td>
                    <td>Chiếc</td>
                    <td>10</td>
                    <td>Nhà cung cấp 1</td>
                    <td>50.000 VNĐ</td>
                    <td>500.000 VNĐ</td>
                  </tr>
                  <tr>
                    <td>123</td>
                    <td>Bàn học sinh</td>
                    <td>Chiếc</td>
                    <td>10</td>
                    <td>Nhà cung cấp 1</td>
                    <td>50.000 VNĐ</td>
                    <td>500.000 VNĐ</td>
                  </tr>
                  <tr>
                    <td>123</td>
                    <td>Bàn học sinh</td>
                    <td>Chiếc</td>
                    <td>10</td>
                    <td>Nhà cung cấp 1</td>
                    <td>50.000 VNĐ</td>
                    <td>500.000 VNĐ</td>
                  </tr>
                  </tbody>
                </Table>
<div className="not-found">
                <i class="fas fa-inbox"></i>
                <span>Không tìm thấy kết quả</span>
              </div>
              </div>
            </div>
            <div className="col-12">
              <p className="font-weight-bold">Ghi chú:</p>
            </div>
            <div className="col-12">
              <div className="row">
                <div className="col-4">
                  <Button type="primary"><i className="bi bi-printer"/>In phiếu</Button>
                </div>
                <div className="col-4"></div>
                <div className="col-4">
                  <p className="font-weight-bold">Hoàng Ngân, ngày 22 tháng 2 năm 2021</p>
                  <p className="font-weight-bold">Người tạo phiếu: Nguyễn Văn A</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Modal.Body>
    </Modal>
  );
}
