import React, { useState } from "react";
import { connect } from "dva";
import "react-datepicker/dist/react-datepicker.css";
import { Button, Table } from "react-bootstrap";
import coso from "../../../mock/coso";
import danhmuc from "../../../mock/danhmuc";
import kho from "../../../mock/kho";
import DatePicker from "react-datepicker";
import CustomModal from "./modal";
import { useHistory } from "react-router-dom";
import "./index.css";
import PageHeader from "../../../components/PageHeader";

const Page = () => {
  const [isCreateNew, setIsCreateNew] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [selectedItem, setSelectedItem] = useState({});
  const [fromDate, setFromDate] = useState(new Date());
  const [toDate, setToDate] = useState(new Date());
  const history = useHistory();
  const handleButtonClick = () => {
    setIsCreateNew(true);
    setShowModal(true);
  };
  const handleViewButtonClick = () => {
    setShowModal(true);
  };
  const handleCloseModal = () => {
    setShowModal(false);
  };
  const handleImportButtonClick = () => {
    history.push("/nhap_hang");
  };
  const handleExportButtonClick = () => {
    history.push("/xuat_hang");
  };
  return (
    <div>
      <PageHeader
        title="Phiếu nhập xuất"
        breadcrums={[{
          title: "Home",
          path: "/"
        }, {
          title: "Phiếu nhập xuất",
          path: "/phieu_nhap_xuat"
        }]}
        subTitle="Dễ dàng quản lý hóa đơn, biên lai"
      />
      <div className="iq-card n-pb-0">
        <div className="iq-card-body">
          <div className="">
            <fieldset className="m-b-2">
              <legend>Tìm kiếm</legend>
              <form action="" className="w-100">
                <div className="row align-end">
                  <div className="col-md-2">
                    <div className="form-group">
                      <label htmlFor="coso">Cơ sở</label>
                      <select name="co_so" className="form-control form-control-lg" id="co_so">
                        {coso.map(item => (
                            <option value={item.value}>{item.name}</option>
                        ))}
                      </select>
                    </div>
                  </div>
                  <div className="col-md-2">
                    <div className="form-group">
                      <label htmlFor="coso">Danh mục</label>
                      <select
                          name="danhmuc"
                          className="form-control form-control-lg"
                          id="danhmuc"
                      >
                        {danhmuc.map(item => (
                            <option value={item.value}>{item.name}</option>
                        ))}
                      </select>
                    </div>
                  </div>
                  <div className="col-md-2">
                    <div className="form-group">
                      <label htmlFor="coso">Kho</label>
                      <select name="kho" className="form-control form-control-lg" id="kho">
                        {kho.map(item => (
                            <option value={item.value}>{item.name}</option>
                        ))}
                      </select>
                    </div>
                  </div>
                  <div className="col-md-4">
                    <div className="form-group">
                      <label htmlFor="coso">Khoảng thời gian</label>
                      <div className="row align-end">
                        <div className="col-md-6">
                          <input
                              type="date"
                              className="form-control form-control-lg"
                              id="bat_dau"
                          />
                        </div>
                        <div className="col-md-6">
                          <input
                              type="date"
                              className="form-control form-control-lg"
                              id="ket_thuc"
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-2">
                    <div className="form-group">
                      <Button variant="primary" type="submit">
                        Tìm kiếm
                      </Button>
                    </div>
                  </div>
                </div>
              </form>
            </fieldset>
            <div className="row">
              <div className="col-12 d-flex justify-content-end m-b-2">
                <Button
                    variant="primary"
                    type="submit"
                    style={{marginRight: 30}}
                    onClick={handleImportButtonClick}
                >
                  Tạo mới phiếu nhập
                </Button>
                <Button
                    variant="primary"
                    type="submit"
                    style={{marginRight: 30}}
                    onClick={handleExportButtonClick}
                >
                  Tạo mới phiếu xuất
                </Button>
                <Button
                    variant="primary"
                    type="submit"
                    onClick={handleButtonClick}
                >
                  Chỉnh sửa
                </Button>
              </div>
              <div className="col-12">
                <div className="w-100">
                  <Table bordered hover className="table">
                    <thead>
                    <tr>
                      <th>
                        <input type="checkbox"/>
                      </th>
                      <th>ID</th>
                      <th>Số phiếu</th>
                      <th>Tên phiếu</th>
                      <th>Ngày tạo</th>
                      <th>Thông tin phiếu</th>
                      <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                      <td>
                        <input type="checkbox"/>
                      </td>
                      <td>123</td>
                      <td>1</td>
                      <td>Phiếu nhập kho số 1</td>
                      <td>
                        2021-02-05
                        <br/>
                        15:52
                      </td>
                      <td>
                        <Button
                            variant="success"
                            onClick={handleViewButtonClick}
                        >
                          Xem
                        </Button>
                      </td>
                      <td></td>
                    </tr>
                    <tr>
                      <td>
                        <input type="checkbox"/>
                      </td>
                      <td>456</td>
                      <td>2</td>
                      <td>Phiếu nhập kho số 2</td>
                      <td>
                        2021-02-04
                        <br/>
                        15:52
                      </td>
                      <td>
                        <Button
                            variant="success"
                            onClick={handleViewButtonClick}
                        >
                          Xem
                        </Button>
                      </td>
                      <td></td>
                    </tr>
                    <tr>
                      <td>
                        <input type="checkbox"/>
                      </td>
                      <td>789</td>
                      <td>3</td>
                      <td>Phiếu nhập kho số 1</td>
                      <td>
                        2021-02-04
                        <br/>
                        15:52
                      </td>
                      <td>
                        <Button
                            variant="success"
                            onClick={handleViewButtonClick}
                        >
                          Xem
                        </Button>
                      </td>
                      <td></td>
                    </tr>
                    <tr>
                      <td>
                        <input type="checkbox"/>
                      </td>
                      <td>246</td>
                      <td>4</td>
                      <td>Phiếu nhập kho số 2</td>
                      <td>
                        2021-02-04
                        <br/>
                        15:52
                      </td>
                      <td>
                        <Button
                            variant="success"
                            onClick={handleViewButtonClick}
                        >
                          Xem
                        </Button>
                      </td>
                      <td></td>
                    </tr>
                    </tbody>
                  </Table>
                  <div className="not-found">
                    <i className="fas fa-inbox"></i>
                    <span>Không tìm thấy kết quả</span>
                  </div>
                  {showModal && (
                      <CustomModal
                          isCreateNew={isCreateNew}
                          selectedItem={selectedItem}
                          handleCloseModal={handleCloseModal}
                      />
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default connect(({ Mock }) => ({
  Mock
}))(Page);
