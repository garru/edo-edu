import React from 'react';
import './index.css';
import './ax.css';

class Page extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
		<div class="" id="base" style={{"margin-left":"-150px"}}>
		   <div class="ax_default shape" id="u3985">
		      <div class="" id="u3985_div">
		      </div>
		      <div class="text" id="u3985_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default shape" id="u3986">
		      <div class="" id="u3986_div">
		      </div>
		      <div class="text" id="u3986_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default" data-height="40" data-label="account" data-left="13" data-top="895" data-width="192" id="u3988">
		      <div class="ax_default shape" data-label="accountLable" id="u3989">
		         <img class="img" id="u3989_img" src="images/account_information/accountlable_u55.svg"/>
		         <div class="text" id="u3989_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Nguyen Van A
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u3990">
		         <img class="img" id="u3990_img" src="images/login/u4.svg"/>
		         <div class="text" id="u3990_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="settingMenu" data-left="11" data-top="826" data-width="125" id="u3991">
		      <div class="ax_default shape" data-label="gearIconLable" id="u3992">
		         <img class="img" id="u3992_img" src="images/account_information/geariconlable_u58.svg"/>
		         <div class="text" id="u3992_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Cài đặt
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="44" data-left="11" data-top="826" data-width="44" id="u3993">
		         <div class="ax_default shape" data-label="gearIconBG" id="u3994">
		            <div class="" id="u3994_div">
		            </div>
		            <div class="text" id="u3994_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default shape" data-label="gearIconBG2" id="u3995">
		            <div class="" id="u3995_div">
		            </div>
		            <div class="text" id="u3995_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" data-label="gearIcon" id="u3996">
		            <img class="img" id="u3996_img" src="images/account_information/gearicon_u62.svg"/>
		            <div class="text" id="u3996_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="customerMenu" data-left="11" data-top="145" data-width="143" id="u3997">
		      <div class="ax_default shape" data-label="customerIconLable" id="u3998">
		         <img class="img" id="u3998_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u3998_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Khách hàng
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="customerIconBG" id="u3999">
		         <div class="" id="u3999_div">
		         </div>
		         <div class="text" id="u3999_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="customerIconBG2" id="u4000">
		         <div class="" id="u4000_div">
		         </div>
		         <div class="text" id="u4000_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" data-label="customer" id="u4001">
		         <img class="img" id="u4001_img" src="images/account_information/customer_u67.svg"/>
		         <div class="text" id="u4001_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="classMenu" data-left="11" data-top="198" data-width="143" id="u4002">
		      <div class="ax_default shape" data-label="classIconLable" id="u4003">
		         <img class="img" id="u4003_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u4003_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Lớp học
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG" id="u4004">
		         <div class="" id="u4004_div">
		         </div>
		         <div class="text" id="u4004_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG2" id="u4005">
		         <div class="" id="u4005_div">
		         </div>
		         <div class="text" id="u4005_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" data-label="classicon" id="u4006">
		         <img class="img" id="u4006_img" src="images/account_information/classicon_u72.svg"/>
		         <div class="text" id="u4006_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="dashboardMenu" data-left="11" data-top="91" data-width="143" id="u4007">
		      <div class="ax_default shape" data-label="dashboardIconLable" id="u4008">
		         <img class="img" id="u4008_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u4008_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Dashnoard
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="dashboardIconBG" id="u4009">
		         <div class="" id="u4009_div">
		         </div>
		         <div class="text" id="u4009_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="dashboardIconBG2" id="u4010">
		         <div class="" id="u4010_div">
		         </div>
		         <div class="text" id="u4010_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" data-label="dashboard" id="u4011">
		         <img class="img" id="u4011_img" src="images/account_information/dashboard_u77.svg"/>
		         <div class="text" id="u4011_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="129" data-label="AccountMenu" data-left="58" data-top="806" data-width="246" id="u4012" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default shape" id="u4013">
		         <div class="" id="u4013_div">
		         </div>
		         <div class="text" id="u4013_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u4014">
		         <div class="" id="u4014_div">
		         </div>
		         <div class="text" id="u4014_text">
		            <p>
		               <span style={{"font-family":"ArialMT, Arial","font-weight":"400","text-decoration":"none"}}>
		                  Xin chào:
		               </span>
		               <span style={{"font-family":"Arial Bold, Arial","font-weight":"700","text-decoration":"none"}}>
		                  Nguyen Van A
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4015">
		         <div class="ax_default image" id="u4016">
		            <img class="img" id="u4016_img" src="images/account_information/u82.png"/>
		            <div class="text" id="u4016_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u4017">
		            <div class="" id="u4017_div">
		            </div>
		            <div class="text" id="u4017_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Đăng xuất
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default line" id="u4018">
		         <img class="img" id="u4018_img" src="images/account_information/u84.svg"/>
		         <div class="text" id="u4018_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4019">
		         <div class="ax_default paragraph" id="u4020">
		            <div class="" id="u4020_div">
		            </div>
		            <div class="text" id="u4020_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tài khoản
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default image" id="u4021">
		            <img class="img" id="u4021_img" src="images/account_information/u87.png"/>
		            <div class="text" id="u4021_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="339" data-label="SettingPannel" data-left="58" data-top="525" data-width="339" id="u4022" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default shape" id="u4023">
		         <div class="" id="u4023_div">
		         </div>
		         <div class="text" id="u4023_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u4024">
		         <div class="" id="u4024_div">
		         </div>
		         <div class="text" id="u4024_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Hệ thống
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4025">
		         <div class="ax_default icon" id="u4026">
		            <img class="img" id="u4026_img" src="images/account_information/u92.svg"/>
		            <div class="text" id="u4026_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u4027">
		            <div class="" id="u4027_div">
		            </div>
		            <div class="text" id="u4027_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tài khoản
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4028">
		         <div class="ax_default paragraph" id="u4029">
		            <div class="" id="u4029_div">
		            </div>
		            <div class="text" id="u4029_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phân quyền
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u4030">
		            <img class="img" id="u4030_img" src="images/account_information/u96.svg"/>
		            <div class="text" id="u4030_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u4031">
		         <div class="" id="u4031_div">
		         </div>
		         <div class="text" id="u4031_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Đào tạo
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4032">
		         <div class="ax_default paragraph" id="u4033">
		            <div class="" id="u4033_div">
		            </div>
		            <div class="text" id="u4033_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Thương hiệu
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u4034">
		            <img class="img" id="u4034_img" src="images/account_information/u100.svg"/>
		            <div class="text" id="u4034_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4035">
		         <div class="ax_default paragraph" id="u4036">
		            <div class="" id="u4036_div">
		            </div>
		            <div class="text" id="u4036_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trung tâm
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u4037">
		            <img class="img" id="u4037_img" src="images/account_information/u103.svg"/>
		            <div class="text" id="u4037_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4038">
		         <div class="ax_default icon" id="u4039">
		            <img class="img" id="u4039_img" src="images/account_information/u105.svg"/>
		            <div class="text" id="u4039_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u4040">
		            <div class="" id="u4040_div">
		            </div>
		            <div class="text" id="u4040_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Ngày nghỉ
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4041">
		         <div class="ax_default icon" id="u4042">
		            <img class="img" id="u4042_img" src="images/account_information/u108.svg"/>
		            <div class="text" id="u4042_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u4043">
		            <div class="" id="u4043_div">
		            </div>
		            <div class="text" id="u4043_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phòng ban - Vai trò
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4044">
		         <div class="ax_default paragraph" id="u4045">
		            <div class="" id="u4045_div">
		            </div>
		            <div class="text" id="u4045_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trạng thái học viên
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u4046">
		            <img class="img" id="u4046_img" src="images/account_information/u112.svg"/>
		            <div class="text" id="u4046_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4047">
		         <div class="ax_default paragraph" id="u4048">
		            <div class="" id="u4048_div">
		            </div>
		            <div class="text" id="u4048_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tiêu chí đánh giá
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u4049">
		            <img class="img" id="u4049_img" src="images/account_information/u115.svg"/>
		            <div class="text" id="u4049_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4050">
		         <div class="ax_default paragraph" id="u4051">
		            <div class="" id="u4051_div">
		            </div>
		            <div class="text" id="u4051_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Chương trình học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4052">
		            <div class="ax_default icon" id="u4053">
		               <img class="img" id="u4053_img" src="images/account_information/u119.svg"/>
		               <div class="text" id="u4053_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4054">
		         <div class="ax_default icon" id="u4055">
		            <img class="img" id="u4055_img" src="images/account_information/u121.svg"/>
		            <div class="text" id="u4055_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u4056">
		            <div class="" id="u4056_div">
		            </div>
		            <div class="text" id="u4056_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Bài học
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4057">
		         <div class="ax_default paragraph" id="u4058">
		            <div class="" id="u4058_div">
		            </div>
		            <div class="text" id="u4058_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phòng học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u4059">
		            <img class="img" id="u4059_img" src="images/account_information/u125.svg"/>
		            <div class="text" id="u4059_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4060">
		         <div class="ax_default paragraph" id="u4061">
		            <div class="" id="u4061_div">
		            </div>
		            <div class="text" id="u4061_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trường học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u4062">
		            <img class="img" id="u4062_img" src="images/account_information/u128.svg"/>
		            <div class="text" id="u4062_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default line" id="u4063">
		         <img class="img" id="u4063_img" src="images/account_information/u129.svg"/>
		         <div class="text" id="u4063_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u4064">
		         <div class="" id="u4064_div">
		         </div>
		         <div class="text" id="u4064_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Tài chính
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4065">
		         <div class="ax_default paragraph" id="u4066">
		            <div class="" id="u4066_div">
		            </div>
		            <div class="text" id="u4066_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Học phí
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u4067">
		            <img class="img" id="u4067_img" src="images/account_information/u133.svg"/>
		            <div class="text" id="u4067_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4068">
		         <div class="ax_default paragraph" id="u4069">
		            <div class="" id="u4069_div">
		            </div>
		            <div class="text" id="u4069_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Khuyến mại
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u4070">
		            <img class="img" id="u4070_img" src="images/account_information/u136.svg"/>
		            <div class="text" id="u4070_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="classMenu" data-left="11" data-top="252" data-width="143" id="u4071">
		      <div class="ax_default shape" data-label="classIconLable" id="u4072">
		         <img class="img" id="u4072_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u4072_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Giáo viên
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG" id="u4073">
		         <div class="" id="u4073_div">
		         </div>
		         <div class="text" id="u4073_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG2" id="u4074">
		         <div class="" id="u4074_div">
		         </div>
		         <div class="text" id="u4074_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u4075">
		         <img class="img" id="u4075_img" src="images/account_information/u141.svg"/>
		         <div class="text" id="u4075_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u4076">
		      <img class="img" id="u4076_img" src="images/account_information/u142.svg"/>
		      <div class="text" id="u4076_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u4077">
		      <img class="img" id="u4077_img" src="images/account_information/u143.svg"/>
		      <div class="text" id="u4077_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div id="u3984" style={{"display":"none","visibility":"hidden"}}>
		   </div>
		   <div class="ax_default" data-height="55" data-label="Header" data-left="133" data-top="17" data-width="520" id="u4078">
		      <div class="ax_default paragraph" id="u4079">
		         <div class="" id="u4079_div">
		         </div>
		         <div class="text" id="u4079_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Quản lý kênh thanh toán của trung tâm
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u4080">
		         <div class="" id="u4080_div">
		         </div>
		         <div class="text" id="u4080_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Các kênh thanh toán trong hệ thống
		               </span>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" id="u4081">
		      <div class="ax_default table_cell" id="u4082">
		         <img class="img" id="u4082_img" src="images/k_nh_thanh_to_n/u4082.png"/>
		         <div class="text" id="u4082_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Tên tài khoản
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4083">
		         <img class="img" id="u4083_img" src="images/k_nh_thanh_to_n/u4083.png"/>
		         <div class="text" id="u4083_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Mô tả
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4084">
		         <img class="img" id="u4084_img" src="images/k_nh_thanh_to_n/u4084.png"/>
		         <div class="text" id="u4084_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Triết khấu
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4085">
		         <img class="img" id="u4085_img" src="images/k_nh_thanh_to_n/u4085.png"/>
		         <div class="text" id="u4085_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Cơ sở
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4086">
		         <img class="img" id="u4086_img" src="images/k_nh_thanh_to_n/u4086.png"/>
		         <div class="text" id="u4086_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Thao tác
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4087">
		         <img class="img" id="u4087_img" src="images/k_nh_thanh_to_n/u4087.png"/>
		         <div class="text" id="u4087_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Chuyển khoản
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4088">
		         <img class="img" id="u4088_img" src="images/k_nh_thanh_to_n/u4088.png"/>
		         <div class="text" id="u4088_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4089">
		         <img class="img" id="u4089_img" src="images/k_nh_thanh_to_n/u4089.png"/>
		         <div class="text" id="u4089_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4090">
		         <img class="img" id="u4090_img" src="images/k_nh_thanh_to_n/u4090.png"/>
		         <div class="text" id="u4090_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4091">
		         <img class="img" id="u4091_img" src="images/quản_lý_giờ_học/u1614.png"/>
		         <div class="text" id="u4091_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4092">
		         <img class="img" id="u4092_img" src="images/k_nh_thanh_to_n/u4087.png"/>
		         <div class="text" id="u4092_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Techcombank
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4093">
		         <img class="img" id="u4093_img" src="images/k_nh_thanh_to_n/u4088.png"/>
		         <div class="text" id="u4093_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  STK: xxxxxxxx Chi nhánh YYYY
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4094">
		         <img class="img" id="u4094_img" src="images/k_nh_thanh_to_n/u4089.png"/>
		         <div class="text" id="u4094_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4095">
		         <img class="img" id="u4095_img" src="images/k_nh_thanh_to_n/u4090.png"/>
		         <div class="text" id="u4095_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  All
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4096">
		         <img class="img" id="u4096_img" src="images/quản_lý_giờ_học/u1614.png"/>
		         <div class="text" id="u4096_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4097">
		         <img class="img" id="u4097_img" src="images/k_nh_thanh_to_n/u4097.png"/>
		         <div class="text" id="u4097_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Agribank
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4098">
		         <img class="img" id="u4098_img" src="images/k_nh_thanh_to_n/u4098.png"/>
		         <div class="text" id="u4098_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4099">
		         <img class="img" id="u4099_img" src="images/k_nh_thanh_to_n/u4099.png"/>
		         <div class="text" id="u4099_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4100">
		         <img class="img" id="u4100_img" src="images/k_nh_thanh_to_n/u4100.png"/>
		         <div class="text" id="u4100_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Hoàng Ngân
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4101">
		         <img class="img" id="u4101_img" src="images/k_nh_thanh_to_n/u4101.png"/>
		         <div class="text" id="u4101_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4102">
		         <img class="img" id="u4102_img" src="images/k_nh_thanh_to_n/u4087.png"/>
		         <div class="text" id="u4102_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Tiền mặt
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4103">
		         <img class="img" id="u4103_img" src="images/k_nh_thanh_to_n/u4088.png"/>
		         <div class="text" id="u4103_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4104">
		         <img class="img" id="u4104_img" src="images/k_nh_thanh_to_n/u4089.png"/>
		         <div class="text" id="u4104_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4105">
		         <img class="img" id="u4105_img" src="images/k_nh_thanh_to_n/u4090.png"/>
		         <div class="text" id="u4105_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4106">
		         <img class="img" id="u4106_img" src="images/quản_lý_giờ_học/u1614.png"/>
		         <div class="text" id="u4106_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4107">
		         <img class="img" id="u4107_img" src="images/k_nh_thanh_to_n/u4087.png"/>
		         <div class="text" id="u4107_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Chưa ghi nhận
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4108">
		         <img class="img" id="u4108_img" src="images/k_nh_thanh_to_n/u4088.png"/>
		         <div class="text" id="u4108_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Tiền có thể được chăm sóc khách hàng thu nhưng chưa bàn giao cho kế toán
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4109">
		         <img class="img" id="u4109_img" src="images/k_nh_thanh_to_n/u4089.png"/>
		         <div class="text" id="u4109_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4110">
		         <img class="img" id="u4110_img" src="images/k_nh_thanh_to_n/u4090.png"/>
		         <div class="text" id="u4110_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  All
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4111">
		         <img class="img" id="u4111_img" src="images/quản_lý_giờ_học/u1614.png"/>
		         <div class="text" id="u4111_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4112">
		         <img class="img" id="u4112_img" src="images/k_nh_thanh_to_n/u4112.png"/>
		         <div class="text" id="u4112_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Đã ghi nhận
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4113">
		         <img class="img" id="u4113_img" src="images/k_nh_thanh_to_n/u4113.png"/>
		         <div class="text" id="u4113_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Tiền đã được bàn giao cho kế toán
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4114">
		         <img class="img" id="u4114_img" src="images/k_nh_thanh_to_n/u4114.png"/>
		         <div class="text" id="u4114_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4115">
		         <img class="img" id="u4115_img" src="images/k_nh_thanh_to_n/u4115.png"/>
		         <div class="text" id="u4115_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  All
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u4116">
		         <img class="img" id="u4116_img" src="images/quản_lý_giờ_học/u1624.png"/>
		         <div class="text" id="u4116_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="22" data-left="1541" data-top="84" data-width="309" id="u4117">
		      <div class="ax_default icon" id="u4118">
		         <img class="img" id="u4118_img" src="images/k_nh_thanh_to_n/u4118.svg"/>
		         <div class="text" id="u4118_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default label" id="u4119">
		         <div class="" id="u4119_div">
		         </div>
		         <div class="text" id="u4119_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Trung tâm
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default droplist" id="u4120">
		         <div class="" id="u4120_div">
		         </div>
		         <select class="u4120_input" id="u4120_input">
		         </select>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u4121">
		      <div class="" id="u4121_div">
		      </div>
		      <div class="text" id="u4121_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Thêm mới
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default" data-height="17" data-left="1772" data-top="208" data-width="44" id="u4122">
		      <div class="ax_default icon" id="u4123">
		         <img class="img" id="u4123_img" src="images/promotion/u3722.svg"/>
		         <div class="text" id="u4123_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u4124">
		         <img class="img" id="u4124_img" src="images/evaluation_category/u2145.svg"/>
		         <div class="text" id="u4124_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="17" data-left="1772" data-top="179" data-width="44" id="u4125">
		      <div class="ax_default icon" id="u4126">
		         <img class="img" id="u4126_img" src="images/promotion/u3722.svg"/>
		         <div class="text" id="u4126_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u4127">
		         <img class="img" id="u4127_img" src="images/evaluation_category/u2145.svg"/>
		         <div class="text" id="u4127_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="17" data-left="1772" data-top="238" data-width="44" id="u4128">
		      <div class="ax_default icon" id="u4129">
		         <img class="img" id="u4129_img" src="images/promotion/u3722.svg"/>
		         <div class="text" id="u4129_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u4130">
		         <img class="img" id="u4130_img" src="images/evaluation_category/u2145.svg"/>
		         <div class="text" id="u4130_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="17" data-left="1772" data-top="267" data-width="44" id="u4131">
		      <div class="ax_default icon" id="u4132">
		         <img class="img" id="u4132_img" src="images/promotion/u3722.svg"/>
		         <div class="text" id="u4132_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u4133">
		         <img class="img" id="u4133_img" src="images/evaluation_category/u2145.svg"/>
		         <div class="text" id="u4133_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="17" data-left="1772" data-top="297" data-width="44" id="u4134">
		      <div class="ax_default icon" id="u4135">
		         <img class="img" id="u4135_img" src="images/promotion/u3722.svg"/>
		         <div class="text" id="u4135_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u4136">
		         <img class="img" id="u4136_img" src="images/evaluation_category/u2145.svg"/>
		         <div class="text" id="u4136_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="17" data-left="1772" data-top="326" data-width="44" id="u4137">
		      <div class="ax_default icon" id="u4138">
		         <img class="img" id="u4138_img" src="images/promotion/u3722.svg"/>
		         <div class="text" id="u4138_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u4139">
		         <img class="img" id="u4139_img" src="images/evaluation_category/u2145.svg"/>
		         <div class="text" id="u4139_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="950" data-label="Add kenh thanh toán" data-left="0" data-top="0" data-width="1920" id="u4140" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default shape" id="u4141">
		         <div class="" id="u4141_div">
		         </div>
		         <div class="text" id="u4141_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4142">
		         <div class="ax_default shape" id="u4143">
		            <div class="" id="u4143_div">
		            </div>
		            <div class="text" id="u4143_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u4144">
		            <div class="" id="u4144_div">
		            </div>
		            <div class="text" id="u4144_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Thêm mới kênh thanh toán
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4145">
		            <div class="ax_default label" id="u4146">
		               <div class="" id="u4146_div">
		               </div>
		               <div class="text" id="u4146_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Tên kênh thanh toán
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default text_field" id="u4147">
		               <div class="" id="u4147_div">
		               </div>
		               <input class="u4147_input" id="u4147_input" type="text" value=""/>
		            </div>
		         </div>
		         <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4148">
		            <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4149">
		               <div class="ax_default label" id="u4150">
		                  <div class="" id="u4150_div">
		                  </div>
		                  <div class="text" id="u4150_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Nhóm cha tương ứng
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default droplist" id="u4151">
		                  <div class="" id="u4151_div">
		                  </div>
		                  <select class="u4151_input" id="u4151_input">
		                     <option class="u4151_input_option" value=" ">
		                     </option>
		                     <option class="u4151_input_option" value="Tiền mặt">
		                        Tiền mặt
		                     </option>
		                     <option class="u4151_input_option" value="Chuyển khoản">
		                        Chuyển khoản
		                     </option>
		                     <option class="u4151_input_option" value="Trả góp">
		                        Trả góp
		                     </option>
		                  </select>
		               </div>
		            </div>
		         </div>
		         <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4152">
		            <div class="ax_default label" id="u4153">
		               <div class="" id="u4153_div">
		               </div>
		               <div class="text" id="u4153_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Chiết khấu
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default text_field" id="u4154">
		               <div class="" id="u4154_div">
		               </div>
		               <input class="u4154_input" id="u4154_input" type="text" value=""/>
		            </div>
		            <div class="ax_default label" id="u4155">
		               <div class="" id="u4155_div">
		               </div>
		               <div class="text" id="u4155_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Là tỷ lệ phần trăm mà cổng thanh toán sẽ giữ lại khi thanh toán qua kênh
		                     </span>
		                  </p>
		               </div>
		            </div>
		         </div>
		         <div class="ax_default primary_button" id="u4156">
		            <div class="" id="u4156_div">
		            </div>
		            <div class="text" id="u4156_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Lưu
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default button" id="u4157">
		            <img class="img" id="u4157_img" src="images/course_price/u3982.svg"/>
		            <div class="text" id="u4157_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Hủy
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u4158">
		            <img class="img" id="u4158_img" src="images/evaluation_category/u2145.svg"/>
		            <div class="text" id="u4158_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4159">
		            <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u4160">
		               <div class="ax_default label" id="u4161">
		                  <div class="" id="u4161_div">
		                  </div>
		                  <div class="text" id="u4161_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Trung tâm áp dụng
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default droplist" id="u4162">
		                  <div class="" id="u4162_div">
		                  </div>
		                  <select class="u4162_input" id="u4162_input">
		                     <option class="u4162_input_option" selected="" value="Có thể chọn nhiều trung tâm">
		                        Có thể chọn nhiều trung tâm
		                     </option>
		                     <option class="u4162_input_option" value="Tất cả">
		                        Tất cả
		                     </option>
		                     <option class="u4162_input_option" value="Hoàng Ngân">
		                        Hoàng Ngân
		                     </option>
		                     <option class="u4162_input_option" value="Hòa Mã">
		                        Hòa Mã
		                     </option>
		                     <option class="u4162_input_option" value="Mai Hắc Đế">
		                        Mai Hắc Đế
		                     </option>
		                  </select>
		               </div>
		            </div>
		         </div>
		         <div class="ax_default label" id="u4163">
		            <div class="" id="u4163_div">
		            </div>
		            <div class="text" id="u4163_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Mô tả
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default text_area" id="u4164">
		            <div class="" id="u4164_div">
		            </div>
		            <textarea class="u4164_input" id="u4164_input"></textarea>
		         </div>
		      </div>
		   </div>
		</div>
		
      </div>
    );
  }
}

export default Page;
