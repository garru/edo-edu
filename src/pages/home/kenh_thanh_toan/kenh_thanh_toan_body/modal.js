import React from "react";
import {Button, Modal} from "react-bootstrap";
import {PHUONG_THUC_THANH_TOAN} from "./data";
import MultiSelect from "react-multi-select-component";
import {ErrorMessage, Formik} from "formik"
import usePayment from "../../../../hooks/usePayment";
import Input from "../../../../components/Form/Input";
import Select from "../../../../components/Form/Select";
import TextArea from "../../../../components/Form/TextArea";
import {connect} from "dva";

function CustomModal(props) {
  const handleClose = () => {
    props.onClose();
  };
  const {isCreateNew} = props;
  const {initialValues, validationSchema, submit} = usePayment({
    dispatch: props.dispatch,
    isCreateNew,
    onClose: props.onClose
  });

  function handleMultiSelectChange(values, setFieldValue, setFieldTouched) {
    setFieldValue("centerIds", values.map(center => center.value));
    // setFieldTouched("centerIds", true, true);
  }

  return (
    <Modal
      size={"lg"}
      show
      onHide={handleClose}
      backdrop="static"
      keyboard={false}
      id="AddNew"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Formik
        initialValues={!isCreateNew ? props.Payment?.current_payment : initialValues}
        validationSchema={validationSchema}
        onSubmit={submit}
      >
        {({
            values, handleChange, handleBlur, handleSubmit, setFieldValue, setFieldTouched
          }) => (
          <form onSubmit={handleSubmit}>
            <Modal.Header closeButton>
              <Modal.Title>
                {isCreateNew ? "Thêm mới gói học phí" : "Chỉnh sửa gói học phí"}
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <div>
                <form onSubmit={handleSubmit}>
                  <div className="iq-card ">
                    <div className="iq-card-body">
                      <div className="row m-b-2">
                        <div className="col-12">
                          <div className="row font-size-14">
                            <div className="col-6 ">
                              <Input
                                id="name"
                                name="name"
                                label="Tên kênh thanh toán:"
                                value={values.name}
                                onChange={handleChange}
                                onBlur={handleBlur}
                              />
                              <Select
                                id="paymentGroup"
                                name="paymentGroup"
                                label="Nhóm cha tương ứng:"
                                value={values.paymentGroup}
                                options={PHUONG_THUC_THANH_TOAN}
                                onChange={handleChange}
                                onBlur={handleBlur}
                              />
                            </div>
                            <div className="col-6 ">
                              <TextArea
                                id="description"
                                name="description"
                                label="Mô tả"
                                value={values.description}
                                onChange={handleChange}
                                onBlur={handleBlur}
                              />
                            </div>
                            <div className="col-6 ">
                              <div className="form-group">
                                <label htmlFor="ten">Trung tâm áp dụng:</label>
                                <MultiSelect
                                  options={(props.Payment.list_center || [])
                                    .map(center => ({label: center.name, value: center.id}))}
                                  value={(props.Payment.list_center || [])
                                    .map(center => ({label: center.name, value: center.id}))
                                    .filter(center => (values.centerIds || []).includes(center.value))}
                                  onChange={values => handleMultiSelectChange(values, setFieldValue, setFieldTouched)}
                                  labelledBy="Select"
                                  overrideStrings={{
                                    allItemsAreSelected: "Tất cả",
                                    clearSearch: "Clear Search",
                                    noOptions: "Không tìm thấy",
                                    search: "Search",
                                    selectAll: "Select All",
                                    selectSomeItems: "Có thể chọn nhiều giá trị"
                                  }}
                                  ArrowRenderer={() => (
                                    <i className="fa fa-chevron-down"/>
                                  )}
                                />
                                <ErrorMessage component="p" className="error-message" name="centerIds"/>
                              </div>
                            </div>
                            <div className="col-6">
                              <Input
                                id="chargePercent"
                                name="chargePercent"
                                label="Chiết khấu:"
                                value={values.chargePercent}
                                onChange={handleChange}
                                onBlur={handleBlur}
                              />
                              <p>
                                <i>
                                  Là tỷ lệ phần trăm mà cổng thanh toán sẽ giữ lại
                                  khi thanh toán qua kênh
                                </i>
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </Modal.Body>
            <Modal.Footer>
              <Button type="primary" variant="primary" style={{marginRight: 15}}>
                {isCreateNew ? "Tạo mới" : "Cập nhật"}
              </Button>
              <Button type="button" variant={"secondary"} onClick={handleClose}>
                Hủy
              </Button>
            </Modal.Footer>
          </form>
        )}
      </Formik>
    </Modal>
  );
}

export default connect(({Payment}) => ({
  Payment
}))(CustomModal);
