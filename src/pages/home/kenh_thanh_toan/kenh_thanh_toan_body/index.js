import React from "react";
import {connect} from "dva";
import {Button} from "react-bootstrap";
import CustomModal from "./modal";
import CustomTable from "../../../../components/Table";
import CustomTooltip from "../../../../components/CustomTooltip";
import usePayment from "../../../../hooks/usePayment";

class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id: null,
      tooltipIndex: null,
      show: false,
      target: null,
      activeIndex: null
    };
  }

  hooks = usePayment({ dispatch: this.props.dispatch })

  renderTooltip(index) {
    const {tooltipIndex} = this.state;
    const {dispatch} = this.props;
    const dataSource = [{
      label: "Chỉnh sửa",
      className: "fa fa-edit",
      onClick: e => {
        dispatch({
          type: "Payment/view",
          payload: {id: this.state.id}
        }).then(() => {
          this.setState({isCreatNew: false, showModal: true});
        });
        this.handleClick(e, tooltipIndex, true);
      }
    }, {
      label: "Xóa",
      className: "fa fa-trash-alt",
      onClick: e => this.hooks._delete(this.state.id)
    }]
    return (
      <CustomTooltip
        show={this.state.show}
        onHide={e => this.handleClick(e, tooltipIndex)}
        dataSource={dataSource}
        target={this.state.target}
      />
    );
  }

  handleClick(e, index, isHide) {
    this.setState({
      show: !(index === this.state.activeIndex),
      target: isHide ? null : e.target,
      activeIndex: index === this.state.activeIndex ? null : index
    });
  }

  onClose = () => {
    this.setState({isCreateNew: false, showModal: false});
  };

  columns = [{
      title: "Tên tài khoản",
      key: "name",
      class: "tb-width-300"
    },
    {
      title: "Mô tả",
      key: "description",
      class: "tb-width-300"
    },
    {
      title: "Chiết khấu",
      key: "chargePercent",
      class: "tb-width-150"
    },
    {
      title: "Trung tâm",
      key: "centerName",
      class: "tb-width-200"
    }, {
      title: "",
      key: "action",
      class: "tb-width-50",
      render: (col, index) => (
        <i className="fa fa-ellipsis-h" onClick={e => {
          e.preventDefault();
          this.handleClick(e, index);
          this.setState({ id: col.id, tooltipIndex: index})
        }}/>
      )
    }]

  render() {
    return (
      <div className="row">
        <div className="col-12 text-right">
          <div className="form-group">
            <Button
              variant="primary"
              type="submit"
              onClick={() =>
                this.setState({isCreateNew: true, showModal: true})
              }
            >
              Tạo mới
            </Button>
          </div>
        </div>
        <div className="col-12">
          <CustomTable
            dataSource={this.props.Payment.list_payment}
            total={this.props.Payment.total_record}
            columns={this.columns}
            onChange={data => this.updateOptions(data)}
            checkbox
          />
        </div>
        {this.state.showModal && (
          <CustomModal
            isCreateNew={this.state.isCreateNew}
            onClose={this.onClose}
            selectedItem={this.state.selectedItem}
            Payment={this.props.Payment}
          />
        )}
        {this.renderTooltip()}
      </div>
    );
  }
}

export default connect(({Payment}) => ({
  Payment
}))(Page);
