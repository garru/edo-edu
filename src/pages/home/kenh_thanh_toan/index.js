import React from "react";
import { connect } from "dva";
import KenhThanhToanBody from "./kenh_thanh_toan_body";
import PageHeader from "../../../components/PageHeader";

class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillMount() {
    const { dispatch } = this.props;
    dispatch({
      type: "Payment/list"
    });
    dispatch({
      type: "Payment/listcenter"
    })
  }

  bre = {
    title: "Quản lý kênh thanh toán của trung tâm",
    subTitle: "Các kênh thanh toán trong hệ thống",
    breadcrums: [
      {
        title: "Home",
        path: "/"
      },
      {
        title: "Quản lý kênh thanh toán của trung tâm",
        path: ""
      }
    ]
  };
  render() {
    return (
      <React.Fragment>
        <PageHeader {...this.bre} />
        <div className="iq-card  pb-0">
          <div className="iq-card-body">
            <div className="">
              <KenhThanhToanBody />
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default connect(({ HealthcareService }) => ({
  HealthcareService
}))(Page);
