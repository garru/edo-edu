import React, { useState, useEffect } from "react";
import { Modal, Button } from "react-bootstrap";
import Rating from "react-rating";
import { connect } from "dva";
import CustomTable from "../../../components/Table";
import { Formik, Field, Form } from "formik";
import Input from "../../../components/Form/Input";
import Select from "../../../components/Form/Select";
import TextArea from "../../../components/Form/TextArea";
import * as yup from "yup";
import moment from "moment";

const CustomModal = ({
  handleClose,
  CustomerDetail,
  StudentReserve,
  dispatch,
  id,
  classId,
  selectedStudent
}) => {
  const [show, setShow] = useState(true);
  console.log(selectedStudent);
  useEffect(() => {
    dispatch({
      type: "Global/showLoading"
    });
    dispatch({
      type: "StudentReserve/viewback",
      payload: {
        id,
        classId
      }
    }).then(data => {
      dispatch({
        type: "Global/hideLoading"
      });
    });
  }, []);

  return (
    <Modal
      size={"lg"}
      show={show}
      onHide={() => handleClose(false)}
      backdrop="static"
      keyboard={false}
      id="ForgotPassword"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title>Bảo lưu</Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <div className="row m-b-2 g-2">
          <div className="col-12">
            <fieldset>
              <legend>Thông tin lịch học cũ</legend>
              <div className="row">
                <div className="col-md-4 col-sm-6 col-xs-12">
                  <p>
                    Giáo trình:
                    <b> {StudentReserve?.viewback?.programName}</b>
                  </p>
                </div>
                <div className="col-md-4 col-sm-6 col-xs-12">
                  <p>
                    Bài tam dừng:
                    <b> {StudentReserve?.viewback?.lessonName}</b>
                  </p>
                </div>
                <div className="col-md-4 col-sm-6 col-xs-12">
                  <p>
                    Nội dung bài:
                    <b> {StudentReserve?.viewback?.lessonDesc}</b>
                  </p>
                </div>
                <div className="col-md-4 col-sm-6 col-xs-12">
                  <p>
                    Dư credit:
                    <b>
                      {" "}
                      {StudentReserve?.viewback?.creditRemain?.toLocaleString(
                        "it-IT",
                        {
                          style: "currency",
                          currency: "VND"
                        }
                      )}
                      }
                    </b>
                  </p>
                </div>
              </div>
            </fieldset>
          </div>
        </div>
        <div className="row m-b-2 g-2">
          <div className="col-12">
            <fieldset>
              <legend>Thông tin lớp học</legend>
              <div className="row">
                <div className="col-md-4 col-sm-6 col-xs-12">
                  <p>
                    Từ bài:
                    <b> {selectedStudent?.lessonName}</b>
                  </p>
                </div>
                <div className="col-md-4 col-sm-6 col-xs-12">
                  <p>
                    Nội dung:
                    <b> {selectedStudent?.lessonDesc}</b>
                  </p>
                </div>
                <div className="col-md-4 col-sm-6 col-xs-12">
                  <p>
                    Ngày học tiếp theo :
                    <b>
                      {" "}
                      {selectedStudent?.classSchedule
                        ? moment(selectedStudent?.classSchedule).format(
                            "DD-MM-YYYY"
                          )
                        : ""}
                    </b>
                  </p>
                </div>
                <div className="col-md-4 col-sm-6 col-xs-12">
                  <p>
                    Lịch học:
                    <b>
                      {" "}
                      {selectedStudent?.classSchedule
                        ? moment(selectedStudent?.classSchedule).format(
                            "DD-MM-YYYY"
                          )
                        : ""}
                    </b>
                  </p>
                </div>
                <div className="col-md-4 col-sm-6 col-xs-12">
                  <p>
                    Phòng học:
                    <b> {selectedStudent?.roomName}</b>
                  </p>
                </div>
                <div className="col-md-4 col-sm-6 col-xs-12">
                  <p>
                    Giáo viên trong nước:
                    <b> {selectedStudent?.teacherDomestic}</b>
                  </p>
                </div>
                <div className="col-md-4 col-sm-6 col-xs-12">
                  <p>
                    Giáo viên nước ngoài:
                    <b> {selectedStudent?.teacherForeign}</b>
                  </p>
                </div>
              </div>
            </fieldset>
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button
          variant={"primary"}
          style={{ marginRight: 15 }}
          onClick={() => handleClose(false)}
        >
          Lưu
        </Button>
        <Button variant={"secondary"} onClick={() => handleClose(false)}>
          Đóng
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default connect(
  ({ CustomerDetail, StudentReserve, ScheduleAttendance, Global }) => ({
    CustomerDetail,
    StudentReserve,
    ScheduleAttendance,
    Global
  })
)(CustomModal);
