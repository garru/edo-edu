import React, { useState, useEffect } from "react";
import { Button, Nav } from "react-bootstrap";
import { connect } from "dva";
import "react-datepicker/dist/react-datepicker.css";
import CustomModal from "./modal";
import CustomTable from "../../../components/Table";
import UploadAvatar from "../../../components/UploadAvatar";
import { useParams, Link } from "react-router-dom";
import PageHeader from "../../../components/PageHeader";
import moment from "moment";
import ModalBaoLuu from "./modal_bao_luu";
import ModalChiTiet from "./modal_chi_tiet";

const Page = props => {
  const [showModal, setShowModal] = useState(false);
  const [count, setCount] = useState(0);
  const [baoLuu, setbaoLuu] = useState(false);
  const [chiTiet, setChiTiet] = useState(false);
  const { id, classId } = useParams();
  const { dispatch, StudentDetail } = props;
  const [selectedStudent, setSelectedStudent] = useState(null);
  const bre = {
    title: "Chi tiết học viên",
    breadcrums: [
      {
        title: "Home",
        path: "/"
      },
      {
        title: "Chi tiết học viên",
        path: ""
      }
    ]
  };
  useEffect(() => {
    dispatch({
      type: "Global/showLoading"
    });
    Promise.all([
      dispatch({
        type: "StudentDetail/currentstudent",
        payload: { id, classId }
      }),
      dispatch({
        type: "StudentDetail/currentreserve",
        payload: { id, classId }
      })
    ]).then(data => {
      dispatch({
        type: "Global/hideLoading"
      });
    });
  }, [count]);

  const handleCloseModal = data => {
    setShowModal(false);
    setbaoLuu(false);
    setChiTiet(false);
    if (data) {
      dispatch({
        type: "Global/showLoading"
      });
      dispatch({
        type: "StudentDetail/currentschedule",
        payload: { id, classId }
      }).then(data => {
        dispatch({
          type: "Global/hideLoading"
        });
      });
    }
  };

  const columns = [
    {
      title: "Loại",
      key: "typeName",
      class: "tb-width-150"
    },
    {
      title: "Thời điểm",
      key: "reserveDate",
      class: "tb-width-150",
      render: (col, index) => (
        <span>
          {col.reserveDate ? moment(col.reserveDate).format("DD-MM-YYYY") : ""}
        </span>
      )
    },
    {
      title: "Thời gian",
      key: "estimateDate",
      class: "tb-width-150",
      render: (col, index) => (
        <span>
          {col.estimateDate
            ? moment(col.estimateDate).format("DD-MM-YYYY")
            : ""}
        </span>
      )
    },
    {
      title: "Đi học lại",
      key: "reserveDuration",
      class: "tb-width-150"
    },
    {
      title: "Giáo trình",
      key: "programName",
      class: "tb-width-150"
    },
    {
      title: "Bài tạm dừng",
      key: "lessonName",
      class: "tb-width-150"
    },
    {
      title: "Lớp mới",
      key: "newClass",
      class: "tb-width-150"
    },
    {
      title: "Trung tâm",
      key: "newCenter",
      class: "tb-width-150"
    },
    {
      title: "Từ ngày",
      key: "newDate",
      class: "tb-width-150",
      render: (col, index) => (
        <span>
          {col.newDate ? moment(col.newDate).format("DD-MM-YYYY") : ""}
        </span>
      )
    },
    {
      title: "",
      key: "status",
      class: "tb-width-150",
      render: (col, index) =>
        col.newClass ? (
          <div
            className="status"
            onClick={() => {
              setSelectedStudent(col);
              setChiTiet(true);
            }}
          >
            Chi tiết
          </div>
        ) : (
          <div className="status">Đi học lại</div>
        )
    }
  ];

  return (
    <>
      <PageHeader {...bre} />
      <div className="iq-card ">
        <div className="iq-card-body">
          <div className="row">
            <div className="col-12">
              <fieldset>
                <legend>Thông tin cơ bản</legend>
                <div className="row">
                  <div className="col-md-4 col-sm-6 col-xs-12">
                    <p>
                      Họ và tên:
                      <b> {StudentDetail?.currentstudent?.fullName}</b>
                    </p>
                  </div>
                  <div className="col-md-4 col-sm-6 col-xs-12">
                    <p>
                      Lớp:
                      <b> {StudentDetail?.currentstudent?.className}</b>
                    </p>
                  </div>
                  <div className="col-md-4 col-sm-6 col-xs-12">
                    <p>
                      Số buổi còn lại :
                      <b> {StudentDetail?.currentstudent?.lessonRemain}</b>
                    </p>
                  </div>
                  <div className="col-md-4 col-sm-6 col-xs-12">
                    <p>
                      Trung tâm :
                      <b> {StudentDetail?.currentstudent?.centerName}</b>
                    </p>
                  </div>
                  <div className="col-md-4 col-sm-6 col-xs-12">
                    <p>
                      Giáo trình:
                      <b> {StudentDetail?.currentstudent?.programName}</b>
                    </p>
                  </div>
                  <div className="col-md-4 col-sm-6 col-xs-12">
                    <p>
                      Trạng thái:
                      <b> {StudentDetail?.currentstudent?.status}</b>
                    </p>
                  </div>
                  <div className="col-md-4 col-sm-6 col-xs-12">
                    <Button
                      variant="primary"
                      style={{ marginRight: 15 }}
                      onClick={() => setbaoLuu(true)}
                    >
                      Bảo lưu/Đi học lại
                    </Button>
                    <Button variant="primary">Chuyển lớp</Button>
                  </div>
                </div>
              </fieldset>
            </div>
            <div className="col-12  m-t-2">
              <Nav
                fill
                variant="pills"
                className="m-b-2"
                defaultActiveKey={`/bao_luu_hv/${id}/${classId}`}
              >
                <Nav.Item>
                  <Nav.Link
                    key="diem_danh_hv"
                    href={`/diem_danh_hv/${id}/${classId}`}
                  >
                    Thảo luận Lịch học/điểm danh
                  </Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link
                    key="bao_luu_hv"
                    href={`/bao_luu_hv/${id}/${classId}`}
                  >
                    Bảo lưu
                  </Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link key="vi_hv" href={`/vi_hv/${id}/${classId}`}>
                    Thông tin ví
                  </Nav.Link>
                </Nav.Item>
              </Nav>
            </div>

            <div className="col-12">
              <div className="row ">
                <div className="col-lg-3 col-md-12 col-sm-12 text-center">
                  <UploadAvatar
                    src={StudentDetail?.currentstudent?.avatar}
                  ></UploadAvatar>
                </div>
                <div className="col-lg-9 col-md-12">
                  <CustomTable
                    dataSource={props.StudentDetail?.currentreserve}
                    total={0}
                    columns={columns}
                    onChange={data => {}}
                    noPaging
                  ></CustomTable>
                </div>
              </div>
              {showModal && <CustomModal handleClose={handleCloseModal} />}
              {baoLuu && (
                <ModalBaoLuu
                  handleClose={handleCloseModal}
                  id={id}
                  classId={classId}
                />
              )}
              {chiTiet && (
                <ModalChiTiet
                  handleClose={handleCloseModal}
                  id={id}
                  classId={classId}
                  selectedStudent={selectedStudent}
                />
              )}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default connect(({ Student, StudentDetail, Global }) => ({
  Student,
  StudentDetail,
  Global
}))(Page);
