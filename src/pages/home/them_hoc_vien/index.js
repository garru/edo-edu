import React from "react";
import { connect } from "dva";
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";
import { API_HOST } from "../../../services/serviceUri";
import { Button } from "react-bootstrap";
import PageHeader from "../../../components/PageHeader";
import UploadAvatar from "../../../components/UploadAvatar";
import { Formik, ErrorMessage } from "formik";
import Input from "../../../components/Form/Input";
import Select from "../../../components/Form/Select";
import TextArea from "../../../components/Form/TextArea";
import * as yup from "yup";
import CustomTable from "../../../components/Table";
import isEqual from "lodash/isEqual";
import cloneDeep from "lodash/cloneDeep";
import isEmpty from "lodash/isEmpty";

let timeoutSearch = "";
class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      avatarImage: "",
      listParent: [],
      listautocomplete: [],
      selectedParent: {}
    };
  }
  componentWillMount() {
    const { dispatch } = this.props;
    dispatch({
      type: "Global/showLoading"
    });
    Promise.all([
      dispatch({
        type: "Customer/listcenter",
        payload: {}
      }),
      dispatch({
        type: "Customer/listpresenter",
        payload: {}
      }),
      dispatch({
        type: "Customer/listevent",
        payload: {}
      }),
      dispatch({
        type: "Customer/listsource",
        payload: {}
      }),
      dispatch({
        type: "Customer/listprovince",
        payload: {}
      }),
      dispatch({
        type: "Customer/listgender",
        payload: {}
      }),
      dispatch({
        type: "Customer/listquanhe",
        payload: {}
      }),
      dispatch({
        type: "Customer/listtrangthai",
        payload: {}
      })
    ]).then(() => {
      dispatch({
        type: "Global/hideLoading"
      });
    });
  }

  componentWillReceiveProps(nextProps) {
    !isEqual(this.state.listautocomplete, nextProps.Student.list_hoc_sinh) &&
      this.setState({
        listautocomplete: nextProps.Student.list_hoc_sinh || []
      });
  }

  componentDidMount() {
    const listautocomplete = cloneDeep(this.props.Student.list_hoc_sinh) || [];
    this.setState({ listautocomplete });
  }

  onChangeAvatar = imageBase64 => {
    this.setState({ avatarImage: imageBase64 });
  };

  onChangeProvinceCode = id => {
    const { dispatch } = this.props;
    dispatch({
      type: "Global/showLoading"
    });
    dispatch({
      type: "Customer/listdistrict",
      payload: {
        provinceCode: id
      }
    }).then(() => {
      dispatch({
        type: "Global/hideLoading"
      });
    });
  };

  onSearch = e => {
    clearTimeout(timeoutSearch);
    const value = e.target?.value;
    timeoutSearch = setTimeout(() => {
      const { dispatch } = this.props;
      dispatch({
        type: "Global/showLoading"
      });
      dispatch({
        type: "Student/searchparent",
        payload: {
          codeOrName: value || ""
          // sortBy: "name"
        }
      }).then(() => {
        dispatch({
          type: "Global/hideLoading"
        });
      });
    }, 300);
  };

  render() {
    const bre = {
      title: "Thêm mới học viên",
      breadcrums: [
        {
          title: "Home",
          path: "/"
        },
        {
          title: "Thêm mới học viên",
          path: ""
        }
      ]
    };
    const columns = [
      {
        title: "Mã",
        key: "code",
        class: "tb-width-150"
      },
      {
        title: "Tên",
        key: "firstName",
        class: "tb-width-200"
      },
      {
        title: "Họ tên",
        key: "lastName",
        class: "tb-width-200"
      },
      {
        title: "Sinh nhật",
        key: "birthDay",
        class: "tb-width-200"
      },
      {
        title: "",
        key: "action",
        class: "tb_width_50",
        render: (col, index) => (
          <>
            <i
              className="fa fa-trash-alt"
              style={{
                marginRight: 0,
                marginLeft: "auto"
              }}
              onClick={e => {
                const newListParent = this.state.listParent.filter(
                  (item, idx) => index !== idx
                );
                this.setState({ listParent: newListParent });
              }}
            />
          </>
        )
      }
    ];

    const initialValues = {
      classify: "",
      status: "",
      relateParent: [],
      centerId: "",
      presenterId: "",
      eventId: "",
      source: "",
      facebook: "",
      provinceCode: "",
      districtCode: "",
      address: "",
      firstName: "",
      lastName: "",
      gender: 1,
      birthDate: "",
      email: "",
      phone: "",
      avatarImage: "",
      search: "",
      samePhone: false
    };
    return (
      <div>
        <PageHeader {...bre}></PageHeader>
        <div>
          <Formik
            enableReinitialize={true}
            initialValues={initialValues}
            validationSchema={yup.object().shape({
              classify: yup.string().required("Trường bắt buộc"),
              status: yup.string().required("Trường bắt buộc"),
              centerId: yup.string().required("Trường bắt buộc"),
              presenterId: yup.string().required("Trường bắt buộc"),
              eventId: yup.string().required("Trường bắt buộc"),
              source: yup.string().required("Trường bắt buộc"),
              facebook: yup.string().required("Trường bắt buộc"),
              provinceCode: yup.string().required("Trường bắt buộc"),
              districtCode: yup.string().required("Trường bắt buộc"),
              address: yup.string().required("Trường bắt buộc"),
              firstName: yup.string().required("Trường bắt buộc"),
              lastName: yup.string().required("Trường bắt buộc"),
              birthDate: yup.string().required("Trường bắt buộc"),
              email: yup
                .string()
                .required("Trường bắt buộc")
                .matches(
                  /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                  "Email không hợp lệ"
                ),
              phone: yup
                .string()
                .matches(
                  /(84|0[3|5|7|8|9])+([0-9]{8})\b/g,
                  "Số điện thoại không hợp lệ"
                )
                .when(["samePhone"], (samePhone, schema) => {
                  return samePhone ? schema : schema.required("Trường bắt buộc");
                })
            })}
            onSubmit={async (values, { resetForm }) => {
              const { dispatch } = this.props;
              const submitData = {
                classify: +values.classify,
                status: +values.status,
                relateParent: this.state.listParent?.map(
                  ({ id, code, firstName, lastName, birthDate }) => {
                    return {
                      id,
                      code,
                      firstName,
                      lastName,
                      birthDate: birthDate || moment().format("YYYY-MM-DD")
                    };
                  }
                ),
                centerId: +values.centerId,
                presenterId: +values.presenterId,
                eventId: +values.eventId,
                source: +values.source,
                facebook: values.facebook,
                provinceCode: values.provinceCode,
                districtCode: values.districtCode,
                address: values.address,
                firstName: values.firstName,
                lastName: values.lastName,
                gender: +values.gender,
                birthDate: values.birthDate,
                email: values.email,
                phone: values.samePhone ? null : values.phone
              };
              console.log(submitData);
              dispatch({
                type: "Global/showLoading"
              });
              dispatch({
                type: "Customer/insertstudent",
                payload: submitData
              }).then(data => {
                dispatch({
                  type: "Global/hideLoading"
                });
                if (data && !data.code) {
                  dispatch({
                    type: "Global/showSuccess"
                  });
                  resetForm && resetForm(initialValues);
                  // window.location.reload();
                } else {
                  dispatch({
                    type: "Global/showError"
                  });
                }
              });
            }}
          >
            {({
              values,
              errors,
              touched,
              handleChange,
              handleBlur,
              handleSubmit,
              isSubmitting,
              setFieldValue,
              resetForm
              /* and other goodies */
            }) => (
              <form onSubmit={handleSubmit}>
                <div className="iq-card">
                  <div className=" iq-card-header d-flex justify-content-between">
                    <div className="iq-header-title">
                      <h6>
                        <b>Thông tin cơ bản</b>
                      </h6>
                    </div>
                  </div>
                  <div className="iq-card-body">
                    <div className="row ">
                      <div className="col-lg-3 col-md-4 col-sm-12 text-center">
                        <div className="profile-img-edit m-b-2">
                          <img
                            alt="profile-pic"
                            className="profile-pic"
                            src={"images/user/11.png"}
                          />
                          <div className="p-image">
                            <i className="fas fa-camera upload-button"></i>
                            <input
                              accept="image/*"
                              className="file-upload"
                              type="file"
                            />
                          </div>
                        </div>
                      </div>

                      <div className="col-lg-9 col-md-8 col-sm-12">
                        <div className="row">
                          <div className="col-lg-4 col-md-6 col-sm-12">
                            <div className="form-group">
                              <Select
                                id="classify"
                                name="classify"
                                label="Quan hệ"
                                options={this.props.Customer?.list_quan_he}
                                value={values.classify}
                                onChange={handleChange}
                                onBlur={handleBlur}
                              />
                            </div>
                          </div>
                          <div className="col-lg-4 col-md-6 col-sm-12">
                            <div className="form-group">
                              <Select
                                id="status"
                                name="status"
                                label="Trạng thái"
                                options={this.props.Customer?.list_trang_thai}
                                value={values.status}
                                onChange={handleChange}
                                onBlur={handleBlur}
                              />
                            </div>
                          </div>
                          <div className="col-lg-4 col-md-6 col-sm-12">
                            <div className="form-group">
                              <Select
                                id="centerId"
                                name="centerId"
                                label="Cơ sở"
                                options={this.props.Customer?.list_center}
                                value={values.centerId}
                                onChange={handleChange}
                                onBlur={handleBlur}
                              />
                            </div>
                          </div>
                          <div className="col-lg-4 col-md-6 col-sm-12">
                            <div className="form-group">
                              <Input
                                id="firstName"
                                name="firstName"
                                label="Tên:"
                                value={values.firstName}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                placeholder={"Nhập tên"}
                              />
                            </div>
                          </div>
                          <div className="col-lg-4 col-md-6 col-sm-12">
                            <div className="form-group">
                              <Input
                                id="lastName"
                                name="lastName"
                                label="Họ đệm:"
                                value={values.lastName}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                placeholder={"Nhập họ đệm"}
                              />
                            </div>
                          </div>
                          <div className="col-lg-4 col-md-6 col-sm-12">
                            <div className="form-group">
                              <Input
                                id="email"
                                name="email"
                                label="Email:"
                                value={values.email}
                                type={"email"}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                placeholder={"Nhập email"}
                              />
                            </div>
                          </div>

                          <div className="col-lg-4 col-md-6 col-sm-12">
                            <div className="form-group m-0">
                              <div className="d-flex align-items-center">
                                <Input
                                  id="phone"
                                  name="phone"
                                  label="Số điện thoại:"
                                  type={"tel"}
                                  value={values.phone}
                                  onChange={handleChange}
                                  onBlur={handleBlur}
                                  placeholder={"Nhập số điện thoại"}
                                />
                                <div className="btn-icon ml-2">
                                  <i className="fa fa-plus" />
                                </div>
                              </div>
                            </div>
                            <div className="form-group">
                              <input
                                type="checkbox"
                                name="samePhone"
                                id="samePhone"
                                value={values.samePhone}
                                onChange={handleChange}
                                onBlur={handleBlur}
                              />
                              <label htmlFor="samePhone">
                                &nbsp;&nbsp; Dùng chung sdt với phụ huynh
                              </label>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="iq-card">
                  <div className=" iq-card-header d-flex justify-content-between">
                    <div className="iq-header-title">
                      <h6>
                        <b>Thông tin bổ sung</b>
                      </h6>
                    </div>
                  </div>
                  <div className="iq-card-body">
                    <div className="row">
                      <div className="col-lg-4 col-md-6 col-sm-12">
                        <div className="form-group">
                          <Select
                            id="presenterId"
                            name="presenterId"
                            label="Người giới thiệu"
                            options={this.props.Customer?.list_nguoi_gioi_thieu}
                            value={values.presenterId}
                            onChange={handleChange}
                            onBlur={handleBlur}
                          />
                        </div>
                      </div>
                      <div className="col-lg-4 col-md-6 col-sm-12">
                        <div className="form-group">
                          <Select
                            id="eventId"
                            name="eventId"
                            label="Sự kiện"
                            options={this.props.Customer?.list_su_kien}
                            value={values.eventId}
                            onChange={handleChange}
                            onBlur={handleBlur}
                          />
                        </div>
                      </div>
                      <div className="col-lg-4 col-md-6 col-sm-12">
                        <div className="form-group">
                          <div className="p-0">
                            <label>Giới tính:</label>
                          </div>
                          <div className="row">
                            {this.props.Customer?.list_gender?.map(gender => (
                              <div
                                className="col-md-6 col-sm-12"
                                key={`gender_${gender.code}`}
                              >
                                <div className="custom-control custom-radio">
                                  <input
                                    className="custom-control-input"
                                    id={gender.code}
                                    name="gender"
                                    type="radio"
                                    value={gender.id}
                                    checked={values.gender === gender.id}
                                    onChange={() =>
                                      setFieldValue("gender", gender.id)
                                    }
                                    onBlur={handleBlur}
                                  />
                                  <label
                                    className="custom-control-label"
                                    htmlFor={gender.code}
                                  >
                                    {gender.name}
                                  </label>
                                </div>
                              </div>
                            ))}
                          </div>
                        </div>
                      </div>
                      <div className="col-lg-4 col-md-6 col-sm-12">
                        <div className="form-group">
                          <Select
                            id="source"
                            name="source"
                            label="Nguồn từ"
                            options={this.props.Customer?.list_nguon}
                            value={values.source}
                            onChange={handleChange}
                            onBlur={handleBlur}
                          />
                        </div>
                      </div>
                      <div className="col-lg-4 col-md-6 col-sm-12">
                        <div className="form-group">
                          <label>Ngày sinh:</label>
                          <input
                            type="date"
                            className="form-control form-control-lg"
                            name="birthDate"
                            id="birthDate"
                            max={moment().format("YYYY-MM-DD")}
                            onChange={handleChange}
                            onBlur={handleBlur}
                          />
                        </div>
                      </div>
                      <div className="col-lg-4 col-md-6 col-sm-12">
                        <div className="form-group">
                          <Input
                            id="facebook"
                            name="facebook"
                            label="Facebook:"
                            value={values.facebook}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            placeholder={"Nhập facebook"}
                          />
                        </div>
                      </div>
                      <div className="col-lg-4 col-md-6 col-sm-12">
                        <div className="form-group">
                          <Select
                            id="provinceCode"
                            name="provinceCode"
                            label="Thành phố"
                            options={this.props.Customer?.list_province}
                            value={values.provinceCode}
                            onChange={e => {
                              console.log(e);
                              setFieldValue("provinceCode", e.target.value);
                              this.onChangeProvinceCode(e.target.value);
                            }}
                            onBlur={handleBlur}
                          />
                        </div>
                      </div>
                      <div className="col-lg-4 col-md-6 col-sm-12">
                        <div className="form-group">
                          <Select
                            id="districtCode"
                            name="districtCode"
                            label="Quận huyện"
                            options={this.props.Customer?.list_district}
                            value={values.districtCode}
                            onChange={handleChange}
                            onBlur={handleBlur}
                          />
                        </div>
                      </div>
                      <div className="col-lg-4 col-md-6 col-sm-12">
                        <div className="form-group">
                          <TextArea
                            id="address"
                            name="address"
                            label="Địa chỉ cụ thể:"
                            rows="4"
                            value={values.address}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            placeholder="Nhập địa chỉ cụ thể"
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="iq-card">
                  <div className=" iq-card-header d-flex justify-content-between">
                    <div className="iq-header-title">
                      <h6>
                        <b>Thêm mới phụ huynh liên quan</b>
                      </h6>
                    </div>
                  </div>
                  <div className="iq-card-body">
                    <div className="row">
                      <div className="col-lg-6 col-md-6 col-sm-12">
                        <div className="row align-center">
                          <div className="col-10">
                            <Input
                              id="search"
                              name="search"
                              label="Từ danh sách đã có:"
                              value={values.search}
                              onChange={e => {
                                setFieldValue("search", e.target.value);
                                this.onSearch(e);
                              }}
                              listautocomplete={this.state.listautocomplete}
                              onBlur={handleBlur}
                              customkey={["firstName", "lastName"]}
                              placeholder={"Nhập tên/Mã học viên/ Phụ huynh"}
                              onPicked={data => {
                                this.setState({
                                  selectedParent: data,
                                  listautocomplete: []
                                });
                                setFieldValue("search", "");
                              }}
                            />
                          </div>
                          <div className="col-2">
                            <div className="btn-icon">
                              <i className="fa fa-plus" />
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="col-lg-6 col-md-6 col-sm-0"></div>
                      <div className="col-lg-4 col-md-6 col-sm-12">
                        <div className="form-group">
                          <label htmlFor="ten">Tên</label>
                          <input
                            type="text"
                            id="ten"
                            name="ten"
                            disabled
                            className="form-control form-control-lg"
                            value={this.state.selectedParent.firstName || ""}
                          />
                        </div>
                      </div>
                      <div className="col-lg-4 col-md-6 col-sm-12">
                        <div className="form-group">
                          <label htmlFor="hodem">Họ đệm</label>
                          <input
                            type="text"
                            id="hodem"
                            name="hodem"
                            disabled
                            className="form-control form-control-lg"
                            value={this.state.selectedParent.lastName || ""}
                          />
                        </div>
                      </div>
                      <div className="col-lg-4 col-md-6 col-sm-12">
                        <div className="form-group">
                          <label htmlFor="sinh_nhat">Sinh nhật:</label>
                          <input
                            type="text"
                            id="sinhnhat"
                            name="sinhnhat"
                            disabled
                            className="form-control form-control-lg"
                            value={
                              this.state.selectedParent.birthDate
                                ? moment(
                                    this.state.selectedParent.birthDate
                                  ).format("YYYY-MM-DD")
                                : ""
                            }
                          />
                        </div>
                      </div>
                      <div className="col-lg-4 col-md-6 col-sm-12">
                        <div className="form-group">
                          <label htmlFor="so_dien_thoai">Số điện thoại</label>
                          <div className="d-flex align-items-center">
                            <input
                              type="text"
                              id="so_dien_thoai"
                              name="so_dien_thoai"
                              disabled
                              className="form-control form-control-lg"
                              value={this.state.selectedParent.phone || ""}
                            />
                            <div className="btn-icon ml-2">
                              <i className="fa fa-plus" />
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="col-lg-4 col-md-6 col-sm-12">
                        <div className="form-group">
                          <label htmlFor="email">Email</label>
                          <input
                            type="text"
                            id="email"
                            name="email"
                            disabled
                            className="form-control form-control-lg"
                            value={this.state.selectedParent.email || ""}
                          />
                        </div>
                      </div>
                      <div className="col-12 text-right m-b-2">
                        <Button
                          variant="primary"
                          style={{ marginRight: 15 }}
                          onClick={() => {
                            if (!isEmpty(this.state.selectedParent)) {
                              const currentListParent = cloneDeep(
                                this.state.listParent
                              );
                              currentListParent.push(this.state.selectedParent);
                              this.setState({
                                listParent: currentListParent,
                                selectedParent: {}
                              });
                            }
                          }}
                        >
                          Thêm
                        </Button>
                      </div>
                      <div className="col-12 ">
                        <CustomTable
                          dataSource={this.state.listParent}
                          columns={columns}
                          onChange={data => {}}
                          noPaging
                        ></CustomTable>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="text-right m-b-3">
                  <Button
                    variant="primary"
                    style={{ marginRight: 15 }}
                    type="submit"
                  >
                    Tạo và tạo hóa đơn
                  </Button>
                  <Button
                    variant="primary"
                    style={{ marginRight: 15 }}
                    type="submit"
                  >
                    Tạo
                  </Button>
                </div>
              </form>
            )}
          </Formik>
        </div>
      </div>
    );
  }
}

export default connect(({ Customer, Center, User, Global, Student }) => ({
  Customer,
  Center,
  User,
  Global,
  Student
}))(Page);
