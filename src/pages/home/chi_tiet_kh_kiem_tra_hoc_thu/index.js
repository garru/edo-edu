import React from "react";
import { Nav } from "react-bootstrap";
import { connect } from "dva";
import "react-datepicker/dist/react-datepicker.css";

const Page = () => {
  return (
    <div className="iq-card ">
      <div className="iq-card-body">
        <div className="">
          <div className="row">
            <div className="col-12">
              <Nav fill variant="pills" className="m-b-2" defaultActiveKey="/kiem_tra_hoc_thu">
                <Nav.Item>
                  <Nav.Link href="/trao_doi">Thảo luận</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link href="/cham_soc">Lịch sử chăm sóc</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link href="/hoa_don">Hóa đơn</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link href="/dd_nhan_xet">Điểm danh/ nhận xét</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link href="/lich_su_hoc">Lịch sử khác</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link href="/kiem_tra_hoc_thu">Kiểm tra/học thử</Nav.Link>
                </Nav.Item>
              </Nav>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default connect(({ Mock }) => ({
  Mock
}))(Page);
