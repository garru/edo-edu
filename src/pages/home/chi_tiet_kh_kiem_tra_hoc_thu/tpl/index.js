import React from 'react';
import './index.css';
import './ax.css';

class Page extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
		<div class="" id="base" style={{"margin-left":"-150px"}}>
		   <div class="ax_default shape" id="u11490">
		      <div class="" id="u11490_div">
		      </div>
		      <div class="text" id="u11490_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default shape" id="u11491">
		      <div class="" id="u11491_div">
		      </div>
		      <div class="text" id="u11491_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default" data-height="40" data-label="account" data-left="13" data-top="895" data-width="192" id="u11493">
		      <div class="ax_default shape" data-label="accountLable" id="u11494">
		         <img class="img" id="u11494_img" src="images/account_information/accountlable_u55.svg"/>
		         <div class="text" id="u11494_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Nguyen Van A
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u11495">
		         <img class="img" id="u11495_img" src="images/login/u4.svg"/>
		         <div class="text" id="u11495_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="settingMenu" data-left="11" data-top="826" data-width="125" id="u11496">
		      <div class="ax_default shape" data-label="gearIconLable" id="u11497">
		         <img class="img" id="u11497_img" src="images/account_information/geariconlable_u58.svg"/>
		         <div class="text" id="u11497_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Cài đặt
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="44" data-left="11" data-top="826" data-width="44" id="u11498">
		         <div class="ax_default shape" data-label="gearIconBG" id="u11499">
		            <div class="" id="u11499_div">
		            </div>
		            <div class="text" id="u11499_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default shape" data-label="gearIconBG2" id="u11500">
		            <div class="" id="u11500_div">
		            </div>
		            <div class="text" id="u11500_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" data-label="gearIcon" id="u11501">
		            <img class="img" id="u11501_img" src="images/account_information/gearicon_u62.svg"/>
		            <div class="text" id="u11501_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="customerMenu" data-left="11" data-top="145" data-width="143" id="u11502">
		      <div class="ax_default shape" data-label="customerIconLable" id="u11503">
		         <img class="img" id="u11503_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u11503_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Khách hàng
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="customerIconBG" id="u11504">
		         <div class="" id="u11504_div">
		         </div>
		         <div class="text" id="u11504_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="customerIconBG2" id="u11505">
		         <div class="" id="u11505_div">
		         </div>
		         <div class="text" id="u11505_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" data-label="customer" id="u11506">
		         <img class="img" id="u11506_img" src="images/account_information/customer_u67.svg"/>
		         <div class="text" id="u11506_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="classMenu" data-left="11" data-top="198" data-width="143" id="u11507">
		      <div class="ax_default shape" data-label="classIconLable" id="u11508">
		         <img class="img" id="u11508_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u11508_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Lớp học
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG" id="u11509">
		         <div class="" id="u11509_div">
		         </div>
		         <div class="text" id="u11509_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG2" id="u11510">
		         <div class="" id="u11510_div">
		         </div>
		         <div class="text" id="u11510_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" data-label="classicon" id="u11511">
		         <img class="img" id="u11511_img" src="images/account_information/classicon_u72.svg"/>
		         <div class="text" id="u11511_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="dashboardMenu" data-left="11" data-top="91" data-width="143" id="u11512">
		      <div class="ax_default shape" data-label="dashboardIconLable" id="u11513">
		         <img class="img" id="u11513_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u11513_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Dashnoard
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="dashboardIconBG" id="u11514">
		         <div class="" id="u11514_div">
		         </div>
		         <div class="text" id="u11514_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="dashboardIconBG2" id="u11515">
		         <div class="" id="u11515_div">
		         </div>
		         <div class="text" id="u11515_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" data-label="dashboard" id="u11516">
		         <img class="img" id="u11516_img" src="images/account_information/dashboard_u77.svg"/>
		         <div class="text" id="u11516_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="129" data-label="AccountMenu" data-left="58" data-top="806" data-width="246" id="u11517" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default shape" id="u11518">
		         <div class="" id="u11518_div">
		         </div>
		         <div class="text" id="u11518_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u11519">
		         <div class="" id="u11519_div">
		         </div>
		         <div class="text" id="u11519_text">
		            <p>
		               <span style={{"font-family":"ArialMT, Arial","font-weight":"400","text-decoration":"none"}}>
		                  Xin chào:
		               </span>
		               <span style={{"font-family":"Arial Bold, Arial","font-weight":"700","text-decoration":"none"}}>
		                  Nguyen Van A
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11520">
		         <div class="ax_default image" id="u11521">
		            <img class="img" id="u11521_img" src="images/account_information/u82.png"/>
		            <div class="text" id="u11521_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u11522">
		            <div class="" id="u11522_div">
		            </div>
		            <div class="text" id="u11522_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Đăng xuất
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default line" id="u11523">
		         <img class="img" id="u11523_img" src="images/account_information/u84.svg"/>
		         <div class="text" id="u11523_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11524">
		         <div class="ax_default paragraph" id="u11525">
		            <div class="" id="u11525_div">
		            </div>
		            <div class="text" id="u11525_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tài khoản
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default image" id="u11526">
		            <img class="img" id="u11526_img" src="images/account_information/u87.png"/>
		            <div class="text" id="u11526_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="339" data-label="SettingPannel" data-left="58" data-top="525" data-width="339" id="u11527" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default shape" id="u11528">
		         <div class="" id="u11528_div">
		         </div>
		         <div class="text" id="u11528_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u11529">
		         <div class="" id="u11529_div">
		         </div>
		         <div class="text" id="u11529_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Hệ thống
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11530">
		         <div class="ax_default icon" id="u11531">
		            <img class="img" id="u11531_img" src="images/account_information/u92.svg"/>
		            <div class="text" id="u11531_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u11532">
		            <div class="" id="u11532_div">
		            </div>
		            <div class="text" id="u11532_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tài khoản
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11533">
		         <div class="ax_default paragraph" id="u11534">
		            <div class="" id="u11534_div">
		            </div>
		            <div class="text" id="u11534_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phân quyền
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u11535">
		            <img class="img" id="u11535_img" src="images/account_information/u96.svg"/>
		            <div class="text" id="u11535_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u11536">
		         <div class="" id="u11536_div">
		         </div>
		         <div class="text" id="u11536_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Đào tạo
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11537">
		         <div class="ax_default paragraph" id="u11538">
		            <div class="" id="u11538_div">
		            </div>
		            <div class="text" id="u11538_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Thương hiệu
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u11539">
		            <img class="img" id="u11539_img" src="images/account_information/u100.svg"/>
		            <div class="text" id="u11539_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11540">
		         <div class="ax_default paragraph" id="u11541">
		            <div class="" id="u11541_div">
		            </div>
		            <div class="text" id="u11541_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trung tâm
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u11542">
		            <img class="img" id="u11542_img" src="images/account_information/u103.svg"/>
		            <div class="text" id="u11542_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11543">
		         <div class="ax_default icon" id="u11544">
		            <img class="img" id="u11544_img" src="images/account_information/u105.svg"/>
		            <div class="text" id="u11544_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u11545">
		            <div class="" id="u11545_div">
		            </div>
		            <div class="text" id="u11545_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Ngày nghỉ
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11546">
		         <div class="ax_default icon" id="u11547">
		            <img class="img" id="u11547_img" src="images/account_information/u108.svg"/>
		            <div class="text" id="u11547_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u11548">
		            <div class="" id="u11548_div">
		            </div>
		            <div class="text" id="u11548_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phòng ban - Vai trò
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11549">
		         <div class="ax_default paragraph" id="u11550">
		            <div class="" id="u11550_div">
		            </div>
		            <div class="text" id="u11550_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trạng thái học viên
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u11551">
		            <img class="img" id="u11551_img" src="images/account_information/u112.svg"/>
		            <div class="text" id="u11551_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11552">
		         <div class="ax_default paragraph" id="u11553">
		            <div class="" id="u11553_div">
		            </div>
		            <div class="text" id="u11553_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tiêu chí đánh giá
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u11554">
		            <img class="img" id="u11554_img" src="images/account_information/u115.svg"/>
		            <div class="text" id="u11554_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11555">
		         <div class="ax_default paragraph" id="u11556">
		            <div class="" id="u11556_div">
		            </div>
		            <div class="text" id="u11556_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Chương trình học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11557">
		            <div class="ax_default icon" id="u11558">
		               <img class="img" id="u11558_img" src="images/account_information/u119.svg"/>
		               <div class="text" id="u11558_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11559">
		         <div class="ax_default icon" id="u11560">
		            <img class="img" id="u11560_img" src="images/account_information/u121.svg"/>
		            <div class="text" id="u11560_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u11561">
		            <div class="" id="u11561_div">
		            </div>
		            <div class="text" id="u11561_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Bài học
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11562">
		         <div class="ax_default paragraph" id="u11563">
		            <div class="" id="u11563_div">
		            </div>
		            <div class="text" id="u11563_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phòng học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u11564">
		            <img class="img" id="u11564_img" src="images/account_information/u125.svg"/>
		            <div class="text" id="u11564_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11565">
		         <div class="ax_default paragraph" id="u11566">
		            <div class="" id="u11566_div">
		            </div>
		            <div class="text" id="u11566_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trường học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u11567">
		            <img class="img" id="u11567_img" src="images/account_information/u128.svg"/>
		            <div class="text" id="u11567_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default line" id="u11568">
		         <img class="img" id="u11568_img" src="images/account_information/u129.svg"/>
		         <div class="text" id="u11568_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u11569">
		         <div class="" id="u11569_div">
		         </div>
		         <div class="text" id="u11569_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Tài chính
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11570">
		         <div class="ax_default paragraph" id="u11571">
		            <div class="" id="u11571_div">
		            </div>
		            <div class="text" id="u11571_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Học phí
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u11572">
		            <img class="img" id="u11572_img" src="images/account_information/u133.svg"/>
		            <div class="text" id="u11572_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11573">
		         <div class="ax_default paragraph" id="u11574">
		            <div class="" id="u11574_div">
		            </div>
		            <div class="text" id="u11574_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Khuyến mại
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u11575">
		            <img class="img" id="u11575_img" src="images/account_information/u136.svg"/>
		            <div class="text" id="u11575_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="classMenu" data-left="11" data-top="252" data-width="143" id="u11576">
		      <div class="ax_default shape" data-label="classIconLable" id="u11577">
		         <img class="img" id="u11577_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u11577_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Giáo viên
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG" id="u11578">
		         <div class="" id="u11578_div">
		         </div>
		         <div class="text" id="u11578_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG2" id="u11579">
		         <div class="" id="u11579_div">
		         </div>
		         <div class="text" id="u11579_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u11580">
		         <img class="img" id="u11580_img" src="images/account_information/u141.svg"/>
		         <div class="text" id="u11580_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u11581">
		      <img class="img" id="u11581_img" src="images/account_information/u142.svg"/>
		      <div class="text" id="u11581_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u11582">
		      <img class="img" id="u11582_img" src="images/account_information/u143.svg"/>
		      <div class="text" id="u11582_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div id="u11489" style={{"display":"none","visibility":"hidden"}}>
		   </div>
		   <div class="ax_default paragraph" id="u11583">
		      <div class="" id="u11583_div">
		      </div>
		      <div class="text" id="u11583_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Chi tiết chăm sóc khách hàng
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default shape" id="u11584">
		      <div class="" id="u11584_div">
		      </div>
		      <div class="text" id="u11584_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default" id="u11585">
		      <div class="ax_default table_cell" id="u11586">
		         <img class="img" id="u11586_img" src="images/chi_ti_t_kh-trao___i/u11177.png"/>
		         <div class="text" id="u11586_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Thảo luận
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11587">
		         <img class="img" id="u11587_img" src="images/chi_ti_t_kh-trao___i/u11177.png"/>
		         <div class="text" id="u11587_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Lịch sử chăm sóc
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11588">
		         <img class="img" id="u11588_img" src="images/chi_ti_t_kh-trao___i/u11177.png"/>
		         <div class="text" id="u11588_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Hóa đơn
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11589">
		         <img class="img" id="u11589_img" src="images/chi_ti_t_kh-trao___i/u11176.png"/>
		         <div class="text" id="u11589_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Điểm danh/ nhận xét
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11590">
		         <img class="img" id="u11590_img" src="images/chi_ti_t_kh-trao___i/u11181.png"/>
		         <div class="text" id="u11590_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Lịch sử học
		               </span>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default shape" id="u11591">
		      <img class="img" id="u11591_img" src="images/chi_ti_t_kh-trao___i/u11182.svg"/>
		      <div class="text" id="u11591_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default" data-height="128" data-left="1581" data-top="130" data-width="178" id="u11592">
		      <div class="ax_default label" id="u11593">
		         <div class="" id="u11593_div">
		         </div>
		         <div class="text" id="u11593_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Hoàng Văn Huy
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default label" id="u11594">
		         <div class="" id="u11594_div">
		         </div>
		         <div class="text" id="u11594_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  HV-000002
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default label" id="u11595">
		         <div class="" id="u11595_div">
		         </div>
		         <div class="text" id="u11595_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  0384455522
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default label" id="u11596">
		         <div class="" id="u11596_div">
		         </div>
		         <div class="text" id="u11596_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  test@qa/team
		               </span>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default shape" id="u11597">
		      <div class="" id="u11597_div">
		      </div>
		      <div class="text" id="u11597_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default" id="u11598">
		      <div class="ax_default table_cell" id="u11599">
		         <img class="img" id="u11599_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11599.png"/>
		         <div class="text" id="u11599_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Tên buổi học
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11600">
		         <img class="img" id="u11600_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11600.png"/>
		         <div class="text" id="u11600_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Ngày học
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11601">
		         <img class="img" id="u11601_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11601.png"/>
		         <div class="text" id="u11601_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Giờ học
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11602">
		         <img class="img" id="u11602_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11602.png"/>
		         <div class="text" id="u11602_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Phòng học
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11603">
		         <img class="img" id="u11603_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11603.png"/>
		         <div class="text" id="u11603_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Giáo viên Việt
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11604">
		         <img class="img" id="u11604_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11604.png"/>
		         <div class="text" id="u11604_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Giáo viên NN
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11605">
		         <img class="img" id="u11605_img" src="images/k_nh_thanh_to_n/u4099.png"/>
		         <div class="text" id="u11605_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Trạng thái
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11606">
		         <img class="img" id="u11606_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11606.png"/>
		         <div class="text" id="u11606_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Thử
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11607">
		         <img class="img" id="u11607_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11607.png"/>
		         <div class="text" id="u11607_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11608">
		         <img class="img" id="u11608_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11608.png"/>
		         <div class="text" id="u11608_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Introduction-Greetings
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11609">
		         <img class="img" id="u11609_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11609.png"/>
		         <div class="text" id="u11609_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  11/01/2021
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11610">
		         <img class="img" id="u11610_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11610.png"/>
		         <div class="text" id="u11610_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  17:30
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11611">
		         <img class="img" id="u11611_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11611.png"/>
		         <div class="text" id="u11611_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Mickey
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11612">
		         <img class="img" id="u11612_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11612.png"/>
		         <div class="text" id="u11612_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Nguyễn Hoàng
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11613">
		         <img class="img" id="u11613_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11613.png"/>
		         <div class="text" id="u11613_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Peter
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11614">
		         <img class="img" id="u11614_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11614.png"/>
		         <div class="text" id="u11614_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11615">
		         <img class="img" id="u11615_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11615.png"/>
		         <div class="text" id="u11615_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11616">
		         <img class="img" id="u11616_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11616.png"/>
		         <div class="text" id="u11616_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11617">
		         <img class="img" id="u11617_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11617.png"/>
		         <div class="text" id="u11617_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Introduction-Greetings
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11618">
		         <img class="img" id="u11618_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11618.png"/>
		         <div class="text" id="u11618_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  11/01/2021
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11619">
		         <img class="img" id="u11619_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11619.png"/>
		         <div class="text" id="u11619_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  17:30
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11620">
		         <img class="img" id="u11620_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11620.png"/>
		         <div class="text" id="u11620_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Mickey
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11621">
		         <img class="img" id="u11621_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11621.png"/>
		         <div class="text" id="u11621_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Nguyễn Hoàng
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11622">
		         <img class="img" id="u11622_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11622.png"/>
		         <div class="text" id="u11622_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Peter
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11623">
		         <img class="img" id="u11623_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11623.png"/>
		         <div class="text" id="u11623_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11624">
		         <img class="img" id="u11624_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11624.png"/>
		         <div class="text" id="u11624_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11625">
		         <img class="img" id="u11625_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11625.png"/>
		         <div class="text" id="u11625_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11626">
		         <img class="img" id="u11626_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11617.png"/>
		         <div class="text" id="u11626_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Introduction-Greetings
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11627">
		         <img class="img" id="u11627_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11618.png"/>
		         <div class="text" id="u11627_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  11/01/2021
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11628">
		         <img class="img" id="u11628_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11619.png"/>
		         <div class="text" id="u11628_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  17:30
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11629">
		         <img class="img" id="u11629_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11620.png"/>
		         <div class="text" id="u11629_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Mickey
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11630">
		         <img class="img" id="u11630_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11621.png"/>
		         <div class="text" id="u11630_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Nguyễn Hoàng
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11631">
		         <img class="img" id="u11631_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11622.png"/>
		         <div class="text" id="u11631_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Peter
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11632">
		         <img class="img" id="u11632_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11623.png"/>
		         <div class="text" id="u11632_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11633">
		         <img class="img" id="u11633_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11624.png"/>
		         <div class="text" id="u11633_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11634">
		         <img class="img" id="u11634_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11625.png"/>
		         <div class="text" id="u11634_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11635">
		         <img class="img" id="u11635_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11617.png"/>
		         <div class="text" id="u11635_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Introduction-Greetings
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11636">
		         <img class="img" id="u11636_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11618.png"/>
		         <div class="text" id="u11636_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  11/01/2021
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11637">
		         <img class="img" id="u11637_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11619.png"/>
		         <div class="text" id="u11637_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  17:30
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11638">
		         <img class="img" id="u11638_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11620.png"/>
		         <div class="text" id="u11638_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Mickey
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11639">
		         <img class="img" id="u11639_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11621.png"/>
		         <div class="text" id="u11639_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Nguyễn Hoàng
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11640">
		         <img class="img" id="u11640_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11622.png"/>
		         <div class="text" id="u11640_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Peter
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11641">
		         <img class="img" id="u11641_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11623.png"/>
		         <div class="text" id="u11641_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11642">
		         <img class="img" id="u11642_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11624.png"/>
		         <div class="text" id="u11642_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11643">
		         <img class="img" id="u11643_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11625.png"/>
		         <div class="text" id="u11643_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11644">
		         <img class="img" id="u11644_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11617.png"/>
		         <div class="text" id="u11644_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Introduction-Greetings
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11645">
		         <img class="img" id="u11645_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11618.png"/>
		         <div class="text" id="u11645_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  11/01/2021
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11646">
		         <img class="img" id="u11646_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11619.png"/>
		         <div class="text" id="u11646_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  17:30
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11647">
		         <img class="img" id="u11647_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11620.png"/>
		         <div class="text" id="u11647_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Mickey
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11648">
		         <img class="img" id="u11648_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11621.png"/>
		         <div class="text" id="u11648_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Nguyễn Hoàng
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11649">
		         <img class="img" id="u11649_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11622.png"/>
		         <div class="text" id="u11649_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Peter
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11650">
		         <img class="img" id="u11650_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11623.png"/>
		         <div class="text" id="u11650_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11651">
		         <img class="img" id="u11651_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11624.png"/>
		         <div class="text" id="u11651_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11652">
		         <img class="img" id="u11652_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11625.png"/>
		         <div class="text" id="u11652_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11653">
		         <img class="img" id="u11653_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11653.png"/>
		         <div class="text" id="u11653_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Kiểm tra
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11654">
		         <img class="img" id="u11654_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11654.png"/>
		         <div class="text" id="u11654_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  11/01/2021
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11655">
		         <img class="img" id="u11655_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11655.png"/>
		         <div class="text" id="u11655_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  17:30
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11656">
		         <img class="img" id="u11656_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11656.png"/>
		         <div class="text" id="u11656_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Mickey
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11657">
		         <img class="img" id="u11657_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11657.png"/>
		         <div class="text" id="u11657_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Nguyễn Hoàng
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11658">
		         <img class="img" id="u11658_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11658.png"/>
		         <div class="text" id="u11658_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11659">
		         <img class="img" id="u11659_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11659.png"/>
		         <div class="text" id="u11659_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11660">
		         <img class="img" id="u11660_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11660.png"/>
		         <div class="text" id="u11660_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default table_cell" id="u11661">
		         <img class="img" id="u11661_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11661.png"/>
		         <div class="text" id="u11661_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default label" id="u11662">
		      <div class="" id="u11662_div">
		      </div>
		      <div class="text" id="u11662_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Con của phụ huynh
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default droplist" id="u11663">
		      <div class="" id="u11663_div">
		      </div>
		      <select class="u11663_input" id="u11663_input">
		      </select>
		   </div>
		   <div class="ax_default label" id="u11664">
		      <div class="" id="u11664_div">
		      </div>
		      <div class="text" id="u11664_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Lớp học tham gia
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default droplist" id="u11665">
		      <div class="" id="u11665_div">
		      </div>
		      <select class="u11665_input" id="u11665_input">
		      </select>
		   </div>
		   <div class="ax_default primary_button" id="u11666">
		      <div class="" id="u11666_div">
		      </div>
		      <div class="text" id="u11666_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Lọc
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default shape" id="u11667">
		      <div class="" id="u11667_div">
		      </div>
		      <div class="text" id="u11667_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Có mặt
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default shape" id="u11668">
		      <div class="" id="u11668_div">
		      </div>
		      <div class="text" id="u11668_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Nghỉ đã học bù
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default shape" id="u11669">
		      <div class="" id="u11669_div">
		      </div>
		      <div class="text" id="u11669_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Nghỉ không phép
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default shape" id="u11670">
		      <div class="" id="u11670_div">
		      </div>
		      <div class="text" id="u11670_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Có mặt
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u11671">
		      <img class="img" id="u11671_img" src="images/th_i_gian_r_nh-list/u3423.svg"/>
		      <div class="text" id="u11671_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="950" data-label="nhận xét" data-left="0" data-top="0" data-width="1920" id="u11672" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default" data-height="0" data-label="Nhận xét" data-left="0" data-top="0" data-width="0" id="u11673">
		         <div class="ax_default shape" id="u11674">
		            <div class="" id="u11674_div">
		            </div>
		            <div class="text" id="u11674_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11675">
		            <div class="ax_default shape" id="u11676">
		               <div class="" id="u11676_div">
		               </div>
		               <div class="text" id="u11676_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default paragraph" id="u11677">
		               <div class="" id="u11677_div">
		               </div>
		               <div class="text" id="u11677_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Nhận xét học viên Bùi Văn Tích
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default" id="u11678">
		               <div class="ax_default table_cell" id="u11679">
		                  <img class="img" id="u11679_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11679.png"/>
		                  <div class="text" id="u11679_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Kỹ năng - Tiêu chí
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u11680">
		                  <img class="img" id="u11680_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11680.png"/>
		                  <div class="text" id="u11680_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Đánh giá
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u11681">
		                  <img class="img" id="u11681_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11681.png"/>
		                  <div class="text" id="u11681_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Thông điệp gửi tới phụ huynh
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u11682">
		                  <img class="img" id="u11682_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11682.png"/>
		                  <div class="text" id="u11682_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Kiến thức
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u11683">
		                  <img class="img" id="u11683_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11683.png"/>
		                  <div class="text" id="u11683_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u11684">
		                  <img class="img" id="u11684_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11684.png"/>
		                  <div class="text" id="u11684_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u11685">
		                  <img class="img" id="u11685_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11685.png"/>
		                  <div class="text" id="u11685_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Ngữ pháp
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u11686">
		                  <img class="img" id="u11686_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11686.png"/>
		                  <div class="text" id="u11686_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u11687">
		                  <img class="img" id="u11687_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11687.png"/>
		                  <div class="text" id="u11687_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u11688">
		                  <img class="img" id="u11688_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11682.png"/>
		                  <div class="text" id="u11688_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Từ vựng
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u11689">
		                  <img class="img" id="u11689_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11683.png"/>
		                  <div class="text" id="u11689_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u11690">
		                  <img class="img" id="u11690_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11684.png"/>
		                  <div class="text" id="u11690_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u11691">
		                  <img class="img" id="u11691_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11691.png"/>
		                  <div class="text" id="u11691_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Kỹ năng
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u11692">
		                  <img class="img" id="u11692_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11692.png"/>
		                  <div class="text" id="u11692_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u11693">
		                  <img class="img" id="u11693_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11693.png"/>
		                  <div class="text" id="u11693_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
		                        </span>
		                     </p>
		                  </div>
		               </div>
		            </div>
		            <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11694">
		               <div class="ax_default icon" id="u11695">
		                  <img class="img" id="u11695_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u11695_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u11696">
		                  <img class="img" id="u11696_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u11696_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u11697">
		                  <img class="img" id="u11697_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u11697_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u11698">
		                  <img class="img" id="u11698_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u11698_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u11699">
		                  <img class="img" id="u11699_img" src="images/_i_m_danh__nh_n_x_t/u7754.svg"/>
		                  <div class="text" id="u11699_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		            </div>
		            <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11700">
		               <div class="ax_default icon" id="u11701">
		                  <img class="img" id="u11701_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u11701_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u11702">
		                  <img class="img" id="u11702_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u11702_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u11703">
		                  <img class="img" id="u11703_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u11703_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u11704">
		                  <img class="img" id="u11704_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u11704_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u11705">
		                  <img class="img" id="u11705_img" src="images/_i_m_danh__nh_n_x_t/u7754.svg"/>
		                  <div class="text" id="u11705_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		            </div>
		            <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11706">
		               <div class="ax_default icon" id="u11707">
		                  <img class="img" id="u11707_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u11707_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u11708">
		                  <img class="img" id="u11708_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u11708_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u11709">
		                  <img class="img" id="u11709_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u11709_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u11710">
		                  <img class="img" id="u11710_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u11710_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u11711">
		                  <img class="img" id="u11711_img" src="images/_i_m_danh__nh_n_x_t/u7754.svg"/>
		                  <div class="text" id="u11711_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		            </div>
		            <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11712">
		               <div class="ax_default icon" id="u11713">
		                  <img class="img" id="u11713_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u11713_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u11714">
		                  <img class="img" id="u11714_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u11714_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u11715">
		                  <img class="img" id="u11715_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u11715_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u11716">
		                  <img class="img" id="u11716_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u11716_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u11717">
		                  <img class="img" id="u11717_img" src="images/_i_m_danh__nh_n_x_t/u7754.svg"/>
		                  <div class="text" id="u11717_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		            </div>
		            <div class="ax_default primary_button" id="u11718">
		               <div class="" id="u11718_div">
		               </div>
		               <div class="text" id="u11718_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Lưu
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default button" id="u11719">
		               <div class="" id="u11719_div">
		               </div>
		               <div class="text" id="u11719_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Đóng
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u11720">
		               <div class="" id="u11720_div">
		               </div>
		               <div class="text" id="u11720_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Nhận xét chung
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default paragraph" id="u11721">
		               <div class="" id="u11721_div">
		               </div>
		               <div class="text" id="u11721_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget odio. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default shape" id="u11722">
		               <div class="" id="u11722_div">
		               </div>
		               <div class="text" id="u11722_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u11723">
		               <div class="" id="u11723_div">
		               </div>
		               <div class="text" id="u11723_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Tên buổi học
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u11724">
		               <div class="" id="u11724_div">
		               </div>
		               <div class="text" id="u11724_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Introduction-Greetings
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u11725">
		               <div class="" id="u11725_div">
		               </div>
		               <div class="text" id="u11725_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Ngày học
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u11726">
		               <div class="" id="u11726_div">
		               </div>
		               <div class="text" id="u11726_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        11/01/2021 17:30
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u11727">
		               <div class="" id="u11727_div">
		               </div>
		               <div class="text" id="u11727_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Phòng học
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u11728">
		               <div class="" id="u11728_div">
		               </div>
		               <div class="text" id="u11728_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Mickey
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u11729">
		               <div class="" id="u11729_div">
		               </div>
		               <div class="text" id="u11729_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        GV Việt
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u11730">
		               <div class="" id="u11730_div">
		               </div>
		               <div class="text" id="u11730_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Nguyễn Hoàng
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u11731">
		               <div class="" id="u11731_div">
		               </div>
		               <div class="text" id="u11731_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        GVNN
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u11732">
		               <div class="" id="u11732_div">
		               </div>
		               <div class="text" id="u11732_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Jame
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u11733">
		               <div class="" id="u11733_div">
		               </div>
		               <div class="text" id="u11733_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Trạng thái
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u11734">
		               <div class="" id="u11734_div">
		               </div>
		               <div class="text" id="u11734_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Có mặt
		                     </span>
		                  </p>
		               </div>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default shape" id="u11735">
		      <div class="" id="u11735_div">
		      </div>
		      <div class="text" id="u11735_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Có mặt
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default shape" id="u11736">
		      <div class="" id="u11736_div">
		      </div>
		      <div class="text" id="u11736_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Có mặt
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default checkbox selected" id="u11737">
		      <label for="u11737_input" id="u11737_input_label" style={{"position":" absolute","left":" 0px"}}>
		         <img class="img" id="u11737_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11737_selected.svg"/>
		         <div class="text" id="u11737_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </label>
		      <input checked="" id="u11737_input" type="checkbox" value="checkbox"/>
		   </div>
		   <div class="ax_default icon" id="u11738">
		      <img class="img" id="u11738_img" src="images/th_i_gian_r_nh-list/u3423.svg"/>
		      <div class="text" id="u11738_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u11739">
		      <img class="img" id="u11739_img" src="images/th_i_gian_r_nh-list/u3423.svg"/>
		      <div class="text" id="u11739_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u11740">
		      <img class="img" id="u11740_img" src="images/th_i_gian_r_nh-list/u3423.svg"/>
		      <div class="text" id="u11740_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u11741">
		      <img class="img" id="u11741_img" src="images/th_i_gian_r_nh-list/u3423.svg"/>
		      <div class="text" id="u11741_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u11742">
		      <img class="img" id="u11742_img" src="images/th_i_gian_r_nh-list/u3423.svg"/>
		      <div class="text" id="u11742_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="950" data-label="nhận xét - Kiểm tra" data-left="0" data-top="0" data-width="1920" id="u11743" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default" data-height="0" data-label="Nhận xét" data-left="0" data-top="0" data-width="0" id="u11744">
		         <div class="ax_default shape" id="u11745">
		            <div class="" id="u11745_div">
		            </div>
		            <div class="text" id="u11745_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11746">
		            <div class="ax_default shape" id="u11747">
		               <div class="" id="u11747_div">
		               </div>
		               <div class="text" id="u11747_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default paragraph" id="u11748">
		               <div class="" id="u11748_div">
		               </div>
		               <div class="text" id="u11748_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Nhận xét học viên Bùi Văn Tích
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default" id="u11749">
		               <div class="ax_default table_cell" id="u11750">
		                  <img class="img" id="u11750_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11679.png"/>
		                  <div class="text" id="u11750_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Kỹ năng - Tiêu chí
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u11751">
		                  <img class="img" id="u11751_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11680.png"/>
		                  <div class="text" id="u11751_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Đánh giá
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u11752">
		                  <img class="img" id="u11752_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11681.png"/>
		                  <div class="text" id="u11752_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Thông điệp gửi tới phụ huynh
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u11753">
		                  <img class="img" id="u11753_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11682.png"/>
		                  <div class="text" id="u11753_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Kiến thức
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u11754">
		                  <img class="img" id="u11754_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11683.png"/>
		                  <div class="text" id="u11754_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u11755">
		                  <img class="img" id="u11755_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11684.png"/>
		                  <div class="text" id="u11755_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u11756">
		                  <img class="img" id="u11756_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11685.png"/>
		                  <div class="text" id="u11756_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Ngữ pháp
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u11757">
		                  <img class="img" id="u11757_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11686.png"/>
		                  <div class="text" id="u11757_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u11758">
		                  <img class="img" id="u11758_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11687.png"/>
		                  <div class="text" id="u11758_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u11759">
		                  <img class="img" id="u11759_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11682.png"/>
		                  <div class="text" id="u11759_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Từ vựng
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u11760">
		                  <img class="img" id="u11760_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11683.png"/>
		                  <div class="text" id="u11760_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u11761">
		                  <img class="img" id="u11761_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11684.png"/>
		                  <div class="text" id="u11761_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u11762">
		                  <img class="img" id="u11762_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11691.png"/>
		                  <div class="text" id="u11762_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Kỹ năng
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u11763">
		                  <img class="img" id="u11763_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11692.png"/>
		                  <div class="text" id="u11763_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u11764">
		                  <img class="img" id="u11764_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11693.png"/>
		                  <div class="text" id="u11764_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
		                        </span>
		                     </p>
		                  </div>
		               </div>
		            </div>
		            <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11765">
		               <div class="ax_default icon" id="u11766">
		                  <img class="img" id="u11766_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u11766_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u11767">
		                  <img class="img" id="u11767_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u11767_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u11768">
		                  <img class="img" id="u11768_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u11768_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u11769">
		                  <img class="img" id="u11769_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u11769_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u11770">
		                  <img class="img" id="u11770_img" src="images/_i_m_danh__nh_n_x_t/u7754.svg"/>
		                  <div class="text" id="u11770_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		            </div>
		            <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11771">
		               <div class="ax_default icon" id="u11772">
		                  <img class="img" id="u11772_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u11772_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u11773">
		                  <img class="img" id="u11773_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u11773_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u11774">
		                  <img class="img" id="u11774_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u11774_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u11775">
		                  <img class="img" id="u11775_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u11775_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u11776">
		                  <img class="img" id="u11776_img" src="images/_i_m_danh__nh_n_x_t/u7754.svg"/>
		                  <div class="text" id="u11776_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		            </div>
		            <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11777">
		               <div class="ax_default icon" id="u11778">
		                  <img class="img" id="u11778_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u11778_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u11779">
		                  <img class="img" id="u11779_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u11779_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u11780">
		                  <img class="img" id="u11780_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u11780_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u11781">
		                  <img class="img" id="u11781_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u11781_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u11782">
		                  <img class="img" id="u11782_img" src="images/_i_m_danh__nh_n_x_t/u7754.svg"/>
		                  <div class="text" id="u11782_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		            </div>
		            <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11783">
		               <div class="ax_default icon" id="u11784">
		                  <img class="img" id="u11784_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u11784_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u11785">
		                  <img class="img" id="u11785_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u11785_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u11786">
		                  <img class="img" id="u11786_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u11786_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u11787">
		                  <img class="img" id="u11787_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u11787_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u11788">
		                  <img class="img" id="u11788_img" src="images/_i_m_danh__nh_n_x_t/u7754.svg"/>
		                  <div class="text" id="u11788_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		            </div>
		            <div class="ax_default primary_button" id="u11789">
		               <div class="" id="u11789_div">
		               </div>
		               <div class="text" id="u11789_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Lưu
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default button" id="u11790">
		               <div class="" id="u11790_div">
		               </div>
		               <div class="text" id="u11790_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Đóng
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u11791">
		               <div class="" id="u11791_div">
		               </div>
		               <div class="text" id="u11791_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Nhận xét chung
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default paragraph" id="u11792">
		               <div class="" id="u11792_div">
		               </div>
		               <div class="text" id="u11792_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default shape" id="u11793">
		               <div class="" id="u11793_div">
		               </div>
		               <div class="text" id="u11793_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u11794">
		               <div class="" id="u11794_div">
		               </div>
		               <div class="text" id="u11794_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Tên buổi học
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u11795">
		               <div class="" id="u11795_div">
		               </div>
		               <div class="text" id="u11795_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Introduction-Greetings
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u11796">
		               <div class="" id="u11796_div">
		               </div>
		               <div class="text" id="u11796_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Ngày học
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u11797">
		               <div class="" id="u11797_div">
		               </div>
		               <div class="text" id="u11797_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        11/01/2021 17:30
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u11798">
		               <div class="" id="u11798_div">
		               </div>
		               <div class="text" id="u11798_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Phòng học
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u11799">
		               <div class="" id="u11799_div">
		               </div>
		               <div class="text" id="u11799_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Mickey
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u11800">
		               <div class="" id="u11800_div">
		               </div>
		               <div class="text" id="u11800_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        GV Việt
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u11801">
		               <div class="" id="u11801_div">
		               </div>
		               <div class="text" id="u11801_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Nguyễn Hoàng
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u11802">
		               <div class="" id="u11802_div">
		               </div>
		               <div class="text" id="u11802_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        GVNN
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u11803">
		               <div class="" id="u11803_div">
		               </div>
		               <div class="text" id="u11803_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Jame
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u11804">
		               <div class="" id="u11804_div">
		               </div>
		               <div class="text" id="u11804_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Trạng thái
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u11805">
		               <div class="" id="u11805_div">
		               </div>
		               <div class="text" id="u11805_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Có mặt
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u11806">
		               <div class="" id="u11806_div">
		               </div>
		               <div class="text" id="u11806_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        KẾT QUẢ
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u11807">
		               <div class="" id="u11807_div">
		               </div>
		               <div class="text" id="u11807_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Đề xuất khác
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default icon" id="u11808">
		               <img class="img" id="u11808_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11808.svg"/>
		               <div class="text" id="u11808_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u11809">
		               <div class="" id="u11809_div">
		               </div>
		               <div class="text" id="u11809_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Đề xuất
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default paragraph" id="u11810">
		               <div class="" id="u11810_div">
		               </div>
		               <div class="text" id="u11810_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Tiến hành học thử với trình độ của lớp BIBOP bài thứ xxx
		                     </span>
		                  </p>
		               </div>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="950" data-label="nhận xét - học thử" data-left="0" data-top="0" data-width="1920" id="u11811" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default" data-height="0" data-label="Nhận xét" data-left="0" data-top="0" data-width="0" id="u11812">
		         <div class="ax_default shape" id="u11813">
		            <div class="" id="u11813_div">
		            </div>
		            <div class="text" id="u11813_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11814">
		            <div class="ax_default shape" id="u11815">
		               <div class="" id="u11815_div">
		               </div>
		               <div class="text" id="u11815_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default paragraph" id="u11816">
		               <div class="" id="u11816_div">
		               </div>
		               <div class="text" id="u11816_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Nhận xét học viên Bùi Văn Tích
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default" id="u11817">
		               <div class="ax_default table_cell" id="u11818">
		                  <img class="img" id="u11818_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11679.png"/>
		                  <div class="text" id="u11818_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Kỹ năng - Tiêu chí
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u11819">
		                  <img class="img" id="u11819_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11680.png"/>
		                  <div class="text" id="u11819_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Đánh giá
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u11820">
		                  <img class="img" id="u11820_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11681.png"/>
		                  <div class="text" id="u11820_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Thông điệp gửi tới phụ huynh
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u11821">
		                  <img class="img" id="u11821_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11682.png"/>
		                  <div class="text" id="u11821_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Kiến thức
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u11822">
		                  <img class="img" id="u11822_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11683.png"/>
		                  <div class="text" id="u11822_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u11823">
		                  <img class="img" id="u11823_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11684.png"/>
		                  <div class="text" id="u11823_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u11824">
		                  <img class="img" id="u11824_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11685.png"/>
		                  <div class="text" id="u11824_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Ngữ pháp
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u11825">
		                  <img class="img" id="u11825_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11686.png"/>
		                  <div class="text" id="u11825_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u11826">
		                  <img class="img" id="u11826_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11687.png"/>
		                  <div class="text" id="u11826_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u11827">
		                  <img class="img" id="u11827_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11682.png"/>
		                  <div class="text" id="u11827_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Từ vựng
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u11828">
		                  <img class="img" id="u11828_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11683.png"/>
		                  <div class="text" id="u11828_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u11829">
		                  <img class="img" id="u11829_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11684.png"/>
		                  <div class="text" id="u11829_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u11830">
		                  <img class="img" id="u11830_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11691.png"/>
		                  <div class="text" id="u11830_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Kỹ năng
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u11831">
		                  <img class="img" id="u11831_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11692.png"/>
		                  <div class="text" id="u11831_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default table_cell" id="u11832">
		                  <img class="img" id="u11832_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11693.png"/>
		                  <div class="text" id="u11832_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
		                        </span>
		                     </p>
		                  </div>
		               </div>
		            </div>
		            <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11833">
		               <div class="ax_default icon" id="u11834">
		                  <img class="img" id="u11834_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u11834_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u11835">
		                  <img class="img" id="u11835_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u11835_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u11836">
		                  <img class="img" id="u11836_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u11836_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u11837">
		                  <img class="img" id="u11837_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u11837_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u11838">
		                  <img class="img" id="u11838_img" src="images/_i_m_danh__nh_n_x_t/u7754.svg"/>
		                  <div class="text" id="u11838_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		            </div>
		            <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11839">
		               <div class="ax_default icon" id="u11840">
		                  <img class="img" id="u11840_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u11840_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u11841">
		                  <img class="img" id="u11841_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u11841_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u11842">
		                  <img class="img" id="u11842_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u11842_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u11843">
		                  <img class="img" id="u11843_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u11843_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u11844">
		                  <img class="img" id="u11844_img" src="images/_i_m_danh__nh_n_x_t/u7754.svg"/>
		                  <div class="text" id="u11844_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		            </div>
		            <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11845">
		               <div class="ax_default icon" id="u11846">
		                  <img class="img" id="u11846_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u11846_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u11847">
		                  <img class="img" id="u11847_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u11847_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u11848">
		                  <img class="img" id="u11848_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u11848_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u11849">
		                  <img class="img" id="u11849_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u11849_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u11850">
		                  <img class="img" id="u11850_img" src="images/_i_m_danh__nh_n_x_t/u7754.svg"/>
		                  <div class="text" id="u11850_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		            </div>
		            <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u11851">
		               <div class="ax_default icon" id="u11852">
		                  <img class="img" id="u11852_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u11852_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u11853">
		                  <img class="img" id="u11853_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u11853_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u11854">
		                  <img class="img" id="u11854_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u11854_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u11855">
		                  <img class="img" id="u11855_img" src="images/_i_m_danh__nh_n_x_t/u7750.svg"/>
		                  <div class="text" id="u11855_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u11856">
		                  <img class="img" id="u11856_img" src="images/_i_m_danh__nh_n_x_t/u7754.svg"/>
		                  <div class="text" id="u11856_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		            </div>
		            <div class="ax_default primary_button" id="u11857">
		               <div class="" id="u11857_div">
		               </div>
		               <div class="text" id="u11857_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Lưu
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default button" id="u11858">
		               <div class="" id="u11858_div">
		               </div>
		               <div class="text" id="u11858_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Đóng
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u11859">
		               <div class="" id="u11859_div">
		               </div>
		               <div class="text" id="u11859_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Nhận xét chung
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default paragraph" id="u11860">
		               <div class="" id="u11860_div">
		               </div>
		               <div class="text" id="u11860_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default shape" id="u11861">
		               <div class="" id="u11861_div">
		               </div>
		               <div class="text" id="u11861_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u11862">
		               <div class="" id="u11862_div">
		               </div>
		               <div class="text" id="u11862_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Tên buổi học
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u11863">
		               <div class="" id="u11863_div">
		               </div>
		               <div class="text" id="u11863_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Introduction-Greetings
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u11864">
		               <div class="" id="u11864_div">
		               </div>
		               <div class="text" id="u11864_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Ngày học
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u11865">
		               <div class="" id="u11865_div">
		               </div>
		               <div class="text" id="u11865_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        11/01/2021 17:30
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u11866">
		               <div class="" id="u11866_div">
		               </div>
		               <div class="text" id="u11866_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Phòng học
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u11867">
		               <div class="" id="u11867_div">
		               </div>
		               <div class="text" id="u11867_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Mickey
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u11868">
		               <div class="" id="u11868_div">
		               </div>
		               <div class="text" id="u11868_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        GV Việt
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u11869">
		               <div class="" id="u11869_div">
		               </div>
		               <div class="text" id="u11869_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Nguyễn Hoàng
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u11870">
		               <div class="" id="u11870_div">
		               </div>
		               <div class="text" id="u11870_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        GVNN
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u11871">
		               <div class="" id="u11871_div">
		               </div>
		               <div class="text" id="u11871_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Jame
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u11872">
		               <div class="" id="u11872_div">
		               </div>
		               <div class="text" id="u11872_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Trạng thái
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u11873">
		               <div class="" id="u11873_div">
		               </div>
		               <div class="text" id="u11873_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Có mặt
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u11874">
		               <div class="" id="u11874_div">
		               </div>
		               <div class="text" id="u11874_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        KẾT QUẢ
		                     </span>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default icon" id="u11875">
		               <img class="img" id="u11875_img" src="images/chi_ti_t_kh-_i_m_danh_nh_n_x_t/u11875.svg"/>
		               <div class="text" id="u11875_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		            <div class="ax_default label" id="u11876">
		               <div class="" id="u11876_div">
		               </div>
		               <div class="text" id="u11876_text">
		                  <p>
		                     <span style={{"text-decoration":"none"}}>
		                        Chấp nhận
		                     </span>
		                  </p>
		               </div>
		            </div>
		         </div>
		      </div>
		   </div>
		</div>
		
      </div>
    );
  }
}

export default Page;
