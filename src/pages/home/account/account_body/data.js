export const HEADER = [
  {
    title: "Mã nhân viên",
    key: "ma_nhan_vien",
    class: "tb-width-150"
  },
  {
    title: "Họ và tên",
    key: "ho_va_ten",
    class: "tb-width-200"
  },
  {
    title: "Số điện thoại",
    key: "so_dien_thoai",
    class: "tb-width-150"
  },
  {
    title: "Email",
    key: "email",
    class: "tb-width-200"
  },
  {
    title: "Trung tâm",
    key: "trung_tam",
    class: "tb-width-150"
  },
  {
    title: "Phòng ban",
    key: "phong_ban",
    class: "tb-width-150"
  },
  {
    title: "Vị trí",
    key: "vi_tri",
    class: "tb-width-150"
  },
  {
    title: "Loại hợp đồng",
    key: "loai_hop_dong",
    class: "tb-width-150"
  },
  {
    title: "Trạng thái",
    key: "trang_thai",
    class: "tb-width-100"
  }
];

export const DATA = [
  {
    id: "1",
    ma_nhan_vien: "TC-00001",
    ho_va_ten: "Bùi Anh Tuấn",
    img: "images/u439.svg",
    so_dien_thoai: "0384433777",
    email: "tuan.bui@bret.edu.vn",
    trung_tam: "Hội sở",
    phong_ban: "Công nghệ thông tin",
    vi_tri: "Nhân viên",
    loai_hop_dong: "Full time",
    trang_thai: true
  },
  {
    id: "1",
    ma_nhan_vien: "TC-00001",
    ho_va_ten: "Bùi Anh Tuấn",
    img: "images/u439.svg",
    so_dien_thoai: "0384433777",
    email: "tuan.bui@bret.edu.vn",
    trung_tam: "Hội sở",
    phong_ban: "Công nghệ thông tin",
    vi_tri: "Nhân viên",
    loai_hop_dong: "Full time",
    trang_thai: true
  },
  {
    id: "1",
    ma_nhan_vien: "TC-00001",
    ho_va_ten: "Bùi Anh Tuấn",
    img: "images/u439.svg",
    so_dien_thoai: "0384433777",
    email: "tuan.bui@bret.edu.vn",
    trung_tam: "Hội sở",
    phong_ban: "Công nghệ thông tin",
    vi_tri: "Nhân viên",
    loai_hop_dong: "Full time",
    trang_thai: true
  },
  {
    id: "1",
    ma_nhan_vien: "TC-00001",
    ho_va_ten: "Bùi Anh Tuấn",
    img: "images/u439.svg",
    so_dien_thoai: "0384433777",
    email: "tuan.bui@bret.edu.vn",
    trung_tam: "Hội sở",
    phong_ban: "Công nghệ thông tin",
    vi_tri: "Nhân viên",
    loai_hop_dong: "Full time",
    trang_thai: true
  },
  {
    id: "1",
    ma_nhan_vien: "TC-00001",
    ho_va_ten: "Bùi Anh Tuấn",
    img: "images/u439.svg",
    so_dien_thoai: "0384433777",
    email: "tuan.bui@bret.edu.vn",
    trung_tam: "Hội sở",
    phong_ban: "Công nghệ thông tin",
    vi_tri: "Nhân viên",
    loai_hop_dong: "Full time",
    trang_thai: true
  },
  {
    id: "1",
    ma_nhan_vien: "TC-00001",
    ho_va_ten: "Bùi Anh Tuấn",
    img: "images/u439.svg",
    so_dien_thoai: "0384433777",
    email: "tuan.bui@bret.edu.vn",
    trung_tam: "Hội sở",
    phong_ban: "Công nghệ thông tin",
    vi_tri: "Nhân viên",
    loai_hop_dong: "Full time",
    trang_thai: true
  },
  {
    id: "1",
    ma_nhan_vien: "TC-00001",
    ho_va_ten: "Bùi Anh Tuấn",
    img: "images/u439.svg",
    so_dien_thoai: "0384433777",
    email: "tuan.bui@bret.edu.vn",
    trung_tam: "Hội sở",
    phong_ban: "Công nghệ thông tin",
    vi_tri: "Nhân viên",
    loai_hop_dong: "Full time",
    trang_thai: true
  },
  {
    id: "1",
    ma_nhan_vien: "TC-00001",
    ho_va_ten: "Bùi Anh Tuấn",
    img: "images/u439.svg",
    so_dien_thoai: "0384433777",
    email: "tuan.bui@bret.edu.vn",
    trung_tam: "Hội sở",
    phong_ban: "Công nghệ thông tin",
    vi_tri: "Nhân viên",
    loai_hop_dong: "Full time",
    trang_thai: true
  },
  {
    id: "1",
    ma_nhan_vien: "TC-00001",
    ho_va_ten: "Bùi Anh Tuấn",
    img: "images/u439.svg",
    so_dien_thoai: "0384433777",
    email: "tuan.bui@bret.edu.vn",
    trung_tam: "Hội sở",
    phong_ban: "Công nghệ thông tin",
    vi_tri: "Nhân viên",
    loai_hop_dong: "Full time",
    trang_thai: true
  },
  {
    id: "1",
    ma_nhan_vien: "TC-00001",
    ho_va_ten: "Bùi Anh Tuấn",
    img: "images/u439.svg",
    so_dien_thoai: "0384433777",
    email: "tuan.bui@bret.edu.vn",
    trung_tam: "Hội sở",
    phong_ban: "Công nghệ thông tin",
    vi_tri: "Nhân viên",
    loai_hop_dong: "Full time",
    trang_thai: true
  }
];
