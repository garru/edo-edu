import React from "react";
import { connect } from "dva";
import { Button } from "react-bootstrap";
import CustomModal from "./modal";
import * as _ from "lodash";
import CustomTooltip from "../../../../components/CustomTooltip";
import CustomTable from "../../../../components/Table";

class Page extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      id: null,
      activeIndex: null,
      showModal: false,
      isCreateNew: false,
      target: null,
      show: false,
      tootlTipindex: null
    };
  }

  updateOptions(data) {
    const { dispatch } = this.props;
    dispatch({
      type: "Global/showLoading"
    });
    dispatch({
      type: "Account/updateoptions",
      payload: data
    }).then(res => {
      dispatch({
        type: "Global/hideLoading"
      });
      !res &&
        dispatch({
          type: "Global/showError"
        });
    });
  }

  renderTooltip = () => {
    const { dispatch } = this.props;
    const { tootlTipindex } = this.state;
    const dataSource = [
      {
        label: "Chỉnh sửa",
        className: "fa fa-edit",
        onClick: e => {
          dispatch({
            type: "Global/showLoading"
          });
          dispatch({
            type: "Account/view",
            payload: { id: this.state.id }
          }).then(() => {
            dispatch({
              type: "Global/hideLoading"
            });
            this.setState({ isCreateNew: false, showModal: true });
          });
          this.handleClick(e, tootlTipindex, true);
        }
      },
      {
        label:
          this.props.Account.list_account[tootlTipindex]?.status === 1
            ? "Khoá"
            : "Mở khoá",
        className: "fa fa-lock",
        onClick: e => {
          this.handleClick(e, tootlTipindex, true);
          dispatch({
            type: "Global/showLoading"
          });
          dispatch({
            type:
              this.props.Account.list_account[tootlTipindex]?.status === 1
                ? "Account/lock"
                : "Account/unlock",
            payload: { id: this.state.id }
          }).then(res => {
            if (res.code) {
              dispatch({
                type: "Global/hideLoading"
              });
              dispatch({
                type: "Global/showError"
              });
            } else {
              dispatch({
                type: "Account/updateoptions",
                payload: {}
              }).then(() => {
                dispatch({
                  type: "Global/hideLoading"
                });
                dispatch({
                  type: "Global/showSuccess"
                });
              });
            }
          });
        }
      },
      {
        label: "Đổi mật khẩu",
        className: "fa fa-key",
        onClick: e => {
          this.handleClick(e, tootlTipindex, true);
        }
      },
      {
        label: "Xoá",
        className: "fa fa-trash-alt",
        onClick: e => {
          this.handleClick(e, tootlTipindex, true);
          dispatch({
            type: "Global/showLoading"
          });
          dispatch({
            type: "Account/delete_account",
            payload: { id: this.state.id }
          }).then(res => {
            if (res.code) {
              dispatch({
                type: "Global/hideLoading"
              });
              dispatch({
                type: "Global/showError"
              });
            } else {
              dispatch({
                type: "Account/updateoptions",
                payload: {}
              }).then(() => {
                dispatch({
                  type: "Global/hideLoading"
                });
                dispatch({
                  type: "Global/showSuccess"
                });
              });
            }
          });
        }
      }
    ];
    return (
      <CustomTooltip
        show={this.state.show}
        onHide={e => {
          this.handleClick(e, tootlTipindex);
        }}
        dataSource={dataSource}
        target={this.state.target}
      ></CustomTooltip>
    );
  };

  handleClick(e, index, isHide) {
    this.setState({
      show: !(isHide || index === this.state.activeIndex),
      target: isHide ? null : e.target,
      activeIndex: isHide || index === this.state.activeIndex ? null : index
    });
  }

  onClose = isUpdate => {
    this.setState({ isCreateNew: false, showModal: false });
    if (isUpdate) {
      const { dispatch } = this.props;
      dispatch({
        type: "Global/showLoading"
      });
      dispatch({
        type: "Account/updateoptions",
        payload: {}
      }).then(() => {
        dispatch({
          type: "Global/hideLoading"
        });
        dispatch({
          type: "Global/showSuccess"
        });
      });
    }
  };

  render() {
    const columns = [
      {
        title: "Mã nhân viên",
        key: "code",
        class: "tb-width-150"
      },
      {
        title: "Họ và tên",
        key: "fullName",
        class: "tb-width-200",
        sortBy: "fullName",
        render: (col, index) => (
          <div className="cl-name">
            <div className="cl-name-text">{col.fullName}</div>
            <div className="cl-name-name">
              <img src={col.avatarImage || "images/user/11.png"} alt="Avatar" />
            </div>
          </div>
        )
      },
      {
        title: "Số điện thoại",
        key: "phone",
        class: "tb-width-150"
      },
      {
        title: "Email",
        key: "email",
        class: "tb-width-200"
      },
      {
        title: "Trung tâm",
        key: "centerName",
        class: "tb-width-150"
      },
      {
        title: "Phòng ban",
        key: "departementName",
        class: "tb-width-150"
      },
      {
        title: "Vị trí",
        key: "roleName",
        class: "tb-width-150"
      },
      {
        title: "Loại hợp đồng",
        key: "contractName",
        class: "tb-width-150"
      },
      {
        title: "Trạng thái",
        key: "status",
        class: "tb-width-100",
        render: (col, index) => (
          <>
            {col.status ? (
              <i
                className="fas fa-check"
                style={{
                  marginRight: 0,
                  marginLeft: "auto",
                  color: "#4b7902"
                }}
              />
            ) : (
              ""
            )}
          </>
        )
      },
      {
        title: "",
        key: "action",
        class: "tb_width_50",
        render: (col, index) => (
          <>
            <i
              className="fa fa-ellipsis-h"
              style={{
                marginRight: 0,
                marginLeft: "auto"
              }}
              onClick={e => {
                e.preventDefault();
                this.handleClick(e, index);
                this.setState({ id: col.id, tootlTipindex: index });
              }}
            />
          </>
        )
      }
    ];

    return (
      <div className="row m-t-3">
        <div className="col-12 text-right">
          <div className="form-group">
            <Button
              variant="primary"
              type="submit"
              onClick={() =>
                this.setState({ isCreateNew: true, showModal: true })
              }
            >
              Tạo mới
            </Button>
          </div>
        </div>

        <div className="col-12">
          <CustomTable
            dataSource={this.props.Account.list_account}
            total={this.props.Account.total_record}
            columns={columns}
            onChange={data => this.updateOptions(data)}
            checkbox
          ></CustomTable>
        </div>

        {this.state.showModal && (
          <CustomModal
            isCreateNew={this.state.isCreateNew}
            onClose={this.onClose}
            id={this.state.id}
          ></CustomModal>
        )}
        {this.renderTooltip()}
      </div>
    );
  }
}

export default connect(({ Account, Center, Global }) => ({
  Account,
  Center,
  Global
}))(Page);
