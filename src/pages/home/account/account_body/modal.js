import React from "react";
import isEqual from "lodash/isEqual";
import cloneDeep from "lodash/cloneDeep";
import { Modal, Button } from "react-bootstrap";
import moment from "moment";
import { connect } from "dva";
import { Formik, ErrorMessage } from "formik";
import CustomTable from "../../../../components/Table";

class CustomModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      show: true,
      count: 0,
      centers: []
    };
  }

  componentWillReceiveProps(nextProps) {
    !isEqual(this.state.centers, nextProps.Account.current_account.centers) &&
      this.setState({
        centers: nextProps.Account.current_account.centers || []
      });
  }

  componentDidMount() {
    const centers = cloneDeep(this.props.Account.current_account.centers) || [];
    this.setState({ centers });
  }

  handleClose = isUpdate => {
    this.props.onClose(isUpdate);
  };

  render() {
    const { isCreateNew, Account } = this.props;
    const {
      firstName,
      lastName,
      gender,
      maritalStatus,
      birthDate,
      birthPlace,
      homeTown,
      nationality,
      permanentAddress,
      email,
      phone,
      centerId,
      departementId,
      roleId,
      contractType,
      startDate,
      code
    } = isCreateNew ? {} : Account?.current_account;
    const removePermision = (index, groupId) => {
      const { dispatch } = this.props;
      dispatch({
        type: "Account/removepermission",
        payload: { index, groupId }
      });
    };
    const columns = [
      {
        title: "Trung tâm",
        key: "centerName"
      },
      {
        title: "Quyền",
        key: "groups",
        render: (col, index) => (
          <>
            {col?.groups.map(item => (
              <div className="text-left" key={`permission${item.groupId}`}>
                <div className="permission">
                  <span>{item.groupName} &nbsp;&nbsp;</span>
                  <span
                    className="permission-delete"
                    onClick={() => removePermision(index, item.groupId)}
                  >
                    x
                  </span>
                </div>
              </div>
            ))}
          </>
        )
      }
    ];

    return (
      <Modal
        size={"lg"}
        show={this.state.show}
        onHide={() => this.handleClose()}
        backdrop="static"
        keyboard={false}
        id="AddNew"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Formik
          enableReinitialize={true}
          initialValues={{
            firstName: firstName || "",
            lastName: lastName || "",
            gender: gender || 1,
            maritalStatus: maritalStatus || 1,
            birthDate: moment(birthDate).format("YYYY-MM-DD"),
            birthPlace: birthPlace || "",
            homeTown: homeTown || "",
            nationality: nationality || "",
            permanentAddress: permanentAddress || "",
            email: email || "",
            phone: phone || "",
            centerId: centerId || "",
            departementId: departementId || "",
            roleId: roleId || "",
            contractType: contractType || "",
            startDate: moment(startDate).format("YYYY-MM-DD")
          }}
          validate={values => {
            const errors = {};
            if (!values.email) {
              errors.email = "Trường bắt buộc";
            } else if (
              !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
            ) {
              errors.email = "Email không hợp lệ";
            }
            !values.firstName && (errors.firstName = "Trường bắt buộc");
            !values.lastName && (errors.lastName = "Trường bắt buộc");
            !values.maritalStatus && (errors.maritalStatus = "Trường bắt buộc");
            !values.birthPlace && (errors.birthPlace = "Trường bắt buộc");
            !values.homeTown && (errors.homeTown = "Trường bắt buộc");
            !values.nationality && (errors.nationality = "Trường bắt buộc");
            !values.permanentAddress &&
              (errors.permanentAddress = "Trường bắt buộc");
            if (!values.phone) {
              errors.phone = "Trường bắt buộc";
            } else if (!/(84|0[3|5|7|8|9])+([0-9]{8})\b/g.test(values.phone)) {
              errors.phone = "Số điện thoại không hợp lệ";
            }
            !values.centerId && (errors.centerId = "Trường bắt buộc");
            !values.departementId && (errors.departementId = "Trường bắt buộc");
            !values.roleId && (errors.roleId = "Trường bắt buộc");
            !values.contractType && (errors.contractType = "Trường bắt buộc");

            return errors;
          }}
          onSubmit={(values, { setSubmitting }) => {
            const { dispatch } = this.props;
            const centers = this.state.centers?.map(cen => {
              const { centerId, groups } = cen;
              const groupsId = groups?.map(gr => gr.groupId);
              return {
                centerId: centerId,
                groups: groupsId
              };
            });
            dispatch({
              type: "Global/showLoading"
            });
            dispatch({
              type: isCreateNew ? "Account/insert" : "Account/update",
              payload: {
                ...values,
                centers: centers,
                maritalStatus: +values.maritalStatus,
                centerId: +values.centerId,
                departementId: +values.departementId,
                roleId: +values.roleId,
                contractType: +values.contractType,
                id: this.props.id,
                code: code
              }
            })
              .then(res => {
                dispatch({
                  type: "Global/hideLoading"
                });
                !res.code
                  ? dispatch({
                      type: "Global/showSuccess"
                    })
                  : dispatch({
                      type: "Global/showError"
                    });
                !res.code && this.props.onClose(true);
              })
              .catch(() => {
                dispatch({
                  type: "Global/showError"
                });
                dispatch({
                  type: "Global/hideLoading"
                });
              });
          }}
        >
          {({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
            setFieldValue
            /* and other goodies */
          }) => (
            <form style={{ width: "100%" }} onSubmit={handleSubmit}>
              <Modal.Header closeButton>
                <Modal.Title>
                  {isCreateNew ? "Thêm mới tài khoản" : "Chỉnh sửa tài khoản"}
                </Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <div className="m-b-3">
                  <div className="row">
                    <div className="col-12">
                      <fieldset>
                        <legend>Thông tin cơ bản</legend>
                        <div className="row ">
                          <div className="col-md-6 ">
                            <div className="form-group">
                              <label htmlFor="firstName">Họ và đệm:</label>
                              <input
                                type="text"
                                name="firstName"
                                className="form-control form-control-lg"
                                id="firstName"
                                value={values.firstName}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                placeholder={"Nhập Họ và đệm"}
                              ></input>
                              <ErrorMessage
                                component="p"
                                className="error-message"
                                name="firstName"
                              ></ErrorMessage>
                            </div>
                            <div className="form-group">
                              <label htmlFor="lastName">Tên:</label>
                              <input
                                type="text"
                                name="lastName"
                                className="form-control form-control-lg"
                                id="lastName"
                                value={values.lastName}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                placeholder={"Nhập Tên"}
                              ></input>
                              <ErrorMessage
                                component="p"
                                className="error-message"
                                name="lastName"
                              ></ErrorMessage>
                            </div>
                            <div className="form-group">
                              <label htmlFor="email">Email:</label>
                              <input
                                type="email"
                                name="email"
                                className="form-control form-control-lg"
                                id="email"
                                value={values.email}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                placeholder={"Nhập Email"}
                              ></input>
                              <ErrorMessage
                                component="p"
                                className="error-message"
                                name="email"
                              ></ErrorMessage>
                            </div>
                            <div className="form-group">
                              <label htmlFor="phone">Số điện thoại:</label>
                              <input
                                type="text"
                                name="phone"
                                className="form-control form-control-lg"
                                id="phone"
                                value={values.phone}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                placeholder={"Nhập Số điện thoại"}
                              ></input>
                              <ErrorMessage
                                component="p"
                                className="error-message"
                                name="phone"
                              ></ErrorMessage>
                            </div>
                            <div className="form-group">
                              <div className="p-0">
                                <label>Giới tính:</label>
                              </div>
                              <div className="row">
                                {this.props.Account?.list_gioi_tinh?.map(
                                  gender => (
                                    <div
                                      className="col-md-6 col-sm-12"
                                      key={`gender_${gender.code}`}
                                    >
                                      <div className="custom-control custom-radio">
                                        <input
                                          className="custom-control-input"
                                          id={gender.code}
                                          name="gender"
                                          type="radio"
                                          value={gender.id}
                                          checked={values.gender === gender.id}
                                          onChange={() =>
                                            setFieldValue("gender", gender.id)
                                          }
                                          onBlur={handleBlur}
                                        />
                                        <label
                                          className="custom-control-label"
                                          htmlFor={gender.code}
                                        >
                                          {gender.name}
                                        </label>
                                      </div>
                                    </div>
                                  )
                                )}
                              </div>
                            </div>
                            <div className="form-group">
                              <label htmlFor="maritalStatus">
                                Tình trạng hôn nhân:
                              </label>
                              <select
                                className="form-control form-control-lg"
                                id="maritalStatus"
                                name="maritalStatus"
                                value={values.maritalStatus}
                                onChange={handleChange}
                                onBlur={handleBlur}
                              >
                                <>
                                  {this.props.Account?.list_tinh_trang_hon_nhan?.map(
                                    item => (
                                      <option
                                        key={`maritalStatus${item.code}`}
                                        value={item.code}
                                        disabled={
                                          item.code !== "" ? false : true
                                        }
                                        hidden={item.code !== "" ? false : true}
                                      >
                                        {item.name}
                                      </option>
                                    )
                                  )}
                                </>
                              </select>
                              <ErrorMessage
                                component="p"
                                className="error-message"
                                name="maritalStatus"
                              ></ErrorMessage>
                            </div>
                          </div>

                          <div className="col-md-6 ">
                            <div className="form-group">
                              <label htmlFor="birthDate">Ngày sinh:</label>
                              <input
                                type="date"
                                className="form-control form-control-lg"
                                id="birthDate"
                                value={values.birthDate}
                                onChange={(date, dateString) =>
                                  setFieldValue("birthDate", dateString)
                                }
                                onBlur={handleBlur}
                              />
                            </div>
                            <div className="form-group">
                              <label htmlFor="birthPlace">Nơi sinh:</label>
                              <select
                                className="form-control form-control-lg"
                                id="birthPlace"
                                name="birthPlace"
                                value={values.birthPlace}
                                onChange={handleChange}
                                onBlur={handleBlur}
                              >
                                <>
                                  <option value="" disabled hidden>
                                    Vui lòng chọn giá trị
                                  </option>
                                  {this.props.Account?.list_noi_sinh?.map(
                                    item => (
                                      <option
                                        key={`birthPlace${item.code}`}
                                        value={item.code}
                                        disabled={
                                          item.code !== "" ? false : true
                                        }
                                        hidden={item.code !== "" ? false : true}
                                      >
                                        {item.name}
                                      </option>
                                    )
                                  )}
                                </>
                              </select>
                              <ErrorMessage
                                component="p"
                                className="error-message"
                                name="birthPlace"
                              ></ErrorMessage>
                            </div>
                            <div className="form-group">
                              <label htmlFor="homeTown">Nguyên quán:</label>
                              <select
                                className="form-control form-control-lg"
                                id="homeTown"
                                name="homeTown"
                                value={values.homeTown}
                                onChange={handleChange}
                                onBlur={handleBlur}
                              >
                                <>
                                  <option value="" disabled hidden>
                                    Vui lòng chọn giá trị
                                  </option>
                                  {this.props.Account?.list_noi_sinh?.map(
                                    item => (
                                      <option
                                        key={`homeTown${item.code}`}
                                        value={item.code}
                                        disabled={
                                          item.code !== "" ? false : true
                                        }
                                        hidden={item.code !== "" ? false : true}
                                      >
                                        {item.name}
                                      </option>
                                    )
                                  )}
                                </>
                              </select>
                              <ErrorMessage
                                component="p"
                                className="error-message"
                                name="homeTown"
                              ></ErrorMessage>
                            </div>
                            <div className="form-group">
                              <label htmlFor="nationality">Quốc tịch:</label>
                              <select
                                className="form-control form-control-lg"
                                id="nationality"
                                name="nationality"
                                value={values.nationality}
                                onChange={handleChange}
                                onBlur={handleBlur}
                              >
                                <>
                                  <option value="" disabled hidden>
                                    Vui lòng chọn giá trị
                                  </option>
                                  {this.props.Account?.list_quoc_tich?.map(
                                    item => (
                                      <option
                                        key={`nationality${item.code}`}
                                        value={item.code}
                                        disabled={
                                          item.code !== "" ? false : true
                                        }
                                        hidden={item.code !== "" ? false : true}
                                      >
                                        {item.name}
                                      </option>
                                    )
                                  )}
                                </>
                              </select>
                              <ErrorMessage
                                component="p"
                                className="error-message"
                                name="nationality"
                              ></ErrorMessage>
                            </div>
                            <div className="form-group">
                              <label htmlFor="permanentAddress">
                                Địa chỉ thường trú:
                              </label>
                              <textarea
                                className="textarea form-control"
                                rows="4"
                                name="permanentAddress"
                                value={values.permanentAddress}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                placeholder={"Nhập Địa chỉ thường trú"}
                              ></textarea>
                              <ErrorMessage
                                component="p"
                                className="error-message"
                                name="permanentAddress"
                              ></ErrorMessage>
                            </div>
                          </div>
                        </div>
                      </fieldset>
                    </div>
                  </div>
                </div>
                <div className="m-b-3">
                  <div className="row">
                    <div className="col-12">
                      <fieldset>
                        <legend>Thông tin làm việc</legend>
                        <div className="row">
                          <div className="col-md-6 ">
                            <div className="form-group">
                              <label htmlFor="centerId">Trung tâm:</label>
                              <select
                                className="form-control form-control-lg"
                                id="centerId"
                                name="centerId"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.centerId}
                              >
                                <option value="" disabled hidden>
                                  Vui lòng chọn giá trị
                                </option>
                                {this.props.Center.list_center?.map(item => (
                                  <option
                                    key={`center${item.id}`}
                                    value={item.id}
                                    disabled={item.id !== "" ? false : true}
                                    hidden={item.id !== "" ? false : true}
                                  >
                                    {item.name}
                                  </option>
                                ))}
                              </select>
                              <ErrorMessage
                                component="p"
                                className="error-message"
                                name="centerId"
                              ></ErrorMessage>
                            </div>
                          </div>
                          <div className="col-md-6 ">
                            <div className="form-group">
                              <label htmlFor="roleId">Vị trí:</label>
                              <select
                                className="form-control form-control-lg"
                                id="roleId"
                                name="roleId"
                                value={values.roleId}
                                onChange={handleChange}
                                onBlur={handleBlur}
                              >
                                <option value="" disabled hidden>
                                  Vui lòng chọn giá trị
                                </option>
                                {this.props.DepartementRole?.list_vai_tro?.map(
                                  item => (
                                    <option
                                      key={`center${item.id}`}
                                      value={item.id}
                                      disabled={item.id !== "" ? false : true}
                                      hidden={item.id !== "" ? false : true}
                                    >
                                      {item.name}
                                    </option>
                                  )
                                )}
                                ))}
                              </select>
                              <ErrorMessage
                                component="p"
                                className="error-message"
                                name="roleId"
                              ></ErrorMessage>
                            </div>
                          </div>
                          <div className="col-md-6 ">
                            <div className="form-group">
                              <label htmlFor="departementId">Phòng ban:</label>
                              <select
                                className="form-control form-control-lg"
                                id="departementId"
                                name="departementId"
                                value={values.departementId}
                                onChange={handleChange}
                                onBlur={handleBlur}
                              >
                                <option value="" disabled hidden>
                                  Vui lòng chọn giá trị
                                </option>
                                {this.props.DepartementRole?.list_phong_ban?.map(
                                  item => (
                                    <option
                                      key={`center${item.id}`}
                                      value={item.id}
                                      disabled={item.id !== "" ? false : true}
                                      hidden={item.id !== "" ? false : true}
                                    >
                                      {item.name}
                                    </option>
                                  )
                                )}
                              </select>
                              <ErrorMessage
                                component="p"
                                className="error-message"
                                name="departementId"
                              ></ErrorMessage>
                            </div>
                          </div>
                          <div className="col-md-6 ">
                            <div className="form-group">
                              <label htmlFor="contractType">Hợp đồng:</label>
                              <select
                                className="form-control form-control-lg"
                                id="contractType"
                                name="contractType"
                                value={values.contractType}
                                onChange={handleChange}
                                onBlur={handleBlur}
                              >
                                <option value="" disabled hidden>
                                  Vui lòng chọn giá trị
                                </option>
                                {this.props.Account?.list_loai_hop_dong?.map(
                                  item => (
                                    <option
                                      key={`center${item.id}`}
                                      value={item.id}
                                      disabled={item.id !== "" ? false : true}
                                      hidden={item.id !== "" ? false : true}
                                    >
                                      {item.name}
                                    </option>
                                  )
                                )}
                              </select>
                              <ErrorMessage
                                component="p"
                                className="error-message"
                                name="contractType"
                              ></ErrorMessage>
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="form-group">
                              <label htmlFor="startDate">Ngày bắt đầu:</label>
                              <input
                                type="date"
                                className="form-control form-control-lg"
                                id="startDate"
                                value={values.startDate}
                                onChange={(date, dateString) =>
                                  setFieldValue("startDate", dateString)
                                }
                                onBlur={handleBlur}
                              />
                            </div>
                          </div>
                        </div>
                      </fieldset>
                    </div>
                  </div>
                </div>

                <div className="row">
                  <div className="col-12">
                    <fieldset>
                      <legend>Bảo mật</legend>
                      <div className="row">
                        <div className="col-12">
                          <CustomTable
                            dataSource={this.state.centers}
                            columns={columns}
                            onChange={() => {}}
                            noPaging
                          ></CustomTable>
                        </div>
                      </div>
                      {/* <div className="row align-end">
                        <div className="col-md-6 ">
                          <div className="form-group">
                            <label htmlFor="password">Mật khẩu</label>
                            <input
                              type="password"
                              name="password"
                              className="form-control form-control-lg"
                              id="password"
                              value={values.fullName}
                              onChange={handleChange}
                              onBlur={handleBlur}
                            ></input>
                          </div>
                        </div>
                        <div className="col-md-6 ">
                          <div className="form-group">
                            <Button
                              variant="primary"
                              style={{ marginRight: 15 }}
                            >
                              Tạo mật khẩu
                            </Button>
                          </div>
                        </div>
                        <div className="col-md-6 ">
                          <div className="form-group">
                            <input
                              type="checkbox"
                              name="yeu_cau_mk"
                              id="yeu_cau_mk"
                            />
                            <label htmlFor="yeu_cau_mk">
                              &nbsp;&nbsp; Yêu cầu đổi mật khẩu khi đăng nhập
                              lần đầu
                            </label>
                          </div>
                        </div>
                      </div>
                     */}
                    </fieldset>
                  </div>
                </div>
              </Modal.Body>
              <Modal.Footer>
                <Button
                  variant="primary"
                  style={{ marginRight: 15 }}
                  type="submit"
                >
                  {isCreateNew ? "Tạo mới" : "Cập nhật"}
                </Button>
                <Button
                  variant={"secondary"}
                  onClick={() => this.handleClose()}
                >
                  Hủy
                </Button>
              </Modal.Footer>
            </form>
          )}
        </Formik>
      </Modal>
    );
  }
}

export default connect(
  ({ Account, Center, DepartementRole, Global, Dictionary }) => ({
    Account,
    Center,
    DepartementRole,
    Global,
    Dictionary
  })
)(CustomModal);
