import React from "react";
import { connect } from "dva";
import AccountHeader from "./account_header";
import AccountBody from "./account_body";
import PageHeader from "../../../components/PageHeader";

class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: "Global/showLoading"
    });
    dispatch({
      type: "Account/search"
    }).then(
      () => {
        dispatch({
          type: "Global/hideLoading"
        });
      },
      error => {
        dispatch({
          type: "Global/hideLoading"
        });
        dispatch({
          type: "Global/showError"
        });
      }
    );
  }

  render() {
    const bre = {
      title: "Tài khoản",
      subTitle:
        "Danh sách tài khoản sử dụng hệ thống, không bao gồm danh sách khách hàng",
      breadcrums: [
        {
          title: "Home",
          path: "/"
        },
        {
          title: "Tài khoản",
          path: ""
        }
      ]
    };

    return (
      <React.Fragment>
        <PageHeader {...bre} />
        <div className="iq-card">
          <div className="iq-card-body">
            <AccountHeader />
            <AccountBody />
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default connect(({ Account, Global }) => ({
  Account,
  Global
}))(Page);
