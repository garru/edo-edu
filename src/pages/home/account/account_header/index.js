import React from "react";
import { connect } from "dva";
import { Button } from "react-bootstrap";
import { Formik, ErrorMessage } from "formik";
class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    console.log("render account_header>>>");

    return (
      <div className="row">
        <Formik
          enableReinitialize={true}
          initialValues={{
            code: "",
            full_Name: "",
            email: "",
            phone: "",
            center_Id: ""
          }}
          onSubmit={(values, { setSubmitting }) => {
            const { dispatch } = this.props;
            dispatch({
              type: "Global/showLoading"
            });
            dispatch({
              type: "Account/updateoptions",
              payload: values
            }).then(() => {
              dispatch({
                type: "Global/hideLoading"
              });
            });
          }}
        >
          {({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting
            /* and other goodies */
          }) => (
            <form style={{ width: "100%" }} onSubmit={handleSubmit}>
              <div className="col-12">
                <fieldset>
                  <legend>Tìm kiếm</legend>
                  <div className="row align-end">
                    <div className="col-lg-10 col-md-9 col-sm-12">
                      <div className="row">
                        <div className="col-lg-2 col-md-4 col-sm-6">
                          <div className="form-group">
                            <label htmlFor="code">Mã nhân viên:</label>
                            <input
                              type="text"
                              name="code"
                              className="form-control form-control-lg"
                              id="code"
                              onChange={handleChange}
                              onBlur={handleBlur}
                              value={values.code}
                              placeholder="Nhập mã nhân viên"
                              // ref={register({ required: true, minLength: 8 })}
                            ></input>
                          </div>
                        </div>
                        <div className="col-lg-2 col-md-4 col-sm-6">
                          <div className="form-group">
                            <label htmlFor="fullName">Họ và tên:</label>
                            <input
                              type="text"
                              name="fullName"
                              className="form-control form-control-lg"
                              id="fullName"
                              // ref={register({ required: true, minLength: 8 })}
                              onChange={handleChange}
                              onBlur={handleBlur}
                              value={values.fullName}
                              placeholder="Nhập họ và tên"
                            ></input>
                          </div>
                        </div>
                        <div className="col-lg-2 col-md-4 col-sm-6">
                          <div className="form-group">
                            <label htmlFor="phone">Số điện thoại:</label>
                            <input
                              type="text"
                              name="phone"
                              className="form-control form-control-lg"
                              id="phone"
                              onChange={handleChange}
                              onBlur={handleBlur}
                              value={values.phone}
                              placeholder="Nhập số điện thoại"
                              // ref={register({ required: true, minLength: 8 })}
                            ></input>
                          </div>
                        </div>
                        <div className="col-lg-3 col-md-4 col-sm-6">
                          <div className="form-group">
                            <label htmlFor="email">Email:</label>
                            <input
                              type="text"
                              name="email"
                              className="form-control form-control-lg"
                              id="email"
                              onChange={handleChange}
                              onBlur={handleBlur}
                              value={values.email}
                              placeholder="Nhập email"
                              // ref={register({ required: true, minLength: 8 })}
                            ></input>
                          </div>
                        </div>
                        <div className="col-lg-3 col-md-4 col-sm-6">
                          <div className="form-group">
                            <label htmlFor="center_Id">Cơ sở hoạt động:</label>
                            <select
                              className="form-control form-control-lg"
                              id="center_Id"
                              name="center_Id"
                              onChange={handleChange}
                              onBlur={handleBlur}
                              value={values.center_Id}
                            >
                              <option value="" disabled hidden>
                                Vui lòng chọn giá trị
                              </option>
                              {this.props.Center.list_center?.map(item => (
                                <option
                                  key={`center${item.id}`}
                                  value={item.id}
                                  disabled={item.id !== "" ? false : true}
                                  hidden={item.id !== "" ? false : true}
                                >
                                  {item.name}
                                </option>
                              ))}
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-2 col-md-3 col-sm-12 text-right">
                      <div className="form-group">
                        <Button variant="primary" type="submit">
                          Tìm kiếm
                        </Button>
                      </div>
                    </div>
                  </div>
                </fieldset>
              </div>
            </form>
          )}
        </Formik>
      </div>
    );
  }
}

export default connect(({ Account, Center }) => ({
  Account,
  Center
}))(Page);
