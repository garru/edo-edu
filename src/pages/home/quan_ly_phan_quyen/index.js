import React from "react";
import { connect } from "dva";
import { Button, Table, Overlay, Popover } from "react-bootstrap";
import CustomModal from "./modal";
import * as _ from "lodash";
import PageHeader from "../../../components/PageHeader";
import CustomTooltip from "../../../components/CustomTooltip";
import CustomTable from "../../../components/Table";

class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id: null,
      activeIndex: null,
      showModal: false,
      isCreateNew: false,
      target: null,
      show: false,
      tootlTipindex: null
    };
  }

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: "Global/showLoading"
    });
    dispatch({
      type: "Group/list"
    }).then(
      () => {
        dispatch({
          type: "Global/hideLoading"
        });
      },
      error => {
        dispatch({
          type: "Global/hideLoading"
        });
        dispatch({
          type: "Global/showError"
        });
      }
    );
  }

  updateOptions(data) {
    const { dispatch } = this.props;
    dispatch({
      type: "Global/showLoading"
    });
    dispatch({
      type: "Group/updateoptions",
      payload: data
    }).then(res => {
      dispatch({
        type: "Global/hideLoading"
      });
      !res &&
        dispatch({
          type: "Global/showError"
        });
    });
  }

  handleClick(e, index, isHide) {
    this.setState({
      show: !(index === this.state.activeIndex),
      target: isHide ? null : e.target,
      activeIndex: index === this.state.activeIndex ? null : index
    });
  }

  onClose = isUpdate => {
    this.setState({ isCreateNew: false, showModal: false });
    if (isUpdate) {
      const { dispatch } = this.props;
      dispatch({
        type: "Global/showLoading"
      });
      dispatch({
        type: "Group/list",
        payload: {}
      }).then(() => {
        dispatch({
          type: "Global/hideLoading"
        });
        dispatch({
          type: "Global/showSuccess"
        });
      });
    }
  };

  render() {
    const { dispatch } = this.props;
    const bre = {
      title: "Phân quyền",
      breadcrums: [
        {
          title: "Home",
          path: "/"
        },
        {
          title: "Phân quyền",
          path: ""
        }
      ]
    };

    const columns = [
      {
        title: "Tên nhóm quyền",
        key: "name",
        class: "tb-width-400",
        render: (col, index) => <div className="text-left">{col.name}</div>
      },
      {
        title: "",
        key: "action",
        class: "tb-width-80",
        render: (col, index) => (
          <>
            <i
              className="fa fa-edit"
              style={{
                marginRight: 15,
                marginLeft: "auto"
              }}
              onClick={e => {
                e.preventDefault();
                this.setState({
                  id: col.id
                });
                dispatch({
                  type: "Global/showLoading"
                });
                dispatch({
                  type: "Group/view",
                  payload: { id: col.id }
                }).then(() => {
                  dispatch({
                    type: "Global/hideLoading"
                  });
                  this.setState({
                    isCreateNew: false,
                    showModal: true
                  });
                });
              }}
            />
            {col.allowDelete && (
              <i
                className="fa fa-trash-alt"
                style={{
                  marginRight: 0,
                  marginLeft: "auto"
                }}
                onClick={e => {
                  e.preventDefault();
                  dispatch({
                    type: "Global/showLoading"
                  });
                  dispatch({
                    type: "Group/delete_group",
                    payload: { id: col.id }
                  }).then(res => {
                    if (res.code) {
                      dispatch({
                        type: "Global/hideLoading"
                      });
                      dispatch({
                        type: "Global/showError"
                      });
                    } else {
                      dispatch({
                        type: "Group/list",
                        payload: {}
                      }).then(() => {
                        dispatch({
                          type: "Global/hideLoading"
                        });
                        dispatch({
                          type: "Global/showSuccess"
                        });
                      });
                    }
                  });
                }}
              />
            )}
          </>
        )
      }
    ];

    return (
      <>
        <PageHeader {...bre}></PageHeader>
        <div className="iq-card">
          <div className="iq-card-body">
            <div className="">
              <div className="row">
                <div className="col-12 text-right">
                  <div className="form-group">
                    <Button
                      variant="primary"
                      type="submit"
                      onClick={() =>
                        this.setState({ isCreateNew: true, showModal: true })
                      }
                    >
                      Tạo mới nhóm quyền
                    </Button>
                  </div>
                </div>
                <div className="col-12">
                  <CustomTable
                    dataSource={this.props.Group.list_group}
                    total={this.props.Group.total_record}
                    columns={columns}
                    onChange={data => this.updateOptions(data)}
                    noPaging
                  ></CustomTable>
                </div>
              </div>
            </div>
          </div>
          {this.state.showModal && (
            <CustomModal
              isCreateNew={this.state.isCreateNew}
              onClose={this.onClose}
              selectedItem={this.state.selectedItem}
              id={this.state.id}
            ></CustomModal>
          )}
        </div>
      </>
    );
  }
}

export default connect(({ Group, Global }) => ({
  Group,
  Global
}))(Page);
