import React from 'react';
import './index.css';
import './ax.css';

class Page extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
		<div class="" id="base" style={{"margin-left":"-150px"}}>
		   <div class="ax_default shape" id="u1008">
		      <div class="" id="u1008_div">
		      </div>
		      <div class="text" id="u1008_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default shape" id="u1009">
		      <div class="" id="u1009_div">
		      </div>
		      <div class="text" id="u1009_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default" data-height="40" data-label="account" data-left="13" data-top="895" data-width="192" id="u1011">
		      <div class="ax_default shape" data-label="accountLable" id="u1012">
		         <img class="img" id="u1012_img" src="images/account_information/accountlable_u55.svg"/>
		         <div class="text" id="u1012_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Nguyen Van A
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u1013">
		         <img class="img" id="u1013_img" src="images/login/u4.svg"/>
		         <div class="text" id="u1013_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="settingMenu" data-left="11" data-top="826" data-width="125" id="u1014">
		      <div class="ax_default shape" data-label="gearIconLable" id="u1015">
		         <img class="img" id="u1015_img" src="images/account_information/geariconlable_u58.svg"/>
		         <div class="text" id="u1015_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Cài đặt
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="44" data-left="11" data-top="826" data-width="44" id="u1016">
		         <div class="ax_default shape" data-label="gearIconBG" id="u1017">
		            <div class="" id="u1017_div">
		            </div>
		            <div class="text" id="u1017_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default shape" data-label="gearIconBG2" id="u1018">
		            <div class="" id="u1018_div">
		            </div>
		            <div class="text" id="u1018_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" data-label="gearIcon" id="u1019">
		            <img class="img" id="u1019_img" src="images/account_information/gearicon_u62.svg"/>
		            <div class="text" id="u1019_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="customerMenu" data-left="11" data-top="145" data-width="143" id="u1020">
		      <div class="ax_default shape" data-label="customerIconLable" id="u1021">
		         <img class="img" id="u1021_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u1021_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Khách hàng
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="customerIconBG" id="u1022">
		         <div class="" id="u1022_div">
		         </div>
		         <div class="text" id="u1022_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="customerIconBG2" id="u1023">
		         <div class="" id="u1023_div">
		         </div>
		         <div class="text" id="u1023_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" data-label="customer" id="u1024">
		         <img class="img" id="u1024_img" src="images/account_information/customer_u67.svg"/>
		         <div class="text" id="u1024_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="classMenu" data-left="11" data-top="198" data-width="143" id="u1025">
		      <div class="ax_default shape" data-label="classIconLable" id="u1026">
		         <img class="img" id="u1026_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u1026_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Lớp học
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG" id="u1027">
		         <div class="" id="u1027_div">
		         </div>
		         <div class="text" id="u1027_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG2" id="u1028">
		         <div class="" id="u1028_div">
		         </div>
		         <div class="text" id="u1028_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" data-label="classicon" id="u1029">
		         <img class="img" id="u1029_img" src="images/account_information/classicon_u72.svg"/>
		         <div class="text" id="u1029_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="dashboardMenu" data-left="11" data-top="91" data-width="143" id="u1030">
		      <div class="ax_default shape" data-label="dashboardIconLable" id="u1031">
		         <img class="img" id="u1031_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u1031_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Dashnoard
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="dashboardIconBG" id="u1032">
		         <div class="" id="u1032_div">
		         </div>
		         <div class="text" id="u1032_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="dashboardIconBG2" id="u1033">
		         <div class="" id="u1033_div">
		         </div>
		         <div class="text" id="u1033_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" data-label="dashboard" id="u1034">
		         <img class="img" id="u1034_img" src="images/account_information/dashboard_u77.svg"/>
		         <div class="text" id="u1034_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="129" data-label="AccountMenu" data-left="58" data-top="806" data-width="246" id="u1035" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default shape" id="u1036">
		         <div class="" id="u1036_div">
		         </div>
		         <div class="text" id="u1036_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u1037">
		         <div class="" id="u1037_div">
		         </div>
		         <div class="text" id="u1037_text">
		            <p>
		               <span style={{"font-family":"ArialMT, Arial","font-weight":"400","text-decoration":"none"}}>
		                  Xin chào:
		               </span>
		               <span style={{"font-family":"Arial Bold, Arial","font-weight":"700","text-decoration":"none"}}>
		                  Nguyen Van A
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u1038">
		         <div class="ax_default image" id="u1039">
		            <img class="img" id="u1039_img" src="images/account_information/u82.png"/>
		            <div class="text" id="u1039_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u1040">
		            <div class="" id="u1040_div">
		            </div>
		            <div class="text" id="u1040_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Đăng xuất
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default line" id="u1041">
		         <img class="img" id="u1041_img" src="images/account_information/u84.svg"/>
		         <div class="text" id="u1041_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u1042">
		         <div class="ax_default paragraph" id="u1043">
		            <div class="" id="u1043_div">
		            </div>
		            <div class="text" id="u1043_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tài khoản
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default image" id="u1044">
		            <img class="img" id="u1044_img" src="images/account_information/u87.png"/>
		            <div class="text" id="u1044_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="339" data-label="SettingPannel" data-left="58" data-top="525" data-width="339" id="u1045" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default shape" id="u1046">
		         <div class="" id="u1046_div">
		         </div>
		         <div class="text" id="u1046_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u1047">
		         <div class="" id="u1047_div">
		         </div>
		         <div class="text" id="u1047_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Hệ thống
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u1048">
		         <div class="ax_default icon" id="u1049">
		            <img class="img" id="u1049_img" src="images/account_information/u92.svg"/>
		            <div class="text" id="u1049_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u1050">
		            <div class="" id="u1050_div">
		            </div>
		            <div class="text" id="u1050_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tài khoản
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u1051">
		         <div class="ax_default paragraph" id="u1052">
		            <div class="" id="u1052_div">
		            </div>
		            <div class="text" id="u1052_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phân quyền
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u1053">
		            <img class="img" id="u1053_img" src="images/account_information/u96.svg"/>
		            <div class="text" id="u1053_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u1054">
		         <div class="" id="u1054_div">
		         </div>
		         <div class="text" id="u1054_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Đào tạo
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u1055">
		         <div class="ax_default paragraph" id="u1056">
		            <div class="" id="u1056_div">
		            </div>
		            <div class="text" id="u1056_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Thương hiệu
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u1057">
		            <img class="img" id="u1057_img" src="images/account_information/u100.svg"/>
		            <div class="text" id="u1057_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u1058">
		         <div class="ax_default paragraph" id="u1059">
		            <div class="" id="u1059_div">
		            </div>
		            <div class="text" id="u1059_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trung tâm
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u1060">
		            <img class="img" id="u1060_img" src="images/account_information/u103.svg"/>
		            <div class="text" id="u1060_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u1061">
		         <div class="ax_default icon" id="u1062">
		            <img class="img" id="u1062_img" src="images/account_information/u105.svg"/>
		            <div class="text" id="u1062_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u1063">
		            <div class="" id="u1063_div">
		            </div>
		            <div class="text" id="u1063_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Ngày nghỉ
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u1064">
		         <div class="ax_default icon" id="u1065">
		            <img class="img" id="u1065_img" src="images/account_information/u108.svg"/>
		            <div class="text" id="u1065_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u1066">
		            <div class="" id="u1066_div">
		            </div>
		            <div class="text" id="u1066_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phòng ban - Vai trò
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u1067">
		         <div class="ax_default paragraph" id="u1068">
		            <div class="" id="u1068_div">
		            </div>
		            <div class="text" id="u1068_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trạng thái học viên
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u1069">
		            <img class="img" id="u1069_img" src="images/account_information/u112.svg"/>
		            <div class="text" id="u1069_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u1070">
		         <div class="ax_default paragraph" id="u1071">
		            <div class="" id="u1071_div">
		            </div>
		            <div class="text" id="u1071_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tiêu chí đánh giá
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u1072">
		            <img class="img" id="u1072_img" src="images/account_information/u115.svg"/>
		            <div class="text" id="u1072_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u1073">
		         <div class="ax_default paragraph" id="u1074">
		            <div class="" id="u1074_div">
		            </div>
		            <div class="text" id="u1074_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Chương trình học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u1075">
		            <div class="ax_default icon" id="u1076">
		               <img class="img" id="u1076_img" src="images/account_information/u119.svg"/>
		               <div class="text" id="u1076_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u1077">
		         <div class="ax_default icon" id="u1078">
		            <img class="img" id="u1078_img" src="images/account_information/u121.svg"/>
		            <div class="text" id="u1078_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default paragraph" id="u1079">
		            <div class="" id="u1079_div">
		            </div>
		            <div class="text" id="u1079_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Bài học
		                  </span>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u1080">
		         <div class="ax_default paragraph" id="u1081">
		            <div class="" id="u1081_div">
		            </div>
		            <div class="text" id="u1081_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phòng học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u1082">
		            <img class="img" id="u1082_img" src="images/account_information/u125.svg"/>
		            <div class="text" id="u1082_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u1083">
		         <div class="ax_default paragraph" id="u1084">
		            <div class="" id="u1084_div">
		            </div>
		            <div class="text" id="u1084_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trường học
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u1085">
		            <img class="img" id="u1085_img" src="images/account_information/u128.svg"/>
		            <div class="text" id="u1085_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default line" id="u1086">
		         <img class="img" id="u1086_img" src="images/account_information/u129.svg"/>
		         <div class="text" id="u1086_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u1087">
		         <div class="" id="u1087_div">
		         </div>
		         <div class="text" id="u1087_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Tài chính
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u1088">
		         <div class="ax_default paragraph" id="u1089">
		            <div class="" id="u1089_div">
		            </div>
		            <div class="text" id="u1089_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Học phí
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u1090">
		            <img class="img" id="u1090_img" src="images/account_information/u133.svg"/>
		            <div class="text" id="u1090_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u1091">
		         <div class="ax_default paragraph" id="u1092">
		            <div class="" id="u1092_div">
		            </div>
		            <div class="text" id="u1092_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Khuyến mại
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u1093">
		            <img class="img" id="u1093_img" src="images/account_information/u136.svg"/>
		            <div class="text" id="u1093_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default" data-height="44" data-label="classMenu" data-left="11" data-top="252" data-width="143" id="u1094">
		      <div class="ax_default shape" data-label="classIconLable" id="u1095">
		         <img class="img" id="u1095_img" src="images/account_information/customericonlable_u64.svg"/>
		         <div class="text" id="u1095_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Giáo viên
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG" id="u1096">
		         <div class="" id="u1096_div">
		         </div>
		         <div class="text" id="u1096_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default shape" data-label="classIconBG2" id="u1097">
		         <div class="" id="u1097_div">
		         </div>
		         <div class="text" id="u1097_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u1098">
		         <img class="img" id="u1098_img" src="images/account_information/u141.svg"/>
		         <div class="text" id="u1098_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u1099">
		      <img class="img" id="u1099_img" src="images/account_information/u142.svg"/>
		      <div class="text" id="u1099_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default icon" id="u1100">
		      <img class="img" id="u1100_img" src="images/account_information/u143.svg"/>
		      <div class="text" id="u1100_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div id="u1007" style={{"display":"none","visibility":"hidden"}}>
		   </div>
		   <div class="ax_default" data-height="55" data-label="Header" data-left="133" data-top="17" data-width="516" id="u1101">
		      <div class="ax_default paragraph" id="u1102">
		         <div class="" id="u1102_div">
		         </div>
		         <div class="text" id="u1102_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Quản lý phân quyền
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default paragraph" id="u1103">
		         <div class="" id="u1103_div">
		         </div>
		         <div class="text" id="u1103_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Phân quyền theo vai trò
		               </span>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default label" id="u1104">
		      <div class="" id="u1104_div">
		      </div>
		      <div class="text" id="u1104_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Thiết lập phòng ban
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default" data-height="248" data-label="thiet lap phong ban" data-left="139" data-top="148" data-width="188" id="u1105">
		      <div class="ax_default label" id="u1106">
		         <div class="" id="u1106_div">
		         </div>
		         <div class="text" id="u1106_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u1107">
		         <img class="img" id="u1107_img" src="images/quan_ly_phan_quyen/u1107.svg"/>
		         <div class="text" id="u1107_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u1108">
		         <img class="img" id="u1108_img" src="images/quan_ly_phan_quyen/u1108.svg"/>
		         <div class="text" id="u1108_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="20" data-left="141" data-top="190" data-width="150" id="u1109">
		         <div class="ax_default link_button" id="u1110">
		            <div class="" id="u1110_div">
		            </div>
		            <div class="text" id="u1110_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phòng hành chính
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default button" id="u1111">
		            <div class="" id="u1111_div">
		            </div>
		            <div class="text" id="u1111_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     &gt;
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u1112">
		            <img class="img" id="u1112_img" src="images/quan_ly_phan_quyen/u1112.svg"/>
		            <div class="text" id="u1112_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u1113">
		         <img class="img" id="u1113_img" src="images/quan_ly_phan_quyen/u1107.svg"/>
		         <div class="text" id="u1113_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u1114">
		         <img class="img" id="u1114_img" src="images/quan_ly_phan_quyen/u1108.svg"/>
		         <div class="text" id="u1114_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="20" data-left="176" data-top="209" data-width="87" id="u1115">
		         <div class="ax_default link_button" id="u1116">
		            <div class="" id="u1116_div">
		            </div>
		            <div class="text" id="u1116_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Nhân viên
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u1117">
		            <img class="img" id="u1117_img" src="images/quan_ly_phan_quyen/u1117.svg"/>
		            <div class="text" id="u1117_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="20" data-label="quản lý" data-left="176" data-top="170" data-width="151" id="u1118">
		         <div class="ax_default" data-height="20" data-left="176" data-top="170" data-width="72" id="u1119">
		            <div class="ax_default" data-height="20" data-left="176" data-top="170" data-width="72" id="u1120">
		               <div class="ax_default link_button" id="u1121">
		                  <div class="" id="u1121_div">
		                  </div>
		                  <div class="text" id="u1121_text">
		                     <p>
		                        <span style={{"text-decoration":"none"}}>
		                           Quản lý
		                        </span>
		                     </p>
		                  </div>
		               </div>
		               <div class="ax_default icon" id="u1122">
		                  <img class="img" id="u1122_img" src="images/quan_ly_phan_quyen/u1117.svg"/>
		                  <div class="text" id="u1122_text" style={{"display":"none","visibility":" hidden"}}>
		                     <p>
		                     </p>
		                  </div>
		               </div>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u1123">
		            <img class="img" id="u1123_img" src="images/quan_ly_phan_quyen/u1107.svg"/>
		            <div class="text" id="u1123_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u1124">
		            <img class="img" id="u1124_img" src="images/quan_ly_phan_quyen/u1108.svg"/>
		            <div class="text" id="u1124_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="20" data-left="141" data-top="229" data-width="131" id="u1125">
		         <div class="ax_default link_button" id="u1126">
		            <div class="" id="u1126_div">
		            </div>
		            <div class="text" id="u1126_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phòng đào tạo
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default button" id="u1127">
		            <div class="" id="u1127_div">
		            </div>
		            <div class="text" id="u1127_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     &gt;
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u1128">
		            <img class="img" id="u1128_img" src="images/quan_ly_phan_quyen/u1112.svg"/>
		            <div class="text" id="u1128_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="20" data-left="176" data-top="250" data-width="83" id="u1129">
		         <div class="ax_default link_button" id="u1130">
		            <div class="" id="u1130_div">
		            </div>
		            <div class="text" id="u1130_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Giáo viên
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u1131">
		            <img class="img" id="u1131_img" src="images/quan_ly_phan_quyen/u1117.svg"/>
		            <div class="text" id="u1131_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="20" data-left="176" data-top="270" data-width="84" id="u1132">
		         <div class="ax_default link_button" id="u1133">
		            <div class="" id="u1133_div">
		            </div>
		            <div class="text" id="u1133_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Trợ giảng
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u1134">
		            <img class="img" id="u1134_img" src="images/quan_ly_phan_quyen/u1117.svg"/>
		            <div class="text" id="u1134_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="20" data-left="141" data-top="289" data-width="127" id="u1135">
		         <div class="ax_default link_button" id="u1136">
		            <div class="" id="u1136_div">
		            </div>
		            <div class="text" id="u1136_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phòng kế toán
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default button" id="u1137">
		            <div class="" id="u1137_div">
		            </div>
		            <div class="text" id="u1137_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     &gt;
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u1138">
		            <img class="img" id="u1138_img" src="images/quan_ly_phan_quyen/u1112.svg"/>
		            <div class="text" id="u1138_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="20" data-left="176" data-top="309" data-width="74" id="u1139">
		         <div class="ax_default link_button" id="u1140">
		            <div class="" id="u1140_div">
		            </div>
		            <div class="text" id="u1140_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Thủ quỹ
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u1141">
		            <img class="img" id="u1141_img" src="images/quan_ly_phan_quyen/u1117.svg"/>
		            <div class="text" id="u1141_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="20" data-left="176" data-top="329" data-width="72" id="u1142">
		         <div class="ax_default link_button" id="u1143">
		            <div class="" id="u1143_div">
		            </div>
		            <div class="text" id="u1143_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Kế toán
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u1144">
		            <img class="img" id="u1144_img" src="images/quan_ly_phan_quyen/u1117.svg"/>
		            <div class="text" id="u1144_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="20" data-left="139" data-top="354" data-width="147" id="u1145">
		         <div class="ax_default link_button" id="u1146">
		            <div class="" id="u1146_div">
		            </div>
		            <div class="text" id="u1146_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Phòng marketing
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default button" id="u1147">
		            <div class="" id="u1147_div">
		            </div>
		            <div class="text" id="u1147_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     &gt;
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u1148">
		            <img class="img" id="u1148_img" src="images/quan_ly_phan_quyen/u1112.svg"/>
		            <div class="text" id="u1148_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default" data-height="20" data-left="176" data-top="376" data-width="87" id="u1149">
		         <div class="ax_default link_button" id="u1150">
		            <div class="" id="u1150_div">
		            </div>
		            <div class="text" id="u1150_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Nhân viên
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u1151">
		            <img class="img" id="u1151_img" src="images/quan_ly_phan_quyen/u1117.svg"/>
		            <div class="text" id="u1151_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u1152">
		         <img class="img" id="u1152_img" src="images/quan_ly_phan_quyen/u1107.svg"/>
		         <div class="text" id="u1152_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u1153">
		         <img class="img" id="u1153_img" src="images/quan_ly_phan_quyen/u1108.svg"/>
		         <div class="text" id="u1153_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u1154">
		         <img class="img" id="u1154_img" src="images/quan_ly_phan_quyen/u1107.svg"/>
		         <div class="text" id="u1154_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u1155">
		         <img class="img" id="u1155_img" src="images/quan_ly_phan_quyen/u1108.svg"/>
		         <div class="text" id="u1155_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u1156">
		         <img class="img" id="u1156_img" src="images/quan_ly_phan_quyen/u1107.svg"/>
		         <div class="text" id="u1156_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u1157">
		         <img class="img" id="u1157_img" src="images/quan_ly_phan_quyen/u1108.svg"/>
		         <div class="text" id="u1157_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u1158">
		         <img class="img" id="u1158_img" src="images/quan_ly_phan_quyen/u1107.svg"/>
		         <div class="text" id="u1158_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u1159">
		         <img class="img" id="u1159_img" src="images/quan_ly_phan_quyen/u1108.svg"/>
		         <div class="text" id="u1159_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u1160">
		         <img class="img" id="u1160_img" src="images/quan_ly_phan_quyen/u1107.svg"/>
		         <div class="text" id="u1160_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u1161">
		         <img class="img" id="u1161_img" src="images/quan_ly_phan_quyen/u1108.svg"/>
		         <div class="text" id="u1161_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u1162">
		         <img class="img" id="u1162_img" src="images/quan_ly_phan_quyen/u1107.svg"/>
		         <div class="text" id="u1162_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u1163">
		         <img class="img" id="u1163_img" src="images/quan_ly_phan_quyen/u1108.svg"/>
		         <div class="text" id="u1163_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u1164">
		         <img class="img" id="u1164_img" src="images/quan_ly_phan_quyen/u1107.svg"/>
		         <div class="text" id="u1164_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u1165">
		         <img class="img" id="u1165_img" src="images/quan_ly_phan_quyen/u1108.svg"/>
		         <div class="text" id="u1165_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u1166">
		         <img class="img" id="u1166_img" src="images/quan_ly_phan_quyen/u1107.svg"/>
		         <div class="text" id="u1166_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u1167">
		         <img class="img" id="u1167_img" src="images/quan_ly_phan_quyen/u1108.svg"/>
		         <div class="text" id="u1167_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u1168">
		         <img class="img" id="u1168_img" src="images/quan_ly_phan_quyen/u1107.svg"/>
		         <div class="text" id="u1168_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u1169">
		         <img class="img" id="u1169_img" src="images/quan_ly_phan_quyen/u1108.svg"/>
		         <div class="text" id="u1169_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon ax_default_hidden" id="u1170" style={{"display":"none","visibility":" hidden"}}>
		         <img class="img" id="u1170_img" src="images/quan_ly_phan_quyen/u1107.svg"/>
		         <div class="text" id="u1170_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon ax_default_hidden" id="u1171" style={{"display":"none","visibility":" hidden"}}>
		         <img class="img" id="u1171_img" src="images/quan_ly_phan_quyen/u1108.svg"/>
		         <div class="text" id="u1171_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default" data-height="0" data-left="0" data-top="0" data-width="0" id="u1172">
		         <div class="ax_default button ax_default_hidden" id="u1173" style={{"display":"none","visibility":" hidden"}}>
		            <div class="" id="u1173_div">
		            </div>
		            <div class="text" id="u1173_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     &gt;
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon ax_default_hidden" id="u1174" style={{"display":"none","visibility":" hidden"}}>
		            <img class="img" id="u1174_img" src="images/quan_ly_phan_quyen/u1112.svg"/>
		            <div class="text" id="u1174_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default text_field ax_default_hidden" data-label="phongban" id="u1175" style={{"display":"none","visibility":" hidden"}}>
		            <div class="" id="u1175_div">
		            </div>
		            <input class="u1175_input" id="u1175_input" type="text" value=""/>
		         </div>
		      </div>
		      <div class="ax_default" data-height="22" data-left="142" data-top="148" data-width="117" id="u1176">
		         <div class="ax_default button" id="u1177">
		            <div class="" id="u1177_div">
		            </div>
		            <div class="text" id="u1177_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     &gt;
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default icon" id="u1178">
		            <img class="img" id="u1178_img" src="images/quan_ly_phan_quyen/u1112.svg"/>
		            <div class="text" id="u1178_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default text_field" data-label="bangiamdoc" id="u1179">
		            <div class="" id="u1179_div">
		            </div>
		            <input class="u1179_input" id="u1179_input" readonly="" type="text" value="Ban giám đốc"/>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="55" data-label="Tìm kiếm" data-left="409" data-top="108" data-width="402.000186732226" id="u1180" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default label" id="u1181">
		         <div class="" id="u1181_div">
		         </div>
		         <div class="text" id="u1181_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Phân quyền cho vai trò
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default text_field" id="u1182">
		         <div class="" id="u1182_div">
		         </div>
		         <input class="u1182_input" id="u1182_input" type="text" value=""/>
		      </div>
		      <div class="ax_default icon" id="u1183">
		         <img class="img" id="u1183_img" src="images/quan_ly_phan_quyen/u1183.svg"/>
		         <div class="text" id="u1183_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default label" id="u1184">
		         <div class="" id="u1184_div">
		         </div>
		         <div class="text" id="u1184_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Tìm kiếm:
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default line" id="u1185">
		         <img class="img" id="u1185_img" src="images/quan_ly_phan_quyen/u1185.svg"/>
		         <div class="text" id="u1185_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default text_field" data-label="Vaitro" id="u1186">
		      <div class="" id="u1186_div">
		      </div>
		      <input class="u1186_input" id="u1186_input" readonly="" type="text" value=""/>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="390" data-label="Table" data-left="409" data-top="203" data-width="1102" id="u1187" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default" id="u1188">
		         <div class="ax_default table_cell" id="u1189">
		            <img class="img" id="u1189_img" src="images/quan_ly_phan_quyen/u1189.png"/>
		            <div class="text" id="u1189_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Quyền
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u1190">
		            <img class="img" id="u1190_img" src="images/quan_ly_phan_quyen/u1190.png"/>
		            <div class="text" id="u1190_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u1191">
		            <img class="img" id="u1191_img" src="images/quan_ly_phan_quyen/u1191.png"/>
		            <div class="text" id="u1191_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tài khoản
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u1192">
		            <img class="img" id="u1192_img" src="images/quan_ly_phan_quyen/u1192.png"/>
		            <div class="text" id="u1192_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u1193">
		            <img class="img" id="u1193_img" src="images/quan_ly_phan_quyen/u1193.png"/>
		            <div class="text" id="u1193_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Tạo mới tài khoản
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u1194">
		            <img class="img" id="u1194_img" src="images/quan_ly_phan_quyen/u1194.png"/>
		            <div class="text" id="u1194_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u1195">
		            <img class="img" id="u1195_img" src="images/quan_ly_phan_quyen/u1193.png"/>
		            <div class="text" id="u1195_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Chỉnh sửa tài khoản
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u1196">
		            <img class="img" id="u1196_img" src="images/quan_ly_phan_quyen/u1194.png"/>
		            <div class="text" id="u1196_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u1197">
		            <img class="img" id="u1197_img" src="images/quan_ly_phan_quyen/u1193.png"/>
		            <div class="text" id="u1197_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Khóa tài khoản
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u1198">
		            <img class="img" id="u1198_img" src="images/quan_ly_phan_quyen/u1194.png"/>
		            <div class="text" id="u1198_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u1199">
		            <img class="img" id="u1199_img" src="images/quan_ly_phan_quyen/u1193.png"/>
		            <div class="text" id="u1199_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Mở khóa tài khoản
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u1200">
		            <img class="img" id="u1200_img" src="images/quan_ly_phan_quyen/u1194.png"/>
		            <div class="text" id="u1200_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u1201">
		            <img class="img" id="u1201_img" src="images/quan_ly_phan_quyen/u1193.png"/>
		            <div class="text" id="u1201_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Xóa tài khoản
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u1202">
		            <img class="img" id="u1202_img" src="images/quan_ly_phan_quyen/u1194.png"/>
		            <div class="text" id="u1202_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u1203">
		            <img class="img" id="u1203_img" src="images/quan_ly_phan_quyen/u1193.png"/>
		            <div class="text" id="u1203_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Reset mật khẩu
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u1204">
		            <img class="img" id="u1204_img" src="images/quan_ly_phan_quyen/u1194.png"/>
		            <div class="text" id="u1204_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u1205">
		            <img class="img" id="u1205_img" src="images/quan_ly_phan_quyen/u1191.png"/>
		            <div class="text" id="u1205_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Cơ sở
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u1206">
		            <img class="img" id="u1206_img" src="images/quan_ly_phan_quyen/u1192.png"/>
		            <div class="text" id="u1206_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u1207">
		            <img class="img" id="u1207_img" src="images/quan_ly_phan_quyen/u1193.png"/>
		            <div class="text" id="u1207_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Thêm cơ sở
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u1208">
		            <img class="img" id="u1208_img" src="images/quan_ly_phan_quyen/u1194.png"/>
		            <div class="text" id="u1208_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u1209">
		            <img class="img" id="u1209_img" src="images/quan_ly_phan_quyen/u1193.png"/>
		            <div class="text" id="u1209_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Xóa cơ sở
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u1210">
		            <img class="img" id="u1210_img" src="images/quan_ly_phan_quyen/u1194.png"/>
		            <div class="text" id="u1210_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u1211">
		            <img class="img" id="u1211_img" src="images/quan_ly_phan_quyen/u1193.png"/>
		            <div class="text" id="u1211_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Cập nhật cơ sở
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u1212">
		            <img class="img" id="u1212_img" src="images/quan_ly_phan_quyen/u1194.png"/>
		            <div class="text" id="u1212_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u1213">
		            <img class="img" id="u1213_img" src="images/quan_ly_phan_quyen/u1193.png"/>
		            <div class="text" id="u1213_text">
		               <p>
		                  <span style={{"text-decoration":"none"}}>
		                     Quản lý cơ sở
		                  </span>
		               </p>
		            </div>
		         </div>
		         <div class="ax_default table_cell" id="u1214">
		            <img class="img" id="u1214_img" src="images/quan_ly_phan_quyen/u1194.png"/>
		            <div class="text" id="u1214_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </div>
		      </div>
		      <div class="ax_default text_field" data-label="Vaitro2" id="u1215">
		         <div class="" id="u1215_div">
		         </div>
		         <input class="u1215_input" id="u1215_input" readonly="" type="text" value=""/>
		      </div>
		      <div class="ax_default checkbox" id="u1216">
		         <label for="u1216_input" id="u1216_input_label" style={{"position":" absolute","left":" 0px"}}>
		            <img class="img" id="u1216_img" src="images/quan_ly_phan_quyen/u1216.svg"/>
		            <div class="text" id="u1216_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </label>
		         <input id="u1216_input" type="checkbox" value="checkbox"/>
		      </div>
		      <div class="ax_default" data-height="0" data-label="Tài khoản" data-left="0" data-top="0" data-width="0" id="u1217">
		         <div class="ax_default checkbox" id="u1218">
		            <label for="u1218_input" id="u1218_input_label" style={{"position":" absolute","left":" 0px"}}>
		               <img class="img" id="u1218_img" src="images/quan_ly_phan_quyen/u1218.svg"/>
		               <div class="text" id="u1218_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </label>
		            <input id="u1218_input" type="checkbox" value="checkbox"/>
		         </div>
		         <div class="ax_default checkbox" id="u1219">
		            <label for="u1219_input" id="u1219_input_label" style={{"position":" absolute","left":" 0px"}}>
		               <img class="img" id="u1219_img" src="images/quan_ly_phan_quyen/u1219.svg"/>
		               <div class="text" id="u1219_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </label>
		            <input id="u1219_input" type="checkbox" value="checkbox"/>
		         </div>
		         <div class="ax_default checkbox" id="u1220">
		            <label for="u1220_input" id="u1220_input_label" style={{"position":" absolute","left":" 0px"}}>
		               <img class="img" id="u1220_img" src="images/quan_ly_phan_quyen/u1220.svg"/>
		               <div class="text" id="u1220_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </label>
		            <input id="u1220_input" type="checkbox" value="checkbox"/>
		         </div>
		         <div class="ax_default checkbox" id="u1221">
		            <label for="u1221_input" id="u1221_input_label" style={{"position":" absolute","left":" 0px"}}>
		               <img class="img" id="u1221_img" src="images/quan_ly_phan_quyen/u1221.svg"/>
		               <div class="text" id="u1221_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </label>
		            <input id="u1221_input" type="checkbox" value="checkbox"/>
		         </div>
		         <div class="ax_default checkbox" id="u1222">
		            <label for="u1222_input" id="u1222_input_label" style={{"position":" absolute","left":" 0px"}}>
		               <img class="img" id="u1222_img" src="images/quan_ly_phan_quyen/u1222.svg"/>
		               <div class="text" id="u1222_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </label>
		            <input id="u1222_input" type="checkbox" value="checkbox"/>
		         </div>
		         <div class="ax_default checkbox" id="u1223">
		            <label for="u1223_input" id="u1223_input_label" style={{"position":" absolute","left":" 0px"}}>
		               <img class="img" id="u1223_img" src="images/quan_ly_phan_quyen/u1223.svg"/>
		               <div class="text" id="u1223_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </label>
		            <input id="u1223_input" type="checkbox" value="checkbox"/>
		         </div>
		      </div>
		      <div class="ax_default checkbox" id="u1224">
		         <label for="u1224_input" id="u1224_input_label" style={{"position":" absolute","left":" 0px"}}>
		            <img class="img" id="u1224_img" src="images/quan_ly_phan_quyen/u1224.svg"/>
		            <div class="text" id="u1224_text" style={{"display":"none","visibility":" hidden"}}>
		               <p>
		               </p>
		            </div>
		         </label>
		         <input id="u1224_input" type="checkbox" value="checkbox"/>
		      </div>
		      <div class="ax_default" data-height="0" data-label="Cơ sở" data-left="0" data-top="0" data-width="0" id="u1225">
		         <div class="ax_default checkbox" id="u1226">
		            <label for="u1226_input" id="u1226_input_label" style={{"position":" absolute","left":" 0px"}}>
		               <img class="img" id="u1226_img" src="images/quan_ly_phan_quyen/u1226.svg"/>
		               <div class="text" id="u1226_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </label>
		            <input id="u1226_input" type="checkbox" value="checkbox"/>
		         </div>
		         <div class="ax_default checkbox" id="u1227">
		            <label for="u1227_input" id="u1227_input_label" style={{"position":" absolute","left":" 0px"}}>
		               <img class="img" id="u1227_img" src="images/quan_ly_phan_quyen/u1227.svg"/>
		               <div class="text" id="u1227_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </label>
		            <input id="u1227_input" type="checkbox" value="checkbox"/>
		         </div>
		         <div class="ax_default checkbox" id="u1228">
		            <label for="u1228_input" id="u1228_input_label" style={{"position":" absolute","left":" 0px"}}>
		               <img class="img" id="u1228_img" src="images/quan_ly_phan_quyen/u1228.svg"/>
		               <div class="text" id="u1228_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </label>
		            <input id="u1228_input" type="checkbox" value="checkbox"/>
		         </div>
		         <div class="ax_default checkbox" id="u1229">
		            <label for="u1229_input" id="u1229_input_label" style={{"position":" absolute","left":" 0px"}}>
		               <img class="img" id="u1229_img" src="images/quan_ly_phan_quyen/u1229.svg"/>
		               <div class="text" id="u1229_text" style={{"display":"none","visibility":" hidden"}}>
		                  <p>
		                  </p>
		               </div>
		            </label>
		            <input id="u1229_input" type="checkbox" value="checkbox"/>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default primary_button" id="u1230">
		      <div class="" id="u1230_div">
		      </div>
		      <div class="text" id="u1230_text">
		         <p>
		            <span style={{"text-decoration":"none"}}>
		               Thêm mới
		            </span>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="39" data-label="menuthemmoi" data-left="1365" data-top="148" data-width="139" id="u1231" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default box_1" id="u1232">
		         <div class="" id="u1232_div">
		         </div>
		         <div class="text" id="u1232_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default link_button" id="u1233">
		         <div class="" id="u1233_div">
		         </div>
		         <div class="text" id="u1233_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Thêm mới phòng ban
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default link_button" id="u1234">
		         <div class="" id="u1234_div">
		         </div>
		         <div class="text" id="u1234_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Thêm mới vai trò
		               </span>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default box_1 ax_default_hidden" id="u1235" style={{"display":"none","visibility":" hidden"}}>
		      <div class="" id="u1235_div">
		      </div>
		      <div class="text" id="u1235_text" style={{"display":"none","visibility":" hidden"}}>
		         <p>
		         </p>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="287" data-label="Thêm mới phòng" data-left="534" data-top="332" data-width="853" id="u1236" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default box_1" id="u1237">
		         <div class="" id="u1237_div">
		         </div>
		         <div class="text" id="u1237_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default label" id="u1238">
		         <div class="" id="u1238_div">
		         </div>
		         <div class="text" id="u1238_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Thêm mới phòng ban
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u1239">
		         <img class="img" id="u1239_img" src="images/quan_ly_phan_quyen/u1239.svg"/>
		         <div class="text" id="u1239_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default label" id="u1240">
		         <div class="" id="u1240_div">
		         </div>
		         <div class="text" id="u1240_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Phòng ban
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default text_field" data-label="tenphong" id="u1241">
		         <div class="" id="u1241_div">
		         </div>
		         <input class="u1241_input" id="u1241_input" type="text" value=""/>
		      </div>
		      <div class="ax_default line" id="u1242">
		         <img class="img" id="u1242_img" src="images/quan_ly_phan_quyen/u1242.svg"/>
		         <div class="text" id="u1242_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default primary_button" id="u1243">
		         <div class="" id="u1243_div">
		         </div>
		         <div class="text" id="u1243_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Thêm mới
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default primary_button" id="u1244">
		         <div class="" id="u1244_div">
		         </div>
		         <div class="text" id="u1244_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Huỷ
		               </span>
		            </p>
		         </div>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="287" data-label="Cập nhật phòng" data-left="534" data-top="332" data-width="853" id="u1245" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default box_1" id="u1246">
		         <div class="" id="u1246_div">
		         </div>
		         <div class="text" id="u1246_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default label" id="u1247">
		         <div class="" id="u1247_div">
		         </div>
		         <div class="text" id="u1247_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Cập nhật phòng ban
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u1248">
		         <img class="img" id="u1248_img" src="images/quan_ly_phan_quyen/u1239.svg"/>
		         <div class="text" id="u1248_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default label" id="u1249">
		         <div class="" id="u1249_div">
		         </div>
		         <div class="text" id="u1249_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Phòng ban
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default text_field" data-label="tenphongcu" id="u1250">
		         <div class="" id="u1250_div">
		         </div>
		         <input class="u1250_input" id="u1250_input" type="text" value=""/>
		      </div>
		      <div class="ax_default line" id="u1251">
		         <img class="img" id="u1251_img" src="images/quan_ly_phan_quyen/u1242.svg"/>
		         <div class="text" id="u1251_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default primary_button" id="u1252">
		         <div class="" id="u1252_div">
		         </div>
		         <div class="text" id="u1252_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Cập nhật
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default primary_button" id="u1253">
		         <div class="" id="u1253_div">
		         </div>
		         <div class="text" id="u1253_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Huỷ
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default text_field" data-label="tenpb_capnhatpb" id="u1254">
		         <div class="" id="u1254_div">
		         </div>
		         <input class="u1254_input" id="u1254_input" type="text" value=""/>
		      </div>
		   </div>
		   <div class="ax_default ax_default_hidden" data-height="287" data-label="Thêm mới vai trò" data-left="534" data-top="332" data-width="853" id="u1255" style={{"display":"none","visibility":" hidden"}}>
		      <div class="ax_default box_1" id="u1256">
		         <div class="" id="u1256_div">
		         </div>
		         <div class="text" id="u1256_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default label" id="u1257">
		         <div class="" id="u1257_div">
		         </div>
		         <div class="text" id="u1257_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Thêm mới vai trò
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default icon" id="u1258">
		         <img class="img" id="u1258_img" src="images/quan_ly_phan_quyen/u1239.svg"/>
		         <div class="text" id="u1258_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default label" id="u1259">
		         <div class="" id="u1259_div">
		         </div>
		         <div class="text" id="u1259_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Phòng ban
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default primary_button" id="u1260">
		         <div class="" id="u1260_div">
		         </div>
		         <div class="text" id="u1260_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Thêm mới
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default primary_button" id="u1261">
		         <div class="" id="u1261_div">
		         </div>
		         <div class="text" id="u1261_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Huỷ
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default label" id="u1262">
		         <div class="" id="u1262_div">
		         </div>
		         <div class="text" id="u1262_text">
		            <p>
		               <span style={{"text-decoration":"none"}}>
		                  Vai trò
		               </span>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default text_field" data-label="tenphong" id="u1263">
		         <div class="" id="u1263_div">
		         </div>
		         <input class="u1263_input" id="u1263_input" type="text" value=""/>
		      </div>
		      <div class="ax_default line" id="u1264">
		         <img class="img" id="u1264_img" src="images/quan_ly_phan_quyen/u1242.svg"/>
		         <div class="text" id="u1264_text" style={{"display":"none","visibility":" hidden"}}>
		            <p>
		            </p>
		         </div>
		      </div>
		      <div class="ax_default droplist" id="u1265">
		         <div class="" id="u1265_div">
		         </div>
		         <select class="u1265_input" id="u1265_input">
		            <option class="u1265_input_option" value="Chọn 1 giá trị">
		               Chọn 1 giá trị
		            </option>
		         </select>
		      </div>
		   </div>
		</div>
		
      </div>
    );
  }
}

export default Page;
