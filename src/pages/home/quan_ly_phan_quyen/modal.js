import React from "react";
import isNumber from "lodash/isNumber";
import { Modal, Button } from "react-bootstrap";
import { connect } from "dva";
import { Formik, ErrorMessage, Field } from "formik";

class CustomModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      show: true
    };
  }

  handleClose = isUpdate => {
    this.props.onClose(isUpdate);
  };

  render() {
    const { isCreateNew, Group, id } = this.props;
    const { name, permission } = isCreateNew
      ? {
          name: "",
          permission: []
        }
      : Group?.current_group;

    const listPermission = permission.map(item =>
      item.isChecked ? item.permissionId : ""
    );
    return (
      <Modal
        size={"lg"}
        show={this.state.show}
        onHide={() => this.handleClose()}
        backdrop="static"
        keyboard={false}
        id="AddNew"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Formik
          enableReinitialize={true}
          initialValues={{
            name: name || "",
            listPermission: listPermission || []
          }}
          validate={values => {
            const errors = {};
            !values.name && (errors.name = "Trường bắt buộc");

            return errors;
          }}
          onSubmit={(values, { setSubmitting }) => {
            const { dispatch } = this.props;
            const permissionFilterd = values.listPermission.filter(
              item => isNumber(item)
            );
            dispatch({
              type: "Global/showLoading"
            });
            dispatch({
              type: isCreateNew ? "Group/insert" : "Group/update",
              payload: {
                name: values.name,
                permission: permissionFilterd,
                id: id
              }
            })
              .then(res => {
                dispatch({
                  type: "Global/hideLoading"
                });
                !res.code
                  ? dispatch({
                      type: "Global/showSuccess"
                    })
                  : dispatch({
                      type: "Global/showError"
                    });
                !res.code && this.props.onClose(true);
              })
              .catch(() => {
                dispatch({
                  type: "Global/showError"
                });
                dispatch({
                  type: "Global/hideLoading"
                });
              });
          }}
        >
          {({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
            setFieldValue
            /* and other goodies */
          }) => (
            <form style={{ width: "100%" }} onSubmit={handleSubmit}>
              <Modal.Header closeButton>
                <Modal.Title>
                  {isCreateNew ? "Tạo nhóm quyền mới" : "Chỉnh sửa nhóm quyền"}
                </Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <div className="row">
                  <div className="col-12">
                    <div className="form-group">
                      <label htmlFor="name">Tên nhóm quyền:</label>
                      <input
                        type="text"
                        name="name"
                        className="form-control form-control-lg"
                        id="name"
                        value={values.name}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        placeholder={"Nhập Tên nhóm quyền"}
                      ></input>
                      <ErrorMessage
                        component="p"
                        className="error-message"
                        name="name"
                      ></ErrorMessage>
                    </div>
                  </div>
                  <div className="col-12">
                    <div role="group" aria-labelledby="checkbox-group">
                      <ul className="m-0">
                        {Group.list_quyen_he_thong?.map((item, index) => {
                          return (
                            <li key={`listPermission${index}`}>
                              <input
                                checked={values.listPermission[index]}
                                value={values.listPermission[index]}
                                type="checkbox"
                                name={`listPermission`}
                                id={`listPermission${index}`}
                                onChange={e => {
                                  setFieldValue(
                                    `listPermission[${index}]`,
                                    item.permissionId
                                  );
                                }}
                                onBlur={handleBlur}
                              />
                              <label htmlFor={`listPermission${index}`}>
                                &nbsp; {item.permissionName}
                              </label>
                            </li>
                          );
                        })}
                      </ul>
                    </div>
                  </div>
                </div>
              </Modal.Body>
              <Modal.Footer>
                <Button
                  variant="primary"
                  style={{ marginRight: 15 }}
                  type="submit"
                >
                  {isCreateNew ? "Tạo mới" : "Cập nhật"}
                </Button>
                <Button
                  variant={"secondary"}
                  onClick={() => this.handleClose()}
                >
                  Hủy
                </Button>
              </Modal.Footer>
            </form>
          )}
        </Formik>
      </Modal>
    );
  }
}

export default connect(({ Group, Global }) => ({
  Group,
  Global
}))(CustomModal);
