import React from "react";
import { Modal, Button } from "react-bootstrap";
import { connect } from "dva";
import { Formik, ErrorMessage } from "formik";
import moment from "moment";

class CustomModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      show: true,
      count: 0,
      centers: []
    };
  }

  handleClose = () => {
    this.props.onClose();
  };

  render() {
    const { isCreateNew, Center, Brand, id } = this.props;
    const { name, shortName, brandId, address, directorId } = isCreateNew
      ? {
          name: "",
          shortName: "",
          brandId: "",
          address: "",
          directorId: ""
        }
      : Center?.current_center;
    return (
      <Modal
        size={"lg"}
        show={this.state.show}
        onHide={this.handleClose}
        backdrop="static"
        keyboard={false}
        id="AddNew"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Formik
          enableReinitialize={true}
          initialValues={{
            name: name || "",
            shortName: shortName || "",
            brandId: brandId || "",
            address: address || "",
            directorId: directorId || ""
          }}
          validate={values => {
            const errors = {};
            !values.name && (errors.name = "Trường bắt buộc");
            !values.shortName && (errors.shortName = "Trường bắt buộc");
            !values.brandId && (errors.brandId = "Trường bắt buộc");
            !values.address && (errors.address = "Trường bắt buộc");
            return errors;
          }}
          onSubmit={(values, { setSubmitting }) => {
            const { dispatch } = this.props;
            dispatch({
              type: "Global/showLoading"
            });
            dispatch({
              type: isCreateNew ? "Center/insert" : "Center/update",
              payload: {
                name: values.name,
                shortName: values.shortName,
                brandId: values.brandId,
                address: values.address,
                directorId: values.directorId,
                id: id || undefined
              }
            })
              .then(res => {
                dispatch({
                  type: "Global/hideLoading"
                });
                !res.code
                  ? dispatch({
                      type: "Global/showSuccess"
                    })
                  : dispatch({
                      type: "Global/showError"
                    });
                !res.code && this.props.onClose(true);
              })
              .catch(() => {
                dispatch({
                  type: "Global/showError"
                });
                dispatch({
                  type: "Global/hideLoading"
                });
              });
          }}
        >
          {({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
            setFieldValue
            /* and other goodies */
          }) => (
            <form style={{ width: "100%" }} onSubmit={handleSubmit}>
              <Modal.Header closeButton>
                <Modal.Title>
                  {isCreateNew ? "Tạo trung tâm mới" : "Chỉnh sửa trung tâm"}
                </Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <div className="row">
                  <div className="col-12">
                    <div className="form-group">
                      <label htmlFor="ten_trung_tam">Tên trung tâm:</label>
                      <input
                        type="text"
                        name="name"
                        className="form-control form-control-lg"
                        id="name"
                        value={values.name}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        placeholder={"Nhập Tên trung tâm"}
                      ></input>
                      <ErrorMessage
                        component="p"
                        className="error-message"
                        name="name"
                      ></ErrorMessage>
                    </div>
                    <div className="form-group">
                      <label htmlFor="shortName">Viết tắt:</label>
                      <input
                        type="text"
                        name="shortName"
                        className="form-control form-control-lg"
                        id="shortName"
                        value={values.shortName}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        placeholder={"Nhập Tên kỳ nghỉ"}
                      ></input>
                      <ErrorMessage
                        component="p"
                        className="error-message"
                        name="shortName"
                      ></ErrorMessage>
                    </div>
                    <div className="form-group">
                      <label htmlFor="brandId">Thương hiệu:</label>
                      <select
                        className="form-control form-control-lg"
                        id="brandId"
                        name="brandId"
                        value={values.brandId}
                        onChange={handleChange}
                        onBlur={handleBlur}
                      >
                        <>
                          <option value="" disabled hidden>
                            Vui lòng chọn giá trị
                          </option>
                          {Center.list_brand?.map(item => (
                            <option
                              key={`brandId${item.id}`}
                              value={item.id}
                              disabled={item.id !== "" ? false : true}
                              selected={item.id !== "" ? false : true}
                              hidden={item.id !== "" ? false : true}
                            >
                              {item.name}
                            </option>
                          ))}
                        </>
                      </select>
                      <ErrorMessage
                        component="p"
                        className="error-message"
                        name="brandId"
                      ></ErrorMessage>
                    </div>
                    <div className="form-group">
                      <label htmlFor="address">Địa chỉ:</label>
                      <input
                        type="text"
                        name="address"
                        className="form-control form-control-lg"
                        id="address"
                        value={values.address}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        placeholder={"Nhập Địa chỉ"}
                      ></input>
                      <ErrorMessage
                        component="p"
                        className="error-message"
                        name="address"
                      ></ErrorMessage>
                    </div>
                    <div className="form-group">
                      <label htmlFor="directorId">G/Đ trung tâm:</label>
                      <select
                        className="form-control form-control-lg"
                        id="directorId"
                        value={values.directorId}
                        onChange={handleChange}
                        onBlur={handleBlur}
                      >
                        <>
                          <option value="" disabled hidden>
                            Vui lòng chọn giá trị
                          </option>
                          {Center.list_giam_doc?.map(item => (
                            <option
                              key={`directorId${item.code}`}
                              value={item.id}
                              disabled={item.id !== "" ? false : true}
                              selected={item.id !== "" ? false : true}
                              hidden={item.id !== "" ? false : true}
                            >
                              {item.name}
                            </option>
                          ))}
                        </>
                      </select>
                    </div>
                  </div>
                </div>
              </Modal.Body>
              <Modal.Footer>
                <Button
                  variant="primary"
                  style={{ marginRight: 15 }}
                  type="submit"
                >
                  {isCreateNew ? "Tạo mới" : "Cập nhật"}
                </Button>
                <Button variant={"secondary"} onClick={this.handleClose}>
                  Hủy
                </Button>
              </Modal.Footer>
            </form>
          )}
        </Formik>
      </Modal>
    );
  }
}
export default connect(({ Center, Global, Brand }) => ({
  Center,
  Global,
  Brand
}))(CustomModal);
