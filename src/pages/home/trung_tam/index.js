import React from "react";
import { connect } from "dva";
import { Button, Table, Overlay, Popover } from "react-bootstrap";
import CustomModal from "./modal";
import * as _ from "lodash";
import PageHeader from "../../../components/PageHeader";
import CustomTooltip from "../../../components/CustomTooltip";
import CustomTable from "../../../components/Table";

class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id: null,
      activeIndex: null,
      showModal: false,
      isCreateNew: false,
      target: null,
      show: false,
      tootlTipindex: null
    };
  }

  componentDidMount() {
    this.refresh();
  }

  refresh(data) {
    const { dispatch } = this.props;
    dispatch({
      type: "Global/showLoading"
    });
    dispatch({
      type: "Center/list",
      payload: data
    }).then(res => {
      dispatch({
        type: "Global/hideLoading"
      });
      !res &&
        dispatch({
          type: "Global/showError"
        });
    });
  }

  handleClick(e, index, isHide) {
    this.setState({
      show: !(index === this.state.activeIndex),
      target: isHide ? null : e.target,
      activeIndex: index === this.state.activeIndex ? null : index
    });
  }

  renderTooltip = () => {
    const { dispatch } = this.props;
    const { tootlTipindex } = this.state;
    const dataSource = [
      {
        label: "Chỉnh sửa",
        className: "fa fa-edit",
        onClick: e => {
          dispatch({
            type: "Global/showLoading"
          });
          dispatch({
            type: "Center/view",
            payload: { id: this.state.id }
          }).then(() => {
            dispatch({
              type: "Global/hideLoading"
            });
            this.setState({ isCreateNew: false, showModal: true });
          });
          this.handleClick(e, tootlTipindex, true);
        }
      },
      {
        label: "Xoá",
        className: "fa fa-trash-alt",
        onClick: e => {
          this.handleClick(e, tootlTipindex, true);
          dispatch({
            type: "Global/showLoading"
          });
          dispatch({
            type: "Center/delete_center",
            payload: { id: this.state.id }
          }).then(res => {
            if (res.code) {
              dispatch({
                type: "Global/hideLoading"
              });
              dispatch({
                type: "Global/showError"
              });
            } else {
              dispatch({
                type: "Global/showSuccess"
              });
              this.refresh();
            }
          });
        }
      }
    ];
    return (
      <CustomTooltip
        show={this.state.show}
        onHide={e => {
          this.handleClick(e, tootlTipindex);
        }}
        dataSource={dataSource}
        target={this.state.target}
      ></CustomTooltip>
    );
  };

  onClose = isUpdate => {
    this.setState({ isCreateNew: false, showModal: false });
    isUpdate && this.refresh();
  };

  render() {
    const bre = {
      title: "Quản lý trung tâm",
      breadcrums: [
        {
          title: "Home",
          path: "/"
        },
        {
          title: "Quản lý trung tâm",
          path: ""
        }
      ]
    };

    const columns = [
      {
        title: "Tên trung tâm",
        key: "name",
        class: "tb-width-300"
      },
      {
        title: "Viết tắt",
        key: "shortName",
        class: "tb-width-150"
      },
      {
        title: "Thương hiệu",
        key: "brandName",
        class: "tb-width-150"
      },
      {
        title: "Địa chỉ",
        key: "address",
        class: "tb-width-300"
      },
      {
        title: "",
        key: "action",
        class: "tb-width-80",
        render: (col, index) => (
          <>
            <i
              className="fa fa-ellipsis-h"
              style={{
                marginRight: 0,
                marginLeft: "auto"
              }}
              onClick={e => {
                e.preventDefault();
                this.handleClick(e, index);
                this.setState({ id: col.id, tootlTipindex: index });
              }}
            />
          </>
        )
      }
    ];
    return (
      <>
        <PageHeader {...bre}></PageHeader>
        <div className="iq-card">
          <div className="iq-card-body">
            <div className="row">
              <div className="col-12 text-right">
                <div className="form-group">
                  <Button
                    variant="primary"
                    type="submit"
                    onClick={() =>
                      this.setState({ isCreateNew: true, showModal: true })
                    }
                  >
                    Tạo mới
                  </Button>
                </div>
              </div>
              <div className="col-12">
                <CustomTable
                  dataSource={this.props.Center.list_center}
                  total={this.props.Center.total_record}
                  columns={columns}
                  onChange={data => this.updateOptions(data)}
                  noPaging
                ></CustomTable>
              </div>
            </div>
          </div>

          {this.state.showModal && (
            <CustomModal
              isCreateNew={this.state.isCreateNew}
              onClose={this.onClose}
              selectedItem={this.state.selectedItem}
              id={this.state.id}
            ></CustomModal>
          )}
        </div>
        {this.renderTooltip()}
      </>
    );
  }
}

export default connect(({ Center, Global }) => ({
  Center,
  Global
}))(Page);
