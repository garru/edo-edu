export const HEADER = [
  {
    title: "Tên trung tâm",
    key: "name",
    class: "tb-width-300"
  },
  {
    title: "Viết tắt",
    key: "shortName",
    class: "tb-width-150"
  },
  {
    title: "Thương hiệu",
    key: "brandName",
    class: "tb-width-150"
  },
  {
    title: "Địa chỉ",
    key: "address",
    class: "tb-width-300"
  }
];


export const DATA = [
  {
    id: "1",
    ten_trung_tam: "Trung tam ABC",
    viet_tat: "TTT",
    thuong_hieu: "ABC",
    dia_chi: "123 Nguyễn Oanh"
  },
  {
    id: "1",
    ten_trung_tam: "Trung tam ABC",
    viet_tat: "TTT",
    thuong_hieu: "ABC",
    dia_chi: "123 Nguyễn Oanh"
  },
  {
    id: "1",
    ten_trung_tam: "Trung tam ABC",
    viet_tat: "TTT",
    thuong_hieu: "ABC",
    dia_chi: "123 Nguyễn Oanh"
  },
  {
    id: "1",
    ten_trung_tam: "Trung tam ABC",
    viet_tat: "TTT",
    thuong_hieu: "ABC",
    dia_chi: "123 Nguyễn Oanh"
  },
  {
    id: "1",
    ten_trung_tam: "Trung tam ABC",
    viet_tat: "TTT",
    thuong_hieu: "ABC",
    dia_chi: "123 Nguyễn Oanh"
  },
];
