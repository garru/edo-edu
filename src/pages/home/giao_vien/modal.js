import React, { useState, useForm } from "react";
import { Modal, Button, Table } from "react-bootstrap";
import { HEADER } from "./data";
import { Link } from "react-router-dom";

export default function CustomModal(props) {
  const [show, setShow] = useState(true);
  const [sortField, setSortField] = useState("hoc_lieu");
  const handleClose = () => {
    // setShow(false);
    props.onClose();
  };
  const { isCreateNew, selectedEvent, range } = props;
  return (
    <Modal
      size={"lg"}
      show={show}
      onHide={handleClose}
      backdrop="static"
      keyboard={false}
      id="AddNew"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title>Thông tin lớp học</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <fieldset className="m-b-2">
          <legend>Thông tin chung</legend>
          <div className="row ">
            <div className="col-lg-6 col-md-6 col-sm-12">
              <p>
                Tên bài học: <b>{selectedEvent?.lessonName}</b>
              </p>
            </div>
            <div className="col-lg-6 col-md-6 col-sm-12">
              <p>
                Tên bài học: <b>{selectedEvent?.lessonName}</b>
              </p>
            </div>
            <div className="col-lg-6 col-md-6 col-sm-12">
              <p>
                Sỹ số tạm tinh: <b>{selectedEvent?.estimateStudent}</b>
              </p>
            </div>
            <div className="col-lg-6 col-md-6 col-sm-12">
              <p>
                Học sinh tạm thời: <b>{selectedEvent?.temporaryStudent}</b>
              </p>
            </div>
            <div className="col-lg-6 col-md-6 col-sm-12">
              <div className="form-group">
                <label htmlFor="mo_ta">Mô tả</label>
                <textarea
                  className="textarea form-control"
                  rows="2"
                  name="mo_ta"
                  defaultValue={selectedEvent?.lessonDesc}
                ></textarea>
              </div>
            </div>

            <div className="col-lg-6 col-md-6 col-sm-12">
              <div>
                <input
                  type="checkbox"
                  name="isTest"
                  defaultChecked={selectedEvent?.isTest}
                />
                <label htmlFor="isTest">&nbsp; Là buổi kiểm tra</label>
              </div>
              <div>
                <input
                  type="checkbox"
                  name="hasForeign"
                  defaultChecked={selectedEvent?.hasForeign}
                />
                <label htmlFor="hasForeign">&nbsp; Giáo viên nước ngoài</label>
              </div>
            </div>

            <div className="col-12">
              <div className="status-group">
                <div className="status">{selectedEvent?.status}</div>
                <div className="status">{selectedEvent?.comment}</div>
              </div>
            </div>
          </div>
        </fieldset>
        <fieldset>
          <legend>Chi tiết bài học</legend>
          <div className="row">
            <div className="col-12">
              <div className="form-group">
                <label htmlFor="lessonContent">Nội dung</label>
                <textarea
                  className="textarea form-control"
                  rows="4"
                  name="lessonContent"
                  defaultValue={selectedEvent?.lessonContent}
                ></textarea>
              </div>
            </div>
            <div className="col-12">
              <div className="form-group">
                <label htmlFor="lessonHomework">Bài về nhà</label>
                <textarea
                  className="textarea form-control"
                  rows="4"
                  name="lessonHomework"
                  defaultValue={selectedEvent?.lessonHomework}
                ></textarea>
              </div>
            </div>
            <div className="col-12">
              <label>Các file sử dụng</label>
              {selectedEvent?.fileList && (
                <ul>
                  {selectedEvent?.fileList.map(item => (
                    <li key={`file${item.code}`}>
                      <a href={item.url}>
                        <i>{item.name}</i>
                      </a>
                    </li>
                  ))}
                </ul>
              )}
            </div>
            <div className="col-12">
              <div className="form-group m-0">
                <label htmlFor="noi_dung">Nội dung</label>
                <div style={{ width: "100%" }}>
                  <div style={{ overflowX: "auto", width: "100%" }}>
                    <Table
                      bordered
                      hover
                      // style={{ minWidth: 1500 }}
                      className="table m-0"
                    >
                      <thead>
                        <tr>
                          {HEADER.map(item => (
                            <th
                              className={`${item.class} ${
                                item.hasSort ? "sort" : ""
                              } ${sortField === item.key ? "active" : ""}`}
                              key={item.key}
                            >
                              <span>{item.title}</span>
                            </th>
                          ))}
                        </tr>
                      </thead>
                      <tbody>
                        {selectedEvent?.coursewareList.map((item, index) => (
                          <React.Fragment>
                            <tr key={`events${index}`}>
                              <td>{item.courseware}</td>

                              <td>{item.quantity}</td>
                              <td>{item.distribution}</td>
                              <td>
                                <input
                                  type="checkbox"
                                  name={`events${index}`}
                                  defaultChecked={item.giveBack}
                                />
                              </td>
                            </tr>
                          </React.Fragment>
                        ))}
                      </tbody>
                    </Table>
                    {selectedEvent?.coursewareList ||
                    selectedEvent?.coursewareList.length ? (
                      ""
                    ) : (
                      <div className="not-found">
                        <i class="fas fa-inbox"></i>
                        <span>Không tìm thấy kết quả</span>
                      </div>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </fieldset>
      </Modal.Body>
      <Modal.Footer>
        <Link to="/diem_danh">
          <Button variant="primary" style={{ marginRight: 15 }}>
            Điểm danh/Nhận xét
          </Button>
        </Link>
        <Button variant={"secondary"} onClick={handleClose}>
          Hủy
        </Button>
      </Modal.Footer>
    </Modal>
  );
}
