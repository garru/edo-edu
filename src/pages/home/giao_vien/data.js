export const HEADER = [
  {
    title: "Học liệu",
    key: "hoc_lieu",
    class: "tb-width-400"
  },
  {
    title: "Số lượng",
    key: "so_luong",
    class: "tb-width-100"
  },
  {
    title: "Phân phối",
    key: "phan_phoi",
    class: "tb-width-150"
  },
  {
    title: "Hoàn",
    key: "hoan",
    class: "tb-width-50"
  }
];


export const DATA = [
    {
        hoc_lieu: "Truyện ABC",
        so_luong: 1,
        phan_phoi: "Cả lớp",
        hoan: true
    },
    {
        hoc_lieu: "Truyện ABC",
        so_luong: 1,
        phan_phoi: "Cả lớp",
        hoan: true
    },
]


export const DATA_NGAY_CONG = {
  gio_hoc_xac_nhan: 14,
  gio_hoc_chua_xac_nhan: 14,
  gio_hoc_con_lai: 14
};

export const HEADER_THONG_TIN = [
  {
    title: "Học liệu",
    key: "hoc_lieu",
    class: "tb-width-300"
  },
  {
    title: "Số lượng",
    key: "so_luong",
    class: "tb-width-100"
  },
  {
    title: "Phân phối",
    key: "phan_phoi",
    class: "tb-width-200"
  },
  {
    title: "Hoàn",
    key: "hoan",
    class: "tb-width-100"
  }
];

export const DATA_THONG_TIN = [
  {
    hoc_lieu: "Truyện ABC",
    so_luong: "1",
    phan_phoi: "Cả lớp",
    hoan: "1",
    thao_tac: ""
  },
  {
    hoc_lieu: "Truyện ABC",
    so_luong: "1",
    phan_phoi: "Cả lớp",
    hoan: "1",
    thao_tac: ""
  },
  {
    hoc_lieu: "Truyện ABC",
    so_luong: "1",
    phan_phoi: "Cả lớp",
    hoan: "1",
    thao_tac: ""
  }
];

export const HEADER_THONG_TIN_BOOK = [
  {
    title: "Học liệu",
    key: "hoc_lieu",
    class: "tb-width-300"
  },
  {
    title: "Số lượng",
    key: "so_luong",
    class: "tb-width-100"
  },
  {
    title: "Phân phối",
    key: "phan_phoi",
    class: "tb-width-200"
  },
  {
    title: "Hoàn",
    key: "hoan",
    class: "tb-width-100"
  },
  {
    title: "Thực lĩnh",
    key: "thuc_linh",
    class: "tb-width-100"
  },
  {
    title: "Thực trả",
    key: "thuc_tra",
    class: "tb-width-100"
  }
];

export const DATA_THONG_TIN_BOOK = [
  {
    hoc_lieu: "Truyện ABC",
    so_luong: "1",
    phan_phoi: "Cả lớp",
    hoan: "1",
    thao_tac: "",
    thuc_linh: 1,
    thuc_tra: 1
  },
  {
    hoc_lieu: "Truyện ABC",
    so_luong: "1",
    phan_phoi: "Cả lớp",
    hoan: "1",
    thao_tac: "",
    thuc_linh: 1,
    thuc_tra: 1
  },
  {
    hoc_lieu: "Truyện ABC",
    so_luong: "1",
    phan_phoi: "Cả lớp",
    hoan: "1",
    thao_tac: "",
    thuc_linh: 1,
    thuc_tra: 1
  }
];
