import React, { useState, useForm } from "react";
import { Modal, Button, Table } from "react-bootstrap";
import { DATA_THONG_TIN_BOOK, HEADER_THONG_TIN_BOOK } from "./data";
import Paging from "../../../components/Paging";

export default function ModalBook(props) {
  const [show, setShow] = useState(true);
  const [listData, setListData] = useState(DATA_THONG_TIN_BOOK);
  const [sortField, setSortField] = useState("hoc_lieu");
  const handleClose = () => {
    // setShow(false);
    props.onClose();
  };
  const { isCreateNew, selectedEvent, range } = props;
  return (
    <Modal
      size={"lg"}
      show={show}
      onHide={handleClose}
      backdrop="static"
      keyboard={false}
      id="AddNew"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title>
          {isCreateNew ? "Tạo lớp mới" : "Chỉnh sửa lớp"}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div>
          <form>
            <div className="form-group">
              <label htmlFor="noi_dung">Nội dung</label>
              <div style={{ width: "100%" }}>
                <div style={{ overflowX: "auto", width: "100%" }}>
                  <Table
                    bordered
                    hover
                    // style={{ minWidth: 1500 }}
                    className="table"
                  >
                    <thead>
                      <tr>
                        {HEADER_THONG_TIN_BOOK.map(item => (
                          <th
                            className={`${item.class} ${
                              item.hasSort ? "sort" : ""
                            } ${sortField === item.key ? "active" : ""}`}
                            key={item.key}
                          >
                            <span>{item.title}</span>
                          </th>
                        ))}
                      </tr>
                    </thead>
                    <tbody>
                      {listData.map((item, index) => (
                        <React.Fragment>
                          <tr key={`account_${item.id}`}>
                            <td>{item.hoc_lieu}</td>
                            <td>{item.so_luong}</td>
                            <td>{item.phan_phoi}</td>
                            <td>
                              <input
                                type="checkbox"
                                name={`account_${item.id}`}
                                checked={item.hoan}
                              />
                            </td>
                            <td>{item.thuc_linh}</td>
                            <td>{item.thuc_tra}</td>
                          </tr>
                        </React.Fragment>
                      ))}
                    </tbody>
                  </Table>
                  {listData && listData.length ? (
                    ""
                  ) : (
                    <div className="not-found">
                      <i class="fas fa-inbox"></i>
                      <span>Không tìm thấy kết quả</span>
                    </div>
                  )}
                </div>
              </div>
              <Paging
                pageNumber={10}
                total={550}
                pageSize={10}
                onChange={console.log}
              ></Paging>
            </div>
          </form>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <div className="row">
          <div className="col-6">
            <Button variant="primary" style={{ marginRight: 30 }}>
              Nhận đủ
            </Button>
            <Button variant={"secondary"} onClick={handleClose}>
              Trả đủ
            </Button>
          </div>
          <div className="col-6" style={{ textAlign: "right" }}>
            <Button variant="primary" style={{ marginRight: 30 }}>
              Gởi trả
            </Button>
            <Button variant={"secondary"} onClick={handleClose}>
              Đã hoàn
            </Button>
          </div>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
