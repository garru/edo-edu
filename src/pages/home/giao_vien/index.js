import React from "react";
import { Calendar, momentLocalizer, Views } from "react-big-calendar";
import moment from "moment";
import events from "./events";
import { connect } from "dva";
import CustomModal from "./modal";
import ModalBook from "./modal_book";
import { Link } from "react-router-dom";
import PageHeader from "../../../components/PageHeader";
import IconTable from "../../../components/IconTable";
import CustomTable from "../../../components/Table";
import CustomToolbar from "../../../components/CustomToolbar";

const ColoredDateCellWrapper = ({ children }) =>
  React.cloneElement(React.Children.only(children), {
    style: {
      backgroundColor: "lightblue"
    }
  });
const localizer = momentLocalizer(moment);

class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
      isCreateNew: false,
      selectedEvent: {},
      showModalBook: false
    };
  }

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: "Global/showLoading"
    });
    Promise.all([
      dispatch({
        type: "Dashboard/schedulenext",
        payload: {}
      }),
      dispatch({
        type: "Dashboard/todolist",
        payload: {}
      }),
      dispatch({
        type: "Dashboard/workdaystatistic",
        payload: {}
      })
    ]).then(data => {
      dispatch({
        type: "Global/hideLoading"
      });
      if (data) {
      } else {
        dispatch({
          type: "Global/showError"
        });
      }
    });
  }

  onOpenModal(id) {
    const { dispatch } = this.props;
    dispatch({
      type: "Global/showLoading"
    });
    dispatch({
      type: "ScheduleCalendar/view",
      payload: {
        id
      }
    }).then(data => {
      dispatch({
        type: "Global/hideLoading"
      });
      if (data) {
        console.log("selectedEvent", data);
        this.setState({ showModal: true, selectedEvent: data?.content });
      } else {
        dispatch({
          type: "Global/showError"
        });
      }
    });
  }

  render() {
    console.log("Props", this.props);
    const onClose = () => {
      this.setState({
        isCreateNew: false,
        showModal: false,
        showModalBook: false
      });
    };

    const now = moment().format("DD/MM/YYYY");

    const bre = {
      title: `Hôm nay, ${now}`,
      breadcrums: [
        {
          title: "Home",
          path: "/"
        },
        {
          title: `Hôm nay, ${now}`,
          path: ""
        }
      ]
    };

    const columns_lop_hoc = [
      {
        title: "Tên lớp",
        key: "className",
        class: "tb-width-300"
      },
      {
        title: "Cơ sở",
        key: "centerName",
        class: "tb-width-200"
      },
      {
        title: "Ngày giờ",
        key: "scheduleText",
        class: "tb-width-150"
      },
      {
        title: "Thông tin",
        key: "thong_tin",
        class: "tb-width-100",
        render: (col, index) => (
          <>
            <IconTable
              classes="fa fa-info-circle"
              styles={{
                marginRight: 15
              }}
              onClick={e => {
                this.onOpenModal(col.id);
              }}
              text="Thông tin"
            ></IconTable>
            <Link to={`/diem_danh/${col.id}`}>
              <IconTable
                classes="far fa-calendar-check"
                styles={{
                  marginRight: 15,
                  color: "#212529"
                }}
                onClick={e => {
                  console.log();
                }}
                text="Điểm danh"
              ></IconTable>
            </Link>
            <IconTable
              classes="fa fa-book"
              styles={{
                marginRight: 0
              }}
              onClick={e => {
                this.setState({ showModalBook: true });
              }}
              text="Book"
            ></IconTable>
          </>
        )
      }
    ];

    const columns_cong_viec = [
      {
        title: "Công việc",
        key: "taskName",
        class: ""
      },
      {
        title: "Thao tác",
        key: "thong_tin",
        class: "tb-width-100",
        render: (col, index) => (
          <>
            <IconTable
              classes="fa fa-edit"
              styles={{
                marginRight: 15
              }}
              onClick={e => {
                // this.setState({ showModal: true });
              }}
              text="Thông tin"
            ></IconTable>
          </>
        )
      }
    ];

    return (
      <React.Fragment>
        <PageHeader {...bre}></PageHeader>

        <div className="iq-card ">
          <div className=" iq-card-header d-flex justify-content-between">
            <div className="iq-header-title">
              <h6>
                <b>Lớp học sắp diễn ra</b>
              </h6>
            </div>
          </div>
          <div className="iq-card-body">
            <div className="row align-end">
              <div className="col-12">
                <CustomTable
                  dataSource={this.props.Dashboard?.list_lop_hoc}
                  total={0}
                  columns={columns_lop_hoc}
                  onChange={data => console.log()}
                  noPaging
                ></CustomTable>
              </div>
            </div>
          </div>
        </div>

        <div className="iq-card ">
          <div className=" iq-card-header d-flex justify-content-between">
            <div className="iq-header-title">
              <h6>
                <b>Công việc cần làm</b>
              </h6>
            </div>
          </div>
          <div className="iq-card-body">
            <div className="">
              <div className="row align-end">
                <div className="col-12">
                  <CustomTable
                    dataSource={this.props.Dashboard?.list_cong_viec}
                    total={0}
                    columns={columns_cong_viec}
                    onChange={data => console.log()}
                    noPaging
                  ></CustomTable>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="iq-card ">
          <div className=" iq-card-header d-flex justify-content-between">
            <div className="iq-header-title">
              <h6>
                <b>Thống kê ngày công</b>
              </h6>
            </div>
          </div>
          <div className="iq-card-body">
            <div className="">
              <React.Fragment>
                <div className="row align-end">
                  <div className="col-12">
                    {this.props.Dashboard?.list_ngay_cong?.map(
                      (item, index) => (
                        <p key={`workDay_${index}`}>
                          {item.typeName}: <b>{item.workDay}</b>
                        </p>
                      )
                    )}
                  </div>
                  <div className="col-12">
                    <Calendar
                      selectable
                      events={events}
                      views={["month"]}
                      step={60}
                      showMultiDayTimes
                      // max={dates.add(dates.endOf(new Date(2015, 17, 1), 'day'), -1, 'hours')}
                      defaultDate={new Date()}
                      components={{
                        timeSlotWrapper: ColoredDateCellWrapper
                      }}
                      localizer={localizer}
                      onSelecting={abc => console.log("abc", abc)}
                      components={{ toolbar: CustomToolbar }}
                    />
                  </div>
                </div>
                {this.state.showModal && (
                  <CustomModal
                    onClose={onClose}
                    selectedEvent={this.state.selectedEvent}
                  ></CustomModal>
                )}
                {this.state.showModalBook && (
                  <ModalBook
                    onClose={onClose}
                    selectedEvent={this.state.selectedEvent}
                  ></ModalBook>
                )}
              </React.Fragment>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default connect(({ Dashboard, Global, Student }) => ({
  Dashboard,
  Global,
  Student
}))(Page);
