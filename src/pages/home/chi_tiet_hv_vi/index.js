import React, { useState, useEffect } from "react";
import { Button, Nav } from "react-bootstrap";
import { connect } from "dva";
import "react-datepicker/dist/react-datepicker.css";
import CustomModal from "./modal";
import CustomTable from "../../../components/Table";
import UploadAvatar from "../../../components/UploadAvatar";
import { useParams } from "react-router-dom";
import PageHeader from "../../../components/PageHeader";
import moment from "moment";
import ModalBaoLuu from "./modal_bao_luu";

const Page = props => {
  const [showModal, setShowModal] = useState(false);
  const [count, setCount] = useState(0);
  const [baoLuu, setbaoLuu] = useState(false);
  const { id, classId } = useParams();
  const { dispatch, StudentDetail } = props;
  const [selectedStudent, setSelectedStudent] = useState(null);
  const bre = {
    title: "Chi tiết học viên",
    breadcrums: [
      {
        title: "Home",
        path: "/"
      },
      {
        title: "Chi tiết học viên",
        path: ""
      }
    ]
  };
  useEffect(() => {
    dispatch({
      type: "Global/showLoading"
    });
    Promise.all([
      dispatch({
        type: "StudentDetail/currentstudent",
        payload: { id, classId }
      }),
      dispatch({
        type: "StudentDetail/currentcredit",
        payload: { id, classId }
      })
    ]).then(data => {
      dispatch({
        type: "Global/hideLoading"
      });
    });
  }, [count]);

  const handleCloseModal = data => {
    setShowModal(false);
    setbaoLuu(false);
    if (data) {
      dispatch({
        type: "Global/showLoading"
      });
      dispatch({
        type: "StudentDetail/currentschedule",
        payload: { id, classId }
      }).then(data => {
        dispatch({
          type: "Global/hideLoading"
        });
      });
    }
  };

  const handleEyeIconClick = col => {
    dispatch({
      type: "Global/showLoading"
    });
    dispatch({
      type: "StudentDetail/selectlesson",
      payload: col
    });
    dispatch({
      type: "StudentDetail/viewlesson",
      payload: { studentId: selectedStudent, lessonId: col.lessonId }
    }).then(data => {
      dispatch({
        type: "Global/hideLoading"
      });
      if (data) {
        setShowModal(true);
      } else {
        dispatch({
          type: "Global/showError"
        });
      }
    });
  };

  const columns = [
    {
      title: "Ngày",
      key: "creditDate",
      class: "tb-width-150",
      render: (col, index) => (
        <span>
          {col.lessonDate ? moment(col.lessonDate).format("DD-MM-YYYY") : ""}
        </span>
      )
    },
    {
      title: "Giải trình",
      key: "creditDesc",
      class: "tb-width-300"
    },
    {
      title: "Ghi có",
      key: "creditAmount",
      class: "tb-width-100",
      render: (col, index) => (
        <span>
          {col.creditAmount?.toLocaleString("it-IT", {
            style: "currency",
            currency: "VND"
          })}
        </span>
      )
    },
    {
      title: "Ghi nợ",
      key: "debitAmount",
      class: "tb-width-150",
      render: (col, index) => (
        <span>
          {col.debitAmount?.toLocaleString("it-IT", {
            style: "currency",
            currency: "VND"
          })}
        </span>
      )
    },
    {
      title: "Số dư",
      key: "balanceAmount",
      class: "tb-width-150",
      render: (col, index) => (
        <span>
          {col.balanceAmount?.toLocaleString("it-IT", {
            style: "currency",
            currency: "VND"
          })}
        </span>
      )
    }
  ];

  return (
    <>
      <PageHeader {...bre} />
      <div className="iq-card ">
        <div className="iq-card-body">
          <div className="row">
            <div className="col-12">
              <fieldset>
                <legend>Thông tin cơ bản</legend>
                <div className="row">
                  <div className="col-md-4 col-sm-6 col-xs-12">
                    <p>
                      Họ và tên:
                      <b> {StudentDetail?.currentstudent?.fullName}</b>
                    </p>
                  </div>
                  <div className="col-md-4 col-sm-6 col-xs-12">
                    <p>
                      Lớp:
                      <b> {StudentDetail?.currentstudent?.className}</b>
                    </p>
                  </div>
                  <div className="col-md-4 col-sm-6 col-xs-12">
                    <p>
                      Số buổi còn lại :
                      <b> {StudentDetail?.currentstudent?.lessonRemain}</b>
                    </p>
                  </div>
                  <div className="col-md-4 col-sm-6 col-xs-12">
                    <p>
                      Trung tâm :
                      <b> {StudentDetail?.currentstudent?.centerName}</b>
                    </p>
                  </div>
                  <div className="col-md-4 col-sm-6 col-xs-12">
                    <p>
                      Giáo trình:
                      <b> {StudentDetail?.currentstudent?.programName}</b>
                    </p>
                  </div>
                  <div className="col-md-4 col-sm-6 col-xs-12">
                    <p>
                      Trạng thái:
                      <b> {StudentDetail?.currentstudent?.status}</b>
                    </p>
                  </div>
                  <div className="col-md-4 col-sm-6 col-xs-12">
                    <Button
                      variant="primary"
                      style={{ marginRight: 15 }}
                      onClick={() => setbaoLuu(true)}
                    >
                      Bảo lưu/Đi học lại
                    </Button>
                    <Button variant="primary">Chuyển lớp</Button>
                  </div>
                </div>
              </fieldset>
            </div>
            <div className="col-12  m-t-2">
              <Nav
                fill
                variant="pills"
                className="m-b-2"
                defaultActiveKey={`/diem_danh_hv/${id}/${classId}`}
              >
                <Nav.Item>
                  <Nav.Link
                    key="diem_danh_hv"
                    href={`/diem_danh_hv/${id}/${classId}`}
                  >
                    Thảo luận Lịch học/điểm danh
                  </Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link
                    key="bao_luu_hv"
                    href={`/bao_luu_hv/${id}/${classId}`}
                  >
                    Bảo lưu
                  </Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link
                    key="thong_tin_vi_hv"
                    href={`/thong_tin_vi_hv/${id}/${classId}`}
                  >
                    Thông tin ví
                  </Nav.Link>
                </Nav.Item>
              </Nav>
            </div>

            <div className="col-12">
              <div className="row ">
                <div className="col-lg-3 col-md-12 col-sm-12 text-center">
                  <UploadAvatar
                    src={StudentDetail?.currentstudent?.avatar}
                  ></UploadAvatar>
                </div>
                <div className="col-lg-9 col-md-12">
                  <h5 style={{ fontSize: 24 }} className="m-t-1">
                    <b>
                      Số dư hiện tại:{" "}
                      {props.StudentDetail?.currentcredit.toLocaleString(
                        "it-IT",
                        {
                          style: "currency",
                          currency: "VND"
                        }
                      )}
                    </b>
                  </h5>
                  <CustomTable
                    dataSource={props.StudentDetail?.creditHistory}
                    total={0}
                    columns={columns}
                    onChange={data => {}}
                    noPaging
                  ></CustomTable>
                </div>
              </div>
              {showModal && <CustomModal handleClose={handleCloseModal} />}
              {baoLuu && (
                <ModalBaoLuu
                  handleClose={handleCloseModal}
                  id={id}
                  classId={classId}
                />
              )}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default connect(
  ({ Student, StudentReserve, StudentDetail, Global }) => ({
    Student,
    StudentReserve,
    StudentDetail,
    Global
  })
)(Page);
