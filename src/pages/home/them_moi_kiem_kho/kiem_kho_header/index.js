import React from "react";
import { connect } from "dva";
import { Button } from "react-bootstrap";
import MultiSelect from "react-multi-select-component";
import {
  TRUNG_TAM_MULTIPLE,
  NHA_CUNG_CAP,
  KHO
} from "../../../../mock/dropdown";

class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      trung_tam: [],
      kho: []
    };
  }

  componentWillMount() {}

  render() {
    const bre = {
      title: "Thêm mới lớp học",
      breadcrums: [
        {
          title: "Home",
          path: "/"
        },
        {
          title: "Thêm mới lớp học",
          path: ""
        }
      ]
    };
    console.log("render account_header>>>");
    return (
      <fieldset className="m-b-2">
        <legend>Tìm kiếm</legend>
        <form style={{ width: "100%" }}>
          <div className="row align-end">
            <div className="col-lg-4 col-md-4 col-sm-4">
              <div className="form-group m-0">
                <label htmlFor="Cơ sở">Cơ sở:</label>
                <MultiSelect
                  options={TRUNG_TAM_MULTIPLE}
                  value={this.state.trung_tam}
                  onChange={data => this.setState({ trung_tam: data })}
                  labelledBy="Select"
                  overrideStrings={{
                    allItemsAreSelected: "Tất cả",
                    clearSearch: "Clear Search",
                    noOptions: "Không tìm thấy",
                    search: "Search",
                    selectAll: "Select All",
                    selectSomeItems: "Có thể chọn nhiều giá trị"
                  }}
                  ArrowRenderer={() => <i className="fa fa-chevron-down" />}
                />
              </div>
            </div>
            <div className="col-lg-3 col-md-3 col-sm-4">
              <div className="form-group m-0">
                <label htmlFor="danh_muc">Nhà cung cấp:</label>
                <select
                  className="form-control form-control-lg"
                  id="danh_muc"
                  name="danh_muc"
                >
                  {NHA_CUNG_CAP.map(item => (
                    <option
                      value={item.code}
                      disabled={item.code !== "" ? false : true}
                      selected={item.code !== "" ? false : true}
                      hidden={item.code !== "" ? false : true}
                    >
                      {item.text}
                    </option>
                  ))}
                </select>
              </div>
            </div>
            <div className="col-lg-3 col-md-3 col-sm-4">
              <div className="form-group m-0">
                <label htmlFor="hon_nhan">Kho:</label>
                <MultiSelect
                  options={KHO}
                  value={this.state.kho}
                  onChange={data => this.setState({ kho: data })}
                  labelledBy="Select"
                  overrideStrings={{
                    allItemsAreSelected: "Tất cả",
                    clearSearch: "Clear Search",
                    noOptions: "Không tìm thấy",
                    search: "Search",
                    selectAll: "Select All",
                    selectSomeItems: "Có thể chọn nhiều giá trị"
                  }}
                  ArrowRenderer={() => <i className="fa fa-chevron-down" />}
                />
              </div>
            </div>
            <div className="col-lg-2 col-md-2 col-sm-4 text-right">
              <Button
                variant="primary"
                type="submit"
                style={{ marginRight: 15 }}
              >
                Tìm kiếm
              </Button>
            </div>
          </div>
        </form>
      </fieldset>
    );
  }
}

export default connect(({ HealthcareService }) => ({
  HealthcareService
}))(Page);
