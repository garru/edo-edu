export const HEADER = [
  {
    title: "ID",
    key: "id",
    class: "tb-width-50"
  },
  {
    title: "Kho",
    key: "kho",
    class: "tb-width-200"
  },
  {
    title: "Tên sản phẩm",
    key: "ten_san_pham",
    class: "tb-width-300"
  },
  {
    title: "Danh mục",
    key: "danh_muc",
    class: "tb-width-150"
  },

  {
    title: "Tồn kho",
    key: "ton_kho",
    class: "tb-width-150"
  }
];

export const DATA = [
  {
    id: "1",
    ten_san_pham: "Sách Tiếng Anh 1",
    kho: "Kho 1",
    danh_muc: "500",
    ton_kho: "3"
  }
];

export const HEADER_SAN_PHAM = [
  {
    title: "ID",
    key: "id",
    class: "tb-width-50"
  },
  {
    title: "Số phiếu",
    key: "so_phieu",
    class: "tb-width-100"
  },
  {
    title: "Kho",
    key: "kho",
    class: "tb-width-100"
  },
  {
    title: "Tên sản phẩm",
    key: "ten_san_pham",
    class: "tb-width-300"
  },
  {
    title: "Số lượng nhập",
    key: "so_luong_nhap",
    class: "tb-width-200"
  },
  {
    title: "Số lượng xuất",
    key: "so_luong_xuat",
    class: "tb-width-200"
  },
  {
    title: "Giá",
    key: "gia",
    class: "tb-width-100"
  },
  {
    title: "Tổng tiền",
    key: "tong_tien",
    class: "tb-width-150"
  },

  {
    title: "Ngày tạo",
    key: "ngay_tao",
    class: "tb-width-200"
  }
];

export const DATA_SAN_PHAM = [
  {
    id: "1",
    so_phieu: "500",
    kho: "Kho 1",
    ten_san_pham: "Sách Tiếng Anh 1",
    so_luong_nhap: 1,
    so_luong_xuat: 1,
    tong_tien: "1,000,000",
    ngay_tao: "2021-02-05 15:52"
  },
  {
    id: "1",
    so_phieu: "500",
    kho: "Kho 1",
    ten_san_pham: "Sách Tiếng Anh 1",
    so_luong_nhap: 1,
    so_luong_xuat: 1,
    tong_tien: "1,000,000",
    ngay_tao: "2021-02-05 15:52"
  },
  {
    id: "1",
    so_phieu: "500",
    kho: "Kho 1",
    ten_san_pham: "Sách Tiếng Anh 1",
    so_luong_nhap: 1,
    so_luong_xuat: 1,
    tong_tien: "1,000,000",
    ngay_tao: "2021-02-05 15:52"
  }
];
