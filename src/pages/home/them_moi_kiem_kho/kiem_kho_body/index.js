import React from "react";
import { connect } from "dva";
import { Button, Table, Overlay, Popover } from "react-bootstrap";
import { DATA, HEADER_SAN_PHAM, DATA_SAN_PHAM, HEADER } from "./data";
import {
  TRUNG_TAM_MULTIPLE,
  NHA_CUNG_CAP,
  KHO
} from "../../../../mock/dropdown";
import * as _ from "lodash";
import MultiSelect from "react-multi-select-component";

class Page extends React.Component {
  constructor(props) {
    super(props);
    const initCheckedItem = DATA.map(item => false);
    this.state = {
      show: initCheckedItem,
      target: false,
      listItem: DATA,
      listItemSanPham: DATA_SAN_PHAM,
      activeIndex: null,
      trung_tam: [],
      kho: []
    };
  }

  renderTooltip(index) {
    return (
      <Overlay
        show={this.state.show[index]}
        target={this.state.target}
        placement="bottom"
        containerPadding={20}
        rootClose={true}
        rootCloseEvent={"click"}
        onHide={e => this.handleClick(e, index)}
      >
        <Popover id="popover-contained">
          <Popover.Content>
            <ul>
              <li
                onClick={e => {
                  this.handleClick(e, index, true);
                  this.setState({ isCreateNew: false, showModal: true });
                }}
              >
                <i
                  className="fa fa-edit"
                  style={{
                    marginRight: 0,
                    marginLeft: "auto"
                  }}
                />
                &nbsp; Chỉnh sửa
              </li>

              <li
                onClick={e => {
                  this.handleClick(e, index, true);
                }}
              >
                <i
                  className="fa fa-trash-alt"
                  style={{
                    marginRight: 0,
                    marginLeft: "auto"
                  }}
                />
                &nbsp; Xoá
              </li>
            </ul>
          </Popover.Content>
        </Popover>
      </Overlay>
    );
  }

  handleClick(e, index, isHide) {
    let { show, activeIndex, listItem } = this.state;
    show[index] = !show[index];
    index !== activeIndex && (show[activeIndex] = false);
    this.setState({
      show: show,
      target: isHide ? null : e.target,
      activeIndex: index,
      selectedItem: listItem[index]
    });
  }
  onClose = () => {
    this.setState({ isCreateNew: false, showModal: false });
  };
  render() {
    return (
      <div>
        <div className="iq-card m-b-2">
          <div className=" iq-card-header d-flex justify-content-between">
            <div className="iq-header-title">
              <h6>
                <b>Danh sách sản phẩm</b>
              </h6>
            </div>
          </div>
          <div className="iq-card-body">
            <div className="row">
              <div className="col-12">
                <fieldset className="m-b-2">
                  <legend>Tìm kiếm</legend>
                  <form style={{ width: "100%" }}>
                    <div className="row align-end">
                      <div className="col-lg-4 col-md-4 col-sm-4">
                        <div className="form-group m-0">
                          <label htmlFor="Cơ sở">Cơ sở:</label>
                          <MultiSelect
                            options={TRUNG_TAM_MULTIPLE}
                            value={this.state.trung_tam}
                            onChange={data =>
                              this.setState({ trung_tam: data })
                            }
                            labelledBy="Select"
                            overrideStrings={{
                              allItemsAreSelected: "Tất cả",
                              clearSearch: "Clear Search",
                              noOptions: "Không tìm thấy",
                              search: "Search",
                              selectAll: "Select All",
                              selectSomeItems: "Có thể chọn nhiều giá trị"
                            }}
                            ArrowRenderer={() => (
                              <i className="fa fa-chevron-down" />
                            )}
                          />
                        </div>
                      </div>
                      <div className="col-lg-3 col-md-3 col-sm-4">
                        <div className="form-group m-0">
                          <label htmlFor="danh_muc">Nhà cung cấp:</label>
                          <select
                            className="form-control form-control-lg"
                            id="danh_muc"
                            name="danh_muc"
                          >
                            {NHA_CUNG_CAP.map(item => (
                              <option
                                value={item.code}
                                disabled={item.code !== "" ? false : true}
                                selected={item.code !== "" ? false : true}
                                hidden={item.code !== "" ? false : true}
                              >
                                {item.text}
                              </option>
                            ))}
                          </select>
                        </div>
                      </div>
                      <div className="col-lg-3 col-md-3 col-sm-4">
                        <div className="form-group m-0">
                          <label htmlFor="hon_nhan">Kho:</label>
                          <MultiSelect
                            options={KHO}
                            value={this.state.kho}
                            onChange={data => this.setState({ kho: data })}
                            labelledBy="Select"
                            overrideStrings={{
                              allItemsAreSelected: "Tất cả",
                              clearSearch: "Clear Search",
                              noOptions: "Không tìm thấy",
                              search: "Search",
                              selectAll: "Select All",
                              selectSomeItems: "Có thể chọn nhiều giá trị"
                            }}
                            ArrowRenderer={() => (
                              <i className="fa fa-chevron-down" />
                            )}
                          />
                        </div>
                      </div>
                      <div className="col-lg-2 col-md-2 col-sm-4 text-right">
                        <Button
                          variant="primary"
                          type="submit"
                          style={{ marginRight: 15 }}
                        >
                          Tìm kiếm
                        </Button>
                      </div>
                    </div>
                  </form>
                </fieldset>
              </div>
              <div className="col-12">
                <div style={{ width: "100%" }}>
                  <div style={{ overflowX: "auto", width: "100%" }}>
                    <Table
                      bordered
                      hover
                      // style={{ minWidth: 1500 }}
                      className="table"
                    >
                      <thead>
                        <tr>
                          {HEADER.map(item => (
                            <th
                              className={`${item.class} ${
                                item.hasSort ? "sort" : ""
                              } ${
                                this.state.sortField === item.key
                                  ? "active"
                                  : ""
                              }`}
                              key={item.key}
                            >
                              <span>{item.title}</span>
                            </th>
                          ))}
                        </tr>
                      </thead>
                      <tbody>
                        {this.state.listItem.map((item, index) => (
                          <React.Fragment>
                            <tr key={`account_${item.id}`}>
                              <td>{item.id}</td>
                              <td>{item.kho}</td>
                              <td>{item.ten_san_pham}</td>
                              <td>{item.danh_muc}</td>
                              <td>{item.ton_kho}</td>
                            </tr>
                          </React.Fragment>
                        ))}
                      </tbody>
                    </Table>
                    {this.state.listItem && this.state.listItem.length ? (
                      ""
                    ) : (
                      <div className="not-found">
                        <i class="fas fa-inbox"></i>
                        <span>Không tìm thấy kết quả</span>
                      </div>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="iq-card">
          <div className=" iq-card-header d-flex justify-content-between">
            <div className="iq-header-title">
              <h6>
                <b>Danh sách sản phẩm nhập xuất trong ngày</b>
              </h6>
            </div>
          </div>
          <div className="iq-card-body">
            <div className="row">
              <div className="col-12">
                <fieldset className="m-b-2">
                  <legend>Tìm kiếm</legend>
                  <form style={{ width: "100%" }}>
                    <div className="row align-end">
                      <div className="col-lg-4 col-md-4 col-sm-4">
                        <div className="form-group m-0">
                          <label htmlFor="tim_kiem">Tìm kiếm:</label>
                          <input
                            type="text"
                            id="tim_kiem"
                            name="tim_kiem"
                            className="form-control form-control-lg"
                          />
                        </div>
                      </div>
                      <div className="col-lg-2 col-md-2 col-sm-4 text-right">
                        <Button
                          variant="primary"
                          type="submit"
                          style={{ marginRight: 15 }}
                        >
                          Tìm kiếm
                        </Button>
                      </div>
                    </div>
                  </form>
                </fieldset>
              </div>

              <div className="col-12">
                <h6>
                  <b>Danh sách sản phẩm nhập xuất trong ngày</b>
                </h6>
              </div>
              <div className="col-12">
                <div style={{ width: "100%" }}>
                  <div style={{ overflowX: "auto", width: "100%" }}>
                    <Table
                      bordered
                      hover
                      // style={{ minWidth: 1500 }}
                      className="table"
                    >
                      <thead>
                        <tr>
                          {HEADER_SAN_PHAM.map(item => (
                            <th
                              className={`${item.class} ${
                                item.hasSort ? "sort" : ""
                              } ${
                                this.state.sortField === item.key
                                  ? "active"
                                  : ""
                              }`}
                              key={item.key}
                            >
                              <span>{item.title}</span>
                            </th>
                          ))}
                        </tr>
                      </thead>
                      <tbody>
                        {this.state.listItemSanPham.map((item, index) => (
                          <React.Fragment>
                            <tr key={`account_${item.id}`}>
                              <td>{item.id}</td>
                              <td>{item.so_phieu}</td>
                              <td>{item.kho}</td>
                              <td>{item.ten_san_pham}</td>
                              <td>{item.so_luong_nhap}</td>
                              <td>{item.so_luong_xuat}</td>
                              <td>{item.gia}</td>
                              <td>{item.tong_tien}</td>
                              <td>{item.ngây_to}</td>
                            </tr>
                          </React.Fragment>
                        ))}
                      </tbody>
                    </Table>
                    {this.state.listItemSanPham &&
                    this.state.listItemSanPham.length ? (
                      ""
                    ) : (
                      <div className="not-found">
                        <i class="fas fa-inbox"></i>
                        <span>Không tìm thấy kết quả</span>
                      </div>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(({ HealthcareService }) => ({
  HealthcareService
}))(Page);
