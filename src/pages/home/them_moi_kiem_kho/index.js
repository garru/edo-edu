import React from "react";
import { connect } from "dva";
import KiemKhoHeader from "./kiem_kho_header";
import KiemKhoBody from "./kiem_kho_body";
import PageHeader from "../../../components/PageHeader";
class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillMount() {}

  render() {
    const bre = {
      title: "Thêm mới kiểm kho",
      breadcrums: [
        {
          title: "Home",
          path: "/"
        },
        {
          title: "Kiểm kho",
          path: "/kiem_kho"
        },
        {
          title: "Thêm mới kiểm kho",
          path: "/them_moi_kiem_kho"
        }
      ],
      subTitle:
        "Tất cả phiếu kiểm kho sẽ được lưu tại đây. Phiếu kiểm kho giúp kiểm soat chặt chẽ số liệu hàng hóa"
    };
    console.log("render account>>>");
    return (
      <div>
        <PageHeader {...bre}></PageHeader>
        {/* <KiemKhoHeader /> */}
        <KiemKhoBody />
      </div>
    );
  }
}

export default connect(({ HealthcareService }) => ({
  HealthcareService
}))(Page);
