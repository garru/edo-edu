import React from "react";
import { connect } from "dva";
import { Button, Table, Overlay, Popover } from "react-bootstrap";
import CustomModal from "./modal";
import * as _ from "lodash";
import PageHeader from "../../../components/PageHeader";
import CustomTooltip from "../../../components/CustomTooltip";
import CustomTable from "../../../components/Table";

class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id: null,
      activeIndex: null,
      showModal: false,
      isCreateNew: false,
      target: null,
      show: false,
      tootlTipindex: null
    };
  }

  componentWillMount() {
    this.refresh();
  }

  refresh() {
    const { dispatch } = this.props;
    dispatch({
      type: "Global/showLoading"
    });
    dispatch({
      type: "Brand/list"
    }).then(res => {
      dispatch({
        type: "Global/hideLoading"
      });
      if (res.code) {
        dispatch({
          type: "Global/showError"
        });
      }
    });
  }

  renderTooltip = () => {
    const { dispatch } = this.props;
    const { tootlTipindex } = this.state;
    const dataSource = [
      {
        label: "Chỉnh sửa",
        className: "fa fa-edit",
        onClick: e => {
          dispatch({
            type: "Global/showLoading"
          });
          dispatch({
            type: "Brand/view",
            payload: { id: this.state.id }
          }).then(() => {
            dispatch({
              type: "Global/hideLoading"
            });
            this.setState({ isCreateNew: false, showModal: true });
          });
          this.handleClick(e, tootlTipindex, true);
        }
      },
      {
        label: "Xoá",
        className: "fa fa-trash-alt",
        onClick: e => {
          this.handleClick(e, tootlTipindex, true);
          dispatch({
            type: "Global/showLoading"
          });
          dispatch({
            type: "Brand/delete_brand",
            payload: { id: this.state.id }
          }).then(res => {
            if (res.code) {
              dispatch({
                type: "Global/hideLoading"
              });
              dispatch({
                type: "Global/showError"
              });
            } else {
              dispatch({
                type: "Global/showSuccess"
              });
              this.refresh();
            }
          });
        }
      }
    ];
    return (
      <CustomTooltip
        show={this.state.show}
        onHide={e => {
          this.handleClick(e, tootlTipindex);
        }}
        dataSource={dataSource}
        target={this.state.target}
      ></CustomTooltip>
    );
  };

  handleClick(e, index, isHide) {
    this.setState({
      show: !(index === this.state.activeIndex),
      target: isHide ? null : e.target,
      activeIndex: index === this.state.activeIndex ? null : index
    });
  }

  onClose = isUpdate => {
    this.setState({ isCreateNew: false, showModal: false });
    if (isUpdate) {
      this.refresh();
    }
  };

  render() {
    const columns = [
      {
        title: "Mã thương hiệu",
        key: "code",
        class: "tb-width-150"
      },
      {
        title: "Tên tiếng Việt",
        key: "nameVI",
        class: "tb-width-200"
      },
      {
        title: "Tên tiếng Anh",
        key: "nameEN",
        class: "tb-width-200"
      },
      {
        title: "Tên viết tắt",
        key: "shortName",
        class: "tb-width-200"
      },
      {
        title: "Biểu tượng",
        key: "logoImage",
        class: "tb-width-100",
        render: (col, index) => (
          <img src={col.logoImage} alt={col.nameVI} style={{ maxWidth: 50 }} />
        )
      },
      {
        title: "",
        key: "action",
        class: "tb_width_50",
        render: (col, index) => (
          <>
            <i
              className="fa fa-ellipsis-h"
              style={{
                marginRight: 0,
                marginLeft: "auto"
              }}
              onClick={e => {
                e.preventDefault();
                this.handleClick(e, index);
                this.setState({ id: col.id, tootlTipindex: index });
              }}
            />
          </>
        )
      }
    ];

    const bre = {
      title: "Thương hiệu",
      subTitle: "Các thương hiệu được quản lý trong hệ thống",
      breadcrums: [
        {
          title: "Home",
          path: "/"
        },
        {
          title: "Thương hiệu",
          path: ""
        }
      ]
    };

    return (
      <React.Fragment>
        <PageHeader {...bre}></PageHeader>
        <div className="iq-card">
          <div className="iq-card-body">
            <div className="row">
              <div className="col-12 text-right">
                <div className="form-group">
                  <Button
                    variant="primary"
                    type="submit"
                    onClick={() =>
                      this.setState({ isCreateNew: true, showModal: true })
                    }
                  >
                    Tạo mới
                  </Button>
                </div>
              </div>
              <div className="col-12">
                <CustomTable
                  dataSource={this.props.Brand.list_brand}
                  total={this.props.Brand.total_record}
                  columns={columns}
                  onChange={data => this.updateOptions(data)}
                  noPaging
                ></CustomTable>
              </div>
            </div>
          </div>
          {this.state.showModal && (
            <CustomModal
              isCreateNew={this.state.isCreateNew}
              onClose={this.onClose}
              selectedItem={this.state.selectedItem}
              id={this.state.id}
            ></CustomModal>
          )}
          {this.renderTooltip()}
        </div>
      </React.Fragment>
    );
  }
}

export default connect(({ Brand, Center, Global }) => ({
  Brand,
  Center,
  Global
}))(Page);
