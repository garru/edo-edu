export const HEADER = [
  {
    title: "Mã thương hiệu",
    key: "ma_thuong_hieu",
    class: "tb-width-200"
  },
  {
    title: "Tên tiếng Việt",
    key: "ten_tieng_viet",
    class: "tb-width-200"
  },
  {
    title: "Tên tiếng Anh",
    key: "ten_tieng_anh",
    class: "tb-width-200"
  },
  {
    title: "Tên viết tắt",
    key: "ten_viet_tat",
    class: "tb-width-200"
  },
  {
    title: "Biểu tượng",
    key: "bieu_tuong",
    class: "tb-width-100"
  }
];

export const DATA = [
  {
    id: "1",
    ma_thuong_hieu: "TC-00001",
    ten_tieng_viet: "Bùi Anh Tuấn",
    bieu_tuong: "images/thuong_hieu.png",
    ten_tieng_anh: "Bristish Royal English Tranning",
    ten_viet_tat: "BRET"
  },
  {
    id: "1",
    ma_thuong_hieu: "TC-00001",
    ten_tieng_viet: "Bùi Anh Tuấn",
    bieu_tuong: "images/thuong_hieu.png",
    ten_tieng_anh: "Bristish Royal English Tranning",
    ten_viet_tat: "BRET"
  },
  {
    id: "1",
    ma_thuong_hieu: "TC-00001",
    ten_tieng_viet: "Bùi Anh Tuấn",
    bieu_tuong: "images/thuong_hieu.png",
    ten_tieng_anh: "Bristish Royal English Tranning",
    ten_viet_tat: "BRET"
  },
  {
    id: "1",
    ma_thuong_hieu: "TC-00001",
    ten_tieng_viet: "Bùi Anh Tuấn",
    bieu_tuong: "images/thuong_hieu.png",
    ten_tieng_anh: "Bristish Royal English Tranning",
    ten_viet_tat: "BRET"
  },
];
