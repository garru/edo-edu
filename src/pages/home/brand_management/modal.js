import React from "react";
import isEqual from "lodash/isEqual";
import cloneDeep from "lodash/cloneDeep";
import { Modal, Button } from "react-bootstrap";
import { connect } from "dva";
import { Formik, ErrorMessage } from "formik";

class CustomModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      show: true,
      count: 0,
      centers: []
    };
  }

  handleClose = isUpdate => {
    this.props.onClose(isUpdate);
  };

  render() {
    const { isCreateNew, Brand, id } = this.props;
    const { code, nameVI, nameEN, shortName, logoImage } = isCreateNew
      ? { code: "", nameVI: "", nameEN: "", shortName: "", logoImage: "" }
      : Brand?.current_brand;
    return (
      <Modal
        size={"lg"}
        show={this.state.show}
        onHide={() => this.handleClose()}
        backdrop="static"
        keyboard={false}
        id="AddNew"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Formik
          enableReinitialize={true}
          initialValues={{
            nameVI: nameVI,
            nameEN: nameEN,
            shortName: shortName,
            logoImageBase64: "",
            code: code
          }}
          validate={values => {
            const errors = {};
            !values.nameVI && (errors.nameVI = "Trường bắt buộc");
            !values.code && (errors.code = "Trường bắt buộc");
            !values.nameEN && (errors.nameEN = "Trường bắt buộc");
            !values.shortName && (errors.shortName = "Trường bắt buộc");
            !values.logoImageBase64 &&
              !logoImage &&
              (errors.logoImageBase64 = "Vui lòng chọn ảnh thương hiệu");
            return errors;
          }}
          onSubmit={(values, { setSubmitting }) => {
            const { dispatch } = this.props;
            dispatch({
              type: "Global/showLoading"
            });
            // Upload Image here
            dispatch({
              type: isCreateNew ? "Brand/insert" : "Brand/update",
              payload: {
                nameVI: values.nameVI,
                nameEN: values.nameEN,
                shortName: values.shortName,
                logoImage: values.logoImageBase64,
                code: values.code,
                id: id || undefined
              }
            })
              .then(res => {
                dispatch({
                  type: "Global/hideLoading"
                });
                !res.code
                  ? dispatch({
                      type: "Global/showSuccess"
                    })
                  : dispatch({
                      type: "Global/showError"
                    });
                !res.code && this.props.onClose(true);
              })
              .catch(() => {
                dispatch({
                  type: "Global/showError"
                });
                dispatch({
                  type: "Global/hideLoading"
                });
              });
          }}
        >
          {({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
            setFieldValue
            /* and other goodies */
          }) => (
            <form style={{ width: "100%" }} onSubmit={handleSubmit}>
              <Modal.Header closeButton>
                <Modal.Title>
                  {isCreateNew ? "Tạo thương hiệu mới" : "Cập nhật thương hiệu"}
                </Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <div className="row">
                  <div className="col-md-8 col-sm-12">
                    <div className="form-group">
                      <label htmlFor="code">Mã thương hiệu:</label>
                      <input
                        type="text"
                        name="code"
                        className="form-control form-control-lg"
                        id="code"
                        value={values.code}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        placeholder={"Nhập Mã thương hiệu"}
                      ></input>
                      <ErrorMessage
                        component="p"
                        className="error-message"
                        name="code"
                      ></ErrorMessage>
                    </div>
                    <div className="form-group">
                      <label htmlFor="shortName">Tên viết tắt:</label>
                      <input
                        type="text"
                        name="shortName"
                        className="form-control form-control-lg"
                        id="shortName"
                        value={values.shortName}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        placeholder={"Nhập Tên viết tắt"}
                      ></input>
                      <ErrorMessage
                        component="p"
                        className="error-message"
                        name="shortName"
                      ></ErrorMessage>
                    </div>
                    <div className="form-group">
                      <label htmlFor="nameVI">Tên tiếng Việt:</label>
                      <input
                        type="text"
                        name="nameVI"
                        className="form-control form-control-lg"
                        id="nameVI"
                        value={values.nameVI}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        placeholder={"Nhập Tên tiếng Việt"}
                      ></input>
                      <ErrorMessage
                        component="p"
                        className="error-message"
                        name="nameVI"
                      ></ErrorMessage>
                    </div>
                    <div className="form-group">
                      <label htmlFor="nameEN">Tên tiếng Anh:</label>
                      <input
                        type="text"
                        name="nameEN"
                        className="form-control form-control-lg"
                        id="nameEN"
                        value={values.nameEN}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        placeholder={"Nhập Tên tiếng Anh"}
                      ></input>
                      <ErrorMessage
                        component="p"
                        className="error-message"
                        name="nameEN"
                      ></ErrorMessage>
                    </div>
                  </div>
                  <div className="col-md-4 col-sm-12">
                    <div className="form-group">
                      <label htmlFor="logoImage">Biểu tượng</label>
                      <input
                        type="file"
                        name="logoImage"
                        className="form-control form-control-lg"
                        id="logoImage"
                        accept="image/*"
                        onChange={e => {
                          const file = e.target.files[0];
                          if (file) {
                            let reader = new FileReader();
                            reader.onload = e => {
                              let image = e.target.result;
                              setFieldValue("logoImageBase64", image);
                            };
                            reader.readAsDataURL(file);
                          }
                        }}
                        onBlur={handleBlur}
                      ></input>
                      <ErrorMessage
                        component="p"
                        className="error-message"
                        name="logoImageBase64"
                      ></ErrorMessage>
                    </div>
                  </div>
                </div>
              </Modal.Body>
              <Modal.Footer>
                <Button
                  variant="primary"
                  style={{ marginRight: 15 }}
                  type="submit"
                >
                  {isCreateNew ? "Tạo mới" : "Cập nhật"}
                </Button>
                <Button
                  variant={"secondary"}
                  onClick={() => this.handleClose()}
                >
                  Hủy
                </Button>
              </Modal.Footer>
            </form>
          )}
        </Formik>
      </Modal>
    );
  }
}

export default connect(({ Brand, Global }) => ({
  Brand,
  Global
}))(CustomModal);
