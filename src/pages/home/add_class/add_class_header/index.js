import React from "react";
import { connect } from "dva";
import { Button } from "react-bootstrap";
import moment from "moment";
import * as yup from "yup";
import Input from "../../../../components/Form/Input";
import Select from "../../../../components/Form/Select";
import TextArea from "../../../../components/Form/TextArea";
import MultiSelect from "react-multi-select-component";
import { Formik, ErrorMessage } from "formik";
import CustomTable from "../../../../components/Table";
import cloneDeep from "lodash/cloneDeep";
import isEqual from "lodash/isEqual";

let timeoutSearch;
class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      listautocomplete: [],
      studentList: []
    };
  }

  componentWillReceiveProps(nextProps) {
    !isEqual(this.state.listautocomplete, nextProps.Student.list_hoc_sinh) &&
      this.setState({
        listautocomplete: nextProps.Student.list_hoc_sinh || []
      });
  }

  componentDidMount() {
    const listautocomplete = cloneDeep(this.props.Student.list_hoc_sinh) || [];
    this.setState({ listautocomplete });
  }

  onSearch = e => {
    clearTimeout(timeoutSearch);
    const value = e.target?.value;
    timeoutSearch = setTimeout(() => {
      const { dispatch } = this.props;
      dispatch({
        type: "Global/showLoading"
      });
      dispatch({
        type: "Student/search",
        payload: {
          codeOrName: value
        }
      }).then(() => {
        dispatch({
          type: "Global/hideLoading"
        });
      });
    }, 300);
  };

  onChangeCenter = id => {
    const { dispatch } = this.props;
    dispatch({
      type: "Global/showLoading"
    });
    Promise.all([
      dispatch({
        type: "Class/listsurchage",
        payload: {
          centerId: id
        }
      }),
      dispatch({
        type: "Class/listmanage",
        payload: {
          centerId: id
        }
      }),
      dispatch({
        type: "Class/listprice",
        payload: {
          centerId: id
        }
      }),
      dispatch({
        type: "Class/listroom",
        payload: {
          centerId: id
        }
      })
    ]).then(
      () => {
        dispatch({
          type: "Global/hideLoading"
        });
      },
      error => {
        dispatch({
          type: "Global/hideLoading"
        });
        dispatch({
          type: "Global/showError"
        });
      }
    );
  };

  render() {
    const { Class } = this.props;

    const columns = [
      {
        title: "STT",
        key: "index",
        class: "tb-width-50",
        render: (col, index) => <span>{index}</span>
      },
      {
        title: "Mã học viên",
        key: "code",
        class: "tb-width-150"
      },
      {
        title: "Tên",
        key: "firstName",
        class: "tb-width-150"
      },
      {
        title: "Họ đệm",
        key: "lastName",
        class: "tb-width-150"
      },
      {
        title: "Số điện thoại",
        key: "phone",
        class: "tb-width-150"
      },
      {
        title: "Email",
        key: "email",
        class: "tb-width-200"
      },
      {
        title: "Đã thanh toán",
        key: "pay",
        class: "tb-width-150"
      },
      {
        title: "Thao tác",
        key: "action",
        class: "tb_width_150",
        render: (col, index) => (
          <>
            <i
              className="fa fa-trash-alt"
              style={{
                marginRight: 0,
                marginLeft: "auto"
              }}
              onClick={e => {
                const newstudentList = this.state.studentList.filter(
                  (item, idx) => index !== idx
                );
                this.setState({ studentList: newstudentList });
              }}
            />
          </>
        )
      }
    ];

    const list_phu_thu = Class?.list_phu_thu?.map(item => ({
      label: item.name,
      value: item.id
    }));

    const initialValues = {
      centerId: "",
      code: "",
      name: "",
      expectedBegin: "",
      expectedEnd: "",
      programId: "",
      roomId: "",
      studentMin: "",
      studentMax: "",
      manageId: "",
      priceId: "",
      surchargeIds: [],
      description: "",
      studentList: [],
      capacity: "",
      sessions: "",
      search: ""
    };
    return (
      <div>
        <Formik
          enableReinitialize={true}
          initialValues={initialValues}
          validationSchema={yup.object().shape({
            centerId: yup.string().required("Trường bắt buộc"),
            code: yup.string().required("Trường bắt buộc"),
            name: yup.string().required("Trường bắt buộc"),
            expectedBegin: yup.string().required("Trường bắt buộc"),
            expectedEnd: yup.string().required("Trường bắt buộc"),
            programId: yup.string().required("Trường bắt buộc"),
            roomId: yup.string().required("Trường bắt buộc"),
            studentMin: yup.string().required("Trường bắt buộc"),
            studentMax: yup.string().required("Trường bắt buộc"),
            manageId: yup.string().required("Trường bắt buộc"),
            priceId: yup.string().required("Trường bắt buộc"),
            capacity: yup.string().required("Trường bắt buộc"),
            sessions: yup.string().required("Trường bắt buộc"),
            search: yup.string().when(["sessions"], (sessions, schema) => {
              console.log("schema", schema, this.state.studentList?.length);
              return this.state.studentList?.length
                ? schema
                : schema.required("Vui lòng thêm ít nhất 1 học sinh");
            }),
            surchargeIds: yup
              .array()
              .min(1, "Trường bắt buộc")
              .required("Trường bắt buộc"),
            description: yup.string().required("Trường bắt buộc")
          })}
          onSubmit={async (values, { resetForm }) => {
            const { dispatch } = this.props;
            const studentList = this.state.studentList?.map(item => +item.id);
            const surchargeIds = values.surchargeIds.map(item => item.value);
            const submitData = {
              centerId: +values.centerId,
              code: values.code,
              name: values.name,
              expectedBegin: values.expectedBegin,
              expectedEnd: values.expectedEnd,
              programId: +values.programId,
              roomId: +values.roomId,
              studentMin: +values.studentMin,
              studentMax: +values.studentMax,
              manageId: +values.manageId,
              priceId: +values.priceId,
              surchargeIds: surchargeIds,
              description: values.description,
              studentList: studentList,
              capacity: +values.capacity,
              sessions: +values.sessions
            };
            console.log(submitData);
            dispatch({
              type: "Global/showLoading"
            });
            dispatch({
              type: "Class/insert",
              payload: submitData
            }).then(data => {
              dispatch({
                type: "Global/hideLoading"
              });
              if (data && !data.code) {
                dispatch({
                  type: "Global/showSuccess"
                });
                resetForm && resetForm(initialValues);
                // window.location.reload();
              } else {
                dispatch({
                  type: "Global/showError"
                });
              }
            });
          }}
        >
          {({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
            setFieldValue,
            resetForm
            /* and other goodies */
          }) => (
            <form onSubmit={handleSubmit}>
              <div className="iq-card">
                <div className=" iq-card-header d-flex justify-content-between">
                  <div className="iq-header-title">
                    <h6>
                      <b>Thông tin lớp học</b>
                    </h6>
                  </div>
                </div>
                <div className="iq-card-body">
                  <div className="row">
                    <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12 ">
                      <div className="form-group">
                        <Select
                          id="centerId"
                          name="centerId"
                          label="Cơ sở"
                          options={Class?.list_trung_tam}
                          value={values.centerId}
                          onChange={e => {
                            const value = e.target.value;
                            setFieldValue("centerId", value);
                            this.onChangeCenter(value);
                          }}
                          onBlur={handleBlur}
                        />
                      </div>
                    </div>
                    <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12 ">
                      <div className="form-group">
                        <Input
                          id="code"
                          name="code"
                          label="Mã lớp:"
                          value={values.code}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          placeholder={"Nhập Mã lớp"}
                        />
                      </div>
                    </div>
                    <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12 ">
                      <div className="form-group">
                        <Input
                          id="name"
                          name="name"
                          label="Tên lớp:"
                          value={values.name}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          placeholder={"Nhập Tên lớp"}
                        />
                      </div>
                    </div>
                    <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12 ">
                      <div className="form-group">
                        <Select
                          id="programId"
                          name="programId"
                          label="Chương trình: "
                          options={Class?.list_chuong_trinh}
                          value={values.programId}
                          onChange={handleChange}
                          onBlur={handleBlur}
                        />
                      </div>
                    </div>
                    <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12 ">
                      <div className="form-group">
                        <Input
                          id="expectedBegin"
                          type="date"
                          name="expectedBegin"
                          label="Bắt đầu dự kiến:"
                          value={values.expectedBegin}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          placeholder={"Nhập Tên lớp"}
                          format="YYYY-MM-DD"
                          min={moment().format("YYYY-MM-DD")}
                        />
                      </div>
                    </div>
                    <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12 ">
                      <div className="form-group">
                        <Input
                          id="expectedEnd"
                          type="date"
                          name="expectedEnd"
                          label="Kết thúc dự kiến:"
                          value={values.expectedEnd}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          placeholder={"Nhập Tên lớp"}
                          format="YYYY-MM-DD"
                          min={
                            values.expectedBegin
                              ? moment(values.expectedBegin).format(
                                  "YYYY-MM-DD"
                                )
                              : moment().format("YYYY-MM-DD")
                          }
                        />
                      </div>
                    </div>

                    <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12 ">
                      <div className="form-group">
                        <Input
                          id="sessions"
                          name="sessions"
                          label="Số buổi:"
                          type="number"
                          min=""
                          value={values.sessions}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          placeholder={"Nhập Số buổi"}
                        />
                      </div>
                    </div>
                    <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12 ">
                      <div className="form-group">
                        <Select
                          id="manageId"
                          name="manageId"
                          label="Phụ trách: "
                          options={Class?.list_quan_ly}
                          value={values.manageId}
                          onChange={handleChange}
                          onBlur={handleBlur}
                        />
                      </div>
                    </div>
                    <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12 ">
                      <div className="form-group">
                        <Select
                          id="roomId"
                          name="roomId"
                          label="Phòng học: "
                          options={Class?.list_phong}
                          value={values.roomId}
                          onChange={handleChange}
                          onBlur={handleBlur}
                        />
                      </div>
                    </div>
                    <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12 ">
                      <div className="form-group">
                        <Input
                          id="capacity"
                          name="capacity"
                          label="Sức chứa:"
                          type="number"
                          min="0"
                          value={values.capacity}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          placeholder={"Nhập Sức chứa"}
                        />
                      </div>
                    </div>
                    <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12 ">
                      <div className="form-group">
                        <Input
                          id="studentMin"
                          name="studentMin"
                          label="Sỹ số tối thiểu:"
                          type="number"
                          min="0"
                          value={values.studentMin}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          placeholder={"Nhập Sỹ số tối thiểu"}
                        />
                      </div>
                    </div>
                    <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12 ">
                      <div className="form-group">
                        <Input
                          id="studentMax"
                          name="studentMax"
                          label="Sỹ số tối đa:"
                          type="number"
                          min="0"
                          value={values.studentMax}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          placeholder={"Nhập Sỹ số tối đa"}
                        />
                      </div>
                    </div>
                    <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12 ">
                      <div className="form-group">
                        <Select
                          id="priceId"
                          name="priceId"
                          label="Gói học phí:"
                          options={Class?.list_goi_hoc_phi}
                          value={values.priceId}
                          onChange={handleChange}
                          onBlur={handleBlur}
                        />
                      </div>
                    </div>
                    <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12 ">
                      <div className="form-group">
                        <label htmlFor="phu_thu">Phụ thu:</label>
                        <MultiSelect
                          options={list_phu_thu}
                          disabled={!Class?.list_phu_thu.length}
                          value={values.surchargeIds}
                          onChange={data => {
                            setFieldValue("surchargeIds", data);
                          }}
                          onBlur={handleBlur}
                          labelledBy="Select"
                          hasSelectAll={false}
                          overrideStrings={{
                            selectSomeItems: "Chọn phụ thu"
                          }}
                          ArrowRenderer={() => (
                            <i className="fa fa-chevron-down" />
                          )}
                          disableSearch={true}
                        />
                        <ErrorMessage
                          component="p"
                          className="error-message"
                          name="surchargeIds"
                        ></ErrorMessage>
                      </div>
                    </div>
                    <div className="col-12 ">
                      <div className="form-group">
                        <TextArea
                          className="textarea form-control"
                          rows="4"
                          label="Mô tả"
                          name="description"
                          value={values.description}
                          onChange={handleChange}
                          onBlur={handleBlur}
                        ></TextArea>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="iq-card">
                <div className=" iq-card-header d-flex justify-content-between">
                  <div className="iq-header-title">
                    <h6>
                      <b>Thông tin học viên</b>
                    </h6>
                  </div>
                </div>
                <div className="iq-card-body">
                  <div className="row">
                    <div className="col-lg-3 col-sm-6 col-xs-12">
                      <div className="form-group">
                        <Input
                          id="search"
                          name="search"
                          label="Từ danh sách đã có:"
                          value={values.search}
                          onChange={e => {
                            e.persist();
                            setFieldValue("search", e.target.value);
                            this.onSearch(e);
                          }}
                          listautocomplete={this.state.listautocomplete}
                          onBlur={handleBlur}
                          customkey={["firstName", "lastName"]}
                          placeholder={"Nhập tên học viên"}
                          onPicked={data => {
                            const currentstudentList = cloneDeep(
                              this.state.studentList
                            );
                            currentstudentList.push(data);
                            console.log("currentstudentList.push(data)");
                            this.setState({
                              studentList: currentstudentList,
                              listautocomplete: []
                            });
                            setFieldValue("search", "");
                          }}
                        />
                        <ErrorMessage
                          component="p"
                          className="error-message"
                          name="studentList"
                        />
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-12 ">
                      <CustomTable
                        dataSource={this.state.studentList}
                        columns={columns}
                        onChange={data => {}}
                        noPaging
                      ></CustomTable>
                    </div>
                    <div
                      className="col-12 m-t-3"
                      style={{ textAlign: "right" }}
                    >
                      <Button
                        variant="primary"
                        // style={{ marginRight: 15 }}
                        type="submit"
                      >
                        Lưu
                      </Button>
                      {/* <Button variant={"secondary"}>Hủy</Button> */}
                    </div>
                  </div>
                </div>
              </div>
            </form>
          )}
        </Formik>
      </div>
    );
  }
}

export default connect(({ Class, Global, Student }) => ({
  Class,
  Global,
  Student
}))(Page);
