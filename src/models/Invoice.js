import {
  listcenter,
  listtype,
  listclass,
  listschedule,
  listpayment,
  listprice,
  listissuer,
  getpromotion,
  listpromotion,
  listsurchage,
  search,
  view,
  insert,
  update,
  paid,
  _delete
} from "../services/Invoice";

export default {
  namespace: "Invoice",

  state: {
    list: [],
    current: {},
    total_record: 0,
    options: {
      codeOrName: "",
      center_Id: "",
      program_Id: "",
      manage_Id: "",
      pageNumber: 1,
      pageSize: 10,
      sortBy: "id"
    },
    listcenter: [],
    listtype: [],
    listclass: [],
    listschedule: [],
    listpayment: [],
    listprice: [],
    listissuer: [],
    listpromotion: [],
    listsurchage: []
  },

  effects: {
    *listcenter({ payload }, { call, put }) {
      console.log("listcenter effect>>");
      const response = yield call(listcenter, payload);
      yield put({
        type: "listcenter_reducer",
        payload: response.data
      });
      return response.data;
    },
    *listtype({ payload }, { call, put }) {
      console.log("listtype effect>>");
      const response = yield call(listtype, payload);
      yield put({
        type: "listtype_reducer",
        payload: response.data
      });
      return response.data;
    },
    *listclass({ payload }, { call, put }) {
      console.log("listclass effect>>");
      const response = yield call(listclass, payload);
      yield put({
        type: "listclass_reducer",
        payload: response.data
      });
      return response.data;
    },
    *updateoptions({ payload }, { call, put, select }) {
      console.log("updateoptions effect>>");
      const oldOptions = yield select(state => state.Class.options);
      const newOption = { ...oldOptions, ...payload };
      yield put({
        type: "update_options_reducer",
        payload: payload
      });
      try {
        const response = yield call(search, newOption);
        yield put({
          type: "search_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },

    *listschedule({ payload }, { call, put }) {
      console.log("listschedule effect>>", payload);
      const response = yield call(listschedule, payload);
      yield put({
        type: "listschedule_reducer",
        payload: response.data
      });
      return response.data;
    },
    *listpayment({ payload }, { call, put }) {
      console.log("listpayment effect>>", payload);
      const response = yield call(listpayment, payload);
      yield put({
        type: "listpayment_reducer",
        payload: response.data
      });
      return response.data;
    },
    *listsurchage({ payload }, { call, put }) {
      console.log("listsurchage effect>>");
      const response = yield call(listsurchage, payload);
      yield put({
        type: "listsurchage_reducer",
        payload: response.data
      });
      return response.data;
    },
    *listprice({ payload }, { call, put }) {
      console.log("listprice effect>>");
      const response = yield call(listprice, payload);
      yield put({
        type: "listprice_reducer",
        payload: response.data
      });
      return response.data;
    },
    *listissuer({ payload }, { call, put }) {
      console.log("listissuer effect>>");
      const response = yield call(listissuer, payload);
      yield put({
        type: "listissuer_reducer",
        payload: response.data
      });
      return response.data;
    },
    *listpromotion({ payload }, { call, put }) {
      console.log("listpromotion effect>>");
      const response = yield call(listpromotion, payload);
      yield put({
        type: "listpromotion_reducer",
        payload: response.data
      });
      return response.data;
    },
    *search({}, { call, put, select }) {
      const params = yield select(state => state.Class.options);
      console.log("search effect>>", params);
      const response = yield call(search, JSON.stringify(params));
      yield put({
        type: "search_reducer",
        payload: response.data
      });
      return response.data;
    },
    *view({ payload }, { call, put }) {
      console.log("view effect>>");
      const response = yield call(view, payload);
      yield put({
        type: "view_reducer",
        payload: response.data
      });
      return response.data;
    },
    *insert({ payload }, { call, put }) {
      try {
        console.log("insert effect>>");
        const response = yield call(insert, payload);
        yield put({
          type: "insert_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *paid({ payload }, { call, put }) {
      console.log("paid effect>>");
      const response = yield call(paid, payload);
      yield put({
        type: "paid_reducer",
        payload: response.data
      });
      return response.data;
    },
    *update({ payload }, { call, put }) {
      console.log("update effect>>");
      const response = yield call(update, payload);
      yield put({
        type: "update_reducer",
        payload: response.data
      });
      return response.data;
    },
    *delete({ payload }, { call, put }) {
      console.log("delete effect>>");
      const response = yield call(_delete, payload);
      yield put({
        type: "delete_reducer",
        payload: response.data
      });
      return response.data;
    },
    *paid({ payload }, { call, put }) {
      console.log("paid effect>>");
      const response = yield call(paid, payload);
      yield put({
        type: "paid_reducer",
        payload: response.data
      });
      return response.data;
    },
    *getpromotion({ payload }, { call, put }) {
      console.log("getpromotion effect>>");
      try {
        const response = yield call(getpromotion, payload);
        yield put({
          type: "getpromotion_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    }
  },

  reducers: {
    listcenter_reducer(state, action) {
      return {
        ...state,
        listcenter: action.payload?.content
      };
    },
    listtype_reducer(state, action) {
      return {
        ...state,
        listtype: action.payload?.content
      };
    },
    listclass_reducer(state, action) {
      return {
        ...state,
        listclass: action.payload?.content
      };
    },
    update_options_reducer(state, action) {
      return {
        ...state,
        options: { ...state.options, ...action.payload }
      };
    },
    listschedule_reducer(state, action) {
      return {
        ...state,
        listschedule: action.payload?.content
      };
    },
    listpayment_reducer(state, action) {
      return {
        ...state,
        listpayment: action.payload?.content
      };
    },
    listsurchage_reducer(state, action) {
      return {
        ...state,
        listsurchage: action.payload?.content
      };
    },
    listprice_reducer(state, action) {
      return {
        ...state,
        listprice: action.payload?.content
      };
    },
    listpromotion_reducer(state, action) {
      return {
        ...state,
        listpromotion: action.payload?.content
      };
    },
    search_reducer(state, action) {
      return {
        ...state,
        list: action.payload?.content?.records || [],
        total_record: action.payload?.content?.totalRecords || 0
      };
    },
    view_reducer(state, action) {
      return {
        ...state,
        current: action.payload?.content || {}
      };
    },
    insert_reducer(state, action) {
      return {
        ...state
      };
    },
    update_reducer(state, action) {
      return {
        ...state
      };
    },
    delete_reducer(state, action) {
      return {
        ...state
      };
    },
    paid_reducer(state, action) {
      return {
        ...state
      };
    },
    getpromotion_reducer(state, action) {
      return {
        ...state
      };
    }
  }
};
