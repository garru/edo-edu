import {
    listcenter, search, list, codes, view, insert, update, delete_promotionsurchage
} from '../services/PromotionSurchage';

export default {
  namespace: 'PromotionSurchage',

  state: {
    list_center: [],
    list_chuong_trinh_khuyen_mai: [],
    list_chuong_trinh_khuyen_mai: [],
    list_code_khuyen_mai: [],
    current_chuong_trinh_khuyen_mai: {},
  },

  effects: {
    *listcenter({ payload }, { call, put }) {
      console.log('listcenter effect>>');
      const response = yield call(listcenter, payload);
      yield put({
        type: 'listcenter_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *search({ payload }, { call, put }) {
      console.log('search effect>>');
      const response = yield call(search, payload);
      yield put({
        type: 'search_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *list({ payload }, { call, put }) {
      console.log('list effect>>');
      const response = yield call(list, payload);
      yield put({
        type: 'list_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *codes({ payload }, { call, put }) {
      console.log('codes effect>>');
      const response = yield call(codes, payload);
      yield put({
        type: 'codes_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *view({ payload }, { call, put }) {
      console.log('view effect>>');
      const response = yield call(view, payload);
      yield put({
        type: 'view_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *insert({ payload }, { call, put }) {
      console.log('insert effect>>');
      const response = yield call(insert, payload);
      yield put({
        type: 'insert_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *update({ payload }, { call, put }) {
      console.log('update effect>>');
      const response = yield call(update, payload);
      yield put({
        type: 'update_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *delete_promotionsurchage({ payload }, { call, put }) {
      console.log('delete_promotionsurchage effect>>');
      const response = yield call(delete_promotionsurchage, payload);
      yield put({
        type: 'delete_promotionsurchage_reducer',
        payload: response.data,
      });
      return response.data;
    },
  },

  reducers: {
    listcenter_reducer(state, action) {
      return {
        ...state,
        list_center: action.payload?.content,
      };
    },
    search_reducer(state, action) {
      return {
        ...state,
        list_chuong_trinh_khuyen_mai: action.payload?.content,
      };
    },
    list_reducer(state, action) {
      return {
        ...state,
        list_chuong_trinh_khuyen_mai: action.payload?.content,
      };
    },
    codes_reducer(state, action) {
      return {
        ...state,
        list_code_khuyen_mai: action.payload?.content,
      };
    },
    view_reducer(state, action) {
      return {
        ...state,
        current_chuong_trinh_khuyen_mai: action.payload?.content,
      };
    },
    insert_reducer(state, action) {
      return {
        ...state,

      };
    },
    update_reducer(state, action) {
      return {
        ...state,

      };
    },
    delete_promotionsurchage_reducer(state, action) {
      return {
        ...state,

      };
    },
  },
};
