import {
  view,
  listdiscussion,
  insertdiscussion,
  listappointment,
  listinvoice,
  printinvoice,
  liststudentbyid,
  liststudentbystudentid,
  listlesson,
  viewlesson,
  listcourse
} from "../services/CustomerDetail";

export default {
  namespace: "CustomerDetail",

  state: {
    curent_customer_detail: {},
    list_noi_dung_thao_luan: [],
    list_lich_su_cham_soc: [],
    list_hoa_don: [],
    list_student: [],
    list_class: [],
    list_buoi_hoc_theo_hoc_vien: [],
    current_buoi_hoc: {},
    list_lich_su_hoc: [],
    current_hoa_don: {},
    current_lesson: {},
    current_student: {}
  },

  effects: {
    *view({ payload }, { call, put }) {
      try {
        console.log("view effect>>");
        const response = yield call(view, payload);
        yield put({
          type: "view_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *listdiscussion({ payload }, { call, put }) {
      console.log("listdiscussion effect>>");
      const response = yield call(listdiscussion, payload);
      yield put({
        type: "listdiscussion_reducer",
        payload: response.data
      });
      return response.data;
    },
    *insertdiscussion({ payload }, { call, put }) {
      try {
        console.log("insertdiscussion effect>>");
        const response = yield call(insertdiscussion, payload);
        yield put({
          type: "insertdiscussion_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *listappointment({ payload }, { call, put }) {
      console.log("listappointment effect>>");
      const response = yield call(listappointment, payload);
      yield put({
        type: "listappointment_reducer",
        payload: response.data
      });
      return response.data;
    },
    *listinvoice({ payload }, { call, put }) {
      console.log("listinvoice effect>>");
      const response = yield call(listinvoice, payload);
      yield put({
        type: "listinvoice_reducer",
        payload: response.data
      });
      return response.data;
    },
    *printinvoice({ payload }, { call, put }) {
      console.log("printinvoice effect>>");
      const response = yield call(printinvoice, payload);
      yield put({
        type: "printinvoice_reducer",
        payload: response.data
      });
      return response.data;
    },
    *liststudentbyid({ payload }, { call, put }) {
      console.log("liststudentbyid effect>>");
      const response = yield call(liststudentbyid, payload);
      yield put({
        type: "liststudentbyid_reducer",
        payload: response.data
      });
      return response.data;
    },
    *liststudentbystudentid({ payload }, { call, put }) {
      console.log("liststudentbystudentid effect>>");
      const response = yield call(liststudentbystudentid, payload);
      yield put({
        type: "liststudentbystudentid_reducer",
        payload: response.data
      });
      return response.data;
    },
    *listlesson({ payload }, { call, put }) {
      console.log("listlesson effect>>");
      const response = yield call(listlesson, payload);
      yield put({
        type: "listlesson_reducer",
        payload: response.data
      });
      return response.data;
    },
    *viewlesson({ payload }, { call, put }) {
      console.log("viewlesson effect>>");
      const response = yield call(viewlesson, payload);
      yield put({
        type: "viewlesson_reducer",
        payload: response.data
      });
      return response.data;
    },
    *listcourse({ payload }, { call, put }) {
      console.log("listcourse effect>>");
      const response = yield call(listcourse, payload);
      yield put({
        type: "listcourse_reducer",
        payload: response.data
      });
      return response.data;
    },
    *selectlesson({ payload }, { call, put }) {
      yield put({
        type: "selectlesson_reducer",
        payload: payload
      });
    },
    *selectstudent({ payload }, { call, put }) {
      yield put({
        type: "selectstudent_reducer",
        payload: payload
      });
    }
  },

  reducers: {
    view_reducer(state, action) {
      return {
        ...state,
        curent_customer_detail: action.payload?.content
      };
    },
    listdiscussion_reducer(state, action) {
      return {
        ...state,
        list_noi_dung_thao_luan: action.payload?.content
      };
    },
    insertdiscussion_reducer(state, action) {
      return {
        ...state
      };
    },
    listappointment_reducer(state, action) {
      return {
        ...state,
        list_lich_su_cham_soc: action.payload?.content
      };
    },
    listinvoice_reducer(state, action) {
      return {
        ...state,
        list_hoa_don: action.payload?.content
      };
    },
    printinvoice_reducer(state, action) {
      return {
        ...state,
        current_hoa_don: action.payload?.content
      };
    },
    liststudentbyid_reducer(state, action) {
      return {
        ...state,
        list_student: action.payload?.content
      };
    },
    liststudentbystudentid_reducer(state, action) {
      return {
        ...state,
        list_class: action.payload?.content
      };
    },
    listlesson_reducer(state, action) {
      return {
        ...state,
        list_buoi_hoc_theo_hoc_vien: action.payload?.content
      };
    },
    viewlesson_reducer(state, action) {
      return {
        ...state,
        current_buoi_hoc: action.payload?.content
      };
    },
    listcourse_reducer(state, action) {
      return {
        ...state,
        list_lich_su_hoc: action.payload?.content
      };
    },
    selectlesson_reducer(state, action) {
      return {
        ...state,
        current_lesson: action.payload
      };
    },
    selectstudent_reducer(state, action) {
      return {
        ...state,
        current_student: action.payload
      };
    }
  }
};
