import {
    search, listitem, view, insert, update
} from '../services/TeacherFreetime';

export default {
  namespace: 'TeacherFreetime',

  state: {
    list_teacher_freetime: [],
    list_freetime: [],
    current_teacher_freetime: {},
  },

  effects: {
    *search({ payload }, { call, put }) {
      console.log('search effect>>');
      const response = yield call(search, payload);
      yield put({
        type: 'search_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *listitem({ payload }, { call, put }) {
      console.log('listitem effect>>');
      const response = yield call(listitem, payload);
      yield put({
        type: 'listitem_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *view({ payload }, { call, put }) {
      console.log('view effect>>');
      const response = yield call(view, payload);
      yield put({
        type: 'view_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *insert({ payload }, { call, put }) {
      console.log('insert effect>>');
      const response = yield call(insert, payload);
      yield put({
        type: 'insert_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *update({ payload }, { call, put }) {
      console.log('update effect>>');
      const response = yield call(update, payload);
      yield put({
        type: 'update_reducer',
        payload: response.data,
      });
      return response.data;
    },
  },

  reducers: {
    search_reducer(state, action) {
      return {
        ...state,
        list_teacher_freetime: action.payload?.content,
      };
    },
    listitem_reducer(state, action) {
      return {
        ...state,
        list_freetime: action.payload?.content,
      };
    },
    view_reducer(state, action) {
      return {
        ...state,
        current_teacher_freetime: action.payload?.content,
      };
    },
    insert_reducer(state, action) {
      return {
        ...state,

      };
    },
    update_reducer(state, action) {
      return {
        ...state,

      };
    },
  },
};
