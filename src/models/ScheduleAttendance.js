import {
  view,
  liststatus,
  commentview,
  commentdictionary,
  updatestatus,
  commentupdate,
  finish
} from "../services/ScheduleAttendance";

import isNil from "lodash/isNil";
import cloneDeep from "lodash/cloneDeep";

export default {
  namespace: "ScheduleAttendance",

  state: {
    current: {},
    list_status: [],
    student_list: [],
    student_list_filtered: [],
    options: {
      search: "",
      status: ""
    },
    onFilter: false,
    student_name: "",
    criteria_list: []
  },

  effects: {
    *view({ payload }, { call, put }) {
      console.log("view effect>>");
      try {
        const response = yield call(view, payload);
        yield put({
          type: "view_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *liststatus({ payload }, { call, put }) {
      console.log("liststatus effect>>");
      try {
        const response = yield call(liststatus, payload);
        yield put({
          type: "liststatus_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *updatestatus({ payload }, { call, put }) {
      console.log("updatestatus effect>>");
      try {
        const response = yield call(updatestatus, payload);
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *commentview({ payload }, { call, put }) {
      console.log("commentview effect>>");
      try {
        const response = yield call(commentview, payload);
        yield put({
          type: "commentview_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *commentdictionary({ payload }, { call, put }) {
      console.log("commentdictionary effect>>");
      try {
        const response = yield call(commentdictionary, payload);
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *commentupdate({ payload }, { call, put }) {
      console.log("commentupdate effect>>");
      try {
        const response = yield call(commentupdate, payload);
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *updatecommentstudent({ payload }, { call, put }) {
      console.log("updatecommentstudent effect>>");
      yield put({
        type: "updatecommentstudent_reducer",
        payload: payload
      });
    },
    *updaterating({ payload }, { call, put }) {
      console.log("updaterating effect>>");
      yield put({
        type: "updaterating_reducer",
        payload: payload
      });
    },
    *finish({ payload }, { call, put }) {
      console.log("finish effect>>");
      try {
        const response = yield call(finish, payload);
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *filtersearch({ payload }, { call, put, select }) {
      const oldOptions = yield select(
        state => state.ScheduleAttendance?.options
      );
      const newOptions = {
        ...oldOptions,
        ...payload
      };
      const { search, status } = newOptions;

      const empty = search === "" && status === "";

      empty &&
        (yield put({
          type: "reset_reducer"
        }));

      !empty &&
        (yield put({
          type: "filtersearch_reducer",
          payload: newOptions
        }));
    }
  },

  reducers: {
    view_reducer(state, action) {
      return {
        ...state,
        current: action.payload?.content,
        student_list: action.payload?.content?.studentList,
        student_summary: action.payload?.content?.studentSummary
      };
    },
    liststatus_reducer(state, action) {
      return {
        ...state,
        list_status: action.payload?.content
      };
    },
    filtersearch_reducer(state, action) {
      const newOptions = action.payload;
      const { search, status } = newOptions;
      const new_student_list_filtered = state.student_list.filter(
        item =>
          (search && `${item.name} ${item.name}`.includes(search)) ||
          // item.code.includes(search) ||
          (!isNil(status) && item.attendanceStatus === status)
      );
      console.log("new_student_list_filtered", new_student_list_filtered);
      return {
        ...state,
        options: newOptions,
        onFilter: true,
        student_list_filtered: new_student_list_filtered
      };
    },
    commentview_reducer(state, action) {
      return {
        ...state,
        student_name: action.payload?.content?.studentName,
        criteria_list: action.payload?.content?.criteriaList
      };
    },
    updatecommentstudent_reducer(state, action) {
      const { comment, index } = action.payload;
      let criteria_list = cloneDeep(state.criteria_list);
      criteria_list[index].comment = comment;
      console.log("comment", criteria_list);
      return {
        ...state,
        criteria_list: criteria_list
      };
    },
    updaterating_reducer(state, action) {
      const { rating, index } = action.payload;
      let criteria_list = cloneDeep(state.criteria_list);
      criteria_list[index].point = rating;
      console.log("rating", criteria_list, rating, index);
      return {
        ...state,
        criteria_list: criteria_list
      };
    },

    reset_reducer(state, action) {
      return {
        ...state,
        options: {
          search: "",
          status: ""
        },
        onFilter: false,
        student_list_filtered: []
      };
    }
  }
};
