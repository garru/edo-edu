import {
    listcenter, listprogram, listtype, search, list, view, insert, update, delete_courseprice
} from '../services/CoursePrice';

export default {
  namespace: 'CoursePrice',

  state: {
    list_center: [],
    list_chuong_trinh: [],
    list_hinh_thuc_thu: [],
    list_course_price: [],
    total_record: 0,
    current_cource_price: {},
  },

  effects: {
    *listcenter({ payload }, { call, put }) {
      console.log('listcenter effect>>');
      const response = yield call(listcenter, payload);
      yield put({
        type: 'listcenter_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *listprogram({ payload }, { call, put }) {
      console.log('listprogram effect>>');
      const response = yield call(listprogram, payload);
      yield put({
        type: 'listprogram_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *listtype({ payload }, { call, put }) {
      console.log('listtype effect>>');
      const response = yield call(listtype, payload);
      yield put({
        type: 'listtype_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *search({ payload }, { call, put }) {
      console.log('search effect>>');
      const response = yield call(search, payload);
      yield put({
        type: 'search_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *list({ payload }, { call, put }) {
      console.log('list effect>>');
      const response = yield call(list, payload);
      yield put({
        type: 'list_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *view({ payload }, { call, put }) {
      console.log('view effect>>');
      const response = yield call(view, payload);
      yield put({
        type: 'view_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *insert({ payload }, { call, put }) {
      console.log('insert effect>>');
      const response = yield call(insert, payload);
      yield put({
        type: 'insert_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *update({ payload }, { call, put }) {
      console.log('update effect>>');
      const response = yield call(update, payload);
      yield put({
        type: 'update_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *delete_courseprice({ payload }, { call, put }) {
      console.log('delete_courseprice effect>>');
      const response = yield call(delete_courseprice, payload);
      yield put({
        type: 'delete_courseprice_reducer',
        payload: response.data,
      });
      return response.data;
    },
  },

  reducers: {
    listcenter_reducer(state, action) {
      return {
        ...state,
        list_center: action.payload?.content,
      };
    },
    listprogram_reducer(state, action) {
      return {
        ...state,
        list_chuong_trinh: action.payload?.content,
      };
    },
    listtype_reducer(state, action) {
      return {
        ...state,
        list_hinh_thuc_thu: action.payload?.content,
      };
    },
    search_reducer(state, action) {
      return {
        ...state,
        list_course_price: action.payload?.content?.content?.records,
        total_record: action.payload?.content?.content?.totalRecords
      };
    },
    list_reducer(state, action) {
      return {
        ...state,
        list_course_price: action.payload?.content,
        total_record: action.payload?.content?.length
      };
    },
    view_reducer(state, action) {
      return {
        ...state,
        current_cource_price: action.payload?.content,
      };
    },
    insert_reducer(state, action) {
      return {
        ...state,

      };
    },
    update_reducer(state, action) {
      return {
        ...state,

      };
    },
    delete_courseprice_reducer(state, action) {
      return {
        ...state,

      };
    },
  },
};
