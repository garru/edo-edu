import {
    listprogram, listroom, listfinish, listrepeat, listtime, listdomestic, listforeign, view, arrange, lessoninsert, lessonupdate, initial
} from '../services/ScheduleInitial';

export default {
  namespace: 'ScheduleInitial',

  state: {
    list_chuong_trinh: [],
    list_phong_hoc: [],
    list_dk_ket_thuc: [],
    list_dk_lap: [],
    list_gio_hoc: [],
    list_giao_vien_vn: [],
    list_giao_vien_nn: [],
    list_thon_tin_lop_hoc: [],
  },

  effects: {
    *listprogram({ payload }, { call, put }) {
      console.log('listprogram effect>>');
      const response = yield call(listprogram, payload);
      yield put({
        type: 'listprogram_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *listroom({ payload }, { call, put }) {
      console.log('listroom effect>>');
      const response = yield call(listroom, payload);
      yield put({
        type: 'listroom_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *listfinish({ payload }, { call, put }) {
      console.log('listfinish effect>>');
      const response = yield call(listfinish, payload);
      yield put({
        type: 'listfinish_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *listrepeat({ payload }, { call, put }) {
      console.log('listrepeat effect>>');
      const response = yield call(listrepeat, payload);
      yield put({
        type: 'listrepeat_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *listtime({ payload }, { call, put }) {
      console.log('listtime effect>>');
      const response = yield call(listtime, payload);
      yield put({
        type: 'listtime_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *listdomestic({ payload }, { call, put }) {
      console.log('listdomestic effect>>');
      const response = yield call(listdomestic, payload);
      yield put({
        type: 'listdomestic_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *listforeign({ payload }, { call, put }) {
      console.log('listforeign effect>>');
      const response = yield call(listforeign, payload);
      yield put({
        type: 'listforeign_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *view({ payload }, { call, put }) {
      console.log('view effect>>');
      const response = yield call(view, payload);
      yield put({
        type: 'view_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *arrange({ payload }, { call, put }) {
      console.log('arrange effect>>');
      const response = yield call(arrange, payload);
      yield put({
        type: 'arrange_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *lessoninsert({ payload }, { call, put }) {
      console.log('lessoninsert effect>>');
      const response = yield call(lessoninsert, payload);
      yield put({
        type: 'lessoninsert_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *lessonupdate({ payload }, { call, put }) {
      console.log('lessonupdate effect>>');
      const response = yield call(lessonupdate, payload);
      yield put({
        type: 'lessonupdate_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *initial({ payload }, { call, put }) {
      console.log('initial effect>>');
      const response = yield call(initial, payload);
      yield put({
        type: 'initial_reducer',
        payload: response.data,
      });
      return response.data;
    },
  },

  reducers: {
    listprogram_reducer(state, action) {
      return {
        ...state,
        list_chuong_trinh: action.payload?.content,
      };
    },
    listroom_reducer(state, action) {
      return {
        ...state,
        list_phong_hoc: action.payload?.content,
      };
    },
    listfinish_reducer(state, action) {
      return {
        ...state,
        list_dk_ket_thuc: action.payload?.content,
      };
    },
    listrepeat_reducer(state, action) {
      return {
        ...state,
        list_dk_lap: action.payload?.content,
      };
    },
    listtime_reducer(state, action) {
      return {
        ...state,
        list_gio_hoc: action.payload?.content,
      };
    },
    listdomestic_reducer(state, action) {
      return {
        ...state,
        list_giao_vien_vn: action.payload?.content,
      };
    },
    listforeign_reducer(state, action) {
      return {
        ...state,
        list_giao_vien_nn: action.payload?.content,
      };
    },
    view_reducer(state, action) {
      return {
        ...state,
        list_thon_tin_lop_hoc: action.payload?.content,
      };
    },
    arrange_reducer(state, action) {
      return {
        ...state,

      };
    },
    lessoninsert_reducer(state, action) {
      return {
        ...state,

      };
    },
    lessonupdate_reducer(state, action) {
      return {
        ...state,

      };
    },
    initial_reducer(state, action) {
      return {
        ...state,

      };
    },
  },
};
