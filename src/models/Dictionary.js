import {
  listdic,
  getdic,
} from "../services/Dictionary";

export default {
  namespace: "Dictionary",

  state: {
    list_danh_muc_he_thong: [],
    list_danh_muc_he_thong_chi_tiet: [],
  },

  effects: {
    *listdic({ payload }, { call, put }) {
      console.log("listdic effect>>");
      const response = yield call(listdic, payload);
      yield put({
        type: "listdic_reducer",
        payload: response.data
      });
      return response.data;
    },
    *getdic({ payload }, { call, put }) {
      console.log("getdic effect>>");
      const response = yield call(getdic, payload);
      yield put({
        type: "getdic_reducer",
        payload: response.data
      });
      return response.data;
    },
  },

  reducers: {
    listdic_reducer(state, action) {
      return {
        ...state,
        list_danh_muc_he_thong: action.payload?.content
      };
    },
    getdic_reducer(state, action) {
      return {
        ...state,
        list_danh_muc_he_thong_chi_tiet: action.payload?.content,
      };
    }
  }
};
