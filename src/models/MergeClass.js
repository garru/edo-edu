import { listcenter, listclass, merge } from "../services/MergeClass";

export default {
  namespace: "MergeClass",

  state: {
    list_trung_tam: [],
    list_lop_hoc_chinh: [],
    list_lop_hoc_phu: [],
    list_lop_hoc_ghep: []
  },

  effects: {
    *listtrungtam({ payload }, { call, put }) {
      try {
        const response = yield call(listcenter, payload);
        yield put({
          type: "listtrungtam_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *listlophocchinh({ payload }, { call, put }) {
      try {
        const response = yield call(listclass, payload);
        yield put({
          type: "listlophocchinh_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *listlophocphu({ payload }, { call, put }) {
      console.log("listlophocphu effect>>");
      try {
        const response = yield call(listclass, payload);
        yield put({
          type: "listlophocphu_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *listlophocghep({ payload }, { call, put }) {
      try {
        const response = yield call(listclass, payload);
        yield put({
          type: "listlophocghep_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *merge({ payload }, { call, put }) {
      console.log("update effect>>");
      try {
        const response = yield call(merge, payload);
        yield put({
          type: "reset_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *reset({}, { put }) {
      yield put({
        type: "reset_reducer"
      });
    }
  },

  reducers: {
    listtrungtam_reducer(state, action) {
      return {
        ...state,
        list_trung_tam: action.payload?.content
      };
    },
    listlophocchinh_reducer(state, action) {
      return {
        ...state,
        list_lop_hoc_chinh: action.payload?.content
      };
    },
    listlophocphu_reducer(state, action) {
      return {
        ...state,
        list_lop_hoc_phu: action.payload?.content
      };
    },
    listlophocghep_reducer(state, action) {
      return {
        ...state,
        list_lop_hoc_ghep: action.payload?.content
      };
    },
    reset_reducer(state, action) {
      console.log("reset", {
        ...state,
        list_lop_hoc_chinh: [],
        list_lop_hoc_phu: [],
        list_lop_hoc_ghep: [],
        list_trung_tam: []
      });
      return {
        ...state,
        list_lop_hoc_chinh: [],
        list_lop_hoc_phu: [],
        list_lop_hoc_ghep: []
      };
    }
  }
};
