import {
    evaluationobject, list, view, insert, update, delete_evaluation
} from '../services/Evaluation';

export default {
  namespace: 'Evaluation',

  state: {
    list_doi_tuong_danh_gia: [],
    list_evaluation: [],
    current_evaluation: {},
    total_records: 0
  },

  effects: {
    *evaluationobject({ payload }, { call, put }) {
      console.log('evaluationobject effect>>');
      const response = yield call(evaluationobject, payload);
      yield put({
        type: 'evaluationobject_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *list({ payload }, { call, put }) {
      console.log('list effect>>');
      const response = yield call(list, payload);
      yield put({
        type: 'list_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *view({ payload }, { call, put }) {
      console.log('view effect>>');
      const response = yield call(view, payload);
      yield put({
        type: 'view_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *insert({ payload }, { call, put }) {
      console.log('insert effect>>');
      const response = yield call(insert, payload);
      yield put({
        type: 'insert_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *update({ payload }, { call, put }) {
      console.log('update effect>>');
      const response = yield call(update, payload);
      yield put({
        type: 'update_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *delete_evaluation({ payload }, { call, put }) {
      console.log('delete_evaluation effect>>');
      const response = yield call(delete_evaluation, payload);
      yield put({
        type: 'delete_evaluation_reducer',
        payload: response.data,
      });
      return response.data;
    },
  },

  reducers: {
    evaluationobject_reducer(state, action) {
      return {
        ...state,
        list_doi_tuong_danh_gia: action.payload?.content,
      };
    },
    list_reducer(state, action) {
      return {
        ...state,
        list_evaluation: action.payload?.content,
        total_records: action.payload?.content?.length
      };
    },
    view_reducer(state, action) {
      return {
        ...state,
        current_evaluation: action.payload?.content,
      };
    },
    insert_reducer(state, action) {
      return {
        ...state,

      };
    },
    update_reducer(state, action) {
      return {
        ...state,

      };
    },
    delete_evaluation_reducer(state, action) {
      return {
        ...state,

      };
    },
  },
};
