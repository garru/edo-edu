import {
    view, evaluation, observationhistory, observationview, parentcommentlist, parentcommentview
} from '../services/TeacherDetail';

export default {
  namespace: 'TeacherDetail',

  state: {
    current_giao_vien: {},
    danh_gia_tong_quan: {},
    danh_gia_giao_vien: {},
    list_nhan_xet_phu_huynh: [],
    current_nhan_xet_phu_huynh: {},
  },

  effects: {
    *view({ payload }, { call, put }) {
      console.log('view effect>>');
      const response = yield call(view, payload);
      yield put({
        type: 'view_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *evaluation({ payload }, { call, put }) {
      console.log('evaluation effect>>');
      const response = yield call(evaluation, payload);
      yield put({
        type: 'evaluation_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *observationhistory({ payload }, { call, put }) {
      console.log('observationhistory effect>>');
      const response = yield call(observationhistory, payload);
      yield put({
        type: 'observationhistory_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *observationview({ payload }, { call, put }) {
      console.log('observationview effect>>');
      const response = yield call(observationview, payload);
      yield put({
        type: 'observationview_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *parentcommentlist({ payload }, { call, put }) {
      console.log('parentcommentlist effect>>');
      const response = yield call(parentcommentlist, payload);
      yield put({
        type: 'parentcommentlist_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *parentcommentview({ payload }, { call, put }) {
      console.log('parentcommentview effect>>');
      const response = yield call(parentcommentview, payload);
      yield put({
        type: 'parentcommentview_reducer',
        payload: response.data,
      });
      return response.data;
    },
  },

  reducers: {
    view_reducer(state, action) {
      return {
        ...state,
        current_giao_vien: action.payload?.content,
      };
    },
    evaluation_reducer(state, action) {
      return {
        ...state,
        danh_gia_tong_quan: action.payload?.content,
      };
    },
    observationhistory_reducer(state, action) {
      return {
        ...state,

      };
    },
    observationview_reducer(state, action) {
      return {
        ...state,
        danh_gia_giao_vien: action.payload?.content,
      };
    },
    parentcommentlist_reducer(state, action) {
      return {
        ...state,
        list_nhan_xet_phu_huynh: action.payload?.content,
      };
    },
    parentcommentview_reducer(state, action) {
      return {
        ...state,
        current_nhan_xet_phu_huynh: action.payload?.content,
      };
    },
  },
};
