import {
    listcenter, list, view, insert, update, delete_teachingtime
} from '../services/TeachingTime';

export default {
  namespace: 'TeachingTime',

  state: {
    list_center: [],
    list_teaching_time: [],
    total_record: 0,
    current_teaching_time: {},
  },

  effects: {
    *listcenter({ payload }, { call, put }) {
      console.log('listcenter effect>>');
      const response = yield call(listcenter, payload);
      yield put({
        type: 'listcenter_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *list({ payload }, { call, put }) {
      console.log('list effect>>');
      const response = yield call(list, payload);
      yield put({
        type: 'list_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *view({ payload }, { call, put }) {
      console.log('view effect>>');
      const response = yield call(view, payload);
      yield put({
        type: 'view_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *insert({ payload }, { call, put }) {
      console.log('insert effect>>');
      const response = yield call(insert, payload);
      yield put({
        type: 'insert_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *update({ payload }, { call, put }) {
      console.log('update effect>>');
      const response = yield call(update, payload);
      yield put({
        type: 'update_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *delete_teachingtime({ payload }, { call, put }) {
      console.log('delete_teachingtime effect>>');
      const response = yield call(delete_teachingtime, payload);
      yield put({
        type: 'delete_teachingtime_reducer',
        payload: response.data,
      });
      return response.data;
    },
  },

  reducers: {
    listcenter_reducer(state, action) {
      return {
        ...state,
        list_center: action.payload?.content,
      };
    },
    list_reducer(state, action) {
      return {
        ...state,
        list_teaching_time: action.payload?.content,
        total_record: action.payload?.content?.length
      };
    },
    view_reducer(state, action) {
      return {
        ...state,
        current_teaching_time: action.payload?.content,
      };
    },
    insert_reducer(state, action) {
      return {
        ...state,

      };
    },
    update_reducer(state, action) {
      return {
        ...state,

      };
    },
    delete_teachingtime_reducer(state, action) {
      return {
        ...state,

      };
    },
  },
};
