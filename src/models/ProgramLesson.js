import {
    listevaluation, list, view, upload, download, insert, update, delete_programlesson
} from '../services/ProgramLesson';

export default {
  namespace: 'ProgramLesson',

  state: {
    list_tieu_chi_danh_gia: [],
    list_program_lession: [],
    total_record: 0,
    current_program_lession: {},
  },

  effects: {
    *listevaluation({ payload }, { call, put }) {
      console.log('listevaluation effect>>');
      const response = yield call(listevaluation, payload);
      yield put({
        type: 'listevaluation_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *list({ payload }, { call, put }) {
      console.log('list effect>>');
      const response = yield call(list, payload);
      yield put({
        type: 'list_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *view({ payload }, { call, put }) {
      console.log('view effect>>');
      const response = yield call(view, payload);
      yield put({
        type: 'view_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *upload({ payload }, { call, put }) {
      console.log('upload effect>>');
      const response = yield call(upload, payload);
      yield put({
        type: 'upload_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *download({ payload }, { call, put }) {
      console.log('download effect>>');
      const response = yield call(download, payload);
      yield put({
        type: 'download_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *insert({ payload }, { call, put }) {
      console.log('insert effect>>');
      const response = yield call(insert, payload);
      yield put({
        type: 'insert_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *update({ payload }, { call, put }) {
      console.log('update effect>>');
      const response = yield call(update, payload);
      yield put({
        type: 'update_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *delete_programlesson({ payload }, { call, put }) {
      console.log('delete_programlesson effect>>');
      const response = yield call(delete_programlesson, payload);
      yield put({
        type: 'delete_programlesson_reducer',
        payload: response.data,
      });
      return response.data;
    },
  },

  reducers: {
    listevaluation_reducer(state, action) {
      return {
        ...state,
        list_tieu_chi_danh_gia: action.payload?.content,
      };
    },
    list_reducer(state, action) {
      return {
        ...state,
        list_program_lession: action.payload?.content,
      };
    },
    view_reducer(state, action) {
      return {
        ...state,
        current_program_lession: action.payload?.content,
      };
    },
    upload_reducer(state, action) {
      return {
        ...state,

      };
    },
    download_reducer(state, action) {
      return {
        ...state,

      };
    },
    insert_reducer(state, action) {
      return {
        ...state,

      };
    },
    update_reducer(state, action) {
      return {
        ...state,

      };
    },
    delete_programlesson_reducer(state, action) {
      return {
        ...state,

      };
    },
  },
};
