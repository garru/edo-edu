import { showLoading, hideLoading } from "../services/Global";

const delay = time => new Promise(resolve => setTimeout(resolve, time));

export default {
  namespace: "Global",

  state: {
    current: {},
    loading: false,
    error: false,
    success: false
  },

  effects: {
    *showLoading({}, { put }) {
      console.log("showLoading>>");
      yield put({
        type: "show_loading_reducer",
        payload: true
      });
      return true;
    },
    *showError({}, { put, call }) {
      console.log("showError>>");
      yield put({
        type: "show_error_reducer",
        payload: true
      });
      yield call(delay, 4000);
      yield put({
        type: "show_error_reducer",
        payload: false
      });
      return true;
    },
    *showSuccess({}, { put, call }) {
      console.log("showSuccess>>");
      yield put({
        type: "show_success_reducer",
        payload: true
      });
      yield call(delay, 4000);
      yield put({
        type: "show_success_reducer",
        payload: false,
        delay: 4000
      });
      return true;
    },
    *hideLoading({}, { put }) {
      yield put({
        type: "hide_loading_reducer",
        payload: false
      });
      return false;
    }
  },

  reducers: {
    show_loading_reducer(state, action) {
      console.log("show_loading_reducer", action);
      return {
        ...state,
        loading: action.payload
      };
    },
    hide_loading_reducer(state, action) {
      console.log("hide_loading_reducer", action);
      return {
        ...state,
        loading: action.payload
      };
    },
    show_error_reducer(state, action) {
      console.log("show_error_reducer", action, new Date());
      return {
        ...state,
        error: action.payload
      };
    },
    show_success_reducer(state, action) {
      console.log("show_success_reducer", action);
      return {
        ...state,
        success: action.payload
      };
    }
  }
};
