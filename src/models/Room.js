import {
    listcenter, listtype, liststatus, search, view, insert, update, delete_room, checklesson, listroom, teacherdomestic, teacherforeign
} from '../services/Room';

export default {
  namespace: 'Room',

  state: {
    list_center: [],
    list_loai_phong: [],
    list_trang_thai: [],
    list_room: [],
    total_record: 0,
    current_room: {},
    list_danh_sach_tiet_hoc: [],
    list_phong_hoc: [],
    list_giao_vien_vn: [],
    list_giao_vien_nn: [],
  },

  effects: {
    *listcenter({ payload }, { call, put }) {
      console.log('listcenter effect>>');
      const response = yield call(listcenter, payload);
      yield put({
        type: 'listcenter_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *listtype({ payload }, { call, put }) {
      console.log('listtype effect>>');
      const response = yield call(listtype, payload);
      yield put({
        type: 'listtype_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *liststatus({ payload }, { call, put }) {
      console.log('liststatus effect>>');
      const response = yield call(liststatus, payload);
      yield put({
        type: 'liststatus_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *search({ payload }, { call, put }) {
      console.log('search effect>>');
      const response = yield call(search, payload);
      yield put({
        type: 'search_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *view({ payload }, { call, put }) {
      console.log('view effect>>');
      const response = yield call(view, payload);
      yield put({
        type: 'view_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *insert({ payload }, { call, put }) {
      console.log('insert effect>>');
      const response = yield call(insert, payload);
      yield put({
        type: 'insert_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *update({ payload }, { call, put }) {
      console.log('update effect>>');
      const response = yield call(update, payload);
      yield put({
        type: 'update_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *delete_room({ payload }, { call, put }) {
      console.log('delete_room effect>>');
      const response = yield call(delete_room, payload);
      yield put({
        type: 'delete_room_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *checklesson({ payload }, { call, put }) {
      console.log('checklesson effect>>');
      const response = yield call(checklesson, payload);
      yield put({
        type: 'checklesson_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *listroom({ payload }, { call, put }) {
      console.log('listroom effect>>');
      const response = yield call(listroom, payload);
      yield put({
        type: 'listroom_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *teacherdomestic({ payload }, { call, put }) {
      console.log('teacherdomestic effect>>');
      const response = yield call(teacherdomestic, payload);
      yield put({
        type: 'teacherdomestic_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *teacherforeign({ payload }, { call, put }) {
      console.log('teacherforeign effect>>');
      const response = yield call(teacherforeign, payload);
      yield put({
        type: 'teacherforeign_reducer',
        payload: response.data,
      });
      return response.data;
    },
  },

  reducers: {
    listcenter_reducer(state, action) {
      return {
        ...state,
        list_center: action.payload?.content,
      };
    },
    listtype_reducer(state, action) {
      return {
        ...state,
        list_loai_phong: action.payload?.content,
      };
    },
    liststatus_reducer(state, action) {
      return {
        ...state,
        list_trang_thai: action.payload?.content,
      };
    },
    search_reducer(state, action) {
      return {
        ...state,
        list_room: action.payload?.content?.content?.records,
        total_record: action.payload?.content?.content?.records?.length
      };
    },
    view_reducer(state, action) {
      return {
        ...state,
        current_room: action.payload?.content,
      };
    },
    insert_reducer(state, action) {
      return {
        ...state,

      };
    },
    update_reducer(state, action) {
      return {
        ...state,

      };
    },
    delete_room_reducer(state, action) {
      return {
        ...state,

      };
    },
    checklesson_reducer(state, action) {
      return {
        ...state,
        list_danh_sach_tiet_hoc: action.payload?.content,
      };
    },
    listroom_reducer(state, action) {
      return {
        ...state,
        list_phong_hoc: action.payload?.content,
      };
    },
    teacherdomestic_reducer(state, action) {
      return {
        ...state,
        list_giao_vien_vn: action.payload?.content,
      };
    },
    teacherforeign_reducer(state, action) {
      return {
        ...state,
        list_giao_vien_nn: action.payload?.content,
      };
    },
  },
};
