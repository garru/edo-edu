import {
    view, listprogram, listclass, listschedule, insert
} from '../services/StudentAbsence';

export default {
  namespace: 'StudentAbsence',

  state: {
    current_thong_tin_buoi_hoc_nghi: {},
    list_chuong_trinh: [],
    list_lop_hoc: [],
    list_buoi_hoc_xep_bu: [],
  },

  effects: {
    *view({ payload }, { call, put }) {
      console.log('view effect>>');
      const response = yield call(view, payload);
      yield put({
        type: 'view_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *listprogram({ payload }, { call, put }) {
      console.log('listprogram effect>>');
      const response = yield call(listprogram, payload);
      yield put({
        type: 'listprogram_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *listclass({ payload }, { call, put }) {
      console.log('listclass effect>>');
      const response = yield call(listclass, payload);
      yield put({
        type: 'listclass_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *listschedule({ payload }, { call, put }) {
      console.log('listschedule effect>>');
      const response = yield call(listschedule, payload);
      yield put({
        type: 'listschedule_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *insert({ payload }, { call, put }) {
      console.log('insert effect>>');
      const response = yield call(insert, payload);
      yield put({
        type: 'insert_reducer',
        payload: response.data,
      });
      return response.data;
    },
  },

  reducers: {
    view_reducer(state, action) {
      return {
        ...state,
        current_thong_tin_buoi_hoc_nghi: action.payload?.content,
      };
    },
    listprogram_reducer(state, action) {
      return {
        ...state,
        list_chuong_trinh: action.payload?.content,
      };
    },
    listclass_reducer(state, action) {
      return {
        ...state,
        list_lop_hoc: action.payload?.content,
      };
    },
    listschedule_reducer(state, action) {
      return {
        ...state,
        list_buoi_hoc_xep_bu: action.payload?.content,
      };
    },
    insert_reducer(state, action) {
      return {
        ...state,

      };
    },
  },
};
