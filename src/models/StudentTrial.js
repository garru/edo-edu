import {
  listcenter,
  listprogram,
  listclass,
  listschedule,
  view,
  insert,
  append
} from "../services/StudentTrial";

export default {
  namespace: "StudentTrial",

  state: {
    list_center: [],
    list_chuong_trinh: [],
    list_lop: [],
    list_schedule: [],
    current_student_trial: {}
  },

  effects: {
    *listcenter({ payload }, { call, put }) {
      try {
        console.log("listcenter effect>>");
        const response = yield call(listcenter, payload);
        yield put({
          type: "listcenter_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *listprogram({ payload }, { call, put }) {
      try {
        console.log("listprogram effect>>");
        const response = yield call(listprogram, payload);
        yield put({
          type: "listprogram_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *listclass({ payload }, { call, put }) {
      try {
        console.log("listclass effect>>");
        const response = yield call(listclass, payload);
        yield put({
          type: "listclass_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *listschedule({ payload }, { call, put }) {
      try {
        console.log("listschedule effect>>");
        const response = yield call(listschedule, payload);
        yield put({
          type: "listschedule_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *view({ payload }, { call, put }) {
      try {
        console.log("view effect>>");
        const response = yield call(view, payload);
        yield put({
          type: "view_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *insert({ payload }, { call, put }) {
      try {
        console.log("insert effect>>");
        const response = yield call(insert, payload);
        yield put({
          type: "insert_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *reset({ payload }, { call, put }) {
      yield put({
        type: "reset_reducer",
        payload: {}
      });
    },
    *append({ payload }, { call, put }) {
      console.log('append effect>>');
      const response = yield call(append, payload);
      yield put({
        type: 'append_reducer',
        payload: response.data,
      });
      return response.data;
    },
  },

  reducers: {
    listcenter_reducer(state, action) {
      return {
        ...state,
        list_center: action.payload?.content
      };
    },
    listprogram_reducer(state, action) {
      return {
        ...state,
        list_chuong_trinh: action.payload?.content
      };
    },
    listclass_reducer(state, action) {
      return {
        ...state,
        list_lop: action.payload?.content
      };
    },
    listschedule_reducer(state, action) {
      return {
        ...state,
        list_schedule: action.payload?.content
      };
    },
    view_reducer(state, action) {
      return {
        ...state,
        current_student_trial: action.payload?.content
      };
    },
    insert_reducer(state, action) {
      return {
        ...state
      };
    },
    reset_reducer(state, action) {
      return {
        ...state,
        list_schedule: []
      };
    },
    append_reducer(state, action) {
      return {
        ...state,

      };
    },
  }
};
