import { listperform, insert, view, update } from "../services/Appointment";

export default {
  namespace: "Appointment",

  state: {
    list_nguoi_thuc_hien: [],
    current_lich_hen: {}
  },

  effects: {
    *listperform({ payload }, { call, put }) {
      console.log("listperform effect>>");
      const response = yield call(listperform, payload);
      yield put({
        type: "listperform_reducer",
        payload: response.data
      });
      return response.data;
    },
    *insert({ payload }, { call, put }) {
      try {
        console.log("insert effect>>");
        const response = yield call(insert, payload);
        yield put({
          type: "insert_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *view({ payload }, { call, put }) {
      try {
        console.log("view effect>>");
        const response = yield call(view, payload);
        yield put({
          type: "view_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *update({ payload }, { call, put }) {
      console.log("update effect>>");
      const response = yield call(update, payload);
      yield put({
        type: "update_reducer",
        payload: response.data
      });
      return response.data;
    }
  },

  reducers: {
    listperform_reducer(state, action) {
      return {
        ...state,
        list_nguoi_thuc_hien: action.payload?.content
      };
    },
    insert_reducer(state, action) {
      return {
        ...state
      };
    },
    view_reducer(state, action) {
      return {
        ...state,
        current_lich_hen: action.payload?.content
      };
    },
    update_reducer(state, action) {
      return {
        ...state
      };
    }
  }
};
