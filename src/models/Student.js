import {
  search,
  view,
  listcenter,
  listprogram,
  listclass,
  changeclass
} from "../services/Student";
import { searchparent } from "../services/Customer";

export default {
  namespace: "Student",

  state: {
    list_hoc_sinh: [],
    current_hoc_sinh: {},
    list_phu_huynh: [],
    options: {
      codeOrName: "",
      center_Id: "",
      class_Id: "",
      pageNumber: 1,
      pageSize: 10,
      sortBy: "id"
    },
    listcenter: [],
    listclass: [],
    listprogram: [],
    summary: [],
    total_record: 0
  },

  effects: {
    *search({ payload }, { call, put }) {
      console.log("search effect>>");
      try {
        const response = yield call(search, payload);
        yield put({
          type: "search_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *searchparent({ payload }, { call, put }) {
      try {
        console.log("searchparent effect>>");
        const response = yield call(searchparent, payload);
        yield put({
          type: "searchparent_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *view({ payload }, { call, put }) {
      try {
        console.log("view effect>>");
        const response = yield call(view, payload);
        yield put({
          type: "view_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },

    *updateoptions({ payload }, { call, put, select }) {
      console.log("updateoptions effect>>");
      const oldOptions = yield select(state => state.Student.options);
      const newOption = { ...oldOptions, ...payload };
      yield put({
        type: "update_options_reducer",
        payload: payload
      });
      try {
        const response = yield call(search, newOption);
        yield put({
          type: "search_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *listcenter({ payload }, { call, put }) {
      try {
        console.log("listcenter effect>>");
        const response = yield call(listcenter, payload);
        yield put({
          type: "listcenter_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *listprogram({ payload }, { call, put }) {
      try {
        console.log("listprogram effect>>");
        const response = yield call(listprogram, payload);
        yield put({
          type: "listprogram_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *listclass({ payload }, { call, put }) {
      try {
        console.log("listclass effect>>");
        const response = yield call(listclass, payload);
        yield put({
          type: "listclass_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *listcenterModal({ payload }, { call, put }) {
      try {
        console.log("listcenter effect>>");
        const response = yield call(listcenter, payload);
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *listprogramModal({ payload }, { call, put }) {
      try {
        console.log("listprogram effect>>");
        const response = yield call(listprogram, payload);
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *listclassModal({ payload }, { call, put }) {
      try {
        console.log("listclass effect>>");
        const response = yield call(listclass, payload);
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *viewModal({ payload }, { call, put }) {
      try {
        console.log("view effect>>");
        const response = yield call(view, payload);
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *changeclass({ payload }, { call, put }) {
      try {
        console.log("changeclass effect>>");
        const response = yield call(changeclass, payload);
        return response.data;
      } catch (error) {
        return false;
      }
    }
  },

  reducers: {
    listcenter_reducer(state, action) {
      return {
        ...state,
        listcenter: action.payload?.content
      };
    },
    listclass_reducer(state, action) {
      return {
        ...state,
        listclass: action.payload?.content
      };
    },
    search_reducer(state, action) {
      return {
        ...state,
        list_hoc_sinh: action.payload?.content?.records || [],
        total_record: action.payload?.content?.totalRecords || 0,
        summary: action.payload?.content?.summary
      };
    },
    searchparent_reducer(state, action) {
      return {
        ...state,
        list_phu_huynh: action.payload?.content?.records
      };
    },
    view_reducer(state, action) {
      return {
        ...state,
        current_hoc_sinh: action.payload?.content
      };
    },
    listprogram_reducer(state, action) {
      return {
        ...state,
        listprogram: action.payload?.content
      };
    },
    update_options_reducer(state, action) {
      return {
        ...state,
        options: { ...state.options, ...action.payload }
      };
    }
  }
};
