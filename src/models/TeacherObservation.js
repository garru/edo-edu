import {
    list, listcenter, listclass, listtype, listcriteria, viewedit, insert, update, delete_teacherobservation, processview, processsummary, processupdate
} from '../services/TeacherObservation';

export default {
  namespace: 'TeacherObservation',

  state: {
    list_nhan_xet_du_gio: [],
    list_center: [],
    list_lop_hoc: [],
    list_loai_danh_gia: [],
    list_cau_hoi_danh_gia: [],
    current_thong_tin: {},
    current_du_gio: {},
  },

  effects: {
    *list({ payload }, { call, put }) {
      console.log('list effect>>');
      const response = yield call(list, payload);
      yield put({
        type: 'list_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *listcenter({ payload }, { call, put }) {
      console.log('listcenter effect>>');
      const response = yield call(listcenter, payload);
      yield put({
        type: 'listcenter_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *listclass({ payload }, { call, put }) {
      console.log('listclass effect>>');
      const response = yield call(listclass, payload);
      yield put({
        type: 'listclass_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *listtype({ payload }, { call, put }) {
      console.log('listtype effect>>');
      const response = yield call(listtype, payload);
      yield put({
        type: 'listtype_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *listcriteria({ payload }, { call, put }) {
      console.log('listcriteria effect>>');
      const response = yield call(listcriteria, payload);
      yield put({
        type: 'listcriteria_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *viewedit({ payload }, { call, put }) {
      console.log('viewedit effect>>');
      const response = yield call(viewedit, payload);
      yield put({
        type: 'viewedit_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *insert({ payload }, { call, put }) {
      console.log('insert effect>>');
      const response = yield call(insert, payload);
      yield put({
        type: 'insert_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *update({ payload }, { call, put }) {
      console.log('update effect>>');
      const response = yield call(update, payload);
      yield put({
        type: 'update_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *delete_teacherobservation({ payload }, { call, put }) {
      console.log('delete_teacherobservation effect>>');
      const response = yield call(delete_teacherobservation, payload);
      yield put({
        type: 'delete_teacherobservation_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *processview({ payload }, { call, put }) {
      console.log('processview effect>>');
      const response = yield call(processview, payload);
      yield put({
        type: 'processview_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *processsummary({ payload }, { call, put }) {
      console.log('processsummary effect>>');
      const response = yield call(processsummary, payload);
      yield put({
        type: 'processsummary_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *processupdate({ payload }, { call, put }) {
      console.log('processupdate effect>>');
      const response = yield call(processupdate, payload);
      yield put({
        type: 'processupdate_reducer',
        payload: response.data,
      });
      return response.data;
    },
  },

  reducers: {
    list_reducer(state, action) {
      return {
        ...state,
        list_nhan_xet_du_gio: action.payload?.content,
      };
    },
    listcenter_reducer(state, action) {
      return {
        ...state,
        list_center: action.payload?.content,
      };
    },
    listclass_reducer(state, action) {
      return {
        ...state,
        list_lop_hoc: action.payload?.content,
      };
    },
    listtype_reducer(state, action) {
      return {
        ...state,
        list_loai_danh_gia: action.payload?.content,
      };
    },
    listcriteria_reducer(state, action) {
      return {
        ...state,
        list_cau_hoi_danh_gia: action.payload?.content,
      };
    },
    viewedit_reducer(state, action) {
      return {
        ...state,
        current_thong_tin: action.payload?.content,
      };
    },
    insert_reducer(state, action) {
      return {
        ...state,

      };
    },
    update_reducer(state, action) {
      return {
        ...state,

      };
    },
    delete_teacherobservation_reducer(state, action) {
      return {
        ...state,

      };
    },
    processview_reducer(state, action) {
      return {
        ...state,
        current_du_gio: action.payload?.content,
      };
    },
    processsummary_reducer(state, action) {
      return {
        ...state,

      };
    },
    processupdate_reducer(state, action) {
      return {
        ...state,

      };
    },
  },
};
