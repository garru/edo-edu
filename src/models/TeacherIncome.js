import {
    search
} from '../services/TeacherIncome';

export default {
  namespace: 'TeacherIncome',

  state: {
    list_teacher_income: [],
    summary: 0,
    total_record: 0
  },

  effects: {
    *search({ payload }, { call, put }) {
      console.log('search effect>>');
      const response = yield call(search, payload);
      yield put({
        type: 'search_reducer',
        payload: response.data,
      });
      return response.data;
    },
  },

  reducers: {
    search_reducer(state, action) {
      return {
        ...state,
        list_teacher_income: action.payload?.content?.records,
        total_record: action.payload?.content?.totalRecords,
        summary: action.payload?.content?.summary,
      };
    },
  },
};
