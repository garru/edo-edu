import { list, view, insert, update, delete_brand } from "../services/Brand";

export default {
  namespace: "Brand",

  state: {
    list_brand: [],
    current_brand: {},
    total_record: 0
  },

  effects: {
    *list({ payload }, { call, put }) {
      console.log("list effect>>");
      const response = yield call(list, payload);
      yield put({
        type: "list_reducer",
        payload: response.data
      });
      return response.data;
    },
    *view({ payload }, { call, put }) {
      console.log("view effect>>");
      const response = yield call(view, payload);
      yield put({
        type: "view_reducer",
        payload: response.data
      });
      return response.data;
    },
    *insert({ payload }, { call, put }) {
      console.log("insert effect>>");
      const response = yield call(insert, payload);
      yield put({
        type: "insert_reducer",
        payload: response.data
      });
      return response.data;
    },
    *update({ payload }, { call, put }) {
      console.log("update effect>>");
      const response = yield call(update, payload);
      yield put({
        type: "update_reducer",
        payload: response.data
      });
      return response.data;
    },
    *delete_brand({ payload }, { call, put }) {
      console.log("delete_brand effect>>");
      const response = yield call(delete_brand, payload);
      yield put({
        type: "delete_brand_reducer",
        payload: response.data
      });
      return response.data;
    }
  },

  reducers: {
    list_reducer(state, action) {
      return {
        ...state,
        list_brand: action.payload?.content
      };
    },
    view_reducer(state, action) {
      return {
        ...state,
        current_brand: action.payload?.content
      };
    },
    insert_reducer(state, action) {
      return {
        ...state
      };
    },
    update_reducer(state, action) {
      return {
        ...state
      };
    },
    delete_brand_reducer(state, action) {
      return {
        ...state
      };
    }
  }
};
