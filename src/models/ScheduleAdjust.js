import {
    view, lesson, evaluation, listcenter, listtime, listroom, listdomestic, listforeign, updatelessonbyid, updatelessonbyid
} from '../services/ScheduleAdjust';

export default {
  namespace: 'ScheduleAdjust',

  state: {
    current_lop_hoc: {},
    current_buoi_hoc: {},
    thong_tin_danh_gia_hoc_vien: {},
    list_center: [],
    list_gio_hoc: [],
    list_phong_hoc: [],
    list_giao_vien_vn: [],
    list_giao_vien_nn: [],
  },

  effects: {
    *view({ payload }, { call, put }) {
      console.log('view effect>>');
      const response = yield call(view, payload);
      yield put({
        type: 'view_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *lesson({ payload }, { call, put }) {
      console.log('lesson effect>>');
      const response = yield call(lesson, payload);
      yield put({
        type: 'lesson_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *evaluation({ payload }, { call, put }) {
      console.log('evaluation effect>>');
      const response = yield call(evaluation, payload);
      yield put({
        type: 'evaluation_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *listcenter({ payload }, { call, put }) {
      console.log('listcenter effect>>');
      const response = yield call(listcenter, payload);
      yield put({
        type: 'listcenter_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *listtime({ payload }, { call, put }) {
      console.log('listtime effect>>');
      const response = yield call(listtime, payload);
      yield put({
        type: 'listtime_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *listroom({ payload }, { call, put }) {
      console.log('listroom effect>>');
      const response = yield call(listroom, payload);
      yield put({
        type: 'listroom_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *listdomestic({ payload }, { call, put }) {
      console.log('listdomestic effect>>');
      const response = yield call(listdomestic, payload);
      yield put({
        type: 'listdomestic_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *listforeign({ payload }, { call, put }) {
      console.log('listforeign effect>>');
      const response = yield call(listforeign, payload);
      yield put({
        type: 'listforeign_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *updatelessonbyid({ payload }, { call, put }) {
      console.log('updatelessonbyid effect>>');
      const response = yield call(updatelessonbyid, payload);
      yield put({
        type: 'updatelessonbyid_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *updatelessonbyid({ payload }, { call, put }) {
      console.log('updatelessonbyid effect>>');
      const response = yield call(updatelessonbyid, payload);
      yield put({
        type: 'updatelessonbyid_reducer',
        payload: response.data,
      });
      return response.data;
    },
  },

  reducers: {
    view_reducer(state, action) {
      return {
        ...state,
        current_lop_hoc: action.payload?.content,
      };
    },
    lesson_reducer(state, action) {
      return {
        ...state,
        current_buoi_hoc: action.payload?.content,
      };
    },
    evaluation_reducer(state, action) {
      return {
        ...state,
        thong_tin_danh_gia_hoc_vien: action.payload?.content,
      };
    },
    listcenter_reducer(state, action) {
      return {
        ...state,
        list_center: action.payload?.content,
      };
    },
    listtime_reducer(state, action) {
      return {
        ...state,
        list_gio_hoc: action.payload?.content,
      };
    },
    listroom_reducer(state, action) {
      return {
        ...state,
        list_phong_hoc: action.payload?.content,
      };
    },
    listdomestic_reducer(state, action) {
      return {
        ...state,
        list_giao_vien_vn: action.payload?.content,
      };
    },
    listforeign_reducer(state, action) {
      return {
        ...state,
        list_giao_vien_nn: action.payload?.content,
      };
    },
    updatelessonbyid_reducer(state, action) {
      return {
        ...state,

      };
    },
    updatelessonbyid_reducer(state, action) {
      return {
        ...state,

      };
    },
  },
};
