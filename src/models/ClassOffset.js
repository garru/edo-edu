import { view, listclass, listprogram, offset } from "../services/ClassOffset";

export default {
  namespace: "ClassOffset",

  state: {
    list_lop_hoc: [],
    list_chuong_trinh: [],
    current: {}
  },

  effects: {
    *listprogram({ payload }, { call, put }) {
      try {
        const response = yield call(listcenter, payload);
        yield put({
          type: "listprogram_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *listlophoc({ payload }, { call, put }) {
      try {
        const response = yield call(listclass, payload);
        yield put({
          type: "listlophoc_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },

    *offset({ payload }, { call, put }) {
      console.log("update effect>>");
      try {
        const response = yield call(offset, payload);
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *reset({}, { put }) {
      yield put({
        type: "reset_reducer"
      });
    }
  },

  reducers: {
    listprogram_reducer(state, action) {
      return {
        ...state,
        list_chuong_trinh: action.payload?.content
      };
    },
    listlophoc_reducer(state, action) {
      return {
        ...state,
        list_lop_hoc: action.payload?.content
      };
    },
    view(state, action) {
      return {
        ...state,
        current: action.payload?.content
      };
    }
  }
};
