import {} from '../services/Mock';
import user from '../mock/user';
import lien_he from '../mock/lien_he';
import bo from '../mock/bo';
import me from '../mock/me';
import anh_em from '../mock/anh_em';
import tien_su_ban_than from '../mock/tien_su_ban_than';
import tien_su_thuoc_la from '../mock/tien_su_thuoc_la';
import tien_su_ruou_bia from '../mock/tien_su_ruou_bia';
import tien_su_qua_trinh_sinh_truong
  from '../mock/tien_su_qua_trinh_sinh_truong';
// import { getPatientByID } from "../services/Patient"

export default {
  namespace: 'Mock',

  state: {
    user: user,
    user_selected: localStorage.getItem('user_selected'),
    lien_he: lien_he,
    bo: bo,
    me: me,
    anh_em: anh_em,
    tien_su_ban_than: tien_su_ban_than,
    tien_su_thuoc_la: tien_su_thuoc_la,
    tien_su_ruou_bia: tien_su_ruou_bia,
    tien_su_qua_trinh_sinh_truong: tien_su_qua_trinh_sinh_truong,
  },

  effects: {
    * selectUser({ payload }, { call, put }) {
      console.log('selectUser effect>>');
      yield put({
        type: 'selectUser_reducer',
        payload: payload,
      });
      return payload;
    },

  },

  reducers: {
    selectUser_reducer(state, action) {
      var user_selected = state.user.findIndex(x => x.id == action.payload?.id);
      localStorage.setItem('user_selected', user_selected, action);
      console.log('selectUser_reducer>>>', user_selected)
      return {
        ...state,
        user_selected: user_selected,
      };
    },
  },
};
