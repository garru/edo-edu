import {
    applyfor, list, view, insert, update, delete_studentstatus
} from '../services/StudentStatus';

export default {
  namespace: 'StudentStatus',

  state: {
    list_danh_muc_ap_dung_cho: [],
    list_student_status: [],
    total_record: 0,
    current_student_status: {},
  },

  effects: {
    *applyfor({ payload }, { call, put }) {
      console.log('applyfor effect>>');
      const response = yield call(applyfor, payload);
      yield put({
        type: 'applyfor_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *list({ payload }, { call, put }) {
      console.log('list effect>>');
      const response = yield call(list, payload);
      yield put({
        type: 'list_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *view({ payload }, { call, put }) {
      console.log('view effect>>');
      const response = yield call(view, payload);
      yield put({
        type: 'view_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *insert({ payload }, { call, put }) {
      console.log('insert effect>>');
      const response = yield call(insert, payload);
      yield put({
        type: 'insert_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *update({ payload }, { call, put }) {
      console.log('update effect>>');
      const response = yield call(update, payload);
      yield put({
        type: 'update_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *delete_studentstatus({ payload }, { call, put }) {
      console.log('delete_studentstatus effect>>');
      const response = yield call(delete_studentstatus, payload);
      yield put({
        type: 'delete_studentstatus_reducer',
        payload: response.data,
      });
      return response.data;
    },
  },

  reducers: {
    applyfor_reducer(state, action) {
      return {
        ...state,
        list_danh_muc_ap_dung_cho: action.payload?.content,
      };
    },
    list_reducer(state, action) {
      return {
        ...state,
        list_student_status: action.payload?.content,
        total_record: action.payload?.content?.length
      };
    },
    view_reducer(state, action) {
      return {
        ...state,
        current_student_status: action.payload?.content,
      };
    },
    insert_reducer(state, action) {
      return {
        ...state,

      };
    },
    update_reducer(state, action) {
      return {
        ...state,

      };
    },
    delete_studentstatus_reducer(state, action) {
      return {
        ...state,

      };
    },
  },
};
