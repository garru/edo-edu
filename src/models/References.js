import {
  getGioiTinh,
  getTinhTrangHonNhan,
  getDanhMucHopDong,
  getDanhSachThuongHieu,
  getDanhSachTrungTam,
  getDanhSachPhongBan,
  getDanhSachQuyen,
  getDanhSachDoiTuongDanhGia,
  getTieuChiDanhGia,
  getDanhMucApDungCho
} from "../services/References";

export default {
  namespace: "References",

  state: {
    current: {},
    GioiTinh: [],
    TinhTrangHonNhan: [],
    DanhMucHopDong: [],
    DanhSachThuongHieu: [],
    DanhSachTrungTam: [],
    DanhSachPhongBan: [],
    DanhSachQuyen: [],
    DanhSachDoiTuongDanhGia: [],
    TieuChiDanhGia: [],
    DanhMucApDungCho: []
  },

  effects: {
    *getGioiTinh({ payload }, { call, put }) {
      console.log("getGioiTinh>>");
      const response = yield call(getGioiTinh, payload);
      yield put({
        type: "gt_reducer",
        payload: response.data
      });
      return response.data;
    },
    *getTinhTrangHonNhan({ payload }, { call, put }) {
      console.log("getTinhTrangHonNhan>>");
      const response = yield call(getTinhTrangHonNhan, payload);
      yield put({
        type: "tthn_reducer",
        payload: response.data
      });
      return response.data;
    },
    *getDanhMucHopDong({ payload }, { call, put }) {
      console.log("getDanhMucHopDong");
      const response = yield call(getDanhMucHopDong, payload);
      yield put({
        type: "dmhd_reducer",
        payload: response.data
      });
      return response.data;
    },
    *getDanhSachThuongHieu({ payload }, { call, put }) {
      console.log("getDanhSachThuongHieu>>");
      const response = yield call(getDanhSachThuongHieu, payload);
      yield put({
        type: "dsth_reducer",
        payload: response.data
      });
      return response.data;
    },
    *getDanhSachTrungTam({ payload }, { call, put }) {
      console.log("getDanhSachTrungTam>>");
      const response = yield call(getDanhSachTrungTam, payload);
      yield put({
        type: "dstt_reducer",
        payload: response.data
      });
      return response.data;
    },
    *getDanhSachPhongBan({ payload }, { call, put }) {
      console.log("getDanhSachPhongBan>>");
      const response = yield call(getDanhSachPhongBan, payload);
      yield put({
        type: "dspb_reducer",
        payload: response.data
      });
      return response.data;
    },
    *getDanhSachQuyen({ payload }, { call, put }) {
      console.log("getDanhSachQuyen>>");
      const response = yield call(getDanhSachQuyen, payload);
      yield put({
        type: "dsq_reducer",
        payload: response.data
      });
      return response.data;
    },
    *getDanhSachDoiTuongDanhGia({ payload }, { call, put }) {
      console.log("getDanhSachDoiTuongDanhGia>>");
      const response = yield call(getDanhSachDoiTuongDanhGia, payload);
      yield put({
        type: "dsdtdg_reducer",
        payload: response.data
      });
      return response.data;
    },
    *getTieuChiDanhGia({ payload }, { call, put }) {
      console.log("getTieuChiDanhGia>>");
      const response = yield call(getTieuChiDanhGia, payload);
      yield put({
        type: "dstcdg_reducer",
        payload: response.data
      });
      return response.data;
    },
    *getDanhMucApDungCho({ payload }, { call, put }) {
      console.log("getDanhMucApDungCho>>");
      const response = yield call(getDanhMucApDungCho, payload);
      yield put({
        type: "dmadc_reducer",
        payload: response.data
      });
      return response.data;
    }
  },

  reducers: {
    gt_reducer(state, action) {
      return {
        ...state,
        list: action.payload?.content
      };
    },
    tthn_reducer(state, action) {
      return {
        ...state,
        TinhTrangHonNhan: action.payload?.content
      };
    },
    dmhd_reducer(state, action) {
      return {
        ...state,
        DanhMucHopDong: action.payload?.content
      };
    },
    dsth_reducer(state, action) {
      return {
        ...state,
        DanhSachTrungTam: action.payload?.content
      };
    },
    dspb_reducer(state, action) {
      return {
        ...state,
        DanhSachPhongBan: action.payload?.content
      };
    },
    dsq_reducer(state, action) {
      return {
        ...state,
        DanhSachQuyen: action.payload?.content
      };
    },
    dsdtdg_reducer(state, action) {
      return {
        ...state,
        DanhSachDoiTuongDanhGia: action.payload?.content
      };
    },
    dstcdg_reducer(state, action) {
      return {
        ...state,
        TieuChiDanhGia: action.payload?.content
      };
    },
    dmadc_reducer(state, action) {
      return {
        ...state,
        DanhMucApDungCho: action.payload?.content
      };
    }
  }
};
