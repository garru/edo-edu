import {
  search,
  view,
  insert,
  update,
  delete_holiday
} from "../services/Holiday";

export default {
  namespace: "Holiday",

  state: {
    list_holiday: [],
    current_holiday: {},
    total_record: 0,
    options: {
      pageNumber: 1,
      pageSize: 10,
      sortBy: ""
    }
  },

  effects: {
    *search({ payload }, { call, put }) {
      console.log("search effect>>");
      const response = yield call(search, payload);
      yield put({
        type: "search_reducer",
        payload: response.data
      });
      return response.data;
    },
    *view({ payload }, { call, put }) {
      console.log("view effect>>");
      const response = yield call(view, payload);
      yield put({
        type: "view_reducer",
        payload: response.data
      });
      return response.data;
    },
    *insert({ payload }, { call, put }) {
      console.log("insert effect>>");
      const response = yield call(insert, payload);
      yield put({
        type: "insert_reducer",
        payload: response.data
      });
      return response.data;
    },
    *update({ payload }, { call, put }) {
      console.log("update effect>>");
      const response = yield call(update, payload);
      yield put({
        type: "update_reducer",
        payload: response.data
      });
      return response.data;
    },
    *delete_holiday({ payload }, { call, put }) {
      console.log("delete_holiday effect>>");
      const response = yield call(delete_holiday, payload);
      yield put({
        type: "delete_holiday_reducer",
        payload: response.data
      });
      return response.data;
    },
    *updateoptions({ payload }, { call, put, select }) {
      console.log("updateoptions effect>>");
      const oldOptions = yield select(state => state.Holiday.options);
      const newOption = { ...oldOptions, ...payload };
      yield put({
        type: "update_options_reducer",
        payload: payload
      });
      try {
        const response = yield call(search, newOption);
        yield put({
          type: "search_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    }
  },

  reducers: {
    search_reducer(state, action) {
      return {
        ...state,
        list_holiday: action.payload?.content?.records,
        total_record: action.payload?.content?.totalRecords || 0
      };
    },
    view_reducer(state, action) {
      return {
        ...state,
        current_holiday: action.payload?.content
      };
    },
    insert_reducer(state, action) {
      return {
        ...state
      };
    },
    update_reducer(state, action) {
      return {
        ...state
      };
    },
    delete_holiday_reducer(state, action) {
      return {
        ...state
      };
    },
    update_options_reducer(state, action) {
      return {
        ...state,
        options: { ...state.options, ...action.payload }
      };
    }
  }
};
