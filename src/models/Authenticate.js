import { login, refresh_token } from "../services/Authenticate";

export default {
  namespace: "Authenticate",

  state: {},

  effects: {
    *login({ payload }, { call, put }) {
      console.log("login effect>>");
      try {
        const response = yield call(login, JSON.stringify(payload));
        yield put({
          type: "login_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return error;
      }
    },
    *refresh_token({ payload }, { call, put }) {
      console.log("refresh_token effect>>");
      const response = yield call(refresh_token, payload);
      yield put({
        type: "refresh_token_reducer",
        payload: response.data
      });
      return response.data;
    }
  },

  reducers: {
    login_reducer(state, action) {
      return {
        ...state
      };
    },
    refresh_token_reducer(state, action) {
      return {
        ...state
      };
    }
  }
};
