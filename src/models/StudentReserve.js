import {
  viewreserve,
  listmonth,
  viewback,
  listcenter,
  listprogram,
  listclass,
  listschedule,
  insertreserve,
  insertback
} from "../services/StudentReserve";

export default {
  namespace: "StudentReserve",

  state: {
    viewreserve: {},
    listmonth: [],
    viewback: {},
    listcenter: [],
    listprogram: [],
    listclass: [],
    listschedule: []
  },

  effects: {
    *insertback({ payload }, { call, put }) {
      try {
        console.log("insertback effect>>");
        const response = yield call(insertback, payload);
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *insertreserve({ payload }, { call, put }) {
      try {
        console.log("insertreserve effect>>");
        const response = yield call(insertreserve, payload);
        yield put({
          type: "insertreserve_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *listschedule({ payload }, { call, put }) {
      try {
        console.log("listschedule effect>>");
        const response = yield call(listschedule, payload);
        yield put({
          type: "listschedule_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *listmonth({ payload }, { call, put }) {
      try {
        console.log("listmonth effect>>");
        const response = yield call(listmonth, payload);
        yield put({
          type: "listmonth_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *viewback({ payload }, { call, put }) {
      try {
        console.log("viewback effect>>");
        const response = yield call(viewback, payload);
        yield put({
          type: "viewback_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *viewreserve({ payload }, { call, put }) {
      try {
        console.log("viewreserve effect>>");
        const response = yield call(viewreserve, payload);
        yield put({
          type: "viewreserve_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *listcenter({ payload }, { call, put }) {
      try {
        console.log("listcenter effect>>");
        const response = yield call(listcenter, payload);
        yield put({
          type: "listcenter_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *listprogram({ payload }, { call, put }) {
      try {
        console.log("listprogram effect>>");
        const response = yield call(listprogram, payload);
        yield put({
          type: "listprogram_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *listclass({ payload }, { call, put }) {
      try {
        console.log("listclass effect>>");
        const response = yield call(listclass, payload);
        yield put({
          type: "listclass_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *listcenterModal({ payload }, { call, put }) {
      try {
        console.log("listcenter effect>>");
        const response = yield call(listcenter, payload);
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *listprogramModal({ payload }, { call, put }) {
      try {
        console.log("listprogram effect>>");
        const response = yield call(listprogram, payload);
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *listclassModal({ payload }, { call, put }) {
      try {
        console.log("listclass effect>>");
        const response = yield call(listclass, payload);
        return response.data;
      } catch (error) {
        return false;
      }
    },
    // *viewModal({ payload }, { call, put }) {
    //   try {
    //     console.log("view effect>>");
    //     const response = yield call(view, payload);
    //     return response.data;
    //   } catch (error) {
    //     return false;
    //   }
    // },
    // *changeclass({ payload }, { call, put }) {
    //   try {
    //     console.log("changeclass effect>>");
    //     const response = yield call(changeclass, payload);
    //     return response.data;
    //   } catch (error) {
    //     return false;
    //   }
    // }
  },

  reducers: {
    listcenter_reducer(state, action) {
      return {
        ...state,
        listcenter: action.payload?.content
      };
    },
    listclass_reducer(state, action) {
      return {
        ...state,
        listclass: action.payload?.content
      };
    },
    listprogram_reducer(state, action) {
      return {
        ...state,
        listprogram: action.payload?.content
      };
    },
    listmonth_reducer(state, action) {
      return {
        ...state,
        listmonth: action.payload?.content
      };
    },
    viewback_reducer(state, action) {
      return {
        ...state,
        viewback: action.payload?.content
      };
    },
    listschedule_reducer(state, action) {
      return {
        ...state,
        listschedule: action.payload?.content
      };
    },
    viewreserve_reducer(state, action) {
      return {
        ...state,
        viewreserve: action.payload?.content
      };
    }
  }
};
