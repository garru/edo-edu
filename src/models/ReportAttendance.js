import {
  listcenter,
  listclass,
  download,
  search
} from "../services/ReportAttendance";
import { view } from "../services/ScheduleAttendance";

export default {
  namespace: "ReportAttendance",

  state: {
    list_trung_tam: [],
    list_lop_hoc: [],
    options: {
      class_Id: "",
      pageNumber: 1,
      pageSize: 10,
      sortBy: "name"
    },
    student_name: [],
    list: [],
    current_class: {},
    total_record: 0
  },

  effects: {
    *view({ payload }, { call, put }) {
      console.log("view effect>>");
      try {
        const response = yield call(view, payload);
        yield put({
          type: "view_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *listtrungtam({ payload }, { call, put }) {
      try {
        const response = yield call(listcenter, payload);
        yield put({
          type: "listtrungtam_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *listlophoc({ payload }, { call, put }) {
      try {
        const response = yield call(listclass, payload);
        yield put({
          type: "listlophoc_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },

    *updateoptions({ payload }, { call, put, select }) {
      try {
        console.log("updateoptions effect>>");
        const oldOptions = yield select(
          state => state.ReportAttendance.options
        );
        const newOption = { ...oldOptions, ...payload };
        yield put({
          type: "update_options_reducer",
          payload: payload
        });
        const response = yield call(search, newOption);
        yield put({
          type: "search_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *search({ payload }, { call, put, select }) {
      try {
        console.log("search effect>>");
        const params = yield select(state => state.Account.options);
        const response = yield call(search, params);
        yield put({
          type: "search_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *download({ payload }, { call }) {
      try {
        console.log("search effect>>");
        const response = yield call(download, payload);
        console.log(response.data)
        return response.data;
      } catch (error) {
        return false;
      }
    }
  },

  reducers: {
    listtrungtam_reducer(state, action) {
      return {
        ...state,
        list_trung_tam: action.payload?.content
      };
    },

    listlophoc_reducer(state, action) {
      return {
        ...state,
        list_lop_hoc: action.payload?.content
      };
    },

    search_reducer(state, action) {
      return {
        ...state,
        list: action.payload?.content?.records,
        total_record: action.payload?.content?.totalRecords,
        student_name: action.payload?.content?.studentNames
      };
    },
    update_options_reducer(state, action) {
      console.log(state, action.payload);
      return {
        ...state,
        options: { ...state.options, ...action.payload }
      };
    },
    view_reducer(state, action) {
      return {
        ...state,
        current_class: action.payload?.content
      };
    }
  }
};
