import {
  departement,
  listbydepartmentid,
  list,
  view,
  insert,
  update,
  delete_departementrole
} from "../services/DepartementRole";

export default {
  namespace: "DepartementRole",

  state: {
    list_phong_ban: [],
    list_vai_tro: [],
    curent_role: {}
  },

  effects: {
    *departement({ payload }, { call, put }) {
      console.log("departement effect>>");
      const response = yield call(departement, payload);
      yield put({
        type: "departement_reducer",
        payload: response.data
      });
      return response.data;
    },
    *listbydepartmentid({ payload }, { call, put }) {
      console.log("listbydepartmentid effect>>");
      const response = yield call(listbydepartmentid, payload);
      yield put({
        type: "listbydepartmentid_reducer",
        payload: response.data
      });
      return response.data;
    },
    *list({ payload }, { call, put }) {
      console.log("list effect>>");
      try {
        const response = yield call(list, payload);
        yield put({
          type: "list_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *view({ payload }, { call, put }) {
      console.log("view effect>>");
      try {
        const response = yield call(view, payload);
        yield put({
          type: "view_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *insert({ payload }, { call, put }) {
      console.log("insert effect>>");
      try {
        const response = yield call(insert, payload);
        yield put({
          type: "insert_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *update({ payload }, { call, put }) {
      console.log("update effect>>");
      try {
        const response = yield call(update, payload);
        yield put({
          type: "update_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *delete_departementrole({ payload }, { call, put }) {
      console.log("delete_departementrole effect>>");
      try {
        const response = yield call(delete_departementrole, payload);
        yield put({
          type: "delete_departementrole_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    }
  },

  reducers: {
    departement_reducer(state, action) {
      return {
        ...state,
        list_phong_ban: action.payload?.content
      };
    },
    listbydepartmentid_reducer(state, action) {
      return {
        ...state,
        list_vai_tro: action.payload?.content
      };
    },
    list_reducer(state, action) {
      return {
        ...state,
        list_vai_tro: action.payload?.content
      };
    },
    view_reducer(state, action) {
      return {
        ...state,
        curent_role: action.payload?.content
      };
    },
    insert_reducer(state, action) {
      return {
        ...state
      };
    },
    update_reducer(state, action) {
      return {
        ...state
      };
    },
    delete_departementrole_reducer(state, action) {
      return {
        ...state
      };
    }
  }
};
