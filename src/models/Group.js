import {
  list,
  permission,
  view,
  insert,
  update,
  delete_group
} from "../services/Group";

export default {
  namespace: "Group",

  state: {
    list_group: [],
    list_quyen_he_thong: [],
    current_group: {}
  },

  effects: {
    *list({ payload }, { call, put }) {
      console.log("list effect>>");
      const response = yield call(list, payload);
      yield put({
        type: "list_reducer",
        payload: response.data
      });
      return response.data;
    },
    *permission({ payload }, { call, put }) {
      console.log("permission effect>>");
      const response = yield call(permission, payload);
      yield put({
        type: "permission_reducer",
        payload: response.data
      });
      return response.data;
    },
    *view({ payload }, { call, put }) {
      console.log("view effect>>");
      const response = yield call(view, payload);
      yield put({
        type: "view_reducer",
        payload: response.data
      });
      return response.data;
    },
    *insert({ payload }, { call, put }) {
      console.log("insert effect>>");
      const response = yield call(insert, payload);
      yield put({
        type: "insert_reducer",
        payload: response.data
      });
      return response.data;
    },
    *update({ payload }, { call, put }) {
      console.log("update effect>>");
      const response = yield call(update, payload);
      yield put({
        type: "update_reducer",
        payload: response.data
      });
      return response.data;
    },
    *delete_group({ payload }, { call, put }) {
      console.log("delete_group effect>>");
      const response = yield call(delete_group, payload);
      yield put({
        type: "delete_group_reducer",
        payload: response.data
      });
      return response.data;
    }
  },

  reducers: {
    list_reducer(state, action) {
      return {
        ...state,
        list_group: action.payload?.content
      };
    },
    permission_reducer(state, action) {
      return {
        ...state,
        list_quyen_he_thong: action.payload?.content
      };
    },
    view_reducer(state, action) {
      return {
        ...state,
        current_group: action.payload?.content
      };
    },
    insert_reducer(state, action) {
      return {
        ...state
      };
    },
    update_reducer(state, action) {
      return {
        ...state
      };
    },
    delete_group_reducer(state, action) {
      return {
        ...state
      };
    }
  }
};
