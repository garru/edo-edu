import {
  currentstudent,
  currentschedule,
  currentreserve,
  currentcredit
} from "../services/StudentDetail";

export default {
  namespace: "StudentDetail",

  state: {
    currentstudent: {},
    currentschedule: [],
    currentreserve: [],
    currentcredit: 0,
    creditHistory: []
  },

  effects: {
    *currentstudent({ payload }, { call, put }) {
      try {
        console.log("currentstudent effect>>");
        const response = yield call(currentstudent, payload);
        yield put({
          type: "currentstudent_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *currentschedule({ payload }, { call, put }) {
      try {
        console.log("currentschedule effect>>");
        const response = yield call(currentschedule, payload);
        yield put({
          type: "currentschedule_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *currentreserve({ payload }, { call, put }) {
      try {
        console.log("currentreserve effect>>");
        const response = yield call(currentreserve, payload);
        yield put({
          type: "currentreserve_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *currentcredit({ payload }, { call, put }) {
      try {
        console.log("currentcredit effect>>");
        const response = yield call(currentcredit, payload);
        yield put({
          type: "currentcredit_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    }
  },

  reducers: {
    currentstudent_reducer(state, action) {
      return {
        ...state,
        currentstudent: action.payload?.content
      };
    },
    currentschedule_reducer(state, action) {
      return {
        ...state,
        currentschedule: action.payload?.content
      };
    },
    currentreserve_reducer(state, action) {
      return {
        ...state,
        currentreserve: action.payload?.content
      };
    },
    currentcredit_reducer(state, action) {
      return {
        ...state,
        currentcredit: action.payload?.content?.creditCurrent,
        creditHistory: action.payload?.content?.creditHistory
      };
    }
  }
};
