import {
    listcenter, listclass, merge
} from '../services/ClassMerge';

export default {
  namespace: 'ClassMerge',

  state: {
    list_center: [],
    list_lop_hoc: [],
  },

  effects: {
    *listcenter({ payload }, { call, put }) {
      console.log('listcenter effect>>');
      const response = yield call(listcenter, payload);
      yield put({
        type: 'listcenter_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *listclass({ payload }, { call, put }) {
      console.log('listclass effect>>');
      const response = yield call(listclass, payload);
      yield put({
        type: 'listclass_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *merge({ payload }, { call, put }) {
      console.log('merge effect>>');
      const response = yield call(merge, payload);
      yield put({
        type: 'merge_reducer',
        payload: response.data,
      });
      return response.data;
    },
  },

  reducers: {
    listcenter_reducer(state, action) {
      return {
        ...state,
        list_center: action.payload?.content,
      };
    },
    listclass_reducer(state, action) {
      return {
        ...state,
        list_lop_hoc: action.payload?.content,
      };
    },
    merge_reducer(state, action) {
      return {
        ...state,

      };
    },
  },
};
