import {
    search, view, viewprice, insertprice, updateprice, deleteprice
} from '../services/Teacher';

export default {
  namespace: 'Teacher',

  state: {
    list_giao_vien: [],
    current_giao_vien: {},
    total_record: 0
  },

  effects: {
    *search({ payload }, { call, put }) {
      console.log('search effect>>');
      const response = yield call(search, payload);
      yield put({
        type: 'search_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *view({ payload }, { call, put }) {
      console.log('view effect>>');
      const response = yield call(view, payload);
      yield put({
        type: 'view_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *viewprice({ payload }, { call, put }) {
      console.log('viewprice effect>>');
      const response = yield call(viewprice, payload);
      yield put({
        type: 'viewprice_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *insertprice({ payload }, { call, put }) {
      console.log('insertprice effect>>');
      const response = yield call(insertprice, payload);
      yield put({
        type: 'insertprice_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *updateprice({ payload }, { call, put }) {
      console.log('updateprice effect>>');
      const response = yield call(updateprice, payload);
      yield put({
        type: 'updateprice_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *deleteprice({ payload }, { call, put }) {
      console.log('deleteprice effect>>');
      const response = yield call(deleteprice, payload);
      yield put({
        type: 'deleteprice_reducer',
        payload: response.data,
      });
      return response.data;
    },
  },

  reducers: {
    search_reducer(state, action) {
      return {
        ...state,
        list_giao_vien: action.payload?.content || [],
      };
    },
    view_reducer(state, action) {
      return {
        ...state,
        current_giao_vien: action.payload?.content,
        total_record: action.payload?.content?.length
      };
    },
    viewprice_reducer(state, action) {
      return {
        ...state,
        current_don_gia_giao_vien: action.payload?.content,
      };
    },
    insertprice_reducer(state, action) {
      return {
        ...state,

      };
    },
    updateprice_reducer(state, action) {
      return {
        ...state,

      };
    },
    deleteprice_reducer(state, action) {
      return {
        ...state,

      };
    },
  },
};
