import {
    search, upload, template, view, insert, update, delete_program
} from '../services/Program';

export default {
  namespace: 'Program',

  state: {
    list_program: [],
    current_program: {},
    total_record: 0,
  },

  effects: {
    *search({ payload }, { call, put }) {
      console.log('search effect>>');
      const response = yield call(search, payload);
      yield put({
        type: 'search_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *upload({ payload }, { call, put }) {
      console.log('upload effect>>');
      const response = yield call(upload, payload);
      yield put({
        type: 'upload_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *template({ payload }, { call, put }) {
      console.log('template effect>>');
      const response = yield call(template, payload);
      yield put({
        type: 'template_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *view({ payload }, { call, put }) {
      console.log('view effect>>');
      const response = yield call(view, payload);
      yield put({
        type: 'view_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *insert({ payload }, { call, put }) {
      console.log('insert effect>>');
      const response = yield call(insert, payload);
      yield put({
        type: 'insert_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *update({ payload }, { call, put }) {
      console.log('update effect>>');
      const response = yield call(update, payload);
      yield put({
        type: 'update_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *delete_program({ payload }, { call, put }) {
      console.log('delete_program effect>>');
      const response = yield call(delete_program, payload);
      yield put({
        type: 'delete_program_reducer',
        payload: response.data,
      });
      return response.data;
    },
  },

  reducers: {
    search_reducer(state, action) {
      return {
        ...state,
        list_program: action.payload?.content?.content?.records || [],
        total_record: action.payload?.content?.content?.totalRecords || 0
      };
    },
    upload_reducer(state, action) {
      return {
        ...state,

      };
    },
    template_reducer(state, action) {
      return {
        ...state,

      };
    },
    view_reducer(state, action) {
      return {
        ...state,
        current_program: action.payload?.content,
      };
    },
    insert_reducer(state, action) {
      return {
        ...state,

      };
    },
    update_reducer(state, action) {
      return {
        ...state,

      };
    },
    delete_program_reducer(state, action) {
      return {
        ...state,

      };
    },
  },
};
