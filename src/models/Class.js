import {
  listcenter,
  listprogram,
  listmanage,
  listsurchage,
  listroom,
  search,
  view,
  insert,
  update,
  delete_class,
  liststatus,
  listprice
} from "../services/Class";

export default {
  namespace: "Class",

  state: {
    list: [],
    current: {},
    total_record: 0,
    options: {
      codeOrName: "",
      center_Id: "",
      program_Id: "",
      manage_Id: "",
      pageNumber: 1,
      pageSize: 10,
      sortBy: "id"
    },
    list_trung_tam: [],
    list_chuong_trinh: [],
    list_quan_ly: [],
    list_phu_thu: [],
    list_goi_hoc_phi: [],
    list_phong: [],
    list_trang_thai: []
  },

  effects: {
    *listcenter({ payload }, { call, put }) {
      console.log("listcenter effect>>");
      const response = yield call(listcenter, payload);
      yield put({
        type: "listcenter_reducer",
        payload: response.data
      });
      return response.data;
    },
    *listprogram({ payload }, { call, put }) {
      console.log("listprogram effect>>");
      const response = yield call(listprogram, payload);
      yield put({
        type: "listprogram_reducer",
        payload: response.data
      });
      return response.data;
    },
    *listprice({ payload }, { call, put }) {
      console.log("listprice effect>>");
      const response = yield call(listprice, payload);
      yield put({
        type: "listprice_reducer",
        payload: response.data
      });
      return response.data;
    },
    *updateoptions({ payload }, { call, put, select }) {
      console.log("updateoptions effect>>");
      const oldOptions = yield select(state => state.Class.options);
      const newOption = { ...oldOptions, ...payload };
      yield put({
        type: "update_options_reducer",
        payload: payload
      });
      try {
        const response = yield call(search, newOption);
        yield put({
          type: "search_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },

    *listmanage({ payload }, { call, put }) {
      console.log("listmanage effect>>", payload);
      const response = yield call(listmanage, payload);
      yield put({
        type: "listmanage_reducer",
        payload: response.data
      });
      return response.data;
    },
    *liststatus({ payload }, { call, put }) {
      console.log("liststatus effect>>", payload);
      const response = yield call(liststatus, payload);
      yield put({
        type: "liststatus_reducer",
        payload: response.data
      });
      return response.data;
    },
    *listsurchage({ payload }, { call, put }) {
      console.log("listsurchage effect>>");
      const response = yield call(listsurchage, payload);
      yield put({
        type: "listsurchage_reducer",
        payload: response.data
      });
      return response.data;
    },
    *listroom({ payload }, { call, put }) {
      console.log("listroom effect>>");
      const response = yield call(listroom, payload);
      yield put({
        type: "listroom_reducer",
        payload: response.data
      });
      return response.data;
    },
    *search({}, { call, put, select }) {
      const params = yield select(state => state.Class.options);
      console.log("search effect>>", params);
      const response = yield call(search, JSON.stringify(params));
      yield put({
        type: "search_reducer",
        payload: response.data
      });
      return response.data;
    },
    *view({ payload }, { call, put }) {
      console.log("view effect>>");
      const response = yield call(view, payload);
      yield put({
        type: "view_reducer",
        payload: response.data
      });
      return response.data;
    },
    *insert({ payload }, { call, put }) {
      console.log("insert effect>>");
      const response = yield call(insert, payload);
      yield put({
        type: "insert_reducer",
        payload: response.data
      });
      return response.data;
    },
    *update({ payload }, { call, put }) {
      console.log("update effect>>");
      const response = yield call(update, payload);
      yield put({
        type: "update_reducer",
        payload: response.data
      });
      return response.data;
    },
    *delete({ payload }, { call, put }) {
      try {
        console.log("delete effect>>");
        const response = yield call(delete_class, payload);
        yield put({
          type: "delete_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    }
  },

  reducers: {
    listcenter_reducer(state, action) {
      return {
        ...state,
        list_trung_tam: action.payload?.content
      };
    },
    listprogram_reducer(state, action) {
      return {
        ...state,
        list_chuong_trinh: action.payload?.content
      };
    },
    listprice_reducer(state, action) {
      return {
        ...state,
        list_goi_hoc_phi: action.payload?.content
      };
    },
    update_options_reducer(state, action) {
      return {
        ...state,
        options: { ...state.options, ...action.payload }
      };
    },
    listmanage_reducer(state, action) {
      return {
        ...state,
        list_quan_ly: action.payload?.content
      };
    },
    liststatus_reducer(state, action) {
      return {
        ...state,
        list_trang_thai: action.payload?.content
      };
    },
    listsurchage_reducer(state, action) {
      return {
        ...state,
        list_phu_thu: action.payload?.content
      };
    },
    listroom_reducer(state, action) {
      return {
        ...state,
        list_phong: action.payload?.content
      };
    },
    search_reducer(state, action) {
      return {
        ...state,
        list: action.payload?.content?.records || [],
        total_record: action.payload?.content?.totalRecords || 0
      };
    },
    view_reducer(state, action) {
      return {
        ...state,
        current_account: action.payload?.content || {}
      };
    },
    insert_reducer(state, action) {
      return {
        ...state
      };
    },
    update_reducer(state, action) {
      return {
        ...state
      };
    },
    delete_reducer(state, action) {
      return {
        ...state
      };
    }
  }
};
