import { view, updateinfo, updateavatar } from "../services/User";

export default {
  namespace: "User",

  state: {
    curent_user: {},
    list_trung_tam: []
  },

  effects: {
    *view({ payload }, { call, put }) {
      console.log("view effect>>");
      const response = yield call(view, payload);
      yield put({
        type: "view_reducer",
        payload: response.data
      });
      return response.data;
    },
    *updateinfo({ payload }, { call, put }) {
      console.log("updateinfo effect>>");
      try {
        const response = yield call(updateinfo, JSON.stringify(payload));
        yield put({
          type: "updateinfo_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *updateavatar({ payload }, { call, put }) {
      console.log("updateavatar effect>>");
      const response = yield call(updateavatar, JSON.stringify(payload));
      yield put({
        type: "updateavatar_reducer",
        payload: response.data
      });
      return response.data;
    }
  },

  reducers: {
    view_reducer(state, action) {
      return {
        ...state,
        curent_user: action.payload?.content
      };
    },
    updateinfo_reducer(state, action) {
      return {
        ...state
      };
    },
    updateavatar_reducer(state, action) {
      return {
        ...state
      };
    }
  }
};
