import {
    schedulenext, schedulecourseware, coursewarereceive, coursewarereturn, todolist, workdaystatistic
} from '../services/Dashboard';

export default {
  namespace: "Dashboard",

  state: {
    list_lop_hoc: [],
    list_cong_viec: [],
    list_ngay_cong: []
  },

  effects: {
    *schedulenext({ payload }, { call, put }) {
      try {
        const response = yield call(schedulenext, payload);
        yield put({
          type: "schedulenext_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *schedulecourseware({ payload }, { call, put }) {
      console.log('schedulecourseware effect>>');
      const response = yield call(schedulecourseware, payload);
      yield put({
        type: 'schedulecourseware_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *coursewarereceive({ payload }, { call, put }) {
      console.log('coursewarereceive effect>>');
      const response = yield call(coursewarereceive, payload);
      yield put({
        type: 'coursewarereceive_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *coursewarereturn({ payload }, { call, put }) {
      console.log('coursewarereturn effect>>');
      const response = yield call(coursewarereturn, payload);
      yield put({
        type: 'coursewarereturn_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *todolist({ payload }, { call, put }) {
      try {
        console.log("todolist effect>>");
        const response = yield call(todolist, payload);
        yield put({
          type: "todolist_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *workdaystatistic({ payload }, { call, put }) {
      try {
        console.log("workdaystatistic effect>>");
        const response = yield call(workdaystatistic, payload);
        yield put({
          type: "workdaystatistic_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    }
  },

  reducers: {
    schedulenext_reducer(state, action) {
      console.log("schedulenext_reducer", action.payload?.content);
      return {
        ...state,
        list_lop_hoc: action.payload?.content
      };
    },
    schedulecourseware_reducer(state, action) {
      return {
        ...state,
        list_hoc_lieu: action.payload?.content,
      };
    },
    coursewarereceive_reducer(state, action) {
      return {
        ...state,

      };
    },
    coursewarereturn_reducer(state, action) {
      return {
        ...state,

      };
    },
    todolist_reducer(state, action) {
      console.log("todolist_reducer", action.payload?.content);
      return {
        ...state,
        list_cong_viec: action.payload?.content
      };
    },
    workdaystatistic_reducer(state, action) {
      console.log("workdaystatistic_reducer", action.payload?.content);
      return {
        ...state,
        list_ngay_cong: action.payload?.content
      };
    }
  }
};
