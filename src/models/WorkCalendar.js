import {
    search, view, upload, template, import_workcalendar, insert, update, delete_workcalendar
} from '../services/WorkCalendar';

export default {
  namespace: 'WorkCalendar',

  state: {
    list_work_calendar: [],
    current_work_calendar: {},
  },

  effects: {
    *search({ payload }, { call, put }) {
      console.log('search effect>>');
      const response = yield call(search, payload);
      yield put({
        type: 'search_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *view({ payload }, { call, put }) {
      console.log('view effect>>');
      const response = yield call(view, payload);
      yield put({
        type: 'view_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *upload({ payload }, { call, put }) {
      console.log('upload effect>>');
      const response = yield call(upload, payload);
      yield put({
        type: 'upload_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *template({ payload }, { call, put }) {
      console.log('template effect>>');
      const response = yield call(template, payload);
      yield put({
        type: 'template_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *import_workcalendar({ payload }, { call, put }) {
      console.log('import_workcalendar effect>>');
      const response = yield call(import_workcalendar, payload);
      yield put({
        type: 'import_workcalendar_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *insert({ payload }, { call, put }) {
      console.log('insert effect>>');
      const response = yield call(insert, payload);
      yield put({
        type: 'insert_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *update({ payload }, { call, put }) {
      console.log('update effect>>');
      const response = yield call(update, payload);
      yield put({
        type: 'update_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *delete_workcalendar({ payload }, { call, put }) {
      console.log('delete_workcalendar effect>>');
      const response = yield call(delete_workcalendar, payload);
      yield put({
        type: 'delete_workcalendar_reducer',
        payload: response.data,
      });
      return response.data;
    },
  },

  reducers: {
    search_reducer(state, action) {
      return {
        ...state,
        list_work_calendar: action.payload?.content,
      };
    },
    view_reducer(state, action) {
      return {
        ...state,
        current_work_calendar: action.payload?.content,
      };
    },
    upload_reducer(state, action) {
      return {
        ...state,

      };
    },
    template_reducer(state, action) {
      return {
        ...state,

      };
    },
    import_workcalendar_reducer(state, action) {
      return {
        ...state,

      };
    },
    insert_reducer(state, action) {
      return {
        ...state,

      };
    },
    update_reducer(state, action) {
      return {
        ...state,

      };
    },
    delete_workcalendar_reducer(state, action) {
      return {
        ...state,

      };
    },
  },
};
