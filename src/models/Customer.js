import {
  search,
  listcenter,
  listpresenter,
  listevent,
  listgender,
  listsource,
  listprovince,
  listdistrict,
  insertcustomer,
  insertparent,
  insertstudent,
  updateclassify,
  updatestatus,
  listquanhe,
  listtrangthai
} from "../services/Customer";

export default {
  namespace: "Customer",

  state: {
    list_customer: [],
    total_customer: 0,
    list_parent: [],
    total_parent: 0,
    list_center: [],
    list_nguoi_gioi_thieu: [],
    list_su_kien: [],
    list_gender: [],
    list_nguon: [],
    list_province: [],
    list_district: [],
    list_quan_he: [],
    list_trang_thai: [],
    options: {
      keyword: "",
      pageNumber: 1,
      pageSize: 10
    },
    summary: []
  },

  effects: {
    *search({ payload }, { call, put }) {
      console.log("search effect>>");
      const response = yield call(search, payload);
      yield put({
        type: "search_reducer",
        payload: response.data
      });
      return response.data;
    },
    *listcenter({ payload }, { call, put }) {
      console.log("listcenter effect>>");
      const response = yield call(listcenter, payload);
      yield put({
        type: "listcenter_reducer",
        payload: response.data
      });
      return response.data;
    },
    *listpresenter({ payload }, { call, put }) {
      console.log("listpresenter effect>>");
      const response = yield call(listpresenter, payload);
      yield put({
        type: "listpresenter_reducer",
        payload: response.data
      });
      return response.data;
    },
    *listevent({ payload }, { call, put }) {
      console.log("listevent effect>>");
      const response = yield call(listevent, payload);
      yield put({
        type: "listevent_reducer",
        payload: response.data
      });
      return response.data;
    },
    *listgender({ payload }, { call, put }) {
      console.log("listgender effect>>");
      const response = yield call(listgender, payload);
      yield put({
        type: "listgender_reducer",
        payload: response.data
      });
      return response.data;
    },
    *listsource({ payload }, { call, put }) {
      console.log("listsource effect>>");
      const response = yield call(listsource, payload);
      yield put({
        type: "listsource_reducer",
        payload: response.data
      });
      return response.data;
    },
    *listprovince({ payload }, { call, put }) {
      console.log("listprovince effect>>");
      const response = yield call(listprovince, payload);
      yield put({
        type: "listprovince_reducer",
        payload: response.data
      });
      return response.data;
    },
    *listdistrict({ payload }, { call, put }) {
      console.log("listdistrict effect>>");
      const response = yield call(listdistrict, payload);
      yield put({
        type: "listdistrict_reducer",
        payload: response.data
      });
      return response.data;
    },
    *listquanhe({ payload }, { call, put }) {
      try {
        console.log("listquanhe effect>>");
        const response = yield call(listquanhe, payload);
        yield put({
          type: "listquanhe_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *listtrangthai({ payload }, { call, put }) {
      try {
        console.log("listtrangthai effect>>");
        const response = yield call(listtrangthai, payload);
        yield put({
          type: "listtrangthai_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *insertcustomer({ payload }, { call, put }) {
      console.log("insertcustomer effect>>");
      const response = yield call(insertcustomer, payload);
      yield put({
        type: "insertcustomer_reducer",
        payload: response.data
      });
      return response.data;
    },
    *insertparent({ payload }, { call, put }) {
      console.log("insertparent effect>>");
      try {
        const response = yield call(insertparent, payload);
        yield put({
          type: "insertparent_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *insertstudent({ payload }, { call, put }) {
      console.log("insertstudent effect>>");
      try {
        const response = yield call(insertstudent, payload);
        yield put({
          type: "insertstudent_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *updateclassify({ payload }, { call, put }) {
      console.log("updateclassify effect>>");
      const response = yield call(updateclassify, payload);
      yield put({
        type: "updateclassify_reducer",
        payload: response.data
      });
      return response.data;
    },
    *updatestatus({ payload }, { call, put }) {
      console.log("updatestatus effect>>");
      const response = yield call(updatestatus, payload);
      yield put({
        type: "updatestatus_reducer",
        payload: response.data
      });
      return response.data;
    },
    *updateoptions({ payload }, { call, put, select }) {
      const oldOptions = yield select(state => state.Customer.options);
      const newOption = { ...oldOptions, ...payload };
      console.log("updateoptions effect>>", newOption);
      try {
        const response = yield call(search, newOption);
        yield put({
          type: "update_options_reducer",
          payload: payload
        });
        yield put({
          type: "search_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    }
  },

  reducers: {
    search_reducer(state, action) {
      return {
        ...state,
        list_customer: action.payload?.content?.customerList.records,
        list_parent: action.payload?.content?.parentList?.records,
        total_customer: action.payload?.content?.customerList?.totalRecords,
        total_parent: action.payload?.content?.parentList?.totalRecords,
        summary: action.payload?.content?.summary
      };
    },
    listcenter_reducer(state, action) {
      return {
        ...state,
        list_center: action.payload?.content
      };
    },
    listpresenter_reducer(state, action) {
      return {
        ...state,
        list_nguoi_gioi_thieu: action.payload?.content
      };
    },
    listevent_reducer(state, action) {
      return {
        ...state,
        list_su_kien: action.payload?.content
      };
    },
    listgender_reducer(state, action) {
      return {
        ...state,
        list_gender: action.payload?.content
      };
    },
    listsource_reducer(state, action) {
      return {
        ...state,
        list_nguon: action.payload?.content
      };
    },
    listprovince_reducer(state, action) {
      return {
        ...state,
        list_province: action.payload?.content
      };
    },
    listdistrict_reducer(state, action) {
      return {
        ...state,
        list_district: action.payload?.content
      };
    },
    listquanhe_reducer(state, action) {
      return {
        ...state,
        list_quan_he: action.payload?.content
      };
    },
    listtrangthai_reducer(state, action) {
      return {
        ...state,
        list_trang_thai: action.payload?.content
      };
    },
    insertcustomer_reducer(state, action) {
      return {
        ...state
      };
    },
    insertparent_reducer(state, action) {
      return {
        ...state
      };
    },
    insertstudent_reducer(state, action) {
      return {
        ...state
      };
    },
    updateclassify_reducer(state, action) {
      return {
        ...state
      };
    },
    updatestatus_reducer(state, action) {
      return {
        ...state
      };
    },
    update_options_reducer(state, action) {
      return {
        ...state,
        options: { ...state.options, ...action.payload }
      };
    }
  }
};
