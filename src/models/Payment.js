import {
    listcenter, search, list, view, insert, update, delete_payment
} from '../services/Payment';

export default {
  namespace: 'Payment',

  state: {
    list_center: [],
    list_payment: [],
    total_record: 0,
    current_payment: {},
  },

  effects: {
    *listcenter({ payload }, { call, put }) {
      console.log('listcenter effect>>');
      const response = yield call(listcenter, payload);
      yield put({
        type: 'listcenter_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *search({ payload }, { call, put }) {
      console.log('search effect>>');
      const response = yield call(search, payload);
      yield put({
        type: 'search_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *list({ payload }, { call, put }) {
      console.log('list effect>>');
      const response = yield call(list, payload);
      yield put({
        type: 'list_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *view({ payload }, { call, put }) {
      console.log('view effect>>');
      const response = yield call(view, payload);
      yield put({
        type: 'view_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *insert({ payload }, { call, put }) {
      console.log('insert effect>>');
      const response = yield call(insert, payload);
      yield put({
        type: 'insert_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *update({ payload }, { call, put }) {
      console.log('update effect>>');
      const response = yield call(update, payload);
      yield put({
        type: 'update_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *delete_payment({ payload }, { call, put }) {
      console.log('delete_payment effect>>');
      const response = yield call(delete_payment, payload);
      yield put({
        type: 'delete_payment_reducer',
        payload: response.data,
      });
      return response.data;
    },
  },

  reducers: {
    listcenter_reducer(state, action) {
      return {
        ...state,
        list_center: action.payload?.content,
      };
    },
    search_reducer(state, action) {
      return {
        ...state,
        list_payment: action.payload?.content,
      };
    },
    list_reducer(state, action) {
      return {
        ...state,
        list_payment: action.payload?.content,
        total_record: action.payload?.content?.length
      };
    },
    view_reducer(state, action) {
      return {
        ...state,
        current_payment: action.payload?.content,
      };
    },
    insert_reducer(state, action) {
      return {
        ...state,

      };
    },
    update_reducer(state, action) {
      return {
        ...state,

      };
    },
    delete_payment_reducer(state, action) {
      return {
        ...state,

      };
    },
  },
};
