import {
  gender,
  maritalstatus,
  contracttype,
  search,
  view,
  insert,
  update,
  delete_account,
  lock,
  unlock,
  getprovince,
  getnationality,
  listcenter
} from "../services/Account";

export default {
  namespace: "Account",

  state: {
    list: [],
    current: {},
    total_record: 0,
    options: {
      code: "",
      full_Name: "",
      phone: "",
      email: "",
      pageNumber: 1,
      pageSize: 10,
      sortBy: "",
      center_Id: ""
    },
    list_gioi_tinh: [],
    list_tinh_trang_hon_nhan: [],
    list_loai_hop_dong: [],
    list_quoc_tich: [],
    list_nguyen_quan: [],
    list_nhom_quyen: [],
    list_account: [],
    current_account: {},
    list_noi_sinh: [],
    list_quoc_tich: []
  },

  effects: {
    *listcenter({ payload }, { call, put }) {
      console.log("listcenter effect>>");
      const response = yield call(listcenter, payload);
      yield put({
        type: "listcenter_reducer",
        payload: response.data
      });
      return response.data;
    },
    *gender({ payload }, { call, put }) {
      console.log("gender effect>>");
      const response = yield call(gender, payload);
      yield put({
        type: "gender_reducer",
        payload: response.data
      });
      return response.data;
    },

    *updateoptions({ payload }, { call, put, select }) {
      console.log("updateoptions effect>>");
      const oldOptions = yield select(state => state.Account.options);
      const newOption = { ...oldOptions, ...payload };
      yield put({
        type: "update_options_reducer",
        payload: payload
      });
      try {
        const response = yield call(search, newOption);
        yield put({
          type: "search_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },

    *maritalstatus({ payload }, { call, put }) {
      console.log("maritalstatus effect>>");
      const response = yield call(maritalstatus, payload);
      yield put({
        type: "maritalstatus_reducer",
        payload: response.data
      });
      return response.data;
    },
    *contracttype({ payload }, { call, put }) {
      console.log("contracttype effect>>");
      const response = yield call(contracttype, payload);
      yield put({
        type: "contracttype_reducer",
        payload: response.data
      });
      return response.data;
    },
    *search({}, { call, put, select }) {
      console.log("search effect>>");
      const params = yield select(state => state.Account.options);
      const response = yield call(search, JSON.stringify(params));
      yield put({
        type: "search_reducer",
        payload: response.data
      });
      return response.data;
    },
    *view({ payload }, { call, put }) {
      console.log("view effect>>");
      const response = yield call(view, payload);
      yield put({
        type: "view_reducer",
        payload: response.data
      });
      return response.data;
    },
    *insert({ payload }, { call, put }) {
      console.log("insert effect>>");
      const response = yield call(insert, payload);
      yield put({
        type: "insert_reducer",
        payload: response.data
      });
      return response.data;
    },
    *update({ payload }, { call, put }) {
      console.log("update effect>>");
      const response = yield call(update, payload);
      yield put({
        type: "update_reducer",
        payload: response.data
      });
      return response.data;
    },
    *delete_account({ payload }, { call, put }) {
      console.log("delete_account effect>>");
      const response = yield call(delete_account, payload);
      yield put({
        type: "delete_account_reducer",
        payload: response.data
      });
      return response.data;
    },
    *lock({ payload }, { call, put }) {
      console.log("lock effect>>");
      const response = yield call(lock, payload);
      yield put({
        type: "lock_reducer",
        payload: response.data
      });
      return response.data;
    },
    *unlock({ payload }, { call, put }) {
      console.log("unlock effect>>");
      const response = yield call(unlock, payload);
      yield put({
        type: "unlock_reducer",
        payload: response.data
      });
      return response.data;
    },
    *removepermission({ payload }, { put }) {
      console.log("removepermission effect>>");
      yield put({
        type: "removepermission_reducer",
        payload: payload
      });
      return payload;
    },
    *getprovince({}, { call, put }) {
      console.log("getprovince effect>>");
      const response = yield call(getprovince);
      yield put({
        type: "getprovince_reducer",
        payload: response.data
      });
      return response.data;
    },
    *getnationality({}, { call, put }) {
      console.log("getnationality effect>>");
      const response = yield call(getnationality);
      yield put({
        type: "getnationality_reducer",
        payload: response.data
      });
      return response.data;
    }
  },

  reducers: {
    listcenter_reducer(state, action) {
      return {
        ...state,
        list_center: action.payload?.content
      };
    },
    gender_reducer(state, action) {
      return {
        ...state,
        list_gioi_tinh: action.payload?.content
      };
    },
    update_options_reducer(state, action) {
      return {
        ...state,
        options: { ...state.options, ...action.payload }
      };
    },
    maritalstatus_reducer(state, action) {
      return {
        ...state,
        list_tinh_trang_hon_nhan: action.payload?.content
      };
    },
    contracttype_reducer(state, action) {
      return {
        ...state,
        list_loai_hop_dong: action.payload?.content
      };
    },
    listnationality_reducer(state, action) {
      return {
        ...state,
        list_quoc_tich: action.payload?.content
      };
    },
    listprovince_reducer(state, action) {
      return {
        ...state,
        list_nguyen_quan: action.payload?.content
      };
    },
    listgroup_reducer(state, action) {
      return {
        ...state,
        list_nhom_quyen: action.payload?.content
      };
    },
    search_reducer(state, action) {
      return {
        ...state,
        list_account: action.payload?.content?.records || [],
        total_record: action.payload?.content?.totalRecords || 0
      };
    },
    view_reducer(state, action) {
      return {
        ...state,
        current_account: action.payload?.content || {}
      };
    },
    insert_reducer(state, action) {
      return {
        ...state
      };
    },
    update_reducer(state, action) {
      return {
        ...state
      };
    },
    delete_account_reducer(state, action) {
      return {
        ...state
      };
    },
    lock_reducer(state, action) {
      return {
        ...state
      };
    },
    unlock_reducer(state, action) {
      return {
        ...state
      };
    },
    removepermission_reducer(state, action) {
      let new_current_account = state.current_account;
      const { index, groupId } = action.payload;
      const groups = new_current_account.centers[index]?.groups.filter(
        item => item.groupId !== groupId
      );
      new_current_account.centers[index]?.groups &&
        (new_current_account.centers[index].groups = groups);
      return {
        ...state
      };
    },
    getprovince_reducer(state, action) {
      return {
        ...state,
        list_noi_sinh: action.payload?.content
      };
    },
    getnationality_reducer(state, action) {
      return {
        ...state,
        list_quoc_tich: action.payload?.content
      };
    }
  }
};
