import { listcenter, listteacher, view, insert, append } from "../services/StudentTest";

export default {
  namespace: "StudentTest",

  state: {
    list_center: [],
    list_giao_vien: [],
    current_student_test: {}
  },

  effects: {
    *listcenter({ payload }, { call, put }) {
      try {
        console.log("listcenter effect>>");
        const response = yield call(listcenter, payload);
        yield put({
          type: "listcenter_reducer",
          payload: response.data
        });
        return response.data;
      } catch {
        return false;
      }
    },
    *listteacher({ payload }, { call, put }) {
      try {
        console.log("listteacher effect>>");
        const response = yield call(listteacher, payload);
        yield put({
          type: "listteacher_reducer",
          payload: response.data
        });
        return response.data;
      } catch {
        return false;
      }
    },
    *view({ payload }, { call, put }) {
      try {
        console.log("view effect>>");
        const response = yield call(view, payload);
        yield put({
          type: "view_reducer",
          payload: response.data
        });
        return response.data;
      } catch {
        return false;
      }
    },
    *insert({ payload }, { call, put }) {
      try {
        console.log("insert effect>>");
        const response = yield call(insert, payload);
        yield put({
          type: "insert_reducer",
          payload: response.data
        });
        return response.data;
      } catch {
        return false;
      }
    },
    *append({ payload }, { call, put }) {
      console.log('append effect>>');
      const response = yield call(append, payload);
      yield put({
        type: 'append_reducer',
        payload: response.data,
      });
      return response.data;
    },
  },

  reducers: {
    listcenter_reducer(state, action) {
      return {
        ...state,
        list_center: action.payload?.content
      };
    },
    listteacher_reducer(state, action) {
      return {
        ...state,
        list_giao_vien: action.payload?.content
      };
    },
    view_reducer(state, action) {
      return {
        ...state,
        current_student_test: action.payload?.content
      };
    },
    insert_reducer(state, action) {
      return {
        ...state
      };
    },
    append_reducer(state, action) {
      return {
        ...state,

      };
    },
  }
};
