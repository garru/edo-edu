import {
  listcenter,
  listteacher,
  listclass,
  list,
  view
} from "../services/ScheduleCalendar";
import moment from "moment";

export default {
  namespace: "ScheduleCalendar",

  state: {
    list: [],
    current: {},
    total_record: 0,
    options: {
      center_Id: "",
      teacher_Id: "",
      class_Id: "",
      year: moment().years(),
      month: moment().months() + 1
    },
    list_trung_tam: [],
    list_giao_vien: [],
    list_lop_hoc: []
  },

  effects: {
    *listcenter({ payload }, { call, put }) {
      console.log("listcenter effect>>");
      const response = yield call(listcenter, payload);
      yield put({
        type: "listcenter_reducer",
        payload: response.data
      });
      return response.data;
    },
    *listclass({ payload }, { call, put }) {
      console.log("listclass effect>>");
      const response = yield call(listclass, payload);
      yield put({
        type: "listclass_reducer",
        payload: response.data
      });
      return response.data;
    },
    *listteacher({ payload }, { call, put }) {
      console.log("listteacher effect>>");
      const response = yield call(listteacher, payload);
      yield put({
        type: "listteacher_reducer",
        payload: response.data
      });
      return response.data;
    },
    *updateoptions({ payload }, { call, put, select }) {
      console.log("updateoptions effect>>");
      const oldOptions = yield select(state => state.ScheduleCalendar.options);
      const newOption = { ...oldOptions, ...payload };
      yield put({
        type: "update_options_reducer",
        payload: payload
      });
      try {
        const response = yield call(list, newOption);
        yield put({
          type: "search_reducer",
          payload: response.data
        });
        return response.data;
      } catch (error) {
        return false;
      }
    },
    *search({}, { call, put, select }) {
      const params = yield select(state => state.ScheduleCalendar.options);
      console.log("search effect>>", params);
      const response = yield call(list, JSON.stringify(params));
      yield put({
        type: "search_reducer",
        payload: response.data
      });
      return response.data;
    },
    *view({ payload }, { call, put }) {
      try {
        const response = yield call(view, payload);
        return response.data;
      } catch (error) {
        return false;
      }
    }
  },

  reducers: {
    listcenter_reducer(state, action) {
      return {
        ...state,
        list_trung_tam: action.payload?.content
      };
    },
    listclass_reducer(state, action) {
      return {
        ...state,
        list_lop_hoc: action.payload?.content
      };
    },
    listteacher_reducer(state, action) {
      return {
        ...state,
        list_giao_vien: action.payload?.content
      };
    },
    update_options_reducer(state, action) {
      return {
        ...state,
        options: { ...state.options, ...action.payload }
      };
    },
    search_reducer(state, action) {
      const data = action.payload?.content;
      const { year, month } = state.options;
      const list_schedule = [];
      if (data) {
        data.forEach(item => {
          if (item.listSchedule?.length) {
            item.listSchedule.forEach(schedule => {
              const day = {
                id: schedule.id,
                title: `${schedule.className}  ${schedule.lessonTime}`,
                allDay: true,
                start: new Date([year, month, item.day]),
                end: new Date([year, month, item.day])
              };
              list_schedule.push(day);
            });
          }
        });
      } else {
      }
      console.log("list_schedule", list_schedule);
      return {
        ...state,
        list: list_schedule
      };
    },
    view_reducer(state, action) {
      return {
        ...state,
        current: action.payload?.content || {}
      };
    }
  }
};
