import {
    listbrand, listdirector, list, view, insert, update, delete_center
} from '../services/Center';

export default {
  namespace: 'Center',

  state: {
    list_brand: [],
    list_giam_doc: [],
    list_center: [],
    current_center: {},
  },

  effects: {
    *listbrand({ payload }, { call, put }) {
      console.log('listbrand effect>>');
      const response = yield call(listbrand, payload);
      yield put({
        type: 'listbrand_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *listdirector({ payload }, { call, put }) {
      console.log('listdirector effect>>');
      const response = yield call(listdirector, payload);
      yield put({
        type: 'listdirector_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *list({ payload }, { call, put }) {
      console.log('list effect>>');
      const response = yield call(list, payload);
      yield put({
        type: 'list_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *view({ payload }, { call, put }) {
      console.log('view effect>>');
      const response = yield call(view, payload);
      yield put({
        type: 'view_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *insert({ payload }, { call, put }) {
      console.log('insert effect>>');
      const response = yield call(insert, payload);
      yield put({
        type: 'insert_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *update({ payload }, { call, put }) {
      console.log('update effect>>');
      const response = yield call(update, payload);
      yield put({
        type: 'update_reducer',
        payload: response.data,
      });
      return response.data;
    },
    *delete_center({ payload }, { call, put }) {
      console.log('delete_center effect>>');
      const response = yield call(delete_center, payload);
      yield put({
        type: 'delete_center_reducer',
        payload: response.data,
      });
      return response.data;
    },
  },

  reducers: {
    listbrand_reducer(state, action) {
      return {
        ...state,
        list_brand: action.payload?.content,
      };
    },
    listdirector_reducer(state, action) {
      return {
        ...state,
        list_giam_doc: action.payload?.content,
      };
    },
    list_reducer(state, action) {
      return {
        ...state,
        list_center: action.payload?.content,
      };
    },
    view_reducer(state, action) {
      return {
        ...state,
        current_center: action.payload?.content,
      };
    },
    insert_reducer(state, action) {
      return {
        ...state,

      };
    },
    update_reducer(state, action) {
      return {
        ...state,

      };
    },
    delete_center_reducer(state, action) {
      return {
        ...state,

      };
    },
  },
};
