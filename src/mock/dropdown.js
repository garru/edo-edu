export const NOI_SINH = [
  {
    text: "Hà Nội",
    code: "1"
  },
  {
    text: "TP HCM",
    code: "2"
  },
  {
    text: "Bình Định",
    code: "3"
  }
];

export const NGUYEN_QUAN = [
  {
    text: "Hà Nội",
    code: "1"
  },
  {
    text: "TP HCM",
    code: "2"
  },
  {
    text: "Bình Định",
    code: "3"
  }
];

export const QUOC_TICH = [
  {
    text: "Việt Nam",
    code: "1"
  },
  {
    text: "Nước Ngoài",
    code: "2"
  }
];

export const HON_NHAN = [
  {
    text: "Vui lòng chọn 1 giá trị",
    code: ""
  },
  {
    text: "Độc thân",
    code: "1"
  },
  {
    text: "Kết hôn",
    code: "2"
  },
  {
    text: "Ly hôn",
    code: "3"
  }
];

export const TRUNG_TAM = [
  {
    text: "Vui lòng chọn 1 giá trị",
    code: ""
  },
  {
    text: "Hội sở chính",
    code: "2"
  },
  {
    text: "Hoàng Ngân",
    code: "3"
  },
  {
    text: "Văn Điển",
    code: "4"
  }
];

export const VI_TRI = [
  {
    text: "Vui lòng chọn 1 giá trị",
    code: "1"
  },
  {
    text: "Trưởng phòng",
    code: "2"
  },
  {
    text: "Nhân viên",
    code: "3"
  }
];

export const PHONG_BAN = [
  {
    text: "Vui lòng chọn 1 giá trị",
    code: ""
  },
  {
    text: "BOD",
    code: "2"
  },
  {
    text: "Giáo vụ",
    code: "3"
  },
  {
    text: "Công nghệ thông tin",
    code: "4"
  },
  {
    text: "Sale và marketing",
    code: "5"
  },
  {
    text: "Chăm sóc khách hàng",
    code: "6"
  }
];

export const HOP_DONG = [
  {
    text: "Vui lòng chọn 1 giá trị",
    code: ""
  },
  {
    text: "Toàn thời gian (Full time)",
    code: "2"
  },
  {
    text: "Bán thời gian (Part time)",
    code: "3"
  },
  {
    text: "Cộng tác viên (Intern)",
    code: "4"
  }
];

export const GIO_HOC = [
  {
    text: "Thứ 2",
    name: 0
  },
  {
    text: "Thứ 3",
    name: 1
  },
  {
    text: "Thứ 4",
    name: 2
  },
  {
    text: "Thứ 5",
    name: 3
  },
  {
    text: "Thứ 6",
    name: 4
  },
  {
    text: "Thứ 7",
    name: 5
  },
  {
    text: "Chủ nhật",
    name: 6
  }
];

export const DANG_BAI = [
  {
    text: "Vui lòng chọn 1 giá trị",
    code: ""
  },
  {
    text: "Bài học",
    code: 1
  },
  {
    text: "Bài kiểm tra",
    code: 2
  },
  {
    text: "Ngoại khoá",
    code: 3
  },
  {
    text: "Sự kiên",
    code: 4
  }
];

export const TIEU_CHI_DANH_GIA = [
  {
    text: "Vui lòng chọn 1 giá trị",
    code: ""
  },
  {
    text: "Ngữ pháp",
    code: 1
  },
  {
    text: "Từ vựng",
    code: 2
  },
  {
    text: "Kỹ năng",
    code: 3
  },
  {
    text: "Nghe",
    code: 4
  }
];
export const CHUONG_TRINH_HOC = [
  {
    text: "Vui lòng chọn 1 giá trị",
    code: ""
  },
  {
    code: "BIBOB 1",
    text: "BIBOB 1"
  },
  {
    code: "BIBOB 2",
    text: "BIBOB 2"
  },
  {
    code: "BIG ENGLIST PLUS 1",
    text: "BIG ENGLIST PLUS 1"
  },
  {
    code: "BIG ENGLIST PLUS 2",
    text: "BIG ENGLIST PLUS 2"
  },
  {
    code: "BIG ENGLIST PLUS 3",
    text: "BIG ENGLIST PLUS 3"
  }
];

export const PHAN_PHOI = [
  {
    text: "Vui lòng chọn giá trị",
    code: ""
  },
  {
    text: "Theo học viên",
    code: 1
  },
  {
    text: "Cả lớp",
    code: 2
  }
];
export const TIEU_CHI_CHA = [
  {
    text: "Vui lòng chọn giá trị",
    code: ""
  },
  {
    text: "Kiến thức",
    code: 1
  }
];

export const TRANG_THAI = [
  {
    text: "Vui lòng chọn 1 giá trị",
    code: ""
  },
  {
    text: "Hoạt động",
    code: 1
  },
  {
    text: "Tạm đóng",
    code: 2
  },
  {
    text: "Ngưng sử dụng",
    code: 3
  }
];

export const LOAI = [
  {
    text: "Vui lòng chọn 1 giá trị",
    code: ""
  },
  {
    text: "Phụ thu",
    code: 1
  },
  {
    text: "Khuyến mãi",
    code: 2
  }
];

export const TRUNG_TAM_MULTIPLE = [
  {
    label: "Hội sở chính",
    value: "2"
  },
  {
    label: "Hoàng Ngân",
    value: "3"
  },
  {
    label: "Văn Điển",
    value: "4"
  }
];

export const DANH_MUC_HOC_TAP = [
  {
    text: "Vui lòng chọn 1 giá trị",
    code: ""
  },
  {
    text: "Dụng cụ học tập",
    code: 1
  },
  {
    text: "Cơ sở vật chất",
    code: 2
  }
];

export const KHO = [
  {
    label: "Chọn 1 giá trị",
    value: ""
  },
  {
    label: "Tất cả",
    value: "all"
  },
  {
    label: "Kho 1",
    value: 1
  },
  {
    label: "Kho 2",
    value: 2
  }
];

export const KIEU_LAP = [
  {
    text: "Vui lòng chọn 1 giá trị",
    code: ""
  },
  {
    text: "Không lập",
    code: ""
  },
  {
    text: "Hằng ngày",
    code: 1
  },
  {
    text: "Theo tuần",
    code: 2
  }
];

export const DK_KET_THUC = [
  {
    text: "Vui lòng chọn 1 giá trị",
    code: ""
  },
  {
    text: "Số buổi",
    code: 0
  },
  {
    text: "Vào ngày",
    code: 1
  },
  {
    text: "Theo giáo trình",
    code: 2
  }
];

export const PHONG_HOC = [
  {
    text: "Vui lòng chọn 1 giá trị",
    code: ""
  },
  {
    text: "Mickey",
    code: 0
  },
  {
    text: "Donal",
    code: 1
  },
  {
    text: "Mie",
    code: 2
  }
];

export const MA_KHUYEN_MAI = [
  {
    text: "Chọn 1 mã khuyến mại",
    code: ""
  }
];

export const LOAI_THU_PHI = [
  {
    text: "Chọn 1 loại thu phí",
    code: ""
  }
];

export const CAP_DO = [
  {
    text: "Started",
    code: ""
  },
  {
    text: "Movers",
    code: "1"
  },
  {
    text: "Flyers",
    code: "2"
  }
];

export const BAI_KIEM_TRA = [
  {
    text: "Kiểm tra giữa kì",
    code: "1"
  },
  {
    text: "Kiểm tra cuối kì",
    code: "2"
  }
];

export const NHA_CUNG_CAP = [
  {
    text: "Chọn 1 giá trị",
    code: ""
  },
  {
    text: "Nhà cung cấp 1",
    code: "1"
  },
  {
    text: "Nhà cung cấp 1",
    code: "2"
  }
];

export const LOAI_TRUNG_TAM = [
  {
    code: "1",
    text: "Trung tâm hoạt động"
  },
  {
    code: "2",
    text: "Hội sở"
  },
  {
    code: "3",
    text: "Hoàng Ngân"
  },
  {
    code: "4",
    text: "Văn Điển"
  }
];
