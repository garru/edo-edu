export default [
  {
    name: "Khách hàng mới",
    code: "1"
  },
  {
    name: "Tiềm năng",
    code: "2"
  },
  {
    name: "Hứa đi học",
    code: "3"
  },
  {
    name: "Đã chốt",
    code: "4"
  },
  {
    name: "Có vấn đề",
    code: "5"
  },
  {
    name: "Dừng chăm sóc",
    code: "6"
  },
  {
    name: "Chăm sóc lại",
    code: "7"
  }
];
