import fetch from "dva/fetch";
import { API_HOST } from "../services/serviceUri";

async function checkStatus(response) {
  if (response.status === 401) {
    localStorage.removeItem("token");
    window.location.reload();
  }
  if (response.status >= 200 && response.status < 300) {
    return response;
  }

  // if (response.status === 403) {
  //   const token = localStorage.getItem("token");
  //   const user = JSON.parse(localStorage.getItem("user_infor"));
  //   const refresh = await fetch(`${API_HOST}/api/Authenticate/Refresh-Token`, {
  //     method: "POST",
  //     headers: {
  //       "Content-type": "application/json",
  //       Authorization: `Bearer ${token}`,
  //       body: {
  //         refreshToken: user?.refreshToken
  //       }
  //     }
  //   });
  //   if (refresh.status >= 200 && refresh.status < 300) {
  //     return "Refreshed";
  //   }
  //   const error = new Error(refresh.statusText);
  //   error.response = response;
  //   throw error;
  // }

  const error = new Error(response.message);
  error.response = response;
  throw error;
}

/**
 * Requests a URL, returning a promise.
 *
 * @param  {string} url       The URL we want to request
 * @param  {object} [options] The options we want to pass to "fetch"
 * @return {object}           An object containing either "data" or "err"
 */

function download(blob, filename) {
  const url = window.URL.createObjectURL(blob);
  const a = document.createElement("a");
  a.style.display = "none";
  a.href = url;
  // the filename you want
  a.download = filename;
  document.body.appendChild(a);
  a.click();
  document.body.removeChild(a);
  window.URL.revokeObjectURL(url);
}

async function request(url, options) {
  // const controller = new AbortController();
  // const { signal } = controller;
  if (options && !options.noToken) {
    const token = localStorage.getItem("token");
    options.headers = {
      ...options.headers,
      Authorization: `Bearer ${token}`
    };
  }

  const response = await fetch(url, options);

  checkStatus(response);

  // if (response === "Refreshed") {
  //   controller.abort();
  //   fetch(url, options);
  //   return;
  // }
  if (options.download) {
    response.blob().then(blob => download(blob, options.fileName));
    return {
      data: true
    };
  } else {
    const data = await response.json();
    const ret = {
      data,
      headers: {}
    };

    if (response.headers.get("x-total-count")) {
      ret.headers["x-total-count"] = response.headers.get("x-total-count");
    }

    return ret;
  }
}

export default request;
