import React from "react";
import PropTypes from "prop-types";
// import {createHashHistory} from 'history';
import { connect } from "dva";

import "../assets/x_ray/scss/style.scss";
import Sidebar from "../components/Sidebar";
import TopHeader from "../components/TopHeader";
import Header from "../pages/home/components/header";
import "../assets/css/reset.css";

// export const history = createHashHistory();

class HomeLayout extends React.Component {
  static propTypes = {
    isFetching: PropTypes.bool,
    isCommonFetching: PropTypes.bool
  };

  constructor(...args) {
    super(...args);
    this.state = {};
  }

  render() {
    
    return (
      <div>
        {/* <TopHeader/> */}
        <div className="wrapper">
          {/*<div className="">*/}
          <div className="iq-sidebar">
            <Sidebar show={true} />
          </div>
          {/*<XSlideBar/>*/}
          <div id="content-page" className="content-page">
            <Header />
            <div className="container-fluid relative">
              <div className="row">
                <div className="col-12">{this.props.children}</div>
              </div>
            </div>
            <footer className="bg-white iq-footer">
              <div className="">
                <div className="row">
                  <div className="col-lg-6">
                    <ul className="list-inline mb-0">
                      <li className="list-inline-item">
                        <a href="privacy-policy.html">Privacy Policy</a>
                      </li>
                      <li className="list-inline-item">
                        <a href="terms-of-service.html">Terms of Use</a>
                      </li>
                    </ul>
                  </div>
                  <div className="col-lg-6 text-right">
                    Copyright 2020 <a href="#">ECORAU</a> All Rights Reserved.
                  </div>
                </div>
              </div>
            </footer>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(({ Global }) => ({
  Global
}))(HomeLayout);

