import React from 'react';
import './index.scss';
import PropTypes from 'prop-types';
import {createHashHistory} from 'history';
import TopHeader from "../components/TopHeader";
import Footer from "../components/Footer";
// import '../assets/x_ray/scss/style.scss';

// import './index.scss'

export const history = createHashHistory();

class BasicLayout extends React.Component {
    static propTypes = {
        isFetching: PropTypes.bool,
        isCommonFetching: PropTypes.bool,
    };

    constructor(...args) {
        super(...args);
        this.state = {};
    }

    componentDidMount() {
    }

    render() {
        console.log("BasicLayout>>>>")
        return (
            <div id='mainContainer' className='test_class'>
                <div >{this.props.children}</div>
            </div>
        );
    }
}

export default BasicLayout;
