import * as yup from "yup";

export default function useStudentStatus({ dispatch, isCreateNew, onClose }) {
  const initialValues = {
    name: "",
    applyFor: null,
    isFinal: false
  };
  const validationSchema = yup.object().shape({
    name: yup.string().required("Tên trạng thái là bắt buộc"),
    applyFor: yup.number().required("Áp dụng cho là bắt buộc"),
    isFinal: yup.boolean()
  });
  const submit = (values) => {
    dispatch({
      type: "Global/showLoading"
    })
    dispatch({
      type: isCreateNew ? "StudentStatus/insert" : "StudentStatus/update",
      payload: {
        ...values
      }
    }).then((res) => { // TODO: move show/hide error in one place for all calling api
      dispatch({
        type: "Global/hideLoading"
      })
      !res.code ? dispatch({ type: "Global/showSuccess"}) : dispatch({ type: "Global/showError"});
      onClose(true);
    }).catch(err => {
      dispatch({
        type: "Global/showError"
      });
      dispatch({
        type: "Global/hideLoading"
      })
    })
  }
  const _delete = (id) => {
    dispatch({
      type: "Global/showLoading"
    })
    dispatch({
      type: "StudentStatus/delete_studentstatus",
      payload: id
    }).then(res => {
      dispatch({ type: "Global/showSuccess"})
      dispatch({type: "Global/hideLoading"})
    }).catch(err => {
      dispatch({type: "Global/hideLoading"})
    })
  }
  return {
    initialValues,
    validationSchema,
    submit,
    _delete
  }
}