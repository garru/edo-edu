import * as moment from "moment";
import * as yup from "yup";
export default function useTeacherIncome({dispatch}) {
  const initialValuesSearch = {
    from_Date: moment().startOf("month").format("YYYY-MM-DD"),
    to_Date: moment(new Date()).format("YYYY-MM-DD")
  }
  const validationSchema = yup.object().shape({
    from_Date: yup.date(),
    to_Date: yup.date().min(yup.ref("from_Date"), "Ngày kết thúc phải lớn hơn ngày bắt đầu")
  })
  const search = (values) => {
    const {from_Date, to_Date} = values || {};
    const payload = {
      from_Date: from_Date || initialValuesSearch.from_Date,
      to_Date: to_Date || initialValuesSearch.to_Date,
    }
    dispatch({
      type: "Global/showLoading"
    });
    dispatch({
      type: "TeacherIncome/search",
      payload
    }).then(
      () => {
        dispatch({
          type: "Global/hideLoading"
        });
      },
      error => {
        dispatch({
          type: "Global/hideLoading"
        });
        dispatch({
          type: "Global/showError"
        });
      }
    );
  }
  return {
    search,
    validationSchema,
    initialValuesSearch
  }
}