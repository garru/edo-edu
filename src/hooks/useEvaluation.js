import * as yup from "yup";
export default function useEvaluation({ dispatch }) {
  const initialValues = {
    criteria: [],
    name: "",
    evaluationObject: 1,
    usingDictionary: true,
    lastComment: true
  }
  const validationSchema = yup.object().shape({
    criteria: yup.array(),
    name: yup.string().required("Tên tiêu chí là bắt buộc"),
    evaluationObject: yup.number(),
    usingDictionary: yup.boolean(),
    lastComment: yup.boolean()
  })
  const onSubmit = (values) => {
    dispatch({
      type: "Global/showLoading"
    })
    dispatch({
      type: "Evaluation/insert",
      payload: values
    }).then((res) => { // TODO: move show/hide error in one place for all calling api
      dispatch({
        type: "Global/hideLoading"
      })
      !res.code ? dispatch({ type: "Global/showSuccess"}) : dispatch({ type: "Global/showError"});
    }).catch(err => {
      dispatch({
        type: "Global/showError"
      });
      dispatch({
        type: "Global/hideLoading"
      })
    })
  }
  return {
    initialValues,
    validationSchema,
    onSubmit
  }
}