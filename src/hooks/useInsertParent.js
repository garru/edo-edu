import * as yup from "yup";
import moment from "moment";

export default function useInsertParent({ dispatch }) {
  const initialValues = {
    classify: "1",
    status: "1",
    relateStudent: [],
    centerId: "1",
    presenterId: "1",
    eventId: "1",
    source: "1",
    facebook: "dasda",
    provinceCode: "31",
    districtCode: "31",
    address: "qweqwe",
    firstName: "Dan",
    lastName: "Do",
    gender: 1,
    birthDate: "1993-03-05",
    email: "dan@gmail.com",
    phone: "0987654321",
    avatarImage: "",
    search: ""
  };
  const validationSchema = yup.object().shape({
    classify: yup.string().required("Trường bắt buộc"),
    status: yup.string().required("Trường bắt buộc"),
    centerId: yup.string().required("Trường bắt buộc"),
    presenterId: yup.string().required("Trường bắt buộc"),
    eventId: yup.string().required("Trường bắt buộc"),
    source: yup.string().required("Trường bắt buộc"),
    facebook: yup.string().required("Trường bắt buộc"),
    provinceCode: yup.string().required("Trường bắt buộc"),
    districtCode: yup.string().required("Trường bắt buộc"),
    address: yup.string().required("Trường bắt buộc"),
    firstName: yup.string().required("Trường bắt buộc"),
    lastName: yup.string().required("Trường bắt buộc"),
    birthDate: yup.string().required("Trường bắt buộc"),
    email: yup
      .string()
      .required("Trường bắt buộc")
      .matches(
        /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
        "Email không hợp lệ"
      ),
    phone: yup
      .string()
      .required("Trường bắt buộc")
      .matches(/(84|0[3|5|7|8|9])+([0-9]{8})\b/g, "Số điện thoại không hợp lệ")
  });

  const submit = (values, listStudent, resetForm) => {
    const submitData = {
      classify: +values.classify,
      status: +values.status,
      relateStudent: listStudent?.map(
        ({ id, code, firstName, lastName, birthDate }) => {
          return {
            id,
            code,
            firstName,
            lastName,
            birthDate: birthDate || moment().format("YYYY-MM-DD")
          };
        }
      ),
      centerId: +values.centerId,
      presenterId: +values.presenterId,
      eventId: +values.eventId,
      source: +values.source,
      facebook: values.facebook,
      provinceCode: values.provinceCode,
      districtCode: values.districtCode,
      address: values.address,
      firstName: values.firstName,
      lastName: values.lastName,
      gender: +values.gender,
      birthDate: values.birthDate,
      email: values.email,
      phone: values.phone
    };
    dispatch({
      type: "Global/showLoading"
    });
    dispatch({
      type: "Customer/insertparent",
      payload: submitData
    }).then(data => {
      dispatch({
        type: "Global/hideLoading"
      });
      if (data && !data.code) {
        dispatch({
          type: "Global/showSuccess"
        });
        resetForm && resetForm(initialValues);
        // window.location.reload();
      } else {
        dispatch({
          type: "Global/showError"
        });
      }
    });
  };
  const search = (e, timeoutSearch) => {
    clearTimeout(timeoutSearch);
    const value = e.target?.value;
    timeoutSearch = setTimeout(() => {
      const { dispatch } = this.props;
      dispatch({
        type: "Global/showLoading"
      });
      dispatch({
        type: "Student/search",
        payload: {
          codeOrName: value || ""
          // sortBy: "name"
        }
      }).then(() => {
        dispatch({
          type: "Global/hideLoading"
        });
      });
    }, 300);
  };

  const getReference = () => {
    dispatch({
      type: "Global/showLoading"
    });
    Promise.all([
      dispatch({
        type: "Customer/listcenter",
        payload: {}
      }),
      dispatch({
        type: "Customer/listpresenter",
        payload: {}
      }),
      dispatch({
        type: "Customer/listevent",
        payload: {}
      }),
      dispatch({
        type: "Customer/listsource",
        payload: {}
      }),
      dispatch({
        type: "Customer/listprovince",
        payload: {}
      }),
      dispatch({
        type: "Customer/listgender",
        payload: {}
      }),
      dispatch({
        type: "Customer/listquanhe",
        payload: {}
      }),
      dispatch({
        type: "Customer/listtrangthai",
        payload: {}
      })
    ]).then(() => {
      dispatch({
        type: "Global/hideLoading"
      });
    });
  };

  const onChangeProvinceCode = id => {
    dispatch({
      type: "Global/showLoading"
    });
    dispatch({
      type: "Customer/listdistrict",
      payload: {
        provinceCode: id
      }
    }).then(() => {
      dispatch({
        type: "Global/hideLoading"
      });
    });
  };

  return {
    initialValues,
    validationSchema,
    submit,
    search,
    getReference,
    onChangeProvinceCode
  };
}
