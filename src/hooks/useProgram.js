import * as yup from 'yup';
export default function useProgram({ dispatch, onClose, isCreateNew }) {
  const initialValues = {
    name: "",
    level: 1,
    lessonCount: 0,
    description: "",
  };
  const validationSchema = yup.object().shape({
    name: yup.string().required("Tên chương trình là bắt buộc"),
    level: yup.number().required(),
    lessonCount: yup.number().required(),
    fileName: yup.string()
  });
  const submit = (values) => {
    dispatch({
      type: "Global/showLoading"
    })
    dispatch({
      type: isCreateNew ? "Program/insert" : "Program/update",
      payload: {
        ...values
      }
    }).then((res) => { // TODO: move show/hide error in one place for all calling api
      dispatch({
        type: "Global/hideLoading"
      })
      !res.code ? dispatch({ type: "Global/showSuccess"}) : dispatch({ type: "Global/showError"});
      onClose(true);
    }).catch(err => {
      dispatch({
        type: "Global/showError"
      });
      dispatch({
        type: "Global/hideLoading"
      })
    })
  }
  const search = (values) => {
    dispatch({
      type: "Global/showLoading"
    })
    dispatch({
      type: "Program/search",
      payload: values
    }).then((res) => { // TODO: move show/hide error in one place for all calling api
      dispatch({
        type: "Global/hideLoading"
      })
    }).catch(err => {
      dispatch({
        type: "Global/showError"
      });
      dispatch({
        type: "Global/hideLoading"
      })
    })
  }
  return {
    initialValues,
    validationSchema,
    submit,
    search
  }
}