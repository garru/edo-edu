import * as yup from "yup";
export default function useTeachingTime({ dispatch,isCreateNew, onClose }) {
  const initialValues = {
    centerId: 0,
    name: "",
    timeBegin: "",
    timeEnd: "",
    mon: true,
    tue: true,
    wed: true,
    thu: true,
    fri: true,
    sat: true,
    sun: true
  };
  const validationSchema = yup.object().shape({
    centerId: yup.number().required("Trường bắt buộc"),
    name: yup.string().required("Trường bắt buộc"),
    timeBegin: yup.string(),
    timeEnd: yup.string(),
    mon: yup.boolean(),
    tue: yup.boolean(),
    wed: yup.boolean(),
    thu: yup.boolean(),
    fri: yup.boolean(),
    sat: yup.boolean(),
    sun: yup.boolean(),
  });
  const submit = (values) => {
    dispatch({
      type: "Global/showLoading"
    })
    dispatch({
      type: isCreateNew ? "TeachingTime/insert" : "TeachingTime/update",
      payload: values
    }).then((res) => {
      dispatch({
        type: "Global/hideLoading"
      })
      !res.code ? dispatch({ type: "Global/showSuccess"}) : dispatch({ type: "Global/showError"});
      onClose(true);
    }).catch(err => {
      dispatch({
        type: "Global/showError"
      });
      dispatch({
        type: "Global/hideLoading"
      })
    })
  }
  const list = () => {
    dispatch({
      type: "Global/showLoading"
    });
    dispatch({
      type: "TeachingTime/list",
    }).then(
      () => {
        dispatch({
          type: "Global/hideLoading"
        });
      },
      error => {
        dispatch({
          type: "Global/hideLoading"
        });
        dispatch({
          type: "Global/showError"
        });
      }
    );
  }
  const _delete = (id) => {
    dispatch({
      type: "Global/showLoading"
    });
    dispatch({
      type: "TeachingTime/delete_teachingtime",
      payload: { id }
    }).then(data => {
      if (data) {
        // this.updateOptions({});
        dispatch({type: "Global/hideLoading"});
        dispatch({ type: "Global/showSuccess"})
      } else {
        dispatch({
          type: "Global/hideLoading"
        });
        dispatch({
          type: "Global/showError"
        });
      }
    });
  }
  return {
    initialValues,
    validationSchema,
    submit,
    list,
    _delete
  }
}