import * as yup from 'yup';
import moment from "moment";
export default function useRoom({ dispatch, isCreateNew, onClose }) {
  const initialValues = {
    name: "",
    roomType: null,
    centerId: null,
    location: "",
    minSize: 0,
    maxSize: 0,
    status: 1,
    effectFrom: moment().format("YYYY-MM-DD"),
    effectTo: moment().format("YYYY-MM-DD")
  };
  const validationSchema = yup.object().shape({
    name: yup.string().required("Tên phòng là bắt buộc"),
    roomType: yup.number(),
    centerId: yup.number().required("Tên trung tâm là bắt buộc"),
    location: yup.string(),
    minSize: yup.number(),
    maxSize: yup.number(),
    status: yup.number(),
    effectFrom: yup.string().nullable(),
    effectTo: yup.string().nullable()
  });
  const submit = (values) => {
    dispatch({
      type: "Global/showLoading"
    })
    dispatch({
      type: isCreateNew ? "Room/insert" : "Room/update",
      payload: {
        ...values
      }
    }).then((res) => { // TODO: move show/hide error in one place for all calling api
      dispatch({
        type: "Global/hideLoading"
      })
      !res.code ? dispatch({ type: "Global/showSuccess"}) : dispatch({ type: "Global/showError"});
      onClose(true);
    }).catch(err => {
      dispatch({
        type: "Global/showError"
      });
      dispatch({
        type: "Global/hideLoading"
      })
    })
  }
  const search = (values) => {
    dispatch({
      type: "Global/showLoading"
    })
    dispatch({
      type: "Room/search",
      payload: values
    }).then(res => {
      dispatch({
        type: "Global/hideLoading"
      })
    }).catch(err => {
      dispatch({
        type: "Global/hideLoading"
      })
    })
  }
  const _delete = (id) => {
    dispatch({
      type: "Global/showLoading"
    })
    dispatch({
      type: "Room/delete_room",
      payload: id
    }).then(res => {
      dispatch({
        type: "Global/hideLoading"
      })
    }).catch(err => {
      dispatch({
        type: "Global/hideLoading"
      })
    })
  }
  return {
    initialValues,
    validationSchema,
    submit,
    search,
    _delete
  }
}