import * as yup from "yup";
export default function usePayment({ dispatch, isCreateNew, onClose }) {
  const initialValues = {
    centerIds: [],
    name: "",
    paymentGroup: 1,
    description: "",
    chargePercent: 0
  }
  const validationSchema = yup.object().shape({
    centerIds: yup.array().min(1, "Trường bắt buộc"),
    name: yup.string().required("Trường bắt buộc"),
    paymentGroup: yup.number(),
    description: yup.string(),
    chargePercent: yup.number()
  })
  const submit = (values) => {
    dispatch({
      type: "Global/showLoading"
    })
    dispatch({
      type: isCreateNew ? "Payment/insert" : "Payment/update",
      payload: values
    }).then((res) => {
      dispatch({
        type: "Global/hideLoading"
      })
      !res.code ? dispatch({ type: "Global/showSuccess"}) : dispatch({ type: "Global/showError"});
      onClose(true);
    }).catch(err => {
      dispatch({
        type: "Global/showError"
      });
      dispatch({
        type: "Global/hideLoading"
      })
    })
  }
  const _delete = (id) => {
    dispatch({
      type: "Global/showLoading"
    });
    dispatch({
      type: "Payment/delete_payment",
      payload: { id }
    }).then(data => {
      if (data) {
        // this.updateOptions({});
        dispatch({type: "Global/hideLoading"});
        dispatch({ type: "Global/showSuccess"})
      } else {
        dispatch({
          type: "Global/hideLoading"
        });
        dispatch({
          type: "Global/showError"
        });
      }
    });
  }
  return {
    initialValues,
    validationSchema,
    submit,
    _delete
  }
}