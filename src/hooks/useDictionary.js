import * as yup from "yup";
export default function useDictionary() {
  const initialValues = {

  }
  const validationSchema = yup.object().shape({

  });
  return {
    initialValues,
    validationSchema
  }
}