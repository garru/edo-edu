import * as yup from "yup";
export default function useCriteria() {
  const initialValues = {
    name: "",
    weight: 0,
    dictionary: [],
    parentId: null,
  }
  const validationSchema = yup.object().shape({
    name: yup.string().required("Kỹ năng - tiêu chí là bắt buộc"),
    weight: yup.number(),
    dictionary: yup.array(),
    parentId: yup.number().nullable()
  });
  return {
    initialValues,
    validationSchema
  }
}