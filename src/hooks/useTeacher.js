export default function useTeacher({ dispatch }) {
  const search = values => {
    dispatch({
      type: "Global/showLoading"
    });
    Promise.all([
      dispatch({
        type: "Teacher/search",
        payload: values
      })
    ]).then(
      () => {
        dispatch({
          type: "Global/hideLoading"
        });
      },
      error => {
        dispatch({
          type: "Global/hideLoading"
        });
        dispatch({
          type: "Global/showError"
        });
      }
    );
  }
  const loadCenter = () => {
    dispatch({
      type: "Center/list"
    });
  }
  return {
    search,
    loadCenter
  }
}