import * as yup from "yup";
export default function useCoursePrice({ dispatch,isCreateNew, onClose }) {
  const initialValues = {
    centerIds: [],
    name: "",
    programId: "",
    amount: 0,
    collectType: 1,
    description: "",
    colorCode: "",
    isActive: true
  };
  const validationSchema = yup.object().shape({
    centerIds: yup.array().min(1, "Trường bắt buộc"),
    name: yup.string().required("Trường bắt buộc"),
    programId: yup.number(),
    amount: yup.number(),
    collectType: yup.number(),
    description: yup.string().nullable(),
    colorCode: yup.string(),
    isActive: yup.boolean()
  });
  const submit = (values) => {
    dispatch({
      type: "Global/showLoading"
    })
    dispatch({
      type: isCreateNew ? "CoursePrice/insert" : "CoursePrice/update",
      payload: values
    }).then((res) => {
      dispatch({
        type: "Global/hideLoading"
      })
      !res.code ? dispatch({ type: "Global/showSuccess"}) : dispatch({ type: "Global/showError"});
      onClose(true);
    }).catch(err => {
      dispatch({
        type: "Global/showError"
      });
      dispatch({
        type: "Global/hideLoading"
      })
    })
  }
  const list = () => {
    dispatch({
      type: "Global/showLoading"
    });
    dispatch({
      type: "CoursePrice/list",
    }).then(
      () => {
        dispatch({
          type: "Global/hideLoading"
        });
      },
      error => {
        dispatch({
          type: "Global/hideLoading"
        });
        dispatch({
          type: "Global/showError"
        });
      }
    );
  }
  const search = values => {
    dispatch({
      type: "Global/showLoading"
    });
    dispatch({
      type: "CoursePrice/search",
      payload: values
    }).then(
      () => {
        dispatch({
          type: "Global/hideLoading"
        });
      },
      error => {
        dispatch({
          type: "Global/hideLoading"
        });
        dispatch({
          type: "Global/showError"
        });
      }
    );
  }
  const listDependencies = () => {
    dispatch({
      type: "CoursePrice/listcenter"
    });
    dispatch({
      type: "CoursePrice/listprogram"
    });
    dispatch({
      type: "CoursePrice/listtype"
    });
  }
  return {
    initialValues,
    validationSchema,
    submit,
    list,
    search,
    listDependencies
  }
}