import * as yup from "yup";
export default function useLesson({ dispatch }) {
  const initialValues = {
    evaluationIds: [],
    programId: null,
    number: 0,
    name: "",
    description: "",
    lessonType: 1,
    foreignTeacher: true,
    lessonContent: "",
    lessonHomework: ""
  };
  const validationSchema = yup.object().shape({
    evaluationIds: yup.array(),
    programId: yup.number().required("Chương trình là bắt buộc"),
    number: yup.number(),
    name: yup.string().required("Tên bài học là bắt buộc"),
    description: yup.string(),
    lessonType: yup.number().required("Dạng bài là bắt buộc"),
    foreignTeacher: yup.boolean(),
    lessonContent: yup.string().required("Nội dung là bắt buộc"),
    lessonHomework: yup.string()
  });
  const onSubmit = (values) => {
    dispatch({
      type: "Global/showLoading"
    })
    dispatch({
      type: "ProgramLesson/insert",
      payload: values
    }).then((res) => { // TODO: move show/hide error in one place for all calling api
      dispatch({
        type: "Global/hideLoading"
      })
      !res.code ? dispatch({ type: "Global/showSuccess"}) : dispatch({ type: "Global/showError"});
    }).catch(err => {
      dispatch({
        type: "Global/showError"
      });
      dispatch({
        type: "Global/hideLoading"
      })
    })
  }
  return {
    initialValues,
    validationSchema,
    onSubmit
  }
}