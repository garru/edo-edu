import React from "react";
import PropTypes from "prop-types";
import "./index.css";
import { Overlay, Popover } from "react-bootstrap";
import Link from "react-router-dom/Link";

class CustomTooltip extends React.Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    show: PropTypes.bool.isRequired,
    onHide: PropTypes.func.isRequired,
    dataSource: PropTypes.array.isRequired,
    style: PropTypes.object
  };

  static defaultProps = {
    show: false,
    dataSource: [],
    index: 0,
    style: {}
  };

  render() {
    const { show, onHide, style, dataSource, index, target } = this.props;

    return (
      <Overlay
        show={show}
        target={target}
        placement="bottom"
        containerPadding={20}
        rootClose={true}
        rootCloseEvent={"click"}
        onHide={e => {
          onHide(e, index);
        }}
        style={style}
      >
        <Popover id="popover-contained">
          <Popover.Content>
            <ul>
              {dataSource.map((item, idx) => (
                <li onClick={e => item.onClick(e)} key={`$tt${idx}`}>
                  {item.render ? (
                    item.render(index)
                  ) : (
                    <>
                      {item.link ? (
                        <Link to={item.link}>
                          <i className={item.className} style={item.style} />
                          &nbsp; {item.label}
                        </Link>
                      ) : (
                        <>
                          <i className={item.className} style={item.style} />
                          &nbsp; {item.label}
                        </>
                      )}
                    </>
                  )}
                </li>
              ))}
            </ul>
          </Popover.Content>
        </Popover>
      </Overlay>
    );
  }
}

export default CustomTooltip;
