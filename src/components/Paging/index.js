import React from "react";
import PropTypes from "prop-types";
import "./index.css";

const DEFAULT_NUMBER_OF_PAGE = 5;
const PER_PAGE = [5, 10, 15, 20];

class Paging extends React.Component {
  static propTypes = {
    pageSize: PropTypes.number.isRequired,
    pageNumber: PropTypes.number.isRequired,
    total: PropTypes.number.isRequired,
    className: PropTypes.string,
    onChange: PropTypes.func
  };

  static defaultProps = {
    onClick: null
  };

  generatePaging = props => {
    const { pageNumber, total, pageSize } = props;
    const totalPage = Math.ceil(total / (pageSize || 10));
    let min, max;
    const hafl_number_of_page = Math.ceil(DEFAULT_NUMBER_OF_PAGE / 2);
    const listPage = [];
    if (totalPage <= DEFAULT_NUMBER_OF_PAGE) {
      min = 1;
      max = totalPage;
    } else {
      if (pageNumber <= hafl_number_of_page) {
        min = 1;
        max = DEFAULT_NUMBER_OF_PAGE;
      } else if (pageNumber >= totalPage - hafl_number_of_page) {
        min = totalPage - DEFAULT_NUMBER_OF_PAGE;
        max = totalPage;
      } else {
        min = pageNumber - hafl_number_of_page + 1;
        max = pageNumber + hafl_number_of_page - 1;
      }
    }
    for (let i = min; i <= max; i++) {
      listPage.push(i);
    }
    return listPage;
  };

  gotoPage = (pageNumber, pageSize) => {
    this.props.onChange({ pageNumber, pageSize });
  };

  render() {
    const { pageSize, pageNumber, total } = this.props;
    const listPage = this.generatePaging(this.props);
    const from = total ? pageSize * (pageNumber - 1) + 1 : total;
    const to = total > pageSize * pageNumber ? pageSize * pageNumber : total;
    const totalPage = Math.ceil(total / pageSize);

    return (
      <div className="paging-wrapper">
        <div className="per-page">
          <span>Số kết quả trên trang &nbsp;</span>
          <select
            id="per_page"
            name="per_page"
            defaultValue={pageSize}
            onChange={e => {
              this.props.onChange({ pageNumber, pageSize: +e.target.value });
            }}
          >
            {PER_PAGE.map(item => (
              <option
                key={`per_page${item}`}
                value={item}
                disabled={item ? false : true}
                defaultValue={item ? false : true}
                hidden={item ? false : true}
              >
                {item}
              </option>
            ))}
          </select>
        </div>

        <div className="paging">
          <ul>
            <li
              key={"First"}
              className={pageNumber <= 1 || total === 0 ? "disabled" : ""}
              onClick={() => this.gotoPage(1, pageSize)}
            >
              <i className="fas fa-angle-double-left"></i>
            </li>
            <li
              key={"Prev"}
              className={pageNumber <= 1 || total === 0 ? "disabled" : ""}
              onClick={() => this.gotoPage(pageNumber - 1, pageSize)}
            >
              <i className="fa fa-chevron-left"></i>
            </li>
            {listPage.map(index => (
              <li
                key={index}
                className={pageNumber === index ? "active" : ""}
                onClick={() => this.gotoPage(index, pageSize)}
              >
                {index}
              </li>
            ))}
            <li
              key={"Next"}
              className={
                pageNumber >= totalPage || total === 0 ? "disabled" : ""
              }
              onClick={() => this.gotoPage(pageNumber + 1, pageSize)}
            >
              <i className="fa fa-chevron-right"></i>
            </li>
            <li
              key={"Last"}
              className={
                pageNumber >= totalPage || total === 0 ? "disabled" : ""
              }
              onClick={() => this.gotoPage(totalPage, pageSize)}
            >
              <i className="fas fa-angle-double-right"></i>
            </li>
          </ul>
        </div>
        <span>
          Hiển thị {from} đến {to} trong tổng số {total} kết quả
        </span>
      </div>
    );
  }
}

export default Paging;
