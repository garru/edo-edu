import React, { useState } from "react";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Tooltip from "react-bootstrap/Tooltip";
import PropTypes from "prop-types";

export default function IconTable(props) {
  const { classes, onClick, text, key, styles } = props;
  return (
    <OverlayTrigger
      key={key}
      placement={"bottom"}
      overlay={<Tooltip id={`tooltip-${key}`}>{text}</Tooltip>}
    >
      <i
        className={classes}
        style={styles}
        onClick={() => {
          onClick();
          console.log("qiwejqi");
        }}
      ></i>
    </OverlayTrigger>
  );
}

IconTable.propTypes = {
  classes: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  text: PropTypes.string.isRequired,
  key: PropTypes.number,
  styles: PropTypes.string
};
