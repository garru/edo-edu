import React from "react";
import "./index.css";

class Loading extends React.Component {
  render() {
    return (
      <div className="loading">
        <div className="center center1">
          <div className="ring"></div>
        </div>
        <div className="center center2">
          <div className="ring"></div>
        </div>
      </div>
    );
  }
}

export default Loading;
