import CustomTooltip from "../CustomTooltip";
import React, {useEffect, useState} from "react";

const Tooltip = ({ onEdit, onDelete, onLock, onChangePassword, model, ...props}) => {
  const { show, toolTipIndex } = props;
  const dataSource = [];
  const [hide, setHide] = useState(!show);
  if (onEdit) {
    dataSource.push({
      label: "Chỉnh sửa",
      className: "fa fa-edit",
      onClick: e => {
        onEdit();
        setHide(true);
      }
    })
  }
  if (onLock) {
    dataSource.push({
      label: model.status === 1 ? "Khoá" : "Mở khoá",
      className: "fa fa-lock",
      onClick: e => {
        onLock();
        setHide(true);
      }
    })
  }
  if (onChangePassword) {
    dataSource.push({
      label: "Đổi mật khẩu",
      className: "fa fa-key",
      onClick: e => {
        setHide(true);
      }
    })
  }
  if (onDelete) {
    dataSource.push({
      label: "Xoá",
      className: "fa fa-trash-alt",
      onClick: e => {
        setHide(true);
        onDelete();
      }
    })
  }
  return (
    <CustomTooltip
      show={props.show}
      onHide={e => {
        setHide(true)
      }}
      dataSource={dataSource}
      target={props.target}
    />
  );
};

export default Tooltip;