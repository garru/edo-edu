import React, { Component } from "react";
import { connect } from "dva";
import Link from "../Link";
import "./style.css";
class Header extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isFullScreen: false,
      lb_name: "Bác sĩ CKI",
      img_user: "images/chuyengia.png"
    };
  }
  toggleFullScreen = () => {
    const isFullScreen = this.state.isFullScreen;
    if (document.fullscreenElement) {
      document.body.requestFullscreen().catch();
    } else {
      try {
        document.fullscreen && document.exitFullscreen();
      } catch {
        document.fullscreen && document.cancelFullScreen();
      }
    }
    this.setState({ isFullScreen: !isFullScreen });
  };

  render() {
    const { title, breadcrums, subTitle } = this.props;
    const length = breadcrums.length;
    return (
      <div className="iq-top-navbar">
        <div className="iq-navbar-custom">
          <div className="header-wrapper">
            <div className="iq-card p-0 m-0 no-shadow">
              <div className="iq-card-body">
                <div className="breadcrum">
                  {breadcrums.map((bre, idx) => {
                    return idx ? (
                      idx === length - 1 ? (
                        <span> / {bre.title}</span>
                      ) : (
                        <React.Fragment>
                          <span> / </span>{" "}
                          <Link to={bre.path}>{bre.title}</Link>
                        </React.Fragment>
                      )
                    ) : (
                      <React.Fragment>
                        <Link to={bre.path}>{bre.title}</Link>
                      </React.Fragment>
                    );
                  })}
                </div>
                <h2>{title}</h2>
                {subTitle ? (
                  <p>{subTitle}</p>
                ) : (
                  <React.Fragment></React.Fragment>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

// export default Header;
export default connect(({ Mock }) => ({
  Mock
}))(Header);
