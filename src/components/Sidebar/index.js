import React from "react";
import MenuItems from "./MenuItems";
import "./sidebar.css";
import { connect } from "dva";
import { Link } from "react-router-dom";

class Sidebar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      // items: MenuItems.Partner,
      items: MenuItems.Main,
      item2s: MenuItems.Partner,
      selected: null,
      activeParent: "",
      user_selected: this.props?.Mock?.user_selected
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    console.log("getDerivedStateFromProps>>>", nextProps);
    // var clone_obj = Object.assign({}, prevState);
    if (prevState.user_selected !== nextProps?.Mock?.user_selected) {
      return { ...prevState, user_selected: nextProps?.Mock?.user_selected };
    }

    return prevState;
  }

  checkActiveItem(url) {
    url = url || "";
    const pathName = window.location.pathname;
    const listPath = pathName.split("/");
    const isActive =
      listPath.indexOf(url.replace("/", "").replace("#", "")) !== -1;
    // isActive && item && item.id && this.setState({activeParent: item.id});
    return url ? isActive : false;
  }

  render_menu() {
    console.log("render_menu>>>");
    return this.state.items.map((item, index) => {
      let activeChild =
        item.children &&
        item.children.find(child => this.checkActiveItem(child.url));
      const activeParent = item.id == this.state.activeParent;
      const activeItem = activeChild || activeParent;
      // console.log("pathName>>>", item, pathName, activeItem);
      activeChild && console.log("pathName>>>", item);
      if (item.isTitle) {
        return (
          <li key={`iq-menu-title_${index}`} className="iq-menu-title">
            <i className="ri-subtract-line"></i>
            <span>{item.title}</span>
          </li>
        );
      } else {
        return item.children ? (
          <li className={activeItem ? "active" : ""} key={item.id}>
            <a
              href={`#${item.id}`}
              className="iq-waves-effect"
              data-toggle="collapse"
              aria-expanded={`${!!activeItem}`}
              onClick={() => {
                this.setState({ activeParent: item.id });
              }}
            >
              <i className={item.icon} /> <span>{item.title}</span>
              <i
                className="fa fa-chevron-right"
                style={{
                  marginRight: 0,
                  marginLeft: "auto"
                }}
              />
            </a>
            <ul
              id={item.id}
              className="iq-submenu collapse"
              data-parent="#iq-sidebar-toggle"
            >
              {item.children.map((child, index) => {
                return (
                  <li
                    key={"li_2" + item.id.toString() + index.toString()}
                    className={this.checkActiveItem(child.url) ? "active" : ""}
                  >
                    <Link to={child.url}>
                      <i className={child.icon} /> <span>{child.title}</span>
                    </Link>
                  </li>
                );
              })}
            </ul>
          </li>
        ) : (
          <li
            className={this.checkActiveItem(item.url) ? "active" : ""}
            key={item.id}
          >
            <a
              href={item.url || "#"}
            >
              <i className={item.icon} /> <span>{item.title}</span>
            </a>
            <ul></ul>
          </li>
        );
      }
    });
  }

  render() {
    let userType = localStorage.getItem("ob_cl_userType");
    let { show } = this.props;
    let activeClass = this.props?.activeClass;
    let parentClass = this.props?.parentClass;
    let selected = this.state.selected;
    let user_selected = this.state.user_selected;

    const pathName = window.location.pathname;

    return (
      <div className="mt-0">
        <nav className="iq-sidebar-menu">
          <div
            className={
              show
                ? "show col-md-12 border-color mb-3 p-0 sidebar-wrapper"
                : "col-md-12 border-color mb-3 p-0 sidebar-wrapper"
            }
            style={{ height: "100%" }}
          >
            <div className="iq-sidebar-logo d-flex justify-content-between">
              <a href="#" className="d-flex align-items-center">
                <img
                  className="img-logo"
                  src="/images/logo_square.png"
                  alt="logo"
                />
                <span className="ml-3">
                  <h4 style={{ color: "white", fontSize: "30px", margin: 0 }}>
                    Eco.Edu
                  </h4>
                </span>
              </a>
              <div className="iq-menu-bt-sidebar">
                <div className="iq-menu-bt align-self-center">
                  <div
                    className="wrapper-menu open"
                    style={{ fontSize: "inherit" }}
                  >
                    <div className="main-circle">
                      <i
                        className="fas fa-chevron-circle-left"
                        style={{ fontSize: 14 }}
                      ></i>
                    </div>
                    <div className="hover-circle">
                      <i
                        className="fas fa-chevron-circle-right"
                        style={{ fontSize: 14 }}
                      ></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div id="sidebar-scrollbar">
              <div>
                <ul
                  id="iq-sidebar-toggle"
                  className="iq-menu"
                  key={user_selected}
                >
                  {this.render_menu()}
                  {this.state.user_selected >= 0 &&
                    this.state.item2s.map(item => {
                      const activeItem = item.children?.find(child =>
                        this.checkActiveItem(child.url)
                      );
                      return item.children ? (
                        <li
                          className={activeItem ? "active" : ""}
                          key={item.id}
                        >
                          <a
                            href={`#${item.id}`}
                            className="iq-waves-effect"
                            data-toggle="collapse"
                            aria-expanded={`${!!activeItem}`}
                          >
                            <i className={item.icon} />
                            <span>{item.title}</span>
                            <i
                              className="fas fa-chevron-right"
                              style={{
                                marginRight: 0,
                                marginLeft: "auto"
                              }}
                            />
                          </a>
                          <ul
                            id={item.id}
                            className="iq-submenu collapse"
                            data-parent="#iq-sidebar-toggle"
                          >
                            {item.children.map(child => {
                              return (
                                <li
                                  className={
                                    this.checkActiveItem(child.url)
                                      ? "active"
                                      : ""
                                  }
                                  key={child.url}
                                >
                                  <Link to={child.url}>
                                    <i className={child.icon} />{" "}
                                    <span>{child.title}</span>
                                  </Link>
                                </li>
                              );
                            })}
                          </ul>
                        </li>
                      ) : (
                        <li
                          className={
                            this.checkActiveItem(item.url) ? "active" : ""
                          }
                          key={item.url}
                        >
                          <Link
                            to={item.url || "#"}
                            className="iq-waves-effect"
                          >
                            <i className={item.icon} />
                            <span>{item.title}</span>
                          </Link>
                        </li>
                      );
                    })}
                </ul>
              </div>
              <div className="p-3"></div>
            </div>
          </div>
        </nav>
      </div>
    );
  }
}

export default connect(({ Mock }) => ({
  Mock
}))(Sidebar);
