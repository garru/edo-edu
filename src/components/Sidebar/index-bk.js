import React from 'react';
import MenuItems from "./MenuItems";
import Link from "../Link";
import {checkPermission} from "../../utils/common";
import "./sidebar.css";
import { connect } from "dva"

class Sidebar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            // items: MenuItems.Partner,
            items: MenuItems.Main,
            item2s: MenuItems.Partner,
            selected: null,
        }
    }

    render() {
        let userType = localStorage.getItem("ob_cl_userType");
        let {show} = this.props;
        let activeClass = this.props?.activeClass;
        let parentClass = this.props?.parentClass;
        let selected = this.state.selected;
        console.log('Sidebar>>>>1',this.props?.Mock?.user_selected);
        return (
            <div  className="mt-0" >
                <div
                    className={show ? "show col-md-12 border-color mb-3 p-0 sidebar-wrapper" : "col-md-12 border-color mb-3 p-0 sidebar-wrapper"}
                    style={{height: '100%'}}>
                    <div className="navbar-brand p-3">
                        <a href="#" className="d-flex align-items-center">
                            <img className="img-logo" src="images/logo_square.png" alt="logo"/>
                            <span className="ml-3"><h4 style={{color: "white", fontSize: "24px"}}>Ecorau.EMR</h4></span>

                        </a>

                    </div>
                    <div id="sidebar-scrollbar" data-scrollbar="true">
                        <ul key={1} className="menu-y text-sm nav nav-tabs sidebar iq-menu" id="iq-sidebar-toggle">
                            {this.state.items?.map((item, index) => (
                                !item.hide && (item.permission && !checkPermission(item.permission) ? null :
                                    <li className={item?.children?.length > 0 ? "dropdown sidebar-item-have-child mt-1" : "mt-1"}
                                        // style={{backgroundColor: "#fff"}}
                                    >
                                        <a href={item.url ? item.url : "#"}
                                        aria-expanded="false"
                                            // data-toggle="collapse"
                                            // data-target={`#${item.activeClass}`}
                                        onClick={() => {
                                            // $(`#${item.activeClass}`).collapse({
                                            //     toggle: false
                                            // })
                                            if (selected == item.activeClass) {
                                                // selected = null;
                                                this.setState({selected: null})
                                            } else {
                                                // selected = item.activeClass
                                                this.setState({selected: item.activeClass})
                                            }
                                        }}
                                        className={`${activeClass === item.activeClass ? 'active' : ''} d-block py-2 no-border-top d-flex justify-content-between align-items-center`}
                                            // type="button"
                                            // id="dropdownMenuButton"
                                            // data-toggle="dropdown"
                                            // aria-haspopup="true"
                                            // aria-expanded="false"
                                        >
                                        <span style={{minHeight: 34, lineHeight: '34px'}}
                                            className="d-flex align-items-center">
                                            <i className={`${item.icon} sidebar-left-icon`} aria-hidden="true"/>
                                            <span className="sidebar-item-title text-nowrap">{item.title}</span>
                                        </span>
                                            {item?.children?.length > 0 &&
                                            <span>
                                                <i className='fas fa-angle-down'/>
                                                <i className='fas fa-angle-right icon-arrow-side-bar'/>
                                            </span>
                                            }
                                            {/*<i className={`${(item.activeClass === parentClass || item.activeClass == selected) ? 'fas fa-angle-down' : 'fas fa-angle-right'} icon-arrow-side-bar`}/>}*/}
                                        </a>
                                        {item?.children?.length > 0 &&
                                        <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <ul id={item.activeClass}
                                                className={`${(item.activeClass === parentClass || item.activeClass == selected) ? 'content-side-bar-active' : 'content-side-bar'} submenu-y p-0`}>
                                                {item?.children?.length > 0 && item?.children?.map((_item, idx) => (
                                                    !_item.hide && (_item.permission && !checkPermission(_item.permission) ? null :
                                                        <li style={{backgroundColor: "#fff"}}>
                                                            <a href={_item.url}
                                                            onClick={() => {
                                                                this.setState({selected: _item.parentClass})
                                                            }}
                                                            className={`${activeClass === _item.activeClass ? 'active' : ''} d-block py-2 dropdown-item`}>
                                                <span style={{minHeight: 34, lineHeight: '34px'}}
                                                    className="d-flex align-items-center">
                                                    <i className={`${_item.icon} sidebar-left-icon`} aria-hidden="true"/>
                                                    <span className="sidebar-item-title">{_item.title}</span>
                                                </span>

                                                            </a>
                                                        </li>
                                                    )))}
                                            </ul>
                                        </div>}
                                    </li>
                                )))}
                        </ul>
                        {
                            this.props?.Mock?.user_selected >= 0 && <ul className="mt-4"><h5 className="text-white text-nowrap">Thông tin bệnh nhân</h5></ul>
                        }
                        {
                            this.props?.Mock?.user_selected >= 0 && <ul key={2} className="mt-2 text-sm nav nav-tabs sidebar overflow-hidden sub-sidebar sidebar iq-menu">
                            {this.state.item2s?.map((item, index) => (
                                !item.hide && (item.permission && !checkPermission(item.permission) ? null :
                                <li style={{backgroundColor: "#fff"}}>
                                    <a href={item.url ? item.url : "#"}
                                    // data-toggle="collapse"
                                    // data-target={`#${item.activeClass}`}
                                    onClick={() => {
                                        // $(`#${item.activeClass}`).collapse({
                                        //     toggle: false
                                        // })
                                        if (selected == item.activeClass) {
                                        // selected = null;
                                        this.setState({selected: null})
                                        } else {
                                        // selected = item.activeClass
                                        this.setState({selected: item.activeClass})
                                        }
                                    }}
                                    className={`${activeClass === item.activeClass ? 'active' : ''} d-block py-2 no-border-top d-flex justify-content-between align-items-center`}>
                                        <span style={{minHeight: 34, lineHeight: '34px'}} className="d-flex align-items-center">
                                            <i className={item.icon} aria-hidden="true"/>
                                            <span className="text-nowrap">{item.title}</span>

                                        </span>
                                    </a>
                                </li>
                                )))}
                            </ul>
                        }
                    </div>
                </div>

                <ul className="p-3"></ul>

            </div>
        )
    }
}

export default connect(({ Mock }) => ({
    Mock
}))(Sidebar);
