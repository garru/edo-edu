import React from 'react';
// import '../../../assets/x_ray/scss/style.scss';
// import '../../../assets/x_ray/css/light/style.scss';

class XSlideBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    componentWillMount() {
    }

    render() {
        return (
            <div class="iq-sidebar">
                <div class="iq-sidebar-logo d-flex justify-content-between">
                    <a href="index.html">
                        <img src="images/logo.png" class="img-fluid" alt=""/>
                        <span>ECORAU</span>
                    </a>
                    <div class="iq-menu-bt-sidebar">
                        <div class="iq-menu-bt align-self-center">
                            <div class="wrapper-menu">
                                <div class="main-circle"><i class="ri-more-fill"></i></div>
                                <div class="hover-circle"><i class="ri-more-2-fill"></i></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="sidebar-scrollbar">
                    <nav class="iq-sidebar-menu">
                        <ul id="iq-sidebar-toggle" class="iq-menu">
                            <li class="iq-menu-title"><i class="ri-subtract-line"></i><span>Chính</span></li>
                            <li>
                                <a className="iq-waves-effect collapsed" data-toggle="collapse"
                                   aria-expanded="false"><i className="ri-apps-fill"></i><span>Trang chủ</span><i
                                    className="ri-arrow-right-s-line iq-arrow-right"></i></a>
                            </li>

                            <li>
                                <a href="/tiep_nhan" className="iq-waves-effect collapsed" data-toggle="collapse"
                                   aria-expanded="false"><i className="ri-apps-fill"></i><span>Tiếp nhận</span><i
                                    className="ri-arrow-right-s-line iq-arrow-right"></i></a>
                            </li>

                            <li>
                                <a href="/danh_sach_benh_nhan" class="iq-waves-effect collapsed" data-toggle="collapse"
                                   aria-expanded="false"><i class="ri-apps-fill"></i><span>Khám bệnh</span><i
                                    class="ri-arrow-right-s-line iq-arrow-right"></i></a>
                            </li>

                            <li>
                                <a className="iq-waves-effect collapsed" data-toggle="collapse"
                                   aria-expanded="false"><i className="ri-apps-fill"></i><span>Nội trú</span><i
                                    className="ri-arrow-right-s-line iq-arrow-right"></i></a>
                            </li>
                            <li>
                                <a  className="iq-waves-effect collapsed" data-toggle="collapse"
                                   aria-expanded="false"><i className="ri-apps-fill"></i><span>Cận lâm sàng</span><i
                                    className="ri-arrow-right-s-line iq-arrow-right"></i></a>
                            </li>
                            <li>
                                <a className="iq-waves-effect collapsed" data-toggle="collapse"
                                   aria-expanded="false"><i className="ri-apps-fill"></i><span>Viện phí</span><i
                                    className="ri-arrow-right-s-line iq-arrow-right"></i></a>
                            </li>
                            <li>
                                <a className="iq-waves-effect collapsed" data-toggle="collapse"
                                   aria-expanded="false"><i className="ri-apps-fill"></i><span>Báo cáo</span><i
                                    className="ri-arrow-right-s-line iq-arrow-right"></i></a>
                            </li>

                            <li className="iq-menu-title"><i className="ri-subtract-line"></i><span>Khác</span>
                            </li>
                            <li>
                                <a href="/thong_tin_lam_san" className="iq-waves-effect collapsed" data-toggle="collapse"
                                   aria-expanded="false"><i className="ri-apps-fill"></i><span>Thông tin lâm sàng</span><i
                                    className="ri-arrow-right-s-line iq-arrow-right"></i></a>
                            </li>

                            <li>
                                <a href="/tong_quan" className="iq-waves-effect collapsed" data-toggle="collapse"
                                   aria-expanded="false"><i className="ri-apps-fill"></i><span>Tổng quan</span><i
                                    className="ri-arrow-right-s-line iq-arrow-right"></i></a>
                            </li>
                            <li>
                                <a href="/y_lenh" className="iq-waves-effect collapsed" data-toggle="collapse"
                                   aria-expanded="false"><i className="ri-apps-fill"></i><span>Y lệnh</span><i
                                    className="ri-arrow-right-s-line iq-arrow-right"></i></a>
                            </li>
                        </ul>
                    </nav>
                    <div class="p-3"></div>
                </div>
            </div>

        );
    }
}

export default XSlideBar;
