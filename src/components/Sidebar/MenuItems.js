const MenuItems = {
  Main: [
    {
      title: "Dashboard",
      activeClass: "dashboard",
      icon: "fas fa-tachometer-alt font-18 icon-side-bar",
      permission: "",
      url: "/giao_vien",
      id: "dashboard"
    },
    {
      title: "Class",
      activeClass: "my_class",
      icon: "fas fa-school font-18 icon-side-bar",
      permission: "",
      id: "my_class",
      children: [
        {
          title: "Danh Sách lớp",
          activeClass: "my_class_list",
          parentClass: "my_class",
          icon: "fas fa-clipboard-list font-18 icon-side-bar",
          url: "/danh_sach_lop",
          permission: ""
        },
        {
          title: "Ghép lớp",
          activeClass: "ghep_lop",
          parentClass: "my_class",
          icon: "fas fa-clipboard-list font-18 icon-side-bar",
          url: "/ghep_lop",
          permission: ""
        },
        {
          title: "Điểm danh/ nhận xét",
          activeClass: "diem_danh",
          parentClass: "my_class",
          icon: "fas fa-clipboard-list font-18 icon-side-bar",
          url: "/diem_danh",
          permission: ""
        },
        {
          title: "Tổng hợp điểm danh theo lớp",
          activeClass: "tong_hop_diem_danh",
          parentClass: "my_class",
          icon: "fas fa-clipboard-list font-18 icon-side-bar",
          url: "/tong_hop_diem_danh",
          permission: ""
        },
        // {
        //   title: "Danh sách học sinh trong lớp",
        //   activeClass: "danh_sach_hoc_sinh_trong_lop",
        //   parentClass: "my_class",
        //   icon: "fas fa-clipboard-list font-18 icon-side-bar",
        //   url: "/danh_sach_hoc_sinh_trong_lop",
        //   permission: ""
        // },
        {
          title: "Lịch học",
          activeClass: "lich_hoc",
          parentClass: "my_class",
          icon: "fas fa-clipboard-list font-18 icon-side-bar",
          url: "/lich_hoc",
          permission: ""
        },
        {
          title: "Lịch học tạo mới",
          activeClass: "lich_hoc",
          parentClass: "my_class",
          icon: "fas fa-clipboard-list font-18 icon-side-bar",
          url: "/tao_moi_lich_hoc",
          permission: ""
        },
        {
          title: "Lịch học chỉnh sửa",
          activeClass: "lich_hoc",
          parentClass: "my_class",
          icon: "fas fa-clipboard-list font-18 icon-side-bar",
          url: "/chinh_sua_lich_hoc",
          permission: ""
        },
        {
          title: "Lớp dạng lịch",
          activeClass: "lop_dang_lich",
          parentClass: "my_class",
          icon: "fas fa-clipboard-list font-18 icon-side-bar",
          url: "/lop_dang_lich",
          permission: ""
        }
      ]
    },
    {
      title: "Teacher",
      activeClass: "teachers",
      icon: "fas fa-wallet font-18 icon-side-bar",
      // url: '/customer/payment-management',
      permission: "",
      id: "teachers",
      children: [
        {
          title: "Danh sách giáo viên",
          activeClass: "danh_sach_giao_vien",
          parentClass: "teachers",
          icon: "fas fa-clipboard-list font-18 icon-side-bar",
          url: "/danh_sach_giao_vien",
          permission: ""
        },
        {
          title: "Chi tiết giáo viên - Rate giáo viên",
          activeClass: "rate_giao_vien",
          parentClass: "teachers",
          icon: "fas fa-clipboard-list font-18 icon-side-bar",
          url: "/chi_tiet_giao_vien_rate_giao_vien",
          permission: ""
        },
        {
          title: "Chi tiết giáo viên - Tổng quan",
          activeClass: "chi_tiet_giao_vien_tong_quan",
          parentClass: "teachers",
          icon: "fas fa-clipboard-list font-18 icon-side-bar",
          url: "/chi_tiet_giao_vien_tong_quan",
          permission: ""
        }
      ]
    },
    {
      title: "CRM",
      activeClass: "crm",
      icon: "fas fa-wallet font-18 icon-side-bar",
      // url: '/customer/payment-management',
      permission: "",
      id: "crm",
      children: [
        {
          title: "Thêm Phụ Huynh",
          activeClass: "them_phu_huynh",
          parentClass: "crm",
          icon: "fas fa-clipboard-list font-18 icon-side-bar",
          url: "/them_phu_huynh",
          permission: ""
        },
        {
          title: "Thêm Học viên",
          activeClass: "them_hoc_vien",
          parentClass: "crm",
          icon: "fas fa-clipboard-list font-18 icon-side-bar",
          url: "/them_hoc_vien",
          permission: ""
        },
        {
          title: "Xuất hóa đơn",
          activeClass: "xuat_hoa_don",
          parentClass: "crm",
          icon: "fas fa-clipboard-list font-18 icon-side-bar",
          url: "/xuat_hoa_don",
          permission: ""
        },
        {
          title: "Chỉnh sửa hóa đơn- chưa xuất",
          activeClass: "hd_chua_xuat",
          parentClass: "crm",
          icon: "fas fa-clipboard-list font-18 icon-side-bar",
          url: "/hd_chua_xuat",
          permission: ""
        },
        {
          title: "CRM List",
          activeClass: "crm_list",
          parentClass: "crm",
          icon: "fas fa-clipboard-list font-18 icon-side-bar",
          url: "/crm_list",
          permission: ""
        },
        {
          title: "Chi tiết KH-Trao đổi",
          activeClass: "trao_doi",
          parentClass: "crm",
          icon: "fas fa-clipboard-list font-18 icon-side-bar",
          url: "/trao_doi",
          permission: ""
        },
        {
          title: "Chi tiết KH-LS chăm sóc",
          activeClass: "cham_soc",
          parentClass: "crm",
          icon: "fas fa-clipboard-list font-18 icon-side-bar",
          url: "/cham_soc",
          permission: ""
        },
        {
          title: "Chi tiết KH-Hóa đơn",
          activeClass: "hoa_don",
          parentClass: "crm",
          icon: "fas fa-clipboard-list font-18 icon-side-bar",
          url: "/hoa_don",
          permission: ""
        },
        {
          title: "Chi tiết KH-Điểm danh nhận xét",
          activeClass: "dd_nhan_xet",
          parentClass: "crm",
          icon: "fas fa-clipboard-list font-18 icon-side-bar",
          url: "/dd_nhan_xet",
          permission: ""
        },
        {
          title: "Chi tiết KH-Lịch sử học",
          activeClass: "lich_su_hoc",
          parentClass: "crm",
          icon: "fas fa-clipboard-list font-18 icon-side-bar",
          url: "/lich_su_hoc",
          permission: ""
        }
      ]
    },

    {
      title: "Hóa đơn",
      activeClass: "hoa_don",
      icon: "fas fa-wallet font-18 icon-side-bar",
      url: "/hoa_don",
      permission: "",
      id: "hoa_don",
      children: [
        {
          title: "Thanh toán",
          activeClass: "thanh_toan",
          parentClass: "hoa_don",
          icon: "fas fa-clipboard-list font-18 icon-side-bar",
          url: "/thanh_toan",
          permission: ""
        },
        {
          title: "Chi tiết",
          activeClass: "chi_tiet_hd",
          parentClass: "hoa_don",
          icon: "fas fa-clipboard-list font-18 icon-side-bar",
          url: "/chi_tiet_hd",
          permission: ""
        },
      ]
    },
    {
      title: "Setting",
      isTitle: true
    },
    {
      title: "Account Information",
      activeClass: "account_information",
      icon: "fas fa-wallet font-18 icon-side-bar",
      url: "/account_information",
      permission: "",
      id: "account_information"
    },
    {
      title: "System setting",
      activeClass: "system_setting",
      icon: "fas fa-wallet font-18 icon-side-bar",
      permission: "",
      id: "system_setting",
      children: [
        {
          title: "Account",
          activeClass: "account",
          parentClass: "system_setting",
          icon: "fas fa-clipboard-list font-18 icon-side-bar",
          url: "/account",
          permission: ""
        },
        {
          title: "Brand Management",
          activeClass: "brand_management",
          parentClass: "system_setting",
          icon: "fas fa-clipboard-list font-18 icon-side-bar",
          url: "/brand_management",
          permission: ""
        },
        {
          title: "Holiday",
          activeClass: "holiday",
          parentClass: "system_setting",
          icon: "fas fa-clipboard-list font-18 icon-side-bar",
          url: "/holiday",
          permission: ""
        },
        {
          title: "Quản lý phòng ban-Vai trò",
          activeClass: "quan_ly_phong_ban_vai_tro",
          parentClass: "system_setting",
          icon: "fas fa-clipboard-list font-18 icon-side-bar",
          url: "/quan_ly_phong_ban_vai_tro",
          permission: ""
        },
        {
          title: "Phân quyền",
          activeClass: "phan_quyen",
          parentClass: "system_setting",
          icon: "fas fa-clipboard-list font-18 icon-side-bar",
          url: "/phan_quyen",
          permission: ""
        },
        {
          title: "Trung tâm",
          activeClass: "trung_tam",
          parentClass: "system_setting",
          icon: "fas fa-clipboard-list font-18 icon-side-bar",
          url: "/trung_tam",
          permission: ""
        }
      ]
    },
    {
      title: "Academy setting",
      activeClass: "academy",
      icon: "fas fa-wallet font-18 icon-side-bar",
      // url: '/customer/payment-management',
      permission: "",
      id: "academy",
      children: [
        {
          title: "Quản lý giờ học",
          activeClass: "quan_ly_gio_hoc",
          parentClass: "academy",
          icon: "fas fa-clipboard-list font-18 icon-side-bar",
          url: "/quan_ly_gio_hoc",
          permission: ""
        },
        {
          title: "Student Status",
          activeClass: "student_status",
          parentClass: "academy",
          icon: "fas fa-clipboard-list font-18 icon-side-bar",
          url: "/student_status",
          permission: ""
        },
        {
          title: "Evaluation category",
          activeClass: "evaluation_category",
          parentClass: "academy",
          icon: "fas fa-clipboard-list font-18 icon-side-bar",
          url: "/evaluation_category",
          permission: ""
        },
        {
          title: "Room Management",
          activeClass: "room_management",
          parentClass: "academy",
          icon: "fas fa-clipboard-list font-18 icon-side-bar",
          url: "/room_management",
          permission: ""
        },
        {
          title: "Program",
          activeClass: "program",
          parentClass: "academy",
          icon: "fas fa-clipboard-list font-18 icon-side-bar",
          url: "/program",
          permission: ""
        },
        {
          title: "Student's school",
          activeClass: "student_s_school",
          parentClass: "academy",
          icon: "fas fa-clipboard-list font-18 icon-side-bar",
          url: "/student_s_school",
          permission: ""
        }
      ]
    },
    {
      title: "Lession",
      activeClass: "lession",
      icon: "fas fa-wallet font-18 icon-side-bar",
      url: "/lession",
      permission: "",
      id: "lession"
    },
    {
      title: "Thời gian rảnh-List",
      activeClass: "thoi_gian_ranh_list",
      icon: "fas fa-wallet font-18 icon-side-bar",
      url: "/thoi_gian_ranh_list",
      permission: "",
      id: "thoi_gian_ranh_list"
    },
    {
      title: "Finance",
      activeClass: "finance",
      icon: "fas fa-wallet font-18 icon-side-bar",
      // url: '/customer/payment-management',
      permission: "",
      id: "finance",
      children: [
        {
          title: "Promotion",
          activeClass: "promotion",
          parentClass: "finance",
          icon: "fas fa-clipboard-list font-18 icon-side-bar",
          url: "/promotion",
          permission: ""
        },
        {
          title: "Course price",
          activeClass: "course_price",
          parentClass: "finance",
          icon: "fas fa-clipboard-list font-18 icon-side-bar",
          url: "/course_price",
          permission: ""
        },

        {
          title: "Kênh thanh toán",
          activeClass: "kenh_thanh_toan",
          parentClass: "finance",
          icon: "fas fa-clipboard-list font-18 icon-side-bar",
          url: "/kenh_thanh_toan",
          permission: ""
        },
        {
          title: "Thống kê thu nhập",
          activeClass: "thong_ke_thu_nhap",
          parentClass: "finance",
          icon: "fas fa-wallet font-18 icon-side-bar",
          url: "thong_ke_thu_nhap",
          permission: ""
        }
      ]
    },
    {
      title: "Cấu hình kho",
      isTitle: true
    },
    {
      title: "Kho",
      activeClass: "kho",
      icon: "fas fa-wallet font-18 icon-side-bar",
      url: "/kho",
      permission: "",
      id: "kho"
    },
    {
      title: "Phiếu nhập xuât",
      activeClass: "phieu_nhap_xuat",
      icon: "fas fa-wallet font-18 icon-side-bar",
      url: "/phieu_nhap_xuat",
      permission: "",
      id: "phieu_nhap_xuat"
    },
    {
      title: "Tồn kho",
      activeClass: "ton_kho",
      icon: "fas fa-wallet font-18 icon-side-bar",
      url: "/ton_kho",
      permission: "",
      id: "ton_kho"
    },
    {
      title: "Nhập hàng",
      activeClass: "nhap_hang",
      icon: "fas fa-wallet font-18 icon-side-bar",
      url: "/nhap_hang",
      permission: "",
      id: "nhap_hang"
    },
    {
      title: "Xuất hàng",
      activeClass: "xuat_hang",
      icon: "fas fa-wallet font-18 icon-side-bar",
      url: "/xuat_hang",
      permission: "",
      id: "xuat_hang"
    },
    {
      title: "Kiểm kho",
      activeClass: "kiem_kho",
      icon: "fas fa-wallet font-18 icon-side-bar",
      url: '/kiem_kho',
      permission: "",
      id: "kiem_kho"
    },
    {
      title: "Quản lý sảm phẩm",
      activeClass: "quan_ly_san_pham",
      icon: "fas fa-wallet font-18 icon-side-bar",
      url: "/quan_ly_san_pham",
      permission: "",
      id: "quan_ly_san_pham"
    },
    {
      title: "Nhà cung cấp",
      activeClass: "nha_cung_cap",
      icon: "fas fa-wallet font-18 icon-side-bar",
      url: "/nha_cung_cap",
      permission: "",
      id: "nha_cung_cap"
    },
    {
      title: "Chuyển kho",
      activeClass: "chuyen_kho",
      icon: "fas fa-wallet font-18 icon-side-bar",
      url: "/chuyen_kho",
      permission: "",
      id: "chuyen_kho"
    }
  ],
  Partner: []
};

export default MenuItems;
