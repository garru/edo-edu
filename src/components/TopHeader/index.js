import React from 'react';
import './style.css';

class TopHeader extends React.Component {

  onChangeLang = (lang) => {
    localStorage.setItem('ob_cl_language', lang);
    // window.OBPubConfig.lang = lang
    window.location.reload()
    // vbnConfig = lang
  };

  render() {
    return (
      <div className="top-header bg-gradient-main color-white d-flex justify-content-between">
        <button className="navbar-toggler" type="button" data-toggle="collapse"
                data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                aria-expanded="false"
                aria-label="Toggle navigation" onClick={()=> this.props.toggleLeftMenu()}>
          <span className="navbar-toggler-icon"/>
        </button>
          <div className="container">
            Hotline: 19000000
          </div>
      </div>
    );
  }
}

export default TopHeader
