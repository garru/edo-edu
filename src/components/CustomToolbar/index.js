import React from "react";

class CustomToolbar extends React.Component {
  render() {
    const replaceMonth = label => {
      let newlabel = label;
      const month = label.split(" ")[0];
      switch (month) {
        case "January":
          return newlabel.replace("January ", "Tháng 1, ");
        case "February":
          return newlabel.replace("February ", "Tháng 2, ");
        case "March":
          return newlabel.replace("March ", "Tháng 3, ");
        case "April":
          return newlabel.replace("April ", "Tháng 4, ");
        case "May":
          return newlabel.replace("May ", "Tháng 5, ");
        case "June":
          return newlabel.replace("June ", "Tháng 6, ");
        case "July":
          return newlabel.replace("July ", "Tháng 7, ");
        case "August":
          return newlabel.replace("August ", "Tháng 8, ");
        case "September":
          return newlabel.replace("September ", "Tháng 9, ");
        case "October":
          return newlabel.replace("October ", "Tháng 10, ");
        case "November":
          return newlabel.replace("November ", "Tháng 11, ");
        default:
          return newlabel.replace("December ", "Tháng 12, ");
      }
    };
    return (
      <div className="rbc-toolbar">
        <span className="rbc-btn-group rbc-btn-group-custom">
          <button type="button" onClick={() => this.navigate("PREV")}>
            <i class="fas fa-arrow-circle-left"></i>
          </button>
        </span>
        <span
          className="rbc-toolbar-label"
          style={{ fontSize: 18, fontWeight: "bold" }}
        >
          {replaceMonth(this.props.label)}
        </span>
        <span className="rbc-btn-group rbc-btn-group-custom">
          <button type="button" onClick={() => this.navigate("NEXT")}>
            <i class="fas fa-arrow-circle-right"></i>
          </button>
        </span>
      </div>
    );
  }

  navigate = action => {
    console.log(action);

    this.props.onNavigate(action);
  };
}
export default CustomToolbar;
