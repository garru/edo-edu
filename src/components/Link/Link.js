import React from 'react';
import PropTypes from 'prop-types';
// import history from '../../history';
import { createHashHistory } from 'history'
export const history = createHashHistory()
function isLeftClickEvent(event) {
  return event.button === 0;
}

function isModifiedEvent(event) {
  return !!(event.metaKey || event.altKey || event.ctrlKey || event.shiftKey);
}

class Link extends React.Component {
  static propTypes = {
    to: PropTypes.string.isRequired,
    className: PropTypes.string,
    children: PropTypes.node.isRequired,
    onClick: PropTypes.func,
  };

  static defaultProps = {
    onClick: null,
  };

  handleClick =(event)=> {
    if (this.props.onClick) {
      this.props.onClick(event);
    }

    if (isModifiedEvent(event) || !isLeftClickEvent(event)) {
      return;
    }

    if (event.defaultPrevented === true) {
      return;
    }
    console.log(this.props.to)
    event.preventDefault();
    // this.props.history.push(this.props.to);
    history.push(this.props.to);
  };

  render() {
    const { to, children, className, ...props } = this.props;
    // console.log("check props>>>>>>", className);
    return (
      <a href={to} {...props} className={className||""} onClick={this.handleClick}>
        {children}
      </a>
    );
  }
}

export default Link;
