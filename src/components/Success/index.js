import React from "react";
import "./index.css";

class Success extends React.Component {
  render() {
    return (
      <div className="handler-wrapper">
        <div className="handler">
          <span className="handler-success">
            {this.props.message || "Thành công"}
          </span>
        </div>
      </div>
    );
  }
}

export default Success;
