import React from 'react';
import PropTypes from 'prop-types';
// import TopHeader from '../TopHeader';
import Footer from '../Footer';
// import {connect} from 'react-redux';
// import HeaderAfterLogin from "../HeaderAfterLogin";
import {createHashHistory} from 'history';
import TopHeader from '../TopHeader';
import HeaderBeforeLogin from '../HeaderBeforeLogin';
import Sidebar from "../Sidebar";

export const history = createHashHistory();

class Layout extends React.Component {
    static propTypes = {
        isFetching: PropTypes.bool,
        isCommonFetching: PropTypes.bool,
    };

    constructor(...args) {
        super(...args);
        this.state = {
            leftMenu: true
        };
    }

    componentDidMount() {
    }

    toggleLeftMenu = () => {
        let status = this.state.leftMenu;
        this.setState({leftMenu: !status})
    }

    render() {
        console.log("Layout>>>>")
        return (
            <div id='mainContainer'>
                {/*<TopHeader/>*/}
                {/*<HeaderAfterLogin /> */}
                {/*<HeaderBeforeLogin/>*/}
                    {/*<div className='content-wrapper d-flex'>*/}
                    <div className='content-wrapper d-flex'>
                        <div className="col-md-3 col-lg-2 pl-0 pr-0 leftmenu-wrapper">
                            <Sidebar show={this.state.leftMenu}/>
                        </div>
                        <div className="col-md-9 col-lg-10 pl-0 pr-0 col-xs-12">
                            <TopHeader toggleLeftMenu={this.toggleLeftMenu}/>
                            <HeaderBeforeLogin/>
                            <div className="content-wrapper">
                                {this.props.children}
                            </div>
                            <Footer/>
                        </div>
                    </div>
                {/*<Footer/>*/}
            </div>
        );
    }
}

// function mapStateToProps(state) {
//     return {
//         isFetching: state.reducer.isFetching,
//     };
// }
//
// export default connect(mapStateToProps)(Layout);

export default Layout;
