import React from "react";

const DateInput = (props) => {
  return (
    <input
      type="date"
      className="form-control form-control-lg"
      id={props.id}
      defaultValue={props.defaultValue}
      value={props.value}
      onChange={props.onChange}
      onBlur={props.onBlur}
    />
  )
}

export default DateInput;