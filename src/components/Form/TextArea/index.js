import React from "react";
import { ErrorMessage } from "formik";

const TextArea = props => (
  <div className="form-group">
    <label htmlFor={props.id}>{props.label}</label>
    <textarea
      className="textarea form-control"
      rows="4"
      name={props.name}
      defaultValue={props.defaultValue}
      value={props.value}
      onChange={props.onChange}
      onBlur={props.onBlur}
      placeholder={props.placeholder}
      disabled={props.disabled}
    />
    <ErrorMessage component="p" className="error-message" name={props.name} />
  </div>
);

export default TextArea;
