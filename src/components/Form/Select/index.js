import React from "react";
import { ErrorMessage } from "formik";
import isEmpty from "lodash/isEmpty";

const Select = props => (
  <div className="form-group">
    <label htmlFor={props.id}>{props.label}</label>
    <select
      className="form-control form-control-lg"
      id={props.id}
      name={props.name}
      defaultValue={props.defaultValue}
      value={props.value}
      onChange={props.onChange}
      onBlur={props.onBlur}
      disabled={!props.options?.length || props.disabled}
    >
      {props.empty && <option value="">{""}</option>}
      {isEmpty(props.value) && (
        <option value={props.value} disabled hidden>
          Vui lòng chọn giá trị
        </option>
      )}

      {props.options?.map(item => (
        <option
          key={`${item.name}_${item.id || item.code}`}
          value={item.id || item.code}
        >
          {item.name}
        </option>
      ))}
    </select>
      {!props.notFormik && <ErrorMessage component="p" className="error-message" name={props.name}/>}
  </div>
);

export default Select;
