import React from "react";
import {ErrorMessage} from "formik";

const Radio = (props) => (
  <div className="form-group">
    <input
      type="radio"
      name={props.name}
      id={props.id}
      value={props.value}
      onChange={props.onChange}
      onBlur={props.onBlur}
    />
    {props.label && <label htmlFor={props.id}>
      &nbsp;&nbsp; {props.label}
    </label>}
    <ErrorMessage
      component="p"
      className="error-message"
      name={props.name}
    />
  </div>
)

export default Radio;