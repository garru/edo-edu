import React from "react";
import { ErrorMessage } from "formik";

const Input = props => (
  <div
    className={`form-group ${
      props.listautocomplete?.length ? "autocomplete" : ""
    }`}
  >
    {props.label && <label htmlFor={props.id}>{props.label}</label>}
    <input
      type={props.type || "text"}
      name={props.name}
      className="form-control form-control-lg"
      id={props.id}
      defaultValue={props.defaultValue}
      value={props.value}
      onChange={props.onChange}
      onBlur={props.onBlur}
      placeholder={props.placeholder}
      listautocomplete={props.listautocomplete}
      {...props}
    />
    {props.listautocomplete?.length ? (
      <div className="autocomplete-items">
        {props.listautocomplete.map((item, index) => (
          <div
            onClick={() => {
              console.log("autocomplete-items");
              props.onPicked && props.onPicked(item);
            }}
            key={`autocomplete${index}`}
          >
            {props.customkey.map(key => item[key]).join(" ")}
          </div>
        ))}
      </div>
    ) : (
      ""
    )}

{!props.notFormik && <ErrorMessage component="p" className="error-message" name={props.name}/>}
  </div>
);

export default Input;
