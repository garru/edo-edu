import React from "react";
import "./index.css";

class Error extends React.Component {
  render() {
    return (
      <div className="handler-wrapper">
        <div className="handler">
          <span className="handler-error">
            {this.props.message || "Có lỗi! Vui lòng thử lại sau"}
          </span>
        </div>
      </div>
    );
  }
}

export default Error;
