import React from "react";
import "./index.css";

class UploadAvatar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    let myInputRef;
  }
  render() {
    const { onChange, onReset, src } = this.props;
    onReset && this.myInputRef && (this.myInputRef.value = "");
    const handleChange = e => {
      const file = e.target.files[0];
      let reader = new FileReader();
      reader.onload = e => {
        let image = e.target.result;
        console.log("base64: ", image);
        onChange(image);
      };
      reader.readAsDataURL(file);
    };
    return (
      <div className={`profile-img-edit m-b-2 ${this.props.className}`} >
        <img
          alt="profile-pic"
          className="profile-pic"
          src={src || "/images/user/11.png"}
        />
        {onChange && (
          <div className="p-image">
            <label htmlFor="upload_avatart">
              <i className="fas fa-camera upload-button"></i>
            </label>
            <input
              accept="image/*"
              className="file-upload"
              type="file"
              id="upload_avatart"
              onChange={e => handleChange(e)}
            />
          </div>
        )}
      </div>
    );
  }
}

export default UploadAvatar;
