import React, { useState, useEffect } from "react";
import { Table } from "react-bootstrap";
import PropTypes from "prop-types";
import Paging from "../Paging";
import cloneDeep from "lodash/cloneDeep";

export default function CustomTable(props) {
  const [checkedAll, setCheckedAll] = useState(false);
  const {
    columns,
    dataSource,
    total,
    onChange,
    checkbox,
    style,
    noPaging
  } = props;
  const [options, setOptions] = useState({
    sortBy: "",
    isAsc: false,
    pageNumber: 1,
    pageSize: 10
  });
  const initCheckedItem = dataSource.map(item => false);
  const [checkedItems, setCheckedItems] = useState(initCheckedItem);
  const [data, setData] = useState([]);

  useEffect(() => {
    // const newDataSource = dataSource(dataSource)
    if (!noPaging) {
      const newData = cloneDeep(dataSource).splice(0, options.pageSize);
      setData(newData);
    }
    if (noPaging) {
      setData(dataSource);
    }
  }, [dataSource]);

  const onCheckedAll = e => {
    const value = e.target.checked;
    let newCheckedItems = [];
    setCheckedAll(true);
    newCheckedItems = checkedItems.map(() => value);
    setCheckedItems(newCheckedItems);
  };

  const updateCheckedItem = (e, index) => {
    let newCheckedItems = checkedItems;
    const value = e.target.checked;
    newCheckedItems[index] = value;
    setCheckedItems(newCheckedItems);
    if (value) {
      const isCheckedAll = this.checkAllCheckboxChecked();
      isCheckedAll && setCheckedAll(true);
    } else {
      setCheckedAll(false);
    }
  };

  const onSort = key => {
    if (key) {
      const newOptions = {
        ...options,
        sortBy: key,
        isAsc: key === options.sortBy ? (options.isAsc ? false : true) : false
      };
      setOptions(newOptions);
      onChange(newOptions);
    }
  };

  const onChangePage = opt => {
    const newOptions = {
      ...opt
    };
    setOptions(newOptions);
    onChange(newOptions);
  };

  return (
    <>
      <div style={{ width: "100%" }}>
        <div style={{ overflowX: "auto", width: "100%" }}>
          <Table bordered hover style={style} className="table">
            <thead>
              <tr>
                {checkbox && (
                  <th>
                    <input
                      type="checkbox"
                      name="check-all"
                      checked={checkedAll}
                      onChange={e => onCheckedAll(e)}
                    />
                  </th>
                )}

                {columns.map((col, index) => (
                  <th
                    className={`${col.class} ${col.sortBy ? "sort" : ""} ${
                      options.sortBy === col.sortBy
                        ? options.isAsc
                          ? "active desc"
                          : "active"
                        : ""
                    }`}
                    key={`table_th_${index}`}
                    onClick={() => {
                      onSort(col.sortBy);
                    }}
                  >
                    {col.customTitle ? (
                      col.customTitle(index)
                    ) : (
                      <span>{col.title}</span>
                    )}
                  </th>
                ))}
              </tr>
            </thead>
            <tbody>
              {data.map((item, index) => (
                <tr key={`table_tr_${index}`}>
                  {checkbox && (
                    <td>
                      <input
                        key={`table_input_${index}`}
                        type="checkbox"
                        name={`table_checkbox_${item.id}`}
                        checked={checkedItems[index] || false}
                        onChange={e => updateCheckedItem(e, index)}
                      />
                    </td>
                  )}
                  {columns.map((col, idx) =>
                    col.render ? (
                      col.overTd ? (
                        col.render(item, index)
                      ) : (
                        <td key={`table_td_${idx}`}>
                          {col.render(item, index)}
                        </td>
                      )
                    ) : (
                      <td key={`table_td_${idx}`}>{item[col.key]}</td>
                    )
                  )}
                </tr>
              ))}
            </tbody>
          </Table>
          {data && data.length ? (
            ""
          ) : (
            <div className="not-found">
              <i className="fas fa-inbox"></i>
              <span>Không tìm thấy kết quả</span>
            </div>
          )}
        </div>
        {!noPaging && (
          <Paging
            pageNumber={options.pageNumber}
            total={total}
            pageSize={options.pageSize}
            onChange={opt => onChangePage(opt)}
          ></Paging>
        )}
      </div>
    </>
  );
}

CustomTable.propTypes = {
  columns: PropTypes.array.isRequired,
  dataSource: PropTypes.array.isRequired,
  onChange: PropTypes.func.isRequired,
  checkbox: PropTypes.bool,
  style: PropTypes.object,
  noPaging: PropTypes.bool
};

CustomTable.defaultProps = {
  columns: [],
  dataSource: [],
  checkbox: false,
  noPaging: false
};
