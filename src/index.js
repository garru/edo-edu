import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import { BrowserRouter, HashRouter } from "react-router-dom";
import dva, { connect } from "dva";
import createLoading from "dva-loading";
import { GoogleReCaptchaProvider } from "react-google-recaptcha-v3";

const app = dva(createLoading());
app.model({ ...require("./models/References").default });
app.model({ ...require("./models/Account").default });
app.model({ ...require("./models/Appointment").default });
app.model({ ...require("./models/Authenticate").default });
app.model({ ...require("./models/Brand").default });
app.model({ ...require("./models/Center").default });
app.model({ ...require("./models/CoursePrice").default });
app.model({ ...require("./models/Customer").default });
app.model({ ...require("./models/CustomerDetail").default });
app.model({ ...require("./models/DepartementRole").default });
app.model({ ...require("./models/Dictionary").default });
app.model({ ...require("./models/Evaluation").default });
app.model({ ...require("./models/Group").default });
app.model({ ...require("./models/Holiday").default });
app.model({ ...require("./models/Payment").default });
app.model({ ...require("./models/Program").default });
app.model({ ...require("./models/ProgramLesson").default });
app.model({ ...require("./models/PromotionSurchage").default });
app.model({ ...require("./models/Room").default });
app.model({ ...require("./models/Student").default });
app.model({ ...require("./models/StudentStatus").default });
app.model({ ...require("./models/StudentTest").default });
app.model({ ...require("./models/StudentTrial").default });
app.model({ ...require("./models/Teacher").default });
app.model({ ...require("./models/TeacherFreetime").default });
app.model({ ...require("./models/TeacherIncome").default });
app.model({ ...require("./models/TeachingTime").default });
app.model({ ...require("./models/User").default });
app.model({ ...require("./models/WorkCalendar").default });
app.model({ ...require("./models/Global").default });
app.model({ ...require("./models/Class").default });
app.model({ ...require("./models/MergeClass").default });
app.model({ ...require("./models/ScheduleAttendance").default });
app.model({ ...require("./models/ReportAttendance").default });
app.model({ ...require("./models/ScheduleCalendar").default });
app.model({ ...require("./models/Dashboard").default });
app.model({ ...require("./models/Invoice").default });
app.model({ ...require("./models/StudentDetail").default });
app.model({ ...require("./models/StudentReserve").default });

app.router(() => (
  <BrowserRouter>
    <GoogleReCaptchaProvider
      reCaptchaKey="6LdE2H0aAAAAAOyoy4Fn2pXVMH6DZO9yus-XcIlt"
      language="[optional_language]"
      useRecaptchaNet="[optional_boolean_value]"
      useEnterprise={true}
      scriptProps={{
        async: false, // optional, default to false,
        defer: false, // optional, default to false
        appendTo: "head", // optional, default to "head", can be "head" or "body",
        nonce: undefined // optional, default undefined
      }}
    >
      <App />
    </GoogleReCaptchaProvider>
  </BrowserRouter>
));
app.start("#root");
